import { defaultCountryCode } from '@app/shared';

abstract class CommonModels {
    constructor(commonModels?: CommonModels) {
        this.createdUserId = commonModels == undefined ? "" : commonModels.createdUserId == undefined ? '' : commonModels.createdUserId;
        this.modifiedUserId = commonModels == undefined ? "" : commonModels.modifiedUserId == undefined ? '' : commonModels.modifiedUserId;
        this.referenceId = commonModels == undefined ? "" : commonModels.referenceId == undefined ? '' : commonModels.referenceId;
        this.partitionId = commonModels == undefined ? null : commonModels.partitionId == undefined ? null : commonModels.partitionId;
        this.customerId = commonModels == undefined ? "" : commonModels.customerId == undefined ? '' : commonModels.customerId;
        this.addressId = commonModels == undefined ? "" : commonModels.addressId == undefined ? '' : commonModels.addressId;
    }
    createdUserId: string;
    modifiedUserId: string;
    referenceId: string;
    partitionId: string;
    customerId: string;
    addressId: string;
}

class DealerKeyHolderInfoAddEditModel {
    constructor(keyHolderInfoModel?: DealerKeyHolderInfoAddEditModel) {
        this.keyholders = keyHolderInfoModel == undefined ? [] : keyHolderInfoModel.keyholders == undefined ? [] : keyHolderInfoModel.keyholders;
        this.emergencyContacts = keyHolderInfoModel == undefined ? [] : keyHolderInfoModel.emergencyContacts == undefined ? [] : keyHolderInfoModel.emergencyContacts;
    }
    keyholders: DealerKeyholderObjectModel[];
    emergencyContacts: DealerEmergencyContactModel[];
}

class DealerKeyholderObjectModel extends CommonModels {
    constructor(keyholderObjectModel?: DealerKeyholderObjectModel) {
        super(keyholderObjectModel);
        this.keyHolderId = keyholderObjectModel == undefined ? "" : keyholderObjectModel.keyHolderId == undefined ? "" : keyholderObjectModel.keyHolderId;
        this.keyHolderName = keyholderObjectModel == undefined ? "" : keyholderObjectModel.keyHolderName == undefined ? "" : keyholderObjectModel.keyHolderName;
        this.contactNo = keyholderObjectModel == undefined ? "" : keyholderObjectModel.contactNo == undefined ? "" : keyholderObjectModel.contactNo;
        this.contactNoCountryCode = keyholderObjectModel == undefined ? defaultCountryCode : keyholderObjectModel.contactNoCountryCode == undefined ? defaultCountryCode : keyholderObjectModel.contactNoCountryCode;
        this.password = keyholderObjectModel == undefined ? "" : keyholderObjectModel.password == undefined ? "" : keyholderObjectModel.password;
        this.index = keyholderObjectModel == undefined ? 0 : keyholderObjectModel.index == undefined ? 0 : keyholderObjectModel.index;
        this.currentCursorIndex = keyholderObjectModel == undefined ? 0 : keyholderObjectModel.currentCursorIndex == undefined ? 0 : keyholderObjectModel.currentCursorIndex;
        this.isPasswordShow = keyholderObjectModel == undefined ? false : keyholderObjectModel.isPasswordShow == undefined ? false : keyholderObjectModel.isPasswordShow;
    }
    keyHolderId: string;
    keyHolderName: string;
    contactNo: string;
    contactNoCountryCode: string;
    password: string;
    index: number;
    currentCursorIndex: number;
    isPasswordShow: boolean;
}

class DealerEmergencyContactModel extends CommonModels {
    constructor(emergencyContactModel?: DealerEmergencyContactModel) {
        super(emergencyContactModel);
        this.emergencyContactId = emergencyContactModel == undefined ? "" : emergencyContactModel.emergencyContactId == undefined ? "" : emergencyContactModel.emergencyContactId;
        this.contactPerson = emergencyContactModel == undefined ? "" : emergencyContactModel.contactPerson == undefined ? "" : emergencyContactModel.contactPerson;
        this.contactNo = emergencyContactModel == undefined ? "" : emergencyContactModel.contactNo == undefined ? "" : emergencyContactModel.contactNo;
        this.contactNoCountryCode = emergencyContactModel == undefined ? defaultCountryCode : emergencyContactModel.contactNoCountryCode == undefined ? defaultCountryCode : emergencyContactModel.contactNoCountryCode;
        this.index = emergencyContactModel == undefined ? 0 : emergencyContactModel.index == undefined ? 0 : emergencyContactModel.index;
        this.currentCursorIndex = emergencyContactModel == undefined ? 0 : emergencyContactModel.currentCursorIndex == undefined ? 0 : emergencyContactModel.currentCursorIndex;
    }
    emergencyContactId: string;
    contactPerson: string;
    contactNo: string;
    contactNoCountryCode: string;
    index: number;
    currentCursorIndex: number;
}

class DealerPasswordsAddEditModel extends CommonModels {
    constructor(passwordsAddEditModel?: DealerPasswordsAddEditModel) {
        super(passwordsAddEditModel);
        this.contractId = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.contractId == undefined ? '' : passwordsAddEditModel.contractId;
        this.specialInstructionId = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.specialInstructionId == undefined ? '' : passwordsAddEditModel.specialInstructionId;
        this.customerSpecialInstructions = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.customerSpecialInstructions == undefined ? '' : passwordsAddEditModel.customerSpecialInstructions;
        this.distressWord = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.distressWord == undefined ? '' : passwordsAddEditModel.distressWord;
        this.medical = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.medical == undefined ? '' : passwordsAddEditModel.medical;
        this.isDogs = passwordsAddEditModel == undefined ? false : passwordsAddEditModel.isDogs == undefined ? false : passwordsAddEditModel.isDogs;
        this.noOfDogs = passwordsAddEditModel == undefined ? null : passwordsAddEditModel.noOfDogs == undefined ? null : passwordsAddEditModel.noOfDogs;
        this.isVAS = passwordsAddEditModel == undefined ? true : passwordsAddEditModel.isVAS == undefined ? true : passwordsAddEditModel.isVAS;
        this.alarmCancellationPassword = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.alarmCancellationPassword == undefined ? '' : passwordsAddEditModel.alarmCancellationPassword;
        this.dogTypes = passwordsAddEditModel == undefined ? [] : passwordsAddEditModel.dogTypes == undefined ? [] : passwordsAddEditModel.dogTypes;
        this.medicalConditions = passwordsAddEditModel == undefined ? [] : passwordsAddEditModel.medicalConditions == undefined ? [] : passwordsAddEditModel.medicalConditions;
        this.dogDescription = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.dogDescription == undefined ? '' : passwordsAddEditModel.dogDescription;
    }
    specialInstructionId: string;
    contractId: string;
    noOfDogs: string;
    customerSpecialInstructions: string;
    distressWord: string;
    medical: string;
    isDogs: boolean;
    isVAS: boolean;
    alarmCancellationPassword: string;
    dogTypes: DealerDocTypes[];
    medicalConditions: DealerMedicalConditions[];
    dogDescription: string;
}

class DealerDocTypes {
    constructor(docTypes?: DealerDocTypes) {
        this.dogTypeDetailId = docTypes == undefined ? "" : docTypes.dogTypeDetailId == undefined ? "" : docTypes.dogTypeDetailId;
        this.dogDetailId = docTypes == undefined ? "" : docTypes.dogDetailId == undefined ? "" : docTypes.dogDetailId;
        this.dogTypeId = docTypes == undefined ? "" : docTypes.dogTypeId == undefined ? "" : docTypes.dogTypeId;
    }
    dogDetailId: string;
    dogTypeId: string;
    dogTypeDetailId: string;
}

class DealerMedicalConditions {
    constructor(medicalConditions?: DealerMedicalConditions) {
        this.medicalDetailId = medicalConditions == undefined ? "" : medicalConditions.medicalDetailId == undefined ? "" : medicalConditions.medicalDetailId;
        this.medicalCondition = medicalConditions == undefined ? "" : medicalConditions.medicalCondition == undefined ? "" : medicalConditions.medicalCondition;
    }
    medicalDetailId: string;
    medicalCondition: string;
}

class DealerSiteHazardAddEditModel extends CommonModels {
    constructor(siteHazardAddEditModel?: DealerSiteHazardAddEditModel) {
        super(siteHazardAddEditModel);
        this.siteHazardId = siteHazardAddEditModel ? siteHazardAddEditModel.siteHazardId == undefined ? '' : siteHazardAddEditModel.siteHazardId : '';
        this.contractId = siteHazardAddEditModel ? siteHazardAddEditModel.contractId == undefined ? '' : siteHazardAddEditModel.contractId : '';
        this.isPremiseVisible = siteHazardAddEditModel ? siteHazardAddEditModel.isPremiseVisible == undefined ? true : siteHazardAddEditModel.isPremiseVisible : true;
        this.isSwimmingPool = siteHazardAddEditModel ? siteHazardAddEditModel.isSwimmingPool == undefined ? true : siteHazardAddEditModel.isSwimmingPool : true;
        this.swimmingPoolLocation = siteHazardAddEditModel ? siteHazardAddEditModel.swimmingPoolLocation == undefined ? '' : siteHazardAddEditModel.swimmingPoolLocation : '';
        this.isSecurityOfficers = siteHazardAddEditModel ? siteHazardAddEditModel.isSecurityOfficers == undefined ? true : siteHazardAddEditModel.isSecurityOfficers : true;
        this.noOfSecurityOfficers = siteHazardAddEditModel ? siteHazardAddEditModel.noOfSecurityOfficers == undefined ? null : siteHazardAddEditModel.noOfSecurityOfficers : null;
        this.securityServiceProviderName = siteHazardAddEditModel ? siteHazardAddEditModel.securityServiceProviderName == undefined ? '' : siteHazardAddEditModel.securityServiceProviderName : '';
        this.controlRoomPhoneNo = siteHazardAddEditModel ? siteHazardAddEditModel.controlRoomPhoneNo == undefined ? '' : siteHazardAddEditModel.controlRoomPhoneNo : '';
        this.controlRoomPhoneNoCountryCode = siteHazardAddEditModel ? siteHazardAddEditModel.controlRoomPhoneNoCountryCode == undefined ? defaultCountryCode : siteHazardAddEditModel.controlRoomPhoneNoCountryCode : defaultCountryCode;
        this.isSecurityProviderControlRoomOnSite = siteHazardAddEditModel ? siteHazardAddEditModel.isSecurityProviderControlRoomOnSite == undefined ? true : siteHazardAddEditModel.isSecurityProviderControlRoomOnSite : true;
        this.riskDescription = siteHazardAddEditModel ? siteHazardAddEditModel.riskDescription == undefined ? '' : siteHazardAddEditModel.riskDescription : '';
        this.description = siteHazardAddEditModel ? siteHazardAddEditModel.description == undefined ? '' : siteHazardAddEditModel.description : '';
        this.oldSiteHazardId = siteHazardAddEditModel ? siteHazardAddEditModel.oldSiteHazardId == undefined ? '' : siteHazardAddEditModel.oldSiteHazardId : '';
    }
    siteHazardId?: string;
    isPremiseVisible: boolean;
    isSwimmingPool: boolean;
    contractId: string;
    swimmingPoolLocation: string;
    isSecurityOfficers: boolean;
    noOfSecurityOfficers: number;
    securityServiceProviderName: string;
    isSecurityProviderControlRoomOnSite: boolean;
    controlRoomPhoneNo: string;
    controlRoomPhoneNoCountryCode: string;
    riskDescription: string;
    description: string;
    oldSiteHazardId?: string;
}

class DealerCellPanicAddEditModel extends CommonModels {
    constructor(cellPanicAddEditModel?: DealerCellPanicAddEditModel) {
        super(cellPanicAddEditModel);
        this.cellPanicId = cellPanicAddEditModel ? cellPanicAddEditModel.cellPanicId == undefined ? '' : cellPanicAddEditModel.cellPanicId : '';
        this.contractId = cellPanicAddEditModel ? cellPanicAddEditModel.contractId == undefined ? '' : cellPanicAddEditModel.contractId : '';
        this.userName = cellPanicAddEditModel ? cellPanicAddEditModel.userName == undefined ? '' : cellPanicAddEditModel.userName : '';
        this.mobileNo = cellPanicAddEditModel ? cellPanicAddEditModel.mobileNo == undefined ? '' : cellPanicAddEditModel.mobileNo : '';
        this.mobileNoCountryCode = cellPanicAddEditModel ? cellPanicAddEditModel.mobileNoCountryCode == undefined ? '+27' : cellPanicAddEditModel.mobileNoCountryCode : '+27';
        this.isActive = cellPanicAddEditModel ? cellPanicAddEditModel.isActive == undefined ? true : cellPanicAddEditModel.isActive : true;
    }
    cellPanicId?: string;
    contractId: string;
    userName: string;
    mobileNo: string;
    mobileNoCountryCode: string;
    isActive: boolean
}

const initialWorkingDays = [{ isChecked: false, name: "Monday" },
{ isChecked: false, name: "Tuesday" }, { isChecked: false, name: "Wednesday" }, { isChecked: false, name: "Thursday" },
{ isChecked: false, name: "Friday" }, { isChecked: false, name: "Saturday" }, { isChecked: false, name: "Sunday" }];

class DealerDomesticWorkersAddEditModel extends CommonModels {
    constructor(domesticWorkersAddEditModel?: DealerDomesticWorkersAddEditModel) {
        super(domesticWorkersAddEditModel);
        this.domesticWorkerTypeId = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.domesticWorkerTypeId == undefined ? '' :
            domesticWorkersAddEditModel.domesticWorkerTypeId : '';
        this.domesticWorkerTypeName = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.domesticWorkerTypeName == undefined ? '' :
            domesticWorkersAddEditModel.domesticWorkerTypeName : '';
        this.domesticWorkerId = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.domesticWorkerId == undefined ? '' :
            domesticWorkersAddEditModel.domesticWorkerId : '';
        this.employeeName = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.employeeName == undefined ? '' : domesticWorkersAddEditModel.employeeName : '';
        this.contactNo = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.contactNo == undefined ? '' : domesticWorkersAddEditModel.contactNo : '';
        this.domesticWorkerName = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.domesticWorkerName == undefined ? '' : domesticWorkersAddEditModel.domesticWorkerName : '';
        this.identityNo = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.identityNo == undefined ? "" : domesticWorkersAddEditModel.identityNo : "";
        this.isSleepIn = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.isSleepIn == undefined ? false : domesticWorkersAddEditModel.isSleepIn : false;
        this.tempWorkingDays = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.tempWorkingDays == undefined ? initialWorkingDays : domesticWorkersAddEditModel.tempWorkingDays : initialWorkingDays;
        this.days = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.days == undefined ? "" : domesticWorkersAddEditModel.days : "";
        this.contactNoCountryCode = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.contactNoCountryCode == undefined ? defaultCountryCode : domesticWorkersAddEditModel.contactNoCountryCode : defaultCountryCode;
        this.tempGUID = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.tempGUID == undefined ? "" : domesticWorkersAddEditModel.tempGUID : "";
    }
    domesticWorkerId?: string;
    employeeName: string;
    identityNo: string;
    contactNo: string;
    contactNoCountryCode: string;
    domesticWorkerTypeName?: string;
    domesticWorkerTypeId?: string;
    domesticWorkerName?: string;
    isSleepIn?: boolean | string = true;
    tempWorkingDays = [];
    days?: string;
    tempGUID?: string;
}

class DealerOpenCloseMonitoringAddEditModel extends CommonModels {
    constructor(openCloseMonitoringAddEditModel?: DealerOpenCloseMonitoringAddEditModel) {
        super(openCloseMonitoringAddEditModel);
        this.mondayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.mondayArmingTime == undefined ? '' : openCloseMonitoringAddEditModel.mondayArmingTime : '';
        this.mondayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.mondayDisarmingTime == undefined ? '' : openCloseMonitoringAddEditModel.mondayDisarmingTime : '';
        this.tuesdayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.tuesdayArmingTime == undefined ? '' : openCloseMonitoringAddEditModel.tuesdayArmingTime : '';
        this.tuesdayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.tuesdayDisarmingTime == undefined ? '' : openCloseMonitoringAddEditModel.tuesdayDisarmingTime : '';
        this.wednesdayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.wednesdayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.wednesdayArmingTime : "";
        this.wednesdayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.wednesdayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.wednesdayDisarmingTime : "";
        this.thursdayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.thursdayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.thursdayArmingTime : "";
        this.thursdayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.thursdayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.thursdayDisarmingTime : "";
        this.fridayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.fridayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.fridayArmingTime : "";
        this.fridayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.fridayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.fridayDisarmingTime : "";
        this.saturdayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.saturdayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.saturdayArmingTime : "";
        this.saturdayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.saturdayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.saturdayDisarmingTime : "";
        this.sundayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.sundayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.sundayArmingTime : "";
        this.sundayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.sundayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.sundayDisarmingTime : "";
        this.publicHolidayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.publicHolidayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.publicHolidayArmingTime : "";
        this.publicHolidayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.publicHolidayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.publicHolidayDisarmingTime : "";
        this.openAndCloseMonitoringId = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.openAndCloseMonitoringId == undefined ? "" : openCloseMonitoringAddEditModel.openAndCloseMonitoringId : "";
        this.monthlyReportEmailTo = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.monthlyReportEmailTo == undefined ? "" : openCloseMonitoringAddEditModel.monthlyReportEmailTo : "";
        this.allDays = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.allDays == undefined ? false : openCloseMonitoringAddEditModel.allDays : false;
        this.contacts = openCloseMonitoringAddEditModel == undefined ? [] : openCloseMonitoringAddEditModel.contacts == undefined ? [] : openCloseMonitoringAddEditModel.contacts;

    }
    mondayArmingTime?: string;
    mondayDisarmingTime?: string;
    tuesdayArmingTime?: string;
    tuesdayDisarmingTime?: string;
    wednesdayArmingTime?: string;
    wednesdayDisarmingTime?: string;
    thursdayArmingTime?: string;
    thursdayDisarmingTime?: string;
    fridayArmingTime?: string;
    fridayDisarmingTime?: string;
    saturdayArmingTime?: string;
    saturdayDisarmingTime?: string;
    sundayArmingTime?: string;
    sundayDisarmingTime?: string;
    publicHolidayArmingTime?: string;
    publicHolidayDisarmingTime?: string;
    monthlyReportEmailTo?: string;
    allDays: boolean;
    openAndCloseMonitoringId?: string;
    contacts: DealerOpenCloseContact[];
}

class DealerOpenCloseContact {
    constructor(openCloseContact?: DealerOpenCloseContact) {
        this.openAndCloseMonitoringContactId = openCloseContact == undefined ? "" : openCloseContact.openAndCloseMonitoringContactId == undefined ? "" : openCloseContact.openAndCloseMonitoringContactId;
        this.smsNoCountryCode = openCloseContact == undefined ? "+27" : openCloseContact.smsNoCountryCode == undefined ? "+27" : openCloseContact.smsNoCountryCode;
        this.smsNo = openCloseContact ? openCloseContact.smsNo == undefined ? "" : openCloseContact.smsNo : "";
        this.index = openCloseContact == undefined ? 0 : openCloseContact.index == undefined ? 0 : openCloseContact.index;
        this.currentCursorIndex = openCloseContact == undefined ? 0 : openCloseContact.currentCursorIndex == undefined ? 0 : openCloseContact.currentCursorIndex;
    }
    openAndCloseMonitoringContactId?: string;
    smsNo?: string;
    smsNoCountryCode?: string;
    index?: number;
    currentCursorIndex?: number;

}

class DealerAccessToPremisesAddEditModel extends CommonModels {
    constructor(accessToPremisesAddEditModel?: DealerAccessToPremisesAddEditModel) {
        super(accessToPremisesAddEditModel);
        this.referenceId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.referenceId == undefined ? "" : accessToPremisesAddEditModel.referenceId : "";
        this.createdUserId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.createdUserId == undefined ? "" : accessToPremisesAddEditModel.createdUserId : "";
        this.customerId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.customerId == undefined ? "" : accessToPremisesAddEditModel.customerId : "";
        this.transmitterId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.transmitterId == undefined ? "" : accessToPremisesAddEditModel.transmitterId : "";
        this.partitionId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.partitionId == undefined ? "" : accessToPremisesAddEditModel.partitionId : "";
        this.receiverId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.receiverId == undefined ? "" : accessToPremisesAddEditModel.receiverId : "";
        this.isArmedResponsePadlock = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isArmedResponsePadlock == undefined ? false : accessToPremisesAddEditModel.isArmedResponsePadlock : false;
        this.isAuthroziedNoAccess = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isAuthroziedNoAccess == undefined ? false : accessToPremisesAddEditModel.isAuthroziedNoAccess : false;
        this.accessPremiseId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.accessPremiseId == undefined ? "" : accessToPremisesAddEditModel.accessPremiseId : "";
        this.padlockAccessCode = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.padlockAccessCode == undefined ? "" : accessToPremisesAddEditModel.padlockAccessCode : "";
        this.barrelLockAccessCode = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.barrelLockAccessCode == undefined ? "" : accessToPremisesAddEditModel.barrelLockAccessCode : "";
        this.isPadlockOnGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isPadlockOnGate == undefined ? false : accessToPremisesAddEditModel.isPadlockOnGate : false;
        this.padlockDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.padlockDescription == undefined ? "" : accessToPremisesAddEditModel.padlockDescription : "";
        this.isBarrelLockFittedOnGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isBarrelLockFittedOnGate == undefined ? false : accessToPremisesAddEditModel.isBarrelLockFittedOnGate : false;
        this.barrelLockFittedDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.barrelLockFittedDescription == undefined ? "" : accessToPremisesAddEditModel.barrelLockFittedDescription : "";
        this.unProtectWallDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.unProtectWallDescription == undefined ? "" : accessToPremisesAddEditModel.unProtectWallDescription : "";
        this.isBarrelLockFitted = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isBarrelLockFitted == undefined ? false : accessToPremisesAddEditModel.isBarrelLockFitted : false;
        this.isDigipad = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isDigipad == undefined ? false : accessToPremisesAddEditModel.isDigipad : false;
        this.isLockBox = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isLockBox == undefined ? false : accessToPremisesAddEditModel.isLockBox : false;
        this.isAuthorisedNoAccess = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isAuthorisedNoAccess == undefined ? false : accessToPremisesAddEditModel.isAuthorisedNoAccess : false;
        this.isArmedResponseReceiverOnGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isArmedResponseReceiverOnGate == undefined ? false : accessToPremisesAddEditModel.isArmedResponseReceiverOnGate : false;
        this.isUnProtectWall = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isUnProtectWall == undefined ? false : accessToPremisesAddEditModel.isUnProtectWall : false;
        this.isVirtualAgentService = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isVirtualAgentService == undefined ? false : accessToPremisesAddEditModel.isVirtualAgentService : false;
        this.siteInstructionForAROfficer = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.siteInstructionForAROfficer == undefined ? null : accessToPremisesAddEditModel.siteInstructionForAROfficer : null;
        this.isSecureGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isSecureGate == undefined ? false : accessToPremisesAddEditModel.isSecureGate : false;
        this.isOpenAccess = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isOpenAccess == undefined ? false : accessToPremisesAddEditModel.isOpenAccess : false;
        this.authroziedNoAccessDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.authroziedNoAccessDescription == undefined ? "" : accessToPremisesAddEditModel.authroziedNoAccessDescription : "";
        this.barrelLockComments = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.barrelLockComments == undefined ? "" : accessToPremisesAddEditModel.barrelLockComments : "";
        this.digipadComments = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.digipadComments == undefined ? "" : accessToPremisesAddEditModel.digipadComments : "";
        this.digipadAccessCode = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.digipadAccessCode == undefined ? "" : accessToPremisesAddEditModel.digipadAccessCode : "";
        this.lockBoxAccessCode = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.lockBoxAccessCode == undefined ? "" : accessToPremisesAddEditModel.lockBoxAccessCode : "";
        this.isPadlockOnDoor = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isPadlockOnDoor == undefined ? false : accessToPremisesAddEditModel.isPadlockOnDoor : false;
        this.isDigipadOnGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isDigipadOnGate == undefined ? false : accessToPremisesAddEditModel.isDigipadOnGate : false;
        this.isLockBox = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isLockBox == undefined ? false : accessToPremisesAddEditModel.isLockBox : false;
        this.isBarrelLockOnDoor = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isBarrelLockOnDoor == undefined ? false : accessToPremisesAddEditModel.isBarrelLockOnDoor : false;
        this.isDigipadOnDoor = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isDigipadOnDoor == undefined ? false : accessToPremisesAddEditModel.isDigipadOnDoor : false;
        this.isLockBoxOnDoor = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isLockBoxOnDoor == undefined ? false : accessToPremisesAddEditModel.isLockBoxOnDoor : false;
        this.openAccessDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.openAccessDescription == undefined ? "" : accessToPremisesAddEditModel.openAccessDescription : "";
        this.secureGateDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.secureGateDescription == undefined ? "" : accessToPremisesAddEditModel.secureGateDescription : "";
        this.lockBoxComments = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.lockBoxComments == undefined ? "" : accessToPremisesAddEditModel.lockBoxComments : "";
        this.armedResponseReceiverOnGateDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.armedResponseReceiverOnGateDescription == undefined ? "" : accessToPremisesAddEditModel.armedResponseReceiverOnGateDescription : "";
        this.lockBoxDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.lockBoxDescription == undefined ? "" : accessToPremisesAddEditModel.lockBoxDescription : "";
        this.digipadDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.digipadDescription == undefined ? "" : accessToPremisesAddEditModel.digipadDescription : "";
        this.oldAccessPremiseId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.oldAccessPremiseId == undefined ? "" : accessToPremisesAddEditModel.oldAccessPremiseId : "";
    }
    referenceId: string;
    createdUserId: string;
    transmitterId: string;
    receiverId: string;
    partitionId: string;
    customerId: string;
    unProtectWallDescription: string;
    accessPremiseId: string;
    isPadlockOnGate?: boolean;
    isArmedResponsePadlock?: boolean;
    padlockAccessCode: string;
    isBarrelLockFitted?: boolean;
    isBarrelLockFittedOnGate?: boolean;
    barrelLockFittedDescription: string;
    barrelLockAccessCode: string;
    isDigipad?: boolean;
    digipadAccessCode: string;
    digipadDescription: string;
    lockBoxDescription: string;
    isLockBox?: boolean;
    lockBoxAccessCode: string;
    isAuthorisedNoAccess?: boolean;
    siteInstructionForAROfficer: string;
    isAuthroziedNoAccess: boolean;
    authroziedNoAccessDescription: string;
    padlockDescription: string;
    barrelLockComments: string;
    digipadComments: string;
    isArmedResponseReceiverOnGate?: boolean;
    isSecureGate: boolean;
    isDigipadOnGate: boolean;
    isUnProtectWall: boolean;
    isOpenAccess: boolean;
    isVirtualAgentService: boolean;
    isPadlockOnDoor: boolean;
    isBarrelLockOnDoor: boolean;
    isDigipadOnDoor: boolean;
    isLockBoxOnDoor: boolean;
    openAccessDescription: string;
    secureGateDescription: string;
    lockBoxComments: string;
    armedResponseReceiverOnGateDescription: string;
    digipads: DealerDigiPadModel[];
    lockBoxes: DealerLockBoxesModel[];
    oldAccessPremiseId?: string;
}

class DealerDigiPadModel {
    constructor(digiPadModel?: DealerDigiPadModel) {
        this.accessPremiseDigipadId = digiPadModel == undefined ? "" : digiPadModel.accessPremiseDigipadId == undefined ? "" : digiPadModel.accessPremiseDigipadId;
        this.digipadAccessCode = digiPadModel == undefined ? "" : digiPadModel.digipadAccessCode == undefined ? "" : digiPadModel.digipadAccessCode;
        this.gateName = digiPadModel == undefined ? "" : digiPadModel.gateName == undefined ? "" : digiPadModel.gateName;
        this.isPasswordShow = digiPadModel == undefined ? false : digiPadModel.isPasswordShow == undefined ? false : digiPadModel.isPasswordShow;
    }
    accessPremiseDigipadId: string;
    digipadAccessCode: string;
    gateName: string;
    isPasswordShow: boolean;
}

class DealerLockBoxesModel {
    constructor(lockBoxesModel?: DealerLockBoxesModel) {
        this.accessPremiseLockBoxId = lockBoxesModel == undefined ? "" : lockBoxesModel.accessPremiseLockBoxId == undefined ? "" : lockBoxesModel.accessPremiseLockBoxId;
        this.lockBoxAccessCode = lockBoxesModel == undefined ? "" : lockBoxesModel.lockBoxAccessCode == undefined ? "" : lockBoxesModel.lockBoxAccessCode;
        this.gateName = lockBoxesModel == undefined ? "" : lockBoxesModel.gateName == undefined ? "" : lockBoxesModel.gateName;
        this.isPasswordShow = lockBoxesModel == undefined ? false : lockBoxesModel.isPasswordShow == undefined ? false : lockBoxesModel.isPasswordShow;
    }
    accessPremiseLockBoxId: string;
    lockBoxAccessCode: string;
    gateName: string;
    isPasswordShow: boolean;
}

class DealerAccountHolderDetailAddEditModel extends CommonModels {
    constructor(accountHolderDetailAddEditModel?: DealerAccountHolderDetailAddEditModel) {
        super(accountHolderDetailAddEditModel);
        this.accountHolderId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.accountHolderId == undefined ? null : accountHolderDetailAddEditModel.accountHolderId : null;
        this.parentId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.parentId == undefined ? "" : accountHolderDetailAddEditModel.parentId : "";
        this.accountHolderName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.accountHolderName == undefined ? "" : accountHolderDetailAddEditModel.accountHolderName : "";
        this.surname = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.surname == undefined ? "" : accountHolderDetailAddEditModel.surname : "";
        this.said = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.said == undefined ? "" : accountHolderDetailAddEditModel.said : "";
        this.bankId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.bankId == undefined ? "" : accountHolderDetailAddEditModel.bankId : "";
        this.accountTypeId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.accountTypeId == undefined ? "" : accountHolderDetailAddEditModel.accountTypeId : "";
        this.debitDate = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.debitDate == undefined ? '0' : accountHolderDetailAddEditModel.debitDate : "0";
        this.accountNumber = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.accountNumber == undefined ? "" : accountHolderDetailAddEditModel.accountNumber : "";
        this.bankBranchId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.bankBranchId == undefined ? "" : accountHolderDetailAddEditModel.bankBranchId : "";
        this.contactName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.contactName == undefined ? "" : accountHolderDetailAddEditModel.contactName : "";
        this.contactNumber = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.contactNumber == undefined ? "" : accountHolderDetailAddEditModel.contactNumber : "";
        this.contactNumberCountryCode = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.contactNumberCountryCode == undefined ? defaultCountryCode : accountHolderDetailAddEditModel.contactNumberCountryCode : defaultCountryCode;
        this.email = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.email == undefined ? "" : accountHolderDetailAddEditModel.email : "";
        this.isSameasPrimaryAddress = accountHolderDetailAddEditModel == undefined ? false : accountHolderDetailAddEditModel.isSameasPrimaryAddress == undefined ? false : accountHolderDetailAddEditModel.isSameasPrimaryAddress;
        this.buildingNumber = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.buildingNumber == undefined ? "" : accountHolderDetailAddEditModel.buildingNumber : "";
        this.buildingName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.buildingName == undefined ? "" : accountHolderDetailAddEditModel.buildingName : "";
        this.address = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.address == undefined ? "" : accountHolderDetailAddEditModel.address : "";
        this.latitude = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.latitude == undefined ? "" : accountHolderDetailAddEditModel.latitude : "";
        this.longitude = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.longitude == undefined ? "" : accountHolderDetailAddEditModel.longitude : "";
        this.latLong = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.latLong == undefined ? "" : accountHolderDetailAddEditModel.latLong : "";
        this.isNewDebtor = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.isNewDebtor == undefined ? true : accountHolderDetailAddEditModel.isNewDebtor : true;
        this.isGroupInvoice = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.isGroupInvoice == undefined ? false : accountHolderDetailAddEditModel.isGroupInvoice : false;
        this.isSendCustomerCopy = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.isSendCustomerCopy == undefined ? true : accountHolderDetailAddEditModel.isSendCustomerCopy : false;
        this.debtorPaymentTypeId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.debtorPaymentTypeId == undefined ? "" : accountHolderDetailAddEditModel.debtorPaymentTypeId : "";
        this.vatNo = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.vatNo == undefined ? "" : accountHolderDetailAddEditModel.vatNo : "";
        this.postalCode = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.postalCode == undefined ? "" : accountHolderDetailAddEditModel.postalCode : "";
        this.streetAddress = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.streetAddress == undefined ? "" : accountHolderDetailAddEditModel.streetAddress : "";
        this.streetNumber = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.streetNumber == undefined ? "" : accountHolderDetailAddEditModel.streetNumber : "";
        this.cityName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.cityName == undefined ? "" : accountHolderDetailAddEditModel.cityName : "";
        this.suburbName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.suburbName == undefined ? "" : accountHolderDetailAddEditModel.suburbName : "";
        this.provinceName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.provinceName == undefined ? "" : accountHolderDetailAddEditModel.provinceName : "";
        this.districtId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.districtId == undefined ? "" : accountHolderDetailAddEditModel.districtId : "";
        this.referenceId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.referenceId == undefined ? "" : accountHolderDetailAddEditModel.referenceId : "";
        this.createdUserId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.createdUserId == undefined ? "" : accountHolderDetailAddEditModel.createdUserId : "";
    }
    accountHolderId?: string;
    parentId: string;
    accountHolderName: string;
    surname: string;
    said: string;
    bankId: string;
    accountTypeId: string;
    debitDate: string;
    accountNumber: string;
    bankBranchId: string;
    contactName: string;
    contactNumber: string;
    contactNumberCountryCode: string;
    email: string;
    isSameasPrimaryAddress: boolean;
    buildingNumber: string;
    buildingName: string;
    address: string;
    latLong: string;
    latitude: string;
    longitude: string;
    isNewDebtor: boolean;
    isGroupInvoice: boolean;
    debtorPaymentTypeId: string;
    isSendCustomerCopy: boolean;
    vatNo: string;
    postalCode: string;
    streetAddress: string;
    streetNumber: string;
    cityName: string;
    suburbName: string;
    provinceName: string;
    districtId?: string;
    referenceId: string;
    createdUserId: string;
}

class DealerCustomerServiceAgreementParamsDataModel {
    constructor(stepperParams?: DealerCustomerServiceAgreementParamsDataModel) {
        this.isAgreementGenerated = stepperParams == undefined ? false : stepperParams.isAgreementGenerated == undefined ? false : stepperParams.isAgreementGenerated;
        this.createdUserId = stepperParams == undefined ? "" : stepperParams.createdUserId == undefined ? "" : stepperParams.createdUserId;
        this.modifiedUserId = stepperParams == undefined ? "" : stepperParams.modifiedUserId == undefined ? "" : stepperParams.modifiedUserId;
        this.partitionId = stepperParams == undefined ? '' : stepperParams.partitionId == undefined ? '' : stepperParams.partitionId;
        this.customerId = stepperParams == undefined ? "" : stepperParams.customerId == undefined ? "" : stepperParams.customerId;
        this.addressId = stepperParams == undefined ? "" : stepperParams.addressId == undefined ? "" : stepperParams.addressId;
        this.agreementTabs = stepperParams == undefined ? [] : stepperParams.agreementTabs == undefined ? [] : stepperParams.agreementTabs;
        this.agreementTabsCopy = stepperParams == undefined ? [] : stepperParams.agreementTabsCopy == undefined ? [] : stepperParams.agreementTabsCopy;
        this.isExist = stepperParams == undefined ? false : stepperParams.isExist == undefined ? false : stepperParams.isExist;
        this.customerRefNo = stepperParams == undefined ? "" : stepperParams.customerRefNo == undefined ? "" : stepperParams.customerRefNo;
        this.customerName = stepperParams == undefined ? "" : stepperParams.customerName == undefined ? "" : stepperParams.customerName;
        this.siteTypeName = stepperParams == undefined ? "" : stepperParams.siteTypeName == undefined ? "" : stepperParams.siteTypeName;
        this.fullAddress = stepperParams == undefined ? "" : stepperParams.fullAddress == undefined ? "" : stepperParams.fullAddress;
        this.referenceId = stepperParams == undefined ? "" : stepperParams.referenceId == undefined ? "" : stepperParams.referenceId;
        this.contractId = stepperParams == undefined ? "" : stepperParams.contractId == undefined ? "" : stepperParams.contractId;
    }
    isAgreementGenerated?: boolean;
    createdUserId?: string;
    modifiedUserId?: string;
    partitionId?: string;
    addressId?: string;
    customerId?: string;
    agreementTabs?:any[];
    agreementTabsCopy?:any[];
    isExist?: boolean;
    customerRefNo?: string;
    customerName?: string;
    siteTypeName?: string;
    fullAddress?: string;
    referenceId?: string;
    contractId?:string;
}
class DealerFinduSubscriberModel extends CommonModels {
    constructor(finduSubscriberModel?: DealerFinduSubscriberModel) {
        super(finduSubscriberModel);
        this.contractId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.contractId == undefined ? '' : finduSubscriberModel.contractId;
        this.mobileNo = finduSubscriberModel == undefined ? '' : finduSubscriberModel.mobileNo == undefined ? '' : finduSubscriberModel.mobileNo;
        this.mobileNoCountryCode = finduSubscriberModel == undefined ? '' : finduSubscriberModel.mobileNoCountryCode == undefined ? '' : finduSubscriberModel.mobileNoCountryCode;
        this.email = finduSubscriberModel == undefined ? '' : finduSubscriberModel.email == undefined ? '' : finduSubscriberModel.email;
        this.title = finduSubscriberModel == undefined ? '' : finduSubscriberModel.title == undefined ? '' : finduSubscriberModel.title;
        this.password = finduSubscriberModel == undefined ? '' : finduSubscriberModel.password == undefined ? '' : finduSubscriberModel.password;
        this.firstName = finduSubscriberModel == undefined ? '' : finduSubscriberModel.firstName == undefined ? '' : finduSubscriberModel.firstName;
        this.lastName = finduSubscriberModel == undefined ? '' : finduSubscriberModel.lastName == undefined ? '' : finduSubscriberModel.lastName;
        this.dob = finduSubscriberModel == undefined ? '' : finduSubscriberModel.dob == undefined ? '' : finduSubscriberModel.dob;
        this.companyName = finduSubscriberModel == undefined ? '' : finduSubscriberModel.companyName == undefined ? '' : finduSubscriberModel.companyName;
        this.address1 = finduSubscriberModel == undefined ? '' : finduSubscriberModel.address1 == undefined ? '' : finduSubscriberModel.address1;
        this.address2 = finduSubscriberModel == undefined ? '' : finduSubscriberModel.address2 == undefined ? '' : finduSubscriberModel.address2;
        this.address3 = finduSubscriberModel == undefined ? '' : finduSubscriberModel.address3 == undefined ? '' : finduSubscriberModel.address3;
        this.cityId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.cityId == undefined ? '' : finduSubscriberModel.cityId;
        this.language = finduSubscriberModel == undefined ? '' : finduSubscriberModel.language == undefined ? '' : finduSubscriberModel.language;
        this.gender = finduSubscriberModel == undefined ? '' : finduSubscriberModel.gender == undefined ? '' : finduSubscriberModel.gender;
        this.height = finduSubscriberModel == undefined ? '' : finduSubscriberModel.height == undefined ? '' : finduSubscriberModel.height;
        this.build = finduSubscriberModel == undefined ? '' : finduSubscriberModel.build == undefined ? '' : finduSubscriberModel.build;
        this.race = finduSubscriberModel == undefined ? '' : finduSubscriberModel.race == undefined ? '' : finduSubscriberModel.race;
        this.nationality = finduSubscriberModel == undefined ? '' : finduSubscriberModel.nationality == undefined ? '' : finduSubscriberModel.nationality;
        this.vechicleRegistrationNo = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vechicleRegistrationNo == undefined ? '' : finduSubscriberModel.vechicleRegistrationNo;
        this.vechicleMake = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vechicleMake == undefined ? '' : finduSubscriberModel.vechicleMake;
        this.vechicleBodyType = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vechicleBodyType == undefined ? '' : finduSubscriberModel.vechicleBodyType;
        this.vechicleColour = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vechicleColour == undefined ? '' : finduSubscriberModel.vechicleColour;
        this.medicalInfo = finduSubscriberModel == undefined ? '' : finduSubscriberModel.medicalInfo == undefined ? '' : finduSubscriberModel.medicalInfo;
        this.medicalSymptoms = finduSubscriberModel == undefined ? '' : finduSubscriberModel.medicalSymptoms == undefined ? '' : finduSubscriberModel.medicalSymptoms;
        this.medicalDiseases = finduSubscriberModel == undefined ? '' : finduSubscriberModel.medicalDiseases == undefined ? '' : finduSubscriberModel.medicalDiseases;
        this.noOfUserCount = finduSubscriberModel == undefined ? '' : finduSubscriberModel.noOfUserCount == undefined ? '' : finduSubscriberModel.noOfUserCount;
        this.createdUserId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.createdUserId == undefined ? '' : finduSubscriberModel.createdUserId;
        this.finduAddressId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.finduAddressId == undefined ? '' : finduSubscriberModel.finduAddressId;
        this.finduUserId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.finduUserId == undefined ? '' : finduSubscriberModel.finduUserId;
        this.additionalUsers = finduSubscriberModel == undefined ? [] : finduSubscriberModel.additionalUsers == undefined ? [] : finduSubscriberModel.additionalUsers;
    }
    contractId: string;
    mobileNo: string;
    mobileNoCountryCode: string;
    email: string;
    title: string;
    password: string;
    firstName: string;
    lastName: string;
    dob: string;
    companyName: string;
    address1: string;
    address2: string;
    address3: string;
    cityId: string;
    language: string;
    gender: string;
    height: string;
    build: string;
    race: string;
    nationality: string;
    vechicleRegistrationNo: string;
    vechicleMake: string;
    vechicleBodyType: string;
    vechicleColour: string;
    medicalInfo: string;
    medicalSymptoms: string;
    medicalDiseases: string;
    noOfUserCount: string;
    createdUserId: string;
    finduAddressId: string;
    finduUserId: string;
    additionalUsers: DealerAdditionalUsers[];
}
class DealerAdditionalUsers {
    constructor(additionalUsers?: DealerAdditionalUsers) {
        this.finduUserId = additionalUsers == undefined ? "" : additionalUsers.finduUserId == undefined ? "" : additionalUsers.finduUserId;
        this.firstName = additionalUsers == undefined ? "" : additionalUsers.firstName == undefined ? "" : additionalUsers.firstName;
        this.lastName = additionalUsers == undefined ? "" : additionalUsers.lastName == undefined ? "" : additionalUsers.lastName;
        this.mobileNo = additionalUsers == undefined ? "" : additionalUsers.mobileNo == undefined ? "" : additionalUsers.mobileNo;
        this.mobileNoCountryCode = additionalUsers == undefined ? "+27" : additionalUsers.mobileNoCountryCode == undefined ? "+27" : additionalUsers.mobileNoCountryCode;
    }
    finduUserId: string;
    lastName: string;
    firstName: string;
    mobileNo: string;
    mobileNoCountryCode: string;
}
class DealerCustomerIdentificationModel {
    constructor(customerIdentificationModel?: DealerCustomerIdentificationModel) {
        this.SAID = customerIdentificationModel == undefined ? null : customerIdentificationModel.SAID == undefined ? null : customerIdentificationModel.SAID;
        this.companyRegNo = customerIdentificationModel == undefined ? null : customerIdentificationModel.companyRegNo == undefined ? null : customerIdentificationModel.companyRegNo;
        this.customerTypeId = customerIdentificationModel == undefined ? null : customerIdentificationModel.customerTypeId == undefined ? null : customerIdentificationModel.customerTypeId;
        this.passportNo = customerIdentificationModel == undefined ? null : customerIdentificationModel.passportNo == undefined ? null : customerIdentificationModel.passportNo;
        this.passportExpiryDate = customerIdentificationModel == undefined ? new Date() : customerIdentificationModel.passportExpiryDate == undefined ? new Date() : customerIdentificationModel.passportExpiryDate;
        this.isPassport = customerIdentificationModel == undefined ? false : customerIdentificationModel.isPassport == undefined ? false : customerIdentificationModel.isPassport;
        this.passportExpiryMonth = customerIdentificationModel == undefined ? null : customerIdentificationModel.passportExpiryMonth == undefined ? null : customerIdentificationModel.passportExpiryMonth;
        this.passportExpiryYear = customerIdentificationModel == undefined ? null : customerIdentificationModel.passportExpiryYear == undefined ? null : customerIdentificationModel.passportExpiryYear;
    }
    SAID?: string;
    companyRegNo?: string;
    customerTypeId?: string;
    isPassport?: boolean;
    passportNo?: string;
    passportExpiryMonth: string;
    passportExpiryYear: string;
    passportExpiryDate: Date;
}

export {
    DealerKeyHolderInfoAddEditModel, DealerPasswordsAddEditModel, DealerSiteHazardAddEditModel, DealerCellPanicAddEditModel,
    DealerDomesticWorkersAddEditModel, DealerOpenCloseMonitoringAddEditModel, DealerAccessToPremisesAddEditModel, DealerDocTypes,
    DealerAccountHolderDetailAddEditModel, DealerLockBoxesModel, DealerDigiPadModel, initialWorkingDays, DealerMedicalConditions,
    DealerKeyholderObjectModel, DealerEmergencyContactModel, DealerOpenCloseContact, DealerCustomerServiceAgreementParamsDataModel,
    DealerFinduSubscriberModel, DealerAdditionalUsers, DealerCustomerIdentificationModel
};
