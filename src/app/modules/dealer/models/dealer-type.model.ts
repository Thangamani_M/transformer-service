
class DealerTypeModel{
    delearTypeId:string;
    dealerTypeName: string;
    description: string;
    isActive:boolean;
    dealerTypeCodeList:DealerTypeListModel[];

    constructor(dealerTypeModel?: DealerTypeModel) {
        this.dealerTypeName = dealerTypeModel ? dealerTypeModel.dealerTypeName == undefined ? null : dealerTypeModel.dealerTypeName : null;
        this.description = dealerTypeModel ? dealerTypeModel.description == undefined ?  '' : dealerTypeModel.description : '';
        this.delearTypeId = dealerTypeModel ? dealerTypeModel.delearTypeId == undefined ?  '' : dealerTypeModel.delearTypeId : '';
        this.isActive = dealerTypeModel ? dealerTypeModel.isActive == undefined ?  false : dealerTypeModel.isActive : false;
    }

}

class DealerTypeListModel{
 
    delearTypeId:string;
    dealerTypeName: string;
    description: string;
    isActive:boolean;

    constructor(dealerTypeModel?: DealerTypeListModel) {
        this.dealerTypeName = dealerTypeModel ? dealerTypeModel.dealerTypeName == undefined ? null : dealerTypeModel.dealerTypeName : null;
        this.description = dealerTypeModel ? dealerTypeModel.description == undefined ?  '' : dealerTypeModel.description : '';
        this.delearTypeId = dealerTypeModel ? dealerTypeModel.delearTypeId == undefined ?  '' : dealerTypeModel.delearTypeId : '';
        this.isActive = dealerTypeModel ? dealerTypeModel.isActive == undefined ?  false : dealerTypeModel.isActive : false;
    }
}

export{DealerTypeModel} 