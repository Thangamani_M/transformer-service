class DealerDocumentDetailModel {
    dealerDocumentId: string;
    dealerDocumentConfigId: string;
    dealerId: string;
    docName: string;
    path: string;
    createdUserId: string;
    constructor(dealerDocumentDetailModel: DealerDocumentDetailModel) {
        this.dealerDocumentId =  dealerDocumentDetailModel?.dealerDocumentId ?  dealerDocumentDetailModel?.dealerDocumentId : '';
        this.dealerDocumentConfigId =  dealerDocumentDetailModel?.dealerDocumentConfigId ?  dealerDocumentDetailModel?.dealerDocumentConfigId : '';
        this.dealerId =  dealerDocumentDetailModel?.dealerId ?  dealerDocumentDetailModel?.dealerId : '';
        this.docName =  dealerDocumentDetailModel?.docName ?  dealerDocumentDetailModel?.docName : '';
        this.path =  dealerDocumentDetailModel?.path ?  dealerDocumentDetailModel?.path : '';
        this.createdUserId =  dealerDocumentDetailModel?.createdUserId ?  dealerDocumentDetailModel?.createdUserId : '';

    }
}

export { DealerDocumentDetailModel }
