class DealerClawbackModel {
  customerId: string;
  dealerBillingCommissionId: string;
  clawbackReasonId: string;
  dealerBranchId: string;
  dealerDebtorId: string;
  customerDebtorId: string;
  createdUserId: string;
  notes: string;
  customerDebtorName: string;
  customerName: string;
  dealerIIPCode: string;
  dealerName: string;
  dealerBranchCode: string;
  invoiceRefNo: string;
  totalAmount: string;
  customerRefNo: string;
  constructor(dealerClawbackModel?: DealerClawbackModel) {
      this.customerId = dealerClawbackModel?.customerId != undefined ? dealerClawbackModel?.customerId : '';
      this.dealerBillingCommissionId = dealerClawbackModel?.dealerBillingCommissionId != undefined ? dealerClawbackModel?.dealerBillingCommissionId : '';
      this.clawbackReasonId = dealerClawbackModel?.clawbackReasonId != undefined ? dealerClawbackModel?.clawbackReasonId : '';
      this.dealerBranchId = dealerClawbackModel?.dealerBranchId != undefined ? dealerClawbackModel?.dealerBranchId : '';
      this.dealerDebtorId = dealerClawbackModel?.dealerDebtorId != undefined ? dealerClawbackModel?.dealerDebtorId : '';
      this.customerDebtorId = dealerClawbackModel?.customerDebtorId != undefined ? dealerClawbackModel?.customerDebtorId : '';
      this.createdUserId = dealerClawbackModel?.createdUserId != undefined ? dealerClawbackModel?.createdUserId : '';
      this.notes = dealerClawbackModel?.notes != undefined ? dealerClawbackModel?.notes : '';
      this.customerDebtorName = dealerClawbackModel?.customerDebtorName != undefined ? dealerClawbackModel?.customerDebtorName : '';
      this.dealerIIPCode = dealerClawbackModel?.dealerIIPCode != undefined ? dealerClawbackModel?.dealerIIPCode : '';
      this.dealerName = dealerClawbackModel?.dealerName != undefined ? dealerClawbackModel?.dealerName : '';
      this.dealerBranchCode = dealerClawbackModel?.dealerBranchCode != undefined ? dealerClawbackModel?.dealerBranchCode : '';
      this.invoiceRefNo = dealerClawbackModel?.invoiceRefNo != undefined ? dealerClawbackModel?.invoiceRefNo : '';
      this.totalAmount = dealerClawbackModel?.totalAmount != undefined ? dealerClawbackModel?.totalAmount : '';
      this.customerRefNo = dealerClawbackModel?.customerRefNo != undefined ? dealerClawbackModel?.customerRefNo : '';
      this.customerName = dealerClawbackModel?.customerName != undefined ? dealerClawbackModel?.customerName : '';
  }
}

export { DealerClawbackModel };

