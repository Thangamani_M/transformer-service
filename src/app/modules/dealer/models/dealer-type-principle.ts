
class DealerPrincipleModel{
    principleName:string;
    principleSurname: string;
    principleIDNumber: string;
    principlePSIRARegNumber:string;
    principlePSIRAExpiryDate:string;
    email:string;
    contactNumber:string;
    creditVettingDocName:string;
    creditVettingDocPath:string;
    isActive:boolean;
    dealerPrincipleId:string;
    phoneNoCountryCode:string;
    fileUpload:string;
    dealerPrincipleCodeList:DealerPrincipleListModel[];
    constructor(dealerPrincipleModel?: DealerPrincipleModel) {
        this.fileUpload = dealerPrincipleModel ? dealerPrincipleModel.fileUpload == undefined ? null : dealerPrincipleModel.fileUpload : null;
        this.principleName = dealerPrincipleModel ? dealerPrincipleModel.principleName == undefined ? null : dealerPrincipleModel.principleName : null;
        this.principleSurname = dealerPrincipleModel ? dealerPrincipleModel.principleSurname == undefined ? null : dealerPrincipleModel.principleSurname : null;
        this.principleIDNumber = dealerPrincipleModel ? dealerPrincipleModel.principleIDNumber == undefined ? null : dealerPrincipleModel.principleIDNumber : null;
        this.principlePSIRARegNumber = dealerPrincipleModel ? dealerPrincipleModel.principlePSIRARegNumber == undefined ? null : dealerPrincipleModel.principlePSIRARegNumber : null;
        this.principlePSIRAExpiryDate = dealerPrincipleModel ? dealerPrincipleModel.principlePSIRAExpiryDate == undefined ? '' : dealerPrincipleModel.principlePSIRAExpiryDate : '';
        this.email = dealerPrincipleModel ? dealerPrincipleModel.email == undefined ? null : dealerPrincipleModel.email : null;
        this.contactNumber = dealerPrincipleModel ? dealerPrincipleModel.contactNumber == undefined ? null : dealerPrincipleModel.contactNumber : null;
        this.creditVettingDocName = dealerPrincipleModel ? dealerPrincipleModel.creditVettingDocName == undefined ? null : dealerPrincipleModel.creditVettingDocName : null;
        this.isActive = dealerPrincipleModel ? dealerPrincipleModel.isActive == undefined ? false : dealerPrincipleModel.isActive : false;
        this.creditVettingDocPath =  dealerPrincipleModel ? dealerPrincipleModel.creditVettingDocPath == undefined ? null : dealerPrincipleModel.creditVettingDocPath : null;
        this.dealerPrincipleId = dealerPrincipleModel ? dealerPrincipleModel.dealerPrincipleId == undefined ? null : dealerPrincipleModel.dealerPrincipleId : null;
        this.phoneNoCountryCode = dealerPrincipleModel?.phoneNoCountryCode ? dealerPrincipleModel?.phoneNoCountryCode : '+27';
    }

}

class DealerPrincipleListModel{
    principleName:string;
    principleSurname: string;
    principleIDNumber: string;
    principlePSIRARegNumber:string;
    principlePSIRAExpiryDate:string;
    email:string;
    contactNumber:string;
    creditVettingDocName:string;
    creditVettingDocPath:string;
    isActive:boolean;
    dealerPrincipleId:string;
    phoneNoCountryCode:string;

    constructor(dealerPrincipleModel?: DealerPrincipleModel) {
        this.principleName = dealerPrincipleModel ? dealerPrincipleModel.principleName == undefined ? null : dealerPrincipleModel.principleName : null;
        this.principleSurname = dealerPrincipleModel ? dealerPrincipleModel.principleSurname == undefined ? null : dealerPrincipleModel.principleSurname : null;
        this.principleIDNumber = dealerPrincipleModel ? dealerPrincipleModel.principleIDNumber == undefined ? null : dealerPrincipleModel.principleIDNumber : null;
        this.principlePSIRARegNumber = dealerPrincipleModel ? dealerPrincipleModel.principlePSIRARegNumber == undefined ? null : dealerPrincipleModel.principlePSIRARegNumber : null;
        this.principlePSIRAExpiryDate = dealerPrincipleModel ? dealerPrincipleModel.principlePSIRAExpiryDate == undefined ? '' : dealerPrincipleModel.principlePSIRAExpiryDate : '';
        this.email = dealerPrincipleModel ? dealerPrincipleModel.email == undefined ? null : dealerPrincipleModel.email : null;
        this.contactNumber = dealerPrincipleModel ? dealerPrincipleModel.contactNumber == undefined ? null : dealerPrincipleModel.contactNumber : null;
        this.creditVettingDocName = dealerPrincipleModel ? dealerPrincipleModel.creditVettingDocName == undefined ? null : dealerPrincipleModel.creditVettingDocName : null;
        this.isActive = dealerPrincipleModel ? dealerPrincipleModel.isActive == undefined ? false : dealerPrincipleModel.isActive : true;
        this.creditVettingDocPath =  dealerPrincipleModel ? dealerPrincipleModel.creditVettingDocPath == undefined ? null : dealerPrincipleModel.creditVettingDocPath : null;
        this.dealerPrincipleId = dealerPrincipleModel ? dealerPrincipleModel.dealerPrincipleId == undefined ? null : dealerPrincipleModel.dealerPrincipleId : null;
        this.phoneNoCountryCode = dealerPrincipleModel?.phoneNoCountryCode ? dealerPrincipleModel?.phoneNoCountryCode : '+27';
    }

}
export { DealerPrincipleModel };

