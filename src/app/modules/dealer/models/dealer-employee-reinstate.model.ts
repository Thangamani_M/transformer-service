
class DealerEmployeeReinstateModel{
    reinstatementDate:string;
    reinstatementNotes:string;
    isReinstatementImmediateEffect: boolean;

    constructor(dealerReinstateModel?: DealerEmployeeReinstateModel) {
        this.reinstatementDate = dealerReinstateModel ? dealerReinstateModel.reinstatementDate == undefined ? null : dealerReinstateModel.reinstatementDate : null;
        this.reinstatementNotes = dealerReinstateModel ? dealerReinstateModel.reinstatementNotes == undefined ? null : dealerReinstateModel.reinstatementNotes : null;
        this.isReinstatementImmediateEffect = dealerReinstateModel ? dealerReinstateModel.isReinstatementImmediateEffect == undefined ? false : dealerReinstateModel.isReinstatementImmediateEffect : false;

    }

}


export { DealerEmployeeReinstateModel };

