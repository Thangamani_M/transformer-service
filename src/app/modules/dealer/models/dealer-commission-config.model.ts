class DealerCommissionFilterModel {
    dealerCategoryTypeId?: string;
    description?: string;
    constructor(dealerCommissionFilterModel: DealerCommissionFilterModel) {
        this.dealerCategoryTypeId = dealerCommissionFilterModel?.dealerCategoryTypeId != undefined ? dealerCommissionFilterModel?.dealerCategoryTypeId : '';
        this.description = dealerCommissionFilterModel?.description != undefined ? dealerCommissionFilterModel?.description : '';
    }
}
class DealerCommissionDetailModel {
    commissionConfigId: string;
    dealerTypeId: string;
    dealerCategoryTypeId: string;
    description: string;
    commission: string;
    isActive: boolean;
    constructor(dealerCommissionDetailModel: DealerCommissionDetailModel) {
        this.commissionConfigId = dealerCommissionDetailModel?.commissionConfigId != undefined ? dealerCommissionDetailModel?.commissionConfigId : '';
        this.dealerTypeId = dealerCommissionDetailModel?.dealerTypeId != undefined ? dealerCommissionDetailModel?.dealerTypeId : '';
        this.dealerCategoryTypeId = dealerCommissionDetailModel?.dealerCategoryTypeId != undefined ? dealerCommissionDetailModel?.dealerCategoryTypeId : '';
        this.description = dealerCommissionDetailModel?.description != undefined ? dealerCommissionDetailModel?.description : '';
        this.commission = dealerCommissionDetailModel?.commission != undefined ? dealerCommissionDetailModel?.commission : '';
        this.isActive = dealerCommissionDetailModel?.isActive != undefined ? dealerCommissionDetailModel?.isActive : true;
    }
}
export {DealerCommissionFilterModel, DealerCommissionDetailModel};