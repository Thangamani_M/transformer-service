class DealerBranchCategoryDetailModel {
    dealerBranchCategoryId: string;
    dealerBranchId: string;
    dealerCategoryTypeId: string;
    commissionPercentage: string;
    commissionStartDate: string;
    bonusMultipleStartDate: string;
    bonusMultiple: string;
    dsfMultiple: string;
    sapVendorNumber: string;
    createdUserId: string;
    constructor(dealerBranchCategoryDetailModel: DealerBranchCategoryDetailModel) {
        this.dealerBranchCategoryId = dealerBranchCategoryDetailModel?.dealerBranchCategoryId != undefined ? dealerBranchCategoryDetailModel?.dealerBranchCategoryId : '';
        this.dealerBranchId = dealerBranchCategoryDetailModel?.dealerBranchId != undefined ? dealerBranchCategoryDetailModel?.dealerBranchId : '';
        this.dealerCategoryTypeId = dealerBranchCategoryDetailModel?.dealerCategoryTypeId != undefined ? dealerBranchCategoryDetailModel?.dealerCategoryTypeId : '';
        this.commissionPercentage = dealerBranchCategoryDetailModel?.commissionPercentage != undefined ? dealerBranchCategoryDetailModel?.commissionPercentage : '';
        this.commissionStartDate = dealerBranchCategoryDetailModel?.commissionStartDate != undefined ? dealerBranchCategoryDetailModel?.commissionStartDate : '';
        this.bonusMultipleStartDate = dealerBranchCategoryDetailModel?.bonusMultipleStartDate != undefined ? dealerBranchCategoryDetailModel?.bonusMultipleStartDate : '';
        this.bonusMultiple = dealerBranchCategoryDetailModel?.bonusMultiple != undefined ? dealerBranchCategoryDetailModel?.bonusMultiple : '';
        this.dsfMultiple = dealerBranchCategoryDetailModel?.dsfMultiple != undefined ? dealerBranchCategoryDetailModel?.dsfMultiple : '';
        this.sapVendorNumber = dealerBranchCategoryDetailModel?.sapVendorNumber != undefined ? dealerBranchCategoryDetailModel?.sapVendorNumber : '';
        this.createdUserId = dealerBranchCategoryDetailModel?.createdUserId != undefined ? dealerBranchCategoryDetailModel?.createdUserId : '';
    }
}

export { DealerBranchCategoryDetailModel }