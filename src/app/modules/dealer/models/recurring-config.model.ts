class recurringconfigAddEditModel{
    constructor(recurringConfigAddModel?:recurringconfigAddEditModel){
        this.dealerIIPCode=recurringConfigAddModel?recurringConfigAddModel.dealerIIPCode==undefined?'':recurringConfigAddModel.dealerIIPCode:'';
        this.recurringMinMonth=recurringConfigAddModel?recurringConfigAddModel.recurringMinMonth==undefined?'':recurringConfigAddModel.recurringMinMonth:'';
        this.recurringPercentage=recurringConfigAddModel?recurringConfigAddModel.recurringPercentage==undefined?'':recurringConfigAddModel.recurringPercentage:'';
        this.minContract=recurringConfigAddModel?recurringConfigAddModel.minContract==undefined?'':recurringConfigAddModel.minContract:'';
        this.dealerCategoryTypeName=recurringConfigAddModel?recurringConfigAddModel.dealerCategoryTypeName==undefined?'':recurringConfigAddModel.dealerCategoryTypeName:''; 
        //this.dealerRecurringRevenueId=recurringConfigAddModel?recurringConfigAddModel.dealerRecurringRevenueId==undefined?'':recurringConfigAddModel.dealerRecurringRevenueId:'';
        

    }
    dealerIIPCode?: string ;
    recurringMinMonth?: string ;
    recurringPercentage?:string;
    minContract?: string ;
    dealerCategoryTypeName?:string;
   // dealerRecurringRevenueId?: string;
   
   
}
export {recurringconfigAddEditModel}