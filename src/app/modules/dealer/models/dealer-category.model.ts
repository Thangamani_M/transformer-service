
class DealerCategoryModel{
    dealerCategoryTypeId:string;
    dealerCategoryTypeName: string;
    description: string;
    isActive:boolean;
    dealerCategoryCodeList:DealerCategoryListModel[];

    constructor(dealerCategoryModel?: DealerCategoryModel) {
        this.dealerCategoryTypeName = dealerCategoryModel ? dealerCategoryModel.dealerCategoryTypeName == undefined ? null : dealerCategoryModel.dealerCategoryTypeName : null;
        this.description = dealerCategoryModel ? dealerCategoryModel.description == undefined ?  '' : dealerCategoryModel.description : '';
        this.dealerCategoryTypeId = dealerCategoryModel ? dealerCategoryModel.dealerCategoryTypeId == undefined ?  '' : dealerCategoryModel.dealerCategoryTypeId : '';
        this.isActive = dealerCategoryModel ? dealerCategoryModel.isActive == undefined ?  false : dealerCategoryModel.isActive : false;
    }

}

class DealerCategoryListModel{
 
    dealerCategoryTypeId:string;
    dealerCategoryTypeName: string;
    description: string;
    isActive:boolean;

    constructor(dealerCategoryModel?: DealerCategoryListModel) {
        this.dealerCategoryTypeName = dealerCategoryModel ? dealerCategoryModel.dealerCategoryTypeName == undefined ? null : dealerCategoryModel.dealerCategoryTypeName : null;
        this.description = dealerCategoryModel ? dealerCategoryModel.description == undefined ?  '' : dealerCategoryModel.description : '';
        this.dealerCategoryTypeId = dealerCategoryModel ? dealerCategoryModel.dealerCategoryTypeId == undefined ?  '' : dealerCategoryModel.dealerCategoryTypeId : '';
        this.isActive = dealerCategoryModel ? dealerCategoryModel.isActive == undefined ?  false : dealerCategoryModel.isActive : false;
    }
}

export { DealerCategoryModel };
