import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'market-type' },
    { path: 'market-type-config', loadChildren: () => import('./components/market-type-config/market-type-config.module').then(m => m.MarketTypeConfigModule) },
    { path: 'dealer-type-config', loadChildren: () => import('./components/dealer-type-config/dealer-type-config.module').then(m => m.DealerTypeConfigModule) },
    { path: 'dealer-type', loadChildren: () => import('./components/dealer-type/dealer-type.module').then(m => m.DealerTypeModule) },
    { path: 'dealer-category', loadChildren: () => import('./components/dealer-category/dealer-category.module').then(m => m.DealerCategoryModule) },
    { path: 'dealer-contract', loadChildren: () => import('./components/dealer-contract/dealer-contract.module').then(m => m.DealerContractModule) },
    { path: 'customer-app-banner-config', loadChildren: () => import('./components/cust-app-banner-config/cust-app-banner-config.module').then(m => m.CustAppBannerConfigModule) },
    { path: 'dealer-commission-config', loadChildren: () => import('./components/dealer-commision-config/dealer-commission-config.module').then(m => m.DealerCommissionConfigModule) },
    { path: 'dealer-maintenance', loadChildren: () => import('./components/dealer-maintenance/dealer-maintenance.module').then(m => m.DealerMaintenanceModule) },
    { path: 'dealer-document-type', loadChildren: () => import('./components/dealer-doc-type/dealer-doc-type.module').then(m => m.DealerDocumentTypeModule) },
    { path: 'dealer-req-doc-config', loadChildren: () => import('./components/dealer-req-doc-config/dealer-req-doc-config.module').then(m => m.DealerReqDocConfigModule) },
    { path: 'dsf-config', loadChildren: () => import('./components/dfs-config/dfs-config.module').then(m => m.DFSConfigTypeModule) },
    { path: 'dealer-stock-selling-price', loadChildren: () => import('./components/dealer-stock-selling-price/dealer-stock-selling-price.module').then(m => m.DealerStockSellingPriceModule) },
    { path: 'dealer-stock-order', loadChildren: () => import('./components/dealer-stock-order/dealer-stock-order.module').then(m => m.DealerStockOrderModule) },
    { path: 'dealer-stock-order-cancellation', loadChildren: () => import('./components/dealer-stock-order-cancellation/dealer-stock-order-cancellation.module').then(m => m.DealerStockOrderCancellationModule) },
    { path: 'average-contract-config', loadChildren:() => import('./components/average-contract-config/average-contract.module').then(m => m.AverageContractModule) },
    { path: 'employee-management', loadChildren: () => import('./components/employee-management/employee-management.module').then(m => m.EmployeeManagementModule)},
    { path: 'dealer-worklist', loadChildren: () => import('./components/dealer-worklist/dealer-worklist.module').then(m => m.DealerWorklistModule)},
    { path: 'dealer-billing-configuration', loadChildren: () => import('./components/dealer-billing-configuration/dealer-billing-configuration.module').then(m => m.DealerBillingConfigurationModule)},
    { path: 'dealer-billing-commission', loadChildren: () => import('./components/dealer-billing-commissions/dealer-billing-commissions.module').then(m => m.DealerBillingCommissionsModule)},
    { path: 'clawback', loadChildren: () => import('./components/clawback/clawback.module').then(m => m.ClawbackModule)},
    { path: 'clawback-reversal', loadChildren: () => import('./components/clawback-reversal/clawback-reversal.module').then(m => m.ClawbackReversalModule)},
    { path: 'recurring-config', loadChildren: () => import('./components/recurring-config/recurring-config.module').then(m => m.RecurringTypeConfigModule)},

];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class DealerRoutingModule { }
