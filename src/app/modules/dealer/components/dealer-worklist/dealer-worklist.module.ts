import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallEscalationModule } from '@modules/customer/components/customer/technician-installation/call-escalation-dialog/call-escalation-dialog.module';
import { DealerWorklistComponent } from '@modules/dealer';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations: [DealerWorklistComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        CallEscalationModule,
        RouterModule.forChild([
            { path: '', component: DealerWorklistComponent,canActivate:[AuthGuard], data: { title: 'Dealer Worklist' } },
        ])
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [],
    exports: [],
})
export class DealerWorklistModule { }
