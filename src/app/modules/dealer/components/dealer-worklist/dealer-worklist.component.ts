import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CallEscalationDialogComponent } from '@modules/customer/components/customer/technician-installation/call-escalation-dialog';
import { DealerTaskRequestType } from '@modules/dealer';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { MY_TASK_COMPONENTS } from '@modules/my-tasks/shared/utils/task-list.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-dealer-worklist',
  templateUrl: './dealer-worklist.component.html'
})
export class DealerWorklistComponent extends PrimeNgTableVariablesModel implements OnInit {

  dateFormat = 'MMM dd, yyyy';
  primengTableConfigProperties: any;
  row: any = {};
  listSubscription: any;
  first: any = 0;
  filterData: any;
  permission = []
  constructor(
    private crudService: CrudService,
    private router: Router,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private datePipe: DatePipe, private dialogService: DialogService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Task List",
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Dealer Task List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Task List',
            dataKey: 'referenceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            // enableSecondHyperLinkwithValid: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'requestNumber', header: 'Request Number', width: '150px' },
              { field: 'reference', header: 'Reference', width: '100px' },
              { field: 'dealerIIPCode', header: 'Dealer Code', width: '120px', isLink: true },
              { field: 'companyName', header: 'Dealer Name', width: '150px' },
              { field: 'dealerTypeName', header: 'Dealer Type', width: '150px' },
              { field: 'customerRefNo', header: 'Customer ID', width: '120px' },
              { field: 'debtorsCode', header: 'Debtors Code', width: '150px' },
              { field: 'custDesc', header: 'Customer Desc', width: '150px' },
              { field: 'requestTypeName', header: 'Request Type', width: '120px' },
              { field: 'requestMotivationName', header: 'Request Motivation', width: '200px' },
              { field: 'value', header: 'Value', width: '80px' },
              { field: 'notes', header: 'Reason', width: '120px' },
              { field: 'dueDate', header: 'Due Date', width: '160px' },
              { field: 'createdDate', header: 'Creation Date', width: '160px' },
              { field: 'createdBy', header: 'Created By', width: '130px' },
              { field: 'creatorRole', header: 'Creators Role', width: '130px' },
              { field: 'divisionName', header: 'Division', width: '100px' },
              { field: 'districtName', header: 'District', width: '100px' },
              { field: 'branchName', header: 'Branch', width: '100px' },
              { field: 'subAraName', header: 'Sub Area', width: '100px' },
              { field: 'techAraName', header: 'Tech Area', width: '100px' },
              { field: 'salesAraName', header: 'Sales Area', width: '100px' },
              { field: 'fullAddress', header: 'Address', width: '150px' },
              { field: 'currentApprovalLevel', header: 'Current Approval Level', width: '200px' },
              { field: 'lastApprovedBY', header: 'Last Approval Done By', width: '200px' },
              { field: 'lastApprovedDate', header: 'Last Approval Date/Time', width: '200px' },
              { field: 'isEscalated', header: 'Esculated', width: '100px' },
              { field: 'actionDate', header: 'Actioned Date', width: '160px' },
              { field: 'status', header: 'Status', width: '80px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_TASK_LIST,
            moduleName: ModulesBasedApiSuffix.DEALER,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      this.permission = response[1][MY_TASK_COMPONENTS.DEALER]
      if (this.permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col)
    }
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.listSubscription = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.dueDate = val?.dueDate ? this.datePipe.transform(val?.dueDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          val.createdDate = val?.createdDate ? this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          val.lastApprovedDate = val?.lastApprovedDate ? this.datePipe.transform(val?.lastApprovedDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          val.actionDate = val?.actionDate ? this.datePipe.transform(val?.actionDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          val.requestId = val?.taskListId ? val?.taskListId : '';
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar, IsAll: true, userId: this.loggedInUserData?.userId };
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row, unknownVar);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;

      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canApprove) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        if (editableObject['requestTypeName'] == DealerTaskRequestType.DEALER_APPROVAL) {
          this.onOpenDealerApproval(editableObject);
        } else if (editableObject['requestTypeName'] == DealerTaskRequestType.DEALER_INSPECTION_FORM) {
          this.openInspectionFormApprovalPage(editableObject);
        } else if (editableObject['requestTypeName'] == DealerTaskRequestType.DEALER_CALL_ESCALATION) {
          this.openCallEscalationDialog(editableObject);
        }
        break;
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        if (editableObject['requestTypeName'] == DealerTaskRequestType.DEALER_APPROVAL && unknownVar == 'dealerIIPCode') {
          window.open(`/dealer/dealer-maintenance/info/view?id=${editableObject?.dealerId}&dealerTypeId=${editableObject?.dealerTypeId}`);
          // this.router.navigate(['/dealer/dealer-maintenance/info/view'], { queryParams: { tab: 0, id: editableObject?.dealerId, dealerTypeId: editableObject?.dealerTypeId, }, skipLocationChange: false })
        } else if ((editableObject['requestTypeName'] == DealerTaskRequestType.DEALER_CALL_ESCALATION || editableObject['requestTypeName'] == DealerTaskRequestType.DEALER_INSPECTION_FORM) && unknownVar == 'reference') {
          window.open(`${window.location.origin}/customer/manage-customers/inspection-call?customerInspectionId=${editableObject['callInitiationId']}&customerId=${editableObject['customerId']}&customerAddressId=${editableObject.addressId}&requestId=${editableObject.requestId}&isNew='taskmanagerlist'&isDealer=true`);
        }
        break;
    }
  }

  onOpenDealerApproval(row) {
    if (row['status']?.toLowerCase() == 'pending' || row['status']?.toLowerCase() == 'resubmit') {
      this.router.navigate(['/dealer/dealer-maintenance'], { queryParams: { tab: 4, id: row?.dealerId, dealerTypeId: row?.dealerTypeId, referenceId: row?.referenceId, }, skipLocationChange: false })
    } else if (row['status']?.toLowerCase() == 'declined') {
      this.snackbarService.openSnackbar(`This request needs to be resubmit the previous level`, ResponseMessageTypes.WARNING);
    } else {
      this.snackbarService.openSnackbar(`This request is already ${row['status']?.toLowerCase()}. So please try any other requests`, ResponseMessageTypes.WARNING);
    }
  }

  openInspectionFormApprovalPage(row) {
    this.router.navigate(['/technical-management', 'inspection-call', 'inspection-form-add-edit'], {
      queryParams: {
        CustomerInspectionId: row['referenceId'],
        CustomerId: row['customerId'],
        AddressId: row['addressId'],
        requestId: row['taskListId'],
        isDealer: true,
        isNew: 'taskmanagerlist'
      }
    });
  }

  /* --- Open Call Escalation Dialog ---- */
  openCallEscalationDialog(row?) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.QUICK_CALL_ESCALATION, null, false, prepareRequiredHttpParams({ CallInitiationEscalationApprovalId: row['referenceId'] })).subscribe((res: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.isSuccess == true && res?.statusCode == 200 && res?.resources) {
          const ref = this.dialogService.open(CallEscalationDialogComponent, {
            header: 'Service Call Escalation',
            baseZIndex: 1000,
            width: '400px',
            // closable: false,
            showHeader: false,
            data: {
              header: 'Dealer Call Escalation',
              ...res?.resources,
              createdUserId: this.loggedInUserData?.userId,
              isServiceCallEscalation: true,
              row: row,
            },
          });
          ref.onClose.subscribe((res) => {
            if (res) {
              this.onCRUDRequested('get');
            }
          });
        }
      });
  }
  /* --- Close Call Escalation Dialog ---- */


  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.permission['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }
}
