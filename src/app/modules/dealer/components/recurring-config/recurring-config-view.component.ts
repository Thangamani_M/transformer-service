import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';

@Component({
  selector: 'app-recurring-config-view',
  templateUrl: './recurring-config-view.component.html',
})
export class RecurringConfigViewComponent implements OnInit {
    DealerRecurringRevenueId:any;
    DealerRecurringRevenueData:any;
  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
        this.DealerRecurringRevenueId = this.activatedRoute.snapshot.queryParams.id;
     }

  ngOnInit(): void {
      this.GetDetailByDealerRecurringRevenueId();
  }

  GetDetailByDealerRecurringRevenueId(): void {
    this.crudService.get( ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_RECURRING_REVENUE, this.DealerRecurringRevenueId, true).subscribe((res) => {
      this.DealerRecurringRevenueData = res.resources
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  
  navigateToEdit() {
    this.router.navigate(['/dealer/recurring-config/add-edit'], { queryParams: { dealerrecurringrevenueid: this.DealerRecurringRevenueId } });
  }

}
