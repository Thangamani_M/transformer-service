import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { recurringconfigAddEditModel } from '@modules/dealer/models/recurring-config.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-recurring-config-add-edit',
  templateUrl: './recurring-config-add-edit.component.html',
})

export class RecurringConfigAddEditComponent implements OnInit {
    DealerRecurringrevenueid:any;
    userData: UserLogin;
    recurringconfigForm:FormGroup;
    isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    isAStringOnlyNoSpace = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  constructor(private router: Router, private dialog: MatDialog, private snackbarService: SnackbarService, private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
      this.DealerRecurringrevenueid = this.activatedRoute.snapshot.queryParams.dealerrecurringrevenueid;
      if (this.DealerRecurringrevenueid) {
        this.GetDetailByRecurringId(this.DealerRecurringrevenueid);
      }
   }

  ngOnInit(): void {
      this.createRecurringForm();
  }
  GetDetailByRecurringId(DealerRecurringrevenueid: string): void {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_RECURRING_REVENUE, DealerRecurringrevenueid, true).subscribe((res) => {
      let Recurring = new recurringconfigAddEditModel(res.resources);
      this.recurringconfigForm.patchValue(Recurring);
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }

  createRecurringForm(): void {
    let Recurring = new recurringconfigAddEditModel();
    this.recurringconfigForm = this.formBuilder.group({});

    Object.keys(Recurring).forEach((key) => {
      this.recurringconfigForm.addControl(key, new FormControl(Recurring[key]));
    });
    this.recurringconfigForm = setRequiredValidator(this.recurringconfigForm, ["dealerIIPCode", "recurringMinMonth", "recurringPercentage", "minContract","dealerCategoryTypeName"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onSubmit():void{
    //   if(this.recurringconfigForm.invalid){
    //       return;
    //   }
    let FormValue = this.recurringconfigForm.value
    let FormValues = this.recurringconfigForm.value
    if(!this.DealerRecurringrevenueid){

        FormValue.modifiedDate = new Date();
        FormValue.modifiedUserId = this.userData.userId;
       // FormValue.dealerRecurringRevenueId = this.DealerRecurringrevenueid;
    }else if(this.DealerRecurringrevenueid){
        FormValues.modifiedDate = new Date();
        FormValues.modifiedUserId = this.userData.userId
        FormValues.dealerRecurringRevenueId = this.DealerRecurringrevenueid;

    }


      let crudService: Observable<IApplicationResponse> = !this.DealerRecurringrevenueid ? this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_RECURRING_REVENUE, FormValue) :
      this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_RECURRING_REVENUE, FormValues)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/dealer/recurring-config']);
      }
    })

  }
}
