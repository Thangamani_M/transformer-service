import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {CrudService, CrudType,LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,RxjsService,} from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { loggedInUserData } from '@modules/others';
import {  Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-recurring-config-list',
  templateUrl: './recurring-config-list.component.html'
})

export class RecurringConfigListComponent  extends PrimeNgTableVariablesModel implements OnInit {
  row: any = {}
  constructor(
    private commonService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>
  ) {
    super();
  }
  primengTableConfigProperties: any = {
    tableCaption: "Recurring Revenue",
    breadCrumbItems:
      [{ displayName: 'Dealer', relativeRouterUrl: '' },{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Recurring Revenue', relativeRouterUrl: '' },],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Recurring Revenue',
          dataKey: 'dealerRecurringRevenueId',
          enableBreadCrumb: true,
          enableAddActionBtn: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          ebableAddActionBtn: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enbableAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'dealerIIPCode', header: 'Dealer ID' },
          { field: 'recurringMinMonth', header: 'Recurring Min Month' },
          { field: 'recurringPercentage', header: 'Recurring Percentage' },
          { field: 'minContract', header: 'Min Contract' },
          { field: 'dealerCategoryTypeName', header: 'Category' },
        ],
          shouldShowCreateActionBtn: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: DealerModuleApiSuffixModels.DEALER_RECURRING_REVENUE,
          moduleName: ModulesBasedApiSuffix.DEALER
        }
      ]

    }
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.commonService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_RECURRING_REVENUE,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col)
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;

      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["dealer/recurring-config/view"], { queryParams: { id: editableObject['dealerRecurringRevenueId'] } });
            break;
        }
    }
  }
}
