import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecurringConfigAddEditComponent } from './recurring-config-add-edit.component';
import { RecurringConfigListComponent } from './recurring-config-list.component';
import { RecurringConfigViewComponent } from './recurring-config-view.component';
const routes: Routes = [
    { path: '', component: RecurringConfigListComponent, data: { title: 'Recurring Config' } },
     { path: 'view', component: RecurringConfigViewComponent, data: { title: 'Recurring Config View' } },
     { path: 'add-edit', component: RecurringConfigAddEditComponent, data: { title: 'Recurring Config Add/Edit' } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class RecurringConfigRoutingModule { }
