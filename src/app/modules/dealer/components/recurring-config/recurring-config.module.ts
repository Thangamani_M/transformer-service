import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { RecurringConfigAddEditComponent } from './recurring-config-add-edit.component';
import { RecurringConfigListComponent } from './recurring-config-list.component';
import { RecurringConfigRoutingModule } from './recurring-config-routing.module';
import { RecurringConfigViewComponent } from './recurring-config-view.component';

@NgModule({
    declarations: [RecurringConfigListComponent,RecurringConfigViewComponent,RecurringConfigAddEditComponent],
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RecurringConfigRoutingModule,
    ],
    entryComponents:[],
    providers: [
        DatePipe
    ]
})
export class RecurringTypeConfigModule { }
