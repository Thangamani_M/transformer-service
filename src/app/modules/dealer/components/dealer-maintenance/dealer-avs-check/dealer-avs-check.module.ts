import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerAVSCheckComponent } from '@modules/dealer';

@NgModule({
    declarations: [DealerAVSCheckComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerAVSCheckComponent],
    exports: [DealerAVSCheckComponent],
})
export class DealerAVSCheckModule { }
