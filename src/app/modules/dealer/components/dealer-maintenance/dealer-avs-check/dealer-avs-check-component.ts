import { Component, OnInit } from "@angular/core";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from "@app/shared";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";


@Component({
  selector: 'app-dealer-avs-check',
  templateUrl: './dealer-avs-check-component.html'
})
export class DealerAVSCheckComponent implements OnInit {

  loggedUser: any;
  tableRec = "<table class='fidelity-table table-bordered table-responsive'><thead><th colspan='2'>AVS Account Verification</th></thead><tbody><tr><td>Verified</td><td>-</td></tr><tr><td>Verified Dated</td><td>-</td></tr><tr><td>Branch Code</td><td>-</td></tr><tr><td>Account Found</td><td>-</td></tr><tr><td>ID Match</td><td>-</td></tr><tr><td>Surname Match</td><td>-</td></tr><tr><td>Account Open</td><td>-</td></tr><tr><td>Account Dormant</td><td>-</td></tr><tr><td>AccountOpen 3Months</td><td>-</td></tr><tr><td>Account Accepts Debits</td><td>-</td></tr><tr><td>Account Accepts Credits</td><td>-</td></tr><tr><td>Error Reason</td><td>Response Received: Failure, ErrorCode: R0109-,NO MATCH FOUND ON OUR DATABASE The id, name/surname combination does not match our home affairs verified data   </td></tr><tr><td></td><td></td></tr><tr><td><b>Legends</b></td><td></td></tr><tr><td><b>Y - Yes</b></td><td>The bank confirms that the field value is true</td></tr><tr><td><b>N - No</b></td><td>The bank confirms that the field value is false</td></tr><tr><td><b>U - Unprocessed</b></td><td>The bank did not process this field</td></tr><tr><td><b>F - Failed</b></td><td>Failed pre-validation</td></tr></tbody></table>";
  avscheckDetails;
  dealerBranchId: string;
  fullName: string;

  constructor(private store: Store<AppState>, private crudService: CrudService,
    public config: DynamicDialogConfig, public ref: DynamicDialogRef,
    private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.dealerBranchId = this.config.data.row.dealerBranchId;
    this.getCrdeitVettingAVSCheck();
  }

  getCrdeitVettingAVSCheck() {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.CREDIT_VETTING_AVS_CHECK, this.dealerBranchId, false,
      null)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200) {
          this.avscheckDetails = response.resources;
          this.fullName = this.avscheckDetails ? this.avscheckDetails?.firstName + ' ' + this.avscheckDetails?.lastName : '';
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      });
  }
  btnCloseClick() {
    this.ref.close(false);
  }
}