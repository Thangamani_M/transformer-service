import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerPrincipleAddEditComponent } from '@modules/dealer';

@NgModule({
    declarations: [DealerPrincipleAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerPrincipleAddEditComponent],
    exports: [DealerPrincipleAddEditComponent],
})
export class DealerPrincipleAddEditModule { }
