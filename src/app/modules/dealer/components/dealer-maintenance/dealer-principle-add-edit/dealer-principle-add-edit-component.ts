import { DatePipe } from '@angular/common';
import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, ModulesBasedApiSuffix, RxjsService, saidPattern, setEmailValidator, setRequiredValidator, setSAIDValidator } from "@app/shared";
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerPrincipleModel } from "@modules/dealer/models/dealer-type-principle";
import { loggedInUserData } from '@modules/others';
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
@Component({
  selector: 'app-dealer-principle-add-edit',
  templateUrl: './dealer-principle-add-edit-component.html',
  styleUrls: ['./dealer-principle-add-edit-component.scss']
})

export class DealerPrincipleAddEditComponent implements OnInit {
  dealerPrincipleDialogForm: FormGroup;
  showFailedImport: boolean = false;
  fileName = '';
  isFileSelected: boolean = false;
  isSubmitted: boolean;
  loggedUser: any;
  formConfigs = formConfigs;
  startTodayDate = new Date();
  selectedFiles = new Array();
  totalFileSize = 0;
  creditVettingDocNamees: string;
  selectedTabIndex = 1;
  dealerId: any;
  statusList = [{ displayName: 'Active', value: true }, { displayName: 'In-Active', value: false }]
  formData = new FormData();
  value: any;
  viewDate: any;
  countryCodes = countryCodes;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  fileName11: any;
  file_Name: any;
  constructor( private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private store: Store<AppState>,
    private crudService: CrudService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, private datePipe: DatePipe, private rxjsService: RxjsService,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.dealerId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
  }
  ngOnInit(): void {
    this.createForm();
  }
  createForm(dealerPrincipleFormModel?: DealerPrincipleModel) {
    let dealerPrincipleModel = new DealerPrincipleModel(dealerPrincipleFormModel);
    this.dealerPrincipleDialogForm = this.formBuilder.group({});
    Object.keys(dealerPrincipleModel).forEach((key) => {
      if (dealerPrincipleModel[key] === 'dealerPrincipleCodeList') {
        this.dealerPrincipleDialogForm.addControl(key, new FormArray(dealerPrincipleModel[key]));
      } else {
        if(key == 'principlePSIRAExpiryDate'){
          this.dealerPrincipleDialogForm.addControl(key, new FormControl(this.config?.data?.row ? new Date(this.config?.data?.row[key]) : dealerPrincipleModel[key]));
         }else {
          this.dealerPrincipleDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : dealerPrincipleModel[key]));
         }
      }
    });
    if(this.dealerId)
       this.dealerPrincipleDialogForm.get('isActive').setValue(true);

    this.dealerPrincipleDialogForm.get('phoneNoCountryCode').disable();
    this.dealerPrincipleDialogForm.get('phoneNoCountryCode').setValue('+27');
    this.dealerPrincipleDialogForm = setRequiredValidator(this.dealerPrincipleDialogForm, ['principleName', 'principleSurname', 'principleIDNumber','principlePSIRAExpiryDate', 'principlePSIRARegNumber', 'email', 'contactNumber']);
    // this.dealerPrincipleDialogForm.get('principleIDNumber').setValidators([Validators.required, saidPattern]);
    // this.dealerPrincipleDialogForm.get('principleIDNumber').updateValueAndValidity();
    this.dealerPrincipleDialogForm = setSAIDValidator(this.dealerPrincipleDialogForm, ['principleIDNumber']);
    this.dealerPrincipleDialogForm = setEmailValidator(this.dealerPrincipleDialogForm, ["email"]);
     this.fileName = (this.config && this.config?.data && this.config?.data?.row && this.config?.data?.row?.creditVettingDocName) ?  this.config.data.row.creditVettingDocName : null;
     this.onValueChanges();
     if (this.config?.data?.isShowAction) {
      this.dealerPrincipleDialogForm.disable();
     }
    }
  onValueChanges() {
    this.dealerPrincipleDialogForm
      .get("phoneNoCountryCode")
      .valueChanges.subscribe((phoneNoCountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(phoneNoCountryCode, "contact");
      });
    this.dealerPrincipleDialogForm
      .get("contactNumber")
      .valueChanges.subscribe((phoneNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dealerPrincipleDialogForm.get("phoneNoCountryCode").value,
          "contact"
        );
      });
  }
  setPhoneNumberLengthByCountryCode(countryCode: string, str: string) {
    switch (countryCode) {
      case "+27":
        if (str == "contact") {
          this.dealerPrincipleDialogForm
            .get("contactNumber")
            .setValidators([
              Validators.minLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
              Validators.maxLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
            ]);
        }

        break;
      default:
        if (str == "contact") {
          this.dealerPrincipleDialogForm
            .get("contactNumber")
            .setValidators([
              Validators.minLength(formConfigs.indianContactNumberMaxLength),
              Validators.maxLength(formConfigs.indianContactNumberMaxLength),
            ]);
        }
        break;
    }
  }
  
  emitUploadedFiles(uploadedFile) {
    if (!uploadedFile || uploadedFile?.length == 0) {
      return;
    }
    this.fileName11 = uploadedFile;
    this.file_Name = uploadedFile['name'];
    this.dealerPrincipleDialogForm.controls.fileUpload.setValue(this.file_Name);
  }

  uploadFiles(event) {
    this.dealerPrincipleDialogForm.controls['creditVettingDocName'].patchValue(event.target.files[0].name);
    this.formData.append('creditVettingDocName', event.target.files[0]);
  }
  obj: any;
  onFileSelected(files: FileList): void {
    this.showFailedImport = false;
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    if (this.fileName) {
      this.isFileSelected = true;
    }
    const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
    if (fileExtension !== '.xlsx') {
     // this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
     // return;
    }
    this.obj = fileObj;
    if (this.obj.name) {
    }
  }
 
  onSubmitDelaerPrincipleDialog() {
    if (!this.dealerPrincipleDialogForm?.valid) {
      return;
    }
    this.formData = new FormData();

    let formValue = this.dealerPrincipleDialogForm.value;   
    let dealerTypeObject = Object.assign({},
      { principleName: this.dealerPrincipleDialogForm.value.principleName ? this.dealerPrincipleDialogForm.value.principleName : '' },
      { principleSurname: this.dealerPrincipleDialogForm.value.principleSurname ? this.dealerPrincipleDialogForm.value.principleSurname : '' },
      { principleIDNumber: this.dealerPrincipleDialogForm.value.principleIDNumber ? this.dealerPrincipleDialogForm.value.principleIDNumber : '' },
      { principlePSIRARegNumber: this.dealerPrincipleDialogForm.value.principlePSIRARegNumber ? this.dealerPrincipleDialogForm.value.principlePSIRARegNumber : '' },
      { principlePSIRAExpiryDate: this.dealerPrincipleDialogForm.value.principlePSIRAExpiryDate ? this.datePipe.transform(this.dealerPrincipleDialogForm.value.principlePSIRAExpiryDate, 'yyyy-MM-ddThh:mm:ss') : '' },
      { email: this.dealerPrincipleDialogForm.value.email ? this.dealerPrincipleDialogForm.value.email : '' },
      { dealerId: this.dealerId },
      { contactNumber: this.dealerPrincipleDialogForm.value.contactNumber ? this.dealerPrincipleDialogForm.value.contactNumber : '' },
      { creditVettingDocName: (this.config?.data?.row && this.config?.data?.row?.creditVettingDocName) ? this.config?.data?.row?.creditVettingDocName : this.obj ? this.obj.name : null },
      { creditVettingDocPath: (this.config?.data?.row && this.config?.data?.row?.creditVettingDocPath) ? this.config?.data?.row?.creditVettingDocPath : '' },
      // { isActive: this.dealerPrincipleDialogForm.value.isActive ? this.dealerPrincipleDialogForm.value.isActive : false },
      { isActive: false },
      { createdUserId: this.loggedUser.userId },
      { dealerPrincipleId: (this.config?.data?.row && this.config?.data?.row?.dealerPrincipleId) ? this.config?.data?.row?.dealerPrincipleId : null }
    );
    this.isSubmitted = true;
    let filterdNewData = Object.entries(dealerTypeObject).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    let isPrincpleIdExists = dealerTypeObject?.dealerPrincipleId;

    Object.keys(this.formData).forEach(key => {
      if (this.formData[key] === "" || this.formData[key].length == 0) {
        delete this.formData[key]
      }
    });
    if(this.fileName11){
      this.formData.append("file", this.fileName11, this.fileName11['name']);
              // this.formData.append("file", this.obj);
    }



    this.formData.append('Obj', JSON.stringify(filterdNewData));

    let api = isPrincpleIdExists ? this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_PRINCIPLE, this.formData) : this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_PRINCIPLE, this.formData);
     api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
      }
      this.ref.close(res);
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }
  btnCloseClick() {
    this.ref.close(false);
  }

}