import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerBranchCategoryAddEditComponent } from '@modules/dealer';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';

@NgModule({
    declarations: [DealerBranchCategoryAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: 'view', component: DealerBranchCategoryAddEditComponent,  canActivate:[AuthGuard],data: { title: 'Dealer Branch Category View' } },
            { path: 'add-edit', component: DealerBranchCategoryAddEditComponent,  canActivate:[AuthGuard],data: { title: 'Dealer Branch Category Add Edit' } },
        ])
    ],
    providers: [
        DatePipe
    ],
    entryComponents: []
})
export class DealerBranchCategoryAddEditModule { }
