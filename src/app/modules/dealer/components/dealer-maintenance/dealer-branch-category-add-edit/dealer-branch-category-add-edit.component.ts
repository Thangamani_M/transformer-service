import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { DealerBranchCategoryDetailModel, DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-dealer-branch-category-add-edit',
  templateUrl: './dealer-branch-category-add-edit.component.html',
})
export class DealerBranchCategoryAddEditComponent implements OnInit {

  dealerBranchCategoryAddEditForm: FormGroup;
  dealerCategoryList: any = [];
  dealerBranchList: any = [];
  dealerBranchCategoryId: any;
  viewable: boolean;
  userData: UserLogin;
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  eventSubscription: any;
  btnName: string;
  showAction: boolean;
  dealerId: string;
  dealerTypeId: string;
  referenceId: string;
  dealerBranchCategoryDetail: any;
  dropdownSubscription: any;
  startCommDate: any = new Date();
  startBonusDate: any = new Date();
  isSubmitted: boolean;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigPropertiesPageLevelAccess :any= {
    tableComponentConfigs:{
      tabsList:[
        {},{},{},{},{}
      ]
    }
  }
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private datePipe: DatePipe,) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.dealerId = (Object.keys(params['params']).length > 0) ? params['params']['dealerId'] : '';
      this.dealerTypeId = (Object.keys(params['params']).length > 0) ? params['params']['dealerTypeId'] : '';
      this.dealerBranchCategoryId = (Object.keys(params['params']).length > 0) ? params['params']['dealerBranchCategoryId'] : '';
      this.referenceId = (Object.keys(params['params']).length > 0) ? params['params']['referenceId'] : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: this.dealerBranchCategoryId && !this.viewable ? 'Update Dealer Branch Category' : this.viewable ? 'View Dealer Branch Category' : 'Create Dealer Branch Category',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Maintenance', relativeRouterUrl: '/dealer/dealer-maintenance' }, { displayName: 'Dealer Branch Category', relativeRouterUrl: '/dealer/dealer-maintenance', queryParams: { tab: 3, id: this.dealerId, dealerTypeId: this.dealerTypeId, } }, { displayName: 'Create Dealer Branch Category', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Create Dealer Branch Category',
            dataKey: 'dealerBranchCategoryId',
            enableBreadCrumb: true,
            enableAction: false,
          },
        ]
      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('&dealerBranchCategoryId') > -1 && data?.url?.indexOf('add-edit') > -1) {
        this.viewable = false;
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_MAINTENANCE]
      if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesPageLevelAccess, permission);
      this.primengTableConfigPropertiesPageLevelAccess = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
    });
    }

  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    this.loadAllDropdown();
    if (this.viewable) {
      if (this.referenceId) {
        this.primengTableConfigProperties.breadCrumbItems[1]['relativeRouterUrl'] = '';
        this.primengTableConfigProperties.breadCrumbItems[2]['queryParams'] = { tab: 3, id: this.dealerId, dealerTypeId: this.dealerTypeId, referenceId: this.referenceId };
      } else {
        this.getDealerDetails();
      }
      this.onShowValue();
    }
  }

  getDealerDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER,
      this.dealerId,
      false, null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        // if (!this.referenceId && (data?.resources?.approvalStatus?.toLowerCase() != 'approved' || !data?.resources?.approvalStatus)) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
        // }
      }

    });
  }

  initForm(dealerBranchCategoryDetailModel?: DealerBranchCategoryDetailModel) {
    let dealerBranchCategoryModel = new DealerBranchCategoryDetailModel(dealerBranchCategoryDetailModel);
    this.dealerBranchCategoryAddEditForm = this.formBuilder.group({});
    Object.keys(dealerBranchCategoryModel).forEach((key) => {
      if (typeof dealerBranchCategoryModel[key] === 'object') {
        this.dealerBranchCategoryAddEditForm.addControl(key, new FormArray(dealerBranchCategoryModel[key]));
      } else if (!this.viewable) {
        this.dealerBranchCategoryAddEditForm.addControl(key, new FormControl(dealerBranchCategoryModel[key]));
      }
    });
    if (!this.viewable) {
      this.onValueChanges();
      this.setFormValidators();
      this.onDisableFormControl();
    }
  }

  setFormValidators() {
    this.dealerBranchCategoryAddEditForm = setRequiredValidator(this.dealerBranchCategoryAddEditForm,
      ["dealerBranchId", "dealerCategoryTypeId", "commissionPercentage", "commissionStartDate", "bonusMultipleStartDate",
        "bonusMultiple", "dsfMultiple", "createdUserId"]);
  }

  onDisableFormControl() {
    this.dealerBranchCategoryAddEditForm.get('commissionPercentage').disable();
    this.dealerBranchCategoryAddEditForm.get('bonusMultiple').disable();
    this.dealerBranchCategoryAddEditForm.get('dsfMultiple').disable();
    this.dealerBranchCategoryAddEditForm.get('createdUserId').setValue(this.userData?.userId);
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.dealerBranchCategoryId && !this.viewable ? 'Update Dealer Branch Category' : this.viewable ? 'View Dealer Branch Category' : 'Add Dealer Branch Category';
    this.showAction = this.viewable ? false : true;
    this.btnName = this.dealerBranchCategoryId ? 'Update' : 'Save';
    this.primengTableConfigProperties.breadCrumbItems[3]['displayName'] = this.viewable ? 'View Dealer Branch Category' : this.dealerBranchCategoryId && !this.viewable ? 'View Dealer Branch Category' : 'Add Dealer Branch Category';
    if (!this.viewable && this.dealerBranchCategoryId) {
      this.primengTableConfigProperties.breadCrumbItems[3]['relativeRouterUrl'] = '/dealer/dealer-maintenance/branch-category/view';
      this.primengTableConfigProperties.breadCrumbItems[3]['queryParams'] = { dealerId: this.dealerId, dealerTypeId: this.dealerTypeId, dealerBranchCategoryId: this.dealerBranchCategoryId };
    }
    if (!this.viewable && this.primengTableConfigProperties.breadCrumbItems?.length == 4) {
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Update Dealer Branch Category' });
    }
    if (this.viewable && this.primengTableConfigProperties.breadCrumbItems[4]) {
      this.primengTableConfigProperties.breadCrumbItems.pop();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['./../add-edit'], { relativeTo: this.activatedRoute, queryParams: { dealerId: this.dealerId, dealerTypeId: this.dealerTypeId, dealerBranchCategoryId: this.dealerBranchCategoryId }, skipLocationChange: true })
  }

  loadAllDropdown() {
    let api: any;
    if (!this.viewable) {
      api = [this.crudService.dropdown(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.UX_DEALER_BRANCH, prepareRequiredHttpParams({ isAll: true, dealerId: this.dealerId })),
      ]
    }
    if (this.viewable) {
      api = [this.getValue()];
    }
    if (this.dealerBranchCategoryId && !this.viewable) {
      api.push(this.getValue());
    }
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
    }
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200 && !this.viewable) {
          switch (ix) {
            case 0:
              this.dealerBranchList = resp.resources;
              break;
            case 1:
              this.editValue(resp);
              break;
          }
        } else if (this.viewable) {
          switch (ix) {
            case 0:
              this.viewValue(resp);
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getValue() {
    return this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_BRANCH_CATEGORY_DETAILS, this.dealerBranchCategoryId);
  }

  viewValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.onShowValue(response);
    }
  }

  onShowValue(response?: any) {
    this.dealerBranchCategoryDetail = [{
      name: 'Dealer Branch Category Details', columns: [
        { name: 'Branch Code', value: response ? response?.resources?.dealerBranchCode : '' },
        { name: 'Bonus Start Date', value: response ? response?.resources?.bonusMultipleStartDate : '', isDate: true },
        { name: 'Monthly Commission Type', value: response ? response?.resources?.dealerCategoryTypeName : '' },
        { name: 'Bonus Multiple', value: response ? response?.resources?.bonusMultiple : '' },
        { name: 'Commission Percentage', value: response ? response?.resources?.commissionPercentage : '', isNumber: true },
        { name: 'DSF Multiple', value: response ? response?.resources?.dsfMultiple : '' },
        { name: 'Commission Start Date', value: response ? response?.resources?.commissionStartDate : '', isDate: true },
        { name: 'SAP Vendor Number', value: response ? response?.resources?.sapVendorNumber : '' },
      ]
    }];
  }

  editValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.dealerBranchCategoryDetail = response?.resources;
      this.startBonusDate = response?.resources?.bonusMultipleStartDate ? new Date(response?.resources?.bonusMultipleStartDate) : new Date();
      this.startCommDate = response?.resources?.commissionStartDate ? new Date(response?.resources?.commissionStartDate) : new Date();
      const dealerBranch = this.dealerBranchList.find(el => el.dealerBranchId == response?.resources?.dealerBranchId);
      this.dealerBranchCategoryAddEditForm.patchValue({
        dealerBranchId: dealerBranch,
        bonusMultipleStartDate: this.startBonusDate ? this.startBonusDate : null,
        bonusMultiple: response?.resources?.bonusMultiple ? response?.resources?.bonusMultiple : '',
        commissionPercentage: response?.resources?.commissionPercentage ? response?.resources?.commissionPercentage : '',
        dsfMultiple: response?.resources?.dsfMultiple ? response?.resources?.dsfMultiple : '',
        commissionStartDate: this.startCommDate ? this.startCommDate : null,
        sapVendorNumber: response?.resources?.sapVendorNumber,
      }, { emitEvent: false });
      this.onLoadDealerCategory(response?.resources, false);
    }
  }

  onValueChanges() {
    this.dealerBranchCategoryAddEditForm.get('dealerBranchId').valueChanges.subscribe(res => {
      if (res) {
        this.onLoadDealerCategory(res);
      }
    })
    this.dealerBranchCategoryAddEditForm.get('dealerCategoryTypeId').valueChanges.subscribe(res => {
      if (res) {
        this.crudService.dropdown(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_BRANCH_CATEGORY, prepareRequiredHttpParams({ dealerTypeId: this.dealerTypeId, dealerCategoryTypeId: res?.id }))
          .subscribe((response: IApplicationResponse) => {
            if (response?.isSuccess && response?.statusCode == 200 && response?.resources) {
              this.dealerBranchCategoryAddEditForm.get('commissionPercentage').setValue(response?.resources?.commission);
              this.dealerBranchCategoryAddEditForm.get('bonusMultiple').setValue(response?.resources?.monthValueName);
              this.dealerBranchCategoryAddEditForm.get('dsfMultiple').setValue(response?.resources?.dsfValue);
            } else {
              this.snackbarService.openSnackbar("Please try selecting any other Monthly Commission Type", ResponseMessageTypes.WARNING);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      }
    })
  }

  onLoadDealerCategory(res, dropdown = true) {
    this.crudService.dropdown(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.UX_DEALER_BRANCH_CATEGORY_COMMISSION_TYPE, prepareRequiredHttpParams({ isAll: false, dealerBranchId: res?.dealerBranchId }))
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.dealerCategoryList = response?.resources;
          if (!dropdown) {
            const dealerCategory = this.dealerCategoryList.find(el => el.id == this.dealerBranchCategoryDetail?.dealerCategoryTypeId);
            this.dealerBranchCategoryAddEditForm.get('dealerCategoryTypeId').setValue(dealerCategory, { emitEvent: false })
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSubmit() {
    this.dealerBranchCategoryAddEditForm.markAsUntouched();
    if (this.dealerBranchCategoryAddEditForm.invalid) {
      this.dealerBranchCategoryAddEditForm.markAllAsTouched();
      return;
    }
    if (!this.viewable) {
      this.onAfterSubmit();
    }
  }

  onAfterSubmit() {
    const dealerBranchCategoryObj = {
      ...this.dealerBranchCategoryAddEditForm.getRawValue()
    }
    delete dealerBranchCategoryObj.commissionPercentage;
    dealerBranchCategoryObj['dealerBranchId'] = this.dealerBranchCategoryAddEditForm.value.dealerBranchId?.dealerBranchId;
    dealerBranchCategoryObj['dealerCategoryTypeId'] = this.dealerBranchCategoryAddEditForm.value.dealerCategoryTypeId?.id;
    dealerBranchCategoryObj['bonusMultipleStartDate'] = this.datePipe.transform(dealerBranchCategoryObj['bonusMultipleStartDate'], 'yyyy-MM-dd');
    dealerBranchCategoryObj['commissionStartDate'] = this.datePipe.transform(dealerBranchCategoryObj['commissionStartDate'], 'yyyy-MM-dd');
    if (this.dealerBranchCategoryId) {
      // delete dealerBranchCategoryObj.createdUserId;
      // dealerBranchCategoryObj['modifieduserId'] = this.userData?.userId;
      dealerBranchCategoryObj['dealerBranchCategoryId'] = this.dealerBranchCategoryId;
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_BRANCH_CATEGORY_API, dealerBranchCategoryObj);
    if (this.dealerBranchCategoryId) {
      api = this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_BRANCH_CATEGORY_API, dealerBranchCategoryObj)
    }
    this.isSubmitted = true;
    api.subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.router.navigate(['/dealer/dealer-maintenance'], { queryParams: { tab: 3, id: this.dealerId, dealerTypeId: this.dealerTypeId, } });
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCancelClick() {
    if (this.viewable || !this.dealerBranchCategoryId || this.dealerBranchCategoryId) {
      if (this.referenceId) {
        this.router.navigate(['/dealer/dealer-maintenance'], { queryParams: { tab: 3, id: this.dealerId, dealerTypeId: this.dealerTypeId, referenceId: this.referenceId, }, skipLocationChange: false })
      } else {
        this.router.navigate(['/dealer/dealer-maintenance'], { queryParams: { tab: 3, id: this.dealerId, dealerTypeId: this.dealerTypeId, } })
      }
    } else {
      this.router.navigate(['/dealer/dealer-maintenance/branch-category/view'], { queryParams: { dealerId: this.dealerId, dealerTypeId: this.dealerTypeId, dealerBranchCategoryId: this.dealerBranchCategoryId } })
    }
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
