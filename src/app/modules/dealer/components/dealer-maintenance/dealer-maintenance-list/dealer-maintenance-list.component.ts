import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengConfirmDialogPopupComponent, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { DealerCommentsHistoryComponent, DealerDocumentDeclineReasonComponent, DealerDocumentsAddEditComponent, DealerPrincipleAddEditComponent } from '@modules/dealer';
import { DealerMaintenanceFilterModel } from '@modules/dealer/models/dealer-maintenance.model';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { DealerAVSCheckComponent } from '../dealer-avs-check/dealer-avs-check-component';
import { DealerCreditVettingComponent } from '../dealer-credit-vetting/dealer-credit-vetting-component';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-dealer-maintenance-list',
  templateUrl: './dealer-maintenance-list.component.html',
  styleUrls: ['./dealer-maintenance-list.component.scss']
})
export class DealerMaintenanceListComponent  extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  isLoading: boolean;
  isSubmitted: boolean;
  showFilterForm = false;
  dealerId: any;
  dealerTypeId: any;
  referenceId: any;
  pageSize: any = 10;
  dealerMaintenanceFilterForm: FormGroup;
  dealerTypeFilterList: any = [];
  psiraRegNoFilterList: any = [];
  dealerTypeList: any = [];
  commissionDescriptionList: any = [];
  selectedRows: any = [];
  listSubscribtion: any;
  dataList: any;
  dateFormat = 'MMM dd, yyyy';
  observableResponse: any;
  row: any = {};
  first: any = 0;
  filterData: any;
  obj: any;
  reset: boolean;
  multipleSubscription: any;
  psiraExpiryDate: any = new Date();
  showFailedImport: boolean = false;
  fileName = '';
  isFileSelected: boolean = false;
  dealerDocumentTypeList: any;
  dealerAddDocumentForm: FormGroup;
  isFileUpload: boolean = false;
  alertDialog: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialogService: DialogService, private datePipe: DatePipe,
    private router: Router, private activatedRoute: ActivatedRoute, private dialog: MatDialog,) {
    super();
      this.primengTableConfigProperties = {
      tableCaption: 'Dealer Maintenance',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Maintenance', relativeRouterUrl: '' }, { displayName: 'Dealer Info', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Info',
            dataKey: 'dealerId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            disabled: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealerIIPCode', header: 'Dealer Code', width: '100px' },
              { field: 'companyName', header: 'Company Name', width: '200px' },
              { field: 'dealerTypeName', header: 'Dealer Type', width: '140px' },
              { field: 'startDate', header: 'Start Date', width: '100px' },
              // { field: 'companyRegNumber', header: 'Company registration number', width: '150px' },
              // { field: 'vatNumber', header: 'Vat number', width: '70px' },
              { field: 'terminationDate', header: 'End Date', width: '100px' },
              { field: 'status', header: 'Status', width: '150px' },
              { field: 'dealerTerminationReasonName', header: 'Termination Reason', width: '120px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: true,
            enableStatusActiveAction: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER,
            deleteAPISuffixModel: DealerModuleApiSuffixModels.DEALER,
            moduleName: ModulesBasedApiSuffix.DEALER,
          },
          {
            caption: 'Dealer Principle',
            dataKey: 'dealerId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            disabled: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'principleName', header: 'Dealer Principle Name', width: '200px' },
              { field: 'principleSurname', header: 'Dealer Principle Surname', width: '200px' },
              { field: 'principleIDNumber', header: 'Dealer Principle ID No', width: '180px' },
              { field: 'principlePSIRARegNumber', header: 'PSIRA Reg Number', width: '160px' },
              { field: 'email', header: 'Email', width: '180px' },
              { field: 'contactNumber', header: 'Contact Number', width: '140px' },
              { field: 'isActive', header: 'Status', width: '100px' },
              { field: 'approvalStatus', header: 'Approval Status', width: '140px' },
              // { field: 'viewResult', header: 'View Results', width: '200px', isUrlLink: true },
              // { field: 'actionLabel', header: 'Action', isButton: true, width: '150px', cssClassName: 'fdt-green-button' },
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_PRINCIPLE,
            deleteAPISuffixModel: DealerModuleApiSuffixModels.DEALER,
            moduleName: ModulesBasedApiSuffix.DEALER,
          },
          {
            caption: 'Branch Details',
            dataKey: 'dealerBranchId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            disabled: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealerBranchCode', header: 'Dealer Branch Code', width: '160px' },
              { field: 'branchName', header: 'Dealer Branch Name', width: '170px' },
              { field: 'managerName', header: 'Branch Manager Name', width: '200px' },
              { field: 'startDate', header: 'Start Date', width: '80px' },
              { field: 'emailAddress', header: 'Email Address', width: '150px' },
              { field: 'phoneNumber', header: 'Contact Number', width: '140px' },
              { field: 'status', header: 'Status', width: '80px' },
              { field: 'approvalStatus', header: 'Approval Status', width: '140px' },
              // { field: 'viewResult', header: 'View Results', width: '200px', isUrlLink: true },
              // { field: 'actionLabel', header: 'Action', isButton: true, width: '150px', cssClassName: 'fdt-green-button' },
            ],
            enableMultiDeleteActionBtn: true,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_BRANCH,
            deleteAPISuffixModel: DealerModuleApiSuffixModels.DEALER_BRANCH_DELETE,
            moduleName: ModulesBasedApiSuffix.DEALER,
          },
          {
            caption: 'Dealer Branch Category',
            dataKey: 'dealerBranchCategoryId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            disabled: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealerBranchCode', header: 'Branch Code', width: '100px' },
              { field: 'dealerCategoryTypeName', header: 'Category (Monthly Commission Type)', width: '150px' },
              { field: 'commissionStartDate', header: 'Commission Start Date', width: '160px' },
              { field: 'bonusMultipleStartDate', header: 'Bonus Multiple Start Date', width: '170px' },
              { field: 'commissionPercentage', header: 'Ccommission Percentage', width: '100px' },
              { field: 'bonusMultiple', header: 'Bonus Multiple', width: '110px' },
              { field: 'dsfMultiple', header: 'DSF Multiple', width: '100px' },
              { field: 'sapVendorNumber', header: 'SAP Vendor Number', width: '140px' },
              { field: 'isActive', header: 'Status', width: '80px' },
              { field: 'approvalStatus', header: 'Approval Status', width: '140px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_BRANCH_CATEGORY_LIST,
            deleteAPISuffixModel: DealerModuleApiSuffixModels.DEALER_BRANCH_CATEGORY_LIST,
            moduleName: ModulesBasedApiSuffix.DEALER,
          },
          {
            caption: 'Dealer Documents',
            dataKey: 'dealerDocumentId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            disabled: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'docName', header: 'Document', width: '200px' },
              { field: 'dealerDocumentTypeName', header: 'Document Type', width: '150px' },
              { field: 'dealerDocumentStatus', header: 'Status', width: '150px' },
              { field: 'approvalStatus', header: 'Approval Status', width: '140px' },
              { field: 'Update', header: 'Action', width: '100px', nofilter: true, isUpdateDelete: true },
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_DOCUMENT,
            deleteAPISuffixModel: DealerModuleApiSuffixModels.DEALER_DOCUMENT_DELETE,
            moduleName: ModulesBasedApiSuffix.DEALER,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.dealerId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.dealerTypeId = (Object.keys(params['params']).length > 0) ? params['params']['dealerTypeId'] : '';
      this.referenceId = (Object.keys(params['params']).length > 0) ? params['params']['referenceId'] : '';
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      if (this.selectedTabIndex == 0 && this.primengTableConfigProperties.breadCrumbItems[2]) {
        this.primengTableConfigProperties.breadCrumbItems.pop();
      }
      if (this.referenceId) {
        this.primengTableConfigProperties.breadCrumbItems[1]['relativeRouterUrl'] = '';
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableAddActionBtn = false;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableMultiDeleteActionBtn = false;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].checkBox = false;
      }
      if (this.referenceId && this.selectedTabIndex == 4) {
        // this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].checkBox = true;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableRowDelete = false;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns = [
          { field: 'docName', header: 'Document', width: '180px' },
          { field: 'dealerDocumentTypeName', header: 'Document Type', width: '100px' },
          { field: 'dealerDocumentStatus', header: 'Status', width: '80px' },
          { field: 'reason', header: 'Comments', width: '150px', isInput: true },
          { field: 'dealerDocumentActionStatus', header: 'Action Status', width: '200px', isRadioBtn: true, options: [{ display: 'Accept', value: true }, { display: 'Reject', value: false }] },
          { field: 'viewResult', header: 'View Results', width: '100px', isUrlLink: true },
        ]
        this.pageSize = 50;
      } else {
        this.pageSize = 10;
      }
      if (!this.referenceId && (this.selectedTabIndex == 1 || this.selectedTabIndex == 2)) {
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[8] =
          { field: 'viewResult', header: 'View Results', width: '150px', isUrlLink: true };
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[9] =
          { field: 'actionLabel', header: 'Action', isButton: true, width: '150px', cssClassName: 'fdt-green-button cursor-pointer' };
      }
      this.getDealerMaintenanceListData();
      this.createDealerMaintenanceFilterForm();
      this.onBreadCumbChange();
      this.onDealerDocumentLoad();
      if (this.selectedTabIndex != 0 && this.dealerId) {
        this.primengTableConfigProperties.tableComponentConfigs.tabsList.map(el => el.disabled = false);
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
    this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
     this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][DEALER_COMPONENT.DEALER_MAINTENANCE]
      if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
      this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
    });
    }

  getDealerDocumentTypeDropdown(params) {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_DOCUMENT_DROPDOWN,
      params,
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.dealerDocumentTypeList = data.resources;
      }

    });
  }
  onDealerDocumentLoad() {
    if (this.selectedTabIndex == 4) {
      this.getDealerDocumentTypeDropdown({ dealerTypeId: this.dealerTypeId });
      this.createDealerDocumentForm();
    }
  }
  createDealerDocumentForm() {
    this.dealerAddDocumentForm = new FormGroup({
      'dealerDocumentConfigId': new FormControl(null),
    });
    this.dealerAddDocumentForm = setRequiredValidator(this.dealerAddDocumentForm, ['dealerDocumentConfigId']);

  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.data && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getDealerMaintenanceListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
        // if (this.selectedTabIndex == 1 && row && row?.viewResult == 'AVS Verification') {
        //   if (row.creditVettingStatus == true)
        //     this.openDealerPrincipleCreditVetting(CrudType.EDIT, row);
        //   else
        //     this.snackbarService.openSnackbar("Please Complete The Credit Vetting.", ResponseMessageTypes.WARNING);
        // }
        // else if (this.selectedTabIndex == 2 && row && row?.viewResult == 'AVS Verification') {
        //   if (row.avsCheckStatus == true)
        //     this.openDealerBranchAVSCheck(CrudType.EDIT, row);
        //   else
        //     this.snackbarService.openSnackbar("Please Complete The AVS Check.", ResponseMessageTypes.WARNING);
        // }
        this.openAddEditPage(CrudType.EDIT, row, unknownVar);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      case CrudType.STATUS_POPUP:
        this.onChangeStatus(row, unknownVar)
        break;
      case CrudType.ACTION:
        if (this.selectedTabIndex == 1) {
          if (row.creditVettingStatus == true)
            this.snackbarService.openSnackbar("The Credit Vetting Verification is completed already.", ResponseMessageTypes.WARNING);
          else
            this.dealerCreditVetting(row);
        }
        else if (this.selectedTabIndex == 2)
          this.dealerCreditVettingAvsCheck(row);
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }else{
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else if (row?.approvalStatus?.toLowerCase() != 'approved') {
            this.onOneOrManyRowsDelete();
          } else {
            this.snackbarService.openSnackbar("This request can't be deleted.", ResponseMessageTypes.WARNING);
          }
        } else if (row?.approvalStatus?.toLowerCase() != 'approved') {
          this.onOneOrManyRowsDelete(row);
        } else {
          this.snackbarService.openSnackbar("This request can't be deleted.", ResponseMessageTypes.WARNING);
        }
      }
        break;
      default:
    }
  }
  dealerCreditVetting(val) {
    let finalObject = {
      dealerPrincipleId: val.dealerPrincipleId,
      principleName: val.principleName,
      principleSurname: val.principleSurname,
      principleIDNumber: val.principleIDNumber,
      contactNumber: val.contactNumber,
      email: val.email,
      creditVettingDocName: val.creditVettingDocName,
      creditVettingDocPath: val.creditVettingDocPath,
      dealerAddress: val.formatedAddress,
      createdUserId: val.createdUserId,
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.CREDIT_VETTING, finalObject, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getDealerMaintenanceListData();
      }
      else
        this.getDealerMaintenanceListData();
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  dealerCreditVettingAvsCheck(val) {
    let finalObject = {
      surname: val.lastName,
      forename1: val.firstName,
      said: val.southAfricanId,
      isLegalEntity: val.isLegalEntity,
      isPerson: val.isPerson,
      companyRegistrationNumber: val.companyRegistrationNumber,
      dealerAddress: val.formatedAddress,
      phoneNo: val.phoneNumber,
      dealerBranchId: val.dealerBranchId,
      createdUserId: this.loggedInUserData.userId,
      principalAccountVerifications: [
        {
          accountHolder: val.firstName + ' ' + val.lastName,
          accountNumber: val.accountNo,
          accountType: val.accountTypeName,
          branchCode: val.bankBranchCode
        }
      ]
    }

    let api = this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.CREDIT_VETTING_AVS_CHECK, finalObject, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getDealerMaintenanceListData();
      }
      else
        this.getDealerMaintenanceListData();
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  tabClick(e) {
    this.selectedTabIndex = e?.index;
    this.dataList = [];
    let queryParams = {};
    if (this.dealerId) {
      queryParams['tab'] = this.selectedTabIndex;
      queryParams['id'] = this.dealerId;
      queryParams['dealerTypeId'] = this.dealerTypeId;
      if (this.referenceId) {
        queryParams['referenceId'] = this.referenceId;
      }
    }
    this.onBreadCumbChange();
    this.onAfterTabChange(queryParams);
    // this.getDealerMaintenanceListData();
  }

  onAfterTabChange(queryParams) {
    this.onDealerDocumentLoad();
    if (this.selectedTabIndex != 0 || !this.dealerId) {
      this.router.navigate([`../`], { relativeTo: this.activatedRoute, queryParams: queryParams });
    }
    if (this.selectedTabIndex == 0 && this.dealerId) {
      this.router.navigate([`../info/view`], { relativeTo: this.activatedRoute, queryParams: queryParams });
    }
  }

  onBreadCumbChange() {
    if (this.selectedTabIndex != 0) {
      this.primengTableConfigProperties.breadCrumbItems[2] = { displayName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption };
    }
    if (this.selectedTabIndex != 0 && this.dealerId && !this.referenceId) {
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/dealer/dealer-maintenance';
    }
    if (!this.dealerId && this.selectedTabIndex == 0) {
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '';
    }
  }

  createDealerMaintenanceFilterForm(dealerMaintenanceFilterModel?: DealerMaintenanceFilterModel) {
    let dealerMaintenanceModel = new DealerMaintenanceFilterModel(dealerMaintenanceFilterModel);
    this.dealerMaintenanceFilterForm = this.formBuilder.group({});
    Object.keys(dealerMaintenanceModel).forEach((key) => {
      this.dealerMaintenanceFilterForm.addControl(key, new FormControl(dealerMaintenanceModel[key]));
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string | any, value?: number | any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["./info/add-edit"], { relativeTo: this.activatedRoute });
            break;
          case 1:
            this.openDealerPrincipleAddEdit(null, null);
            break;
          case 2:
            this.router.navigate(['dealer/dealer-maintenance/branch/add-edit'],
              { queryParams: { dealerId: this.dealerId, dealerTypeId: this.dealerTypeId, } });
            break;
          case 3:
            this.router.navigate(['dealer/dealer-maintenance/branch-category/add-edit'],
              { queryParams: { dealerId: this.dealerId, dealerTypeId: this.dealerTypeId, } });
            break;
          case 4:
            this.openDealerDocumentDialog('', '25vh');
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["./info/view"], {
              relativeTo: this.activatedRoute, queryParams: {
                id: editableObject[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
                dealerTypeId: editableObject['dealerTypeId'],
              }
            });
            break;
          case 1:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }

            this.openDealerPrincipleAddEdit(CrudType.VIEW, editableObject);
            break;
          case 2:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }

            if (this.referenceId) {
              this.router.navigate(['dealer/dealer-maintenance/branch/add-edit'],
                { queryParams: { dealerId: this.dealerId, id: editableObject['dealerBranchId'], dealerTypeId: this.dealerTypeId, referenceId: this.referenceId, }, skipLocationChange: true });
            } else {
              this.router.navigate(['dealer/dealer-maintenance/branch/add-edit'],
                { queryParams: { dealerId: this.dealerId, id: editableObject['dealerBranchId'], dealerTypeId: this.dealerTypeId, } });
            }
            break;
          case 3:
            if (this.referenceId) {
              this.router.navigate(['dealer/dealer-maintenance/branch-category/view'],
                { queryParams: { dealerId: this.dealerId, dealerTypeId: this.dealerTypeId, dealerBranchCategoryId: editableObject['dealerBranchCategoryId'], referenceId: this.referenceId, }, skipLocationChange: true });
            } else {
              this.router.navigate(['dealer/dealer-maintenance/branch-category/view'],
                { queryParams: { dealerId: this.dealerId, dealerTypeId: this.dealerTypeId, dealerBranchCategoryId: editableObject['dealerBranchCategoryId'] } });
            }
            break;
          case 4:
            if (editableObject && editableObject['path'] && editableObject['dealerDocumentTypeName']) {
              this.downloadExportedFile(editableObject['path'], editableObject['dealerDocumentTypeName']);
            }
            break;
        }
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 1:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }

            if (editableObject && editableObject?.viewResult == 'AVS Verification') {
              if (editableObject.creditVettingStatus == true) {
                this.openDealerPrincipleCreditVetting(CrudType.EDIT, editableObject);
              } else {
                this.snackbarService.openSnackbar("Please Complete The Credit Vetting.", ResponseMessageTypes.WARNING);
              }
            }
            break;
          case 2:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }

            if (editableObject && editableObject?.viewResult == 'AVS Verification') {
              if (editableObject.avsCheckStatus == true) {
                this.openDealerBranchAVSCheck(CrudType.EDIT, editableObject);
              }
              else {
                this.snackbarService.openSnackbar("Please Complete The AVS Check.", ResponseMessageTypes.WARNING);
              }
            }
            break;
          case 4:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }

            if (editableObject && editableObject?.viewResult?.toLowerCase() == 'comments history' && value == 'viewResult') {
              this.openDealerCommentHistory('Comments History', editableObject);
            } else if (!this.referenceId && editableObject?.approvalStatus?.toLowerCase() != 'approved') {
              this.openDealerDocumentDialog(editableObject, '15vh');
            } else {
              this.openDealerDocumentDialog(editableObject, '15vh');
              // this.snackbarService.openSnackbar("This document can't be edited.", ResponseMessageTypes.WARNING);
            }
            break;
        }
        break;
    }
  }
  downloadExportedFile(url, fileName) {
    var link = document.createElement("a");
    if (link.download !== undefined) {
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  openDealerDocumentDialog(row?, height?) {
    const ref = this.dialogService.open(DealerDocumentsAddEditComponent, {
      header: row ? 'Update Dealer Document' : 'Create Dealer Document',
      showHeader: false,
      baseZIndex: 1000,
      width: '500px',
      // height: '500px',
      data: {
        row: {
          ...row,
          // path: '',
          dealerId: this.dealerId,
          createdUserId: this.loggedInUserData?.userId,
        },
        api: DealerModuleApiSuffixModels.DEALER_DOCUMENT,
        module: ModulesBasedApiSuffix.DEALER,
        dealerDocumentTypeList: this.dealerDocumentTypeList,
        dialogHeight: height,
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.getDealerMaintenanceListData()
      }
    });
  }
  openDealerPrincipleAddEdit(type: CrudType | string, editableObject?: object | string | any) {
    let isShowAction;
    // if (this.referenceId || editableObject?.approvalStatus?.toLowerCase() == 'approved') {
    //   isShowAction = true;
    // }
    const ref = this.dialogService.open(DealerPrincipleAddEditComponent, {
      header: editableObject ? 'Update Dealer Principle' : 'Add Dealer Principle',
      showHeader: false,
      baseZIndex: 1000,
      width: '54vw',
      // height: '500px',
      data: {
        row: editableObject,
        isShowAction: isShowAction,
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.getDealerMaintenanceListData()
      }
    });

  }
  openDealerPrincipleCreditVetting(type: CrudType | string, editableObject?: object | string) {
    const ref = this.dialogService.open(DealerCreditVettingComponent, {
      header: 'Credit Vetting Result',
      showHeader: false,
      baseZIndex: 1000,
      width: '80vw',
      // height: '400px',
      data: {
        row: editableObject
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.getDealerMaintenanceListData()
      }
    });

  }
  openDealerBranchAVSCheck(type: CrudType | string, editableObject?: object | string) {
    const ref = this.dialogService.open(DealerAVSCheckComponent, {
      header: 'AVS Check',
      showHeader: false,
      baseZIndex: 1000,
      width: '62vw',
      // height: '70vh',
      data: {
        row: editableObject
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.getDealerMaintenanceListData();
      }
    });
  }

  openDealerCommentHistory(header, data) {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_DOCUMENT_COMMENT_HISTORY, undefined,
      false, prepareRequiredHttpParams({ dealerDocumentId: data?.dealerDocumentId }))
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.isSuccess && res?.statusCode == 200) {
          const ref = this.dialogService.open(DealerCommentsHistoryComponent, {
            header: header,
            showHeader: false,
            baseZIndex: 1000,
            width: '62vw',
            // height: '70vh',
            data: res?.resources,
          });
          ref.onClose.subscribe((result) => {
            if (result) {

            }
          });
        }
      })
  }

  onChangeStatus(rowData?, index?) {
    let status;
    if (rowData['isActive']) {
      status = true
    } else {
      status = false
    }
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: status,
        // activeText: 'Enable',
        // inActiveText: 'Disable',
        modifiedUserId: this.loggedInUserData?.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        // this.dataList[index].status = this.dataList[index].status == 'Enable' ? 'Disable' : 'Enable';
        this.getDealerMaintenanceListData(this.row["pageIndex"], this.row["pageSize"]);
      } else {
        this.getDealerMaintenanceListData(this.row["pageIndex"], this.row["pageSize"]);
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (this.selectedTabIndex == 4 && rowData['dealerDocumentStatus']?.toLowerCase() == 'approved') {
      this.snackbarService.openSnackbar("Once Approved Document can't be deleted", ResponseMessageTypes.WARNING);
      return;
    }
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].deleteAPISuffixModel,
        method: 'delete',
        dataObject: {
          ids: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
          isDeleted: true,
          dealerId: this.dealerId,
          modifiedUserId: this.loggedInUserData?.userId,
        }
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.getDealerMaintenanceListData();
      }
    });
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.showFilterForm) {
      this.dealerTypeFilterList = [];
      this.psiraRegNoFilterList = [];
      this.loadActionTypes([
        this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
          DealerModuleApiSuffixModels.DEALER_TYPE, prepareRequiredHttpParams({ isAll: false }))
      ]);
    }
  }

  loadActionTypes(api) {
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.dealerTypeFilterList= getPDropdownData(resp.resources,"dealerTypeName","dealerTypeId");
              break;
            case 1:
              let psiraRegNoFilterList = resp.resources;
              let psiraRegNoFilterArray = [];
              for (var i = 0; i < psiraRegNoFilterList.length; i++) {
                let tmp = {};
                tmp['value'] = psiraRegNoFilterList[i].id?.toString();
                tmp['display'] = psiraRegNoFilterList[i].displayName;
                psiraRegNoFilterArray.push(tmp);
              }
              this.psiraRegNoFilterList = psiraRegNoFilterArray;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
      // this.setFilteredValue();
    });
  }

  onFileSelected(files: FileList): void {
    this.showFailedImport = false;
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    if (this.fileName) {
      this.isFileSelected = true;
    }
    const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
    if (fileExtension !== '.xlsx') {
      // this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
      //  return;
    }
    this.obj = fileObj;
    if (this.obj.name) {
    }
  }


  upload() {
    this.isSubmitted = true;
    this.isFileUpload = true;
    if (this.dealerAddDocumentForm.invalid) {
      this.dealerAddDocumentForm.markAllAsTouched();
      return;
    }
    let formValue = this.dealerAddDocumentForm.value;
    let formData = new FormData();
    let dealerTypeObject = Object.assign({},
      { dealerDocumentConfigId: formValue.dealerDocumentConfigId.toString() },
      { dealerId: this.dealerId },
      { createdUserId: this.loggedInUserData.userId }
    );
    let filterdNewData = Object.entries(dealerTypeObject).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    Object.keys(formData).forEach(key => {
      if (formData[key] === "" || formData[key].length == 0) {
        delete formData[key]
      }
    });
    formData.append("file", this.obj);
    formData.append('Obj', JSON.stringify(filterdNewData));
    this.crudService.fileUpload(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_DOCUMENT, formData).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.isFileUpload = false;
        this.dealerAddDocumentForm.reset();
        this.fileName = null;
        this.getDealerMaintenanceListData();
      }
    });
  }

  submitFilter() {
    this.filterData = Object.assign({},
      this.dealerMaintenanceFilterForm.get('dealerTypeConfigId').value == '' ? null : { dealerTypeConfigId: this.dealerMaintenanceFilterForm.get('dealerTypeConfigId').value },
      this.dealerMaintenanceFilterForm.get('dealerIIPCode').value ? { dealerIIPCode: this.dealerMaintenanceFilterForm.get('dealerIIPCode').value } : '',
      this.dealerMaintenanceFilterForm.get('companyName').value ? { companyName: this.dealerMaintenanceFilterForm.get('companyName').value } : '',
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.observableResponse = this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.dealerMaintenanceFilterForm.reset();
    this.filterData = null;
    this.showFilterForm = !this.showFilterForm;
    if (!this.showFilterForm) {
      this.getDealerMaintenanceListData();
    }
  }

  getDealerMaintenanceListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    const addParams = (this.selectedTabIndex == 3) ? { dealerId: this.dealerId, isAll: true } : (this.selectedTabIndex == 4
      && this.referenceId) ? { dealerId: this.dealerId, dealerApprovalId: this.referenceId } :
      (this.selectedTabIndex != 0) ? { dealerId: this.dealerId } : {};
    otherParams = { ...otherParams, ...addParams };
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    const moduleName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion?.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if (this.selectedTabIndex == 1 || this.selectedTabIndex == 2) {
            val.viewResult = 'AVS Verification';
            val.actionLabel = this.selectedTabIndex == 1 ? 'Credit Vetting' : 'AVS Check';
          }
          if (this.selectedTabIndex == 4) {
            val.viewResult = 'Comments History';
          }
          if (this.selectedTabIndex == 0) {
            val.terminationDate = val.terminationDate ? this.datePipe.transform(val.terminationDate, 'dd-MM-yyyy') : '';
            val.psiraExpiryDate = val.psiraExpiryDate ? this.datePipe.transform(val.psiraExpiryDate, 'dd-MM-yyyy') : '';
          }
          if (this.selectedTabIndex == 0 || this.selectedTabIndex == 2) {
            val.startDate = val.startDate ? this.datePipe.transform(val.startDate, 'dd-MM-yyyy') : '';
          }
          if (this.selectedTabIndex == 3) {
            val.commissionStartDate = val.commissionStartDate ? this.datePipe.transform(val.commissionStartDate, 'dd-MM-yyyy') : '';
            val.bonusMultipleStartDate = val.bonusMultipleStartDate ? this.datePipe.transform(val.bonusMultipleStartDate, 'dd-MM-yyyy') : '';
          }
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
      }
      this.reset = false;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    }, error => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.observableResponse = null;
      this.dataList = this.observableResponse;
      this.totalRecords = 0;
    })
  }

  onApproveAction() {
    if (this.dataList?.length == 0) {
      this.snackbarService.openSnackbar("Please select atleast one document", ResponseMessageTypes.WARNING);
      return;
    }
    const reqObj = {
      dealerId: this.dealerId,
      dealerApprovalId: this.referenceId,
      dealerApprovalActionStatus: true,
      reason: "Approved",
      createdUserId: this.loggedInUserData?.userId,
      dealerDocumentApprovalInsertDTO: [],
    };
    this.dataList.forEach(el => {
      if (el) {
        reqObj.dealerDocumentApprovalInsertDTO.push({
          dealerId: this.dealerId,
          dealerDocumentId: el?.dealerDocumentId,
          dealerApprovalId: this.referenceId,
          dealerDocumentActionStatus: el?.dealerDocumentActionStatus,
          reason: el?.reason,
          isActive: true,
          createdUserId: this.loggedInUserData?.userId,
        });
      }
    });
    this.isSubmitted = true;
    this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_APPROVAL, reqObj)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          // this.onCRUDRequested('get');
          this.router.navigate(['/dealer/dealer-worklist']);
        }
        this.isSubmitted = false;
      })
  }

  onDeclineAction() {
    if (this.dataList?.length == 0) {
      this.snackbarService.openSnackbar("Please select atleast one document", ResponseMessageTypes.WARNING);
      return;
    }
    const reqObj = {
      dealerId: this.dealerId,
      dealerApprovalId: this.referenceId,
      dealerApprovalActionStatus: false,
      reason: "",
      createdUserId: this.loggedInUserData?.userId,
      dealerDocumentApprovalInsertDTO: [],
    };
    this.dataList.forEach(el => {
      if (el) {
        reqObj.dealerDocumentApprovalInsertDTO.push({
          dealerId: this.dealerId,
          dealerDocumentId: el?.dealerDocumentId,
          dealerApprovalId: el?.dealerApprovalId,
          dealerDocumentActionStatus: el?.dealerDocumentActionStatus,
          reason: el?.reason,
          isActive: true,
          createdUserId: this.loggedInUserData?.userId,
        });
      }
    });
    this.openDealerDeclineReason('Decline Reason', reqObj);
  }

  openDealerDeclineReason(header, data) {
    const ref = this.dialogService.open(DealerDocumentDeclineReasonComponent, {
      header: header,
      showHeader: false,
      baseZIndex: 1000,
      width: '450px',
      // height: '30vh',
      data: {
        row: data,
        module: ModulesBasedApiSuffix.DEALER,
        api: DealerModuleApiSuffixModels.DEALER_APPROVAL,
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.router.navigate(['/dealer/dealer-worklist']);
      }
    });
  }

  enableApproveBtn() {
    return this.dataList?.every(el => el?.dealerDocumentActionStatus == true);
  }

  enableDeclineBtn() {
    // const items = this.dataList.filter(el => ((el?.reason == undefined || el?.reason == null || el?.reason == '') && el?.dealerDocumentActionStatus == false) || (el?.dealerDocumentActionStatus == undefined
    //   || el?.dealerDocumentActionStatus == null));
    const items = this.dataList?.filter(el => el?.dealerDocumentActionStatus == false);
    return items?.every(el => (el?.reason != undefined && el?.reason != null && el?.reason != ''));
  }

  onSubmitAction() {
    if (!this.enableSubmitAction()) {
      return;
    }
    const message = `Are you sure want to final submit the dealer?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.onValidateDealerDocument();
      }
    });
  }

  onValidateDealerDocument() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_DOCUMENT_VALIDATION, null, false, prepareRequiredHttpParams({dealerId: this.dealerId, userId: this.loggedInUserData?.userId}))
    .subscribe((res: IApplicationResponse) => {
      if(res?.isSuccess && res?.statusCode == 200) {
        if(res?.resources?.isSuccess) {
          this.onSubmit();
        } else if(!res?.resources?.isSuccess) {
          this.openDialog(res?.resources?.responseMessage);
        }
      }
      this.rxjsService.setDialogOpenProperty(false);
    })
  }

  openDialog(message) {
    const dialogData = new ConfirmDialogModel("Alert", message);
    this.alertDialog = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
      showHeader: false,
      width: "450px",
      data: {...dialogData, isAlert: true, isClose: true},
      baseZIndex: 5000,
    });
    this.alertDialog.onClose.subscribe(dialogResult => {
      if(dialogResult) {
        return;
      }
    });
  }

  onSubmit() {
    const reqObj = {
      dealerId: this.dealerId,
      createdUserId: this.loggedInUserData?.userId,
    }
    this.isSubmitted = true;
    this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_APPROVAL_REQUEST, reqObj)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.onCRUDRequested('get');
        }
        this.isSubmitted = false;
      })
  }

  enableSubmitAction() {
    return this.dataList?.every(el => el?.approvalStatus?.toLowerCase() == null);
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion?.unsubscribe();
    }
  }
}
