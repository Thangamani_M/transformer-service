import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from '@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module';
import { MaterialModule } from '@app/shared/material.module';
import { DealerMaintenanceListComponent } from '@modules/dealer';
import { DealerAVSCheckModule } from '../dealer-avs-check/dealer-avs-check.module';
import { DealerCommentsHistoryModule } from '../dealer-comments-history/dealer-comments-history.module';
import { DealerCreditVettingModule } from '../dealer-credit-vetting/dealer-credit-vetting.module';
import { DealerDocumentDeclineReasonModule } from '../dealer-document-decline-reason/dealer-document-decline-reason.module';
import { DealerDocumentsAddEditModule } from '../dealer-documents-add-edit/dealer-documents-add-edit.module';
import { DealerPrincipleAddEditModule } from '../dealer-principle-add-edit/dealer-principle-add-edit.module';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';
@NgModule({
    declarations: [DealerMaintenanceListComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        DealerAVSCheckModule,
        DealerCreditVettingModule,
        DealerPrincipleAddEditModule,
        DealerCommentsHistoryModule,
        DealerDocumentDeclineReasonModule,
        DealerDocumentsAddEditModule,
        PrimengConfirmDialogPopupModule,
        RouterModule.forChild([
            { path: '', component: DealerMaintenanceListComponent, canActivate:[AuthGuard], data: { title: 'Dealer Maintenance' } },
        ])
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [],
    exports: [],
})
export class DealerMaintenanceListModule { }
