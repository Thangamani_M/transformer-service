import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { ConfirmDialogPopupComponent, countryCodes, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, destructureAfrigisObjectAddressComponents, formConfigs, IAfrigisAddressComponents, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setEmailValidator, setRequiredValidator, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from "@modules/dealer";
import { DealerBranchAddEditModel } from "@modules/dealer/models/dealer-branch.model";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, NewAddressPopupComponent, SalesModuleApiSuffixModels } from "@modules/sales";
import { TechnicalMgntModuleApiSuffixModels } from "@modules/technical-management/shared/enum.ts/technical.enum";
import { select, Store } from "@ngrx/store";
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


@Component({
  selector: 'app-dealer-branch-add-edit',
  templateUrl: './dealer-branch-add-edit-component.html',
  styleUrls: ['./dealer-branch-add-edit-component.scss']
})
export class DealerBranchAddEditComponent implements OnInit {

  primengTableConfigProperties: any;
  dealerId: any;
  viewable: boolean = true;
  userData: UserLogin;
  selectedTabIndex = 0;
  dealerBranchAddEditForm: FormGroup;
  startTodayDate = new Date();
  isLoading = false;
  addressList = [];
  checkMaxLengthAddress: boolean;
  inValid = false;
  accountTypeList = [];
  bankList = [];
  commissionDropDownList = [];
  warehouseDropDownList = [];
  storageLocationWarehouseDropDownList = [];
  bankBranchesList = [];
  techAreaList = [];
  titlesList = [];
  ispostalAddressLoading: boolean;
  postalAddressList = [];
  dsfMultipleList = [];
  physicalAddressList: any = [];
  postalAddressJsonAddress: any;
  inValidPostalAddress: boolean;
  checkMaxLengthPostalAddress: boolean;
  checkMaxLengthPhysicalAddress: boolean;
  inValidPhysicalAddress: boolean;
  showAddress: boolean;
  showPostalAddress: boolean;
  isPhysicalAddressLoading: boolean;
  dealerBranchId: string;
  params: any;
  formConfigs = formConfigs;
  dealerTypeId: any;
  referenceId: string;
  wareHouseId: any;
  locationId: any;
  countryCodes = countryCodes;
  isLegalEntity: boolean = false;
  isPerson: boolean = false;
  isLegalEntityPerson: boolean;
  techStockLocationNumber;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericWithSlash = new CustomDirectiveConfig({ isAnAlphaNumericWithSlash: true });
  comStartMonthValueName;
  bonusMonthValueName
  primengTableConfigPropertiesPageLevelAccess :any = {
    tableComponentConfigs:{
      tabsList:[
        {},{},{},{},{}
      ]
    }
  }
  dealerDetails:any = {}
  constructor(private snackbarService: SnackbarService, private dialog: MatDialog, private momentService: MomentService, private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.dealerId = (Object.keys(params['params']).length > 0) ? params['params']['dealerId'] : '';
      this.dealerBranchId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.dealerTypeId = (Object.keys(params['params']).length > 0) ? params['params']['dealerTypeId'] : '';
      this.referenceId = (Object.keys(params['params']).length > 0) ? params['params']['referenceId'] : '';
    });
    this.params = { dealerId: this.dealerId };

    this.primengTableConfigProperties = {
      tableCaption: this.dealerBranchId ? 'Update Branch' : 'Create Branch',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Maintenance', relativeRouterUrl: '/dealer/dealer-maintenance', }, { displayName: 'Dealer Branch Details', relativeRouterUrl: '/dealer/dealer-maintenance', queryParams: { tab: 2, id: this.dealerId, dealerTypeId: this.dealerTypeId } }, { displayName: 'Create Branch', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Branch',
            dataKey: 'branchId',
            enableBreadCrumb: true,
            enableAction: false,
            url: '',
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    if (this.referenceId) {
      this.primengTableConfigProperties.breadCrumbItems[1]['relativeRouterUrl'] = '';
      this.primengTableConfigProperties.breadCrumbItems[2]['queryParams'] = { tab: 2, id: this.dealerId, dealerTypeId: this.dealerTypeId, referenceId: this.referenceId, };
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createForm();
    this.getBankAccountTypeDropdown();
    this.getBankDropdown();
    this.getCommissionConfigDropdown();
    this.getTechArea();
    this.getDealerBranchDetails({ "dealerId": this.dealerId });
    this.getDealerDetails();
    this.getTitle();
    this.getDsfMultiple({ IsAll: true });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_MAINTENANCE]
      if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesPageLevelAccess, permission);
      this.primengTableConfigPropertiesPageLevelAccess = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
    });
    }

  createForm(dealerBranchAddEditModel?: DealerBranchAddEditModel) {
    let dealerTypeModel = new DealerBranchAddEditModel(dealerBranchAddEditModel);
    this.dealerBranchAddEditForm = this.formBuilder.group({});
    Object.keys(dealerTypeModel).forEach((key) => {
      if (dealerTypeModel[key] === 'object') {
        this.dealerBranchAddEditForm.addControl(key, new FormArray(dealerTypeModel[key]));
      } else {
        this.dealerBranchAddEditForm.addControl(key, new FormControl());
      }
    });
    this.dealerBranchAddEditForm.get('telePhoneNoCountryCode').disable();
    this.dealerBranchAddEditForm.get('phoneNoCountryCode').disable();
    this.dealerBranchAddEditForm.get('debtorCode').disable();
    this.dealerBranchAddEditForm.get('phoneNoCountryCode').setValue('+27');
    this.dealerBranchAddEditForm.get('telePhoneNoCountryCode').setValue('+27');
    this.dealerBranchAddEditForm = setRequiredValidator(this.dealerBranchAddEditForm, ["isLegalEntityPerson", "companyName", "branchName", "phoneNumber", "fullAddress",  "startDate", "commissionStartDate", "bonusMultipleStartDate", "bankId", "techAreaId", "dealerCategoryTypeId", "titleId", "firstName", "lastName", "bankAccountNumber", "accountTypeId", "bankBranchId", "commissionPercentage", "bonusMultiple", "dsfMultiple"]);
    this.dealerBranchAddEditForm = setEmailValidator(this.dealerBranchAddEditForm, ["emailAddress", "adminEmailAddress", "accountsEmailAddress", "technicalEmailAddress"]);
    //set any one value in default
    this.dealerBranchAddEditForm.get('isLegalEntityPerson').setValue(true);
    this.isLegalEntityPerson = true;
    // this.setisLegalEntityPerson(this.isLegalEntityPerson);
    if (this.referenceId) {
      this.dealerBranchAddEditForm.disable();
    }
    this.onValueChanges();
    this.onDealerBranchValueChanges();
  }

  onSelectPhysicalAddress(e) {
    this.onSelectedPhysicalAddressFromAutoComplete(e?.description, e, e?.seoid);
  }

  onSelectPostalAddress(e) {
    this.onSelectedAddressFromAutoComplete(e?.description, e, e?.seoid);
  }

  onSearchPhysicalAddress(val) {
    this.filterServiceTypeDetailsBySearchOptions(val?.query)
      .subscribe((response: any) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources != null) {
            if (response?.resources[0]?.resultNotFound) {
              this.setEmptyPhysicalAddress(response?.resources[0]?.description);
            } else {
              this.physicalAddressList = response.resources;
            }
            if (this.physicalAddressList.length == 0) {
              this.setEmptyPhysicalAddress();
            }
          } else {
            this.setEmptyPhysicalAddress();
          }
          this.ispostalAddressLoading = false;
        } else {
          this.physicalAddressList = [];
        }
      });
  }

  onSearchPostalAddress(val) {
    this.filterServiceTypeDetailsBySearchOptions(val?.query)
      .subscribe((response: any) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources != null) {
            // this.postalAddressList = response.resources;
            // if (this.postalAddressList.length == 0) {
            //   this.postalAddressList = [];
            //   this.inValidPostalAddress = true;
            //   this.dealerBranchAddEditForm.controls['latlang'].setValue('');
            // }
            //  this.postalAddressList = response.resources;
            if (response?.resources[0]?.resultNotFound) {
              this.setEmptyPostalAddress(response?.resources[0]?.description);
            } else {
              this.postalAddressList = response.resources;
            }
            if (this.postalAddressList.length == 0) {
              this.setEmptyPostalAddress();
            }
          } else {
            this.setEmptyPostalAddress();
          }
          this.ispostalAddressLoading = false;
        } else {
          this.postalAddressList = [];
        }
      });
  }

  onValueChanges() {
    this.dealerBranchAddEditForm
      .get("phoneNoCountryCode")
      .valueChanges.subscribe((phoneNoCountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(phoneNoCountryCode, "contact");
      });

    this.dealerBranchAddEditForm
      .get("telePhoneNoCountryCode")
      .valueChanges.subscribe((telePhoneNoCountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(telePhoneNoCountryCode, "contact");
      });

    this.dealerBranchAddEditForm
      .get("isLegalEntityPerson")
      .valueChanges.subscribe((val: string) => {
        this.setisLegalEntityPerson(val);
      });


    this.dealerBranchAddEditForm
      .get("phoneNumber")
      .valueChanges.subscribe((phoneNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dealerBranchAddEditForm.get("phoneNoCountryCode").value,
          "contact"
        );
      });

    this.dealerBranchAddEditForm
      .get("supportPhoneNumber")
      .valueChanges.subscribe((phoneNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dealerBranchAddEditForm.get("telePhoneNoCountryCode").value,
          "contact"
        );
      });
      this.dealerBranchAddEditForm
      .get("techAreaId")
      .valueChanges.subscribe((id: string) => {
        if(id) {
          this.onChangeTechArea(id);
        }
      });
  }
  setisLegalEntityPerson(val) {
    if (val == 'true' || val == true) {
      this.isLegalEntityPerson = true;
      this.dealerBranchAddEditForm = setRequiredValidator(this.dealerBranchAddEditForm, ["companyRegistrationNumber"]);
      this.dealerBranchAddEditForm.get('southAfricanId').clearValidators();
    }
    else {
      this.isLegalEntityPerson = false;
      this.dealerBranchAddEditForm = setRequiredValidator(this.dealerBranchAddEditForm, ["southAfricanId"]);
      this.dealerBranchAddEditForm.get('companyRegistrationNumber').clearValidators();
    }
  }
  setPhoneNumberLengthByCountryCode(countryCode: string, str: string) {
    switch (countryCode) {
      case "+27":
        if (str == "contact") {
          this.dealerBranchAddEditForm
            .get("phoneNumber")
            .setValidators([
              Validators.minLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
              Validators.maxLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
            ]);
        }

        break;
      default:
        if (str == "contact") {
          this.dealerBranchAddEditForm
            .get("phoneNumber")
            .setValidators([
              Validators.minLength(formConfigs.indianContactNumberMaxLength),
              Validators.maxLength(formConfigs.indianContactNumberMaxLength),
            ]);
        }
        break;
    }
  }
  onDealerBranchValueChanges() {

    // this.dealerBranchAddEditForm.get('fullAddress').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
    //   switchMap((val) => {
    //     if (val) {
    //       this.physicalAddressList = [];
    //       return this.filterServiceTypeDetailsBySearchOptions(val);
    //     } else {
    //       this.physicalAddressList = [];
    //       this.showPostalAddress = false;
    //     }
    //   })).subscribe((response: any) => {
    //     if (response.isSuccess) {
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //       if (response.resources != null) {
    //         this.showPostalAddress = true;

    //         if (response.resources.length)
    //           this.physicalAddressList = response.resources;

    //         if (this.physicalAddressList.length == 0) {

    //           this.physicalAddressList = [];
    //           this.inValidPostalAddress = true;
    //           this.dealerBranchAddEditForm.controls['latlang'].setValue('');
    //           this.showPostalAddress = false;
    //         }
    //         if(this.dealerBranchAddEditForm.get('isSameAsPhysicalAddress').value) {
    //           this.setSameAsPhysicalAddress();
    //         }
    //       } else {
    //         this.physicalAddressList = [];
    //         this.inValidPostalAddress = true;
    //         this.showPostalAddress = false;
    //       }
    //     }
    //   });


    // this.dealerBranchAddEditForm.get('postalFullAddress').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
    //   switchMap((val) => {
    //     if (val) {
    //       this.postalAddressList = [];
    //       return this.filterServiceTypeDetailsBySearchOptions(val);
    //     } else {
    //       this.postalAddressList = [];
    //       this.showPostalAddress = false;
    //     }
    //   })).subscribe((response: any) => {
    //     if (response.isSuccess) {
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //       if (response.resources != null) {
    //         this.showPostalAddress = true;
    //         this.postalAddressList = response.resources;

    //         if (this.postalAddressList.length == 0) {

    //           this.postalAddressList = [];
    //           this.inValidPostalAddress = true;
    //           this.dealerBranchAddEditForm.controls['latlang'].setValue('');
    //           this.showPostalAddress = false;
    //         }
    //       } else {
    //         this.postalAddressList = [];
    //         this.inValidPostalAddress = true;
    //         this.showPostalAddress = false;
    //       }
    //     }
    //   });


    this.dealerBranchAddEditForm.get('isSameAsPhysicalAddress').valueChanges.pipe(debounceTime(800), distinctUntilChanged())
      .subscribe((response: any) => {
        if (response == true) {
          let postalAddress1 = this.dealerBranchAddEditForm.get('buildingNo').value + ' ' + this.dealerBranchAddEditForm.get('buildingName').value +' ' + this.dealerBranchAddEditForm.get('streetNo').value;
          let postalAddress2 = this.dealerBranchAddEditForm.get('streetName').value + ' ' +this.dealerBranchAddEditForm.get('suburbName').value;
          let postalAddress3 = this.dealerBranchAddEditForm.get('cityName').value;
          let postalAddress4 = this.dealerBranchAddEditForm.get('provinceName').value;
          let postalAddress5 = this.dealerBranchAddEditForm.get('postalCode').value;
          this.dealerBranchAddEditForm.get('postalAddress1').setValue(postalAddress1);
          this.dealerBranchAddEditForm.get('postalAddress2').setValue(postalAddress2);
          this.dealerBranchAddEditForm.get('postalAddress3').setValue(postalAddress3);
          this.dealerBranchAddEditForm.get('postalAddress4').setValue(postalAddress4);
          this.dealerBranchAddEditForm.get('postalAddress5').setValue(postalAddress5);
          // this.postalAddressList = this.physicalAddressList;
          // this.setSameAsPhysicalAddress();
          // this.enableDisablePostalAddress(response);
        } else if (response == false) {
          // this.enableDisablePostalAddress(response);
        }
      });

      this.dealerBranchAddEditForm.get('isSameAsDealerAddress').valueChanges.subscribe((response: any) => {

        if(response){
          this.dealerBranchAddEditForm.get("fullAddress").setValue({ description: this.dealerDetails.formatedAddress });
          this.dealerBranchAddEditForm.get("buildingNo").setValue(this.dealerDetails?.buildingNo);
          this.dealerBranchAddEditForm.get("buildingName").setValue(this.dealerDetails?.buildingName);
          this.dealerBranchAddEditForm.get("streetNo").setValue(this.dealerDetails.streetNo);
          this.dealerBranchAddEditForm.get("streetName").setValue(this.dealerDetails.streetName);
          this.dealerBranchAddEditForm.get("suburbName").setValue(this.dealerDetails.suburbName);
          this.dealerBranchAddEditForm.get("cityName").setValue(this.dealerDetails.cityName);
          this.dealerBranchAddEditForm.get("provinceName").setValue(this.dealerDetails.provinceName);
          this.dealerBranchAddEditForm.get("postalCode").setValue(this.dealerDetails.postalCode);
          this.dealerBranchAddEditForm.get("physicalAddressId").setValue(this.dealerDetails.physicalAddressId);

        }
      })
  }
  setSameAsPhysicalAddress() {
    if (this.dealerBranchAddEditForm.get('isSameAsPhysicalAddress').value) {
      this.dealerBranchAddEditForm.patchValue({
        postalSuburbName: this.dealerBranchAddEditForm.get('suburbName').value, postalCityName: this.dealerBranchAddEditForm.get('cityName').value,
        postalProvinceName: this.dealerBranchAddEditForm.get('provinceName').value, postalPostalCode: this.dealerBranchAddEditForm.get('postalCode').value,
        postalStreetName: this.dealerBranchAddEditForm.get('streetName').value, postalStreetNo: this.dealerBranchAddEditForm.get('streetNo').value,
        postalBuildingName: this.dealerBranchAddEditForm.get('buildingName').value, postalBuildingNo: this.dealerBranchAddEditForm.get('buildingNo').value,
        postalLatitude: this.dealerBranchAddEditForm.get('latitude').value, postalLongitude: this.dealerBranchAddEditForm.get('longitude').value,
        postalseoId: this.dealerBranchAddEditForm.get('seoId').value, postalJsonObject: this.dealerBranchAddEditForm.get('jsonObject').value,
      }, { emitEvent: false })
      this.dealerBranchAddEditForm.get('postalFullAddress').setValue(this.dealerBranchAddEditForm.get('fullAddress').value, { emitEvent: false });
      this.dealerBranchAddEditForm.get('postalFullAddress').disable();
    } else {
      this.dealerBranchAddEditForm.get('postalFullAddress').enable();
    }
  }
  getDealerDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER,
      this.dealerId,
      false, null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.dealerDetails =  data.resources
        if (data.resources.companyName) {
          this.dealerBranchAddEditForm.get('companyName').setValue(data.resources.companyName);
          this.dealerBranchAddEditForm.get('companyName').disable();
        }
        if (data.resources.dealerIIPCode) {
          this.dealerBranchAddEditForm.get('dealerCode').setValue(data.resources.dealerIIPCode);
          this.dealerBranchAddEditForm.get('dealerCode').disable();
        }
        if (!this.referenceId && (data?.resources?.approvalStatus?.toLowerCase() != 'approved' || !data?.resources?.approvalStatus)) {
          this.viewable = false;
        } else if (this.dealerBranchId && data?.resources?.approvalStatus?.toLowerCase() == 'approved') {
          // this.dealerBranchAddEditForm.disable();
          this.viewable = false;
        }
      }

    });
  }
  getDsfMultiple(params) {
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.UX_DSF_CONFIG,
      undefined,
      false, prepareRequiredHttpParams(params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.dsfMultipleList = data.resources;
      }

    });
  }
  getTitle() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_TITLES,
      undefined,
      false, prepareRequiredHttpParams(this.params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.titlesList = data.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }

    });
  }
  getBankAccountTypeDropdown() {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.UX_ACCOUNT_TYPES).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
          this.accountTypeList = data.resources;
        }

      });
  }
  getBankDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_BANKS,
      undefined,
      false, prepareRequiredHttpParams(this.params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.bankList = data.resources;
      }

    });
  }
  getCommissionConfigDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CATEGORY,
      undefined,
      false, prepareRequiredHttpParams(this.params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.commissionDropDownList = data.resources;
      }

    });
  }

  getStorageLocationDropdown() {
    //IsAll=true
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION_WAREHOUSE,
      undefined,
      false, prepareRequiredHttpParams(this.params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.storageLocationWarehouseDropDownList = data.resources;
      }

    });
  }
  getBankBranches(otherParams) {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_BANK_BRANCHES,
      undefined,
      false, prepareRequiredHttpParams(otherParams)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.bankBranchesList = data.resources;

        if (this.bankBranchesList && this.bankBranchesList.length == 0)
          this.snackbarService.openSnackbar("Please try selecting bank which has branch", ResponseMessageTypes.WARNING);

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  getTechArea() {

    this.params = { IsOverhead: true, IsDescription: true }
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_AREAS,
      undefined,
      false, prepareRequiredHttpParams(this.params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.techAreaList = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  getDealerBranchDetails(otherParams) {
    if (this.dealerBranchId) {
      this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_BRANCH, this.dealerBranchId, false,
        prepareRequiredHttpParams(otherParams))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.setValue(response.resources);
          }
        });
    }
  }
  onchangeCommision(values, otherParams?: any, type?: any) {
    otherParams = { dealerTypeId: this.dealerTypeId, dealerCategoryTypeId: values }
    if (values) {

      this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_BRANCH_CATEGORY, null, false,
        prepareRequiredHttpParams(otherParams))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            if (response.resources.commission == null || response.resources.monthValueName == null || response.resources.dsfValue == null)
              this.snackbarService.openSnackbar("Please try selecting any other Commission Type ", ResponseMessageTypes.WARNING);

            this.dealerBranchAddEditForm.get("commissionPercentage").setValue(response.resources.commission);
            this.dealerBranchAddEditForm.get("bonusMultiple").setValue(response.resources.monthValueName);
            this.dealerBranchAddEditForm.get("dsfMultiple").setValue(response.resources.dsfValue);
            if (type == 'manual')
              this.dealerBranchAddEditForm.get("bonusMultiple").setValue(0);

            if (type == 'manual' && response.resources.monthValueName) {
              this.bonusMonthValueName = parseInt(response.resources.monthValueName);
              let formValue = this.dealerBranchAddEditForm.getRawValue();
              if (formValue.startDate) {
                formValue.startDate = (formValue.startDate && formValue.startDate != null) ? this.momentService.toMoment(formValue.startDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
                var newDate = new Date(formValue.startDate);
                newDate.setMonth(newDate.getMonth() + this.bonusMonthValueName)
                this.dealerBranchAddEditForm.get("bonusMultipleStartDate").setValue(newDate);
              }
            }
            if (type == 'manual' && response.resources.comStartMonthValueName) {
              this.comStartMonthValueName = parseInt(response.resources.comStartMonthValueName);
              let formValue = this.dealerBranchAddEditForm.getRawValue();
              if (formValue.startDate) {
                formValue.startDate = (formValue.startDate && formValue.startDate != null) ? this.momentService.toMoment(formValue.startDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
                var newDate1 = new Date(formValue.startDate);
                newDate1.setMonth(newDate1.getMonth() + this.comStartMonthValueName)

                this.dealerBranchAddEditForm.get("commissionStartDate").setValue(newDate1);
              }
            }
          } else {
            this.snackbarService.openSnackbar("Please try selecting any other Commission Type ", ResponseMessageTypes.WARNING);
            this.dealerBranchAddEditForm.get("commissionPercentage").setValue(null);
            this.dealerBranchAddEditForm.get("bonusMultiple").setValue(null);
            this.dealerBranchAddEditForm.get("dsfMultiple").setValue(null);
            this.comStartMonthValueName = 0;
            this.bonusMonthValueName = 0;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }
  param: any
  onChangeTechArea(techId) {
    if (techId && this.dealerBranchId) {
      this.onValidateTechArea(techId);
    } else if (techId) {
      this.onLoadTechAreaDetails(techId);
    }
  }

  onValidateTechArea(techId) {
    if(techId) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(
        ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_BRANCH_TECHAREA_VALIDATION,
        undefined,
        false, prepareRequiredHttpParams({ dealerBranchId: this.dealerBranchId, techAreaId: techId })
      ).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
          if(data.resources?.isSuccess) {
            this.onLoadTechAreaDetails(techId);
          } else if(!data.resources?.isSuccess) {
            const dialogData = {
              title: 'Alert',
              message: data.resources.validateMessage,
              isClose: true,
              isAlert: true,
              msgCenter: true,
            }
            this.openConfirmDialog(dialogData);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onLoadTechAreaDetails(techId) {
    if(techId) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        DealerModuleApiSuffixModels.DEALER_TECH_AREA_DETAILS,
        undefined,
        false, prepareRequiredHttpParams({ techAreaId: techId })
      ).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200 && data.resources) {
          let techResult = data.resources;
          this.dealerBranchAddEditForm.get('warehouseId').setValue(techResult.warehouseName);
          this.dealerBranchAddEditForm.get('locationId').setValue(techResult.storageLocationName);
          this.wareHouseId = techResult.warehouseId;
          this.locationId = techResult.storageLocationId;
        } else {
          this.dealerBranchAddEditForm.get('warehouseId').setValue(null);
          this.dealerBranchAddEditForm.get('locationId').setValue(null);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  openConfirmDialog(dialogData) {
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "500px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) {
        this.getDealerBranchDetails({ "dealerId": this.dealerId });
      } else {
        this.onLoadTechAreaDetails(this.dealerBranchAddEditForm.get("techAreaId").value);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  bankParam: any
  onChangeBank(event) {
    let bankId = event ? event : null;
    if (bankId) {
      this.bankParam = { ...{ bankId: bankId } };
      this.getBankBranches(this.bankParam);
      this.dealerBranchAddEditForm.get("bankBranchCode").setValue(null);
    }
  }
  bankBranchId: any; filterData: any;
  onChangeBankBranch(events) {
    this.bankBranchId = events;
    if (this.bankBranchesList && this.bankBranchesList.length > 0) {
      this.filterData = this.bankBranchesList.filter(x => x.id == this.bankBranchId);
      if (this.filterData && this.filterData.length > 0)
        this.dealerBranchAddEditForm.get("bankBranchCode").setValue(this.filterData[0].bankBranchCode);
    }
  }
  setValue(response) {
    if (response.dealerCategoryTypeId) {
      this.onchangeCommision(response.dealerCategoryTypeId, null, 'set');
    }
    if (response.bankId) {
      this.onChangeBank(response.bankId);
    }
    if (response.techAreaId) {
      this.onLoadTechAreaDetails(response.techAreaId);
    }
    // this.dealerBranchAddEditForm.get("dealerCode").setValue(this.dealerCode);
    this.dealerBranchAddEditForm.get("bankBranchId").setValue(response.bankBranchId);
    this.dealerBranchAddEditForm.get("dealerBranchCode").setValue(response.dealerBranchCode);
    this.dealerBranchAddEditForm.get("companyName").setValue(response.companyName);
    this.dealerBranchAddEditForm.get("branchName").setValue(response.branchName);
    this.dealerBranchAddEditForm.get("phoneNumber").setValue(response.phoneNumber);
    this.dealerBranchAddEditForm.get("accountTypeId").setValue(response.accountTypeId);
    this.dealerBranchAddEditForm.get("accountsEmailAddress").setValue(response.accountsEmailAddress);
    this.dealerBranchAddEditForm.get("adminEmailAddress").setValue(response.adminEmailAddress);
    this.dealerBranchAddEditForm.get("technicalEmailAddress").setValue(response.technicalEmailAddress);
    this.dealerBranchAddEditForm.get("emailAddress").setValue(response.emailAddress);
    this.dealerBranchAddEditForm.get("managerName").setValue(response.managerName);
    this.dealerBranchAddEditForm.get("managerSurname").setValue(response.managerSurname);
    this.dealerBranchAddEditForm.get("supportPhoneNumber").setValue(response.supportPhoneNumber);
    this.dealerBranchAddEditForm.get("isAuthorizedToRemoveRS").setValue(response.isAuthorizedToRemoveRS);
    this.dealerBranchAddEditForm.get("dealerCategoryTypeId").setValue(response.dealerCategoryTypeId);
    this.dealerBranchAddEditForm.get("bonusMultiple").setValue(response.bonusMultiple);
    this.dealerBranchAddEditForm.get("debtorCode").setValue(response.debtorCode);
    this.dealerBranchAddEditForm.get("sapVendorNumber").setValue(response.sapVendorNumber);
    // this.dealerBranchAddEditForm.get("bankAccountHolderName").setValue(response.bankAccountHolderName);
    this.dealerBranchAddEditForm.get("accountTypeId").setValue(response.accountTypeId);
    this.dealerBranchAddEditForm.get("bankAccountNumber").setValue(response.accountNo);
    this.dealerBranchAddEditForm.get("bankId").setValue(response.bankId);
    this.dealerBranchAddEditForm.get("bankBranchId").setValue(response.bankBranchId);
    // this.dealerBranchAddEditForm.get("isBankStatus").setValue(response.isBankStatus);
    this.dealerBranchAddEditForm.get("bankBranchCode").setValue(response.bankBranchCode);
    this.dealerBranchAddEditForm.get("locationId").setValue(response.storageLocationName);
    this.dealerBranchAddEditForm.get("techAreaId").setValue(response.techAreaId, {emitEvent: false});
    this.dealerBranchAddEditForm.get("techStockLocationId").setValue(response.techStockLocationId);
    this.dealerBranchAddEditForm.get("warehouseId").setValue(response.warehouseName);
    this.dealerBranchAddEditForm.get("fullAddress").setValue({ description: response.formatedAddress });
    this.dealerBranchAddEditForm.get("buildingNo").setValue(response?.buildingNo);
    this.dealerBranchAddEditForm.get("buildingName").setValue(response?.buildingName);
    this.dealerBranchAddEditForm.get("streetNo").setValue(response.streetNo);
    this.dealerBranchAddEditForm.get("streetName").setValue(response.streetName);
    this.dealerBranchAddEditForm.get("suburbName").setValue(response.suburbName);
    this.dealerBranchAddEditForm.get("cityName").setValue(response.cityName);
    this.dealerBranchAddEditForm.get("provinceName").setValue(response.provinceName);
    this.dealerBranchAddEditForm.get("postalCode").setValue(response.postalCode);
    this.dealerBranchAddEditForm.get("dsfMultiple").setValue(response.dsfMultiple);
    this.dealerBranchAddEditForm.get("isSameAsDealerAddress").setValue(response?.isSameAsDealerAddress);
    this.dealerBranchAddEditForm.get("commissionStartDate").setValue(new Date(response.commissionStartDate));
    this.dealerBranchAddEditForm.get("startDate").setValue(new Date(response.startDate));
    this.dealerBranchAddEditForm.get("bonusMultipleStartDate").setValue(new Date(response.bonusMultipleStartDate));
    // this.dealerBranchAddEditForm.get("postalFullAddress").setValue({ description: response.postalFormatedAddress });
    // this.dealerBranchAddEditForm.get("postalBuildingNo").setValue(response?.postalBuildingNo);
    // this.dealerBranchAddEditForm.get("postalBuildingName").setValue(response?.postalBuildingName);
    // this.dealerBranchAddEditForm.get("postalStreetNo").setValue(response.postalStreetNo);
    // this.dealerBranchAddEditForm.get("postalStreetName").setValue(response.postalStreetName);
    // this.dealerBranchAddEditForm.get("postalSuburbName").setValue(response.postalSuburbName);
    // this.dealerBranchAddEditForm.get("postalCityName").setValue(response.postalCityName);
    // this.dealerBranchAddEditForm.get("postalProvinceName").setValue(response.postalProvinceName);
    // this.dealerBranchAddEditForm.get("postalPostalCode").setValue(response.postalPostalCode);
    this.dealerBranchAddEditForm.get("isSameAsPhysicalAddress").setValue(response.isSameAsPhysicalAddress);

    this.dealerBranchAddEditForm.get("isLegalEntity").setValue(response.isLegalEntity);
    this.dealerBranchAddEditForm.get("isPerson").setValue(response.isPerson);
    this.dealerBranchAddEditForm.get("firstName").setValue(response.firstName);
    this.dealerBranchAddEditForm.get("lastName").setValue(response.lastName);
    this.dealerBranchAddEditForm.get("companyRegistrationNumber").setValue(response.companyRegistrationNumber);
    this.dealerBranchAddEditForm.get("southAfricanId").setValue(response.southAfricanId);
    this.dealerBranchAddEditForm.get("titleId").setValue(response.titleId);

    this.dealerBranchAddEditForm.get("postalAddressId").setValue(response.postalAddressId);
    this.dealerBranchAddEditForm.get("physicalAddressId").setValue(response.physicalAddressId);
    this.dealerBranchAddEditForm.get("latitude").setValue(response.latitude);
    this.dealerBranchAddEditForm.get("longitude").setValue(response.longitude);
    this.dealerBranchAddEditForm.get("postalLatitude").setValue(response.postalLatitude);
    this.dealerBranchAddEditForm.get("postalLongitude").setValue(response.postalLongitude);

    this.dealerBranchAddEditForm.get('postalAddress1').setValue(response.postalAddress1);
    this.dealerBranchAddEditForm.get('postalAddress2').setValue(response.postalAddress2);
    this.dealerBranchAddEditForm.get('postalAddress3').setValue(response.postalAddress3);
    this.dealerBranchAddEditForm.get('postalAddress4').setValue(response.postalAddress4);
    this.dealerBranchAddEditForm.get('postalAddress5').setValue(response.postalAddress5);
    this.techStockLocationNumber = response.techStockLocationNumber;
    if (response.isLegalEntity) {
      this.dealerBranchAddEditForm.get("isLegalEntityPerson").setValue(true);
      this.setisLegalEntityPerson(true);
    }
    else {
      this.dealerBranchAddEditForm.get("isLegalEntityPerson").setValue(false);
      this.setisLegalEntityPerson(false);
    }

  }
  onSelectedItemOption(
    isSelected: boolean,
    type: string,
    selectedObject: object
  ): void {

  }


  enableDisablePostalAddress(response) {
    if (response == true) {
      this.dealerBranchAddEditForm.get('postalFullAddress').disable({ emitEvent: false });
      this.dealerBranchAddEditForm.get('postalStreetNo').disable();
      this.dealerBranchAddEditForm.get('postalBuildingName').disable();
      this.dealerBranchAddEditForm.get('postalBuildingNo').disable();
    } else if (response == false) {
      this.dealerBranchAddEditForm.get('postalFullAddress').enable({ emitEvent: false });
      this.dealerBranchAddEditForm.get('postalStreetNo').enable();
      this.dealerBranchAddEditForm.get('postalBuildingName').enable();
      this.dealerBranchAddEditForm.get('postalBuildingNo').enable();
    }
  }
  filterServiceTypeDetailsBySearchOptions(searchValue?: string): Observable<IApplicationResponse> | any {
    this.checkMaxLengthPhysicalAddress = false;
    this.checkMaxLengthPostalAddress = false;
    if (searchValue.length < 3) {
      this.checkMaxLengthPhysicalAddress = true;
      this.checkMaxLengthPostalAddress = true;
      return of([]);
    } else {
      return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_ADDRESS_FROM_AFRIGIS, null, true, prepareGetRequestHttpParams(null, null, {
        searchtext: searchValue,
        IsAfrigisSearch: true
      }), 1)
    }
  }
  setFormValidators() {
    this.dealerBranchAddEditForm = setRequiredValidator(this.dealerBranchAddEditForm,
      []);
  }

  onSelectedPhysicalAddressFromAutoComplete(value, address, seoid) {
    let afrigisAddressComponents: IAfrigisAddressComponents;
    this.inValidPostalAddress = false;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
      prepareRequiredHttpParams({ seoid: seoid }), 1)
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.resources) {
          this.postalAddressJsonAddress = JSON.stringify(res.resources);
          this.physicalAddressList = res.resources.addressDetails[0].formatted_address.split(',');
          // res.resources.addressDetails[0].formatted_address ? res.resources.addressDetails[0].formatted_address : [];
        }
        let latLong_details = res.resources.addressDetails[0].geometry;
        this.dealerBranchAddEditForm.patchValue({
          buildingNo: '', buildingName: '', streetNo: '', streetName: '', suburbName: '', cityName: '',
          provinceId: '', postalCode: '', latitude: '', longitude: '', seoId: '', estateName: '',
          estateStreetNo: '', estateStreetName: '', jsonObject: '',
        })
        if (latLong_details) {
          // this.dealerMaintenanceAddEditForm.controls['locationPin'].setValue(latLong_details.location.lat + ',' + latLong_details.location.lng);
          this.dealerBranchAddEditForm.controls['latitude'].setValue(latLong_details.location.lat);
          this.dealerBranchAddEditForm.controls['longitude'].setValue(latLong_details.location.lng);
        }
        this.dealerBranchAddEditForm.get('seoId').setValue(res.resources.addressDetails[0].seoid);
        afrigisAddressComponents = destructureAfrigisObjectAddressComponents(res.resources.addressDetails[0].address_components);
        this.dealerBranchAddEditForm.patchValue({
          suburbName: afrigisAddressComponents.suburbName, cityName: afrigisAddressComponents.cityName,
          provinceName: afrigisAddressComponents.provinceName, postalCode: afrigisAddressComponents.postalCode,
          streetName: afrigisAddressComponents.streetName, streetNo: afrigisAddressComponents.streetNo,
          buildingName: afrigisAddressComponents.buildingName, buildingNo: afrigisAddressComponents.buildingNo,
          jsonObject: JSON.stringify(res.resources),
        })
      })
  }
  onSelectedAddressFromAutoComplete(value, address, seoid) {
    let afrigisAddressComponents: IAfrigisAddressComponents;
    this.inValidPostalAddress = false;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
      prepareRequiredHttpParams({ seoid: seoid }), 1)
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.resources) {
          this.postalAddressJsonAddress = JSON.stringify(res.resources);
          this.postalAddressList = res.resources.addressDetails[0].formatted_address.split(',');
        }
        let latLong_details = res.resources.addressDetails[0].geometry;
        this.dealerBranchAddEditForm.patchValue({
          postalBuildingNo: '', postalBuildingName: '', postalStreetNo: '', postalStreetName: '', postalSuburbName: '', postalCityName: '',
          postalProvinceName: '', postalPostalCode: '', postalLatitude: '', postalLongitude: '', postalseoId: '', postalEstateName: '',
          postalEstateStreetNo: '', postalEstateStreetName: '', postalJsonObject: '',
        })
        if (latLong_details) {
          // this.dealerMaintenanceAddEditForm.controls['locationPin'].setValue(latLong_details.location.lat + ',' + latLong_details.location.lng);
          this.dealerBranchAddEditForm.controls['postalLatitude'].setValue(latLong_details.location.lat);
          this.dealerBranchAddEditForm.controls['postalLongitude'].setValue(latLong_details.location.lng);
        }
        this.dealerBranchAddEditForm.get('postalseoId').setValue(res.resources.addressDetails[0].seoid);
        afrigisAddressComponents = destructureAfrigisObjectAddressComponents(res.resources.addressDetails[0].address_components);
        this.dealerBranchAddEditForm.patchValue({
          postalSuburbName: afrigisAddressComponents.suburbName, postalCityName: afrigisAddressComponents.cityName,
          postalProvinceName: afrigisAddressComponents.provinceName, postalPostalCode: afrigisAddressComponents.postalCode,
          postalStreetName: afrigisAddressComponents.streetName, postalStreetNo: afrigisAddressComponents.streetNo,
          postalBuildingName: afrigisAddressComponents.buildingName, postalBuildingNo: afrigisAddressComponents.buildingNo,
          postalJsonObject: JSON.stringify(res.resources),
        })
      })
  }

  setEmptyPhysicalAddress(desc = '') {
    this.physicalAddressList = [];
    this.inValidPhysicalAddress = true;
    this.dealerBranchAddEditForm.get('seoId').setValue('');
    this.dealerBranchAddEditForm.get('latitude').setValue('');
    this.dealerBranchAddEditForm.get('longitude').setValue('');
    this.dealerBranchAddEditForm.get('jsonObject').setValue('');
    if (desc) {
      this.setEmptyAddressMsg(desc);
    }
  }

  setEmptyPostalAddress(desc = '') {
    this.postalAddressList = [];
    this.inValidPostalAddress = true;
    this.dealerBranchAddEditForm.get('postalseoId').setValue('');
    this.dealerBranchAddEditForm.get('postalLatitude').setValue('');
    this.dealerBranchAddEditForm.get('postalLongitude').setValue('');
    this.dealerBranchAddEditForm.get('postalJsonObject').setValue('');
    if (desc) {
      this.setEmptyAddressMsg(desc);
    }
  }

  setEmptyAddressMsg(desc) {
    this.snackbarService.openSnackbar(desc, ResponseMessageTypes.ERROR);
    return;
  }


  openAddressPopup(addType?) {
    const data = {
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId,
      isShowAddressType: false, mobile2: null, officeNo: null, mobile1: null, premisesNo: null,
      formatedAddress: addType == 'physical' ? this.dealerBranchAddEditForm.get('fullAddress').value?.description : this.dealerBranchAddEditForm.get('postalFullAddress').value?.description,
      isAddressType: false, isAddressExtra: false, isHideEstateAddres: true,
    }
    const dialogReff = this.dialog.open(NewAddressPopupComponent, {
      width: "750px",
      disableClose: true,
      data: data,
    });

    dialogReff.afterClosed().subscribe((result) => { });
    dialogReff.componentInstance.outputData.subscribe((ele) => {
      if (ele) {
        const fullAddress = ele.streetNo + ',' + ele.streetName + ',' + ele.suburbName + ',' + ele.cityName + ',' + ele.provinceName + ',' + ele.postalCode;
        if (addType == 'physical') {
          this.dealerBranchAddEditForm.get("fullAddress").setValue({ description: fullAddress });
          this.dealerBranchAddEditForm.get("buildingNo").setValue(ele?.buildingNo);
          this.dealerBranchAddEditForm.get("buildingName").setValue(ele?.buildingName);
          this.dealerBranchAddEditForm.get("streetNo").setValue(ele.streetNo);
          this.dealerBranchAddEditForm.get("streetName").setValue(ele.streetName);
          this.dealerBranchAddEditForm.get("suburbName").setValue(ele.suburbName);
          this.dealerBranchAddEditForm.get("cityName").setValue(ele.cityName);
          this.dealerBranchAddEditForm.get("provinceName").setValue(ele.provinceName);
          this.dealerBranchAddEditForm.get("postalCode").setValue(ele.postalCode);
          this.dealerBranchAddEditForm.get("latitude").setValue('');
          this.dealerBranchAddEditForm.get("longitude").setValue('');
          this.dealerBranchAddEditForm.get("seoId").setValue('');
          this.dealerBranchAddEditForm.get("jsonObject").setValue('');
          this.dealerBranchAddEditForm.get("estateName").setValue(ele.estateName);
          this.dealerBranchAddEditForm.get("estateStreetNo").setValue(ele.estateStreetNo);
          this.dealerBranchAddEditForm.get("estateStreetName").setValue(ele.estateStreetName);
        } else {
          this.dealerBranchAddEditForm.get("postalFullAddress").setValue({ description: fullAddress });
          this.dealerBranchAddEditForm.get("postalBuildingNo").setValue(ele?.buildingNo);
          this.dealerBranchAddEditForm.get("postalBuildingName").setValue(ele?.buildingName);
          this.dealerBranchAddEditForm.get("postalStreetNo").setValue(ele.streetNo);
          this.dealerBranchAddEditForm.get("postalStreetName").setValue(ele.streetName);
          this.dealerBranchAddEditForm.get("postalSuburbName").setValue(ele.suburbName);
          this.dealerBranchAddEditForm.get("postalCityName").setValue(ele.cityName);
          this.dealerBranchAddEditForm.get("postalProvinceName").setValue(ele.provinceName);
          this.dealerBranchAddEditForm.get("postalPostalCode").setValue(ele.postalCode);
          this.dealerBranchAddEditForm.get("postalLatitude").setValue('');
          this.dealerBranchAddEditForm.get("postalLongitude").setValue('');
          this.dealerBranchAddEditForm.get("postalseoId").setValue('');
          this.dealerBranchAddEditForm.get("postalJsonObject").setValue('');
          this.dealerBranchAddEditForm.get("postalEstateName").setValue(ele.estateName);
          this.dealerBranchAddEditForm.get("postalEstateStreetNo").setValue(ele.estateStreetNo);
          this.dealerBranchAddEditForm.get("postalEstateStreetName").setValue(ele.estateStreetName);
        }

      }
    });
  }

  onSubmit() {
    if (this.dealerBranchAddEditForm.invalid) {
      this.dealerBranchAddEditForm.markAllAsTouched();
      return;
    }

    let formValue = this.dealerBranchAddEditForm.getRawValue();
    formValue.startDate = (formValue.startDate && formValue.startDate != null) ? this.momentService.toMoment(formValue.startDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    formValue.commissionStartDate = (formValue.commissionStartDate && formValue.commissionStartDate != null) ? this.momentService.toMoment(formValue.commissionStartDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    formValue.bonusMultipleStartDate = (formValue.bonusMultipleStartDate && formValue.bonusMultipleStartDate != null) ? this.momentService.toMoment(formValue.bonusMultipleStartDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;

    let finalObject = {
      dealerBranchId: this.dealerBranchId ? this.dealerBranchId : null,
      dealerId: this.dealerId,
      startDate: formValue.startDate,
      companyName: formValue.companyName,
      branchName: formValue.branchName,
      phoneNumber: formValue.phoneNumber,
      dealerBranchPhysicalAddress: {
        addressId: formValue.physicalAddressId,
        buildingName: formValue.buildingName,
        buildingNo: formValue.buildingNo,
        streetNo: formValue.streetNo,
        streetName: formValue.streetName,
        estateName: formValue.estateName,
        estateStreetNo: formValue.estateStreetNo,
        estateStreetName: formValue.estateStreetName,
        formatedAddress: formValue.fullAddress,
        jsonObject: formValue.jsonObject,
        seoId: formValue.seoId,
        postalCode: formValue.postalCode,
        latitude: formValue.latitude,
        longitude: formValue.longitude,
        addressConfidentLevelId: formValue.addressConfidentLevelId ? formValue.addressConfidentLevelId : null,
        isAddressComplex: formValue.isAddressComplex ? formValue.isAddressComplex : false,
        isAddressExtra: formValue.isAddressExtra ? formValue.isAddressExtra : false,
        afriGISAddressId: formValue.afriGISAddressId,
        suburbName: formValue.suburbName,
        cityName: formValue.cityName,
        provinceName: formValue.provinceName,
      },
      isSameAsPhysicalAddress: formValue.isSameAsPhysicalAddress ? true : false,
      dealerBranchPostalAddress: {
        addressId: formValue.postalAddressId,
        buildingName: formValue.postalBuildingName,
        buildingNo: formValue.postalBuildingNo,
        streetNo: formValue.postalStreetNo,
        streetName: formValue.postalStreetName,
        estateName: formValue.postalEstateName,
        estateStreetNo: formValue.postalEstateStreetNo,
        estateStreetName: formValue.postalEstateStreetName,
        formatedAddress: formValue.postalFullAddress,
        jsonObject: formValue.postalJsonObject,
        seoId: formValue.postalseoId,
        postalCode: formValue.postalCode,
        latitude: formValue.postalLatitude,
        longitude: formValue.postalLongitude,
        addressConfidentLevelId: formValue.postalAddressConfidentLevelId ? formValue.postalAddressConfidentLevelId : null,
        isAddressComplex: formValue.postalIsAddressComplex ? formValue.postalIsAddressComplex : false,
        isAddressExtra: formValue.postalIsAddressExtra ? formValue.postalIsAddressExtra : false,
        afriGISAddressId: formValue.postalAfriGISAddressId,
        suburbName: formValue.postalSuburbName,
        cityName: formValue.postalCityName,
        provinceName: formValue.postalProvinceName,
      },
      emailAddress: formValue.emailAddress,
      adminEmailAddress: formValue.adminEmailAddress,
      accountsEmailAddress: formValue.accountsEmailAddress,
      technicalEmailAddress: formValue.technicalEmailAddress,
      managerName: formValue.managerName,
      managerSurname: formValue.managerSurname,
      supportPhoneNumber: formValue.supportPhoneNumber,
      isAuthorizedToRemoveRS: formValue.isAuthorizedToRemoveRS ? true : false,
      dealerCategoryTypeId: formValue.dealerCategoryTypeId,
      commissionStartDate: formValue.commissionStartDate,
      bonusMultipleStartDate: formValue.bonusMultipleStartDate,
      bonusMultiple: formValue.bonusMultiple,
      debtorCode: formValue.debtorCode,
      sapVendorNumber: formValue.sapVendorNumber,
      dsfMultiple: formValue.dsfMultiple,
      // bankAccountHolderName: formValue.bankAccountHolderName,
      isLegalEntity: formValue.isLegalEntityPerson.toString() == 'true' ? true : false,
      isPerson: formValue.isLegalEntityPerson.toString() == 'false' ? true : false,
      firstName: formValue.firstName,
      lastName: formValue.lastName,
      companyRegistrationNumber: formValue.companyRegistrationNumber,
      southAfricanId: formValue.southAfricanId,
      titleId: Number(formValue.titleId),
      accountTypeId: formValue.accountTypeId,
      accountNo: formValue.bankAccountNumber,
      bankId: formValue.bankId,
      bankBranchId: formValue.bankBranchId,
      // isBankStatus: formValue.isBankStatus ? true : false,
      locationId: this.locationId,
      techAreaId: formValue.techAreaId,
      techStockLocationId: this.locationId,
      WarehouseId: this.wareHouseId,
      isDraft: true,
      // isActive: true,
      createdUserId: this.userData.userId
    }
    if (typeof finalObject['dealerBranchPhysicalAddress']?.formatedAddress == 'object') {
      finalObject['dealerBranchPhysicalAddress'].formatedAddress = finalObject['dealerBranchPhysicalAddress']?.formatedAddress?.description;
    }
    if (typeof finalObject['dealerBranchPostalAddress']?.formatedAddress == 'object') {
      finalObject['dealerBranchPostalAddress'].formatedAddress = finalObject['dealerBranchPostalAddress']?.formatedAddress?.description;
    }
    finalObject['dealerBranchPhysicalAddress']['createdUserId'] = this.userData?.userId;
    finalObject['dealerBranchPostalAddress']['createdUserId'] = this.userData?.userId;
    let api = this.dealerBranchId ? this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_BRANCH, finalObject) :
      this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_BRANCH, finalObject, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(['dealer/dealer-maintenance'],
          { queryParams: { tab: 2, id: this.dealerId, dealerTypeId: this.dealerTypeId } });

      }
      this.rxjsService.setDialogOpenProperty(false);
    });

  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) { }
  }
}
