import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerDocumentsAddEditComponent } from '@modules/dealer';

@NgModule({
    declarations: [DealerDocumentsAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerDocumentsAddEditComponent],
    exports: [DealerDocumentsAddEditComponent],
})
export class DealerDocumentsAddEditModule { }
