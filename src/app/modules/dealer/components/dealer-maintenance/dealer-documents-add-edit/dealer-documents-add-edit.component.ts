import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CrudService, IApplicationResponse, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { DealerDocumentDetailModel } from '@modules/dealer';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-dealer-documents-add-edit',
  templateUrl: './dealer-documents-add-edit.component.html',
  styleUrls: ['./dealer-documents-add-edit.component.scss']
})
export class DealerDocumentsAddEditComponent implements OnInit {

  dealerDocumentTypeList: any = [];
  isSubmitted: boolean;
  dealerDocumentDialogForm: FormGroup;
  fileName: any;
  showDocError: boolean;
  btnName: any = 'Save';
  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
      if(config?.data?.row?.dealerDocumentId) {
        this.btnName = 'Update';
      }
     }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(dealerDocumentDetailModel?: DealerDocumentDetailModel) {
    let DealerDocumentModel = new DealerDocumentDetailModel(dealerDocumentDetailModel);
    this.dealerDocumentDialogForm = this.formBuilder.group({});
    Object.keys(DealerDocumentModel).forEach((key) => {
      if (typeof DealerDocumentModel[key] === 'object') {
        this.dealerDocumentDialogForm.addControl(key, new FormArray(DealerDocumentModel[key]));
      } else {
        this.dealerDocumentDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : DealerDocumentModel[key]));
      }
    });
    if (!this.dealerDocumentDialogForm.get('dealerDocumentId').value) {
      this.dealerDocumentDialogForm = setRequiredValidator(this.dealerDocumentDialogForm, ["dealerDocumentConfigId"]);
    }
    this.dealerDocumentDialogForm = setRequiredValidator(this.dealerDocumentDialogForm, ["docName"]);
  }

  btnCloseClick() {
    this.dealerDocumentDialogForm.reset();
    this.ref.close(false);
  }

  onFileSelected(file) {
    this.dealerDocumentDialogForm.get('docName').markAsDirty();
    if (file?.length) {
      this.showDocError = false;
      const fileObj = file.item(0);
      const fileName = fileObj.name;
      const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
      if (fileExtension !== '.xlsx') {
        // this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
        //  return;
      }
      if (fileName) {
        this.dealerDocumentDialogForm.get('docName').setValue(fileName);
      }
      this.fileName = fileObj;
    } else if (!this.dealerDocumentDialogForm.get('docName').value) {
      this.showDocError = true;
    }
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.dealerDocumentDialogForm?.valid) {
      this.dealerDocumentDialogForm?.markAllAsTouched();
      return;
    } else if (!this.dealerDocumentDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.fileName?.name) {
      this.snackbarService.openSnackbar("Please upload any file", ResponseMessageTypes.WARNING);
      return;
    }
    let obj = this.dealerDocumentDialogForm.getRawValue();
    if (!this.dealerDocumentDialogForm.get('dealerDocumentId').value) {
      delete obj['dealerDocumentId'];
    }
    if (this.dealerDocumentDialogForm.get('dealerDocumentId').value) {
      delete obj['dealerDocumentConfigId'];
    }
    let formData = new FormData();
    formData.append("file", this.fileName);
    formData.append('Obj', JSON.stringify(obj));
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    let api = this.crudService.create(this.config?.data?.module, this.config?.data?.api, formData);
    if (this.dealerDocumentDialogForm.get('dealerDocumentId').value) {
      api = this.crudService.update(this.config?.data?.module, this.config?.data?.api, formData);
    }
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.dealerDocumentDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }

}
