import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerTerminationComponent } from '@modules/dealer';

@NgModule({
    declarations: [DealerTerminationComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerTerminationComponent],
    exports: [DealerTerminationComponent],
})
export class DealerTerminationModule { }
