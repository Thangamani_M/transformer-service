import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { countryCodes, CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { DealerTerminationModel } from "@modules/dealer/models/dealer-termination.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
  selector: 'app-dealer-termination',
  templateUrl: './dealer-termination-component.html',
  styleUrls: ['./dealer-termination-component.scss']
})

export class DealerTerminationComponent implements OnInit {

  dealerTerminationForm: FormGroup;
  disciplinaryFileName = '';
  resignationFileName = '';
  isFileSelected: boolean = false;
  loggedUser: any;
  formData = new FormData();
  showFailedImport: boolean = false;
  countryCodess = countryCodes;
  terminationReasonList = [];
  isResignationRequired = false;
  isSubmitted = false;
  startTodayDate = new Date();

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private momentService: MomentService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }
  ngOnInit(): void {
    this.createForm();
    this.getSuspensionReasonListDropdown();
  }
  getSuspensionReasonListDropdown() {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_TERMINATION_REASONS,
      undefined
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.terminationReasonList = data.resources;
        // this.terminationReasonList.push({id:2,displayName:'Resignation'});
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  createForm(dealerTerminationFormModel?: DealerTerminationModel) {
    let dealerTerminationModel = new DealerTerminationModel(dealerTerminationFormModel);
    this.dealerTerminationForm = this.formBuilder.group({});
    Object.keys(dealerTerminationModel).forEach((key) => {
      if (dealerTerminationModel[key] === 'dealerBranchAddEditList') {
        this.dealerTerminationForm.addControl(key, new FormArray(dealerTerminationModel[key]));
      } else {
        this.dealerTerminationForm.addControl(key, new FormControl());

      }
    });
    this.dealerTerminationForm = setRequiredValidator(this.dealerTerminationForm, ["terminationDate", "dealerTerminationReasonId", "isServiceCustomer", "notes"]);
    this.dealerTerminationForm.get('isServiceCustomer').setValue("true");
    this.onValueChanges();
  }

  onValueChanges() {
    //based on immediate effect set date as system generated date as current
    this.dealerTerminationForm
      .get("isImmediateEffect")
      .valueChanges.subscribe((isImmediateEffect: boolean) => {
        if (isImmediateEffect)
          this.dealerTerminationForm.get('terminationDate').setValue(new Date());
        else
          this.dealerTerminationForm.get('terminationDate').reset();
      });

    //based on immediate effect set date as system generated date as current
    this.dealerTerminationForm
      .get("dealerTerminationReasonId")
      .valueChanges.subscribe((dealerTerminationReasonId: any) => {
        let filterData = this.terminationReasonList.filter(x => x.id == dealerTerminationReasonId);
        if (filterData && filterData[0].displayName == 'Resignation') {
          this.isResignationRequired = true;
        }
        else {
          this.isResignationRequired = false;
        }
      });

  }

  obj1: any; obj2: any;
  onFileSelected(files: FileList, type): void {
    this.showFailedImport = false;
    const fileObj = files.item(0);
    if (type == 'disciplinary') {
      this.disciplinaryFileName = fileObj.name;
    }
    else {
      this.resignationFileName = fileObj.name;
      this.isResignationRequired = false;
    }

    if (this.disciplinaryFileName || this.resignationFileName) {
      this.isFileSelected = true;
    }
    const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
    if (fileExtension !== '.xlsx') {
      // this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
      // return;
    }
    if (type == 'disciplinary')
      this.obj1 = fileObj;
    else
      this.obj2 = fileObj;
  }

  onSubmit() {
    if (!this.dealerTerminationForm?.valid) {
      return;
    }

    let formValue = this.dealerTerminationForm.value;
    formValue.terminationDate = (formValue.terminationDate && formValue.terminationDate != null) ? this.momentService.toMoment(formValue.terminationDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    let dealerTypeObject = Object.assign({},
      { dealerSuspensionId: null },
      { dealerId: this.config?.data?.row?.dealerId },
      { dealerTerminationReasonId: formValue.dealerTerminationReasonId },
      { terminationDate: formValue.terminationDate },
      { isImmediateEffect: formValue.isImmediateEffect ? true : false },
      { isServiceCustomer: formValue.isServiceCustomer },
      { notes: formValue.notes },
      { isActive: true },
      { createdUserId: this.loggedUser.userId }
    );
    this.isSubmitted = true;
    let filterdNewData = Object.entries(dealerTypeObject).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    Object.keys(this.formData).forEach(key => {
      if (this.formData[key] === "" || this.formData[key].length == 0) {
        delete this.formData[key]
      }
    });
    if (this.obj1)
      this.formData.append("Disciplinary", this.obj1);
    if (this.obj2)
      this.formData.append("Resignation", this.obj2);

    this.formData.append('Obj', JSON.stringify(filterdNewData));
    let api = this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_TERMINATION, this.formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
      }
      this.ref.close(res);
      this.rxjsService.setDialogOpenProperty(false);
    })
  }
  btnCloseClick() {
    this.ref.close(false);
  }
}