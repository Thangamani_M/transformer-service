import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerDocumentDeclineReasonComponent } from '@modules/dealer';

@NgModule({
    declarations: [DealerDocumentDeclineReasonComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerDocumentDeclineReasonComponent],
    exports: [DealerDocumentDeclineReasonComponent],
})
export class DealerDocumentDeclineReasonModule { }
