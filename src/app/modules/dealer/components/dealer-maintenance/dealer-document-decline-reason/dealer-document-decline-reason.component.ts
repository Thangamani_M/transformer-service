import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CrudService, IApplicationResponse, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-dealer-document-decline-reason',
  templateUrl: './dealer-document-decline-reason.component.html',
})
export class DealerDocumentDeclineReasonComponent implements OnInit {
  reasonDialogForm: FormGroup;
  isSubmitted: boolean;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private crudService: CrudService, private snackbarService: SnackbarService) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.reasonDialogForm = new FormGroup({
      notes: new FormControl('', [Validators.required])
    })
  }

  ngAfterViewInit() {
  }

  btnCloseClick() {
    this.reasonDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.reasonDialogForm?.valid) {
      this.reasonDialogForm?.markAllAsTouched();
      return;
    } else if (!this.reasonDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const reqObj = {
      ...this.config?.data?.row,
    }
    reqObj['reason'] = this.reasonDialogForm.value.notes;
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    this.crudService.create(this.config?.data?.module, this.config?.data?.api, reqObj)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          this.reasonDialogForm.reset();
          this.ref.close(res);
        }
        this.rxjsService.setDialogOpenProperty(false);
        this.isSubmitted = false;
      })
  }
}
