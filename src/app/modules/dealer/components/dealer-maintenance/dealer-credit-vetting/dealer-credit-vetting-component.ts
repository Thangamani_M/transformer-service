import { Component, OnInit } from "@angular/core";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from "@app/shared";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
  selector: 'app-dealer-credit-vetting',
  templateUrl: './dealer-credit-vetting-component.html',
  styleUrls: ['./dealer-credit-vetting-component.scss']
})
export class DealerCreditVettingComponent implements OnInit {

  loggedUser: any;
  btnName = 'Proceed';
  options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    arcColors: ["rgb(255,84,84)", "rgb(239,214,19)", "rgb(61,204,91)"],
    arcDelimiters: [35, 70],
    rangeLabel: ['0', '900'],
    needleStartValue: 10,
  }
  needleValue2: Number = 0;
  bottomLabel2: any;
  name2: any = '';
  centralLabel2 = '600';
  btnName2 = 'Proceed';
  options2 = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    arcColors: ["rgb(255,84,84)", "rgb(239,214,19)", "rgb(61,204,91)"],
    arcDelimiters: [35, 70],
    rangeLabel: ['0', '900'],
    needleStartValue: 10,
  }
  principleDetails: any = {
    dealerPrincipleNameandsurname: '',
    principleIDNumber: '',
    dealerAddress: '',
    dateOfBirth: null,
    reason: '',
  };
  dealerPrincipleId;
  avscheckDetails;

  constructor(private store: Store<AppState>, private crudService: CrudService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.dealerPrincipleId = this.config.data.row.dealerPrincipleId;
    this.options2.rangeLabel = ['0', '900'];
    this.getCrdeitVetting();
  }
  getCrdeitVetting() {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.CREDIT_VETTING, this.dealerPrincipleId, false,
      null)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources) {
          this.principleDetails = response.resources;
          this.principleDetails.dateOfBirth = new Date(response.resources.dateOfBirth);
          this.needleValue2 = (parseFloat(this.principleDetails.creditScore) / 900) * 100;
          this.bottomLabel2 = 'Score :' + this.principleDetails.creditScore;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  btnCloseClick() {
    this.ref.close(false);
  }
}