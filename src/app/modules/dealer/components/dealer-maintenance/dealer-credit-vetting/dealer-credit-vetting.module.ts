import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerCreditVettingComponent } from '@modules/dealer';
import { GaugeChartModule } from 'angular-gauge-chart';

@NgModule({
    declarations: [DealerCreditVettingComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        GaugeChartModule
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerCreditVettingComponent],
    exports: [DealerCreditVettingComponent],
})
export class DealerCreditVettingModule { }
