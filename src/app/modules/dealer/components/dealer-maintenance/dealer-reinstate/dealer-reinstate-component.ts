import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { DealerReinstateModel } from "@modules/dealer/models/dealer-reinstate.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-dealer-reinstate',
    templateUrl: './dealer-reinstate-component.html',
    styleUrls: ['./dealer-reinstate-component.scss']
  })
  
  export class DealerReinstateComponent implements OnInit { 

    dealerReinstateForm:FormGroup;
    reasonSuspensionList=[];
    fileName = '';
    isFileSelected: boolean = false;
    loggedUser:any;
    startTodayDate= new Date();

    constructor(private rxjsService: RxjsService, private crudService: CrudService,private ref: DynamicDialogRef, private momentService: MomentService, private formBuilder: FormBuilder,private store: Store<AppState>, public config: DynamicDialogConfig){
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
          this.loggedUser = userData;
       });
     }
    ngOnInit(): void {
       this.createForm();
    }
    createForm(dealerReinstateFormModel?: DealerReinstateModel) {
      let dealerReinstateModel = new DealerReinstateModel(dealerReinstateFormModel);
      this.dealerReinstateForm = this.formBuilder.group({});
      Object.keys(dealerReinstateModel).forEach((key) => {
        // if (dealerReinstateModel[key] === 'dealerBranchAddEditList') {
        //   this.dealerReinstateForm.addControl(key, new FormArray(dealerReinstateModel[key]));
        // } else {
          this.dealerReinstateForm.addControl(key, new FormControl());  
        // }
      });     
      this.dealerReinstateForm = setRequiredValidator(this.dealerReinstateForm, ["reinstatementDate","reinstatementNotes"]);
      this.onValueChanges();
    }
    onValueChanges() {
      //based on immediate effect set date as system generated date as current
      this.dealerReinstateForm
      .get("isReinstatementImmediateEffect")
      .valueChanges.subscribe((isReinstatementImmediateEffect: boolean) => {
          if(isReinstatementImmediateEffect)
             this.dealerReinstateForm.get('reinstatementDate').setValue(new Date());
          else
             this.dealerReinstateForm.get('reinstatementDate').reset();
      });
    }
    onSubmit() {
      if (!this.dealerReinstateForm?.valid) {
        return;
      }
      let formValue = this.dealerReinstateForm.value;   
      formValue.reinstatementDate = (formValue.reinstatementDate && formValue.reinstatementDate!=null)  ? this.momentService.toMoment(formValue.reinstatementDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;  
      let dealerReinstateObject = {
          dealerId:   this.config?.data?.row?.dealerId,
          reinstatementDate: formValue.reinstatementDate ,
          dealerSuspensionId :  this.config?.data?.row?.dealerSuspensionId,
          isReinstatementImmediateEffect: formValue.isReinstatementImmediateEffect ? formValue.isReinstatementImmediateEffect : false ,
          reinstatementNotes: formValue.reinstatementNotes ,
          isActive:true,
          createdUserId: this.loggedUser.userId,
          modifiedUserId: this.loggedUser.userId,
      }
      let api = this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_REINSTATE,dealerReinstateObject);
      api.subscribe((res: any) => {
       if (res?.isSuccess == true && res?.statusCode == 200) {
       }
       this.ref.close(res);
       this.rxjsService.setDialogOpenProperty(false);
     })
    }
    btnCloseClick() {
      this.ref.close(false);
    }
  }