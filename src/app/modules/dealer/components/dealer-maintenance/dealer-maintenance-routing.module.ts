import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', loadChildren: () => import('./dealer-maintenance-list/dealer-maintenance-list.module').then(m => m.DealerMaintenanceListModule) },
    { path: 'info', loadChildren: () => import('./dealer-maintenance-add-edit/dealer-maintenance-add-edit.module').then(m => m.DealerMaintenanceAddEditModule) },
    { path: 'branch', loadChildren: () => import('./dealer-branch-add-edit/dealer-branch-add-edit.module').then(m => m.DealerBranchAddEditModule) },
    { path: 'branch-category', loadChildren: () => import('./dealer-branch-category-add-edit/dealer-branch-category-add-edit.module').then(m => m.DealerBranchCategoryAddEditModule) }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class DealerMaintenanceRoutingModule { }
