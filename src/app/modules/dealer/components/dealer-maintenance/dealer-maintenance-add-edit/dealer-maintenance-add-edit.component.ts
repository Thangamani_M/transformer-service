import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, destructureAfrigisObjectAddressComponents, getPDropdownData, IAfrigisAddressComponents, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { DealerMaintenanceAddEditModel, DealerMaintenanceAddressModel } from '@modules/dealer/models/dealer-maintenance.model';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { NewAddressPopupComponent } from '@modules/sales';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { DealerReinstateComponent } from '../dealer-reinstate/dealer-reinstate-component';
import { DealerSuspensionComponent } from '../dealer-suspension/dealer-suspension-component';
import { DealerSuspensionDetailComponent } from '../dealer-suspension/dealer-suspension-detail-component';
import { DealerTerminationComponent } from '../dealer-termination/dealer-termination-component';

@Component({
  selector: 'app-dealer-maintenance-add-edit',
  templateUrl: './dealer-maintenance-add-edit.component.html',
  styleUrls: ['./dealer-maintenance-add-edit.component.scss']
})
export class DealerMaintenanceAddEditComponent implements OnInit {

  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  dealerId: any;
  dealerTypeId: any;
  referenceId: string;
  eventSubscription: any;
  dealerDetail: any;
  viewable: boolean;
  isLoading: boolean;
  isSubmitted: boolean;
  dealerMaintenanceAddEditForm: FormGroup;
  dealerTypeList: any = [];
  status: any = [];
  physicalAddressList: any;
  postalAddressList: any;
  startTodayDate: any = new Date();
  psiraExpiryDate: any = new Date();
  userData: UserLogin;
  showAction: boolean;
  physicalAddressJsonAddress: any;
  postalAddressJsonAddress: any;
  dropdownSubscription: any;
  ispostalAddressLoading: boolean;
  inValidPostalAddress: boolean;
  checkMaxLengthPostalAddress: boolean;
  isPhysicalAddressLoading: boolean;
  inValidPhysicalAddress: boolean;
  checkMaxLengthPhysicalAddress: boolean;
  terminationDetails: any;
  btnName: any = 'Save';
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isSuspended: boolean = false;
  isTerminated: boolean = false;
  dealerSuspensionId: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAnAlphaNumericWithSlash = new CustomDirectiveConfig({ isAnAlphaNumericWithSlash: true });

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private dialog: MatDialog,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private dialogService: DialogService, private crudService: CrudService,
    private datePipe: DatePipe, private snackbarService: SnackbarService,) {
    this.dealerId = this.activatedRoute.snapshot.queryParamMap['params']?.id;
    this.dealerTypeId = this.activatedRoute.snapshot.queryParamMap['params']?.dealerTypeId;
    this.referenceId = this.activatedRoute.snapshot.queryParamMap['params']?.referenceId;
    this.primengTableConfigProperties = {
      tableCaption: this.dealerId && !this.viewable ? 'Update Dealer' : this.viewable ? 'Dealer Info-View' : 'Create Dealer',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Maintenance', relativeRouterUrl: '/dealer/dealer-maintenance' }, { displayName: 'Create Dealer', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Info',
            dataKey: 'dealerId',
            enableBreadCrumb: true,
            enableAction: false,
            disabled: false,
            url: 'dealer/dealer-maintenance',
          },
          {
            caption: 'Dealer Principle',
            dataKey: 'dealerId',
            enableBreadCrumb: true,
            enableAction: false,
            disabled: true,
            url: 'dealer/dealer-maintenance',
          },
          {
            caption: 'Branch Details',
            dataKey: 'dealerBranchId',
            enableBreadCrumb: true,
            enableAction: false,
            disabled: true,
            url: 'dealer/dealer-maintenance',
          },
          {
            caption: 'Dealer Branch Category',
            dataKey: 'dealerBranchCategoryId',
            enableBreadCrumb: true,
            enableAction: false,
            disabled: true,
            url: 'dealer/dealer-maintenance',
          },
          {
            caption: 'Dealer Documents',
            dataKey: 'dealerId',
            enableBreadCrumb: true,
            enableAction: false,
            disabled: true,
            url: 'dealer/dealer-maintenance',
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('dealer-maintenance/info/add-edit') > -1) {
        this.viewable = false;
      } else if (data?.url?.indexOf('dealer-maintenance/info/view') > -1) {
        this.viewable = true;
      }
      this.onLoadValue();
    })
  }

  ngOnInit(): void {
    // this.onLoadValue();
    this.combineLatestNgrxStoreData()
    this.isTerminated = true;
    this.isSuspended = true;
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_MAINTENANCE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
    if (this.dealerId) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList.map(el => el.disabled = false);
    } else {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList.map(el => el.disabled = true);
    }
  }

  getTerminationDetails(params) {
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_TERMINATION_DETAILS,
      undefined,
      false, prepareRequiredHttpParams(params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.terminationDetails = data.resources;
      }
    });
  }
  tabClick(e) {
    if (!this.dealerId) {
      return
    }
    this.selectedTabIndex = e?.index;
    let queryParams = { tab: this.selectedTabIndex };
    if (this.selectedTabIndex != 0) {
      queryParams['id'] = this.dealerId;
      queryParams['dealerTypeId'] = this.dealerTypeId;
      if (this.referenceId) {
        queryParams['referenceId'] = this.referenceId;
      }
    }
    this.router.navigate([`./${this.primengTableConfigProperties.tableComponentConfigs.tabsList[e?.index].url}`], { queryParams: queryParams });
  }

  onLoadValue() {
    this.initForm();
    if (this.dealerId) {
      this.getTerminationDetails({ dealerId: this.dealerId });
      this.primengTableConfigProperties.tableComponentConfigs.tabsList.map(el => el.disabled = false);
    }
    this.onPrimeTitleChanges();
    this.loadAllDropdown();
    if (this.viewable) {
      if (!this.referenceId) {
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableAction = true;
      }
      if (this.referenceId) {
        this.primengTableConfigProperties.breadCrumbItems[1]['relativeRouterUrl'] = '';
      }
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].isRemoveFormArray = false;
      this.onShowValue();
    } else {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableAction = false;
    }
    if (this.viewable && this.primengTableConfigProperties.breadCrumbItems[3]) {
      this.primengTableConfigProperties.breadCrumbItems.pop();
    }
  }

  initForm(dealerMaintenanceAddEditModel?: DealerMaintenanceAddEditModel) {
    let dealerMaintenanceModel = new DealerMaintenanceAddEditModel(dealerMaintenanceAddEditModel);
    this.dealerMaintenanceAddEditForm = this.formBuilder.group({});
    Object.keys(dealerMaintenanceModel).forEach((key) => {
      if (typeof dealerMaintenanceModel[key] === 'object') {
        if (dealerMaintenanceModel[key]?.length == 0) {
          this.dealerMaintenanceAddEditForm.addControl(key, new FormArray(dealerMaintenanceModel[key]));
        } else if (Object.keys(dealerMaintenanceModel[key])?.length == 0) {
          this.dealerMaintenanceAddEditForm.addControl(key, new FormGroup(dealerMaintenanceModel[key]));
          if (key == 'dealerPhysicalAddress' || key == 'dealerPostalAddress') {
            const basicInfoDetailsModel = new DealerMaintenanceAddressModel();
            this.initFormInfoForm(this.dealerMaintenanceAddEditForm.get(key), basicInfoDetailsModel);
          }
        }
      } else if (!this.viewable) {
        this.dealerMaintenanceAddEditForm.addControl(key, new FormControl(dealerMaintenanceModel[key]));
      }
    });
    if (!this.viewable) {
      this.setFormValidators();
      this.onDealerMaintainenanceValueChanges();
      this.setDisableValues();
    }
  }

  initFormInfoForm(form, formDetailsModel: any) {
    Object.keys(formDetailsModel)?.forEach((key: any) => {
      if (typeof formDetailsModel[key] == 'object') {
        form.addControl(key, new FormArray(formDetailsModel[key]));
      } else {
        form.addControl(key, new FormControl(formDetailsModel[key]));
      }
    });
  }

  openAddressPopup(addType?) {
    const data = {
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId,
      isShowAddressType: false, mobile2: '', officeNo: '', mobile1: '', premisesNo: '', isAddressExtra: false, isHideEstateAddres: false,
      formatedAddress: addType == 'physicalAddress' ? this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.formatedAddress').value?.description
        : this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.formatedAddress').value?.description,
    }
    const dialogReff = this.dialog.open(NewAddressPopupComponent, {
      width: "750px",
      disableClose: true,
      data: data,
    });
    dialogReff.afterClosed().subscribe((result) => { });
    dialogReff.componentInstance.outputData.subscribe((result) => {
      if (result) {
        if (addType == 'physicalAddress') {
          // this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress').reset('', {eventEmit: false});
          this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress').patchValue({
            formatedAddress: { description: this.getAddressName(result) },
            buildingNo: result?.buildingNo,
            buildingName: result?.buildingName,
            streetNo: result?.streetNo,
            streetName: result?.streetName,
            suburbName: result?.suburbName,
            cityName: result?.cityName,
            provinceName: result?.provinceName,
            postalCode: result?.postalCode,
            latitude: '',
            longitude: '',
            seoId: '',
            jsonObject: '',
            estateName: result?.estateName,
            estateStreetNo: result?.estateStreetNo,
            estateStreetName: result?.estateStreetName,
          }, { emitEvent: false });
        } else if (addType == 'postalAddress') {
          // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress').reset('', {eventEmit: false});
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress').patchValue({
            formatedAddress: { description: this.getAddressName(result) },
            buildingNo: result?.buildingNo,
            buildingName: result?.buildingName,
            streetNo: result?.streetNo,
            streetName: result?.streetName,
            suburbName: result?.suburbName,
            cityName: result?.cityName,
            provinceName: result?.provinceName,
            postalCode: result?.postalCode,
            latitude: '',
            longitude: '',
            seoId: '',
            jsonObject: '',
            estateName: result?.estateName,
            estateStreetNo: result?.estateStreetNo,
            estateStreetName: result?.estateStreetName,
          }, { emitEvent: false });
        }
      }
    });
  }

  getAddressName(result) {
    if (result) {
      const streetNo = result?.streetNo ? `${result?.streetNo}, ` : '';
      const streetName = result?.streetName ? `${result?.streetName}, ` : '';
      const suburbName = result?.suburbName ? `${result?.suburbName}, ` : '';
      const cityName = result?.cityName ? `${result?.cityName}, ` : '';
      const postalCode = result?.postalCode ? `${result?.postalCode}, ` : '';
      const provinceName = result?.provinceName ? `${result?.provinceName}` : '';
      return `${streetNo}${streetName}${suburbName}${cityName}${postalCode}${provinceName}`;
    }
    return null;
  }

  onDealerMaintainenanceValueChanges() {
    // this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.formatedAddress').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
    //   switchMap((val) => {
    //     if (val) {
    //       this.isPhysicalAddressLoading = true;
    //     } else {
    //       this.isPhysicalAddressLoading = undefined;
    //     }
    //     this.physicalAddressList = [];
    //     return this.filterServiceTypeDetailsBySearchOptions(val);
    //   }))
    //   .subscribe((response: any) => {
    //     if (response?.isSuccess && response?.statusCode == 200) {
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //       if (response.resources != null) {
    //         this.physicalAddressList = response.resources;
    //         if (this.physicalAddressList.length == 0) {
    //           this.physicalAddressList = [];
    //           this.inValidPhysicalAddress = true;
    //           this.dealerMaintenanceAddEditForm.controls['latlang'].setValue('');
    //         }
    //       } else {
    //         this.physicalAddressList = [];
    //         this.inValidPhysicalAddress = true;
    //       }
    //       this.isPhysicalAddressLoading = false;
    //     }
    //     if (this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').value) {
    //       this.isSameAsAddress(this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').value);
    //     }
    //   });
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.formatedAddress').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
    //   switchMap((val) => {
    //     if (val) {
    //       this.ispostalAddressLoading = true;
    //     } else {
    //       this.ispostalAddressLoading = undefined;
    //     }
    //     this.postalAddressList = [];
    //     return this.filterServiceTypeDetailsBySearchOptions(val, false);
    //   }))
    //   .subscribe((response: any) => {
    //     if (response?.isSuccess && response?.statusCode == 200) {
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //       if (response.resources != null) {
    //         this.postalAddressList = response.resources;
    //         if (this.postalAddressList.length == 0) {
    //           this.postalAddressList = [];
    //           this.inValidPostalAddress = true;
    //           this.dealerMaintenanceAddEditForm.controls['latlang'].setValue('');
    //         }
    //       } else {
    //         this.physicalAddressList = [];
    //         this.inValidPostalAddress = true;
    //       }
    //       this.ispostalAddressLoading = false;
    //     }
    //   });
    this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').valueChanges.pipe(debounceTime(800), distinctUntilChanged())
      .subscribe((response: any) => {
         if (response == true) {
            let postalAddress1 = (this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.buildingNo').value ? this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.buildingNo').value + ' ' : '')  + (this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.buildingName').value ? this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.buildingName').value+' ' : '') + this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.streetNo').value;
            let postalAddress2 = this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.streetName').value + ' ' +this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.suburbName').value;
            let postalAddress3 = this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.cityName').value;
            let postalAddress4 = this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.provinceName').value;
            let postalAddress5 = this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.postalCode').value;
            this.dealerMaintenanceAddEditForm.get('postalAddress1').setValue(postalAddress1);
            this.dealerMaintenanceAddEditForm.get('postalAddress2').setValue(postalAddress2);
            this.dealerMaintenanceAddEditForm.get('postalAddress3').setValue(postalAddress3);
            this.dealerMaintenanceAddEditForm.get('postalAddress4').setValue(postalAddress4);
            this.dealerMaintenanceAddEditForm.get('postalAddress5').setValue(postalAddress5);
         }
        // if (response == true) {
        //   this.isSameAsAddress(response);
        // } else if (response == false) {
        //   this.enableDisablePostalAddress(response);
        // }
      });
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.buildingNo').valueChanges.pipe(debounceTime(800), distinctUntilChanged())
      .subscribe((response: any) => {
        if (response && this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').value) {
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.buildingNo').setValue(response);
        } else if (this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').value) {
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.buildingNo').setValue(response);
        }
      });
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.buildingName').valueChanges.pipe(debounceTime(800), distinctUntilChanged())
      .subscribe((response: any) => {
        if (response && this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').value) {
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.buildingName').setValue(response);
        } else if (this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').value) {
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.buildingName').setValue(response);
        }
      });
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.streetNo').valueChanges.pipe(debounceTime(800), distinctUntilChanged())
      .subscribe((response: any) => {
        if (response && this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').value) {
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.streetNo').setValue(response);
        } else if (this.dealerMaintenanceAddEditForm.get('isSameAsPhysicalAddress').value) {
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.streetNo').setValue(response);
        }
      });
  }

  isSameAsAddress(response) {
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.formatedAddress').reset('', {emitEvent: false});
    this.postalAddressList = this.physicalAddressList;
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress').patchValue({
      suburbName: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.suburbName').value, cityName: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.cityName').value,
      provinceName: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.provinceName').value, postalCode: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.postalCode').value,
      streetName: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.streetName').value, streetNo: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.streetNo').value,
      buildingName: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.buildingName').value, buildingNo: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.buildingNo').value,
      latitude: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.latitude').value, longitude: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.longitude').value,
      seoId: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.seoId').value, estateName: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.estateName').value,
      estateStreetNo: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.estateStreetNo').value, estateStreetName: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.estateStreetName').value,
      jsonObject: this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.jsonObject').value,
    }, { emitEvent: false });
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.formatedAddress').setValue(this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.formatedAddress').value, { emitEvent: false });
    this.enableDisablePostalAddress(response);
  }

  enableDisablePostalAddress(response) {
    if (response == true) {
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.formatedAddress').disable({ emitEvent: false });
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.streetNo').disable({ emitEvent: false });
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.buildingName').disable({ emitEvent: false });
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.buildingNo').disable({ emitEvent: false });
    } else if (response == false) {
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.formatedAddress').enable({ emitEvent: false });
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.streetNo').enable({ emitEvent: false });
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.buildingName').enable({ emitEvent: false });
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.buildingNo').enable({ emitEvent: false });
    }
  }

  setDisableValues() {
    this.dealerMaintenanceAddEditForm.get('dealerIIPCode').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.streetName').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.suburbName').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.cityName').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.provinceName').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.postalCode').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.streetName').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.suburbName').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.cityName').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.provinceName').disable();
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.postalCode').disable();
  }

  filterServiceTypeDetailsBySearchOptions(searchValue?: string, isType = true): Observable<IApplicationResponse> | any {
    this.checkMaxLengthPhysicalAddress = this.checkMaxLengthPostalAddress = false;
    if ((searchValue?.length < 3 || !searchValue) && isType) {
      this.checkMaxLengthPhysicalAddress = searchValue ? true : false;
      this.isPhysicalAddressLoading = undefined;
      this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress').patchValue({
        suburbName: '', cityName: '', provinceName: '', streetNo: '', buildingNo: '', buildingName: '',
        streetName: '', postalCode: '', latitude: '', longitude: '', seoId: '', estateName: '',
        estateStreetNo: '', estateStreetName: '', jsonObject: '',
      }, { emitEvent: false })
      return of([]);
    } else if ((searchValue.length < 3 || !searchValue) && !isType) {
      this.checkMaxLengthPostalAddress = searchValue ? true : false;
      this.ispostalAddressLoading = undefined;
      this.dealerMaintenanceAddEditForm.get('dealerPostalAddress').patchValue({
        buildingNo: '', buildingName: '', streetNo: '', streetName: '', suburbName: '', cityName: '',
        provinceName: '', postalCode: '', latitude: '', longitude: '', seoId: '', estateName: '',
        estateStreetNo: '', estateStreetName: '', jsonObject: '',
      }, { emitEvent: false })
      return of([]);
    } else {
      return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_ADDRESS_FROM_AFRIGIS, null, true, prepareRequiredHttpParams({
        searchtext: searchValue,
        IsAfrigisSearch: true
      }), 1)
    }
  }

  setFormValidators() {
    this.dealerMaintenanceAddEditForm = setRequiredValidator(this.dealerMaintenanceAddEditForm,
      ["dealerTypeId", "startDate", "companyName", "psiraRegNumber", "psiraExpiryDate",]);
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.formatedAddress').setValidators([Validators.required]);
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.streetName').setValidators([Validators.required]);
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.postalCode').setValidators([Validators.required]);
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.suburbName').setValidators([Validators.required]);
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.cityName').setValidators([Validators.required]);
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.provinceName').setValidators([Validators.required]);
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress').updateValueAndValidity();
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.formatedAddress').setValidators([Validators.required]);
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.streetName').setValidators([Validators.required]);
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.postalCode').setValidators([Validators.required]);
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.suburbName').setValidators([Validators.required]);
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.cityName').setValidators([Validators.required]);
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.provinceName').setValidators([Validators.required]);
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress').updateValueAndValidity();
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.dealerId && !this.viewable ? 'Update Dealer' : this.viewable ? 'View Dealer' : 'Create Dealer';
    this.showAction = this.viewable ? false : true;
    this.btnName = this.dealerId ? 'Update' : 'Save';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.viewable ? 'Dealer Info-View' : this.dealerId && !this.viewable ? 'View Dealer' : 'Create Dealer';
    this.primengTableConfigProperties.breadCrumbItems[2]['relativeRouterUrl'] = this.viewable || !this.dealerId ? '' : '/dealer/dealer-maintenance/info/view';
    this.primengTableConfigProperties.breadCrumbItems[2]['queryParams'] = this.viewable || !this.dealerId ? {} : { id: this.dealerId, dealerTypeId: this.dealerTypeId, };
    if (!this.viewable && this.dealerId) {
      this.primengTableConfigProperties.breadCrumbItems[3] = { displayName: 'Update Dealer' };
    }
  }

  onSearchPostalAddress(val) {
    this.filterServiceTypeDetailsBySearchOptions(val?.query, false)
      .subscribe((response: any) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources != null) {
            if (response?.resources[0]?.resultNotFound) {
              this.setEmptyPostalAddress(response?.resources[0]?.description);
            } else {
              this.postalAddressList = response.resources;
            }
            if (this.postalAddressList.length == 0) {
              this.setEmptyPostalAddress();
            }
          } else {
            this.setEmptyPostalAddress();
          }
          this.ispostalAddressLoading = false;
        } else {
          this.postalAddressList = [];
        }
      });
  }

  onSearchPhysicalAddress(val) {
    this.filterServiceTypeDetailsBySearchOptions(val?.query)
      .subscribe((response: any) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources.length > 0) {
            if (response?.resources[0]?.resultNotFound) {
              this.setEmptyPhysicalAddress(response?.resources[0]?.description);
            } else {
              this.physicalAddressList = response.resources;
            }
            if (this.physicalAddressList.length == 0) {
              this.setEmptyPhysicalAddress();
            }
          } else {
            this.setEmptyPhysicalAddress();
          }
          this.ispostalAddressLoading = false;
        } else {
          this.physicalAddressList = [];
        }
      });
  }

  setEmptyPhysicalAddress(desc = '') {
    this.physicalAddressList = [];
    this.inValidPhysicalAddress = true;
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.seoId').setValue('');
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.longitude').setValue('');
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.latitude').setValue('');
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.jsonObject').setValue('');
    if (desc) {
      this.setEmptyAddressMsg(desc);
    }
  }

  setEmptyPostalAddress(desc = '') {
    this.postalAddressList = [];
    this.inValidPostalAddress = true;
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.seoId').setValue('');
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.longitude').setValue('');
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.latitude').setValue('');
    this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.jsonObject').setValue('');
    if (desc) {
      this.setEmptyAddressMsg(desc);
    }
  }

  setEmptyAddressMsg(desc) {
    this.snackbarService.openSnackbar(desc, ResponseMessageTypes.ERROR);
    return;
  }

  onSelectPhysicalAddress(e) {
    this.onSelectedPhysicalAddressFromAutoComplete(e?.description, e, e?.seoid);
  }

  onSelectPostalAddress(e) {
    this.onSelectedPostalAddressFromAutoComplete(e?.description, e, e?.seoid);
  }

  onSelectedPhysicalAddressFromAutoComplete(value, address, seoid) {
    let afrigisAddressComponents: IAfrigisAddressComponents;
    this.inValidPostalAddress = false;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
      prepareRequiredHttpParams({ seoid: seoid }), 1)
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.resources) {
          this.physicalAddressJsonAddress = JSON.stringify(res.resources);
          this.physicalAddressList = res.resources.addressDetails;
        }
        let latLong_details = res.resources.addressDetails[0].geometry;
        this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress').patchValue({
          suburbName: '', cityName: '', provinceName: '', streetNo: '', buildingNo: '', buildingName: '',
          streetName: '', postalCode: '', latitude: '', longitude: '', seoId: '', estateName: '',
          estateStreetNo: '', estateStreetName: '',
        }, { emitEvent: false })
        if (latLong_details) {
          this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.latitude').setValue(latLong_details.location.lat);
          this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.longitude').setValue(latLong_details.location.lng);
        }
        this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress.seoId').setValue(res.resources.addressDetails[0].seoid);
        afrigisAddressComponents = destructureAfrigisObjectAddressComponents(res.resources.addressDetails[0].address_components);
        this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress').patchValue({
          suburbName: afrigisAddressComponents.suburbName, cityName: afrigisAddressComponents.cityName,
          provinceName: afrigisAddressComponents.provinceName, postalCode: afrigisAddressComponents.postalCode,
          streetName: afrigisAddressComponents.streetName? afrigisAddressComponents.streetName :null, streetNo: afrigisAddressComponents.streetNo,
          buildingName: afrigisAddressComponents.buildingName, buildingNo: afrigisAddressComponents.buildingNo,
          jsonObject: JSON.stringify(res.resources),
        }, { emitEvent: false })
      })
  }

  onSelectedPostalAddressFromAutoComplete(value, address, seoid) {
    let afrigisAddressComponents: IAfrigisAddressComponents;
    this.inValidPostalAddress = false;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
      prepareRequiredHttpParams({ seoid: seoid }), 1)
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.resources) {
          this.postalAddressJsonAddress = JSON.stringify(res.resources);
          this.postalAddressList = res.resources.addressDetails;
        }
        let latLong_details = res.resources.addressDetails[0].geometry;
        this.dealerMaintenanceAddEditForm.get('dealerPostalAddress').patchValue({
          buildingNo: '', buildingName: '', streetNo: '', streetName: '', suburbName: '', cityName: '',
          provinceName: '', postalCode: '', latitude: '', longitude: '', seoId: '', estateName: '',
          estateStreetNo: '', estateStreetName: '',
        }, { emitEvent: false })
        if (latLong_details) {
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.latitude').setValue(latLong_details.location.lat);
          this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.longitude').setValue(latLong_details.location.lng);
        }
        this.dealerMaintenanceAddEditForm.get('dealerPostalAddress.seoId').setValue(res.resources.addressDetails[0].seoid);
        afrigisAddressComponents = destructureAfrigisObjectAddressComponents(res.resources.addressDetails[0].address_components);
        this.dealerMaintenanceAddEditForm.get('dealerPostalAddress').patchValue({
          suburbName: afrigisAddressComponents.suburbName, cityName: afrigisAddressComponents.cityName,
          provinceName: afrigisAddressComponents.provinceName, postalCode: afrigisAddressComponents.postalCode,
          streetName: afrigisAddressComponents.streetName, streetNo: afrigisAddressComponents.streetNo,
          buildingName: afrigisAddressComponents.buildingName, buildingNo: afrigisAddressComponents.buildingNo,
          jsonObject: JSON.stringify(res.resources),
        }, { emitEvent: false })
      })
  }

  loadAllDropdown() {
    let api: any;
    if (!this.viewable) {
      this.isLoading = true;
      api = [this.crudService.dropdown(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_TYPE_LIST, prepareRequiredHttpParams({ IsAll: false })),
      ]
      if (this.dealerId) {
        api.push(this.getValue())
      }
      if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
        this.dropdownSubscription.unsubscribe();
      }
      this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        response?.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200 && !this.viewable) {
            switch (ix) {
              case 0:
                this.dealerTypeList = getPDropdownData(resp.resources, 'dealerTypeName', 'dealerTypeId');
                break;
              case 1:
                this.psiraExpiryDate = resp?.resources?.psiraExpiryDate ? new Date(resp?.resources?.psiraExpiryDate) : null;
                this.startTodayDate = resp?.resources?.startDate ? new Date(resp?.resources?.startDate) : null;
                this.enableDisablePostalAddress(resp?.resources?.isSameAsPhysicalAddress);
                this.onPatchValue(resp);
                break;
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      });
    } else {
      this.viewValue();
    }
  }

  onPatchValue(resp) {
    // const dealerTypeConfig = this.dealerTypeList.find(el => el?.dealerTypeId == resp?.resources?.dealerTypeId);
    const statusId = this.status.find(el => el?.value == resp?.resources?.isActive);
    // this.dealerMaintenanceAddEditForm.patchValue(resp?.resources, {emitEvent: false});
    this.dealerMaintenanceAddEditForm.patchValue({
      companyName: resp?.resources?.companyName ? resp?.resources?.companyName : '',
      companyRegNumber: resp?.resources?.companyRegNumber ? resp?.resources?.companyRegNumber : '',
      createdUserId: resp?.resources?.createdUserId,
      dealerIIPCode: resp?.resources?.dealerIIPCode ? resp?.resources?.dealerIIPCode : '',
      dealerTypeId: resp?.resources?.dealerTypeId,
      // isActive: statusId,
      isAuthorizedToRemoveRS: resp?.resources?.isAuthorizedToRemoveRS?.toString() ? resp?.resources?.isAuthorizedToRemoveRS : null,
      isDraft: resp?.resources?.isDraft,
      isSameAsPhysicalAddress: resp?.resources?.isSameAsPhysicalAddress?.toString() ? resp?.resources?.isSameAsPhysicalAddress : null,
      modifiedUserId: resp?.resources?.modifiedUserId,
      psiraExpiryDate: resp?.resources?.psiraExpiryDate ? this.psiraExpiryDate : '',
      psiraRegNumber: resp?.resources?.psiraRegNumber ? resp?.resources?.psiraRegNumber : '',
      startDate: this.startTodayDate,
      vatNumber: resp?.resources?.vatNumber ? resp?.resources?.vatNumber : '',
    }, { emitEvent: false });
    this.dealerMaintenanceAddEditForm.get('dealerPhysicalAddress').patchValue({
      addressId: resp?.resources?.physicalAddressId ? resp?.resources?.physicalAddressId : '',
      formatedAddress: resp?.resources?.formatedAddress ? { description: resp?.resources?.formatedAddress } : null,
      buildingNo: resp?.resources?.buildingNo ? resp?.resources?.buildingNo : '',
      buildingName: resp?.resources?.buildingName ? resp?.resources?.buildingName : '',
      streetNo: resp?.resources?.streetNo ? resp?.resources?.streetNo : '',
      streetName: resp?.resources?.streetName ? resp?.resources?.streetName : '',
      suburbName: resp?.resources?.suburbName ? resp?.resources?.suburbName : '',
      cityName: resp?.resources?.cityName ? resp?.resources?.cityName : '',
      provinceName: resp?.resources?.provinceName ? resp?.resources?.provinceName : '',
      postalCode: resp?.resources?.postalCode ? resp?.resources?.postalCode : '',
      latitude: resp?.resources?.latitude ? resp?.resources?.latitude : '',
      longitude: resp?.resources?.longitude ? resp?.resources?.longitude : '',
    }, { eventEmit: false });
    this.dealerMaintenanceAddEditForm.get('postalAddress1').setValue(resp?.resources?.postalAddress1);
    this.dealerMaintenanceAddEditForm.get('postalAddress2').setValue(resp?.resources?.postalAddress2);
    this.dealerMaintenanceAddEditForm.get('postalAddress3').setValue(resp?.resources?.postalAddress3);
    this.dealerMaintenanceAddEditForm.get('postalAddress4').setValue(resp?.resources?.postalAddress4);
    this.dealerMaintenanceAddEditForm.get('postalAddress5').setValue(resp?.resources?.postalAddress5);
    // this.dealerMaintenanceAddEditForm.get('dealerPostalAddress').patchValue({
    //   addressId: resp?.resources?.postalAddressId ? resp?.resources?.postalAddressId : '',
    //   formatedAddress: resp?.resources?.postalFormatedAddress ? { description: resp?.resources?.postalFormatedAddress } : null,
    //   buildingNo: resp?.resources?.postalBuildingNo ? resp?.resources?.postalBuildingNo : '',
    //   buildingName: resp?.resources?.postalBuildingName ? resp?.resources?.postalBuildingName : '',
    //   streetNo: resp?.resources?.postalStreetNo ? resp?.resources?.postalStreetNo : '',
    //   streetName: resp?.resources?.postalStreetName ? resp?.resources?.postalStreetName : '',
    //   suburbName: resp?.resources?.postalSuburbName ? resp?.resources?.postalSuburbName : '',
    //   cityName: resp?.resources?.postalCityName ? resp?.resources?.postalCityName : '',
    //   provinceName: resp?.resources?.postalProvinceName ? resp?.resources?.postalProvinceName : '',
    //   postalCode: resp?.resources?.postalPostalCode ? resp?.resources?.postalPostalCode : '',
    //   latitude: resp?.resources?.postalLatitude ? resp?.resources?.postalLatitude : '',
    //   longitude: resp?.resources?.postalLongitude ? resp?.resources?.postalLongitude : '',
    // }, { eventEmit: false });
    this.enableEditandTab(resp);
  }

  getValue() {
    return this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER, this.dealerId);
  }

  editValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.dealerDetail = response?.resources;
      this.dealerMaintenanceAddEditForm.patchValue(response?.resources, { emitEvent: false });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  viewValue() {
    this.getValue().subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.isTerminated = response.resources.isTerminated;
        this.isSuspended = response.resources.isSuspended;
        this.dealerSuspensionId = response.resources.dealerSuspensionId;
        // if (!this.referenceId) {
        //   if ((!this.isTerminated && this.isSuspended == false) || this.isTerminated) {
        //     document.getElementById('btnReinstate').setAttribute("disabled", "disabled");
        //   } else {
        //     document.getElementById("btnReinstate").removeAttribute("disabled");
        //   }
        //   if ((this.isSuspended == true || this.isSuspended == null) || this.isTerminated) {
        //     document.getElementById('btnSuspend').setAttribute("disabled", "disabled");
        //   } else {
        //     document.getElementById("btnSuspend").removeAttribute("disabled");
        //   }
        //   if (this.isTerminated) {
        //     document.getElementById('btnTermination').setAttribute("disabled", "disabled");
        //   }
        //   else {
        //     document.getElementById("btnTermination").removeAttribute("disabled");
        //   }
        // }
        this.onShowValue(response);
        this.enableEditandTab(response);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  enableEditandTab(response) {
    if (!response?.resources?.isTerminated) {// && response?.resources?.approvalStatus?.toLowerCase() != 'approved'
    //   // this.primengTableConfigProperties.tableComponentConfigs.tabsList.map(el => el.disabled = false);
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableViewBtn = true;
    }
  }

  onShowValue(response?: any) {
    this.dealerDetail = [
      {
        name: 'Basic Info', columns: [
          { name: 'Dealer Type', value: response ? response?.resources?.dealerTypeName : '', order: 1 },
          { name: 'Physical Address', value: response ? `${response?.resources?.streetNo}, ${response?.resources?.streetName}, ${response?.resources?.suburbName}` : '', order: 2 },
          { name: 'Dealer/IIP Code', value: response ? response?.resources?.dealerIIPCode : '', order: 3 },
          { name: ' ', value: response ? `${response?.resources?.cityName}, ${response?.resources?.postalCode}` : '', order: 4 },
          { name: 'Start Date', value: response ? response?.resources?.startDate : '', isDate: true, order: 5 },
          { name: ' ', value: response ? response?.resources?.provinceName : '', order: 6 },
          { name: 'Company Name', value: response ? response?.resources?.companyName : '', order: 7 },
          { name: 'Postal Address', value: response ? this.getPostalAddressLine1(response) : '', order: 8 },
          { name: 'Company Registration Number', value: response ? response?.resources?.companyRegNumber : '', order: 9 },
          { name: ' ', value: response ? this.getPostalAddressLine2(response) : '', order: 10 },
          { name: ' ', value: ' ', order: 11 },
          { name: ' ', value: response ? response?.resources?.postalAddress4 : '', order: 12 },
          { name: 'Vat Number', value: response ? response?.resources?.vatNumber : '', order: 13 },
          {
            name: ' ', isCheckbox: true, isName: true, className: 'p-relative', valueclassName: 'p-2 p-absolute', options: [{
              name: 'Authorized to remove radios and systems', checked: response ? response?.resources?.isAuthorizedToRemoveRS : '',
            }], order: 14
          },
          { name: 'Company PSIRA Registration Number', value: response ? response?.resources?.psiraRegNumber : '', order: 15 },
          { name: ' ', value: ' ', order: 16 },
          { name: 'Company PSIRA Expiry Date', value: response ? response?.resources?.psiraExpiryDate : '', isDate: true, order: 17 },
          { name: 'Suspension Date', value: response ? response?.resources?.suspensionDate : '', isDate: true, order: 18 },
          { name: 'Suspension Reason', value: response ? response?.resources?.dealerSuspensionReasonName : '', order: 19 },
          { name: 'Termination Date', value: response ? response?.resources?.terminationDate : '-', isDate: true, order: 20 },
          { name: 'Termination Reason', value: response ? response?.resources?.dealerTerminationReasonName : '-', order: 21 },
          { value: 'Suspension History', name: '', isName: true, valueColor: '#459CE8', valueWidth: 'auto', enableHyperLink: true, order: 22 },
        ]
      }
    ];
  }

  getPostalAddressLine1(response) {
    let arr = [];
    // if(response?.resources?.postalStreetNo) {
    //   arr.push(response?.resources?.postalStreetNo);
    // }
    // if(response?.resources?.postalStreetName) {
    //   arr.push(response?.resources?.postalStreetName);
    // }
    // if(response?.resources?.postalSuburbName) {
    //   arr.push(response?.resources?.postalSuburbName);
    // }
    if(response?.resources?.postalAddress1) {
      arr.push(response?.resources?.postalAddress1);
    }
    if(response?.resources?.postalAddress2) {
      arr.push(response?.resources?.postalAddress2);
    }
    return arr?.join(', ');
  }

  getPostalAddressLine2(response) {
    let arr = [];
    // if(response?.resources?.postalCityName) {
    //   arr.push(response?.resources?.postalCityName);
    // }
    // if(response?.resources?.postalPostalCode) {
    //   arr.push(response?.resources?.postalPostalCode);
    // }
    if(response?.resources?.postalAddress3) {
      arr.push(response?.resources?.postalAddress3);
    }
    if(response?.resources?.postalAddress5) {
      arr.push(response?.resources?.postalAddress5);
    }
    return arr?.join(', ');
  }


  redirectOnClicks(val) {
    if (val?.value == 'Suspension History') {
      this.openSuspensionDetailPopup();
    }
  }

  onEditButtonClicked(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
    this.router.navigate(['./../add-edit'], { relativeTo: this.activatedRoute, queryParams: { id: this.dealerId, dealerTypeId: this.dealerTypeId }, skipLocationChange: true })
  }
}
  openReinstatePopup() {

    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canReinstate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const ref = this.dialogService.open(DealerReinstateComponent, {
      header: 'Reinstate (Dealer and IIP)',
      showHeader: false,
      baseZIndex: 1000,
      width: '400px',
      // height: '300px',
      data: {
        row: { dealerId: this.dealerId, dealerSuspensionId: this.dealerSuspensionId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.viewValue();
        // this.getDealerMaintenanceListData()
      }
    });
  }
  openSuspensionPopup() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSuspend) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const ref = this.dialogService.open(DealerSuspensionComponent, {
      header: 'Suspensions (Dealer and IIP)',
      showHeader: false,
      baseZIndex: 1000,
      width: '63vw',
      // height: '470px',
      data: {
        row: { dealerId: this.dealerId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.viewValue();
        // this.getDealerMaintenanceListData()
      }
    });
  }
  openTerminationPopup() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canTermination) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    //DealerTerminationComponent
    const ref = this.dialogService.open(DealerTerminationComponent, {
      header: 'Dealer Termination',
      showHeader: false,
      baseZIndex: 1000,
      width: '63vw',
      // height: '470px',
      data: {
        row: { dealerId: this.dealerId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.viewValue();
        // this.getDealerMaintenanceListData()
      }
    });
  }
  openSuspensionDetailPopup() {
    const ref = this.dialogService.open(DealerSuspensionDetailComponent, {
      header: 'Suspensions History',
      showHeader: false,
      baseZIndex: 1000,
      width: '915px',
      // height: '450px',
      data: {
        row: { dealerId: this.dealerId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        // this.getDealerMaintenanceListData()
      }
    });
  }
  onSubmit(isSave = true) {
    this.dealerMaintenanceAddEditForm.markAsUntouched();
    if (this.dealerMaintenanceAddEditForm.invalid) {
      this.dealerMaintenanceAddEditForm.markAllAsTouched();
      return;
    }
    if (!this.viewable) {
      this.onAfterSubmit(isSave);
    }
  }

  onAfterSubmit(isSave) {
    const dealerObj = {
      ...this.dealerMaintenanceAddEditForm.getRawValue(),
      createdUserId: this.userData?.userId,
      // isActive: true,
    }
    dealerObj['startDate'] = this.datePipe.transform(dealerObj['startDate'], 'yyyy-MM-dd');
    dealerObj['psiraExpiryDate'] = this.datePipe.transform(dealerObj['psiraExpiryDate'], 'yyyy-MM-dd');
    // dealerObj['dealerTypeId'] = dealerObj['dealerTypeId']?.dealerTypeId;
    if (typeof dealerObj['dealerPhysicalAddress']?.formatedAddress == 'object') {
      dealerObj['dealerPhysicalAddress'].formatedAddress = dealerObj['dealerPhysicalAddress']?.formatedAddress?.description;
    }
    if (typeof dealerObj['dealerPostalAddress']?.formatedAddress == 'object') {
      dealerObj['dealerPostalAddress'].formatedAddress = dealerObj['dealerPostalAddress']?.formatedAddress?.description;
    }
    dealerObj['dealerPhysicalAddress']['createdUserId'] = this.userData?.userId;
    dealerObj['dealerPostalAddress']['createdUserId'] = this.userData?.userId;
    // dealerObj['isActive'] = this.dealerMaintenanceAddEditForm.get('isActive').value?.value;
    if (this.dealerId) {
      dealerObj['modifieduserId'] = this.userData?.userId;
      dealerObj['dealerId'] = this.dealerId;
    }
    this.dealerTypeId = dealerObj['dealerTypeId'];
    let api = this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER, dealerObj);
    if (this.dealerId) {
      api = this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER, dealerObj)
    }
    this.isSubmitted = true;
    api.subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        if ((isSave || !isSave) && !this.dealerId) {
          this.dealerId = response?.resources;
        }
        if (isSave && this.dealerId) {
          this.router.navigate(['/dealer/dealer-maintenance/info/view'], { queryParams: { tab: 0, id: this.dealerId, dealerTypeId: this.dealerTypeId, } });
        } else if (!isSave && this.dealerId) {
          this.router.navigate(['/dealer/dealer-maintenance'], { queryParams: { tab: 1, id: this.dealerId, dealerTypeId: this.dealerTypeId, } });
        }
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  ngOnDestroy() {
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
