import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerMaintenanceAddEditComponent } from '@modules/dealer';
import { NewAddressModule } from '@modules/sales/components/task-management/lead-info/new-address/new-address.module';
import { DealerReinstateModule } from '../dealer-reinstate/dealer-reinstate.module';
import { DealerSuspensionModule } from '../dealer-suspension/dealer-suspension.module';
import { DealerTerminationModule } from '../dealer-termination/dealer-termination.module';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';
@NgModule({
    declarations: [DealerMaintenanceAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        NewAddressModule,
        DealerSuspensionModule,
        DealerReinstateModule,
        DealerTerminationModule,
        RouterModule.forChild([
            { path: 'view', component: DealerMaintenanceAddEditComponent,  canActivate:[AuthGuard],data: { title: 'Dealer Maintenance View' } },
            { path: 'add-edit', component: DealerMaintenanceAddEditComponent, canActivate:[AuthGuard], data: { title: 'Dealer Maintenance Add Edit' } },
        ])
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [],
    exports: [],
})
export class DealerMaintenanceAddEditModule { }
