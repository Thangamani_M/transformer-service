import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerSuspensionComponent, DealerSuspensionDetailComponent } from '@modules/dealer';

@NgModule({
    declarations: [DealerSuspensionComponent, DealerSuspensionDetailComponent,],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerSuspensionComponent, DealerSuspensionDetailComponent],
    exports: [DealerSuspensionComponent, DealerSuspensionDetailComponent],
})
export class DealerSuspensionModule { }
