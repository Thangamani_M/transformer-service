import { Component, OnInit } from "@angular/core";
import { CrudService, ModulesBasedApiSuffix, RxjsService } from "@app/shared";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-dealer-suspension-detail',
    templateUrl: './dealer-suspension-detail-component.html',
    styleUrls: ['./dealer-suspension-component.scss']
  })
  
  export class DealerSuspensionDetailComponent implements OnInit {  
   
    suspensionDetailList=[];

    constructor(public ref: DynamicDialogRef,private rxjsService: RxjsService,public config: DynamicDialogConfig,private crudService: CrudService ){      
    }

    ngOnInit(): void {
        this.getRequiredData();
    }
    getRequiredData() {
          //this.config.data.row.dealerId
          this.crudService.get(
            ModulesBasedApiSuffix.DEALER,
            DealerModuleApiSuffixModels.DEALER_SUSPENSION_DETAILS,
            this.config.data.row.dealerId,null
          ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
             //  this.suspensionReasonList = data.resources;
             this.suspensionDetailList =  data.resources;
            }
              this.rxjsService.setGlobalLoaderProperty(false);
          });
    }

    btnCloseClick() {
        this.ref.close(false);
    }
  }