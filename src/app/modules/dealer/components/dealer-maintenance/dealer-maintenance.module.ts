import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerMaintenanceRoutingModule } from './dealer-maintenance-routing.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        DealerMaintenanceRoutingModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: []
})
export class DealerMaintenanceModule { }
