import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerCommentsHistoryComponent } from '@modules/dealer';

@NgModule({
    declarations: [DealerCommentsHistoryComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerCommentsHistoryComponent],
    exports: [DealerCommentsHistoryComponent],
})
export class DealerCommentsHistoryModule { }
