import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-dealer-comments-history',
  templateUrl: './dealer-comments-history.component.html'
})
export class DealerCommentsHistoryComponent implements OnInit {
  dealerCommentsHistoryDetail: any = [];
  constructor(public config: DynamicDialogConfig, public ref: DynamicDialogRef) {
    this.onLoadData();
  }

  ngOnInit(): void {
  }

  onLoadData() {
    this.config?.data.forEach(el => {
      if (el) {
        this.dealerCommentsHistoryDetail.push([
          { name: 'Document Type', value: el?.dealerDocumentTypeName },
          { name: 'Role', value: el?.roleName },
          { name: 'Comments', value: el?.reason },
          { name: 'Commented On', value: el?.commentedOn, isDateTime: true },
          { name: 'Commented By', value: el?.commentedBy },
        ])
      }
    });
  }

  btnCloseClick() {
    this.ref.close(false);
  }

}
