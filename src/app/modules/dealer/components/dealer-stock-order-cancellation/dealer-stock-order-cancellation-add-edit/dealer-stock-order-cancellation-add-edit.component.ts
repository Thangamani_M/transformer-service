import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { DealerStockOrderCreationModel } from "@modules/dealer/models/dealer-stock-order.model";
import { ItManagementApiSuffixModels, loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { forkJoin } from "rxjs";
@Component({
    selector: 'app-dealer-stock-order-cancellation-add-edit',
    templateUrl: './dealer-stock-order-cancellation-add-edit.component.html'
    // styleUrls: ['./dealer-stock-order-cancellation-add-edit.component.scss']
})
export class DealerStockOrderCancellationAddEditComponent { 
    stockOrderId: string;
    loggedUser;
    stockOrderCancellationForm: FormGroup;
    dealerStockLocationList = [];
    taxPercentage;
    stockOrderNumberList:any=[];
    isLoading:boolean=false;
    filteredStockCodes:any=[];
    filteredStockDescription:any=[];
    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder,
        private rxjsService: RxjsService, private crudService: CrudService,
        private snakBarService: SnackbarService) {
        this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
       // this.isEdit = this.activatedRoute.snapshot.queryParams.type == 'add' ? true : false;
       // this.stockOrderType = this.activatedRoute.snapshot.queryParams.type;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
     
    }
    ngOnInit() {
        this.createstockOrderCreationForm();
        this.getTax();
        this.getDropdown();
      //  if (this.stockOrderId)
           // this.getStockOrderId();
    }
    createstockOrderCreationForm(): void {
        let stockOrderModel = new DealerStockOrderCreationModel();
        // create form controls dynamically from model class
        this.stockOrderCancellationForm = this.formBuilder.group({
            dealerStockOrderItemsPostDTO: this.formBuilder.array([])
        });
        Object.keys(stockOrderModel).forEach((key) => {
            this.stockOrderCancellationForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
        this.stockOrderCancellationForm = setRequiredValidator(this.stockOrderCancellationForm, ["stockOrderNumber"]);
        //this.onValueChanges();
    }
    getTax() {
        let api = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_TAX, null, false, null);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200 && response.resources) {
                this.taxPercentage = response.resources.taxPercentage;
            }
        });
    }
    getDropdown() {
        forkJoin([
            this.crudService.dropdown(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.UX_DEALER_STOCK_ORDER_CANCELLATION, undefined, 1)
        ]).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.stockOrderNumberList = resp.resources;
                           // this.stockOrderCreationForm.get('warehouseId').setValue(this.loggedUser.warehouseId);
                            break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        });

    }
    
    onSelectStockId(stockId: string) {
        let api = this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_CANCELLATION, stockId, false, null);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200 && response.resources) {
                  let dealerStockOrderCancellationDetail = response.resources.dealerStockOrderCancellationDetail;
                  this.stockOrderId = dealerStockOrderCancellationDetail.stockOrderId;                  
                  this.stockOrderCancellationForm.get('stockOrderNumber').setValue(this.stockOrderCancellationForm.get('stockOrderNumber')?.value);
                  this.stockOrderCancellationForm.get('warehouseId').setValue(dealerStockOrderCancellationDetail?.warehouse);
                  this.stockOrderCancellationForm.get('dealerId').setValue(dealerStockOrderCancellationDetail?.dealerCode);
                  this.stockOrderCancellationForm.get('dealerName').setValue(dealerStockOrderCancellationDetail?.dealerName);
                  this.stockOrderCancellationForm.get('dealerStockLocationId').setValue(dealerStockOrderCancellationDetail?.dealerLocation);
                  this.stockOrderCancellationForm.get('priorityName').setValue(dealerStockOrderCancellationDetail?.priorityName);
                  this.stockOrderCancellationForm.get('RQNStatusName').setValue(dealerStockOrderCancellationDetail?.createdBy);
                  if (response.resources.dealerStockOrderCancellationItemDetail && response.resources.dealerStockOrderCancellationItemDetail.length > 0) {
                    response.resources.dealerStockOrderCancellationItemDetail.forEach((stockOrder) => {
                        let dealerTypeItem = this.formBuilder.group({              
                            stockCode: [{value:stockOrder.itemCode, disabled:true}],
                            stockDescription: [{value:stockOrder.itemName, disabled:true}],
                            quantity:  [{value:stockOrder.quantity, disabled:true}],
                            collectedQuantity: [{value:stockOrder.collectedQuantity, disabled:true}],
                            outstandingQuantity: [{value:stockOrder.varianceQuantity, disabled:true}],
                            itemId:stockOrder.itemId
                        });
                        this.getStockOrderItemsArray.push(dealerTypeItem);
                    });
                }
            };
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    getDealerStockList(params) {
        let api = this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.UX_DEALER_BRANCH, null, false, prepareRequiredHttpParams(params));
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200 && response.resources) {
                this.dealerStockLocationList = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    get getStockOrderItemsArray(): FormArray {
        if (this.stockOrderCancellationForm !== undefined) {
            return (<FormArray>this.stockOrderCancellationForm.get('dealerStockOrderItemsPostDTO'));
        }
    }
    onStatucCodeSelected(value, type,i){

    }
    onSubmit() {
        if (this.stockOrderCancellationForm.invalid ){
            return;
        }
        let formValue =  this.stockOrderCancellationForm.value;
        let finalObj={
            createdUserId :this.loggedUser.userId,
            stockOrderId :this.stockOrderId
        }    
        let api =  this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_CANCELLATION, finalObj);
           api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigateToList();
            }
        });
    };
    actionOnLeavingComponent() {
    //   this.isEdit = !this.isEdit
    }
    navigateToList() {
        this.router.navigate(['/dealer/dealer-stock-order-cancellation']);
    }
}