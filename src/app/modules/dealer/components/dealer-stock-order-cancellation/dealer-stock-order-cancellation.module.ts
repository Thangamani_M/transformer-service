import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerStockOrderCancellationAddEditComponent } from './dealer-stock-order-cancellation-add-edit/dealer-stock-order-cancellation-add-edit.component';
import { DealerStockOrderCancellationRoutingModule } from './dealer-stock-order-cancellation-routing.module';
import { DealerStockOrderCancellationComponent } from './dealer-stock-order-cancellation/dealer-stock-order-cancellation.component';

@NgModule({
    declarations: [ DealerStockOrderCancellationComponent,DealerStockOrderCancellationAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        DealerStockOrderCancellationRoutingModule
    ],
    entryComponents: [],
    providers: [
        DatePipe
    ]
})
export class DealerStockOrderCancellationModule { }
