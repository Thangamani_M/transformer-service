import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerStockOrderCancellationAddEditComponent } from './dealer-stock-order-cancellation-add-edit/dealer-stock-order-cancellation-add-edit.component';
import { DealerStockOrderCancellationComponent } from './dealer-stock-order-cancellation/dealer-stock-order-cancellation.component';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: DealerStockOrderCancellationComponent, canActivate: [AuthGuard], data: { title: 'Dealer Stock Order Cancellation List' } },
  { path: 'add-edit', component: DealerStockOrderCancellationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Dealer Stock Order Cancellation Cancel' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class DealerStockOrderCancellationRoutingModule { }
