import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddClawbackComponent } from './add-clawback/add-clawback.component';
import { ClawbackListComponent } from './clawback-list.component';

const routes: Routes = [
  { path: '', component: ClawbackListComponent, data: { title: "Clawback List" } },
  { path: 'create', component: AddClawbackComponent, data: { title: "Create Clawback" } },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ClawbackRoutingModule { }
