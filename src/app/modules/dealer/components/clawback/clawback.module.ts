import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AddClawbackComponent } from './add-clawback/add-clawback.component';
import { ClawbackListComponent } from './clawback-list.component';
import { ClawbackRoutingModule } from './clawback-routing.module';
@NgModule({
  declarations: [ClawbackListComponent, AddClawbackComponent],
  imports: [
    CommonModule,LayoutModule,SharedModule,ReactiveFormsModule,MaterialModule,FormsModule,
    ClawbackRoutingModule
  ]
})
export class ClawbackModule { }
