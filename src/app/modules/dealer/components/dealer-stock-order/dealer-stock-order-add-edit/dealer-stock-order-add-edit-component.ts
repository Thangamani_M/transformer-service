import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { DealerStockOrderCreationModel } from "@modules/dealer/models/dealer-stock-order.model";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { ItManagementApiSuffixModels, loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest, forkJoin, Observable } from "rxjs";
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';

@Component({
    selector: 'app-dealer-stock-order-add-edit',
    templateUrl: './dealer-stock-order-add-edit-component.html',
    styles: [`
        ::ng-deep .fidelity-autocomplete-dropdown-con.dealer-stock-order .form-group mat-form-field.mat-form-field {
            border: none !important;
        }
    `]
})
export class DealerStockOrderAddEditComponent {

    stockOrderId: string;
    stockOrderCreationForm: FormGroup;
    werehouseDropDown = [];
    dealerStockLocationList = [];
    requestWarehouseList = [];
    dealerStockOrderItemsPostDTO;
    dealerCodeDropDown = [];
    loggedInUserData;
    filteredStockCodes = [];
    filteredStockDescription = [];
    isLoading: boolean;
    searchTerm: string;
    loggedUser;
    taxPercentage;
    isEdit: boolean = false;
    stockOrderType: string;
    stockItems;
    isAddedErr: boolean = false;
    isStockCodeLoading: any = [];
    isStockDescLoading: any = [];
    isInvalidStockCode: boolean;
    isInvalidStockDesc: boolean;
    stockSubscription: any;
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    status: string;

    constructor(
        private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
        private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder,
        private rxjsService: RxjsService, private crudService: CrudService,) {
        // this.stockOrderId = this.activatedRoute?.snapshot?.queryParams?.id;
        // this.isEdit = (this.activatedRoute?.snapshot?.queryParams?.type == 'add' || this.activatedRoute?.snapshot?.queryParams?.type == 'update') ? true : false;
        // this.stockOrderType = this.activatedRoute?.snapshot?.queryParams?.type;
        // this.status = this.activatedRoute?.snapshot?.queryParams?.status;
        this.activatedRoute.queryParamMap.subscribe((params: any) => {
            this.stockOrderId = params?.params?.id ? params['params']['id'] : '';
            this.stockOrderType = params?.params?.type ? params['params']['type'] : '';
            this.isEdit = params?.params?.type ? (params['params']['type'] == 'add' || params['params']['type'] == 'update') ? true : false : false;
            this.status = params?.params?.status ? params['params']['status'] : '';
            this.combineLatestNgrxStoreData();
            this.createstockOrderCreationForm();
            this.onLoadValue();
        });
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });

    }
    ngOnInit() {
    }

    combineLatestNgrxStoreData() {
        combineLatest(
            this.store.select(loggedInUserData)
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    createstockOrderCreationForm(): void {
        let stockOrderModel = new DealerStockOrderCreationModel();
        // create form controls dynamically from model class
        this.stockOrderCreationForm = this.formBuilder.group({
            dealerStockOrderItemsPostDTO: this.formBuilder.array([])
        });
        Object.keys(stockOrderModel).forEach((key) => {
            this.stockOrderCreationForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
        if (this.stockOrderId) {
            this.stockOrderCreationForm = setRequiredValidator(this.stockOrderCreationForm, ["warehouseId", "dealerId", "dealerBranchId"]);
        } else {
            this.stockOrderCreationForm = setRequiredValidator(this.stockOrderCreationForm, ["warehouseId", "dealerId", "dealerBranchId"]);
        }
        this.stockOrderCreationForm.get('RQNStatusName').setValue(this.loggedUser?.displayName);
        this.stockOrderCreationForm.get('priorityName').patchValue('Standard');
        if (!this.stockOrderId) {
            this.onValueChanges();
        }
        let dealerTypeItem = this.formBuilder.group({
            consumable: '',
            quantity: [{ value: '', disabled: !this.isEdit }],
            stockCode: [{ value: '', disabled: !this.isEdit }],
            stockDescription: [{ value: '', disabled: !this.isEdit }],
            totalPrice: '',
            unitPrice: '',
            itemId: '',
        })
        dealerTypeItem = setRequiredValidator(dealerTypeItem, ["stockCode", "stockDescription", "quantity",]);
        this.getStockOrderItemsArray.insert(0, dealerTypeItem);
    }

    onValueChanges() {
        this.stockOrderCreationForm.get('dealerId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),)
            .subscribe((id: any) => {
                if (id) {
                    let filterData = this.dealerCodeDropDown.filter(x => x.id == id);
                    if (filterData && filterData.length > 0) {
                        this.stockOrderCreationForm.get('dealerName').setValue(filterData[0].dealerIIPCode);
                        let params = { isAll: true, dealerId: id }
                        this.getDealerStockList(params);
                    }
                }
            });
    }

    stockCodeList(val, type) {
        let params = { searchText: val, isDealer: true, isAll: false };
        let apiUrl = InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE;
        if (type == 'desc') {
            apiUrl = InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME;
        }
        return this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            apiUrl, null, false,
            prepareRequiredHttpParams(params))
    }

    calculateTax() {
        let total = 0; let vat = 0; let totalOrderValue = 0;
        for (let i = 0; i < this.getStockOrderItemsArray.length; i++) {
            total += +this.getStockOrderItemsArray.controls[i].get('totalPrice').value;
        }
        if (total) {
            vat = +total * (+this.taxPercentage / 100);
            totalOrderValue = +total + +vat;
        }
        total = total ? +total?.toFixed(2) : 0;
        vat = vat ? +vat?.toFixed(2) : 0;
        totalOrderValue = totalOrderValue ? +totalOrderValue?.toFixed(2) : 0;
        this.stockOrderCreationForm.get('total').setValue(total);
        this.stockOrderCreationForm.get('vat').setValue(vat);
        this.stockOrderCreationForm.get('totalOrderValue').setValue(totalOrderValue);
    }

    onLoadValue() {
        let api = [
            this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSES, prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
            this.crudService.dropdown(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.UX_DEALER, prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_TAX, null, false, null)
        ];
        if (this.stockOrderId) {
            api.push(this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_STOCK_ORDER, this.stockOrderId, false, null));
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        forkJoin(api).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.requestWarehouseList = resp.resources;
                            this.stockOrderCreationForm.get('warehouseId').setValue(this.loggedUser?.warehouseId ? this.loggedUser?.warehouseId : '');
                            break;
                        case 1:
                            this.dealerCodeDropDown = resp.resources;
                            break;
                        case 2:
                            this.taxPercentage = resp.resources?.taxPercentage;
                            break;
                        case 3:
                            this.getStockOrderId(resp);
                            break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        });

    }
    getStockOrderId(response) {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
            let stockOrderModel = new DealerStockOrderCreationModel(response.resources);
            this.stockOrderCreationForm.get('dealerId').setValue(response.resources?.dealerId);
            let params = { isAll: true, dealerId: response.resources?.dealerId }
            this.getDealerStockList(params, false);
            this.stockOrderCreationForm.patchValue(stockOrderModel, { emitEvent: false });
            this.clearFormArray(this.getStockOrderItemsArray);
            if (response.resources.dealerStockOrdersItemDetailsDTO && response.resources.dealerStockOrdersItemDetailsDTO.length > 0) {
                response.resources.dealerStockOrdersItemDetailsDTO.forEach((stockOrder) => {
                    let dealerTypeItem = this.formBuilder.group({
                        consumable: (stockOrder.consumable == 'Yes' || stockOrder.consumable == 'No') ? stockOrder.consumable : stockOrder.consumable == true ? 'Yes' : 'No',
                        quantity: stockOrder.quantity,
                        stockCode: [{ value: stockOrder.stockCode, disabled: true }],
                        stockDescription: [{ value: stockOrder.stockName, disabled: true }],
                        totalPrice: stockOrder.totalPrice,
                        unitPrice: stockOrder.unitPrice,
                        itemId: stockOrder.stockOrderItemId
                    });
                    dealerTypeItem = setRequiredValidator(dealerTypeItem, ["quantity"]);
                    this.getStockOrderItemsArray.insert(0, dealerTypeItem);
                });
                this.calculateTax();
            }

        }
        this.stockOrderCreationForm.get('priorityName').patchValue('Standard');
        this.stockOrderCreationForm.get('RQNStatusName').setValue(this.loggedUser?.displayName);
    }

    validateExist() {
        const currItem = this.getStockOrderItemsArray.getRawValue()[0]?.itemId;
        const findItem = this.getStockOrderItemsArray.getRawValue().find((el, index) => el?.itemId == currItem && 0 != index);
        return findItem ? true : false;
      }

    //Add Employee Details
    addDealerStockOrderItem() {
        if (this.getStockOrderItemsArray.invalid) {
            this.getStockOrderItemsArray.markAllAsTouched();
            return;
        } else if (this.validateExist()) {
            this.snackbarService.openSnackbar(`Dealer Stock Order item already exists`, ResponseMessageTypes.WARNING);
            return;
        }
        let stockOrder = {
            consumable: this.getStockOrderItemsArray?.controls[0].get("consumable").value, quantity: this.getStockOrderItemsArray?.controls[0].get("quantity").value, stockCode: this.getStockOrderItemsArray?.controls[0].get("stockCode").value,
            stockDescription: this.getStockOrderItemsArray?.controls[0].get("stockDescription").value, totalPrice: this.getStockOrderItemsArray?.controls[0].get("totalPrice").value, unitPrice: this.getStockOrderItemsArray?.controls[0].get("unitPrice").value,
            itemId: this.getStockOrderItemsArray?.controls[0].get("itemId").value
        }
        let dealerTypeItem = this.formBuilder.group({
            consumable: (stockOrder.consumable == 'Yes' || stockOrder.consumable == 'No') ? stockOrder.consumable : stockOrder.consumable == true ? 'Yes' : 'No',
            quantity: stockOrder.quantity?.toString() ? +stockOrder.quantity : 0,
            stockCode: stockOrder.stockCode,
            stockDescription: stockOrder.stockDescription,
            totalPrice: stockOrder.totalPrice?.toString() ? (+stockOrder.totalPrice)?.toFixed(2) : 0,
            unitPrice: stockOrder.unitPrice?.toString() ? (+stockOrder.unitPrice)?.toFixed(2) : 0,
            itemId: stockOrder.itemId
        })
        dealerTypeItem = setRequiredValidator(dealerTypeItem, ["stockCode", "stockDescription", "quantity",]);
        this.getStockOrderItemsArray.insert(0, dealerTypeItem);
        this.isAddedErr = false;
        this.getStockOrderItemsArray?.controls[0].get('quantity').setValue('');
        this.getStockOrderItemsArray?.controls[0].get('consumable').setValue('');
        this.getStockOrderItemsArray?.controls[0].get('stockCode').setValue('');
        this.getStockOrderItemsArray?.controls[0].get('totalPrice').setValue('');
        this.getStockOrderItemsArray?.controls[0].get('unitPrice').setValue('');
        this.getStockOrderItemsArray?.controls[0].get('stockDescription').setValue('');
        this.getStockOrderItemsArray?.controls[0].get('itemId').setValue('');
        this.filteredStockCodes = [];
        this.filteredStockDescription = [];
        this.getStockOrderItemsArray?.controls[0].markAsUntouched();
    }

    //Clear from arry
    clearFormArray = (formArray: FormArray) => {
        while (formArray.length !== 0) {
            formArray.removeAt(0);
        }
    };

    get getStockOrderItemsArray(): FormArray {
        if (this.stockOrderCreationForm !== undefined) {
            return (<FormArray>this.stockOrderCreationForm.get('dealerStockOrderItemsPostDTO'));
        }
    }

    removeStockOrderItems(index) {
        let total = 0; let vat = 0; let totalOrderValue = 0;
        total = this.stockOrderCreationForm.get('total').value - this.getStockOrderItemsArray.controls[index].get('totalPrice').value;
        vat = total * (this.taxPercentage / 100);
        totalOrderValue = total + vat;
        this.stockOrderCreationForm.get('total').setValue(total);
        this.stockOrderCreationForm.get('vat').setValue(vat);
        this.stockOrderCreationForm.get('totalOrderValue').setValue(totalOrderValue);
        this.getStockOrderItemsArray.removeAt(index);
    }

    onDealerCodeChange(event) {
        if (event) {
            let id = event.target.value;
            let filterData = this.dealerCodeDropDown.filter(x => x.id == id);
            if (filterData && filterData.length > 0) {
                this.stockOrderCreationForm.get('dealerName').setValue(filterData[0].dealerIIPCode);
                let params = { isAll: true, dealerId: filterData[0].id }
                this.getDealerStockList(params);
            }
        }
    }

    getDealerStockList(params, change = true) {
        let api = this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.UX_DEALER_BRANCH, null, false, prepareRequiredHttpParams(params));
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.dealerStockLocationList = response.resources;
                if (change) {
                    this.stockOrderCreationForm.get('dealerBranchId').setValue('');
                }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onChangeStockCode(event, type, index?: any) {
        if (event.target.value) {
            let params = { searchText: event.target.value, isDealer: true, isAll: false };
            this.searchTerm = event.target.value;
            let api = (type == 'stock-code') ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, false, prepareRequiredHttpParams(params)) : this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, false, prepareRequiredHttpParams(params));
            if(type == 'stock-code') {
                this.isStockCodeLoading[index] = true;
                this.filteredStockCodes = [];
            } else if(type == 'stock-description') {
                this.isStockDescLoading[index] = true;
                this.filteredStockDescription = [];
            }
            if (this.stockSubscription && !this.stockSubscription?.closed) {
                this.stockSubscription.unsubscribe();
            }
            this.stockSubscription = api.subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.resources?.length) {
                    if(type == 'stock-code') {
                        this.filteredStockCodes = response.resources;
                        this.getStockOrderItemsArray.controls[index].get('stockCode').setErrors(null, { emitEvent: false });
                    } else if(type == 'stock-description') {
                        this.filteredStockDescription = response.resources;
                        this.getStockOrderItemsArray.controls[index].get('stockDescription').setErrors(null, { emitEvent: false });
                    }
                    this.getStockOrderItemsArray.controls[index].updateValueAndValidity();
                } else {
                    if(type == 'stock-code') {
                        this.filteredStockCodes = [];
                        this.getStockOrderItemsArray.controls[index].get('stockCode').setErrors({invalid: true}, { emitEvent: false });
                    } else if(type == 'stock-description') {
                        this.filteredStockDescription = [];
                        this.getStockOrderItemsArray.controls[index].get('stockDescription').setErrors({invalid: true}, { emitEvent: false });
                    }
                    this.getStockOrderItemsArray.controls[index].updateValueAndValidity();
                }
                if(type == 'stock-code') {
                    this.isStockCodeLoading[index] = false;
                } else if(type == 'stock-description') {
                    this.isStockDescLoading[index] = false;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
            if(this.getStockOrderItemsArray.controls[index].get('quantity').value) {
                this.calculateTax();
            }
        } else {
            this.getStockOrderItemsArray.controls[index].get('unitPrice').setValue('');
            this.getStockOrderItemsArray.controls[index].get('consumable').setValue('');
            this.getStockOrderItemsArray.controls[index].get('itemId').setValue('');
            this.getStockOrderItemsArray.controls[index].get('stockCode').setValue('', { emitEvent: false });
            this.getStockOrderItemsArray.controls[index].get('stockCode').markAsUntouched();
            // this.getStockOrderItemsArray.controls[index].get('stockCode').setErrors(null, { emitEvent: false });
            this.getStockOrderItemsArray.controls[index].get('stockDescription').setValue('', { emitEvent: false });
            this.getStockOrderItemsArray.controls[index].get('stockDescription').markAsUntouched();
            // this.getStockOrderItemsArray.controls[index].get('stockDescription').setErrors(null, { emitEvent: false });
        }
    }

    onStatucCodeSelected(value, type, index?: any) {
        let TYPE = (type == 'stock-description') ? 'stockDescription' : 'stockCode';
        if (value) {
            let filter = (type == 'stock-code' && this.filteredStockCodes) ? this.filteredStockCodes.find(x => x.displayName == value) : (type == 'stock-description' && this.filteredStockDescription) ? this.filteredStockDescription.find(x => x.displayName == value) : null;
            if (filter) {
                this.getPictureProfile(filter?.id).subscribe((res) => {
                    if (index || index == 0) {
                        this.getStockOrderItemsArray.controls[index].get(TYPE).setValue(filter?.displayName);
                        this.getStockOrderItemsArray.controls[index].get('unitPrice').setValue(filter?.sellingPrice);
                        this.getStockOrderItemsArray.controls[index].get('consumable').setValue(filter?.consumable);
                        this.getStockOrderItemsArray.controls[index].get('itemId').setValue(filter?.id);
                        this.getStockOrderItemsArray.controls[index].get('stockCode').setValue(res.resources.itemCode, { emitEvent: false });
                        this.getStockOrderItemsArray.controls[index].get('stockCode').setErrors(null, { emitEvent: false });
                        this.getStockOrderItemsArray.controls[index].get('stockDescription').setValue(res.resources.itemName, { emitEvent: false });
                        this.getStockOrderItemsArray.controls[index].get('stockDescription').setErrors(null, { emitEvent: false });
                        if (type == 'stock-description') {
                            this.filteredStockCodes = [{
                                displayName: res.resources?.itemCode,
                                id: filter?.id,
                                sellingPrice: filter?.sellingPrice,
                                consumable: filter?.consumable,
                                itemName: res?.resources?.itemName,
                            }];
                            this.filteredStockDescription = [{
                                displayName: filter?.displayName,
                                id: filter?.id,
                                sellingPrice: filter?.sellingPrice,
                                consumable: filter?.consumable,
                                itemCode: res?.resources?.itemCode,
                            }];
                        } else if (type == 'stock-code') {
                            this.filteredStockCodes = [{
                                displayName: filter?.displayName,
                                id: filter?.id,
                                sellingPrice: filter?.sellingPrice,
                                consumable: filter?.consumable,
                                itemName: res?.resources?.itemName,
                            }];
                            this.filteredStockDescription = [{
                                displayName: res?.resources?.itemName,
                                id: filter?.id,
                                sellingPrice: filter?.sellingPrice,
                                consumable: filter?.consumable,
                                itemCode: res?.resources?.itemCode,
                            }];
                        }
                        this.onCalculateRequestedQty(index);
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
            }
        }
    }

    getPictureProfile(id): Observable<any> {
        return this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.ITEM,
            id,
            false,
            prepareRequiredHttpParams({})
        ).pipe(
            map(result => {
                return result;
            })
        );
    }

    onChangeRequestedQty(index?: any) {
        this.onCalculateRequestedQty(index);
    }

    onCalculateRequestedQty(index) {
        let val = this.getStockOrderItemsArray.controls[index].get('quantity').value;
        if ((index || index == 0) && val?.toString()) {
            if(val == 0) {
                this.snackbarService.openSnackbar("Please enter valid Requested Qty", ResponseMessageTypes.WARNING);
                return;
            }
            let sellingPrice = this.getStockOrderItemsArray.controls[index].get('unitPrice').value;
            this.getStockOrderItemsArray.controls[index].get('totalPrice').setValue((sellingPrice * val)?.toFixed(2));
            this.calculateTax();
        }
    }

    validateExistonSubmit() {
        let arr = [];
        this.getStockOrderItemsArray.getRawValue()?.forEach((el, i) => {
            this.getStockOrderItemsArray.getRawValue()?.forEach((el1, j) => {
                if(i != j && el?.itemId?.toLowerCase() == el1?.itemId?.toLowerCase()) {
                    arr.push(true);
                  }
            });
        });
        return arr?.length ? true : false;
    }

    onSubmit(type) {
        if (!this.stockOrderCreationForm.dirty) {
            this.snackbarService.openSnackbar("No changes were detected", ResponseMessageTypes.WARNING);
            return;
        }
        if (this.stockOrderCreationForm.invalid) {
            this.stockOrderCreationForm.markAllAsTouched();
            return;
        } else if (this.validateExistonSubmit()) {
            this.snackbarService.openSnackbar(`Dealer Stock Order item already exists`, ResponseMessageTypes.WARNING);
            return;
        }
        this.stockOrderCreationForm.markAsUntouched();
        let formValue = this.stockOrderCreationForm.value;
        formValue.createdUserId = this.loggedUser.userId;
        formValue.RQNStatusName = type;
        formValue.stockOrderId = this.stockOrderId;
        let crudService: Observable<IApplicationResponse> = !this.stockOrderId ? this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_STOCK_ORDER, formValue) :
            this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_STOCK_ORDER, formValue)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigateToList();
            }
        });
    };

    actionOnLeavingComponent() {
        // this.isEdit = !this.isEdit
        this.router.navigate(['/dealer/dealer-stock-order/add-edit'], {
            queryParams: {
                type: 'update',
                id: this.stockOrderId,
                status: this.status,
            }, skipLocationChange: true
        });
    }
    navigateToList() {
        this.router.navigate(['/dealer/dealer-stock-order']);
    }
}
