import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerStockOrderAddEditComponent } from './dealer-stock-order-add-edit/dealer-stock-order-add-edit-component';
import { DealerStockOrderListComponent } from './dealer-stock-order-list/dealer-stock-order-list-component';
import { DealerStockOrderRoutingModule } from './dealer-stock-order-routing.module';

@NgModule({
    declarations: [ DealerStockOrderListComponent,DealerStockOrderAddEditComponent],
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        DealerStockOrderRoutingModule
    ],
    entryComponents: [],
    providers: [
        DatePipe
    ]
})
export class DealerStockOrderModule { }
