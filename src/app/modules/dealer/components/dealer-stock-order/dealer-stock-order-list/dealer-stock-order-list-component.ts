import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from "@modules/dealer";
import { DealerStockOrderModel } from "@modules/dealer/models/dealer-stock-order.model";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { TechnicalMgntModuleApiSuffixModels } from "@modules/technical-management/shared/enum.ts/technical.enum";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-dealer-stock-order-list',
  templateUrl: './dealer-stock-order-list-component.html',
})
export class DealerStockOrderListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {}
  orderTypeList: any = [];
  loggedUser: any;
  orderList: any = [];
  statusList = [];
  showFilterForm: boolean = false;
  startTodayDate = 0;
  filterData;
  dealerStockFilterForm: FormGroup;
  first: any = 0;

  constructor(private momentService: MomentService, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private router: Router, private formBuilder: FormBuilder, private datePipe: DatePipe,
    private snackbarService: SnackbarService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Dealer  Stock Order",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Stock Order', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Stock Order',
            dataKey: 'stockOrderId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'stockOrderNumber', header: 'Stock Order Number', width: '150px' },
              { field: 'dealerCode', header: 'Dealer Code', width: '150px' },
              { field: 'dealerName', header: 'Dealer Name', width: '150px' },
              { field: 'batchBarCode', header: 'Batch Bar Code', width: '150px' },
              { field: 'createdBy', header: 'Created By', width: '150px' },
              { field: 'createdDate', header: 'Created Date', width: '150px' },
              { field: 'priorityName', header: 'Priority', width: '150px' },
              { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '150px' },
              { field: 'status', header: 'Status', width: '150px' },
            ],
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: true,
            enableAddActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_STOCK_ORDER,
            moduleName: ModulesBasedApiSuffix.DEALER,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createFilterForm();
    this.getDealerStockOrderListData();
    this.getOrderTypeDropDown();
    this.getDropDown();
    this.getOrdersDropDown();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_STOCK_ORDER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createFilterForm(): void {
    let filterFormModel = new DealerStockOrderModel();
    // create form controls dynamically from model class
    this.dealerStockFilterForm = this.formBuilder.group({});
    Object.keys(filterFormModel).forEach((key) => {

      this.dealerStockFilterForm.addControl(key, new FormControl());
    });
    this.rxjsService.setGlobalLoaderProperty(false);

  }
  getDropDown(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.UX_RQNSTATUS, undefined)
      .subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.statusList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }

      });
  }
  getOrderTypeDropDown(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_TYPES, undefined)
      .subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.orderList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }

      });
  }
  getOrdersDropDown(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_TYPES, undefined)
      .subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.orderTypeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }

      });
  }
  getDealerStockOrderListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { ...otherParams, ...{ userId: this.loggedUser.userId } }
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.createdDate = this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }
  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar }
        this.getDealerStockOrderListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      default:
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/dealer/dealer-stock-order/add-edit'], {
          queryParams: { type: 'add' }
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/dealer/dealer-stock-order/add-edit'], {
          queryParams: {
            type: 'view',
            id: editableObject['stockOrderId']
          }
        });
        break;
    }
  }
  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
  }
  cancel() {
    this.showFilterForm = !this.showFilterForm;
  }
  resetForm() {
    this.dealerStockFilterForm.reset();
    this.getDealerStockOrderListData();
    this.showFilterForm = !this.showFilterForm;
  }
  submitFilter() {
    let formValue = this.dealerStockFilterForm.getRawValue();
    if (formValue.dealerFrom)
      formValue.dealerFrom = (formValue.dealerFrom && formValue.dealerFrom != null) ? this.momentService.toMoment(formValue.dealerFrom).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    if (formValue.dealerTo)
      formValue.dealerTo = (formValue.dealerFrom && formValue.dealerTo != null) ? this.momentService.toMoment(formValue.dealerTo).format('YYYY-MM-DDThh:mm:ss[Z]') : null;

    let statusIds = [];
    if (formValue.statusIds && formValue.statusIds.length > 0) {
      formValue.statusIds.forEach(element => {
        statusIds.push(element.id)
      });

    }
    let orderTypeIds = [];
    if (formValue.orderTypeIds && formValue.orderTypeIds.length > 0) {
      formValue.orderTypeIds.forEach(element => {
        orderTypeIds.push(element.id)
      });
    }
    let stockOrderIds = [];
    if (formValue.stockOrderIds && formValue.stockOrderIds.length > 0) {
      formValue.stockOrderIds.forEach(element => {
        stockOrderIds.push(element.id)
      });
    }
    this.filterData = {
      stockOrderIds: stockOrderIds,
      dealerIds: formValue.dealerIds,
      dealerFrom: formValue.dealerFrom,
      dealerTo: formValue.dealerTo,
      statusIds: statusIds,
      orderTypeIds: orderTypeIds,
      dealerName: formValue.dealerName,
    }
    Object.keys(this.filterData).forEach(key => {
      if (this.filterData[key] === "" || this.filterData[key] == null)
        delete this.filterData[key];

      if (this.filterData['orderTypeIds'] && this.filterData['orderTypeIds'].length == 0)
        delete this.filterData['orderTypeIds'];
      if (this.filterData['statusIds'] && this.filterData['statusIds'].length == 0)
        delete this.filterData['statusIds'];
      if (this.filterData['stockOrderIds'] && this.filterData['stockOrderIds'].length == 0)
        delete this.filterData['stockOrderIds'];
    });
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
