import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerStockOrderAddEditComponent } from './dealer-stock-order-add-edit/dealer-stock-order-add-edit-component';
import { DealerStockOrderListComponent } from './dealer-stock-order-list/dealer-stock-order-list-component';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: DealerStockOrderListComponent, canActivate: [AuthGuard], data: { title: 'Dealer Stock Order List' } },
  { path: 'add-edit', component: DealerStockOrderAddEditComponent, canActivate: [AuthGuard], data: { title: 'Dealer Stock Order' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class DealerStockOrderRoutingModule { }
