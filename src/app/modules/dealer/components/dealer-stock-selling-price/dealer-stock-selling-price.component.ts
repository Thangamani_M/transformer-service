import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService ,currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService, exportList} from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-dealer-stock-selling-price',
  templateUrl: './dealer-stock-selling-price.component.html',
  styleUrls: ['./dealer-stock-selling-price.component.scss']
})
export class DealerStockSellingPriceComponent extends PrimeNgTableVariablesModel implements OnInit {

  dataList: any;
  primengTableConfigProperties: any;
  row: any = {};
  listSubscription: any;
  first: any = 0;
  filterData: any;
  otherParams:any
  constructor(private crudService: CrudService,private snackbarService:SnackbarService,
    private rxjsService: RxjsService,private store: Store<AppState>,private router: Router,private activatedRoute: ActivatedRoute,private datePipe: DatePipe,) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Stock Table Selling Price",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Dealer Stock Table Selling Price' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Stock Table Selling Price',
            dataKey: 'dealerItemSellingPriceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enablePrintBtn: true,
            printTitle: 'Dealer Stock Table Selling Price',
            printSection: 'print-section0',
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'itemSellingPriceCode', header: 'Selling Price ID', width: '100px' },
              { field: 'stockCode', header: 'Stock Code', width: '80px' },
              { field: 'stockDescription', header: 'Stock Description', width: '250px' },
              { field: 'sellingPrice', header: 'Selling Price', width: '100px', isDecimalFormat: true },
              { field: 'systemType', header: 'System Type', width: '100px' },
              { field: 'componentGroup', header: 'Component Group', width: '130px' },
              { field: 'spLastUpdate', header: 'SP - Last Updated', width: '140px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableScheduleActionBtn: true,
            enableExportCSV: false,
            enableExportBtn: true,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.DEALER_STOCK_TABLE_SELLING_PRICE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col)
    }
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    otherParams = {
      userId: this.loggedInUserData?.userId,
      ...otherParams,
    }
    this.otherParams = otherParams;
    this.listSubscription = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.spLastUpdate = this.datePipe.transform(val.spLastUpdate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    }, error => {
      this.dataList = null;
      this.totalRecords = 0;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar, IsAll: true };
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.SCHEDULE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canSchedule) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        this.router.navigate(['/dealer/dealer-stock-selling-price/dealer-stock-scheduling']);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        if (this.selectedTabIndex == 0) {
          this.router.navigate(['./view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey] } })
        }
        break;
      case CrudType.EDIT:
        break;

    }
  }

  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SELLING_PRICE_EXPORT, this.crudService, this.rxjsService, 'Dealer Stock Table Selling Price');
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }

}
