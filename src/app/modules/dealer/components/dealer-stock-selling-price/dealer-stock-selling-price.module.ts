import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerStockSellingPriceComponent } from '@modules/dealer';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
    declarations: [DealerStockSellingPriceComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: DealerStockSellingPriceComponent, data: { title: 'Dealer Stock Selling Price' },canActivate:[AuthGuard] },
            { path: 'view', loadChildren: () => import('./dealer-stock-selling-price-view/dealer-stock-selling-price-view.module').then(m => m.DealerStockSellingPriceViewModule),canActivate:[AuthGuard] },
            { path: 'dealer-stock-scheduling', loadChildren: () => import('../dealer-stock-scheduling/dealer-stock-scheduling.module').then(m => m.DealerStockSchedulingModule),canActivate:[AuthGuard] },
        ])
    ],
    entryComponents: [],
    providers: []
})
export class DealerStockSellingPriceModule { }