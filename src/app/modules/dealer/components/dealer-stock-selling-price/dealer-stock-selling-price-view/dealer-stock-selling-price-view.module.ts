import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerStockSellingPriceViewComponent } from '@modules/dealer';

@NgModule({
    declarations: [DealerStockSellingPriceViewComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: DealerStockSellingPriceViewComponent, data: { title: 'Dealer Stock Selling Price View' } },
        ])
    ],
    entryComponents: [],
    providers: []
})
export class DealerStockSellingPriceViewModule { }