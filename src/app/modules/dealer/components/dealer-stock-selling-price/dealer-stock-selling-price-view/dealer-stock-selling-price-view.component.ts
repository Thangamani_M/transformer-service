import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-dealer-stock-selling-price-view',
    templateUrl: './dealer-stock-selling-price-view.component.html',
    styleUrls: ['./dealer-stock-selling-price-view.component.scss']
})
export class DealerStockSellingPriceViewComponent extends PrimeNgTableVariablesModel implements OnInit {
    dealerItemSellingPriceId: any;
    dealerStockSellingPriceDetail: any;
    primengTableConfigProperties: any;
    observableResponse: any;
    row: any = {};
    listSubscription: any;
    first: any = 0;
    filterData: any;
    constructor(private activatedRoute: ActivatedRoute, private router: Router,
        private store: Store<AppState>, private datePipe: DatePipe,
        private rxjsService: RxjsService, private crudService: CrudService) {
            super();
        this.dealerItemSellingPriceId = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
            tableCaption: "Dealer Stock Table Selling Price View",
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Dealer Stock Table Selling Price', relativeRouterUrl: '/dealer/dealer-stock-selling-price' },
            { displayName: 'Dealer Stock Table Selling Price View', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Dealer Stock Table Selling Price View',
                        dataKey: 'dealerItemSellingPriceId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableHyperLink: false,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'updatedDate', header: 'Updated Date', width: '120px' },
                            { field: 'sellingPrice', header: 'Selling Price', width: '150px', isDecimalFormat: true },
                            { field: 'costPrice', header: 'Cost Price', width: '150px', isDecimalFormat: true },
                            { field: 'labourRate', header: 'Labour Rate', width: '150px', isDecimalFormat: true },
                            { field: 'materialMarkup', header: 'Material Markup', width: '150px' },
                            { field: 'consumableMarkup', header: 'Consumable Markup', width: '90px' },
                            { field: 'updatedBy', header: 'Updated By', width: '130px' },
                            { field: 'status', header: 'Status', width: '100px' },
                        ],
                        enableEditActionBtn: false,
                        enableClearfix: true,
                        apiSuffixModel: DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE_HISTORY,
                        moduleName: ModulesBasedApiSuffix.INVENTORY,
                    }]
            }
        }
        this.viewValue();
    }

    ngOnInit() {
        if (this.dealerItemSellingPriceId) {
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE_DETAILS, null, false, prepareRequiredHttpParams({ DealerItemSellingPriceId: this.dealerItemSellingPriceId }))
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.viewValue(response);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
            this.onCRUDRequested('get');
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest(
            this.store.select(loggedInUserData)
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e.col) {
            this.onCRUDRequested(e.type, e.data)
        } else if (e.data && e.search) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data) {
            this.onCRUDRequested(e.type, {})
        } else if (e.type && e.col) {
            this.onCRUDRequested(e.type, e.data, e.col)
        }
    }

    getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
        dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
        if (this.listSubscription && !this.listSubscription.closed) {
            this.listSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.listSubscription = this.crudService.get(
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
            dealerModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).pipe(map((res: IApplicationResponse) => {
            if (res?.resources) {
                res?.resources?.forEach(val => {
                    val.updatedDate = this.datePipe.transform(val.updatedDate, 'dd-MM-yyyy, h:mm:ss a');
                    val.sellingPrice = 'R' + val.sellingPrice;
                    val.costPrice = 'R' + val.costPrice;
                    val.labourRate = 'R' + val.labourRate;
                    return val;
                })
            }
            return res;
        })).subscribe((data: IApplicationResponse) => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.observableResponse = data.resources;
                this.dataList = this.observableResponse;
                this.totalRecords = data.totalCount;
            } else {
                this.observableResponse = null;
                this.dataList = this.observableResponse;
                this.totalRecords = 0;
            }
        }, error => {
            this.observableResponse = null;
            this.dataList = this.observableResponse;
            this.totalRecords = 0;
        })
    }

    onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
        switch (type) {
            case CrudType.CREATE:
                this.openAddEditPage(CrudType.CREATE, row);
                break;
            case CrudType.GET:
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                unknownVar = { ...this.filterData, ...unknownVar, dealerItemSellingPriceId: this.dealerItemSellingPriceId };
                this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
                break;
            case CrudType.EDIT:
                this.openAddEditPage(CrudType.EDIT, row);
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            default:
        }
    }

    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
        switch (type) {
            case CrudType.VIEW:
                if (this.selectedTabIndex == 0) {
                    this.router.navigate(['/dealer/dealer-stock-selling-price/view'], { queryParams: { id: editableObject[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey] } })
                }
                break;
            case CrudType.EDIT:
                break;
        }
    }

    viewValue(response?: any) {
        this.dealerStockSellingPriceDetail = [
            { name: 'Selling Price ID', value: response?.resources ? response?.resources?.sellingPriceCode : '' },
            { name: 'Stock Code', value: response?.resources ? response?.resources?.stockCode : '' },
            { name: 'Stock Description', value: response?.resources ? response?.resources?.stockDescription : '' },
            { name: 'Selling Price', value: response?.resources ? response?.resources?.sellingPrice : '' },
            { name: 'Ownership', value: response?.resources ? response?.resources?.ownership : '' },
            { name: 'System Type', value: response?.resources ? response?.resources?.systemType : '' },
            { name: 'Component Group', value: response?.resources ? response?.resources?.componentGroup : '' },
            { name: 'SP Last Updated', value: response?.resources ? response?.resources?.spLastUpdated : '', isDateTime: true },
        ]
    }

    onChangeSelecedRows(e) {
        this.selectedRows = e;
    }

    ngOnDestroy() {
        if (this.listSubscription) {
            this.listSubscription.unsubscribe();
        }
    }

}
