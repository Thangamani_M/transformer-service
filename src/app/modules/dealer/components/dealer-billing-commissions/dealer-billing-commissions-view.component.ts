import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
@Component({
  selector: 'app-dealer-billing-commissions-view',
  templateUrl: './dealer-billing-commissions-view.component.html'
})
export class DealerBillingCommissionsViewComponent implements OnInit {

  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  dataList: any;
  dataList1: any
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  totalRecords: any;
  observableResponse: any;
  loggedUser: any;
  status: any = [];
  showFilterForm: any = false;
  proFormInvoiceFilterForm: FormGroup;
  startTodayDate = new Date();
  filteredData: any = {};
  first: any = 0;
  dateFormat = 'MMM dd, yyyy';
  chargeListId: any;
  chargeListDetails: any;
  viewData=[]

  constructor(private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private rxjsService: RxjsService, private snackbarService: SnackbarService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Billing",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '/dealer' }, { displayName: 'Dealer Billing', relativeRouterUrl: '/dealer/dealer-billing-commission' }, { displayName: 'View Dealer Billing', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Monthly Test Run',
            dataKey: 'chargeListId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'customerRefNo', header: 'Customer Code', width: '200px' },
              { field: 'dealerIIPCode', header: 'Dealer Code', width: '200px' },
              { field: 'customerName', header: 'Customer Name', width: '200px' },
              { field: 'contractAmount', header: 'Contract Amount', width: '200px' },
              { field: 'dealerMultiple', header: 'Dealer Multiple', width: '200px' },
              { field: 'bonusMultipleAmount', header: 'Bonus Multiple (0)', width: '200px' },
              { field: 'grossAmount', header: 'Cross Amount', width: '200px' },
              { field: 'vatAmount', header: 'VAT(15%)', width: '200px' },
              { field: 'additionalDSFAmount', header: 'Additional DSF', width: '200px' },
              { field: 'dsfAmount', header: 'DSF Amount', width: '200px' },
              { field: 'totalAmount', header: 'Total Amount', width: '200px' },
              { field: 'createdDate', header: 'Created Date', width: '200px' },
            ],
            enableAddActionBtn: false,
            enablePrintBtn: false,
            printTitle: 'Pro Forma Invoice',
            printSection: 'print-section0',
            enableEmailBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableAction: false,
            enableExportBtn: false
          },

        ]
      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.chargeListId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';

    });
  }
  ngOnInit(): void {
    this.getRequiredDetailData();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.viewData = [
          { name: 'Dealer Code', value: "" },
          { name: 'Dealer Branch Code', value: "" },
          { name: 'Dealer Type', value: "" },
          { name: 'Dealer Category', value: "" },
          { name: 'Dealer Name', value: "" },
          { name: 'Invoice Number', value: "" },
          { name: 'Dealer Branch Name', value: "" },
    ]
  }
  // paramss: any;
  getRequiredDetailData() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEALER_BILLING_COMMISSIONS, this.chargeListId, false)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode = 200 && response.resources) {
          this.chargeListDetails = response.resources;

          this.viewData = [
            { name: 'Dealer Code', value: this.chargeListDetails.dealerIIPCode },
            { name: 'Dealer Branch Code', value: this.chargeListDetails.dealerBranchCode },
            { name: 'Dealer Type', value: this.chargeListDetails.dealerTypeName },
            { name: 'Dealer Category', value: this.chargeListDetails.dealerCategoryTypeName },
            { name: 'Dealer Name', value: this.chargeListDetails.dealerName },
            { name: 'Invoice Number', value: this.chargeListDetails.invoiceRefNo },
            { name: 'Dealer Branch Name', value: this.chargeListDetails.branchName },
      ]

          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[5].header = 'Bonus Multiple (' + this.chargeListDetails.bonusMultiple + ')'
          this.observableResponse = this.chargeListDetails.customerList;
          this.dataList1 = this.chargeListDetails.customerList;
          this.totalRecords = this.dataList1.length;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredDetailData()
        break;
      case CrudType.EXPORT:
        this.exportList();
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }


  exportList() {
    if (this.dataList.length != 0) {
      let fileName = 'Monthly Test Run ' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["Branch Name", "Group", "Debtor", "BDI", "Invoice No.", "Contract No.", "Payment Method", "Run Code", "Payment Fee", "Invoice Total", "Radio Amount Excl.", "Radio Amount Incl.", "DO Number", "DO Total", "DO Override", "DO Underride", "District", "CRM Status", "CRM Error", "Bank Status", "Bank Error"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList1.map(c => {
        csv += [c['branchName'], c['groupInvoice'], c['debtorRefNo'], c['bdiNumber'], c['invoiceRefNo'], c['contractRefNo'], c['paymentMethodName'], c['debitOrderRunCodeName'], c['invTotalExc'], c['invTotalIncl'], c['radioAmountExc'], c['radioAmountIncl'], c['doNumber'], c['doTotal'], c['doOverrideAmount'], c['doUnderrideAmount'], c['districtName'], c['crmStatus'], c['crmError'], c['bankStatus'], c['bankError']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    } else {
      this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
    }
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList1);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }


}
