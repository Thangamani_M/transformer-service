import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerBillingCommissionsListComponent } from './dealer-billing-commissions-list.component';
import { DealerBillingCommissionsRoutingModule } from './dealer-billing-commissions-routing.module';
import { DealerBillingCommissionsViewComponent } from './dealer-billing-commissions-view.component';
@NgModule({
  declarations: [DealerBillingCommissionsListComponent, DealerBillingCommissionsViewComponent],
  imports: [
    CommonModule,
    DealerBillingCommissionsRoutingModule,
    LayoutModule,SharedModule,MaterialModule,ReactiveFormsModule,FormsModule
  ]
})
export class DealerBillingCommissionsModule { }
