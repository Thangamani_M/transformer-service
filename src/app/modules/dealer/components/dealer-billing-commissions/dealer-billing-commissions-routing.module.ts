import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerBillingCommissionsListComponent } from './dealer-billing-commissions-list.component';
import { DealerBillingCommissionsViewComponent } from './dealer-billing-commissions-view.component';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: DealerBillingCommissionsListComponent,canActivate: [AuthGuard], data: { title: 'Dealer Billing Commission List' } },
  { path: 'view', component: DealerBillingCommissionsViewComponent,canActivate: [AuthGuard], data: { title: 'Dealer Billing Commission View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class DealerBillingCommissionsRoutingModule { }
