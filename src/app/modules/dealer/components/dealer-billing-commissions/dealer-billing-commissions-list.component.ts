import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { Store } from "@ngrx/store";
import { map } from "rxjs/operators";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { DEALER_COMPONENT } from "@modules/dealer/shared";
@Component({
  selector: 'app-dealer-billing-commissions-list',
  templateUrl: './dealer-billing-commissions-list.component.html'
})
export class DealerBillingCommissionsListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {}

  constructor(private router: Router, private rxjsService: RxjsService,private store: Store<AppState>,    private snackbarService: SnackbarService,

    private crudService: CrudService) {
      super()

    this.primengTableConfigProperties = {
      tableCaption: "Dealer Billing",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '/dealer' }, { displayName: 'Dealer Billing', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Billing',
            dataKey: 'invoiceId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'invoiceRefNo', header: 'Invoice Billing', width: '200px' }, { field: 'dealerIIPCode', header: 'Dealer Code', width: '200px' },
            { field: 'companyRegNumber', header: 'Company Registration Number', width: '250px' }, { field: 'vatNumber', header: 'Dealer VAT Number', width: '200px' },
            { field: 'debtorRefNo', header: 'Dealer Debtor Code', width: '200px' }, { field: 'dealerBranchCode', header: 'Dealer Branch Code', width: '200px' },
            { field: 'createdDate', header: 'Created Date', width: '200px' }, { field: 'invoiceTypeName', header: 'Invoice Type', width: '200px' },
            { field: 'totalAmount', header: 'Invoice Amount', width: '200px' },
            { field: 'dealerCategoryTypeName', header: 'Dealer Category', width: '200px' },
            { field: 'statusConfigName', header: 'Status', width: '200px' },
            { field: 'documentPath', header: 'Download', width: '200px', isDownloadLink: true, nofilter: true },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.DEALER_BILLING_COMMISSIONS,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_BILLING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let apiVersion = 1;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.DEALER_BILLING_COMMISSIONS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams),
      apiVersion
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          return val;
        })
      }
      return res;
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.EDIT:
        if (otherParams == "documentPath" && row['documentPath']) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canDownload) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
          window.open(row['documentPath'])
        }
        break;
      case CrudType.VIEW:
        this.router.navigate(['dealer/dealer-billing-commission/view'], { queryParams: { id: row["invoiceId"] } });
        break;
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
