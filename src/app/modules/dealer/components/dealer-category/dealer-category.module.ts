import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerCategoryAddEditComponent, DealerCategoryListComponent } from '@modules/dealer';
import { DealerCategoryRoutingModule } from './dealer-category-routing.module';
@NgModule({
    declarations: [DealerCategoryListComponent, DealerCategoryAddEditComponent],
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        DealerCategoryRoutingModule
    ],
    entryComponents: [DealerCategoryAddEditComponent],
    providers: [
        DatePipe
    ]
})
export class DealerCategoryModule { }
