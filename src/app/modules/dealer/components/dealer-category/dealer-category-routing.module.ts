import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerCategoryListComponent } from './dealer-category-list.component';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: DealerCategoryListComponent,canActivate:[AuthGuard], data: { title: 'Dealer Type' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})
export class DealerCategoryRoutingModule { }
