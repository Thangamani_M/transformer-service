import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerCategoryModel } from '@modules/dealer/models/dealer-category.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-dealer-category-add-edit',
  templateUrl: './dealer-category-add-edit.component.html'
})
export class DealerCategoryAddEditComponent implements OnInit {

  dealerCategoryDialogForm: FormGroup;
  isSubmitted: boolean;
  loggedUser: any;
  isSubmit: boolean = true;
  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, private store: Store<AppState>,
    private crudService: CrudService, private formBuilder: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }
  ngOnInit(): void {
    this.initForm();
  }
  initForm(dealerCategoryFormModel?: DealerCategoryModel) {
    let dealerCategoryeModel = new DealerCategoryModel(dealerCategoryFormModel);
    this.dealerCategoryDialogForm = this.formBuilder.group({});
    Object.keys(dealerCategoryeModel).forEach((key) => {
      if (dealerCategoryeModel[key] === 'dealerCategoryCodeList') {
        this.dealerCategoryDialogForm.addControl(key, new FormArray(dealerCategoryeModel[key]));
      } else {
        this.dealerCategoryDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : dealerCategoryeModel[key]));
      }
    });
    this.dealerCategoryDialogForm = setRequiredValidator(this.dealerCategoryDialogForm, ["dealerCategoryTypeName", "description"]);
  }
  btnCloseClick() {
    this.ref.close(false);
  }
  onSubmitDelaerTypeDialog() {
    if (this.isSubmitted || !this.dealerCategoryDialogForm?.valid) {
      return;
    }
    let dealerCategoryObject = {
      dealerCategoryTypeName: this.dealerCategoryDialogForm.value.dealerCategoryTypeName ? this.dealerCategoryDialogForm.value.dealerCategoryTypeName : null,
      description: this.dealerCategoryDialogForm.value.description ? this.dealerCategoryDialogForm.value.description : null,
      isActive: true,
      createdUserId: this.loggedUser.userId,
      dealerCategoryTypeId: this.config?.data?.row?.dealerCategoryTypeId ? this.config?.data?.row?.dealerCategoryTypeId : null,
    };
    this.isSubmitted = true;
    let api = dealerCategoryObject.dealerCategoryTypeId == null ? this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_CATEGORY, dealerCategoryObject) : this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_CATEGORY, dealerCategoryObject);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
      }
      this.ref.close(res);
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }

}