import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { MarketTypeConfigDetailComponent } from '@modules/dealer';
import { InputSwitchModule } from 'primeng/inputswitch';
import { MarketTypeConfigRoutingModule } from './market-type-config-routing.module';

@NgModule({
    declarations: [MarketTypeConfigDetailComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        InputSwitchModule,
        ReactiveFormsModule,
        FormsModule,
        MarketTypeConfigRoutingModule,
    ],
    providers: [
        DatePipe
    ]
})
export class MarketTypeConfigModule { }