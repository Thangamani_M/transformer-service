import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MarketTypeConfigArrayDetailsModel, MarketTypeConfigDetailModel } from '@modules/dealer/models/market-type-config.model';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-market-type-config-detail',
  templateUrl: './market-type-config-detail.component.html',
  styleUrls: ['./market-type-config-detail.component.scss']
})
export class MarketTypeConfigDetailComponent implements OnInit {

  marketTypeConfigDetailForm: FormGroup;
  selectedIndex = 0;
  primengTableConfigProperties: any;
  isLoading: boolean;
  isSubmitted: boolean;
  userData: any;
  listSubscription: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: 'Marketing Type Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Marketing', relativeRouterUrl: '' }, { displayName: 'Marketing Type Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Marketing Type Config',
            dataKey: 'marketingTypeId',
            formArrayName: 'marketTypeConfigDetailsArray',
            columns: [
              { field: 'marketingTypeName', displayName: 'Market Type Name', type: 'input_text', className: 'col-6' },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1 mt-2', required: true, isChangeEvent: true },
            ],
            enableBreadCrumb: true,
            detailsAPI: DealerModuleApiSuffixModels.MARKETING_TYPE,
            postAPI: DealerModuleApiSuffixModels.MARKETING_TYPE,
            deleteAPI: DealerModuleApiSuffixModels.MARKETING_TYPE,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.initForm();
    this.onLoadValue();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]['Marketing Type Config']
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm(marketConfigDetailModel?: MarketTypeConfigDetailModel) {
    let marketTypeConfigModel = new MarketTypeConfigDetailModel(marketConfigDetailModel);
    this.marketTypeConfigDetailForm = this.formBuilder.group({});
    Object.keys(marketTypeConfigModel).forEach((key) => {
      if (typeof marketTypeConfigModel[key] === 'object') {
        this.marketTypeConfigDetailForm.addControl(key, new FormArray(marketTypeConfigModel[key]));
      } else {
        this.marketTypeConfigDetailForm.addControl(key, new FormControl(marketTypeConfigModel[key]));
      }
    });
    this.marketTypeConfigDetailForm = setRequiredValidator(this.marketTypeConfigDetailForm, ["marketingTypeName"]);
  }

  onLoadValue() {
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.listSubscription = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.deleteAPI)
      .subscribe((resp: IApplicationResponse) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          resp?.resources.forEach(el => {
            this.initFormArray({
              marketingTypeId: el?.marketingTypeId,
              marketingTypeName: el?.marketingTypeName,
              isActive: el?.isActive,
            });
            return el;
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  addConfigItem() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
    if (this.marketTypeConfigDetailForm.invalid) {
      this.marketTypeConfigDetailForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("market Config item already exists", ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  validateExist(e?: any) {
    const findItem = this.getmarketTypeConfigFormArray.value.find(el => el.marketingTypeName == this.marketTypeConfigDetailForm?.value?.marketingTypeName);
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    this.isSubmitted = true;
    if (this.marketTypeConfigDetailForm.invalid) {
      this.marketTypeConfigDetailForm.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else {
      const addObj = {
        marketingTypeId: 0,
        marketingTypeName: this.marketTypeConfigDetailForm.value.marketingTypeName,
        isActive: this.marketTypeConfigDetailForm.value.isActive,
        createdUserId: this.userData?.userId,
      }
      this.onAfterSubmit(addObj);
    }

  }

  initFormArray(marketTypeConfigArrayDetailsModel?: MarketTypeConfigArrayDetailsModel, isAdd = false) {
    let marketTypeConfigDetailsModel = new MarketTypeConfigArrayDetailsModel(marketTypeConfigArrayDetailsModel);
    let marketTypeConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(marketTypeConfigDetailsModel).forEach((key) => {
      marketTypeConfigDetailsFormArray.addControl(key, new FormControl({ value: marketTypeConfigDetailsModel[key], disabled: true }));
    });
    marketTypeConfigDetailsFormArray?.get('isActive')?.enable();
    this.getmarketTypeConfigFormArray.insert(0, marketTypeConfigDetailsFormArray);
    if (isAdd) {
      this.marketTypeConfigDetailForm.patchValue({
        marketingTypeName: '',
        isActive: true,
      })
    }
  }

  changeStatus(i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
    const reqObj = {
      marketingTypeId: this.getmarketTypeConfigFormArray?.controls[i]?.get('marketingTypeId')?.value,
      isActive: this.getmarketTypeConfigFormArray?.controls[i]?.get('isActive')?.value,
      modifiedUserId: this.userData?.userId,
    };
    this.onAfterSubmit(reqObj);
  }

  get getmarketTypeConfigFormArray(): FormArray {
    if (!this.marketTypeConfigDetailForm) return;
    return this.marketTypeConfigDetailForm.get("marketTypeConfigDetailsArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmit() {
    this.isSubmitted = true;
    if (this.getmarketTypeConfigFormArray.invalid) {
      this.marketTypeConfigDetailForm.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else {
      const reqObj = {
        marketingTypeName: this.marketTypeConfigDetailForm?.get('marketingTypeName')?.value,
        isActive: this.marketTypeConfigDetailForm?.get('isActive')?.value,
        createdUserId: this.userData?.userId,
      };
      this.onAfterSubmit(reqObj);
    }
  }

  onAfterSubmit(reqObj) {
    let api = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].postAPI, reqObj);
    if (reqObj?.marketingTypeId) {
      api = this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].postAPI, reqObj);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.marketTypeConfigDetailForm.reset();
        this.clearFormArray(this.getmarketTypeConfigFormArray);
        this.onLoadValue();
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }
}
