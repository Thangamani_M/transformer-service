import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarketTypeConfigDetailComponent } from '@modules/dealer';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: MarketTypeConfigDetailComponent, canActivate:[AuthGuard],data: { title: 'Marketing Type Config' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class MarketTypeConfigRoutingModule { }
