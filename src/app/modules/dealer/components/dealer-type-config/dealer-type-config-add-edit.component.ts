import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer';
import { DealerTypeConfigAddEditModel } from '@modules/dealer/models/dealer-type-config.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-dealer-type-config-add-edit',
  templateUrl: './dealer-type-config-add-edit.component.html',
  styleUrls: ['./dealer-type-config.component.scss']
})
export class DealerTypeConfigAddEditComponent implements OnInit {

  primengTableConfigProperties: any;
  selectedTabIndex: any = 0; //selected tab
  DealerTypeConfigAddEditForm: FormGroup; // form group
  userData: any;
  isSubmitted: boolean;
  dealerMonthDropdown: any;
  dealerTypeDropdown: any;
  dealerCategoryDropdown: any;
  isVolumeBonusDropdown = [{ id: 'Y', display: 'Yes' }, { id: 'N', display: 'No' }];

  constructor(private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private crudService: CrudService, private dialog: MatDialog,private snackbarService: SnackbarService) {
    this.primengTableConfigProperties = {
      tableCaption: 'Add  Dealer Type Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '/dealer/dashboard' }, { displayName: 'Dealer Type Configuration', relativeRouterUrl: '/dealer/dealer-type-config', }, { displayName: 'Add Dealer Type Config', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
            dataKey: 'kitConfigItemId',
          }
        ]
      }
    }

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createDealerTypeConfigAddEdit();
    this.getDealerMonthDropDown();
    this.getDealerTypeDropDown();
    this.getDealerCategoryDropDown();
    this.dealerTypeConfigDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_TYPE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }



  createDealerTypeConfigAddEdit(): void {
    let CreateDealerTypeConfigAddEditModel = new DealerTypeConfigAddEditModel();
    // create form controls dynamically from model class
    this.DealerTypeConfigAddEditForm = this.formBuilder.group({});
    Object.keys(CreateDealerTypeConfigAddEditModel).forEach((key) => {
      if (key == 'dealerTypeConfigAddEditCodeList') {
        let CreateDealerTypeConfigAddEditFormArray = this.formBuilder.array([]);
        this.DealerTypeConfigAddEditForm.addControl(key, CreateDealerTypeConfigAddEditFormArray);
      }
      else {
        this.DealerTypeConfigAddEditForm.addControl(key, new FormControl(CreateDealerTypeConfigAddEditModel[key]));
      }
    });
    this.DealerTypeConfigAddEditForm = setRequiredValidator(this.DealerTypeConfigAddEditForm, ["dealerTypeName", "dealerCategory", "description", "comStartMonths", "bonusMultiple"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getDealerTypeDropDown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_TYPE_DROPDOWN, prepareGetRequestHttpParams(null, null,
        { IsAll: false })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            let dealerTypeDropdownFilterArray = [];
            for (var i = 0; i < response.resources.length; i++) {
              let tmp = {};
              tmp['id'] = response.resources[i].id;
              tmp['display'] = response.resources[i].displayName;
              dealerTypeDropdownFilterArray.push(tmp);
            }
            this.dealerTypeDropdown = dealerTypeDropdownFilterArray;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }
  getDealerCategoryDropDown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALET_CATEGORY_DROPDOWN, prepareGetRequestHttpParams(null, null, { IsAll: false })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          let dealerCategoryDropdownFilterArray = [];
          for (var i = 0; i < response.resources.length; i++) {
            let tmp = {};
            tmp['id'] = response.resources[i].id;
            tmp['display'] = response.resources[i].displayName;
            dealerCategoryDropdownFilterArray.push(tmp);
          }
          this.dealerCategoryDropdown = dealerCategoryDropdownFilterArray;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getDealerMonthDropDown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALET_MONTH_DROPDOWN, prepareGetRequestHttpParams(null, null,
        { IsAll: true })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            this.dealerMonthDropdown = response.resources;
            let dealerMonthDropdownFilterArray = [];
            for (var i = 0; i < response.resources.length; i++) {
              let tmp = {};
              tmp['value'] = response.resources[i].id;
              tmp['display'] = response.resources[i].displayName;
              dealerMonthDropdownFilterArray.push(tmp);
            }
            this.dealerMonthDropdown = dealerMonthDropdownFilterArray;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  dealerTypeConfigDetails() {
    this.crudService.get(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_TYPE).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          for (let i = 0; i < response.resources.length; i++) {
            let dealerTypeItem = this.formBuilder.group({
              dealerTypeName: new FormControl({ value: response.resources[i]?.dealerTypeId, disabled: true }),
              dealerCategory: new FormControl({ value: response.resources[i]?.dealerCategoryTypeId, disabled: true }),
              description: new FormControl({ value: response?.resources[i]?.description ? response?.resources[i]?.description : '', disabled: true }),
              comStartMonths: new FormControl({ value: response?.resources[i]?.comStartMonths ? response?.resources[i]?.comStartMonths : '', disabled: true }),
              bonusMultiple: new FormControl({ value: response?.resources[i]?.bonusMultiple ? response?.resources[i]?.bonusMultiple : '', disabled: true }),
              isVolumeBonus: new FormControl({ value: response?.resources[i]?.isVolumeBonus ? 'Y' : 'N', disabled: true }),
              isActive: new FormControl({ value: response?.resources[i]?.isActive ? response?.resources[i]?.isActive : false, disabled: true }),
              createdDate: response.resources[i].createdDate,
              createdUserId: response.resources[i].createdUserId,
              dealerTypeConfigId: response.resources[i].dealerTypeConfigId ? response.resources[i].dealerTypeConfigId : null,
              // dealerCategoryTypeId: response.resources[i].dealerCategoryTypeId,
              // dealerTypeId: response.resources[i].dealerTypeId,
            })
            dealerTypeItem = setRequiredValidator(dealerTypeItem, ["dealerTypeName", "dealerCategory", "description", "comStartMonths", "bonusMultiple"]);
            this.dealerTypeConfigAddEditCodesFormArray.push(dealerTypeItem);
          }

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  get dealerTypeConfigAddEditCodesFormArray(): FormArray {
    if (this.DealerTypeConfigAddEditForm !== undefined) {
      return (<FormArray>this.DealerTypeConfigAddEditForm.get('dealerTypeConfigAddEditCodeList'));
    }
  }
  getType(val) {
    if (val && this.dealerTypeDropdown && this.dealerTypeDropdown.length > 0) {
      return this.dealerTypeDropdown.find(el => el?.id == val)?.display;
    }
  }
  getCategory(val) {
    if (val && this.dealerCategoryDropdown && this.dealerCategoryDropdown.length > 0) {
      return this.dealerCategoryDropdown.find(el => el?.id == val)?.display;
    }
  }
  addDealerTypeCode() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.DealerTypeConfigAddEditForm.invalid) {
      this.DealerTypeConfigAddEditForm.markAllAsTouched();
      return;
    }
    let dealerTypeItem = this.formBuilder.group({
      dealerTypeName: new FormControl({ value: this.DealerTypeConfigAddEditForm.value.dealerTypeName, disabled: true }),
      dealerCategory: new FormControl({ value: this.DealerTypeConfigAddEditForm.value.dealerCategory, disabled: true }),
      description: new FormControl({ value: this.DealerTypeConfigAddEditForm.value.description, disabled: true }),
      comStartMonths: new FormControl({ value: this.DealerTypeConfigAddEditForm.value.comStartMonths, disabled: true }),
      bonusMultiple: new FormControl({ value: this.DealerTypeConfigAddEditForm.value.bonusMultiple, disabled: true }),
      isVolumeBonus: new FormControl({ value: this.DealerTypeConfigAddEditForm.value.isVolumeBonus, disabled: true }),
      isActive: new FormControl({ value: this.DealerTypeConfigAddEditForm.value.isActive, disabled: true }),
      createdUserId: this.userData.userOId,
      dealerTypeConfigId: null

    })
    dealerTypeItem = setRequiredValidator(dealerTypeItem, ["dealerTypeName", "dealerCategory", "description", "comStartMonths", "bonusMultiple", "isVolumeBonus", "isActive"]);

    this.dealerTypeConfigAddEditCodesFormArray.push(dealerTypeItem);
    this.DealerTypeConfigAddEditForm.controls['dealerTypeName'].reset();
    this.DealerTypeConfigAddEditForm.controls['dealerCategory'].reset();
    this.DealerTypeConfigAddEditForm.controls['description'].reset();
    this.DealerTypeConfigAddEditForm.controls['comStartMonths'].reset();
    this.DealerTypeConfigAddEditForm.controls['bonusMultiple'].reset();
    this.DealerTypeConfigAddEditForm.controls['isVolumeBonus'].reset();
    this.DealerTypeConfigAddEditForm.controls['isActive'].reset();

  }
  deleteDealerType(index) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    if (this.dealerTypeConfigAddEditCodesFormArray.controls[index].get('dealerTypeConfigId').value != null) {
      let saveBody = {
        body: {
          "ids": this.dealerTypeConfigAddEditCodesFormArray.controls[index].get('dealerTypeConfigId').value.toString(),
          "isDeleted": true
        }
      };
      this.crudService
        .deleteByParams(ModulesBasedApiSuffix.DEALER,
          DealerModuleApiSuffixModels.DEALER_TYPE_DELETE,
          saveBody).subscribe({
            next: response => {
              if (response.isSuccess == true && response.statusCode == 200) {
                this.rxjsService.setGlobalLoaderProperty(false);
                this.dealerTypeConfigAddEditCodesFormArray.removeAt(index);
              } else {
                this.rxjsService.setGlobalLoaderProperty(false);
                this.dealerTypeConfigAddEditCodesFormArray.removeAt(index);
              }
            },

          });
    }
    else {
      this.dealerTypeConfigAddEditCodesFormArray.removeAt(index);
    }
  }

  editDealerTypeConfig(index) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    let dealerTypeConfigFormArray = this.dealerTypeConfigAddEditCodesFormArray;
    dealerTypeConfigFormArray.controls[index].get('dealerTypeName').enable();
    dealerTypeConfigFormArray.controls[index].get('dealerCategory').enable();
    dealerTypeConfigFormArray.controls[index].get('description').enable();
    dealerTypeConfigFormArray.controls[index].get('comStartMonths').enable();
    dealerTypeConfigFormArray.controls[index].get('bonusMultiple').enable();
    dealerTypeConfigFormArray.controls[index].get('isVolumeBonus').enable();
    dealerTypeConfigFormArray.controls[index].get('isActive').enable();
    dealerTypeConfigFormArray.controls[index].get('comStartMonths').enable();
  }
  saveTechDescCodes() {
    let saveBody = [];

    for (let i = 0; i < this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList.length; i++) {
      saveBody.push({
        Description: this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].description,
        ComStartMonths: parseInt(this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].comStartMonths),
        BonusMultiple: parseInt(this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].bonusMultiple),
        IsVolumeBonus: this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].isVolumeBonus == "Y" ? true : false,
        IsActive: this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].isActive,
        CreatedUserId: this.userData.userId,
        dealerTypeConfigId: this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].dealerTypeConfigId ? this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].dealerTypeConfigId : null,
        dealerCategoryTypeId: parseInt(this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].dealerCategory),
        dealerTypeId: parseInt(this.DealerTypeConfigAddEditForm.getRawValue().dealerTypeConfigAddEditCodeList[i].dealerTypeName),
      })
    }
    this.isSubmitted = true;
    this.crudService
      .create(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_TYPE,
        saveBody).subscribe({
          next: response => {
            if (response.isSuccess == true && response.statusCode == 200) {
              this.rxjsService.setGlobalLoaderProperty(false);
              this.DealerTypeConfigAddEditForm.reset();
              this.createDealerTypeConfigAddEdit();
              //  this.dealerTypeConfigAddEditCodesFormArray.clear();
              this.isSubmitted = false;
              //  this.dealerTypeConfigDetails();
              this.router.navigateByUrl(`/dealer/dealer-type-config`);
            } else {
              this.rxjsService.setGlobalLoaderProperty(false);
              this.DealerTypeConfigAddEditForm.reset();
              this.createDealerTypeConfigAddEdit();
              this.router.navigateByUrl(`/dealer/dealer-type-config`);
              this.isSubmitted = false;
            }
          },

        });
  }
  cancel() {
    this.router.navigateByUrl(`/dealer/dealer-type-config`);
  }
  onCRUDRequested(e) {

  }

}

