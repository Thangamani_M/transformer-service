import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, PrimengStatusConfirmDialogComponent,  ResponseMessageTypes,  RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { DealerTypeConfigAddEditModel } from '@modules/dealer/models/dealer-type-config.model';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-dealer-type-config',
  templateUrl: './dealer-type-config-list.component.html',
  styleUrls: ['./dealer-type-config.component.scss']
})
export class DealerTypeConfigListComponent extends PrimeNgTableVariablesModel  implements OnInit {

  primengTableConfigProperties: any;
  userData: any;
  showFilterForm = false;
  dealerTypeFilterForm: FormGroup;
  listSubscribtion: any;
  row: any = {};
  first: any = 0;
  dealerTypeDropdown: any;
  dealerMonthDropdown: any;
  otherParams = {};
  dealerTypeName: any;
  comStartMonths: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private dialogService: DialogService, private snackbarService: SnackbarService,
    private router: Router) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: 'Dealer Type Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Type Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Type Config',
            dataKey: 'DealerTypeConfigId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealerTypeName', header: 'Dealer Type Name', width: '155px' },
              { field: 'dealerCategoryTypeName', header: 'Dealer Category', width: '150px' },
              { field: 'description', header: 'Description', width: '150px' },
              { field: 'comStartMonthsName', header: 'Com Start Months', width: '150px' },
              { field: 'bonusMultipleName', header: 'Bonus Multiple', width: '150px' },
              { field: 'isVolumeBonus', header: 'Volume Bonus', width: '170px' },
              { field: 'isActive', header: 'Status' },

            ],
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: true,
            enableStatusActiveAction: true,
            detailsAPI: DealerModuleApiSuffixModels.DEALER_TYPE,
            postAPI: DealerModuleApiSuffixModels.DEALER_TYPE,
            deleteAPI: DealerModuleApiSuffixModels.DEALER_TYPE,
            moduleName: ModulesBasedApiSuffix.DEALER,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getDealerTypeListData(null, null, { IsAll: true });
    this.getStatuses();
    this.getDealerTypeDropDown();
    this.getDealerMonthDropDown();
    this.createFilterForm();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_TYPE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {

    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        if (unknownVar?.isVolumeBonus) {
          unknownVar.isVolumeBonus = (unknownVar.isVolumeBonus == "Yes" || unknownVar.isVolumeBonus == "yes") ? true : false;
        }
        this.getDealerTypeListData(row['pageIndex'], row['pageSize'], unknownVar);
        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onChangeStatus(row, unknownVar);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      default:

    }
  }
  onChangeStatus(rowData, index) {
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    let module = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].detailsAPI;

    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 1000,
      width: '400px',
      data: {
        index: index,
        ids: rowData.dealerTypeConfigId,
        isActive: rowData.isActive,
        modifiedUserId: this.userData.userId,
        moduleName: module,
        apiSuffixModel: dealerModuleApiSuffixModels
      },
    });

    ref.onClose.subscribe((index) => {
      this.getDealerTypeListData(this.row['pageIndex'], this.row['pageSize'], { IsAll: true });
    });
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/dealer', 'dealer-type-config', 'add-edit']);
        break;
      case CrudType.VIEW:
        this.router.navigate(['/dealer', 'dealer-type-config', 'add-edit']);
        break;
    }
  }

  getDealerTypeListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    let module = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].detailsAPI;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      module,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      else {
        this.totalRecords = 0;
      }
    })
  }

  getDealerTypeDropDown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_TYPE_DROPDOWN, prepareGetRequestHttpParams(null, null,
        { IsAll: false })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            this.dealerTypeDropdown = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getDealerMonthDropDown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALET_MONTH_DROPDOWN, prepareGetRequestHttpParams(null, null,
        { IsAll: true })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            this.dealerMonthDropdown =  getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getStatuses() {
    this.status = [{ value: true, label: 'Active' }, { value: false, label: 'InActive' }];
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
  }
  createFilterForm(): void {

    let filterFormModel = new DealerTypeConfigAddEditModel();
    // create form controls dynamically from model class
    this.dealerTypeFilterForm = this.formBuilder.group({});
    Object.keys(filterFormModel).forEach((key) => {
      if (key == 'dealerTypeConfigAddEditCodeList') {
        let CreateDealerTypeConfigAddEditFormArray = this.formBuilder.array([]);
        this.dealerTypeFilterForm.addControl(key, CreateDealerTypeConfigAddEditFormArray);
      }
      else {
        this.dealerTypeFilterForm.addControl(key, new FormControl(filterFormModel[key]));
      }
    });
    this.dealerTypeFilterForm = setRequiredValidator(this.dealerTypeFilterForm, ["dealerTypeName", "description", "comStartMonths"]);
    this.rxjsService.setGlobalLoaderProperty(false);

  }
  submitFilter() {

    let filteredData = Object.assign({},
      { dealerTypeName: this.dealerTypeFilterForm.get('dealerTypeName').value ? this.dealerTypeFilterForm.get('dealerTypeName').value : '' },
      { dealerCategoryTypeName: this.dealerTypeFilterForm.get('dealerCategoryTypeName').value ? this.dealerTypeFilterForm.get('dealerCategoryTypeName').value : '' },
      { description: this.dealerTypeFilterForm.get('description').value ? this.dealerTypeFilterForm.get('description').value : '' },
      { comStartMonths: this.dealerTypeFilterForm.get('comStartMonths').value ? this.dealerTypeFilterForm.get('comStartMonths').value : '' },
      { bonusMultiple: this.dealerTypeFilterForm.get('bonusMultiple').value ? this.dealerTypeFilterForm.get('bonusMultiple').value : '' },
      { isVolumeBonus: this.dealerTypeFilterForm.get('isVolumeBonus').value ? this.dealerTypeFilterForm.get('isVolumeBonus').value : '' },
    );
    // if (this.dealerMonthDropdown && this.dealerMonthDropdown.length > 0) {
    //   let data = this.dealerMonthDropdown.filter(y => {
    //     return y.label == filteredData.comStartMonths;
    //   });
    //   // filteredData.comStartMonths = data && data.length > 0 ? data[0].value : "";
    // }

    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0;
    this.getDealerTypeListData(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }
  cancel() {
    this.showFilterForm = !this.showFilterForm;
  }
  resetForm() {
    this.dealerTypeFilterForm.reset();
    this.getDealerTypeListData(null, null, { IsAll: true });
    this.showFilterForm = !this.showFilterForm;
  }
}
