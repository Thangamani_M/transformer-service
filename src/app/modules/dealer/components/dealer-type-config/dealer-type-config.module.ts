import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerTypeConfigAddEditComponent, DealerTypeConfigListComponent } from '@modules/dealer';
import { InputSwitchModule } from 'primeng/inputswitch';
import { DealerTypeConfigRoutingModule } from './dealer-type-config-routing.module';
@NgModule({
    declarations: [DealerTypeConfigAddEditComponent,DealerTypeConfigListComponent,],
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        InputSwitchModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        DealerTypeConfigRoutingModule
    ],
    entryComponents:[],
    providers: [
        DatePipe
    ]
})
export class DealerTypeConfigModule { }
