import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerTypeConfigAddEditComponent, DealerTypeConfigListComponent } from '@modules/dealer';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: DealerTypeConfigListComponent, canActivate: [AuthGuard], data: { title: 'Dealer Type Config' } },
  { path: 'add-edit', component: DealerTypeConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Dealer Type Config Add/Edit' } },
  { path: 'add-view', component: DealerTypeConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Dealer Type Config Add/Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class DealerTypeConfigRoutingModule { }
