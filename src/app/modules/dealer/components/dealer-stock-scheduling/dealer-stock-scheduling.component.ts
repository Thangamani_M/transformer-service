import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { DealerExecuteDialogComponent } from '@modules/dealer';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-dealer-stock-scheduling',
  templateUrl: './dealer-stock-scheduling.component.html'
})
export class DealerStockSchedulingComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  listSubscription: any;
  first: any = 0;
  filterData: any;
  seletedItemsIds: any = [];

  constructor(
    private crudService: CrudService,
    private dialogService: DialogService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Stock Scheduling",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Dealer Stock Table Selling Price', relativeRouterUrl: '/dealer/dealer-stock-selling-price' }, { displayName: 'Dealer Stock Scheduling' },
      { displayName: 'Schedule Selling Price' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Schedule Selling Price',
            dataKey: 'dealerItemSellingPriceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealeritemSellingPriceCode', header: 'Selling Price ID', width: '130px' },
              { field: 'itemCode', header: 'Stock Code', width: '110px' },
              { field: 'sellingPrice', header: 'Selling Price', width: '120px', isDecimalFormat: true },
              { field: 'costPrice', header: 'Cost Price', width: '90px', isDecimalFormat: true },
              { field: 'labourRate', header: 'Labour Rate', width: '110px', isDecimalFormat: true },
              { field: 'materialMarkup', header: 'Material Markup', width: '130px' },
              { field: 'consumableMarkup', header: 'Consumable Markup', width: '160px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableReloadBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_STOCK_SCHEDULE_SELLING_PRICE,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
          {
            caption: 'Scheduled Jobs',
            dataKey: 'dealerSellingPriceJobId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealerSellingPriceJobCode', header: 'Job ID', width: '100px' },
              { field: 'executionDate', header: 'Execution Date', width: '70px' },
              { field: 'userName', header: 'Scheduled By', width: '100px' },
              { field: 'createdDate', header: 'Created Date', width: '100px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE_JOB,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
          {
            caption: 'Archive',
            dataKey: 'dealerSellingPriceJobId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealerSellingPriceJobCode', header: 'Job ID', width: '100px' },
              { field: 'executionDate', header: 'Execution Date', width: '70px' },
              { field: 'userName', header: 'Scheduled By', width: '100px' },
              { field: 'createdDate', header: 'Created Date', width: '100px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE_JOB,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col)
    }
  }
  tabClick(e) {
    this.selectedTabIndex = e?.index;
    this.dataList = [];
    this.onCRUDRequested('get');
    this.router.navigate([`../`], { relativeTo: this.activatedRoute, queryParams: { tab: this.selectedTabIndex } });
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.listSubscription = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if (this.selectedTabIndex != 0) {
            val.executionDate = this.datePipe.transform(val.executionDate, 'dd-MM-yyyy, h:mm:ss a');
            val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          } else {
            val.spLastUpdate = this.datePipe.transform(val.spLastUpdate, 'dd-MM-yyyy, h:mm:ss a');
          }
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        switch (this.selectedTabIndex) {
          case 0:
            unknownVar = { IsAll: true, ...unknownVar };
            break;
          case 1:
            unknownVar = { IsArchive: false, ...unknownVar };
            break;
          case 2:
            unknownVar = { IsArchive: true, ...unknownVar };
            break;
          default:
            break;
        }
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.RELOAD:
        this.onCRUDRequested('get');
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        if (this.selectedTabIndex == 1) {
          this.router.navigate(['./view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey], archive: false } })
        }
        switch (this.selectedTabIndex) {
          case 1:
            this.router.navigate(['./view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey], archive: false } })
            break;
          case 2:
            this.router.navigate(['./view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey], archive: true } })
            break;
          default:
            break;
        }
        break;
      case CrudType.EDIT:
        break;
    }
  }


  onChangeSelecedRows(e) {
    this.selectedRows = e;
    var seletedIds = [];
    this.selectedRows.forEach((element: any) => {
      seletedIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
    });
    this.seletedItemsIds = seletedIds;
  }

  proceed() {
    const data = {
      row: {
        dealerItemSellingPriceId: this.seletedItemsIds.toString(),
        executionDate: null,
        createdUserId: this.loggedInUserData.userId,
      },
      api: DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE_JOB,
      module: ModulesBasedApiSuffix.INVENTORY,
    };
    this.onOpenExecuteDialog('Execute', data);
  }

  proceedAll() {
    const data = {
      row: {
        dealerItemSellingPriceId: null,
        executionDate: null,
        createdUserId: this.loggedInUserData.userId,
      },
      api: DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE_JOB,
      module: ModulesBasedApiSuffix.INVENTORY,
    };
    this.onOpenExecuteDialog('Execute All', data);
  }

  onOpenExecuteDialog(header, data) {
    const ref = this.dialogService.open(DealerExecuteDialogComponent, {
      header: header,
      showHeader: false,
      baseZIndex: 1000,
      width: '400px',
      data: data
    });
    ref.onClose.subscribe((result: IApplicationResponse) => {
      if (result?.isSuccess && result?.statusCode == 200) {
        this.selectedRows = [];
        this.getTableListData();
      }
    });
  }

  cancel() {
    this.router.navigate(['/dealer/dealer-stock-selling-price']);
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }
}
