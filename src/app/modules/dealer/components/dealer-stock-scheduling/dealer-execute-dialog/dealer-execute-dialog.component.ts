import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, } from '@app/shared';
import { DealerExecuteDetailModel } from '@modules/dealer/models/dealer-stock-scheduling.model';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-dealer-execute-dialog',
  templateUrl: './dealer-execute-dialog.component.html'
})
export class DealerExecuteDialogComponent implements OnInit {

  dealerExecuteForm: FormGroup;
  isSubmitted: boolean;
  startTodayDate = new Date();
  constructor(public config: DynamicDialogConfig, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    public ref: DynamicDialogRef, private crudService: CrudService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(dealerExecuteFormModel?: DealerExecuteDetailModel) {
    let dealerExecuteModel = new DealerExecuteDetailModel(dealerExecuteFormModel);
    this.dealerExecuteForm = this.formBuilder.group({});
    Object.keys(dealerExecuteModel).forEach((key) => {
      if (typeof dealerExecuteModel[key] === 'object') {
        this.dealerExecuteForm.addControl(key, new FormArray(dealerExecuteModel[key]));
      } else {
        this.dealerExecuteForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : dealerExecuteModel[key]));
      }
    });
    this.dealerExecuteForm = setRequiredValidator(this.dealerExecuteForm, ["executionDate"]);
  }

  btnCloseClick() {
    this.dealerExecuteForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.dealerExecuteForm.invalid) {
      this.dealerExecuteForm.markAllAsTouched();
      return;
    }
    this.isSubmitted = true;
    this.dealerExecuteForm.value.executionDate = this.dealerExecuteForm.value.executionDate.toLocaleString();
    this.crudService.create(this.config?.data?.module, this.config?.data?.api,
      this.dealerExecuteForm.value).subscribe((response: IApplicationResponse) => {
        this.isSubmitted = false;
        if (response.isSuccess && response.statusCode == 200) {
          this.ref.close(response);
        }
      });
  }
}
