import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-dealer-stock-scheduling-view',
  templateUrl: './dealer-stock-scheduling-view.component.html'
})
export class DealerStockSchedulingViewComponent implements OnInit {

  dealerSellingPriceJobId: any;
  dealerStockSellingPriceDetail: any;
  primengTableConfigProperties: any;
  dataList: any;
  dateFormat = 'MMM dd, yyyy';
  observableResponse: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  selectedTabIndex: any = 0;
  row: any = {};
  loading: boolean;
  selectedRows: any = [];
  listSubscription: any;
  first: any = 0;
  filterData: any;
  isArchive: string;

  constructor(
      private activatedRoute: ActivatedRoute, private router: Router,
      private store: Store<AppState>, private datePipe: DatePipe,
      private rxjsService: RxjsService, private crudService: CrudService) {
      this.dealerSellingPriceJobId = this.activatedRoute.snapshot?.queryParams?.id;
      this.isArchive = this.activatedRoute.snapshot?.queryParams?.archive;
      this.primengTableConfigProperties = {
          tableCaption: "Dealer Stock Table Selling Price View",
          breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Dealer Stock Table Selling Price', relativeRouterUrl: '/dealer/dealer-stock-selling-price' },
          { displayName: 'Dealer Stock Scheduling', relativeRouterUrl: '/dealer/dealer-stock-selling-price/dealer-stock-scheduling', queryParams: {tab: 1} }, { displayName: 'Archive View', relativeRouterUrl: '' }],
          selectedTabIndex: 0,
          tableComponentConfigs: {
              tabsList: [
                  {
                      caption: 'Dealer Stock Table Selling Price View',
                      dataKey: 'dealerSellingPriceJobId',
                      enableBreadCrumb: true,
                      enableAction: true,
                      enableHyperLink: false,
                      cursorLinkIndex: 0,
                      columns: [
                          { field: 'itemSellingPriceCode', header: 'Selling Price ID', width: '100px' },
                          { field: 'itemCode', header: 'Stock Code', width: '80px' },
                          { field: 'description', header: 'Stock Description', width: '250px' },
                          { field: 'sellingPrice', header: 'Selling Price', width: '100px', isDecimalFormat: true },
                          { field: 'systemTypeName', header: 'System Type', width: '100px' },
                          { field: 'componentGroupName', header: 'Component Group', width: '130px' },
                      ],
                      enableEditActionBtn: false,
                      enableClearfix: true,
                      apiSuffixModel: DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE_JOB_DETAILS_LIST,
                      moduleName: ModulesBasedApiSuffix.INVENTORY,
                  }]
          }
      }
      this.viewValue();
  }

  ngOnInit() {
      if (this.dealerSellingPriceJobId) {
          this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          DealerModuleApiSuffixModels.DEALER_STOCK_SELLING_PRICE_JOB, this.dealerSellingPriceJobId, false, null)
          .subscribe((response: IApplicationResponse) => {
              if (response.resources && response.statusCode === 200) {
                  this.viewValue(response);
                  this.rxjsService.setGlobalLoaderProperty(false);
              }
          });
          this.onCRUDRequested('get');
      }
      if(this.isArchive == 'true') {
        this.primengTableConfigProperties.tableCaption = 'Archive View';
        this.primengTableConfigProperties.breadCrumbItems[2].queryParams = {tab: 2};
      } else {
        this.primengTableConfigProperties.tableCaption = 'Scheduled Jobs View';
        this.primengTableConfigProperties.breadCrumbItems[2].queryParams = {tab: 1};
      }
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableCaption;
  }

  combineLatestNgrxStoreData() {
      combineLatest(
          this.store.select(loggedInUserData)
      ).subscribe((response) => {
          this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  onActionSubmited(e: any) {
      if (e.data && !e.search && !e.col) {
          this.onCRUDRequested(e.type, e.data)
      } else if (e.data && e.search) {
          this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data) {
          this.onCRUDRequested(e.type, {})
      } else if (e.type && e.col) {
          this.onCRUDRequested(e.type, e.data, e.col)
      }
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
      this.loading = true;
      let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
      dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
      if (this.listSubscription && !this.listSubscription.closed) {
          this.listSubscription.unsubscribe();
          this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.listSubscription = this.crudService.get(
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
          dealerModuleApiSuffixModels,
          undefined,
          false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
          if (res?.resources) {
              res?.resources?.forEach(val => {
                  val.updatedDate = this.datePipe.transform(val.updatedDate, 'dd-MM-yyyy, h:mm:ss a');
                  return val;
              })
          }
          return res;
      })).subscribe((data: IApplicationResponse) => {
          this.loading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          if (data.isSuccess) {
              this.observableResponse = data.resources;
              this.dataList = this.observableResponse;
              this.totalRecords = data.totalCount;
          } else {
              this.observableResponse = null;
              this.dataList = this.observableResponse;
              this.totalRecords = 0;
          }
      }, error => {
          this.observableResponse = null;
          this.dataList = this.observableResponse;
          this.totalRecords = 0;
      })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
      switch (type) {
          case CrudType.CREATE:
              this.openAddEditPage(CrudType.CREATE, row);
              break;
          case CrudType.GET:
              this.row = row ? row : { pageIndex: 0, pageSize: 10 };
              this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
              unknownVar = { ...this.filterData, ...unknownVar, dealerSellingPriceJobId: this.dealerSellingPriceJobId };
              this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
              break;
          case CrudType.EDIT:
              this.openAddEditPage(CrudType.EDIT, row);
              break;
          case CrudType.VIEW:
              this.openAddEditPage(CrudType.VIEW, row);
              break;
          default:
      }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
      switch (type) {
          case CrudType.VIEW:
              if (this.selectedTabIndex == 0) {
                  this.router.navigate(['/dealer/dealer-stock-selling-price/view'], { queryParams: { id: editableObject[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey] } })
              }
              break;
          case CrudType.EDIT:
              break;
      }
  }

  viewValue(response?: any) {
      this.dealerStockSellingPriceDetail = [
          { name: 'Job ID', value: response?.resources ? response?.resources?.dealerSellingPriceJobCode : '' },
          { name: 'Execution Date', value: response?.resources ? response?.resources?.executionDate : '', isDateTime: true },
          { name: 'Scheduled By', value: response?.resources ? response?.resources?.createdDate : '', isDateTime: true },
          { name: 'Created By', value: response?.resources ? response?.resources?.userName : '' },
      ]
  }

  onChangeSelecedRows(e) {
      this.selectedRows = e;
  }

  ngOnDestroy() {
      if (this.listSubscription) {
          this.listSubscription.unsubscribe();
      }
  }
}
