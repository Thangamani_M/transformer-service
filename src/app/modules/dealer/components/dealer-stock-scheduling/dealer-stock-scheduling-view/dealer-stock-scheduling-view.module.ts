import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerStockSchedulingViewComponent } from '@modules/dealer';
@NgModule({
    declarations: [DealerStockSchedulingViewComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: DealerStockSchedulingViewComponent, data: { title: 'Dealer Stock Scheduling View' } },
        ])
    ],
    entryComponents: [],
    providers: []
})
export class DealerStockSchedulingViewModule { }