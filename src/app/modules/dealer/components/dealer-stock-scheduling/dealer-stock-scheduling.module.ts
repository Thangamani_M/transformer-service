import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerExecuteDialogComponent } from './dealer-execute-dialog/dealer-execute-dialog.component';
import { DealerStockSchedulingComponent } from './dealer-stock-scheduling.component';

@NgModule({
    declarations: [DealerStockSchedulingComponent, DealerExecuteDialogComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: DealerStockSchedulingComponent, data: { title: 'Dealer Stock Scheduling' } },
            { path: 'view', loadChildren: () => import('./dealer-stock-scheduling-view/dealer-stock-scheduling-view.module').then(m => m.DealerStockSchedulingViewModule) },
        ]),
    ],
    entryComponents: [DealerExecuteDialogComponent],
    providers: []
})
export class DealerStockSchedulingModule { }