import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { DfsConfigAddEditComponent } from '@modules/dealer';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-dfs-config-list',
  templateUrl: './dfs-config-list.component.html'
})
export class DfsConfigListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  dealerCategoryTypeList: any = [];
  dealerTypeList: any = [];
  listSubscribtion: any;
  row: any = {};
  first: any = 0;
  multipleSubscription: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private store: Store<AppState>, private dialogService: DialogService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: 'DSF Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'DSF Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Delayed Settlement Funds(DSF) Config',
            dataKey: 'dsfConfigId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dsfConfigId', header: 'DSF Config ID', width: '100px' },
              { field: 'dealerTypeName', header: 'Dealer Type', width: '100px' },
              { field: 'dealerCategoryTypeName', header: 'Category Type', width: '120px' },
              { field: 'description', header: 'Category Description', width: '300px' },
              { field: 'dsfValue', header: 'DSF Value', width: '100px' },
              { field: 'isActive', header: 'Status', width: '150px' }
            ],
            enableMultiDeleteActionBtn: true,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: DealerModuleApiSuffixModels.DFS_CONFIG_DELETE,
            listapiSuffixModel: DealerModuleApiSuffixModels.DFS_CONFIG,
            deleteAPISuffixModel: DealerModuleApiSuffixModels.DFS_CONFIG_DELETE,
            moduleName: ModulesBasedApiSuffix.DEALER,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getDSFConfigListData();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][DEALER_COMPONENT.DSF_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getDSFConfigListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[row].isActive = this.dataList[row].isActive ? false : true;
            }
          });
        break;

      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      default:
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let dealerCategoryTypeFilterList = resp.resources;
              let dealerCategoryTypeFilterArray = [];
              for (var i = 0; i < dealerCategoryTypeFilterList.length; i++) {
                let tmp = {};
                tmp['value'] = dealerCategoryTypeFilterList[i].id;
                tmp['display'] = dealerCategoryTypeFilterList[i].displayName;
                tmp['description'] = dealerCategoryTypeFilterList[i].description;
                dealerCategoryTypeFilterArray.push(tmp);
              }
              this.dealerCategoryTypeList = dealerCategoryTypeFilterArray;
              break;
            case 1:
              let dealerTypeFilterList = getPDropdownData(resp.resources);

              if (!filter) {
                this.dealerTypeList = dealerTypeFilterList;
                switch (type) {
                  case CrudType.CREATE:
                    this.openAddEditPopup('Create DSF Config', editableObject);
                    break;
                  case CrudType.VIEW:
                    switch (this.selectedTabIndex) {
                      case 0:
                        this.openAddEditPopup('View/Edit DSF Config', editableObject);
                        break;
                    }
                    break;
                }
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
      // this.setFilteredValue();
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes([
      this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALET_CATEGORY_DROPDOWN, prepareRequiredHttpParams({ isAll: true })),
      this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_TYPE_DROPDOWN, prepareRequiredHttpParams({ isAll: true }))
    ], false, editableObject, type);
  }

  openAddEditPopup(header, row) {
    let rowData;
    if (row) {
      rowData = { ...row };
      rowData['dealerTypeId'] = rowData['dealerTypeId']?.toString();
      rowData['dealerCategoryTypeId'] = rowData['dealerCategoryTypeId']?.toString();
    }
    let data = {
      createdUserId: this.loggedInUserData?.userId,
      row: rowData,
    };
    data['dealerCategoryTypeList'] = this.dealerCategoryTypeList;
    data['dealerTypeList'] = this.dealerTypeList;
    const ref = this.dialogService.open(DfsConfigAddEditComponent, {
      header: header,
      baseZIndex: 500,
      width: '700px',
      closable: false,
      showHeader: false,
      data: data,
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getDSFConfigListData();
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].deleteAPISuffixModel,
        method: 'delete',
        dataObject: {
          ids: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
          isDeleted: true,
          modifiedUserId: this.loggedInUserData?.userId,
        }
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.getDSFConfigListData();
      }
    });
  }

  getDSFConfigListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].listapiSuffixModel;
    const moduleName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName;
    this.listSubscribtion = this.crudService.get(
      moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
