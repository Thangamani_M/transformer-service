import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DfsConfigAddEditComponent, DfsConfigListComponent } from '@modules/dealer';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';
@NgModule({
    declarations: [DfsConfigListComponent, DfsConfigAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        InputSwitchModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: DfsConfigListComponent, canActivate:[AuthGuard], data: { title: 'DSF Config' } }
        ])
    ],
    entryComponents: [DfsConfigAddEditComponent],
    providers: []
})
export class DFSConfigTypeModule { }
