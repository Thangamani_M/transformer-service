import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setPercentageValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { DFSConfigModel } from '@modules/dealer';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-dfs-config-add-edit',
  templateUrl: './dfs-config-add-edit.component.html',
  styles:[`
  ::ng-deep label.ui-dropdown-label.ui-inputtext.ui-corner-all.ui-placeholder {
    height: unset !important;
  }`]
})
export class DfsConfigAddEditComponent implements OnInit {


  dfsConfigDialogForm: FormGroup;
  isSubmitted: boolean;
  categorylistSubscription: any;
  dealerCategoryTypeList = [];
  _dealerCategoryTypeList = [];
  dealerTypeList = [];
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 4 } });

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
    this.dealerCategoryTypeList = getPDropdownData(this.config?.data?.dealerCategoryTypeList,"display",'value');
    this._dealerCategoryTypeList = this.config?.data?.dealerCategoryTypeList;
    this.dealerTypeList = this.config?.data?.dealerTypeList;
  }

  ngAfterViewInit() {
  }

  initForm(dfsConfigDetailModel?: DFSConfigModel) {
    let dfsConfigModel = new DFSConfigModel(dfsConfigDetailModel);
    this.dfsConfigDialogForm = this.formBuilder.group({});
    Object.keys(dfsConfigModel).forEach((key) => {
      if (typeof dfsConfigModel[key] === 'object') {
        this.dfsConfigDialogForm.addControl(key, new FormArray(dfsConfigModel[key]));
      } else {
        this.dfsConfigDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : dfsConfigModel[key]));
      }
    });
    this.dfsConfigDialogForm.get('description').disable();
    this.dfsConfigDialogForm = setRequiredValidator(this.dfsConfigDialogForm, ["dealerTypeId", "dealerCategoryTypeId", "dsfValue"]);
    this.dfsConfigDialogForm = setPercentageValidator(this.dfsConfigDialogForm, ["dsfValue"]);
    this.onValueChanges();
  }

  onValueChanges() {
    this.dfsConfigDialogForm.get('dealerCategoryTypeId').valueChanges.subscribe(res => {
      console.log("res",res)
      console.log("res",this._dealerCategoryTypeList)
      if (res) {

        const desc = this._dealerCategoryTypeList.find(el => el?.value == res);
        this.dfsConfigDialogForm.get('description').patchValue(desc?.description);
      }
    })
  }

  btnCloseClick() {
    this.dfsConfigDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.dfsConfigDialogForm?.valid) {
      this.dfsConfigDialogForm?.markAllAsTouched();
      return;
    } else if (!this.dfsConfigDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const dfsObject = {
      ...this.dfsConfigDialogForm.value,
    }
    if (this.dfsConfigDialogForm.value?.dsfConfigId) {
      dfsObject['createdUserId'] = this.config?.data?.createdUserId;
    } else {
      dfsObject['createdUserId'] = this.config?.data?.createdUserId;
      delete dfsObject['dsfConfigId'];
    }
    delete dfsObject['description'];
    dfsObject['dealerCategoryTypeId'] = +dfsObject['dealerCategoryTypeId'];
    dfsObject['dealerTypeId'] = +dfsObject['dealerTypeId'];
    dfsObject['dsfValue'] = +dfsObject['dsfValue'];
    delete dfsObject['categoryDescription'];
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    let api = this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DFS_CONFIG, dfsObject)
    if (this.dfsConfigDialogForm.value?.dsfConfigId) {
      api = this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DFS_CONFIG, dfsObject);
    }
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.dfsConfigDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }
}
