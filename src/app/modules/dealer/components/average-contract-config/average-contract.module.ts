import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AverageContractAddEditComponent } from './average-contract-add-edit.component';
import { AverageContractListComponent } from "./average-contract-list.component";
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';
const routes: Routes = [{
  path: '', component: AverageContractListComponent, canActivate: [AuthGuard], data: { title: 'Average Contract List' }
}]
@NgModule({
  declarations: [AverageContractListComponent, AverageContractAddEditComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, MaterialModule, RouterModule.forChild(routes)],

  entryComponents: [AverageContractAddEditComponent],
})

export class AverageContractModule { }
