import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
@Component({
  selector: 'app-average-contract-add-edit',
  templateUrl: './average-contract-add-edit.component.html'
})

export class AverageContractAddEditComponent {

  averageAddEditDialogForm: FormGroup;
  loggedUser;
  averageContractConfigId;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private httpCancelService: HttpCancelService, private snackbarService: SnackbarService, private rxjsService: RxjsService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, private store: Store<AppState>,
    private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.averageContractConfigId = this.config.data?.row?.averageContractConfigId ? this.config.data?.row?.averageContractConfigId : null;
  }
  ngOnInit(): void {
    this.initForm();
    if (this.averageContractConfigId)
      this.getAverageContractDetail({ ...{ averageContractConfigId: this.averageContractConfigId } });
  }
  initForm() {
    this.averageAddEditDialogForm = new FormGroup({
      'contractFrom': new FormControl(null),
      'contractTo': new FormControl(null),
      'bonusMultiple': new FormControl(null),
    });
    this.averageAddEditDialogForm = setRequiredValidator(this.averageAddEditDialogForm, ['contractFrom', 'contractTo', 'bonusMultiple']);
    this.onValueChanges();
  }
  onValueChanges() {
    this.averageAddEditDialogForm
      .get("contractTo")
      .valueChanges.subscribe((contractTo: string) => {
        if (contractTo && Number(contractTo) <= 0)
          this.snackbarService.openSnackbar("ContractTo should be greater than 0.", ResponseMessageTypes.WARNING);
      });
    this.averageAddEditDialogForm
      .get("bonusMultiple")
      .valueChanges.subscribe((bonusMultiple: string) => {
        if (bonusMultiple && Number(bonusMultiple) <= 0)
          this.snackbarService.openSnackbar("BonusMultiple should be greater than 0.", ResponseMessageTypes.WARNING);
      });
  }
  getAverageContractDetail(params) {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_AVERAGE_CONTRACT_DETAIL, null, false,
      prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.setValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  setValue(response) {
    this.averageAddEditDialogForm.get("contractFrom").setValue(response.contractFrom);
    this.averageAddEditDialogForm.get("contractTo").setValue(response.contractTo);
    this.averageAddEditDialogForm.get("bonusMultiple").setValue(response.bonusMultiple);
  }
  onSubmitDialog() {
    if (!this.averageAddEditDialogForm?.valid) {
      return;
    }
    let dealerTypeObject = {
      contractFrom: this.averageAddEditDialogForm.value.contractFrom ? this.averageAddEditDialogForm.value.contractFrom : null,
      contractTo: this.averageAddEditDialogForm.value.contractTo ? this.averageAddEditDialogForm.value.contractTo : null,
      bonusMultiple: this.averageAddEditDialogForm.value.bonusMultiple ? this.averageAddEditDialogForm.value.bonusMultiple : null,
      isActive: true,
      createdUserId: this.loggedUser.userId,
      averageContractConfigId: this.averageContractConfigId
    };
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let api = this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_AVERAGE_CONTRACT, dealerTypeObject);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
      }
      this.ref.close(res);
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  btnCloseClick() {
    this.ref.close(false);
  }
}