import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerDocTypeDetailComponent } from '@modules/dealer';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';

@NgModule({
    declarations: [DealerDocTypeDetailComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,InputSwitchModule,
        RouterModule.forChild([
            { path: '', component: DealerDocTypeDetailComponent, canActivate:[AuthGuard],data: { title: 'Dealer Document Type Config' } }
        ])
    ],
    providers: []
})
export class DealerDocumentTypeModule { }
