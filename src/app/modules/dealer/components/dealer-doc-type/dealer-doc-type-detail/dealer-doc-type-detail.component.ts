import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { DealerDocTypeArrayDetailsModel, DealerDocTypeDetailModel } from '@modules/dealer/models/dealer-doc-type.model';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-dealer-doc-type-detail',
  templateUrl: './dealer-doc-type-detail.component.html'
})
export class DealerDocTypeDetailComponent implements OnInit {

  dealerDocTypeDetailForm: FormGroup;
  selectedIndex: any = 0;
  primengTableConfigProperties: any;
  isLoading: boolean;
  isSubmitted: boolean;
  isFormChangeDetected: boolean;
  dropdownSubscription: any;
  userData: any;

  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private route: ActivatedRoute, private dialog: MatDialog,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Document Type Config",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Document Type Config' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Document Type Config',
            dataKey: 'dealerDocumentTypeId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            formArrayName: 'dealerDocTypeDetailsArray',
            columns: [
              { field: 'dealerDocumentTypeName', displayName: 'Document Type', type: 'input_text', className: 'col-5', required: true },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1', required: true },
              { field: 'createdUserId', displayName: '', type: '', className: '' },
            ],
            isEditFormArray: false,
            isRemoveFormArray: true,
            detailsAPI: DealerModuleApiSuffixModels.DEALER_DOCUMENT_TYPE,
            postAPI: DealerModuleApiSuffixModels.DEALER_DOCUMENT_TYPE,
            deleteAPI: DealerModuleApiSuffixModels.DEALER_DOCUMENT_TYPE,
            moduleName: ModulesBasedApiSuffix.DEALER,
          }
        ]
      }
    }
    this.route.data.subscribe((data) => {
      this.selectedIndex = data?.index ? data?.index : 0;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.onLoadValue()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_DOCUMENT_TYPE]
      if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
      this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
    });
    }

  onLoadValue() {
    switch (this.selectedIndex) {
      case 0:
        this.initDealerDocTypeForm();
        break;
    }
    this.loadDealerDocTypeValue();
  }

  initDealerDocTypeForm(dealerDocTypeDetailModel?: DealerDocTypeDetailModel) {
    let dealerDocTypeModel = new DealerDocTypeDetailModel(dealerDocTypeDetailModel);
    this.dealerDocTypeDetailForm = this.formBuilder.group({});
    Object.keys(dealerDocTypeModel).forEach((key) => {
      if (typeof dealerDocTypeModel[key] === 'object') {
        this.dealerDocTypeDetailForm.addControl(key, new FormArray(dealerDocTypeModel[key]));
      } else {
        this.dealerDocTypeDetailForm.addControl(key, new FormControl(dealerDocTypeModel[key]));
      }
    });
    this.dealerDocTypeDetailForm = setRequiredValidator(this.dealerDocTypeDetailForm, ["dealerDocumentTypeName", "isActive"]);
  }

  loadDealerDocTypeValue() {
    let api: any;
    this.isLoading = true;
    api = this.crudService.get(ModulesBasedApiSuffix.DEALER, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].detailsAPI, null);
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = api.subscribe((resp: IApplicationResponse) => {
      if (resp.isSuccess && resp.statusCode === 200) {
        this.patchDealerDocTypeValue(resp);
      }
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  patchDealerDocTypeValue(resp) {
    let addObj = {};
    if (resp?.resources?.length) {
      resp?.resources.forEach(val => {
        switch (this.selectedIndex) {
          case 0:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey];
            break;
        }
        this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns.forEach(el => {
          addObj[el.field] = val[el.field];
          return el;
        });
        this.addFormArray(addObj);
        return val;
      });
    }
  }

  addFormArray(obj) {
    switch (this.selectedIndex) {
      case 0:
        this.initDealerDocTypeFormArray(obj);
        break;
    }
  }

  initDealerDocTypeFormArray(dealerDocTypeArrayDetailsModel?: DealerDocTypeArrayDetailsModel) {
    let dealerDocTypeDetailsModel = new DealerDocTypeArrayDetailsModel(dealerDocTypeArrayDetailsModel);
    let dealerDocTypeDetailsFormArray = this.formBuilder.group({});
    Object.keys(dealerDocTypeDetailsModel).forEach((key) => {
      dealerDocTypeDetailsFormArray.addControl(key, new FormControl({ value: dealerDocTypeDetailsModel[key], disabled: false }));
    });
    dealerDocTypeDetailsFormArray = setRequiredValidator(dealerDocTypeDetailsFormArray, ["dealerDocumentTypeName", "isActive"]);
    if (dealerDocTypeDetailsFormArray.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey).value) {
      this.getDealerDocTypeFormArray.push(dealerDocTypeDetailsFormArray);
    } else {
      this.getDealerDocTypeFormArray.insert(0, dealerDocTypeDetailsFormArray);
    }
  }

  addConfigItem() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.dealerDocTypeDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field] ||
      !this.dealerDocTypeDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1]?.field]) {
      this.dealerDocTypeDetailForm.markAllAsTouched();
      // this.snackbarService.openSnackbar(`Please enter the valid item`, ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  validateExist() {
    const findItem = this.getDealerDocTypeFormArray.value.find(el => el[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field].toLowerCase() == this.dealerDocTypeDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field].toLowerCase());
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    this.isFormChangeDetected = true;
    const addObj = {
      [this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]: null,
    }
    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns.forEach(el => {
      addObj[el.field] = this.dealerDocTypeDetailForm.value[el.field];
    });
    switch (this.selectedIndex) {
      case 0:
        addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = null;
        break;
    }
    this.addFormArray(addObj);
    this.dealerDocTypeDetailForm.get('dealerDocumentTypeName').reset();
    this.dealerDocTypeDetailForm.get('isActive').setValue(true);
  }

  validateExistItem(e) {
    const findItem = this.getDealerDocTypeFormArray.controls.filter(el => el.value[e.field].toLowerCase() === this.getDealerDocTypeFormArray.controls[e.index].get(e.field).value.toLowerCase());
    if (findItem.length > 1) {
      this.isSubmitted = true;
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.isSubmitted = false;
      return;
    }
  }

  removeConfigItem(i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let remItem;
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getDealerDocTypeFormArray?.get([i]).get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value) {
        this.isSubmitted = true;
        switch (this.selectedIndex) {
          case 0:
            remItem = { ids: this.getDealerDocTypeFormArray?.get([i]).get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value, modifiedUserId: this.userData?.userId, isDeleted: true }
            break;
        }
        this.crudService.deleteByParams(ModulesBasedApiSuffix.DEALER, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].deleteAPI, { body: remItem })
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response?.statusCode == 200) {
              this.getDealerDocTypeFormArray.removeAt(i);
              // this.dealerDocTypeDetailForm.reset();
              // this.clearFormArray(this.getDealerDocTypeFormArray);
              // this.onLoadValue();
            }
            this.isSubmitted = false;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      } else {
        this.getDealerDocTypeFormArray.removeAt(i);
      }
    });
  }

  get getDealerDocTypeFormArray(): FormArray {
    if (!this.dealerDocTypeDetailForm) return;
    return this.dealerDocTypeDetailForm.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].formArrayName) as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  editConfigItem(i) {
    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns.forEach(el => {
      this.getDealerDocTypeFormArray.controls[i].get(el.field).enable();
    });
  }

  onSubmitRequest() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate && !this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.isSubmitted = true;
    if (!this.isFormChangeDetected) {
      this.isFormChangeDetected = this.getDealerDocTypeFormArray.dirty;
    }
    const areFormClassNamesNotIncluded = this.isFormChangeDetected ? false :
      (!this.getDealerDocTypeFormArray.dirty || this.getDealerDocTypeFormArray.pristine || this.getDealerDocTypeFormArray.untouched);
    const areClassNamesListOneNotIncluded = !this.getDealerDocTypeFormArray.invalid;
    if ((this.getDealerDocTypeFormArray.invalid && this.getDealerDocTypeFormArray?.length) || (!this.getDealerDocTypeFormArray.valid)) {
      this.getDealerDocTypeFormArray.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else if (areFormClassNamesNotIncluded && areClassNamesListOneNotIncluded) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setPopupLoaderProperty(false);
      this.isSubmitted = false;
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else {
      const reqObj = this.createReqObj();
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.create(ModulesBasedApiSuffix.DEALER, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].postAPI, reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response?.statusCode == 200) {
            this.dealerDocTypeDetailForm.reset();
            this.clearFormArray(this.getDealerDocTypeFormArray);
            this.onLoadValue();
          }
          this.isFormChangeDetected = false;
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  setFormUnTouched() {
    switch (this.selectedIndex) {
      case 0:
        this.dealerDocTypeDetailForm.get('dealerDocumentTypeName').markAsUntouched();
        this.dealerDocTypeDetailForm.get('isActive').markAsUntouched();
        break;
    }
  }

  createReqObj() {
    const arrObj: any = [];
    this.setFormUnTouched();
    this.getDealerDocTypeFormArray.controls.forEach((el, i) => {
      switch (this.selectedIndex) {
        case 0:
          arrObj.push({
            dealerDocumentTypeId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value : null,
            dealerDocumentTypeName: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field]).value,
            isActive: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value,
            createdUserId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value : this.userData?.userId,
          })
          break;
      }
      return el;
    });
    return arrObj;
  }
}
