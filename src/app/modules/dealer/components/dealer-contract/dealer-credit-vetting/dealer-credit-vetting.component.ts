import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';

import {
  BreadCrumbModel, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService,
  IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator, SnackbarService, ResponseMessageTypes
} from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared/utils/customer-module.enums';
import { loggedInUserData, selectStaticEagerLoadingCardTypesState$, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
//import { MY_FORMATS } from '@modules/others/configuration/components/sales-configuration/seller-management/components/seller-profile/seller-profile-add-edit.component';
import { UserLogin } from '@modules/others/models';
import {
  CreditVettingInfo,
  CreditVettingModalComponent, LeadHeaderData,
  SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$, ServiceAgreementStepsParams
} from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { DealerCreditVettingModalComponent } from './dealer-credit-vetting-modal.component';

const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'dealer-credit-vetting-info',
  templateUrl: './dealer-credit-vetting.component.html',
  // styleUrls: ['./service-installation-agreement.scss'],
  providers: [DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})

export class DealerCreditVettingInfoComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  leadStatusCssClass: any;
  currentAgreementTabObj;
  isExistingDebtor = false;
  createNewBankAccount = false;
  details: any;
  bankSelected = true;
  monthArray: any = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  YearArray = [20];
  yearArray = [];
  titleList = [];
  bankList = [];
  branchList = [];
  accountTypeList = [];
  cardTypeList = [];
  selectedBranch: any;
  creditVettingInfoForm: FormGroup;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isAccountNumber = new CustomDirectiveConfig({ isAccountNumber: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  accountNumberAfterMask: any;
  IsInstallationDebtor: boolean = false;
  loggedInUserData: LoggedInUserModel;
  dogTypesList: unknown;
  maxDate = new Date();
  successMsg: boolean = false;
  fileName11: any;
  file_Name: any;
  customerId: string;
  debtorId: string;
  addressId: string;
  dealerId: string
  debtorAccountDetailId: string;
  breadCrumb: BreadCrumbModel;

  constructor(private crudService: CrudService, private dialog: MatDialog,
    private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private router: Router) {
    this.combineLatestNgrxStoreData();
    this.activatedRoute.queryParams.subscribe(params => {
      this.customerId = params.customerId;
      this.debtorId = params.debtorId;
      this.addressId = params.addressId;
      this.dealerId = params.dealerId;
      this.debtorAccountDetailId = params.debtorAccountDetailId?params.debtorAccountDetailId:''
    })
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadHeaderDataState$), this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingCardTypesState$)]
    ).subscribe((response) => {
      this.leadHeaderData = response[0];
      this.titleList = response[1];
      this.cardTypeList = response[2];
    });
  }

  ngOnInit() {

    this.yearArray = this.generateArrayOfYears();
    this.getDetails();
    this.createDebterForm();
    this.onFormControlChanges();
  }

  onFormControlChanges() {
    this.creditVettingInfoForm.get('isPassportDebtor').valueChanges.subscribe((isPassportDebtor: boolean) => {
      if (isPassportDebtor) {
        this.creditVettingInfoForm.get('passportNoDebtor').setValidators([Validators.required])
        this.creditVettingInfoForm.get('passportNoDebtor').updateValueAndValidity();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').setValidators([Validators.required])
        this.creditVettingInfoForm.get('passportExpiryDateDebt').updateValueAndValidity();
        this.creditVettingInfoForm.get('saidDebtor').clearValidators();
        this.creditVettingInfoForm.get('saidDebtor').updateValueAndValidity();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').enable();
      } else {
        this.creditVettingInfoForm.get('passportNoDebtor').clearValidators();
        this.creditVettingInfoForm.get('passportNoDebtor').updateValueAndValidity();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').clearValidators();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').updateValueAndValidity();
        this.creditVettingInfoForm.get('saidDebtor').setValidators([Validators.required])
        this.creditVettingInfoForm.get('saidDebtor').updateValueAndValidity();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').disable();
      }
    })
  }

  validate() {
    if (this.details.debtorInformation.debtorTypeId == 1) {
      this.creditVettingInfoForm.get('companyNameDebt').setValidators([Validators.required])
      this.creditVettingInfoForm.get('companyNameDebt').updateValueAndValidity();
      this.creditVettingInfoForm.get('companyRegNoDebt').setValidators([Validators.required])
      this.creditVettingInfoForm.get('companyRegNoDebt').updateValueAndValidity();
    } else {
      this.creditVettingInfoForm.get('companyNameDebt').clearValidators();
      this.creditVettingInfoForm.get('companyNameDebt').updateValueAndValidity();
      this.creditVettingInfoForm.get('companyRegNoDebt').clearValidators();
      this.creditVettingInfoForm.get('companyRegNoDebt').updateValueAndValidity();
    }
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CREDIT_VETTING_DEALERS, null, false,
      prepareGetRequestHttpParams(null, null,
        {
          customerId: this.customerId,
          debtorId: this.debtorId,
          addressId: this.addressId,
          debtorAccountDetailId: this.debtorAccountDetailId,
        }))
      .subscribe((response: IApplicationResponse) => {
        this.details = response.resources;
        if (this.details) {
          // this.details.debtorInformation.debtorTypeId = 1;
          // this.details.debtorInformation.systemOwnershipId = 2;
          if (this.details.debtorInformation) {
            if (this.details.debtorInformation.debtorTypeId == 1) {
              this.creditVettingInfoForm.get('companyNameDebt').setValidators([Validators.required])
              this.creditVettingInfoForm.get('companyNameDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('companyRegNoDebt').setValidators([Validators.required])
              this.creditVettingInfoForm.get('companyRegNoDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('passportNoDebtor').clearValidators();
              this.creditVettingInfoForm.get('passportNoDebtor').updateValueAndValidity();
              this.creditVettingInfoForm.get('passportExpiryDateDebt').clearValidators();
              this.creditVettingInfoForm.get('passportExpiryDateDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('saidDebtor').clearValidators();
              this.creditVettingInfoForm.get('saidDebtor').updateValueAndValidity();
              this.creditVettingInfoForm.get('dobDebt').clearValidators();
              this.creditVettingInfoForm.get('dobDebt').updateValueAndValidity();
            } else {
              this.creditVettingInfoForm.get('companyNameDebt').clearValidators();
              this.creditVettingInfoForm.get('companyNameDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('companyRegNoDebt').clearValidators();
              this.creditVettingInfoForm.get('companyRegNoDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('passportNoDebtor').setValidators([Validators.required])
              this.creditVettingInfoForm.get('passportNoDebtor').updateValueAndValidity();
              this.creditVettingInfoForm.get('passportExpiryDateDebt').setValidators([Validators.required])
              this.creditVettingInfoForm.get('passportExpiryDateDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('saidDebtor').setValidators([Validators.required])
              this.creditVettingInfoForm.get('saidDebtor').updateValueAndValidity();
              this.creditVettingInfoForm.get('dobDebt').setValidators([Validators.required])
              this.creditVettingInfoForm.get('dobDebt').updateValueAndValidity();
            }
            if (!this.details.debtorInformation.companyRegNo && !this.details.debtorInformation.said) {
              this.details.debtorInformation.isPassport = true;
            } else {
              this.details.debtorInformation.isPassport = false;
            }
            this.creditVettingInfoForm.patchValue({
              isPassportDebtor: this.details.debtorInformation.isPassport,
              dobDebt: this.details.debtorInformation.dob,
              passportExpiryDateDebt: this.details.debtorInformation.passportExpiryDate,
              passportNoDebtor: this.details.debtorInformation.passportNo,
              saidDebtor: this.details.debtorInformation.said,
              systemOwnershipId: this.details.debtorInformation.systemOwnershipId,
              companyNameDebt: this.details.debtorInformation.companyName ? this.details.debtorInformation.companyName : null,
              companyRegNoDebt: this.details.debtorInformation.companyRegNo,
            })
          }

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createDebterForm() {
    let debtorBankingDetailsModel = new CreditVettingInfo();
    this.creditVettingInfoForm = this.formBuilder.group({});
    Object.keys(debtorBankingDetailsModel).forEach((key) => {
      this.creditVettingInfoForm.addControl(key,
        new FormControl(debtorBankingDetailsModel[key]));
    });
    this.creditVettingInfoForm = setRequiredValidator(this.creditVettingInfoForm, ["dobDebt", "saidDebtor"]);
  }

  uploadFiles(files) {
    this.fileName11 = files[0];
    this.file_Name = files[0]['name'];
    this.creditVettingInfoForm.controls.documentName.setValue(this.file_Name);
  }

  generateArrayOfYears() {
    var min = new Date().getFullYear();
    var max = 2099
    var years = []
    for (var i = min; i <= max; i++) {
      years.push(i)
    }
    return years
  }

  validateDebtor() {
    const dialogReff = this.dialog.open(DealerCreditVettingModalComponent, { width: '960px', data: this.details.debtorInformation, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData1.subscribe(ele => {
      if (ele) {
        this.details.debtorInformation.itNumber = ele.itNumber;
      }
    })
  }

  onSubmit() {
    this.validate();
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.details.debtorInformation.debtorTypeId == 1) {
      this.creditVettingInfoForm.get('saidDebtor').setValue(this.creditVettingInfoForm.value.companyRegNoDebt)
    }
    if (this.creditVettingInfoForm.invalid) {
      return;
    }

    if (this.details.debtorInformation.debtorTypeId == 1) {
      if (!this.details.debtorInformation.itNumber) {
        this.snackbarService.openSnackbar("Please click Validate debtor to get IT number before proceeding credit vetting", ResponseMessageTypes.ERROR);
        return;
      }
    }


    if (this.creditVettingInfoForm.value.isPassportDebtor) {
      this.creditVettingInfoForm.get('saidDebtor').setValue(null)
    } else {
      this.creditVettingInfoForm.get('passportNoDebtor').setValue(null)
      this.creditVettingInfoForm.get('passportExpiryDateDebt').setValue(null)
    }
    let data = this.creditVettingInfoForm.getRawValue();
    let obj = {
      'debtorInformation': {},
      'debtorBankingInformation': {},
      'createdUserId': this.loggedInUserData.userId
    };
    obj.debtorInformation = {
      "debtorId": this.debtorId,
      "customerId": this.details.debtorInformation ? this.details.debtorInformation.customerId : null,
      "leadId": this.details.debtorInformation ? this.details.debtorInformation.leadId : null,
      "debtorRefNo": this.details.debtorInformation ? this.details.debtorInformation.debtorRefNo : null,
      "debtorName": this.details.debtorInformation ? this.details.debtorInformation.debtorName : null,
      "debtorType": this.details.debtorInformation ? this.details.debtorInformation.debtorType : null,
      "itNumber": this.details.debtorInformation ? this.details.debtorInformation.itNumber : null,
      "systemOwnershipId": 2,
      // "systemOwnershipId": this.details.debtorInformation ? this.details.debtorInformation.systemOwnershipId : 2,
      "customerAddress": this.details.debtorInformation ? this.details.debtorInformation.customerAddress : null,
      "firstName": this.details.debtorInformation ? this.details.debtorInformation.firstName : null,
      "lastName": this.details.debtorInformation ? this.details.debtorInformation.lastName : null,
      "said": data.saidDebtor,
      "mobile1": this.details.debtorInformation ? this.details.debtorInformation.mobile1 : '',
      "mobile1CountryCode": this.details.debtorInformation ? this.details.debtorInformation.mobile1CountryCode : '',
      "officeNo": null,
      "officeNoCountryCode": null,
      "foreName1": this.details.debtorInformation ? this.details.debtorInformation.foreName1 : null,
      "foreName2": this.details.debtorInformation ? this.details.debtorInformation.foreName2 : null,
      "buildingNumber": this.details.debtorInformation ? this.details.debtorInformation.buildingNumber : null,
      "streetNumber": this.details.debtorInformation ? this.details.debtorInformation.streetNumber : null,
      "isPassport": data.isPassportDebtor,
      "dob": data.dobDebt,
      "passportNo": data.passportNoDebtor,
      "companyName": data.companyNameDebt,
      "companyRegNo": data.companyRegNoDebt,
      "passportExpiryDate": data.passportExpiryDateDebt,
      "debtorTypeId": this.details.debtorInformation ? this.details.debtorInformation.debtorTypeId : null,
      "isDebtorSameAsCustomer": this.details.debtorInformation ? this.details.debtorInformation.isDebtorSameAsCustomer : false,
      'systemOwnership': data.systemOwnership,
      "isDebtorVetted": this.details.debtorInformation ? this.details.debtorInformation.isDebtorVetted : false,
    }
    obj.debtorBankingInformation = {
      "accountHolderName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountHolderName : null,
      "companyName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.companyName : null,
      "isBankAccount": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.isBankAccount : false,
      "bankName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.bankName : null,
      "bankBranchName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.bankBranchName : null,
      "bankBranchCode": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.bankBranchCode : null,
      "accountNo": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountNo : null,
      "accountTypeName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountTypeName : null,
      "cardNo": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.cardNo : null,
      "expiryDate": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.expiryDate : null,
      "cardTypeName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.cardTypeName : null,
      "accountTypeDescription": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountTypeDescription : null,
      "accountTypeId": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountTypeId : null,
      "isDebtorAccountVetted": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.isDebtorAccountVetted : false,
      "debtorAccountDetailId": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.debtorAccountDetailId : null,
      "systemOwnershipId": 2,

    }
    let formData = new FormData();
    formData.append('data', JSON.stringify(obj));
    if (this.fileName11) {
      formData.append('file', this.fileName11);
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CREDIT_VETTING, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
        //  this.saveDealerClassification();
        this.successMsg = true;
        }
      })
  }

  redirectToDebtorBankingDetails() {
    this.router.navigate(['/dealer/dealer-contract/debtor-banking-creation'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
  }



  cancel() {
    this.successMsg = false;
  }
}
