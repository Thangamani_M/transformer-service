import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams } from '@app/shared/utils';
@Component({
  selector: 'app-credit-vetting',
  templateUrl: './dealer-credit-vetting-modal.component.html'
})

export class DealerCreditVettingModalComponent implements OnInit {
  creditVettingReport: any;
  @Output() outputData1 = new EventEmitter<any>();
  currentDate = new Date();
  canvasWidth = 300;
  needleValue = 350;
  bottomLabel: '350';
  name: any;
  centralLabel = '350';
  btnName = 'Proceed';
  options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 100,
    arcColors: ["rgb(255,84,84)", "rgb(239,214,19)", "rgb(61,204,91)"],
    arcDelimiters: [40, 60],
    rangeLabel: ['0', '900'],
    needleStartValue: 10,
  }
  isRequestInProgress = false;
  listData: any;
  searchType = "";

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService, @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialog) {
    this.data['tradingNo'] = '';
  }

  selectedDebtor: any;
  select(e, i, dt) {
    this.selectedDebtor = dt;
    this.listData.forEach((element, index) => {
      if (index == i) {
        element.isChecked = true;
      } else {
        element.isChecked = false;
      }
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
  }

  submit() {
    this.outputData1.emit(this.selectedDebtor);
    this.dialog.closeAll();
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.VALIDATE_CUSTOMER, null, false,
      prepareRequiredHttpParams(
        {
          searchType: this.searchType,
          subjectName: this.searchType == 'Name' ? this.data.companyName : '',
          TradingNo: this.searchType == 'TradingNo' ? this.data.tradingNo : '',
          registrationNo: this.searchType == 'RegNo' ? this.data.companyRegNo : '',
        }
      ))
      .subscribe((response: IApplicationResponse) => {
        this.listData = response.resources;
        if (response.resources && response.resources.length > 0) {
          this.listData.forEach(element => {
            element.isChecked = false;
          });
        }

      });
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
