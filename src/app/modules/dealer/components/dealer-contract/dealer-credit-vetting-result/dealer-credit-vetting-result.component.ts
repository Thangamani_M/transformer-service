import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService,
  IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { selectStaticEagerLoadingCardTypesState$, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
//import { MY_FORMATS } from '@modules/others/configuration/components/sales-configuration/seller-management/components/seller-profile/seller-profile-add-edit.component';
import {
  CreditVettingInfo, LeadHeaderData,
  selectLeadHeaderDataState$, ServiceAgreementStepsParams
} from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'dealer-credit-vetting-result',
  templateUrl: './dealer-credit-vetting-result.component.html',
  providers: [DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})

export class DealerCreditVettingResultComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  leadStatusCssClass: any;
  currentAgreementTabObj;
  isExistingDebtor = false;
  createNewBankAccount = false;
  details: any;
  bankSelected = true;
  monthArray: any = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  YearArray = [20];
  yearArray = [];
  titleList = [];
  bankList = [];
  branchList = [];
  accountTypeList = [];
  cardTypeList = [];
  selectedBranch: any;
  creditVettingInfoForm: FormGroup;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isAccountNumber = new CustomDirectiveConfig({ isAccountNumber: true });
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  accountNumberAfterMask: any;
  loggedInUserData: LoggedInUserModel;
  dogTypesList: unknown;
  maxDate = new Date();
  successMsg: boolean = false;
  canvasWidth = 500;
  needleValue: Number = 0;
  centralLabel: any;
  bottomLabel: any;
  name: any;
  // centralLabel = '600';
  btnName = 'Proceed';
  options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    arcColors: ["rgb(255,84,84)", "rgb(239,214,19)", "rgb(61,204,91)"],
    arcDelimiters: [35, 70],
    rangeLabel: ['0', '900'],
    needleStartValue: 10,
  }
  needleValue2: Number = 0;
  bottomLabel2: any;
  name2: any = '';
  centralLabel2 = '600';
  btnName2 = 'Proceed';
  options2 = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    arcColors: ["rgb(255,84,84)","rgb(239,214,19)","rgb(61,204,91)"],
    arcDelimiters: [35, 70],
    rangeLabel: ['0', '900'],
    needleStartValue: 10,
  }
  customerId: any;
  debtorId: any;
  addressId: any;
  debtorAccountDetailId: any;
  fileName11: any;
  file_Name: any;
  breadCrumb: BreadCrumbModel;

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private router: Router) {
    this.combineLatestNgrxStoreData();
    this.activatedRoute.queryParams.subscribe(params => {
      this.customerId = params.customerId;
      this.debtorId = params.debtorId;
      this.debtorId = params.debtorId;
      this.addressId = params.addressId;
      this.debtorAccountDetailId = params.debtorAccountDetailId?params.debtorAccountDetailId:'';
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadHeaderDataState$), this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingCardTypesState$)]
    ).subscribe((response) => {
      this.leadHeaderData = response[0];
      this.titleList = response[1];
      this.cardTypeList = response[2];
    });
  }

  ngOnInit() {

    this.yearArray = this.generateArrayOfYears();
    this.getDetails();
    this.createDebterForm();
    this.onFormControlChanges();
  }

  createDebterForm() {
    let debtorBankingDetailsModel = new CreditVettingInfo();
    this.creditVettingInfoForm = this.formBuilder.group({});
    Object.keys(debtorBankingDetailsModel).forEach((key) => {
      this.creditVettingInfoForm.addControl(key,
        new FormControl(debtorBankingDetailsModel[key]));
    });
  }

  onFormControlChanges() {
    this.creditVettingInfoForm.get('isPassportCust').valueChanges.subscribe((isPassportCust: boolean) => {
      if (!isPassportCust) {
        this.creditVettingInfoForm.get('passportNoCustomer').setValidators([Validators.required])
        this.creditVettingInfoForm.get('passportNoCustomer').updateValueAndValidity();
        this.creditVettingInfoForm.get('expiryCustDate').setValidators([Validators.required])
        this.creditVettingInfoForm.get('expiryCustDate').updateValueAndValidity();
        this.creditVettingInfoForm.get('expiryCustMonth').setValidators([Validators.required])
        this.creditVettingInfoForm.get('expiryCustMonth').updateValueAndValidity();
      } else {
        this.creditVettingInfoForm.get('passportNoCustomer').clearValidators();
        this.creditVettingInfoForm.get('passportNoCustomer').updateValueAndValidity();
        this.creditVettingInfoForm.get('expiryCustDate').clearValidators();
        this.creditVettingInfoForm.get('expiryCustDate').updateValueAndValidity();
        this.creditVettingInfoForm.get('expiryCustMonth').clearValidators();
        this.creditVettingInfoForm.get('expiryCustMonth').updateValueAndValidity();
      }
    })
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CREDIT_VETTING_DEALERS_SCORE, null, false,
      prepareGetRequestHttpParams(null, null,
        {
          customerId: this.customerId,
          debtorAccountDetailId: this.debtorAccountDetailId,
          debtorId: this.debtorId,
          addressId: this.addressId,
        }))
      .subscribe((response: IApplicationResponse) => {
        this.details = response.resources;

        if (this.details && this.details.debtorInformation) {
          // this.details.debtorInformation.comments ='<table class="fidelity-table table-bordered table-responsive"> <thead> <th colspan="2">Account Verification</th> </thead> <tbody> <tr> <td>Verified</td> <td>Y</td> </tr> <tr> <td>Verification Date</td> <td>1323424</td> </tr> <tr> <td><b>Legends</b></td> <td>The bank confirms that the field value is true</td> </tr> </tbody> </table>'
          let rating = "";
          if (this.details.debtorInformation.isDynamicRating) {
            this.options2.rangeLabel = ['0', '5'];
            rating = 'Dynamic';
            this.bottomLabel2 = rating + ' Score :' + this.details.debtorInformation.creditScore;
            this.needleValue2 = (parseFloat(this.details.debtorInformation.creditScore) / 5) * 100;
          }
          if (this.details.debtorInformation.isSME) {
            this.options2.rangeLabel = ['0', '900'];

            rating = 'SME'
            this.bottomLabel2 = rating + ' Score :' + this.details.debtorInformation.creditScore;
            this.needleValue2 = (parseFloat(this.details.debtorInformation.creditScore) / 900) * 100;
          }
          if (!this.details.debtorInformation.isSME && !this.details.debtorInformation.isDynamicRating) {
            this.options2.rangeLabel = ['0', '900'];
            this.bottomLabel2 = rating + ' Score :' + this.details.debtorInformation.creditScore;
            this.needleValue2 = (parseFloat(this.details.debtorInformation.creditScore) / 900) * 100;
          }

        }
        if (this.details && this.details.debtorBankingInformation && this.details.debtorBankingInformation.accountNo) {
          // this.details.debtorBankingInformation.bankAccountFailureReason ='<table class="fidelity-table mt-2 service-table w-100 table-responsive"><thead><th style="border: 1px solid black;">AccountHolder</th><th style="border: 1px solid black;">AccountNumber</th><th style="border: 1px solid black;">AccountFound</th><th style="border: 1px solid black;">Verified</th><th style="border: 1px solid black;">VerifiedDated</th><th style="border: 1px solid black;">AccountOpen</th><th style="border: 1px solid black;">AccountOpen3Months</th><th style="border: 1px solid black;">IDMatch</th><th style="border: 1px solid black;">AccountDormant</th><th style="border: 1px solid black;">ErrorMessage</th></thead><tbody><tr><td style="border: 1px solid black;">MR.MEI CHUNG</td><td style="border: 1px solid black;">800412670450</td><td style="border: 1px solid black;">N</td><td style="border: 1px solid black;">N</td><td style="border: 1px solid black;">20210728</td><td style="border: 1px solid black;">U</td><td style="border: 1px solid black;">U</td><td style="border: 1px solid black;">U</td><td style="border: 1px solid black;">U</td><td style="border: 1px solid black;">Success</td></tr></tbody></table>'

          let len = this.details.debtorBankingInformation.accountNo.length - 4;
          let first = this.details.debtorBankingInformation.accountNo.substring(0, len);
          let last = this.details.debtorBankingInformation.accountNo.substring(len, this.details.debtorBankingInformation.accountNo.length);
          let str = ''
          for (let i = 0; i < first.length; i++) {
            str = str + '*';
          }
          this.details.debtorBankingInformation.accountNo = str + '' + last;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  generateArrayOfYears() {
    var min = new Date().getFullYear();
    var max = 2099
    var years = []
    for (var i = min; i <= max; i++) {
      years.push(i)
    }
    return years;
  }

  uploadFiles(files) {
    this.fileName11 = files[0];
    this.file_Name = files[0]['name'];
  }

  onSubmit() {
    let obj = {
      'customerId': this.customerId
    }
    let formData = new FormData();
    formData.append('data', JSON.stringify(obj));
    if (this.fileName11) {
      formData.append('file', this.fileName11);
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CREDIT_VETTING_DEALERS_PROOF_OF_DOCUMENT, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
          this.redirectToDebtorBankingDetails();

        }
      })
  }

  redirectToDebtorBankingDetails() {
    this.router.navigate(['/dealer/dealer-contract/debtor-banking-creation'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
  }
}
