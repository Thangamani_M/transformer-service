import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-dealer-contract-list',
  templateUrl: './dealer-contract-list.component.html',
  styleUrls: ['./dealer-contract-list.component.scss']
})
export class DealerContractListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {}
  status: any = [];
  siteType: any =[];

    constructor(private rxjsService: RxjsService,
      private crudService: CrudService,
      private store: Store<AppState>,
      private snackbarService: SnackbarService,
      private router : Router) {
      super()

    this.primengTableConfigProperties = {
      tableCaption: "Customer Contract",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Customer Contract ', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Customer Contract',
            dataKey: 'customerRefNo',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'customerRefNo', header: 'CAF Number', width: '200px' },
              { field: 'clientName', header: 'Client Name', width: '200px' },
              { field: 'fullAddress', header: 'Client Address', width: '200px' },
              { field: 'contactNo', header: 'Mobile Number', width: '200px' },
              { field: 'email', header: 'Email Address', width: '200px' },
              { field: 'suburbName', header: 'Suburb', width: '200px' },
              { field: 'siteType', header: 'Site Type', width: '200px',type:'dropdown', options:[] , placeholder:'Select Site Type' },
              { field: 'customerStatus', header: 'Status', width: '200px', type:'dropdown', options:[] },

            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_CUSTOMER,
            moduleName: ModulesBasedApiSuffix.DEALER,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.combineLatestNgrxStoreData()
    this.getDealerContractListData();
    this.getStatus();
    this.getDealerCustomerStatus();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
    this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
     this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][DEALER_COMPONENT.CUSTOMER_CONTRACT]
      if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
      this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
    });
    }


  getStatus(){
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      DealerModuleApiSuffixModels.UX_STATIC,
      undefined,
      false
    ).subscribe((data: IApplicationResponse) => {

      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.siteType = data.resources.customerTypes;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[6].options =  this.siteType  =  this.siteType.map(item => {
          return  {label: item.displayName,value:item.id};
        });

      } else {
        this.siteType = [];

      }
    })
  }

  getDealerCustomerStatus(){
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.UX_DEALER_CUSTOMER_STATUS,
      undefined,
      false
    ).subscribe((data: IApplicationResponse) => {

      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.status = data.resources;

         this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[7].options =  this.status  =  this.status.map(item => {
            return  {label: item.displayName,value:item.id};
          });

      } else {
        this.status = [];

      }
    })
  }

  getDealerContractListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams= {...otherParams, userId: this.loggedInUserData?.userId}
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.map(item=>{
          item.cssClass = item.customerStatusCssClass
        });
        this.dataList =data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {

      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getDealerContractListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['dealer/dealer-contract/add-edit']);
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['dealer/dealer-contract/add-edit'], {queryParams:{customerId: editableObject.customerId, addressId:editableObject.addressId}});
            break;
        }
        break;
    }
  }



  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col)
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
