import { Component, Inject, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import {
  ConfirmDialogModel,
  ConfirmDialogPopupComponent,
  CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse,
  ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { DealerModuleApiSuffixModels, DealerPartitionCreationModel } from '@modules/dealer';
import { PartitionCreationModel, Partitions, SalesModuleApiSuffixModels } from '@modules/sales';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-dealer-partition-popup',
  templateUrl: './dealer-partition-popup.component.html',
  styleUrls: ['./dealer-latest-service-info.component.scss']
})

export class DealerPartitionPopupComponent implements OnInit {
  formConfigs = formConfigs;
  partitionCreationForm: FormGroup;
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  partitionsList = [];
  secondaryPartitions = [];
  partitions: FormArray;
  allValidAndDisabledArrayValuesLength = 1;
  paymentTypeId: number;
  @ViewChildren('input') rows: QueryList<any>;

  constructor(@Inject(MAT_DIALOG_DATA) public partitionData,
    private dialog: MatDialog, private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private crudService: CrudService, private httpCancelService: HttpCancelService, private snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createPartitionForm();
  }

  getPartitions(form): Array<any> {
    return form.controls.partitions.controls;
  }

  createPrimaryBasedSecondaryPartitionsForm(formGroup: FormGroup): FormGroup {
    formGroup = this.formBuilder.group({
      allPartitions: new FormArray([])
    });
    return formGroup;
  }

  get partitionsArray(): FormArray {
    if (!this.partitionCreationForm) return;
    return this.partitionCreationForm.get('partitions') as FormArray;
  }

  findDuplicate(formGroup: FormGroup, formArray: FormArray, duplicateFormControlName: string, currentCursorIndex: number): void {
    const lookup = formArray.value.reduce((a, e) => {
      if (a[e[duplicateFormControlName]]) {
        a[e[duplicateFormControlName]] = a[e[duplicateFormControlName]] + 1;
      }
      else {
        a[e[duplicateFormControlName]] = 1;
      }
      return a;
    }, {});

    const duplicatedDataOne = this.getPartitions(formGroup).
      filter((duplicatedFormGroup: FormGroup) => {
        let filteredResult = lookup[duplicatedFormGroup.controls[duplicateFormControlName].value] !== 1 &&
          lookup[duplicatedFormGroup.controls[duplicateFormControlName].value] !== "";
        if (filteredResult) {
          return duplicatedFormGroup;
        }
      });

    if (currentCursorIndex <= duplicatedDataOne.length) {
      if (duplicatedDataOne.length > 1) {
        duplicatedDataOne.forEach((formGroup) => {
          Object.keys(formGroup.controls).forEach((formControlName) => {
            if (formControlName === duplicateFormControlName && formGroup.controls[duplicateFormControlName].value && formGroup.controls['currentCursorIndex'].value == currentCursorIndex) {
              formGroup.get(duplicateFormControlName).setErrors({ duplicate: true });
            }
          });
        });
      }
    }
    else {
      duplicatedDataOne.forEach((formGroup, ix) => {
        Object.keys(formGroup.controls).forEach(() => {
          if (ix === duplicatedDataOne.length - 1) {
            formGroup.get(duplicateFormControlName).setErrors({ duplicate: true });
          }
        });
      });
    }
  }

  createPartitionForm(): void {
    this.partitionData.addressId = this.partitionData.addressId;
    let partitionCreationModel = new DealerPartitionCreationModel(this.partitionData);
    this.partitionCreationForm = this.formBuilder.group({});
    Object.keys(partitionCreationModel).forEach((key) => {
      if (key === 'partitions') {
        this.partitionCreationForm.addControl(key, new FormArray([]));
      }
      else {
        this.partitionCreationForm.addControl(key, new FormControl(partitionCreationModel[key]));
      }
    });
    this.generateOrGetAllPartitions();
  }

  generateOrGetAllPartitions(noOfPartitions?: string): void {
    this.partitions = this.partitionsArray;
    if (this.partitions.invalid && noOfPartitions) {
      this.focusInAndOutFormArrayFields();
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_PARTITIONS_GENERATE, undefined,
      false, prepareRequiredHttpParams(
        {
          noOfPartitions: noOfPartitions ? noOfPartitions : this.partitionCreationForm.get('noOfPartitions').value,
          customerId: this.partitionCreationForm.get('customerId').value,
          addressId: this.partitionCreationForm.get('addressId').value,
          isSingle: noOfPartitions ? true : false
        }), 1)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {

          resp.resources.sort((a, b) => {
            return a.serialNumber - b.serialNumber;
          });

          if (noOfPartitions) {
            this.partitions.insert(0, this.addNewPartition(resp.resources[0], 0, 'create'));
          }
          else {
            if (this.partitions) {
              while (this.partitions.length) {
                this.partitions.removeAt(this.partitions.length - 1);
              }
            }
            resp.resources.forEach((partitions: any, index: number) => {
              this.partitions.push(this.addNewPartition(partitions, index, 'update'));
            });
          }
          this.prepareIsCheckedPropValues();
          // if the isChecked length zero save button is disabled logic
          for (let i = 0; i < this.partitionsArray.length; i++) {
            this.partitionsArray.controls[i].get('isChecked').valueChanges.subscribe((isChecked: boolean) => {
              const isCheckedArrayLength = this.partitions.value.filter(p => p['isChecked'] || p['isChecked'] == undefined).length;
              this.allValidAndDisabledArrayValuesLength = (isCheckedArrayLength == 1 && !isChecked) ? 0 : isCheckedArrayLength + 1;
            });
          }
          if (this.paymentTypeId == 2) {
            this.allValidAndDisabledArrayValuesLength = (this.partitionsArray.length == this.partitions.value.filter(p => p['isChecked'] || p['isPrimary']).length) ? 0 : 1;
          }
        }
      });
  }

  prepareIsCheckedPropValues() {
    this.paymentTypeId = +this.partitionCreationForm.get('paymentTypeId').value;
    this.partitions.controls.forEach((partitionAbstractControl, index: number) => {
      switch (this.paymentTypeId) {
        // pay for all
        case 1:
          partitionAbstractControl.get('isChecked').setValue(true);
          partitionAbstractControl.get('isChecked').disable();
          break;
        // pay partially
        case 2:
          if (partitionAbstractControl.get('isPrimary').value) {
            partitionAbstractControl.get('isChecked').setValue(true);
            partitionAbstractControl.get('isChecked').disable();
          }
          else if (partitionAbstractControl.get('partitionCustomerId').value) {
            partitionAbstractControl.get('isChecked').setValue(true);
            partitionAbstractControl.get('isChecked').enable();
          }
          else if (!partitionAbstractControl.get('partitionCustomerId').value) {
            partitionAbstractControl.get('isChecked').setValue(false);
            partitionAbstractControl.get('isChecked').enable();
          }
          break;
        // pay for standalone alias primary
        case 3:
          if (partitionAbstractControl.get('isPrimary').value) {
            partitionAbstractControl.get('isChecked').setValue(true);
          }
          else {
            partitionAbstractControl.get('isChecked').setValue(false);
          }
          partitionAbstractControl.get('isChecked').disable();
          break;
      }
    })
  }

  onChange() {
    if (this.paymentTypeId == 2) {
      this.allValidAndDisabledArrayValuesLength = (this.partitionsArray.length == this.partitions.value.filter(p => p['isChecked'] || p['isPrimary']).length) ? 0 : 1;
    }
  }

  addNewPartition(partitionModel: any, index: number, type: string): FormGroup {
    if (type == 'create') {
      partitionModel.partitionCustomerId = this.partitionData.customerId;
    }
    let partitionsModel = new Partitions(partitionModel);
    let formControls = {};
    Object.keys(partitionsModel).forEach((key) => {
      formControls[key] = [{
        value: key == 'index' ? index : partitionsModel[key],
        disabled: (key === 'partitionCode' || key === 'serialNumber') ? true : false
      },
      key === 'partitionName' ?
        [Validators.required] : []];
    });
    return this.formBuilder.group(formControls);
  }
  onPartitionNameChanges(currentCursorIndex: number): void {
    this.partitionsArray.controls[currentCursorIndex].get('currentCursorIndex').setValue(currentCursorIndex);
    this.partitionsArray.controls[currentCursorIndex].get('partitionName').valueChanges.subscribe((partitionName: string) => {
      setTimeout(() => {
        this.findDuplicate(this.partitionCreationForm, this.partitions, 'partitionName', currentCursorIndex);
      }, 100)
    });
  }

  onBlur(e, index: number) {
    if (this.partitionsArray.controls[index].get('partitionName').errors) {
      if (this.partitionsArray.controls[index].get('partitionName').errors.hasOwnProperty('duplicate')) {
        var event = this.disableAllUserEvents();
        e.target.focus();
        event();
        return;
      }
    }
  }

  addPartition(): void {
    if (this.partitionCreationForm.get('paymentTypeId').value == '3') {
      return;
    }
    if (this.partitions.length > 15) {
      this.snackbarService.openSnackbar("Maximum Partitions Limit is exceeded..!!", ResponseMessageTypes.WARNING);
      return;
    }
    this.generateOrGetAllPartitions('1');
  }

  removePartition(i: number): void {
    if (!this.partitions.controls[i].value.partitionId) {
      this.partitions.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.partitions.controls[i].value.partitionId && this.partitions.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_PARTITIONS, undefined,
          prepareRequiredHttpParams({
            partitionId: this.partitions.controls[i].value.partitionId,
            customerId: this.partitionData.customerId,
            addressId: this.partitionData.addressId,
            createdUserId: this.partitionData.userId,
            isDeleted: true
          }), 1)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.partitions.removeAt(i);
            }
            if (this.partitions.length === 0) {
              this.addPartition();
            };
          });
      }
      else {
        this.partitions.removeAt(i);
      }
    });
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  onSubmit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.focusInAndOutFormArrayFields();
    let partitionsRawArray = [...this.partitions.getRawValue()];
    this.partitionCreationForm.value.partitions = partitionsRawArray;
    if (this.partitions.length === 0 || this.partitionCreationForm.invalid) return;
    this.partitionCreationForm.value.dealerId = this.partitionData.dealerId;
    this.partitionCreationForm.value.dealerCategoryTypeId = this.partitionData.dealerCategoryTypeId;
    this.partitionCreationForm.value.partitions.forEach((partition) => {
      partition.partitionCustomerId = partition.isChecked ? this.partitionData.customerId : null
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_PARTITIONS, this.partitionCreationForm.value, 1).pipe(tap(() => {
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dialog.closeAll();
        }
      });
  }

  disableAllUserEvents = () => {
    const events = [
      "click",
      "contextmenu",
      "dblclick",
      "mousedown",
      "mouseenter",
      "mouseleave",
      "mousemove",
      "mouseover",
      "mouseout",
      "mouseup",
      "blur",
      "change",
      "focus",
      "focusin",
      "focusout",
      "input",
      "invalid",
      "reset",
      "search",
      "select",
      "submit",
      "drag",
      "dragend",
      "dragenter",
      "dragleave",
      "dragover",
      "dragstart",
      "drop",
      "copy",
      "cut",
      "paste",
      "mousewheel",
      "wheel",
      "touchcancel",
      "touchend",
      "touchmove",
      "touchstart"
    ];

    const handler = event => {
      event.stopPropagation();
      event.preventDefault();

      return false;
    };

    for (let i = 0, l = events.length; i < l; i++) {
      document.addEventListener(events[i], handler, true);
    }

    return () => {
      for (let i = 0, l = events.length; i < l; i++) {
        document.removeEventListener(events[i], handler, true);
      }
    };
  };

  ngOnDestroy(): void {
    this.rxjsService.setPropertyValue(this.partitions.length);
    this.rxjsService.setDialogOpenProperty(false);
  }

}
