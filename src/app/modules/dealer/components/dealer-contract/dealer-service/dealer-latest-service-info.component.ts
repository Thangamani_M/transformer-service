
import { Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  addFormControls, BreadCrumbModel, ComponentProperties, ConfirmDialogModel, ConfirmDialogPopupComponent,
  CustomDirectiveConfig, formConfigs,
  getPDropdownData,
  IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix,
  prepareRequiredHttpParams, removeFormControlError, removeFormControls, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { DealerHeader, DealerModuleApiSuffixModels, DealerServiceInfoFormModel } from '@modules/dealer';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import {
  loggedInUserData, selectStaticEagerLoadingBillingIntervalsState$,
  selectStaticEagerLoadingContractPeriodsState$, selectStaticEagerLoadingPartitionTypesState$,
  selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingPaymentTypesState$
} from '@modules/others';
import {
  BillingModuleApiSuffixModels,
  LeadCreationUserDataModel,

  LeadOutcomeStatusNames,
  PartitionCreationModel, SalesModuleApiSuffixModels, ServicesModel
} from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { DealerCustomerHeaderState$ } from '../dealer-header/dealer-header-ngrx-files';
import { DealerPartitionPopupComponent } from './dealer-partition-popup.component';
import { DealerServicePopupComponent } from './dealer-service-popup.component';

declare var $;

@Component({
  selector: 'app-dealer-latest-service-info',
  templateUrl: './dealer-latest-service-info.component.html',
  styleUrls: ['./dealer-latest-service-info.component.scss']
})

export class DealerLatestServiceInfoComponent {
  //newer
  contractPeriods = [];
  paymentFrequencies = [];
  lssContributionDetails;
  partitionTypes = [];
  paymentMethods = [];
  paymentTypes = [];
  secondaryPartitions = [];
  selectedPartitionType = '';
  selectedServiceOption = {};
  secondaryPartition = null;
  @ViewChildren('input') rows: QueryList<any>;
  noOfPartitionsBtn = 'Capture';
  breadCrumb: BreadCrumbModel;
  selectedItemsWithKeyValue = {};
  @ViewChild('serviceModal', { static: false }) serviceModal: ElementRef;
  formConfigs = formConfigs;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  serviceInfoAddEditForm: FormGroup;
  partitionForm: FormGroup;
  serviceCategoryList = [];
  serviceItemList = [];
  serviceObj = {};
  componentProperties: ComponentProperties;
  observableResponse;
  serviceItemsLength = 0;
  queryParams;
  customerId: string
  addressId: string
  otherServicesTotalAmount = {
    servicePrice: 0,
    taxPrice: 0,
    totalPrice: 0
  };
  valueAddedServicesTotalAmount = {
    servicePrice: 0,
    taxPrice: 0,
    totalPrice: 0
  };
  VAServices = 'Value Added Services';
  VAS = 'VAS';
  customerData: DealerHeader
  objectKeys = Object.keys;
  loggedInUserData: LoggedInUserModel;
  isFormChangeDetected = false;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  dealerId: string
  availablePartitions = []
  dealerCategory = [];
  @ViewChild('ngForm', { static: false }) ngForm;
  categoryDropDown: any = [];
  originDropDown: any = [];
  installOriginDropDown: any = [];
  ownershipDropDown: any = [];
  serviceCategories: any
  debtorAccountDetailId = ""
  creditVettingScore = 0
  classificationList = []
  dealerCategoryTypeId = ""
  constructor(private crudService: CrudService,
    public rxjsService: RxjsService, private store: Store<AppState>, private dialog: MatDialog,
    private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService) {
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId
    this.debtorAccountDetailId = this.activatedRoute.snapshot.queryParams.debtorAccountDetailId?this.activatedRoute.snapshot.queryParams.debtorAccountDetailId:''
    // this.rxjsService.setQuickActionComponent(ModuleName.LEAD)
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(selectStaticEagerLoadingContractPeriodsState$), this.store.select(selectStaticEagerLoadingBillingIntervalsState$),
    this.store.select(selectStaticEagerLoadingPartitionTypesState$), this.store.select(selectStaticEagerLoadingPaymentMethodsState$),
    this.store.select(selectStaticEagerLoadingPaymentTypesState$),
    this.store.select(DealerCustomerHeaderState$)])
      .pipe(take(1))
      .subscribe((response) => {
        // this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.contractPeriods = response[1];
        this.paymentFrequencies = response[2];
        this.partitionTypes = response[3];
        this.paymentMethods = response[4];
        this.paymentTypes = response[5];
        this.customerData = new DealerHeader(response[6]);
      });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();

    this.createDealerClassificationForm()
    this.getDealerClassificationListDetails()
    this.createServiceInfoForm();
    this.getDealerCategoryDetails()
    this.onFormControlChanges();
    this.getCategoryDropDown();
    this.getOriginDropDown();
    this.getInstallDropDown();
    this.getOwnershipDropDown();

    this.dealerId = this.customerData.dealerId
    this.serviceInfoAddEditForm.get('dealerCategoryTypeId').setValue(this.customerData.dealerCategoryTypeId);
    this.serviceInfoAddEditForm.get('dealerId').setValue(this.customerData.dealerId);
    this.serviceInfoAddEditForm.get('customerId').setValue(this.customerId);
    this.serviceInfoAddEditForm.get('addressId').setValue(this.addressId);
    this.dealerCategory = [{ id: this.customerData.dealerCategoryTypeId, displayName: this.customerData.dealerCategoryTypeName }]
    this.serviceInfoAddEditForm.get('dealerCategoryTypeId').disable()
    this.getServicesByCustomerId();

  }

  getAvailablePartitions() {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.UX_SERVICE_PARTITIONS, null, false,
      prepareRequiredHttpParams({
        customerId: this.customerId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.availablePartitions =  getPDropdownData(response.resources,"partitionName","partitionId")

          this.serviceCategories?.forEach((respObj) => {
            this.serviceObj[respObj['serviceCategoryName']]['values'].forEach((serviceSubObj) => {
              if (serviceSubObj['partitions'] && serviceSubObj['partitions'].length > 0) {
                serviceSubObj['selectedPartitionIds'] = serviceSubObj['partitions']?.map(pO => pO['partitionId']);
              }
              else {
                serviceSubObj['selectedPartitionIds'] = [];
              }
            });
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onPartitionSelectionChanged(serviceSubItem, selectedPartitionIds) {
    console.log(serviceSubItem, selectedPartitionIds)
    if (selectedPartitionIds?.value) {
      serviceSubItem.selectedPartitionIds = selectedPartitionIds?.value;
    }
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  onFormControlChanges(): void {
    this.serviceInfoAddEditForm.get('isPartitionedSystem').valueChanges.subscribe((isPartitionedSystem: boolean) => {
      if (isPartitionedSystem) {
        this.serviceInfoAddEditForm = addFormControls(this.serviceInfoAddEditForm, [{
          controlName: "partitionTypeId",
          defaultValue: this.serviceInfoAddEditForm.get('partitionTypeId') ? this.serviceInfoAddEditForm.get('partitionTypeId').value : null
        },
        { controlName: "paymentTypeId", defaultValue: this.serviceInfoAddEditForm.get('paymentTypeId') ? this.serviceInfoAddEditForm.get('paymentTypeId').value : null }]);
        this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, ["partitionTypeId", "paymentTypeId"]);
        this.serviceInfoAddEditForm.get('partitionTypeId').valueChanges.subscribe((partitionTypeId: string) => {
          if (!partitionTypeId) {
            this.selectedPartitionType = '';
            return;
          };
          this.selectedPartitionType = this.partitionTypes.find(p => p['id'] == +partitionTypeId).displayName;
          if (this.selectedPartitionType == 'Primary') {
            if (!this.serviceInfoAddEditForm.get('noOfPartitions').value) {
              this.serviceInfoAddEditForm.get('noOfPartitions').enable();
            }
            this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, ["noOfPartitions"]);
            this.serviceInfoAddEditForm.get('noOfPartitions').valueChanges.subscribe((noOfPartitions: string) => {
              if (noOfPartitions == '1' || +noOfPartitions > 15) {
                this.serviceInfoAddEditForm.get('noOfPartitions').setErrors({ invalid: true });
              }
              else {
                this.serviceInfoAddEditForm = removeFormControlError(this.serviceInfoAddEditForm, 'invalid');
              }
            });
            this.serviceInfoAddEditForm = removeFormControls(this.serviceInfoAddEditForm, ['secondaryPartitionCode']);
            this.serviceInfoAddEditForm = addFormControls(this.serviceInfoAddEditForm,
              [{ controlName: "paymentTypeId", defaultValue: null }]);
            this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, ["paymentTypeId"]);
          }
          else {
            this.serviceInfoAddEditForm = addFormControls(this.serviceInfoAddEditForm,
              ["secondaryPartitionCode"]);
            this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, ["secondaryPartitionCode"]);
            this.serviceInfoAddEditForm = removeFormControls(this.serviceInfoAddEditForm, ['paymentTypeId']);
          }
        });
      }
      else {
        this.serviceInfoAddEditForm = removeFormControls(this.serviceInfoAddEditForm, ["partitionTypeId", "paymentTypeId"]);
        this.serviceInfoAddEditForm.get('noOfPartitions').disable({ emitEvent: false, onlySelf: true });
        this.serviceInfoAddEditForm.get('noOfPartitions').setValue(null);
      }
    });
  }

  onOpenModal() {
    this.serviceInfoAddEditForm.value.addressId = this.addressId;
    this.serviceInfoAddEditForm.value.customerId = this.customerId;
    this.serviceInfoAddEditForm.value.createdUserId = this.loggedInUserData.userId;
    const dialogRef = this.dialog.open(DealerPartitionPopupComponent, {
      width: "700px",
      minHeight: '300px',
      data: { ...this.serviceInfoAddEditForm.getRawValue(), ...this.serviceInfoAddEditForm.value, ...this.customerData, userId: this.loggedInUserData.userId },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((dialogResult) => {
      this.getAvailablePartitions()
      if (dialogResult) return;
      this.noOfPartitionsBtn = 'Edit';
      this.rxjsService.getPropertyValue().subscribe((noOfPartitions) => {
        this.serviceInfoAddEditForm.get('noOfPartitions').setValue(noOfPartitions);
      });
    });
  }

  getSecondaryPartitionsBySearchKeyword(searchKeyword): void {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_PARTITIONS_SEARCH, null, false,
      prepareRequiredHttpParams({
        partitionCode: searchKeyword, customerId: this.customerId, addressId: this.addressId
      }), 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.secondaryPartition = response.resources;
          this.serviceInfoAddEditForm.get('secondaryPartitionCode').setErrors({ invalid: false });
          this.serviceInfoAddEditForm.get('secondaryPartitionCode').updateValueAndValidity();
          this.serviceInfoAddEditForm.updateValueAndValidity();
          this.focusInAndOutFormArrayFields();
        }
        else {
          this.snackbarService.openSnackbar("No Partition Code found", ResponseMessageTypes.WARNING)
          this.secondaryPartition = {};
          this.serviceInfoAddEditForm.get('secondaryPartitionCode').setValue(null)
          this.serviceInfoAddEditForm.get('secondaryPartitionCode').setErrors({ invalid: true });
          this.focusInAndOutFormArrayFields();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSecondarySearch() {
    if (!this.serviceInfoAddEditForm.get('secondaryPartitionCode').value) {
      return;
    }
    this.getSecondaryPartitionsBySearchKeyword(this.serviceInfoAddEditForm.get('secondaryPartitionCode').value);
  }

  onAddServices(): void {
    // if (this.serviceInfoAddEditForm.invalid) return;
    let data = { ...this.serviceInfoAddEditForm.value, ...this.customerData };
    data['serviceObj'] = this.serviceObj;
    data['customerId'] = this.customerId;
    data['addressId'] = this.addressId;
    data['createdUserId'] = this.loggedInUserData.userId;
    const dialogRef = this.dialog.open(DealerServicePopupComponent, {
      width: "1000px",
      data,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      this.getAvailablePartitions()
      if (typeof dialogResult === 'boolean' && dialogResult) {
        this.isFormChangeDetected = false;
        return;
      }

      if (dialogResult.fromComponent && dialogResult.fromComponent == 'services') {
        this.calculateTotalAmountSummary();
      }
      else if (dialogResult.fromComponent && dialogResult.fromComponent == 'lssContribution') {
        this.getServicesByCustomerId('fromPopup');
      }
    });
    var self = this;
    dialogRef.componentInstance.outputData.subscribe(ele => {
      self.serviceObj = ele.serviceObj;
      this.isFormChangeDetected = true;
    });
  }

  getServicesByCustomerId(type?: string): void {
    if (!this.addressId || !this.customerId || !this.dealerId) return;
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_SERVICE_DETAILS, null,
      false, prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.addressId, dealerId: this.dealerId }), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          let serviceInfoFormModel: DealerServiceInfoFormModel;
          let leadCreationUserDataModel = { ...this.leadCreationUserDataModel };
          leadCreationUserDataModel.servicesCount = response.resources.services ? response.resources.services.length : 0;
          if (type == 'fromPopup') {
            serviceInfoFormModel = new DealerServiceInfoFormModel({ ...this.serviceInfoAddEditForm.value, ...this.serviceInfoAddEditForm.getRawValue() });
          }
          else {
            serviceInfoFormModel = new DealerServiceInfoFormModel(response.resources);
          }

          // (this.serviceInfoAddEditForm.get("dealerClassificationForm") as FormGroup).get("categoryId").setValue(response?.resources?.dealerCategoryTypeId);

          if (serviceInfoFormModel.primaryPartition) {
            this.secondaryPartition = {
              customerName: serviceInfoFormModel.primaryPartition.customerName,
              partitionCode: serviceInfoFormModel.primaryPartition.partitionCode,
              address: serviceInfoFormModel.primaryPartition.address,
              partitionCustomerId: this.customerId
            }
            this.serviceInfoAddEditForm.patchValue(serviceInfoFormModel);
            this.serviceInfoAddEditForm.get('secondaryPartitionCode').setValue(serviceInfoFormModel.secondaryPartitionCode, { emitEvent: false, onlySelf: true });
          }
          else {
            this.serviceInfoAddEditForm.patchValue(serviceInfoFormModel);
            if (serviceInfoFormModel.paymentTypeId && this.serviceInfoAddEditForm.get('paymentTypeId')) {
              this.serviceInfoAddEditForm.get('paymentTypeId').setValue(serviceInfoFormModel.paymentTypeId);
            }
          }
          if (serviceInfoFormModel.noOfPartitions) {
            this.noOfPartitionsBtn = 'Edit';
            this.serviceInfoAddEditForm.get('noOfPartitions').disable();
          }
          this.serviceInfoAddEditForm.get('isPartitionedSystem').setValue(serviceInfoFormModel.isPartitionedSystem);

          if (response.resources.services) {
            this.serviceCategories = response.resources.services
            response.resources.services.forEach((respObj) => {
              this.serviceObj[respObj['serviceCategoryName']] = {};
              this.serviceObj[respObj['serviceCategoryName']]['values'] = respObj['services'];
              this.serviceObj[respObj['serviceCategoryName']]['serviceCategoryId'] = respObj['serviceCategoryId'];
            });

            this.calculateTotalAmountSummary();
            this.getAvailablePartitions()
          }

          if (response.resources.lssContributionDetails) {
            this.serviceObj['LSS Scheme Name'] = {};
            this.serviceObj['LSS Scheme Name']['values'] = [];
            this.serviceObj['LSS Scheme Name']['totalAmount'] = {
              servicePrice: response.resources.lssContributionDetails.unitPrice,
              taxPrice: response.resources.lssContributionDetails.unitTaxPrice,
              totalPrice: response.resources.lssContributionDetails.totalPrice
            };
            let lssContributionObj = {
              taxPrice: response.resources.lssContributionDetails.unitTaxPrice,
              serviceName: response.resources.lssContributionDetails.lssName,
              serviceCode: response.resources.lssContributionDetails.lssRefNo,
              servicePrice: response.resources.lssContributionDetails.unitPrice
            }
            this.serviceObj['LSS Scheme Name']['values'].push({ ...response.resources.lssContributionDetails, ...lssContributionObj });
            this.calculateTotalAmountSummary();
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  dealerClassificationForm: FormGroup;
  createDealerClassificationForm() {

    this.dealerClassificationForm = this.formBuilder.group({
      dealerClassificationMappingId: [''],
      categoryId: ['', [Validators.required]],
      itemOwnershipTypeId: ['', []],
      originId: ['', []],
      installOriginId: ['', []],
      dealType: ['', []],
      dealerId: [this.dealerId, [Validators.required]],
      customerId: [this.customerId, [Validators.required]],
      addressId: [this.addressId, [Validators.required]],
      modifiedUserId: [this.loggedInUserData?.userId, [Validators.required]],
      modifiedDate: [new Date().toDateString(), [Validators.required]],
    })
  }

  getDealerCategoryDetails() {
    let obj = {
      dealerId: this.customerData.dealerId,
      addressId: this.addressId,
      customerId: this.customerId,
      debtorAccountDetailId: this.debtorAccountDetailId
    }
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CUSTOMER_CLASSIFICATION_DEALER_DETAILS, undefined, false, prepareRequiredHttpParams(obj)).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {

        (this.serviceInfoAddEditForm.get("dealerClassificationForm") as FormGroup).patchValue(response.resources);
        this.onChangeCategory({ target: { value: response?.resources?.categoryId } });
        this.creditVettingScore = response.resources?.creditVettingScore ? response.resources?.creditVettingScore : '0'
      }

    })
  }
  createServiceInfoForm(): void {
    let serviceInfoFormModel = new DealerServiceInfoFormModel();
    this.serviceInfoAddEditForm = this.formBuilder.group({});
    Object.keys(serviceInfoFormModel).forEach((key) => {
      this.serviceInfoAddEditForm.addControl(
        key,
        new FormControl(serviceInfoFormModel[key])
      );
    });
    this.serviceInfoAddEditForm.addControl('dealerClassificationForm', this.dealerClassificationForm);

    // this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, [
    //   "contractPeriodId", "billingIntervalId", "paymentMethodId"
    // ]);
    // this.serviceInfoAddEditForm.get('contractPeriodId').setValue(this.contractPeriods.find(c => c.isDefault).id)
    // this.serviceInfoAddEditForm.get('billingIntervalId').setValue(this.paymentFrequencies.find(p => p.isDefault).id)
    // this.serviceInfoAddEditForm.get('paymentMethodId').setValue(this.paymentMethods.find(p => p.isDefault).id)
  }
  saveDealerCategoryClassification() {
    if (this.dealerClassificationForm.invalid) return "";
    let API
    if (this.dealerClassificationForm.value.dealerClassificationMappingId) {
      let reqObj = {
        dealerClassificationMappingId: this.dealerClassificationForm.value.dealerClassificationMappingId,
        categoryId: this.dealerClassificationForm.value.categoryId,
        dealerId: this.customerData.dealerId,
        customerId: this.dealerClassificationForm.value.customerId,
        addressId: this.dealerClassificationForm.value.addressId,
        modifiedUserId: this.dealerClassificationForm.value.modifiedUserId,
        modifiedDate: new Date().toDateString()
      }
      API = this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CUSTOMER_CLASSIFICATION_DEALER, reqObj)
    } else {
      let reqObj = {
        categoryId: this.dealerClassificationForm.value.categoryId,
        customerId: this.customerId,
        addressId: this.addressId,
        dealerId: this.customerData.dealerId,
        createdUserId: this.loggedInUserData?.userId,
        dealerClassificationMappingId:null
      }
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CUSTOMER_CLASSIFICATION_DEALER, reqObj)
    }
    API.subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goNext();
      }
    })
  }


  createPartitionForm(): void {
    let partitionCreationModel = new PartitionCreationModel();
    this.partitionForm = this.formBuilder.group({});
    Object.keys(partitionCreationModel).forEach((key) => {
      this.partitionForm.addControl(
        key,
        new FormControl(partitionCreationModel[key])
      );
    });
  }

  removeItem(serviceItemKey: string, serviceItemObj: object) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      let areAllServicesRemoved = false;
      if (serviceItemObj['dealerServiceId']) {
        this.crudService.delete(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_SERVICES, undefined,
          prepareRequiredHttpParams({
            dealerServiceIds: serviceItemObj['dealerServiceId'],
            isDeleted: true,
            createdUserId: this.loggedInUserData.userId
          }), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              Object.keys(this.serviceObj).forEach((key: string) => {
                if (key === serviceItemKey) {
                  for (const [ix, val] of this.serviceObj[key]['values'].entries()) {
                    if (val['dealerServiceId'] === serviceItemObj['dealerServiceId']) {
                      this.serviceObj[key]['values'].splice(ix, 1);
                      if (this.serviceObj[key]['values'].length === 0) {
                        delete this.serviceObj[key];
                        areAllServicesRemoved = true;
                      }
                      else {
                        areAllServicesRemoved = false;
                      }
                      this.calculateTotalAmountSummary();
                    }
                  }
                }
              });
              let leadCreationUserDataModel = { ...this.leadCreationUserDataModel };
              leadCreationUserDataModel.servicesCount = areAllServicesRemoved ? 0 : 1;
              this.selectedItemsWithKeyValue = { ...this.serviceObj };
            }
          })
      }
      else if (serviceItemObj['lssId']) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LSS_CONTRIBUTION, undefined,
          prepareRequiredHttpParams({
            ids: serviceItemObj['lssId'],
            customerId: this.customerId,
            isDeleted: true,
            modifiedUserId: this.loggedInUserData.userId
          }), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              delete this.serviceObj['LSS Scheme Name'];
              this.calculateTotalAmountSummary();
            }
          })
      }
      else {
        Object.keys(this.serviceObj).forEach((key: string) => {
          if (key === serviceItemKey) {
            for (const [ix, val] of this.serviceObj[key]['values'].entries()) {
              if (val['servicePriceId'] === serviceItemObj['servicePriceId']) {
                this.serviceObj[key]['values'].splice(ix, 1);
                if (this.serviceObj[key]['values'].length === 0) {
                  delete this.serviceObj[key];
                  areAllServicesRemoved = true;
                }
                else {
                  areAllServicesRemoved = false;
                }
                this.calculateTotalAmountSummary();
              }
            }
          }
        });
        let leadCreationUserDataModel = { ...this.leadCreationUserDataModel };
        leadCreationUserDataModel.servicesCount = areAllServicesRemoved ? 0 : 1;
        this.selectedItemsWithKeyValue = { ...this.serviceObj };
      }
      this.selectedItemsWithKeyValue = JSON.parse(JSON.stringify(this.serviceObj));
    });
  }

  calculateTotalAmountSummary(): void {
    this.valueAddedServicesTotalAmount = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };
    this.otherServicesTotalAmount = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };

    Object.keys(this.serviceObj).forEach((key) => {
      this.serviceObj[key]["totalAmount"] = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };
      this.serviceObj[key]['isQtyAvailable'] = this.serviceObj[key]['values'].find(s => s['isQtyAvailable'] == true) ? true : false;
      if (this.serviceObj[key]['isQtyAvailable']) {
        this.serviceObj[key]['values'].forEach((obj: ServicesModel) => {
          this.valueAddedServicesTotalAmount.servicePrice = this.valueAddedServicesTotalAmount.servicePrice + (obj['servicePrice']);
          this.valueAddedServicesTotalAmount.taxPrice = this.valueAddedServicesTotalAmount.taxPrice + (obj['taxPrice']);
          this.valueAddedServicesTotalAmount.totalPrice = this.valueAddedServicesTotalAmount.totalPrice +
            (obj['servicePrice'] + obj['taxPrice']);
          this.serviceObj[key]["totalAmount"]['servicePrice'] = this.serviceObj[key]["totalAmount"]['servicePrice'] + (obj['servicePrice']);
          this.serviceObj[key]["totalAmount"]['taxPrice'] = this.serviceObj[key]["totalAmount"]['taxPrice'] + (obj['taxPrice']);
          this.serviceObj[key]["totalAmount"]['totalPrice'] = this.serviceObj[key]["totalAmount"]['totalPrice'] + (obj['servicePrice'] + obj['taxPrice']);
        });
      }
      else {
        this.serviceObj[key]['values'].forEach((obj: ServicesModel) => {
          this.otherServicesTotalAmount.servicePrice = this.otherServicesTotalAmount.servicePrice + obj['unitPrice'];
          this.otherServicesTotalAmount.taxPrice = this.otherServicesTotalAmount.taxPrice + obj['unitTaxPrice'];
          this.otherServicesTotalAmount.totalPrice = this.otherServicesTotalAmount.totalPrice + (obj['unitPrice'] + obj['unitTaxPrice']);
          this.serviceObj[key]["totalAmount"]['servicePrice'] = this.serviceObj[key]["totalAmount"]['servicePrice'] + (obj['unitPrice']);
          this.serviceObj[key]["totalAmount"]['taxPrice'] = this.serviceObj[key]["totalAmount"]['taxPrice'] + (obj['unitTaxPrice']);
          this.serviceObj[key]["totalAmount"]['totalPrice'] = this.serviceObj[key]["totalAmount"]['totalPrice'] + (obj['unitPrice'] + obj['unitTaxPrice']);
        });
      }
    });
    this.serviceItemsLength = this.objectKeys(this.serviceObj).length;
    $('#services_modal').modal('hide');
  }

  onQuantityChange(type, serviceSubItem: object): void {
    if (type === 'plus') {
      serviceSubItem['qty'] = serviceSubItem['qty'] + 1;
    }
    else {
      serviceSubItem['qty'] = (serviceSubItem['qty'] === 1) ? serviceSubItem['qty'] : serviceSubItem['qty'] - 1;
    }
    serviceSubItem['servicePrice'] = (serviceSubItem['qty'] * serviceSubItem['unitPrice']);
    serviceSubItem['taxPrice'] = (serviceSubItem['qty'] * serviceSubItem['unitTaxPrice']);
    serviceSubItem['totalPrice'] = (serviceSubItem['servicePrice'] + serviceSubItem['taxPrice']);
    this.calculateTotalAmountSummary();
    this.isFormChangeDetected = (type == 'minus' && serviceSubItem['qty'] === 1) ? false : true;
  }

  onSubmit(): void {

    if (this.serviceObj.hasOwnProperty('LSS Scheme Name') && this.objectKeys(this.serviceObj).length == 1 ||
      !this.serviceObj.hasOwnProperty('LSS Scheme Name') && this.objectKeys(this.serviceObj).length == 0) {
      this.snackbarService.openSnackbar("Atleast One Service Subscription is required..!!", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.serviceInfoAddEditForm.invalid) {
      return;
    }


    let reqObject = this.serviceInfoAddEditForm.getRawValue()
    this.rxjsService.setFormChangeDetectionProperty(true);
    reqObject.services = [];
    Object.keys(this.serviceObj).forEach((key) => {
      if (this.serviceObj.hasOwnProperty(key) && key !== 'LSS Scheme Name') {    // change if the key is changed
        this.serviceObj[key]['values'].forEach((service) => {
          reqObject.services.push(service);
        });
      }
      let serviceInfo = [...this.serviceObj[key]['values'].entries()]
      for (const [ix, val] of serviceInfo ) {
        val['partitions'] = [];
        if (val['selectedPartitionIds'] && val['selectedPartitionIds'].length > 0) {
          val['selectedPartitionIds'].forEach((partitionId) => {
            val['partitions'].push({
              dealerServicePartitionId: (val['servicePartitions']?.find(sPC => sPC['partitionId'] == partitionId)?.leadServicePartitionId ?
                val['servicePartitions'].find(sPC => sPC['partitionId'] == partitionId).dealerServicePartitionId : null),
              partitionId
            });
          });
        }
        delete val['servicePartitions'];
        delete val['selectedPartitionIds'];
      }
    });

    reqObject.dealerId = this.dealerId;
    reqObject.addressId = this.addressId;
    reqObject.dealerCategoryTypeId = this.dealerCategoryTypeId;
    reqObject.customerId = this.customerId;
    reqObject.createdUserId = this.loggedInUserData.userId;
    if (this.secondaryPartition) {
      this.secondaryPartition.partitionCustomerId = this.customerId;
      reqObject.secondaryPartition = this.secondaryPartition;
      this.secondaryPartition.partitionTypeId = this.serviceInfoAddEditForm.value.partitionTypeId;
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_SERVICES, reqObject, 1);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.isFormChangeDetected = false;
        this.getServicesByCustomerId();
        this.saveDealerCategoryClassification()
        // this.goNext();
      }
    })
  }

  getCategoryDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEALER_CLASSIFICATION_MAPPINGS_DEALER_CATEGORY, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.categoryDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOriginDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEALER_CLASSIFICATION_MAPPINGS_ORIGIN, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.originDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getInstallDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEALER_CLASSIFICATION_MAPPINGS_INSTALL_ORIGIN, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.installOriginDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOwnershipDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_OWNER_SHIP_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.ownershipDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getDealerClassificationListDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEALER_CLASSIFICATION_MAPPINGS).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.classificationList = response.resources;
      }
    })
  }

  onChangeCategory(event) {
    let catId = event.target?.value
    this.dealerCategoryTypeId = catId
    let filter = this.classificationList.find(item => item.categoryId == catId);
    if (filter) {
      (this.serviceInfoAddEditForm.get("dealerClassificationForm") as FormGroup).patchValue(filter);

    }
  }


  goNext() {
    this.router.navigate(['/dealer/dealer-contract/dealer-agreement-summary'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
  }

  goBack() {
    this.router.navigate(['/dealer/dealer-contract/debtor-banking-creation'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
  }

  ngOnDestroy() {
    this.rxjsService.setFormChangesDetectionPropertyForPageReload(this.isFormChangeDetected);
  }

}
