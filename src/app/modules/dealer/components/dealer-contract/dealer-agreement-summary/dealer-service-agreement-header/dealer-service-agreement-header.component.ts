import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { RxjsService } from '@app/shared';
import { DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
@Component({
  selector: 'app-dealer-service-agreement-header',
  templateUrl: './dealer-service-agreement-header.component.html',
  styleUrls: ['./dealer-service-agreement-header.component.scss']
})
export class DealerServiceAgreementHeaderComponent implements OnInit {
  @Input() pageHeading: string;
  @Input() currentStep: number;
  totalSteps: number;
  stepperArray = [];
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;

  constructor(private router: Router, private rxjsService: RxjsService,
    private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(dealerCustomerServiceAgreementDataState$)]
    ).subscribe((response) => {
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[0]);
      this.totalSteps = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.length;
      for (let index = 1; index <= this.totalSteps; index++) {
        let isActive = index <= this.currentStep;
        this.stepperArray.push({
          index: index,
          active: isActive,
          navigationUrl: this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy[index - 1].navigationUrl
        })
      }
    });
  }

  navigateToCustomer() {
    this.rxjsService.setViewCustomerData({
      customerId: this.dealerCustomerServiceAgreementParamsDataModel?.customerId,
      addressId: this.dealerCustomerServiceAgreementParamsDataModel?.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  navigateSteps(index) {
    this.router.navigateByUrl(this.stepperArray[index].navigationUrl);
  }
}
