import { DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { DealerCustomerServiceAgreementDataActions, DealerCustomerServiceAgreementDataActionTypes } from './dealer-customer-creation-service-agreement.actions';

export function dealerCustomerServiceAgreementDataReducer(state = new DealerCustomerServiceAgreementParamsDataModel(),
  action: DealerCustomerServiceAgreementDataActions): DealerCustomerServiceAgreementParamsDataModel {
  switch (action.type) {
    case DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataCreateAction:
      return action.payload.dealerCustomerServiceAgreementDataModel;

      case DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataChangeAction:
        return {...state,...action.payload.dealerCustomerServiceAgreementDataModel};

    case DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataRemoveAction:
      return null;

    default:
      return state;
  }
}

