import { DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { Action } from '@ngrx/store';

enum DealerCustomerServiceAgreementDataActionTypes {
  DealerCustomerServiceAgreementDataCreateAction = '[Dealer Customer Creation Service Agreement]  Create Action',
  DealerCustomerServiceAgreementDataChangeAction = '[Dealer Customer Creation Service Agreement] Change Action',
  DealerCustomerServiceAgreementDataRemoveAction = '[[Dealer Customer Creation Service Agreement]  Remove Action',
}
class DealerCustomerServiceAgreementDataCreateAction implements Action {
  readonly type = DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataCreateAction;

  constructor(public payload: { dealerCustomerServiceAgreementDataModel: DealerCustomerServiceAgreementParamsDataModel }) {
  }

}
class DealerCustomerServiceAgreementDataChangeAction implements Action {
  readonly type = DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataChangeAction;
  constructor(public payload: { dealerCustomerServiceAgreementDataModel: DealerCustomerServiceAgreementParamsDataModel }) {
  }

}
class DealerCustomerServiceAgreementDataRemoveAction implements Action {

  readonly type = DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataRemoveAction;

}

type DealerCustomerServiceAgreementDataActions = DealerCustomerServiceAgreementDataCreateAction | DealerCustomerServiceAgreementDataChangeAction | DealerCustomerServiceAgreementDataRemoveAction;


export {
  DealerCustomerServiceAgreementDataCreateAction, DealerCustomerServiceAgreementDataChangeAction,
  DealerCustomerServiceAgreementDataRemoveAction, DealerCustomerServiceAgreementDataActions, DealerCustomerServiceAgreementDataActionTypes
};

