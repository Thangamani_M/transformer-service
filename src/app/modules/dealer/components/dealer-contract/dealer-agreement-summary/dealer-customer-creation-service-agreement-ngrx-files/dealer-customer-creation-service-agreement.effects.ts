import { Injectable } from '@angular/core';
import { AppDataService } from '@app/shared/services';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { defer, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DealerCustomerServiceAgreementDataActionTypes, DealerCustomerServiceAgreementDataChangeAction, DealerCustomerServiceAgreementDataCreateAction, DealerCustomerServiceAgreementDataRemoveAction } from './dealer-customer-creation-service-agreement.actions';

@Injectable()
export class DealerCustomerServiceAgreementEffects {
  @Effect({ dispatch: false })
  createData$ = this.actions$.pipe(
    ofType<DealerCustomerServiceAgreementDataCreateAction>(DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataCreateAction),
    tap(action =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = action.payload.dealerCustomerServiceAgreementDataModel)
  );

  @Effect({ dispatch: false })
  updateData$ = this.actions$.pipe(
    ofType<DealerCustomerServiceAgreementDataChangeAction>(DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataChangeAction),
    tap(action =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = action.payload.dealerCustomerServiceAgreementDataModel)
  );

  @Effect({ dispatch: false })
  removeData$ = this.actions$.pipe(
    ofType<DealerCustomerServiceAgreementDataRemoveAction>(DealerCustomerServiceAgreementDataActionTypes.DealerCustomerServiceAgreementDataRemoveAction),
    tap(() =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = null
    )
  );

  @Effect()
  initUserData$ = defer(() => {
    const dealerCustomerServiceAgreementDataModel = this.appDataService.dealerCustomerCreationServiceAgreementData;
    if (dealerCustomerServiceAgreementDataModel) {
      return of(new DealerCustomerServiceAgreementDataCreateAction({ dealerCustomerServiceAgreementDataModel }));
    }
    else {
      return <any>of(new DealerCustomerServiceAgreementDataRemoveAction());
    }
  });

  constructor(private actions$: Actions, private appDataService: AppDataService) {

  }

}
