import { DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { createFeatureSelector, createSelector } from '@ngrx/store';

const dealerCustomerServiceAgreementDataState = createFeatureSelector<DealerCustomerServiceAgreementParamsDataModel>("dealerCustomerServiceAgreementData");

const dealerCustomerServiceAgreementDataState$ = createSelector(
  dealerCustomerServiceAgreementDataState,
  dealerCustomerServiceAgreementDataState => dealerCustomerServiceAgreementDataState
);

export {
  dealerCustomerServiceAgreementDataState$
};

