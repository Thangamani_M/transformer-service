import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogModel,
  ConfirmDialogPopupComponent,
  countryCodes, CrudService, CustomDirectiveConfig, formConfigs,
  HttpCancelService, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  onBlurActionOnDuplicatedFormControl,
  onFormControlChangesToFindDuplicate,
  prepareGetRequestHttpParams, prepareRequiredHttpParams,
  RxjsService, SnackbarService
} from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerCellPanicAddEditModel, DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
@Component({
  selector: 'app-dealer-cell-panic',
  templateUrl: './dealer-cell-panic.component.html',
  styleUrls: ['./dealer-service-installation-agreement.scss']
})

export class DealerCellPanicComponent implements OnInit {
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  formConfigs = formConfigs;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  numberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  cellPanicAddEditForm: FormGroup;
  countryCodes = countryCodes;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  cellPanicId: any;
  cellPanicArray: FormArray;
  isUpdate = false;
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChildren('contactRows',) contactRows: QueryList<ElementRef>;
  pageHeading = 'Cell Panic';
  currentStep;

  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private formBuilder: FormBuilder, private dialog: MatDialog,
    public rxjsService: RxjsService, private store: Store<AppState>, private router: Router) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(dealerCustomerServiceAgreementDataState$)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[1]);
      this.currentAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['agreementTabName'].toLowerCase().includes('cell panic'));
      this.currentStep = this.currentAgreementTabObj.indexNo;
    });
  }

  ngOnInit(): void {
    this.createCellPanicForm();
    this.cellPanicArray = this.getCellPanic;
    this.cellPanicArray.push(this.createPanicArray());
    this.getCellPanicById().subscribe((response: IApplicationResponse) => {
      if (response.resources && response.isSuccess && response.statusCode == 200) {
        if (response.resources.length > 0) {
          this.focusAllFields();
        }
        let cellPanic = new DealerCellPanicAddEditModel(response.resources);
        this.cellPanicAddEditForm.patchValue(cellPanic);
        this.cellPanicArray = this.getCellPanic;
        if (response.resources.length) {
          this.cellPanicArray.removeAt(this.cellPanicArray.length - 1);
        }
        response.resources.forEach((cellPanicAddEditModel: DealerCellPanicAddEditModel) => {
          this.cellPanicArray.push(this.createPanicArray(cellPanicAddEditModel));
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  focusAllFields() {
    setTimeout(() => {
      this.focusInAndOutFormArrayFields();
    }, 100);
  }

  createCellPanicForm(): void {
    this.cellPanicAddEditForm = this.formBuilder.group({
      cellPanicArray: this.formBuilder.array([]),
    });
  }

  get getCellPanic(): FormArray {
    if (this.cellPanicAddEditForm) {
      return this.cellPanicAddEditForm.get("cellPanicArray") as FormArray;
    }
  }

  createPanicArray(usefulNumberContactListModel?: DealerCellPanicAddEditModel): FormGroup {
    let usefulNumbersContactListFormControlModel = new DealerCellPanicAddEditModel(usefulNumberContactListModel);
    let formControls = {};
    Object.keys(usefulNumbersContactListFormControlModel).forEach((key) => {
      if (key === 'userName' || key === 'mobileNo') {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.cellPanicId) {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(itemType = 'cellPanicArray'): void {
    this.contactRows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addCellPanic(itemType = 'cellPanicArray') {
    if (this.getCellPanic.invalid) {
      this.focusInAndOutFormArrayFields(itemType);
      return;
    };
    if (this.cellPanicAddEditForm.invalid) return;
    this.cellPanicArray = this.getCellPanic;
    let salesChannel = new DealerCellPanicAddEditModel();
    this.cellPanicArray.insert(0, this.createPanicArray(salesChannel));
  }

  removeCellPanic(i: number): void {
    if (!this.cellPanicArray.controls[i].value.cellPanicId) {
      this.cellPanicArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.cellPanicArray.controls[i].value.cellPanicId && this.cellPanicArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CELL_PANIC, undefined,
          prepareRequiredHttpParams({
            id: this.cellPanicArray.controls[i].value.cellPanicId,
            isDeleted: true,
            modifiedUserId: this.dealerCustomerServiceAgreementParamsDataModel.modifiedUserId
          }), 1)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.cellPanicArray.removeAt(i);
              this.rxjsService.setGlobalLoaderProperty(false);
            }
            if (this.cellPanicArray.length === 0) {
              this.addCellPanic();
            };
          });
      }
      else {
        this.cellPanicArray.removeAt(i);
      }
    });
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.cellPanicAddEditForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  getCellPanicById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.CELL_PANIC,
      undefined,
      null,
      prepareGetRequestHttpParams(null, null, {
        customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
        addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
        partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId,
        isExist: this.dealerCustomerServiceAgreementParamsDataModel.isExist,
      })
    );
  }

  onSubmit(): void {
    if (this.cellPanicAddEditForm.invalid) {
      return;
    }
    this.cellPanicAddEditForm.value.cellPanicArray.forEach(element => {
      element.createdUserId = this.dealerCustomerServiceAgreementParamsDataModel.createdUserId;
      element.modifiedUserId = this.loggedInUserData.userId;
      element.partitionId = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
      element.customerId = this.dealerCustomerServiceAgreementParamsDataModel.customerId;
      element.addressId = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
      element.referenceId = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
      if (element.mobileNo) {
        element.mobileNo = element.mobileNo.toString().replace(/\s/g, "");
      }
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.CELL_PANIC, this.cellPanicAddEditForm.value.cellPanicArray, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources) {
            response.resources.forEach((cellPanicAddEditModel: DealerCellPanicAddEditModel, index: number) => {
              this.cellPanicArray.controls[index].get('cellPanicId').setValue(cellPanicAddEditModel.cellPanicId);
            });
          }
        }
      });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToAgreementSummaryPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'next') {
        if (!this.currentAgreementTabObj.isMandatory || (this.cellPanicAddEditForm.value.cellPanicArray[0]?.cellPanicId &&
          this.currentAgreementTabObj.isMandatory)) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'agreement summary') {
        this.rxjsService.navigateToAgreementSummaryPage();
      }
    }
  }
}
