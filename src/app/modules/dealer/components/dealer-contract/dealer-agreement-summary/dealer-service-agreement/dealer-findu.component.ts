import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  loggedInUserData,
  SalesModuleApiSuffixModels, selectStaticEagerLoadingTitlesState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import {
  countryCodes, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator
} from '@app/shared/utils';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerAdditionalUsers, DealerCustomerServiceAgreementParamsDataModel, DealerFinduSubscriberModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { LeadHeaderData } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
@Component({
  selector: 'app-dealer-findu',
  templateUrl: './dealer-findu.component.html',
  styleUrls: ['./dealer-service-installation-agreement.scss']
})

export class DealerFinduComponent implements OnInit {
  responseLength = 0;
  finduForm: FormGroup;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  leadHeaderData: LeadHeaderData;
  additionalUsers: FormArray;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  maxDate = new Date();
  titles;
  currentAgreementTabObj;
  findUdetails;
  makeList;
  isPasswordShow = false;
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  pageHeading = 'Primary User';
  currentStep;
  loggedInUserData: LoggedInUserModel;

  constructor(private crudService: CrudService, private router: Router, private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    public rxjsService: RxjsService, private store: Store<AppState>
  ) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createFindUForm();
    this.getFindUDetailsByLeadId();
    this.getMakeList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(dealerCustomerServiceAgreementDataState$),
    ]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.titles = response[1];
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[2]);
      this.currentAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['agreementTabName'].toLowerCase().includes('find'));
      this.currentStep = this.currentAgreementTabObj.indexNo;
    });
  }

  isEmailExist = true;
  checkDuplication() {
    this.rxjsService.setNotificationProperty(false);
    this.crudService
      .get(
        ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        SalesModuleApiSuffixModels.FINDU_VALIDATION,
        undefined,
        false,
        prepareGetRequestHttpParams(null, null, {
          email: this.finduForm.value.email,
        }),
        1
      )
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.isEmailExist = response.isSuccess;
      });
  }

  getMakeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MAKE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.makeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createFindUForm(): void {
    let finduSubscriberModel = new DealerFinduSubscriberModel();
    this.finduForm = this.formBuilder.group({
      additionalUsers: this.formBuilder.array([]),
    });
    Object.keys(finduSubscriberModel).forEach((key) => {
      this.finduForm.addControl(key,
        new FormControl(finduSubscriberModel[key]));
    });
    this.finduForm = setRequiredValidator(this.finduForm, ["mobileNo", "email", "firstName", "lastName", "dob",
      "title", "password"]);
    this.finduForm.get("mobileNoCountryCode").setValue("+27");
  }

  get getAdditionalUsers(): FormArray {
    if (!this.finduForm) return;
    return this.finduForm.get("additionalUsers") as FormArray;
  }

  //Create FormArray controls
  createUserModel(userModel?: DealerAdditionalUsers): FormGroup {
    let userModelFormControlModel = new DealerAdditionalUsers(userModel);
    let formControls = {};
    Object.keys(userModelFormControlModel).forEach((key) => {
      if (key == 'finduUserId') {
        formControls[key] = [{ value: userModelFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: userModelFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  getFindUDetailsByLeadId(): void {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.FIND_U_USERS, undefined, false,
      prepareRequiredHttpParams({
        customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
        addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
        partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId
      })).subscribe((response: IApplicationResponse) => {
        this.findUdetails = response.resources;
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.findUdetails.mobileNoCountryCode = this.findUdetails.mobileNoCountryCode ? this.findUdetails.mobileNoCountryCode : '+27';
          this.findUdetails.additionalUsers = this.findUdetails.additionalUsers ? this.findUdetails.additionalUsers : []
          this.finduForm.patchValue(response.resources);
          this.additionalUsers = this.getAdditionalUsers;
          if (response.resources.finduUserId) {
            if (response.resources.additionalUsers.length > 0) {
              response.resources.additionalUsers.forEach((dg) => {
                this.additionalUsers.push(this.createUserModel(dg));
              });
            }
          } else {
            if (response.resources.noOfUserCount > 0) {
              for (var i = 0; i < response.resources.noOfUserCount; i++) {
                this.additionalUsers.insert(i, this.createUserModel());
              }
            }
          }
          this.finduForm.get('noOfUserCount').setValue(response.resources.noOfUserCount);
        }
        this.rxjsService.setGlobalLoaderProperty(false)
      });
  }

  isDuplicate: boolean = false;
  OnChange(index): boolean {
    if (this.additionalUsers.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.additionalUsers.value.slice();
      cloneArray.splice(index, 1);
      cloneArray.forEach((obj) => {
        if (typeof obj.mobileNo === "string") {
          obj.mobileNo = obj.mobileNo.split(" ").join("")
          obj.mobileNo = obj.mobileNo.replace(/"/g, "");
        }
      });
      this.additionalUsers.value.forEach((obj) => {
        if (typeof obj.mobileNo === "string") {
          obj.mobileNo = obj.mobileNo.split(" ").join("")
          obj.mobileNo = obj.mobileNo.replace(/"/g, "");
        }
      });
      var findIndex = cloneArray.some(x => x.mobileNo == this.additionalUsers.value[index].mobileNo);
      if (findIndex) {
        this.snackbarService.openSnackbar("Contact number already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }
    }
  }

  onSubmit() {
    if (this.finduForm.invalid) {
      return;
    }
    if (!this.isEmailExist) {
      this.snackbarService.openSnackbar("Email already exist", ResponseMessageTypes.ERROR);
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Contact number already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.finduForm.value.customerId = this.dealerCustomerServiceAgreementParamsDataModel.customerId;
    this.finduForm.value.referenceId = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
    this.finduForm.value.partitionId = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
    this.finduForm.value.addressId = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
    this.finduForm.value.createdUserId = this.dealerCustomerServiceAgreementParamsDataModel.createdUserId;
    this.finduForm.value.modifiedUserId = this.loggedInUserData.userId;
    const mobile = this.finduForm.get('mobileNo').value.toString().replace(/\s/g, '');
    this.finduForm.value.mobileNo = mobile;
    let additionalUsers = this.finduForm.value.additionalUsers;
    additionalUsers.forEach(element => {
      if (typeof element.mobileNo === "string") {
        element.mobileNo = element.mobileNo.split(" ").join("")
        element.mobileNo = element.mobileNo.replace(/\s/g, '').trim();
      }
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService = this.finduForm.value.finduUserId ? 
    this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.FIND_U_USERS, this.finduForm.value) :
      this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.FIND_U_USERS, this.finduForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        if (!this.finduForm.get('finduUserId').value) {
          this.finduForm.get('finduUserId').setValue(response.resources);
        }
      }
    });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToAgreementSummaryPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'next') {
        if ((this.finduForm.get('finduUserId').value &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'agreement summary') {
        this.rxjsService.navigateToAgreementSummaryPage();
      }
    }
  }
}
