import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  addFormControls, countryCodes, CrudService, CustomDirectiveConfig, defaultCountryCode, formConfigs,
  HttpCancelService, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, removeFormControls,
  RxjsService, setRequiredValidator
} from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerCustomerServiceAgreementParamsDataModel, DealerSiteHazardAddEditModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
@Component({
  selector: 'app-dealer-site-hazard',
  templateUrl: './dealer-site-hazard.component.html',
  styleUrls: ['./dealer-service-installation-agreement.scss']
})

export class DealerSiteHazardComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  siteHazardAddEditForm: FormGroup;
  countryCodes = countryCodes;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  pageHeading = 'Site Hazard & Special Risk';
  currentStep;

  constructor(private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder, private dialog: MatDialog,
    public rxjsService: RxjsService, private store: Store<AppState>, private router: Router) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(dealerCustomerServiceAgreementDataState$)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[1]);
      this.currentAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['agreementTabName'].toLowerCase().includes('site'));
      this.currentStep = this.currentAgreementTabObj.indexNo;
    });
  }

  ngOnInit(): void {
    this.createSiteHazardForm();
    this.onFormControlChanges();
    this.getSiteHazardById();
  }

  onFormControlChanges(): void {
    this.siteHazardAddEditForm.get('isPremiseVisible').valueChanges.subscribe((isPremiseVisible: boolean) => {
      if (isPremiseVisible) {
        this.siteHazardAddEditForm = removeFormControls(this.siteHazardAddEditForm, ["description"]);
      }
      else {
        this.siteHazardAddEditForm = addFormControls(this.siteHazardAddEditForm, [{ controlName: "description" }]);
        this.siteHazardAddEditForm = setRequiredValidator(this.siteHazardAddEditForm, ['description']);
      }
    });
    this.siteHazardAddEditForm.get('isSwimmingPool').valueChanges.subscribe((isSwimmingPool: boolean) => {
      if (isSwimmingPool) {
        this.siteHazardAddEditForm = addFormControls(this.siteHazardAddEditForm, [{ controlName: "swimmingPoolLocation" }]);
        this.siteHazardAddEditForm = setRequiredValidator(this.siteHazardAddEditForm, ['swimmingPoolLocation']);
      }
      else {
        this.siteHazardAddEditForm = removeFormControls(this.siteHazardAddEditForm, ["swimmingPoolLocation"]);
      }
    });
    this.siteHazardAddEditForm.get('isSecurityOfficers').valueChanges.subscribe((isSecurityOfficers: boolean) => {
      if (isSecurityOfficers) {
        this.siteHazardAddEditForm = addFormControls(this.siteHazardAddEditForm, [
          { controlName: "noOfSecurityOfficers" }, { controlName: 'securityServiceProviderName' }, { controlName: "isSecurityProviderControlRoomOnSite", defaultValue: true },
          { controlName: 'controlRoomPhoneNoCountryCode', defaultValue: defaultCountryCode }, { controlName: "controlRoomPhoneNo" }]);
        this.siteHazardAddEditForm = setRequiredValidator(this.siteHazardAddEditForm, ['noOfSecurityOfficers',
          'securityServiceProviderName', "controlRoomPhoneNo"]);
      }
      else {
        this.siteHazardAddEditForm = removeFormControls(this.siteHazardAddEditForm, [
          "noOfSecurityOfficers", 'securityServiceProviderName', "controlRoomPhoneNoCountryCode", "controlRoomPhoneNo",
          "isSecurityProviderControlRoomOnSite"]);
      }
    });
  }

  getSiteHazardById(): void {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.SITE_HAZARD_SERVICES, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined, {
        customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
        addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
        partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId,
        isExist: this.dealerCustomerServiceAgreementParamsDataModel.isExist,
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (Object.keys(response.resources ? response.resources : {}).length === 0) {
            this.rxjsService.setGlobalLoaderProperty(false);
            return
          };
          const siteHazardAddEditModel = new DealerSiteHazardAddEditModel(response.resources);
          this.siteHazardAddEditForm.patchValue(siteHazardAddEditModel);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createSiteHazardForm(): void {
    let siteHazardAddEditModel = new DealerSiteHazardAddEditModel();
    this.siteHazardAddEditForm = this.formBuilder.group({});
    Object.keys(siteHazardAddEditModel).forEach((key) => {
      this.siteHazardAddEditForm.addControl(key, new FormControl(siteHazardAddEditModel[key]));
    });
    this.siteHazardAddEditForm = setRequiredValidator(this.siteHazardAddEditForm, ["swimmingPoolLocation",
      "noOfSecurityOfficers", 'securityServiceProviderName', "controlRoomPhoneNo"]);
  }

  onSubmit(): void {
    if (this.siteHazardAddEditForm.invalid) {
      return;
    }
    this.siteHazardAddEditForm.value.createdUserId = this.dealerCustomerServiceAgreementParamsDataModel.createdUserId;
    this.siteHazardAddEditForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.siteHazardAddEditForm.value.referenceId = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
    this.siteHazardAddEditForm.value.customerId = this.dealerCustomerServiceAgreementParamsDataModel.customerId;
    this.siteHazardAddEditForm.value.addressId = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
    this.siteHazardAddEditForm.value.partitionId = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
    if (this.siteHazardAddEditForm.value.controlRoomPhoneNo) {
      this.siteHazardAddEditForm.value.controlRoomPhoneNo = this.siteHazardAddEditForm.value.controlRoomPhoneNo.toString().replace(/\s/g, "");
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.SITE_HAZARD_SERVICES, this.siteHazardAddEditForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (!this.siteHazardAddEditForm.value.siteHazardId) {
            this.siteHazardAddEditForm.get('siteHazardId').setValue(response.resources);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToAgreementSummaryPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'next') {
        if ((this.siteHazardAddEditForm.get('siteHazardId').value &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'agreement summary') {
        this.rxjsService.navigateToAgreementSummaryPage();
      }
    }
  }
}
