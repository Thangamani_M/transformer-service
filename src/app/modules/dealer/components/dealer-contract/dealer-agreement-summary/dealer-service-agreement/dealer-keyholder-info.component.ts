import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import {
  loggedInUserData,
  SalesModuleApiSuffixModels
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogModel, ConfirmDialogPopupComponent,
  countryCodes, CrudService, CustomDirectiveConfig, formConfigs,
  HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  onBlurActionOnDuplicatedFormControl,
  onFormControlChangesToFindDuplicate,
  prepareGetRequestHttpParams,
  prepareRequiredHttpParams, RxjsService
} from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerCustomerServiceAgreementParamsDataModel, DealerEmergencyContactModel, DealerKeyHolderInfoAddEditModel, DealerKeyholderObjectModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
@Component({
  selector: 'app-dealer-keyholder-info',
  templateUrl: './dealer-keyholder-info.component.html',
  styleUrls: ['./dealer-service-installation-agreement.scss']
})

export class DealerKeyholderInfoComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  keyHolderInfoForm: FormGroup;
  keyholders: FormArray;
  emergencyContacts: FormArray;
  isFocus = false;
  countryCodes = countryCodes;
  currentAgreementTabObj;
  @ViewChildren('keyholderRows',) keyholderRows: QueryList<ElementRef>;
  @ViewChildren('emergencyContactRows') emergencyContactRows: QueryList<ElementRef>;
  areEmergencyContactsFocus = false;
  areKeyholdersFocus = false;
  loggedInUserData: LoggedInUserModel;
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  pageHeading = 'Keyholder Information';
  currentStep;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private router: Router, public rxjsService: RxjsService, private store: Store<AppState>,
    private dialog: MatDialog
  ) {
    this.combineLatestNgrxStoreData();
  }

  ngOnInit(): void {
    this.createKeyHolderInfoForm();
    this.getKeyholderById();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(dealerCustomerServiceAgreementDataState$)]
    ).pipe(take(1)).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[1]);
      this.currentAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['agreementTabName'].toLowerCase().includes('key holder'));
      this.currentStep = this.currentAgreementTabObj.indexNo;
    });
  }

  getKeyholderById(): void {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.KEYHOLDER_GET, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined, {
        customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
        addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
        partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId,
        isExist: this.dealerCustomerServiceAgreementParamsDataModel.isExist,
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.keyholders = this.keyHoldersArray;
          while (this.keyHoldersArray.length) {
            this.keyHoldersArray.removeAt(this.keyHoldersArray.length - 1);
          }
          this.emergencyContacts = this.emergencyContactsArray;
          while (this.emergencyContactsArray.length) {
            this.emergencyContactsArray.removeAt(this.emergencyContactsArray.length - 1);
          }
          if (response.resources.keyholders.length == 0) {
            this.keyHoldersArray.push(this.createNewItem());
            this.keyHoldersArray.push(this.createNewItem());
            this.emergencyContactsArray.push(this.createNewItem('emergencyContacts'));
          }
          else {
            response.resources.keyholders.forEach((keyholderPersonObj: DealerKeyholderObjectModel) => {
              this.keyHoldersArray.push(this.createNewItem('keyholders', keyholderPersonObj));
            });
            response.resources.emergencyContacts.forEach((emergencyContactModel: DealerEmergencyContactModel) => {
              this.emergencyContactsArray.push(this.createNewItem('emergencyContacts', emergencyContactModel));
            });
          }
          this.keyholders = this.keyHoldersArray;
          this.emergencyContacts = this.emergencyContactsArray;
          const keyHolderInfoAddEditModel = new DealerKeyHolderInfoAddEditModel(response.resources);
          this.keyHolderInfoForm.patchValue(keyHolderInfoAddEditModel);
          if (response.resources.keyholders.length > 0 && response.resources.emergencyContacts.length > 0) {
            this.focusAllFields();
          }
        }
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 1000);
      });
  }

  focusAllFields() {
    setTimeout(() => {
      this.focusInAndOutFormArrayFields();
      this.focusInAndOutFormArrayFields('emergencyContacts');
    }, 100);
  }

  createKeyHolderInfoForm(): void {
    let keyHolderInfoAddEditModel = new DealerKeyHolderInfoAddEditModel();
    this.keyHolderInfoForm = this.formBuilder.group({});
    Object.keys(keyHolderInfoAddEditModel).forEach((key) => {
      this.keyHolderInfoForm.addControl(key, new FormArray([]));
    });
  }

  createNewItem(itemType = 'keyholders', object?): FormGroup {
    let objectModel = itemType == 'keyholders' ? new DealerKeyholderObjectModel(object) : new DealerEmergencyContactModel(object);
    let formControls = {};
    if (itemType == 'keyholders') {
      Object.keys(objectModel).forEach((key) => {
        formControls[key] = [objectModel[key],
        (key === 'keyHolderId' || key === 'keyholderPersonId' || key == 'addressId' || key == 'referenceId' || key == 'customerId' ||
          key === 'password' || key === 'createdUserId' || key === 'partitionId') ? [] : [Validators.required]]
      });
    }
    else {
      Object.keys(objectModel).forEach((key) => {
        formControls[key] = [objectModel[key],
        (key === 'emergencyContactId' || key === 'createdUserId' || key == 'addressId' || key == 'referenceId' || key == 'customerId' || key === 'partitionId') ? [] : [Validators.required]]
      });
    }
    formControls['createdUserId'][0] = this.dealerCustomerServiceAgreementParamsDataModel.createdUserId;
    formControls['modifiedUserId'][0] = this.loggedInUserData.userId;
    formControls['referenceId'][0] = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
    formControls['partitionId'][0] = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
    formControls['customerId'][0] = this.dealerCustomerServiceAgreementParamsDataModel.customerId;
    formControls['addressId'][0] = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(itemType = 'keyholders'): void {
    if (itemType == 'keyholders') {
      this.areKeyholdersFocus = true;
      this.keyholderRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    }
    else {
      this.areEmergencyContactsFocus = true;
      this.emergencyContactRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
    }
  }

  addItem(itemType = 'keyholders'): void {
    if (itemType == 'keyholders') {
      if (this.keyHoldersArray.invalid) {
        this.focusInAndOutFormArrayFields(itemType);
        return;
      };
      this.areKeyholdersFocus = false;
      this.keyholders = this.keyHoldersArray;
      let keyholderObjectModel = new DealerKeyholderObjectModel();
      this.keyholders.insert(0, this.createNewItem(itemType, keyholderObjectModel));
    }
    else {
      if (this.emergencyContactsArray.invalid) {
        this.focusInAndOutFormArrayFields(itemType);
        return;
      };
      this.areEmergencyContactsFocus = false;
      this.emergencyContacts = this.emergencyContactsArray;
      let emergencyContactModel = new DealerEmergencyContactModel();
      this.emergencyContacts.insert(0, this.createNewItem(itemType, emergencyContactModel));
    }
  }

  removeItem(itemType = 'keyholders', i: number): void {
    if (itemType == 'keyholders') {
      if (!this.keyHoldersArray.controls[i].value.keyHolderId) {
        this.keyHoldersArray.removeAt(i);
        return;
      }
    }
    else {
      if (!this.emergencyContactsArray.controls[i].value.emergencyContactId) {
        this.emergencyContactsArray.removeAt(i);
        return;
      }
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    if (itemType == 'keyholders') {
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.keyHoldersArray.controls[i].value.keyHolderId && this.keyHoldersArray.length > 1) {
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_KEY_HOLDERS, undefined,
            prepareRequiredHttpParams({
              ids: this.keyHoldersArray.controls[i].value.keyHolderId,
              isDeleted: true,
              modifiedUserId: this.dealerCustomerServiceAgreementParamsDataModel.modifiedUserId
            }), 1).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.keyHoldersArray.removeAt(i);
              }
              if (this.keyHoldersArray.length === 0) {
                this.addItem();
              };
            });
        }
        else {
          this.keyHoldersArray.removeAt(i);
        }
      });
    }
    else {
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.emergencyContactsArray.controls[i].value.emergencyContactId && this.emergencyContactsArray.length > 1) {
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_EMERGENCY_CONTACTS, undefined,
            prepareRequiredHttpParams({
              ids: this.emergencyContactsArray.controls[i].value.emergencyContactId,
              isDeleted: true,
              modifiedUserId: this.loggedInUserData.userId
            }), 1).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.emergencyContactsArray.removeAt(i);
              }
              if (this.emergencyContactsArray.length === 0) {
                this.addItem('emergencyContacts');
              };
            });
        }
        else {
          this.emergencyContactsArray.removeAt(i);
        }
      });
    }
  }

  get keyHoldersArray(): FormArray {
    if (!this.keyHolderInfoForm) return;
    return this.keyHolderInfoForm.get("keyholders") as FormArray;
  }

  get emergencyContactsArray(): FormArray {
    if (!this.keyHolderInfoForm) return;
    return this.keyHolderInfoForm.get("emergencyContacts") as FormArray;
  }

  getKeyHolders(form): Array<any> {
    return form.controls.keyholders.controls;
  }

  getEmergencyContacts(form): Array<any> {
    return form.controls.emergencyContacts.controls;
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.keyHolderInfoForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  onSubmit(): void {
    if (this.keyHolderInfoForm.invalid) {
      return;
    }
    this.keyHolderInfoForm.value.keyholders.forEach((keyholderObj: DealerKeyholderObjectModel) => {
      keyholderObj.contactNo = keyholderObj.contactNo.toString().replace(/\s/g, "");
      keyholderObj.partitionId = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
      keyholderObj.addressId = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
      keyholderObj.referenceId = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
    });
    this.keyHolderInfoForm.value.emergencyContacts.forEach((emergencyContactObj: DealerEmergencyContactModel) => {
      emergencyContactObj.contactNo = emergencyContactObj.contactNo.toString().replace(/\s/g, "");
      emergencyContactObj.addressId = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
      emergencyContactObj.partitionId = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
      emergencyContactObj.referenceId = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.KEYHOLDER_POST, this.keyHolderInfoForm.value, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources.keyholders) {
            response.resources.keyholders.forEach((keyholderPersonObj: DealerKeyholderObjectModel, index: number) => {
              this.keyHoldersArray.controls[index].get('keyHolderId').setValue(keyholderPersonObj.keyHolderId);
            });
          }
          if (response.resources.emergencyContacts) {
            response.resources.emergencyContacts.forEach((emergencyContactModel: DealerEmergencyContactModel, index: number) => {
              this.emergencyContactsArray.controls[index].get('emergencyContactId').setValue(emergencyContactModel.emergencyContactId);
            });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToAgreementSummaryPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'next') {
        if (!this.currentAgreementTabObj.isMandatory || (this.keyHolderInfoForm.value.keyholders[0]?.keyHolderId &&
          this.keyHolderInfoForm.value.emergencyContacts[0]?.emergencyContactId &&
          this.currentAgreementTabObj.isMandatory)) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'agreement summary') {
        this.rxjsService.navigateToAgreementSummaryPage();
      }
    }
  }
}
