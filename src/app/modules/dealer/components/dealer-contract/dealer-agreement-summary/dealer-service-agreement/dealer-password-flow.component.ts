import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SalesModuleApiSuffixModels, selectStaticEagerLoadingDogTypesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogModel, ConfirmDialogPopupComponent,
  CustomDirectiveConfig, ResponseMessageTypes, SnackbarService
} from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import {
  clearFormControlValidators,
  formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator
} from '@app/shared/utils';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerCustomerServiceAgreementParamsDataModel, DealerDocTypes, DealerMedicalConditions, DealerPasswordsAddEditModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
@Component({
  selector: 'app-dealer-password-flow',
  templateUrl: './dealer-password-flow.component.html',
  styleUrls: ['./dealer-service-installation-agreement.scss']
})

export class DealerPasswordFlowComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  passwordsForm: FormGroup;
  responseLength = 0;
  dogTypes: FormArray;
  medicalConditions: FormArray;
  @ViewChildren('input') rows: QueryList<any>;
  dogTypesList = [];
  details;
  show: boolean = false;
  showPlus: boolean = false;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  isPasswordShow = false;
  isPasswordShowForDistressWord = false;
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  pageHeading = 'Password Special Instructions';
  currentStep;

  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private dialog: MatDialog, private router: Router,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    public rxjsService: RxjsService, private store: Store<AppState>
  ) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectStaticEagerLoadingDogTypesState$),
      this.store.select(loggedInUserData),
      this.store.select(dealerCustomerServiceAgreementDataState$)]
    ).subscribe((response) => {
      this.dogTypesList = response[0];
      this.loggedInUserData = new LoggedInUserModel(response[1]);
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[2]);
      this.currentAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['agreementTabName'].toLowerCase().includes('special'));
      this.currentStep = this.currentAgreementTabObj.indexNo;
    });
  }

  ngOnInit(): void {
    this.createPasswordForm();
    this.getSpecialInstructionsByCustomerAndPartitionId();
    this.onFormControlChanges();
  }

  showNoOfDogs: boolean = false;
  isDogDescriptionRequired =false
  onFormControlChanges(): void {
    this.passwordsForm.get('isDogs').valueChanges.subscribe((isDogOnSite: boolean) => {
      if (!isDogOnSite) {
        this.isDogDescriptionRequired = false
        this.passwordsForm = clearFormControlValidators(this.passwordsForm, ["dogDescription"]);
        this.passwordsForm.updateValueAndValidity()
        if (this.details.specialInstructionId) {
          if (this.dogTypes && this.dogTypes.length > 0) {
            this.snackbarService.openSnackbar("Please delete existing dogs", ResponseMessageTypes.WARNING);
            setTimeout(() => {
              this.passwordsForm.get('isDogs').setValue(true)

            }, 10)
          }
        }
      }else{
        this.isDogDescriptionRequired = true
        this.passwordsForm = setRequiredValidator(this.passwordsForm, ["dogDescription"]);
        this.passwordsForm.updateValueAndValidity()
      }
    });

    this.passwordsForm.get('noOfDogs').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.showPlus = true;
      }
      else {
        this.showPlus = false;
        this.show = false;
      }
    });
  }

  getSpecialInstructionsByCustomerAndPartitionId(): void {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.PASSWORD_SPECIAL_INSTRUCTIONS, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined, {
        customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
        addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
        partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId,
        isExist: this.dealerCustomerServiceAgreementParamsDataModel.isExist,
      })).subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources && response.isSuccess) {
          this.details = response.resources;
          if (this.dogTypes) {
            this.dogTypes.clear();
          }
          if (this.details.contractId || this.details.customerId) {
            this.passwordsForm.patchValue(response.resources);
            this.passwordsForm.get('isDogs').setValue(this.details.isDogs);
            this.dogTypes = this.getdogTypesListArray;
            this.medicalConditions = this.getMedicalConditionListArray;
            if (response.resources.dogTypes.length > 0) {
              this.show = true;
              response.resources.dogTypes.forEach((dg) => {
                this.dogTypes.push(this.createDogTypesListModel(dg));
              });
            };
            if (this.details.contractId) {
              this.passwordsForm.get('isDogs').disable();
            }
            this.medicalConditions = this.getMedicalConditionListArray;
            while (this.getMedicalConditionListArray.length) {
              this.getMedicalConditionListArray.removeAt(this.getMedicalConditionListArray.length - 1);
            }
            if (response.resources.medicalConditions.length > 0) {
              response.resources.medicalConditions.forEach((md) => {
                this.medicalConditions.push(this.createMedicalConditionListModel(md));
              });
            }
            else {
              this.medicalConditions.push(this.createMedicalConditionListModel());
            }
          } else {
            this.medicalConditions = this.getMedicalConditionListArray;
            this.medicalConditions.push(this.createMedicalConditionListModel());
          }
        } else {
          this.medicalConditions = this.getMedicalConditionListArray;
          this.medicalConditions.push(this.createMedicalConditionListModel());
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createPasswordForm(): void {
    let passwordsAddEditModel = new DealerPasswordsAddEditModel();
    this.passwordsForm = this.formBuilder.group({
      dogTypes: this.formBuilder.array([]),
      medicalConditions: this.formBuilder.array([])
    });
    Object.keys(passwordsAddEditModel).forEach((key) => {
      this.passwordsForm.addControl(key, new FormControl(passwordsAddEditModel[key]));
    });
    this.passwordsForm = setRequiredValidator(this.passwordsForm, ["alarmCancellationPassword"]);
  }

  get getdogTypesListArray(): FormArray {
    if (!this.passwordsForm) return;
    return this.passwordsForm.get("dogTypes") as FormArray;
  }

  //Create FormArray controls
  createDogTypesListModel(docTypes?: DealerDocTypes): FormGroup {
    let docTypesFormControlModel = new DealerDocTypes(docTypes);
    let formControls = {};
    Object.keys(docTypesFormControlModel).forEach((key) => {
      if (key == 'dogTypeId') {
        formControls[key] = [{ value: docTypesFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: docTypesFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getMedicalConditionListArray(): FormArray {
    if (!this.passwordsForm) return;
    return this.passwordsForm.get("medicalConditions") as FormArray;
  }

  //Create FormArray controls
  createMedicalConditionListModel(medicalConditions?: DealerMedicalConditions): FormGroup {
    let medicalConditionsFormControlModel = new DealerMedicalConditions(medicalConditions);
    let formControls = {};
    Object.keys(medicalConditionsFormControlModel).forEach((key) => {
      formControls[key] = [{ value: medicalConditionsFormControlModel[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addItem(event): void {
    this.dogTypes = this.getdogTypesListArray;
    let docTypesFormControlModel = new DealerDocTypes();

    let dogs = parseInt(event.target.value);
    if (event.target.value) {
      if (this.details && this.details?.isDogs && dogs < this.details?.dogTypes.length) {
        this.snackbarService.openSnackbar("Please delete existing dogs", ResponseMessageTypes.WARNING);
        this.passwordsForm.get('noOfDogs').setValue(this.details?.dogTypes.lengthh);

        return;
      }
      else if (this.details && this.details?.isDogs && dogs >= this.details?.dogTypes.length) {
        this.dogTypes.clear();
        this.details.dogTypes.forEach((dg) => {
          this.dogTypes.push(this.createDogTypesListModel(dg));
        });
        for (var i = this.details?.dogTypes.length; i < dogs; i++) {
          this.dogTypes.insert(i, this.createDogTypesListModel(docTypesFormControlModel));
        }

      } else {
        this.dogTypes.clear();
        for (var j = 0; j < this.passwordsForm.value.noOfDogs; j++) {
          this.dogTypes.insert(j, this.createDogTypesListModel(docTypesFormControlModel));
        }
      }
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  addMedicalItem(): void {
    this.medicalConditions = this.getMedicalConditionListArray;
    this.medicalConditions.insert(0, this.createMedicalConditionListModel());
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  deleteDogType(i: number) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getdogTypesListArray.controls[i].value.dogTypeDetailId && this.getdogTypesListArray.length > 0) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_DOG_TYPES,
          undefined,
          prepareRequiredHttpParams({
            Ids: this.getdogTypesListArray.controls[i].value.dogTypeDetailId,
            IsDeleted: false,
            modifiedUserId: this.loggedInUserData.userId
          })).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getdogTypesListArray.removeAt(i);
              this.details.dogTypes.splice(i, 1);
            }
            this.passwordsForm.get('noOfDogs').setValue(this.getdogTypesListArray.length);
          });
      }
      else {
        this.getdogTypesListArray.removeAt(i);
        this.passwordsForm.get('noOfDogs').setValue(this.getdogTypesListArray.length);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  deleteMedical(i: number) {
    if (this.getMedicalConditionListArray.controls[i].value.medicalDetailId == '') {
      this.getMedicalConditionListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getMedicalConditionListArray.controls[i].value.medicalDetailId && this.getMedicalConditionListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_MEDICAL,
          undefined,
          prepareRequiredHttpParams({
            Ids: this.getMedicalConditionListArray.controls[i].value.medicalDetailId,
            IsDeleted: false,
            modifiedUserId: this.loggedInUserData.userId
          }), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getMedicalConditionListArray.removeAt(i);
            }
            if (this.getMedicalConditionListArray.length === 0) {
              this.addMedicalItem();
            };
          });
      }
      else {
        this.getdogTypesListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    let postObj = this.passwordsForm.getRawValue();
    if (!postObj.isDogs) {
      if (this.dogTypes) {
        this.dogTypes.clear();
      }
    }
    if (this.passwordsForm.invalid) {
      return;
    }
    postObj.createdUserId = this.dealerCustomerServiceAgreementParamsDataModel.createdUserId;
    postObj.modifiedUserId = this.loggedInUserData.userId;
    postObj.addressId = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
    postObj.customerId = this.dealerCustomerServiceAgreementParamsDataModel.customerId;
    postObj.referenceId = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
    postObj.partitionId = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
    let data = postObj.dogTypes;
    let detailId;
    if (data.length > 0) {
      data.forEach(element => {
        detailId = element.dogDetailId
      });
    }
    if (detailId) {
      if (data.length > 0) {
        data.forEach(element => {
          element.dogDetailId = detailId;
        });
      }
    }
    postObj.dogTypes = data;
    if (!postObj.isDogs) {
      postObj.dogTypes = null;
      postObj.noOfDogs = null;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.PASSWORD_SPECIAL_INSTRUCTIONS, postObj)
      .subscribe(resp => {
        if (resp.resources && resp.statusCode == 200 && resp.isSuccess) {
          if (!this.passwordsForm.get('specialInstructionId').value) {
            this.passwordsForm.get('specialInstructionId').setValue(resp.resources);
          }
        }
      });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToAgreementSummaryPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'next') {
        if ((this.passwordsForm.value.specialInstructionId &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'agreement summary') {
        this.rxjsService.navigateToAgreementSummaryPage();
      }
    }
  }
}
