import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import {
  loggedInUserData, SalesModuleApiSuffixModels
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogModel,
  ConfirmDialogPopupComponent, countryCodes, CrudService, CustomDirectiveConfig,
  emailPattern, formConfigs,
  HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  onBlurActionOnDuplicatedFormControl,
  onFormControlChangesToFindDuplicate,
  prepareGetRequestHttpParams,
  prepareRequiredHttpParams, ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerCustomerServiceAgreementParamsDataModel, DealerOpenCloseContact, DealerOpenCloseMonitoringAddEditModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
@Component({
  selector: 'app-dealer-open-close-monitoring',
  templateUrl: './dealer-open-close-monitoring.component.html',
  styleUrls: ['./dealer-service-installation-agreement.scss']
})

export class DealerOpenAndCloseMonitoringComponent implements OnInit {
  formConfigs = formConfigs;
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  openCloseMonitoringForm: FormGroup;
  countryCodes = countryCodes;
  dateFormate: '24';
  openAndCloseMonitoringId;
  contacts: FormArray;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  @ViewChildren('contactRows') contactRows: QueryList<ElementRef>;
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  pageHeading = 'Arming and Disarming Time';
  currentStep;

  constructor(private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder, private dialog: MatDialog,
    public rxjsService: RxjsService, private store: Store<AppState>,
    private snackbarService: SnackbarService, private router: Router, private momentService: MomentService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(dealerCustomerServiceAgreementDataState$)
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[1]);
      this.currentAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['agreementTabName'].toLowerCase().includes('arming'));
      this.currentStep = this.currentAgreementTabObj.indexNo;
    });
  }

  ngOnInit(): void {
    this.createOpenCloseMonitoringForm();
    this.getArmingAndDisarmingByCustomerAndPartitionId();
  }

  getSelectedValue(event) {
    if (!event) return;
    this.toggleAddDays({ checked: this.openCloseMonitoringForm.get('allDays').value })
  }

  focusInAndOutFormArrayFields(): void {
    setTimeout(() => {
      this.contactRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    }, 100);
  }

  getContactControl(form): Array<any> {
    return form.controls.contacts.controls;
  }

  addItem() {
    if (this.contacts.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.contacts = this.getContacts;
    let openCloseContactModel = new DealerOpenCloseContact();
    this.contacts.insert(0, this.createContact(openCloseContactModel));
  }

  createOpenCloseMonitoringForm(): void {
    let openCloseMonitoringAddEditModel = new DealerOpenCloseMonitoringAddEditModel();
    this.openCloseMonitoringForm = this.formBuilder.group({
      contacts: this.formBuilder.array([]),
    });
    Object.keys(openCloseMonitoringAddEditModel).forEach((key) => {
      if (key == 'monthlyReportEmailTo') {
        this.openCloseMonitoringForm.addControl(key,
          new FormControl(openCloseMonitoringAddEditModel[key], Validators.compose([Validators.email, emailPattern])));
      }
      else {
        this.openCloseMonitoringForm.addControl(key,
          new FormControl(openCloseMonitoringAddEditModel[key]));
      }
    });
  }

  get getContacts(): FormArray {
    if (!this.openCloseMonitoringForm) return;
    return this.openCloseMonitoringForm.get("contacts") as FormArray;
  }

  createContact(openCloseContact?: DealerOpenCloseContact): FormGroup {
    let openCloseContactModel = new DealerOpenCloseContact(openCloseContact);
    let formControls = {};
    Object.keys(openCloseContactModel).forEach((key) => {
      formControls[key] = [openCloseContactModel[key],
      (key === 'smsNo') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  toggleAddDays(event): void {
    let mondayArmingTime = this.openCloseMonitoringForm.get('mondayArmingTime').value;
    let mondayDisarmingTime = this.openCloseMonitoringForm.get('mondayDisarmingTime').value;
    if (event.checked && (mondayArmingTime || mondayDisarmingTime)) {
      this.openCloseMonitoringForm.patchValue({
        "tuesdayArmingTime": mondayArmingTime,
        "wednesdayArmingTime": mondayArmingTime,
        "thursdayArmingTime": mondayArmingTime,
        "fridayArmingTime": mondayArmingTime,
      })
      setTimeout(() => {
        this.openCloseMonitoringForm.patchValue({
          "tuesdayDisarmingTime": mondayDisarmingTime,
          "wednesdayDisarmingTime": mondayDisarmingTime,
          "thursdayDisarmingTime": mondayDisarmingTime,
          "fridayDisarmingTime": mondayDisarmingTime,
        })
      }, 0);
    }
  }

  removeItem(i: number): void {
    if (!this.contacts.controls[i].value.openAndCloseMonitoringContactId) {
      this.contacts.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.contacts.controls[i].value.openAndCloseMonitoringContactId && this.contacts.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DELETE_OPENCLOSE_CONTACT, undefined,
          prepareRequiredHttpParams({
            ids: this.contacts.controls[i].value.openAndCloseMonitoringContactId,
            isDeleted: true,
            modifiedUserId: this.dealerCustomerServiceAgreementParamsDataModel.modifiedUserId
          }), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.contacts.removeAt(i);
            }
            if (this.contacts.length === 0) {
              this.addItem();
            };
          });
      }
      else {
        this.contacts.removeAt(i);
      }
    });
  }

  getArmingAndDisarmingByCustomerAndPartitionId(): void {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.OPEN_CLOSE_MONITORING, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined, {
        customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
        addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
        partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId,
        isExist: this.dealerCustomerServiceAgreementParamsDataModel.isExist,
      })).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources && (response.resources.openAndCloseMonitoringId || response.resources.customerId)) {
            // this.openCloseMonitoringForm.patchValue(response.resources);
            response.resources.mondayArmingTime = this.formateTime(response.resources.mondayArmingTime)
            response.resources.mondayDisarmingTime = this.formateTime(response.resources.mondayDisarmingTime)
            response.resources.tuesdayArmingTime = this.formateTime(response.resources.tuesdayArmingTime)
            response.resources.tuesdayDisarmingTime = this.formateTime(response.resources.tuesdayDisarmingTime)
            response.resources.thursdayDisarmingTime = this.formateTime(response.resources.thursdayDisarmingTime)
            response.resources.thursdayArmingTime = this.formateTime(response.resources.thursdayArmingTime)
            response.resources.wednesdayArmingTime = this.formateTime(response.resources.wednesdayArmingTime)
            response.resources.wednesdayDisarmingTime = this.formateTime(response.resources.wednesdayDisarmingTime)
            response.resources.fridayArmingTime = this.formateTime(response.resources.fridayArmingTime)
            response.resources.fridayDisarmingTime = this.formateTime(response.resources.fridayDisarmingTime)
            response.resources.saturdayArmingTime = this.formateTime(response.resources.saturdayArmingTime)
            response.resources.saturdayDisarmingTime = this.formateTime(response.resources.saturdayDisarmingTime)
            response.resources.sundayArmingTime = this.formateTime(response.resources.sundayArmingTime)
            response.resources.sundayDisarmingTime = this.formateTime(response.resources.sundayDisarmingTime)
            response.resources.publicHolidayArmingTime = this.formateTime(response.resources.publicHolidayArmingTime)
            response.resources.publicHolidayDisarmingTime = this.formateTime(response.resources.publicHolidayDisarmingTime)

            this.contacts = this.getContacts;
            while (this.contacts.length) {
              this.contacts.removeAt(this.contacts.length - 1);
            }
            response.resources.contacts.forEach((obj) => {
              if (typeof obj.smsNo === "string") {
                obj.smsNo = obj.smsNo.split(" ").join("")
                obj.smsNo = obj.smsNo.replace(/"/g, "");
              }
              this.contacts.push(this.createContact(obj));
            });
            this.openAndCloseMonitoringId = response.resources.openAndCloseMonitoringId
            this.openCloseMonitoringForm.controls['openAndCloseMonitoringId'].setValue(response.resources.openAndCloseMonitoringId);
            if (Object.keys(response.resources ? response.resources : {}).length === 0) return;
            const openCloseMonitoringAddEditModel = new DealerOpenCloseMonitoringAddEditModel(response.resources);
            Object.keys(openCloseMonitoringAddEditModel).forEach((key) => {
              if (openCloseMonitoringAddEditModel[key]) {
                if (key == "createdUserId" || key == "modifiedUserId" || key == "contacts" ||
                  key == "customerId" || key == "partitionId" ||
                  key == "addressId" || key == "openAndCloseMonitoringId" || key == "countryCode"
                  || key == "monthlyReportEmailTo") {
                  openCloseMonitoringAddEditModel[key] = openCloseMonitoringAddEditModel[key]
                } else {
                  openCloseMonitoringAddEditModel[key] = this.formateTime(openCloseMonitoringAddEditModel[key])
                }
              }
            });
            this.openCloseMonitoringForm.patchValue(openCloseMonitoringAddEditModel);
            setTimeout(() => {
              this.openCloseMonitoringForm.patchValue(openCloseMonitoringAddEditModel); // To resolve form min value data binding validation issue
            }, 5);
          }
          else {
            this.contacts = this.getContacts;
            this.contacts.push(this.createContact());
          }
          if (response.resources && response.resources.contacts.length > 0) {
            setTimeout(() => {
              this.focusInAndOutFormArrayFields();
            }, 10);
          }
        }
      });
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.openCloseMonitoringForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  ifEmpty(val) {
    if (!val && !this.openCloseMonitoringForm.get('allDays').value)
      this.snackbarService.openSnackbar('Select Start Time', ResponseMessageTypes.WARNING)
  }
  todayEndTime(dateTime) {
    if (!dateTime) return
    let date = new Date(dateTime).setHours(23, 59)
    return new Date(date)
  }
  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date)
  }


  onSubmit(): void {
    if (this.openCloseMonitoringForm.invalid) return;
    const openCloseMonitoringAddEditModel = new DealerOpenCloseMonitoringAddEditModel(this.openCloseMonitoringForm.value);
    Object.keys(openCloseMonitoringAddEditModel).forEach((key) => {
      if (openCloseMonitoringAddEditModel[key]) {
        if (key == "createdUserId" || key == "contacts" || key == "customerId" || key == "customerId" || key == "partitionId"
          || key == "addressId" || key == "openAndCloseMonitoringId" || key == "countryCode" ||
          key == "smsNumber" || key == "smsNumberCountryCode" || key == "monthlyReportEmailTo") {
          openCloseMonitoringAddEditModel[key] = openCloseMonitoringAddEditModel[key]
        } else {
          openCloseMonitoringAddEditModel[key] = this.momentService.convertNormalToRailayTime(openCloseMonitoringAddEditModel[key])
        }
      }
    });
    let contacts = this.openCloseMonitoringForm.value.contacts;
    contacts.forEach(element => {
      if (typeof element.smsNo === "string") {
        element.smsNo = element.smsNo.split(" ").join("")
        element.smsNo = element.smsNo.replace(/"/g, "");
      }
    });
    openCloseMonitoringAddEditModel.contacts = contacts;
    openCloseMonitoringAddEditModel.createdUserId = this.dealerCustomerServiceAgreementParamsDataModel.createdUserId;
    openCloseMonitoringAddEditModel.modifiedUserId = this.loggedInUserData.userId;
    openCloseMonitoringAddEditModel.partitionId = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
    openCloseMonitoringAddEditModel.customerId = this.dealerCustomerServiceAgreementParamsDataModel.customerId;
    openCloseMonitoringAddEditModel.addressId = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
    openCloseMonitoringAddEditModel.referenceId = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
    openCloseMonitoringAddEditModel.monthlyReportEmailTo = openCloseMonitoringAddEditModel.monthlyReportEmailTo ? openCloseMonitoringAddEditModel.monthlyReportEmailTo : null;
    openCloseMonitoringAddEditModel.openAndCloseMonitoringId = this.openCloseMonitoringForm.get('openAndCloseMonitoringId').value ?
      this.openCloseMonitoringForm.get('openAndCloseMonitoringId').value : this.openAndCloseMonitoringId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.OPEN_CLOSE_MONITORING, openCloseMonitoringAddEditModel)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          if (response.message.toLowerCase().includes('created')) {
            response.resources.contacts.forEach((openCloseMonitoringAddEditModel, index: number) => {
              this.contacts.controls[index].get('openAndCloseMonitoringContactId').setValue(openCloseMonitoringAddEditModel.openAndCloseMonitoringContactId);
            });
            this.openCloseMonitoringForm.get('openAndCloseMonitoringId').setValue(response.resources.openAndCloseMonitoringId);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToAgreementSummaryPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'next') {
        if ((this.openCloseMonitoringForm.value.openAndCloseMonitoringId &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'agreement summary') {
        this.rxjsService.navigateToAgreementSummaryPage();
      }
    }
  }
}