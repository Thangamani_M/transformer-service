import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { generateCurrentYearToNext99Years, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, monthsByNumber, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared/utils';
import { HubConnection } from '@aspnet/signalr';
import { DealerCustomerIdentificationModel, DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData, selectStaticEagerLoadingCustomerTypesState$ } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
declare var $;
@Component({
  selector: 'app-dealer-contract-creation',
  templateUrl: './dealer-contract-creation.component.html',
  styleUrls: ['./dealer-service-installation-agreement.scss']
})

export class DealerContractCreationComponent implements OnInit {
  availableTabs = [];
  isContractValid = true;
  currentAgreementTabObj;
  contractTermsandConditions = [];
  customer_identification_modal =false
  parseHtml;
  connection: HubConnection;
  customerIdentificationForm: FormGroup;
  customerTypes = [];
  customerSiteType;
  loggedInUserData: LoggedInUserModel;
  months = monthsByNumber;
  years = generateCurrentYearToNext99Years();
  selectedItem;
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  pageHeading = 'Service Contract Creation';
  currentStep;

  constructor(private crudService: CrudService, private store: Store<AppState>,
    private rxjsService: RxjsService, private router: Router, private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService, private formBuilder: FormBuilder) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectStaticEagerLoadingCustomerTypesState$),
      this.store.select(loggedInUserData),
      this.store.select(dealerCustomerServiceAgreementDataState$)]
    ).subscribe((response) => {
      this.customerTypes = response[0];
      this.loggedInUserData = new LoggedInUserModel(response[1]);
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[2]);
      this.currentAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('contract'));
      this.currentStep = this.currentAgreementTabObj.indexNo;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getAgreementTabs();
    this.onBootstrapModalEventChanges();
    this.createCustomerIdentificationForm();
    this.onFormControlChanges();
  }

  createCustomerIdentificationForm(): void {
    let customerIdentificationModel = new DealerCustomerIdentificationModel();
    this.customerIdentificationForm = this.formBuilder.group({});
    Object.keys(customerIdentificationModel).forEach((key) => {
      this.customerIdentificationForm.addControl(key,
        new FormControl(customerIdentificationModel[key]));
    });
  }

  onBootstrapModalEventChanges(): void {
    if(this.customer_identification_modal) {
      this.rxjsService.setDialogOpenProperty(true);
    }
    if(!this.customer_identification_modal) {
      this.rxjsService.setDialogOpenProperty(false);
    }
  }

  onFormControlChanges() {
    this.customerIdentificationForm.get('isPassport').valueChanges.subscribe((isPassport: boolean) => {
      if (this.customerSiteType.id == 2) {
        return;
      }
      if (isPassport) {
        this.customerIdentificationForm = setRequiredValidator(this.customerIdentificationForm, ['passportNo', 'passportExpiryMonth', 'passportExpiryYear']);
        this.customerIdentificationForm.get('SAID').clearValidators();
        this.customerIdentificationForm.get('SAID').updateValueAndValidity();
        this.customerIdentificationForm.get('SAID').setValue(null);
      }
      else {
        this.customerIdentificationForm = setRequiredValidator(this.customerIdentificationForm, ['SAID']);
        this.customerIdentificationForm.get('passportNo').clearValidators();
        this.customerIdentificationForm.get('passportNo').updateValueAndValidity();
        this.customerIdentificationForm.get('passportNo').setValue(null);
        this.customerIdentificationForm.get('passportExpiryMonth').clearValidators();
        this.customerIdentificationForm.get('passportExpiryMonth').updateValueAndValidity();
        this.customerIdentificationForm.get('passportExpiryMonth').setValue(null);
        this.customerIdentificationForm.get('passportExpiryYear').clearValidators();
        this.customerIdentificationForm.get('passportExpiryYear').updateValueAndValidity();
        this.customerIdentificationForm.get('passportExpiryYear').setValue(null);
      }
    });
  }

  getAgreementTabs() {
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_SUMMARY_TAB_DETAILS,
      undefined,
      false,
      prepareRequiredHttpParams({
        customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
        addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
        partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          this.manipulateAgreementTabs(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCustomerIdentification(): void {
    // $(this.customer_identification_modal.nativeElement).modal('show');
    this.customer_identification_modal = true;
    this.onBootstrapModalEventChanges()
    this.crudService
      .get(
        ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.CUSTOMER_IDENTIFICATION,
        this.dealerCustomerServiceAgreementParamsDataModel.customerId
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.customerSiteType = this.customerTypes.find(cT => cT['id'] == response.resources.customerTypeId);
          if (this.customerSiteType.id == 2) {
            this.customerIdentificationForm = setRequiredValidator(this.customerIdentificationForm, ['companyRegNo']);
          }
          if (response.resources.isPassport) {
            response.resources['passportExpiryMonth'] = response.resources.passportExpiryDate.split("/")[0];
            response.resources['passportExpiryYear'] = response.resources.passportExpiryDate.split("/")[0];
          }
          this.customerIdentificationForm.patchValue(response.resources);
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  manipulateAgreementTabs(resources) {
    this.availableTabs = this.prepareAgreementTabs(resources).filter(r => r.agreementTabName !== 'Contract Creation');
    this.validateRequiredTabsData();
  }

  prepareAgreementTabs(resources) {
    resources.forEach((tabObj) => {
      tabObj.navigationUrl = '/dealer/dealer-contract' + tabObj.navigationUrl;
    });
    return resources;
  }

  validateRequiredTabsData(): void {
    let isMandatoryButNotFilledCount = 0;
    this.availableTabs.filter(a => a['isMandatory']).forEach((tabObj) => {
      const agreementTabName = tabObj.agreementTabName.replace(/\s/g, "").toLowerCase();
      if (agreementTabName.includes('keyholder') || agreementTabName.includes('domestic') || agreementTabName.includes('special') ||
        agreementTabName.includes('arming') || agreementTabName.includes('access') || agreementTabName.includes('cellpanic') || agreementTabName.includes('site') ||
        agreementTabName.includes('debtorcreation') || agreementTabName.includes('debtorbanking') || agreementTabName.includes('findu')
        || agreementTabName.includes('kycverification') || agreementTabName.includes('customeridentification')) {
        if (!tabObj.isFilled) {
          isMandatoryButNotFilledCount = 1;
        }
      }
    });
    this.isContractValid = isMandatoryButNotFilledCount == 0 ? true : false;
  }

  onSubmitCustomerIdentification() {
    if (this.customerIdentificationForm.invalid) {
      return;
    }
    this.customerIdentificationForm.value.customerId = this.dealerCustomerServiceAgreementParamsDataModel.customerId;
    this.customerIdentificationForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.customerIdentificationForm.value.partitionId = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
    this.customerIdentificationForm.value.addressId = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
    this.customerIdentificationForm.value.referenceId = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
    if (this.customerIdentificationForm.value.isPassport) {
      this.customerIdentificationForm.value.passportExpiryDate = `${this.customerIdentificationForm.value.passportExpiryMonth}/${this.customerIdentificationForm.value.passportExpiryYear}`
    }
    delete this.customerIdentificationForm.value.passportExpiryMonth;
    delete this.customerIdentificationForm.value.passportExpiryYear;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.CUSTOMER_IDENTIFICATION_AGREEMENT_TAB_POST, this.customerIdentificationForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.customer_identification_modal =false;
        this.onBootstrapModalEventChanges()
        this.getAgreementTabs();
      }
    });
  }

  onSubmit(): void {
    if (!this.isContractValid) {
      this.snackbarService.openSnackbar("Mandatory Tabs Needs To Be Filled..!!", ResponseMessageTypes.WARNING);
      return;
    }
    let payload = {
      createdUserId: this.loggedInUserData.userId,
      addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
      customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
      partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId,
      referenceId: this.dealerCustomerServiceAgreementParamsDataModel.referenceId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CONTRACTS, payload);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.onArrowClick('create contract');
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onArrowClick(type?: string) {
    if (type == 'create contract' || type == 'agreement summary') {
      this.rxjsService.navigateToAgreementSummaryPage();
    } else if (type == 'previous') {
      let previousAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
      this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
    }
  }
}
