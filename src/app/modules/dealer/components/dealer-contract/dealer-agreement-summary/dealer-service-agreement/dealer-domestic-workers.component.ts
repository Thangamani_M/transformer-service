import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import {
  loggedInUserData, SalesModuleApiSuffixModels, selectStaticEagerLoadingTypeOfWorkersState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  addFormControls, BreadCrumbModel, ConfirmDialogModel, ConfirmDialogPopupComponent,
  countryCodes, CrudService, CustomDirectiveConfig,
  formConfigs,
  HttpCancelService, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, removeAllFormValidators,
  removeFormControlError, removeFormControls,
  RxjsService, setRequiredValidator
} from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerCustomerServiceAgreementParamsDataModel, DealerDomesticWorkersAddEditModel, initialWorkingDays } from '@modules/dealer/models/dealer-service-agreement.model';
import { Store } from '@ngrx/store';
import { Guid } from 'guid-typescript';
import { combineLatest } from 'rxjs';
import { dealerCustomerServiceAgreementDataState$ } from '../dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';

@Component({
  selector: 'app-dealer-domestic-workers',
  templateUrl: './dealer-domestic-workers.component.html',
  styleUrls: ['./dealer-service-installation-agreement.scss']
})

export class DealerDomesticWorkersComponent implements OnInit {
  @ViewChildren('input') arrayList: QueryList<ElementRef>;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  selectedDomesticTypeWorkerName: string = "Domestic";
  initialWorkingDays = initialWorkingDays;
  tempWorkingDays = initialWorkingDays;
  domesticWorkersArray: any = [];
  domesticWorkersForm: FormGroup;
  domesticWorkers = [];
  selectedItemIndex = -1;
  countryCodes = countryCodes;
  selectedTypeOfWorkersName = "";
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  pageHeading = 'Domestic Workers and Garden Services';
  currentStep;

  constructor(private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private dialog: MatDialog, private rxjsService: RxjsService, private store: Store<AppState>, private router: Router
  ) {
    this.combineLatestNgrxStoreData();
  }

  ngOnInit(): void {
    this.createDomesticWorkersForm();
    this.createCheckBoxGroup();
    this.onDomesticWorkerTypeChanges();
    this.getDomesticWorkersById();
    this.onFormControlChanges();
  }

  onFormControlChanges(): void {
    this.domesticWorkersForm.get('identityNo').valueChanges.subscribe((identityNo: string) => {
      if(identityNo){
      if (this.domesticWorkersArray.find(d => d.identityNo == identityNo) && this.selectedItemIndex == -1) {
        this.domesticWorkersForm.get('identityNo').setErrors({ duplicate: true });
      }
      else if (this.selectedItemIndex == -1) {
        this.domesticWorkersForm = removeFormControlError(this.domesticWorkersForm, 'duplicate');
      }
    }
    });

  }

  onBlur() {
    if (this.domesticWorkersArray.find(d => d.identityNo == this.domesticWorkersForm.get('identityNo').value) && this.selectedItemIndex == -1) {
      if(this.domesticWorkersForm.get('identityNo').value){
        this.domesticWorkersForm.get('identityNo').setErrors({ duplicate: true });
      }
    }
    else if (this.selectedItemIndex == -1) {
      this.domesticWorkersForm = removeFormControlError(this.domesticWorkersForm, 'duplicate');
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTypeOfWorkersState$),
      this.store.select(dealerCustomerServiceAgreementDataState$)
    ]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.domesticWorkers = response[1];
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[2]);
      this.currentAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['agreementTabName'].toLowerCase().includes('domestic'));
      this.currentStep = this.currentAgreementTabObj.indexNo;
    });
  }

  createCheckBoxGroup(): void {
    let checkboxGroup = new FormArray(this.tempWorkingDays.map(item => new FormGroup({
      name: new FormControl(item.name),
      isChecked: new FormControl(item.isChecked),
    })));
    let hiddenControl = new FormControl(this.mapItems(checkboxGroup.value), Validators.required);
    checkboxGroup.valueChanges.subscribe((v) => {
      hiddenControl.setValue(this.mapItems(v));
    });
    this.domesticWorkersForm.addControl("tempWorkingDays", checkboxGroup);
    this.domesticWorkersForm.addControl("selectedItems", hiddenControl);
    this.domesticWorkersForm.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  }

  mapItems(items) {
    let selectedItems = items.filter((item) => item.isChecked).map((item) => item.name);
    return selectedItems.length ? selectedItems : null;
  }

  onDomesticWorkerTypeChanges(): void {
    this.domesticWorkersForm.get('domesticWorkerTypeId').valueChanges.subscribe((domesticWorkerTypeId: string) => {
      if (domesticWorkerTypeId == "" || !domesticWorkerTypeId) return;
      this.getTempWorkingDaysArray.markAsUntouched();
      this.selectedDomesticTypeWorkerName = this.domesticWorkers.find(d => d['id'] == domesticWorkerTypeId).displayName;
      this.domesticWorkersForm.get('domesticWorkerTypeName').patchValue(this.selectedDomesticTypeWorkerName);
      switch (this.selectedDomesticTypeWorkerName) {
        case "Domestic Employees":
          this.domesticWorkersForm = addFormControls(this.domesticWorkersForm, [{ controlName: "isSleepIn", defaultValue: false }]);
          break;
        case "Gardener":
          this.domesticWorkersForm = addFormControls(this.domesticWorkersForm, [{ controlName: "isSleepIn", defaultValue: false }]);
          break;
        case "Gardening Service":
          this.domesticWorkersForm = removeFormControls(this.domesticWorkersForm, ["isSleepIn"]);
          break;
        case "Gardener Details":
          this.domesticWorkersForm = removeFormControls(this.domesticWorkersForm, ["isSleepIn"]);
          break;
        default:
          this.domesticWorkersForm = removeFormControls(this.domesticWorkersForm, ["isSleepIn"]);
          break;
      }
    });
  }

  getDomesticWorkersById(): void {
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.GET_DOMESTIC_WORKERS, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined,
        {
          customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId,
          addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId,
          partitionId: this.dealerCustomerServiceAgreementParamsDataModel.partitionId,
          isExist: this.dealerCustomerServiceAgreementParamsDataModel.isExist,
        }), 1).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            response.resources.forEach((domesticWorkersAddEditModel: DealerDomesticWorkersAddEditModel) => {
              domesticWorkersAddEditModel.isSleepIn = (domesticWorkersAddEditModel.isSleepIn.toString().toLowerCase() === "true") ? true :
                (domesticWorkersAddEditModel.isSleepIn.toString().toLowerCase() === "false") ? false : '-';
            });
            response.resources.forEach((resource) => {
              resource.tempGUID = Guid.create()['value'];
            })
            this.domesticWorkersArray = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  createDomesticWorkersForm(): void {
    let domesticWorkersAddEditModel = new DealerDomesticWorkersAddEditModel();
    this.domesticWorkersForm = this.formBuilder.group({});
    Object.keys(domesticWorkersAddEditModel).forEach((key) => {
      if (key === 'tempWorkingDays') return;
      this.domesticWorkersForm.addControl(key,
        new FormControl(domesticWorkersAddEditModel[key]));
    });
    this.domesticWorkersForm = setRequiredValidator(this.domesticWorkersForm, ["domesticWorkerTypeId", "employeeName",
    ]);
  }

  onChange(event): void {
    let workingDaysObj = this.tempWorkingDays.find(w => w.name === event.source.value);
    workingDaysObj.isChecked = event.checked ? true : false;
  }

  focusInAndOutFormArrayFields(): void {
    setTimeout(() => {
      this.arrayList.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    }, 100)
  }

  get getTempWorkingDaysArray() {
    if (!this.domesticWorkersForm) return;
    return this.domesticWorkersForm.get('tempWorkingDays') as FormArray;
  }

  addItem(): void {
    this.getTempWorkingDaysArray.markAsTouched();
    if (this.domesticWorkersForm.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };

    let workingDaysCommaSeperated = [];
    this.getTempWorkingDaysArray.controls.forEach((control: AbstractControl) => {
      if (control.value['isChecked']) {
        workingDaysCommaSeperated.push(control.value);
      }
    });
    let days = workingDaysCommaSeperated.map(obj => obj.name).join();
    this.domesticWorkersForm.patchValue({ days });
    if (!this.domesticWorkersForm.value.tempGUID) {
      this.domesticWorkersForm.value.tempGUID = Guid.create.toString();
      this.domesticWorkersArray.push(this.domesticWorkersForm.value);
    }
    else {
      let foundIndex = this.domesticWorkersArray.findIndex((domesticWorkersObj: DealerDomesticWorkersAddEditModel) => domesticWorkersObj.tempGUID == this.domesticWorkersForm.value.tempGUID);
      this.domesticWorkersArray[foundIndex] = this.domesticWorkersForm.value;
    }
    this.domesticWorkersForm.reset(new DealerDomesticWorkersAddEditModel(), { emitEvent: false, onlySelf: true });
    this.selectedItemIndex = -1;
  }

  onItemClicked(index: number, domesticWorkersAddEditModel?: DealerDomesticWorkersAddEditModel): void {
    this.selectedItemIndex = index;
    if (domesticWorkersAddEditModel.isSleepIn == true || domesticWorkersAddEditModel.isSleepIn == false) {
      domesticWorkersAddEditModel.isSleepIn =
        (domesticWorkersAddEditModel.isSleepIn.toString().toLowerCase() === "true") ? true :
          (domesticWorkersAddEditModel.isSleepIn.toString().toLowerCase() === "false") ? false : '-';
    }
    else {
      domesticWorkersAddEditModel.isSleepIn = '-';
    }
    this.domesticWorkersForm.patchValue(domesticWorkersAddEditModel);
    this.getTempWorkingDaysArray.controls.forEach((control: AbstractControl) => {
      let daysSplitArray = !Array.isArray(domesticWorkersAddEditModel.days) ? domesticWorkersAddEditModel.days :
        domesticWorkersAddEditModel.days.join();
      if (daysSplitArray.includes(control.value['name'])) {
        control.patchValue({ isChecked: true });
      }
      else {
        control.patchValue({ isChecked: false });
      }
    });
    this.focusInAndOutFormArrayFields();
  }

  removeItem(index: number, domesticWorkersObj?: DealerDomesticWorkersAddEditModel) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (domesticWorkersObj.domesticWorkerId) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.SALES_API_DOMESTIC_WORKERS, undefined,
          prepareRequiredHttpParams({
            ids: domesticWorkersObj.domesticWorkerId,
            isDeleted: true,
            modifiedUserId: this.dealerCustomerServiceAgreementParamsDataModel.modifiedUserId
          }), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.rxjsService.setFormChangeDetectionProperty(true);
              this.domesticWorkersArray.splice(index, 1);
            }
          });
      }
      else {
        this.domesticWorkersArray.splice(index, 1);
      }
    });
  }

  onSubmit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.domesticWorkersForm = removeAllFormValidators(this.domesticWorkersForm);
    if (this.domesticWorkersArray.length === 0) {
      return;
    }
    this.domesticWorkersArray.forEach((obj) => {
      if (obj['isSleepIn'] === '-') {
        delete obj['isSleepIn'];
      }
      delete obj['selectedItems'];
      delete obj['tempWorkingDays'];
      delete obj['tempGUID'];
      obj['referenceId'] = this.dealerCustomerServiceAgreementParamsDataModel.referenceId;
      obj['customerId'] = this.dealerCustomerServiceAgreementParamsDataModel.customerId;
      obj['partitionId'] = this.dealerCustomerServiceAgreementParamsDataModel.partitionId;
      obj['addressId'] = this.dealerCustomerServiceAgreementParamsDataModel.addressId;
      obj['contactNo'] = obj['contactNo'] ? obj['contactNo'].toString().replace(/\s/g, "") : null;
      obj['createdUserId'] = this.dealerCustomerServiceAgreementParamsDataModel.createdUserId;
      obj['modifiedUserId'] = this.loggedInUserData.userId;
      if (!Array.isArray(obj['days'])) {
        obj['days'] = obj['days'].split(',');
      }
    })
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    const payload = {
      sharedDomesticWorkerList: this.domesticWorkersArray
    }
    this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DOMESTIC_WORKERS, payload)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources.sharedDomesticWorkerList) {
            response.resources.sharedDomesticWorkerList.forEach((domesticWorkersAddEditModel: DealerDomesticWorkersAddEditModel, index: number) => {
              this.domesticWorkersArray[index].domesticWorkerId = domesticWorkersAddEditModel.domesticWorkerId;
            });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToAgreementSummaryPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'next') {
        if (!this.currentAgreementTabObj.isMandatory || (this.domesticWorkersArray[0]?.domesticWorkerId &&
          this.currentAgreementTabObj.isMandatory)) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
      else if (type == 'agreement summary') {
        this.rxjsService.navigateToAgreementSummaryPage();
      }
    }
  }
}
