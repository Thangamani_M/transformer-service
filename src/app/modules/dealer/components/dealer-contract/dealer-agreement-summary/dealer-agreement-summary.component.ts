import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services/crud.service';
import { DealerHeader, DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { DealerCustomerHeaderState$ } from '../dealer-header/dealer-header-ngrx-files';
import { DealerCustomerServiceAgreementDataCreateAction } from './dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.actions';
import { dealerCustomerServiceAgreementDataState$ } from './dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
@Component({
  selector: 'app-dealer-agreement-summary',
  templateUrl: './dealer-agreement-summary.component.html',
  styleUrls: ['./../dealer-contract-list.component.scss']
})
export class DealerAgreementSummaryComponent implements OnInit {
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  customerId: string
  addressId: string
  customerData: DealerHeader
  dataList = []
  totalRecords = 0;
  agreementDetails = {
    services: [],
    dealerCustomerContracts: []
  }
  pageLimit: any = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any = {
    tableCaption: "Services",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [],
    tableComponentConfigs: {
      tabsList: [
        {
          dataKey: 'servicePriceId',
          checkBox: false,
          resizableColumns: false,
          columns: [{ field: 'serviceName', header: 'Armed Response And Monitoring' },
          { field: 'serviceCode', header: 'Service Code' },
          { field: 'servicePrice', header: 'Sub Total' },
          ]
        }]
    }
  }
  loggedInUserData: LoggedInUserModel;
  isSaveDisabled = true;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private rxjsService: RxjsService, private dialog: MatDialog, private store: Store<AppState>, private httpCancelService: HttpCancelService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getDealerAgreementDetails()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(dealerCustomerServiceAgreementDataState$), this.activatedRoute.queryParams,
      this.store.select(DealerCustomerHeaderState$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[1]);
      this.customerId = response[2].customerId;
      this.addressId = response[2].addressId;
      this.customerData = new DealerHeader(response[3])
    });
  }

  getDealerAgreementDetails() {
    // DEALER_SUMMARY_DETAILS
    this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_SUMMARY_DETAILS, null, false, prepareRequiredHttpParams({
      customerId: this.customerId,
      addressId: this.addressId,
      dealerId: this.customerData.dealerId
    })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.agreementDetails = response.resources;
        this.agreementDetails.services.map(item => {
          item.expand = false;
        });
        let count = 0;
        this.agreementDetails.dealerCustomerContracts.forEach((dealerCustomerContract) => {
          count = dealerCustomerContract.contractType.contractId ? count + 1 : count;
        });
        this.isSaveDisabled = count == this.agreementDetails.dealerCustomerContracts.length ? false : true;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }



  getAgreementTabs(contractObj) {
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_SUMMARY_TAB_DETAILS,
      undefined,
      false,
      prepareRequiredHttpParams({
        customerId: this.customerId,
        addressId: this.addressId,
        partitionId: contractObj.partitionId == null ? '' : contractObj.partitionId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          if (this.customerData.isContractCreated) {
            const message = `Do you want to copy the existing details?`;
            const dialogData = new ConfirmDialogModel("Confirm Action", message);
            const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
              maxWidth: "400px",
              data: dialogData,
              disableClose: true
            });
            dialogRef.afterClosed().subscribe(dialogResult => {
              this.dealerCustomerServiceAgreementParamsDataModel.isExist = dialogResult ? true : false;
              this.prepareNgrxDataForAgreementTabs(response, contractObj);
            });
          }
          else {
            this.dealerCustomerServiceAgreementParamsDataModel.isExist = false;
            this.prepareNgrxDataForAgreementTabs(response, contractObj);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  prepareNgrxDataForAgreementTabs(response: IApplicationResponse, contractObj) {
    this.dealerCustomerServiceAgreementParamsDataModel.referenceId = this.customerData.dealerId;
    this.dealerCustomerServiceAgreementParamsDataModel.partitionId = contractObj.partitionId ? contractObj.partitionId : "";
    this.dealerCustomerServiceAgreementParamsDataModel.customerId = this.customerData.customerId;
    this.dealerCustomerServiceAgreementParamsDataModel.customerName = this.customerData.customerName;
    this.dealerCustomerServiceAgreementParamsDataModel.fullAddress = this.customerData.fullAddress;
    this.dealerCustomerServiceAgreementParamsDataModel.siteTypeName = this.customerData.siteTypeName;
    this.dealerCustomerServiceAgreementParamsDataModel.customerRefNo = this.customerData.customerRefNo;
    this.dealerCustomerServiceAgreementParamsDataModel.addressId = this.customerData.addressId;
    this.dealerCustomerServiceAgreementParamsDataModel.agreementTabs = this.prepareAgreementTabs(response);
    this.dealerCustomerServiceAgreementParamsDataModel.agreementTabsCopy = this.removeNonRequiredServiceAgreementTabs(response);
    this.dealerCustomerServiceAgreementParamsDataModel.modifiedUserId = this.loggedInUserData.userId;
    this.dealerCustomerServiceAgreementParamsDataModel.createdUserId = this.loggedInUserData.userId;
    this.store.dispatch(new DealerCustomerServiceAgreementDataCreateAction({ dealerCustomerServiceAgreementDataModel: this.dealerCustomerServiceAgreementParamsDataModel }));
    let firstServiceAgreementTabObj = response.resources.find(s => s['indexNo'] == 1);
    this.router.navigateByUrl(firstServiceAgreementTabObj.navigationUrl);
  }

  prepareAgreementTabs(response: IApplicationResponse) {
    response.resources.forEach((tabObj) => {
      tabObj.navigationUrl = '/dealer/dealer-contract' + tabObj.navigationUrl;
    });
    return response.resources;
  }

  removeNonRequiredServiceAgreementTabs(response: IApplicationResponse): any[] {
    let requiredAgreementTabs = [];
    response.resources.forEach((agreementTab: any) => {
      if (agreementTab.agreementTabName !== "Debtor Creation" && agreementTab.agreementTabName !== "Debtor Banking Details" &&
        agreementTab.agreementTabName !== "KYC Verification" && agreementTab.agreementTabName !== "KYC Verification" &&
        agreementTab.agreementTabName !== "Customer Identification") {
        requiredAgreementTabs.push(agreementTab);
      }
    });
    requiredAgreementTabs.forEach((requiredAgreementTabObj, index) => {
      if (requiredAgreementTabObj.indexNo !== index + 1) {
        requiredAgreementTabObj.indexNo = index + 1;
      }
    });
    return requiredAgreementTabs;
  }

  loadPaginationLazy(event) { }

  onCRUDRequest(type) { }

  goBack() {
    this.router.navigate(['/dealer/dealer-contract/dealer-service-info'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
  }

  sendToDOAApproval() {
    let payload = {
      createdUserId: this.loggedInUserData.userId,
      isApproved: true,
      addressId: this.customerData.addressId,
      customerId: this.customerData.customerId,
      dealerId: this.customerData.dealerId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CUSTOMER_APPROVAL, payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  goNext() { }

}
