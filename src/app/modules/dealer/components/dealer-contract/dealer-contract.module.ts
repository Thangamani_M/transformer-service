import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ServiceSalesInstallationAgreementModule } from '@modules/sales';
import { NewAddressModule } from '@modules/sales/components/task-management/lead-info/new-address/new-address.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { GaugeChartModule } from 'angular-gauge-chart';
import { DealerAddEditComponent } from './dealer-add-edit/dealer-add-edit.component';
import { DealerAgreementSummaryComponent, dealerCustomerServiceAgreementDataReducer, DealerCustomerServiceAgreementEffects } from './dealer-agreement-summary';
import { DealerServiceAgreementHeaderComponent } from './dealer-agreement-summary/dealer-service-agreement-header/dealer-service-agreement-header.component';
import { DealerAccessToPremisesComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-access-to-premises.component';
import { DealerCellPanicComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-cell-panic.component';
import { DealerContractCreationComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-contract-creation.component';
import { DealerDomesticWorkersComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-domestic-workers.component';
import { DealerFinduComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-findu.component';
import { DealerKeyholderInfoComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-keyholder-info.component';
import { DealerOpenAndCloseMonitoringComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-open-close-monitoring.component';
import { DealerPasswordFlowComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-password-flow.component';
import { DealerSiteHazardComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-site-hazard.component';
import { DealerContractListComponent } from './dealer-contract-list.component';
import { DealerContractRoutingModule } from './dealer-contract-routing.module';
import { DealerCreditVettingResultComponent } from './dealer-credit-vetting-result/dealer-credit-vetting-result.component';
import { DealerCreditVettingModalComponent } from './dealer-credit-vetting/dealer-credit-vetting-modal.component';
import { DealerCreditVettingInfoComponent } from './dealer-credit-vetting/dealer-credit-vetting.component';
import { DealerDebtorBankingomponent } from './dealer-debtor-banking/dealer-debtor-banking.component';
import { DealerDebtorCreationComponent } from './dealer-debtor-creation/dealer-debtor-creation.component';
import { DealerCustomerHeaderReducer, DealerHeaderEffects } from './dealer-header/dealer-header-ngrx-files';
import { DealerHeaderComponent } from './dealer-header/dealer-header.component';
import { DealerLatestServiceInfoComponent } from './dealer-service/dealer-latest-service-info.component';
import { DealerPartitionPopupComponent } from './dealer-service/dealer-partition-popup.component';
import { DealerServicePopupComponent } from './dealer-service/dealer-service-popup.component';
@NgModule({
  declarations:
    [DealerContractListComponent,
      DealerAddEditComponent,
      DealerHeaderComponent,
      DealerDebtorCreationComponent,
      DealerDebtorBankingomponent,
      DealerCreditVettingInfoComponent,
      DealerCreditVettingResultComponent,
      DealerCreditVettingModalComponent,
      DealerServicePopupComponent,
      DealerPartitionPopupComponent,
      DealerLatestServiceInfoComponent,
      DealerAgreementSummaryComponent,
       DealerServiceAgreementHeaderComponent,
      DealerAccessToPremisesComponent,
      DealerKeyholderInfoComponent, DealerPasswordFlowComponent, DealerDomesticWorkersComponent, DealerOpenAndCloseMonitoringComponent,
      DealerSiteHazardComponent, DealerCellPanicComponent, DealerContractCreationComponent, DealerFinduComponent
    ],
  imports: [
    CommonModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    DealerContractRoutingModule,
    GaugeChartModule,
    StoreModule.forFeature('dealerCustomerServiceAgreementData', dealerCustomerServiceAgreementDataReducer),
    StoreModule.forFeature('dealerCustomerHeader', DealerCustomerHeaderReducer),
    EffectsModule.forFeature([DealerCustomerServiceAgreementEffects]),
    EffectsModule.forFeature([DealerHeaderEffects]),
    NewAddressModule,
    ServiceSalesInstallationAgreementModule
  ],
  entryComponents: [DealerCreditVettingModalComponent, DealerServicePopupComponent, DealerPartitionPopupComponent],
  exports: [DealerHeaderComponent,
    DealerPartitionPopupComponent]
})
export class DealerContractModule { }
