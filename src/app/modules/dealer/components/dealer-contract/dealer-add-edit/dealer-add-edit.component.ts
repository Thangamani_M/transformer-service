import {
  Component,
  ElementRef,
  EventEmitter, Output, ViewChild
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import {
  btnActionTypes, LeadCreationUserDataModel,
  SalesModuleApiSuffixModels, selectStaticEagerLoadingCustomerTypesState$,
  selectStaticEagerLoadingSiteTypesState$,
  selectStaticEagerLoadingTitlesState$
} from "@app/modules";
import { AppState } from "@app/reducers";
import {
  addFormControls,
  BreadCrumbModel,
  countryCodes,
  CrudService,
  CrudType,
  CustomDirectiveConfig,
  debounceTimeForSearchkeyword,
  destructureAfrigisObjectAddressComponents,
  disableFormControls,
  enableFormControls,
  FindDuplicatePipe,
  formConfigs,
  getFullFormatedAddress,
  HttpCancelService,
  IApplicationResponse,
  landLineNumberMaskPattern,
  LeadGroupTypes,
  LoggedInUserModel,
  mobileNumberMaskPattern,
  ModulesBasedApiSuffix, prepareRequiredHttpParams,
  removeFormControlError,
  removeFormControls,
  ResponseMessageTypes,
  RxjsService,
  setRequiredValidator,
  SnackbarService,
  validateLatLongFormat
} from "@app/shared";
import { LeafLetFullMapViewModalComponent } from "@app/shared/components/leaf-let";
import { PrimengDeleteConfirmDialogComponent } from "@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component";
import { TableFilterFormService } from "@app/shared/services/create-form.services";
import { DealerAddressModel, DealerBasicInfoModel, DealerContactInfoModel, DealerHeader, DealerLeadInfoModel, DealerModuleApiSuffixModels } from '@modules/dealer';
import { loggedInUserData } from "@modules/others";
import { NewAddressPopupComponent } from "@modules/sales";
import { LeadOutcomeStatusNames } from "@modules/sales/shared";
import { Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { combineLatest, Observable, of } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  take
} from "rxjs/operators";
import { isUndefined } from "util";
import { DealerCustomerHeaderRemoveAction, DealerCustomerHeaderState$ } from "../dealer-header/dealer-header-ngrx-files";

declare var $;
@Component({
  selector: "app-dealer-add-edit",
  templateUrl: "./dealer-add-edit.component.html",
  styleUrls: ["./dealer-add-edit.component.scss"],
})
export class DealerAddEditComponent {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({
    isAnAlphaNumericOnly: true,
  });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({
    isAlphaNumericSomeSpecialCharterOnly: true,
  });
  leadForm: FormGroup;
  rawLeadId: string;
  leadId: string;
  customerId: string;
  addressId: string;
  addressList = [];
  today: any = new Date();
  searchColumns;
  @Output() outputData = new EventEmitter<any>();
  pageSize: number = 10;
  filteredSchemes = [];
  selectedAddressOption = {};
  selectedLocationOption = {};
  selectedCommercialProducts: any;
  loggedInUserData: LoggedInUserModel;
  isSchemeMandatory: boolean;
  siteType: string;
  pageTitle: string = "Create";
  btnName = "Next";
  fromPage: string;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  lssSchemes = [];
  customerTypes = [];
  leadCategories = [];
  titles = [];
  siteTypes = [];
  latLongList = [];
  selectedColumns: [];
  selectedRows: string[] = [];
  countryCodes = countryCodes;
  customerTypeName: string;
  commercialCustomerType = "Commercial"; // edit here
  residentialCustomerType = "Residential"; // edit here
  siteTypeName: string;
  mobileNumberMaskPattern = mobileNumberMaskPattern;
  landLineNumberMaskPattern = landLineNumberMaskPattern;
  existingCustomers = [];
  selectedExistingCustomer;
  mobileNumberConfig = new CustomDirectiveConfig({
    isAValidPhoneNumberOnly: true,
  });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  valueChangesCount = 0;
  addressModalMessage = "";
  existingAddresses = [];
  installedRentedItems = [];
  existingCustomerAddress;
  selectedInstalledRentedItems = [];
  sourceTypes = [];
  isFormSubmitted = false;
  dataList = [];
  dataListCopy = [];
  dataListSecondCopy = [];
  totalRecords: any;
  loading: boolean;
  isFileExist = false;
  existingLeadsObservableResponse: IApplicationResponse;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  primengTableConfigProperties = {
    tableCaption: "Template",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [
      { displayName: "Customers", relativeRouterUrl: "" },
      { displayName: "Ticket List" },
    ],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Found Existing Customer Profile",
          dataKey: "index",
          reorderableColumns: false,
          resizableColumns: false,
          enableHyperLink: false,
          enableFieldsSearch: true,
          cursorLinkIndex: 0,
          columns: [
            { field: "isChecked", header: "Select" },
            { field: "customerRefNo", header: "Customer ID" },
            { field: "firstName", header: "First Name" },
            { field: "lastName", header: "Last Name" },
            { field: "email", header: "Email" },
            { field: "mobile1", header: "Mobile No" },
            { field: "customerTypeName", header: "Customer Type" },
            { field: "fullAddress", header: "Address" },
            // { field: "isOpenLeads", header: "Open Lead" },
          ],
        },
      ],
    },
  };
  secondPrimengTableConfigProperties = {
    tableCaption: "Template",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Address Confirmation: Reconnection",
          dataKey: "index",
          reorderableColumns: false,
          resizableColumns: false,
          enableHyperLink: false,
          enableFieldsSearch: false,
          cursorLinkIndex: 0,
          columns: [
            { field: "isChecked", header: "Select" },
            { field: "customerRefNo", header: "Customer ID" },
            { field: "firstName", header: "First Name" },
            { field: "lastName", header: "Last Name" },
            { field: "isSiteSpecific", header: "Service Status Active" },
            { field: "isRelocationNoticeRaised", header: "Relocation Raised" },
            { field: "isMovingCancellations", header: "Moving Cancellation (12 M)" },
          ],
        },
      ],
    },
  };
  fromUrl = "Leads List";
  isLeadGroupUpgrade = false;
  reconnectionMessage = "";
  selectedExistingCustomerProfile: any = { isOpenLeads: false };
  isDataFetchedFromSelectedCustomerProfile = false;
  breadCrumb: BreadCrumbModel;
  areFieldsDisabled = false;
  columnFilterForm: FormGroup;
  columnFilterFormTwo: FormGroup;
  isResetBtnDisabled = true;
  isExistingLeadModalClosed = false;
  isNewProfileBtnDisabled = false;
  leadGroupTypes = LeadGroupTypes;
  isFormChangeDetected = false;
  selectedReconnCustomerProfile = { isCreateNewlead: false, isProceedAsReconnectionLead: false };
  shouldShowLocationPinBtn = false;
  latLongObj;
  isCustomAddressSaved = false;
  existing_lead_modal = false
  reconn_new_customer_modal = false
  confirm_reconnection_modal = false
  existing_address_modal_installed_items = false
  existing_address_footprint_modal  = false
  @ViewChild("input", { static: false }) row;

  formData = new FormData();
  dealerHeaderData: DealerHeader
  constructor(
    private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {

    this.combineLatestNgrxStoreDataTwo();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(DealerCustomerHeaderState$)
    ]).subscribe(response => {
      this.dealerHeaderData = new DealerHeader(response[0])
    })
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(selectStaticEagerLoadingCustomerTypesState$),

    ])
      .pipe(take(1))
      .subscribe((response) => {

        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.titles = response[1];
        this.siteTypes = response[2];
        this.customerTypes = response[3];

      });
  }

  combineLatestNgrxStoreDataTwo() {
    combineLatest([
      this.activatedRoute.queryParams,
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.customerId = response[0]["customerId"];
        this.addressId = response[0]["addressId"];
        this.fromPage = response[0]["pageFrom"];

        if (!this.customerId) {
          this.store.dispatch(new DealerCustomerHeaderRemoveAction());
        }
        this.getRawLeadOrLeadOrCustomerById("ngOnInit");
      });
  }

  uploadFiles(event) {
    this.formData.delete('file');
    this.leadForm.controls['documentName'].patchValue(event.target.files[0].name);
    this.formData.append('file', event.target.files[0]);
  }

  ngOnInit(): void {

    this.combineLatestNgrxStoreData();
    this.combineLatestNgrxStoreDataOne();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]
        .columns
    );
    this.columnFilterFormTwo = this.tableFilterFormService.createFormGroup(
      this.secondPrimengTableConfigProperties.tableComponentConfigs.tabsList[0]
        .columns
    );
    this.columnFilterRequest();
    this.columnFilterRequest('two');
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createLeadInfoForm();
   // this.onBootstrapModalEventChanges();
    this.onFormControlChanges();
    if(!this.customerId){
      this.getDebtorDataByUser()
    }
    if (this.leadId) {
      this.pageTitle = "Update";
    }

  }



  columnFilterRequest(type = 'one') {
    let columnFilterFormGroup = type == 'one' ? this.columnFilterForm : this.columnFilterFormTwo;
    columnFilterFormGroup.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap((obj) => {
          Object.keys(obj).forEach((key) => {
            if (obj[key] === "") {
              delete obj[key];
            }
          });
          this.searchColumns = Object.entries(obj).reduce(
            (a, [k, v]) => (v == null ? a : ((a[k] = v), a)),
            {}
          ); //Filter all falsy values ( null, undefined )
          let row = {};
          row["searchColumns"] = this.searchColumns;
          return of(this.searchColumns);
        })
      )
      .subscribe((searchedColumnKeyValues) => {
        this.rxjsService.setPopupLoaderProperty(true);
        this.dataList = this.dataListSecondCopy;
        this.dataListCopy = this.dataListSecondCopy;
        if (Object.keys(searchedColumnKeyValues).length == 0) {
          this.isResetBtnDisabled = true;
          this.dataList = this.dataListSecondCopy;
          this.dataListCopy = this.dataListSecondCopy;
          this.totalRecords = this.dataListSecondCopy.length;
        } else {
          this.isResetBtnDisabled = false;
          Object.keys(searchedColumnKeyValues).forEach((key, ix) => {
            if (Object.keys(searchedColumnKeyValues).length == 1) {
              this.dataListCopy = this.filterListByKey(
                this.dataListCopy,
                key,
                searchedColumnKeyValues[key]
              );
            } else {
              this.dataListCopy = this.filterListByKey(
                this.dataListCopy,
                key,
                searchedColumnKeyValues[key]
              );
            }
            if (Object.keys(searchedColumnKeyValues).length == ix + 1) {
              this.dataList = this.dataListCopy;
              this.totalRecords = this.dataListCopy.length;
            }
          });
        }
        setTimeout(() => {
          this.rxjsService.setPopupLoaderProperty(false);
        }, 500);
      });
  }

  filterListByKey(arr, key, searchKey) {
    return arr.filter((obj) =>
      Object.keys(obj).some((nestedKey) => {
        if (key == nestedKey) {
          if (typeof obj[key] === "number") {
            return obj[key].toString().includes(searchKey);
          } else if (typeof obj[key] === "string") {
            return obj[key].toLowerCase().includes(searchKey.toLowerCase());
          }
        }
      })
    );
  }

  onResetFilter(type?: any) {
    if (type == 'modalClose') {
      return;
    }
    this.columnFilterForm.patchValue({
      customerRefNo: "",
      customerTypeName: "",
      email: "",
      firstName: "",
      fullAddress: "",
      lastName: "",
      mobile1: "",
    });
    this.selectedExistingCustomerProfile = { isOpenLeads: false };
    this.getExistingLeads('reset');
  }

  loadPaginationLazy(event) {
    this.rxjsService.setPopupLoaderProperty(true);
    if (event.sortField && event.sortOrder) {
      this.dataList.sort((a, b) => {
        let fieldA = a[event.sortField].toUpperCase(); // ignore upper and lowercase
        var fieldB = b[event.sortField].toUpperCase(); // ignore upper and lowercase
        if (event.sortOrder == 1) {
          if (fieldA < fieldB) {
            return -1;
          }
          if (fieldA > fieldB) {
            return 1;
          }
        } else {
          if (fieldB < fieldA) {
            return -1;
          }
          if (fieldB > fieldA) {
            return 1;
          }
        }
      });
    }
    setTimeout(() => {
      this.rxjsService.setPopupLoaderProperty(false);
    }, 500);
  }

  onCRUDRequested(type: CrudType | string, field?: string, row?: object): void {
    switch (type) {
      case CrudType.EDIT:
        this.reconn_new_customer_modal = false;
        this.onBootstrapModalEventChanges();
        if (field == 'customerRefNo') {
          this.router.navigate(
            ["customer/manage-customers/view/" +
              row["customerId"]], { queryParams: { addressId: this.addressId } }
          );
        }
        else if (field == 'leadId') {
          window.open(
            `/sales/task-management/lead-creation?leadId=${row["leadId"]}`,
            "_blank"
          );
        }
        break;
      case CrudType.GET:
        break;
    }
  }

  onBootstrapModalEventChanges(): void {
    if(this.existing_lead_modal){

      this.searchColumns = "";
      this.contactInfoFormGroupControls.get("email").markAsPristine();
      this.contactInfoFormGroupControls.get("mobile1").markAsPristine();
      this.contactInfoFormGroupControls.updateValueAndValidity();
      this.basicInfoFormGroupControls.get("firstName").markAsPristine();
      this.basicInfoFormGroupControls.get("lastName").markAsPristine();
      this.basicInfoFormGroupControls.updateValueAndValidity();
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.isExistingLeadModalClosed = false;
      this.dataList.map((obj) => (obj["isChecked"] = false));
      this.rxjsService.setDialogOpenProperty(true);
  }

  if(!this.existing_lead_modal){
      this.isExistingLeadModalClosed = true;
      this.addressInfoFormGroupControls.get("formatedAddress").enable({ emitEvent: false, onlySelf: true });
      setTimeout(() => {
        this.isExistingLeadModalClosed = false;
      }, 100);
      this.rxjsService.setDialogOpenProperty(false);
      this.onResetFilter('modalClose');
      }
    $("#existing_address_footprint_modal").on("shown.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#existing_address_footprint_modal").on("hidden.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(false);
    });

    if(this.reconn_new_customer_modal || this.confirm_reconnection_modal || this.existing_address_footprint_modal) {
      this.rxjsService.setDialogOpenProperty(true);
    };
    if(!this.reconn_new_customer_modal || !this.confirm_reconnection_modal || !this.existing_address_footprint_modal) {
      this.searchColumns = "";
      this.rxjsService.setDialogOpenProperty(false);
    }
  }

  createLeadInfoForm(): void {
    let leadInfoModel = new DealerLeadInfoModel();
    this.leadForm = this.formBuilder.group({});
    Object.keys(leadInfoModel).forEach((key) => {
      switch (key) {
        case "basicInfo":
          this.leadForm.addControl(key, this.createBasicInfoForm());
          break;
        case "contactInfo":
          this.leadForm.addControl(key, this.createContactInfoForm());
          break;
        case "addressInfo":
          this.leadForm.addControl(key, this.createAndGetAddressForm());
          break;
        default:
          this.leadForm.addControl(key, new FormControl(leadInfoModel[key]));
          break;
      }
    });
    this.leadForm.get('dealerName').disable()
    this.leadForm = setRequiredValidator(this.leadForm, [
      "siteTypeId", 'dealerName', 'dealerBranchCode', 'dealerCode'
    ]);
    this.leadForm.controls["basicInfo"] = setRequiredValidator(
      this.leadForm.controls["basicInfo"] as FormGroup,
      ["customerTypeId", "titleId", "firstName", "lastName"]
    );
    this.leadForm.controls["contactInfo"] = setRequiredValidator(
      this.leadForm.controls["contactInfo"] as FormGroup,
      ["email", "mobile1"]
    );
    this.leadForm.controls["addressInfo"] = setRequiredValidator(
      this.leadForm.controls["addressInfo"] as FormGroup,
      ["formatedAddress", 'latLong', "streetNo"]
    );
  }

  createBasicInfoForm(basicInfo?: DealerBasicInfoModel): FormGroup {
    let basicInfoModel = new DealerBasicInfoModel(basicInfo);
    let formControls = {};
    Object.keys(basicInfoModel).forEach((key) => {
      formControls[key] = new FormControl(basicInfoModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createContactInfoForm(contactInfo?: DealerContactInfoModel): FormGroup {
    let contactInfoModel = new DealerContactInfoModel(contactInfo);
    let formControls = {};
    Object.keys(contactInfoModel).forEach((key) => {
      formControls[key] = new FormControl(contactInfoModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createAndGetAddressForm(address?: DealerAddressModel): FormGroup {
    let addressModel = new DealerAddressModel(address);
    let formControls = {};
    Object.keys(addressModel).forEach((key) => {
      if (
        key === "streetName" ||
        key === "suburbName" ||
        key === "cityName" ||
        key === "provinceName" ||
        key === "postalCode" ||
        key === "estateStreetNo" ||
        key === "estateStreetName" ||
        key === "estateName" ||
        key === "addressConfidentLevel"
      ) {
        formControls[key] = new FormControl({
          value: addressModel[key],
          disabled: true,
        });
      } else {
        formControls[key] = new FormControl(addressModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  onFormControlChanges(): void {
    this.addressInfoFormGroupControls.get('isAfrigisSearch').valueChanges.pipe(
      debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged()).subscribe((isAfrigisSearch: boolean) => {
      this.addressInfoFormGroupControls.get('formatedAddress').setValue("");
      this.addressList = [];
    });
    // this.addressInfoFormGroupControls.get('formatedAddress').valueChanges.subscribe((formatedAddress) => {
    //   this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    // });
    // this.addressInfoFormGroupControls.get('buildingNo').valueChanges.subscribe((buildingNo) => {
    //   this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    // });
    // this.addressInfoFormGroupControls.get('buildingName').valueChanges.subscribe((buildingName) => {
    //   this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    // });
    // this.addressInfoFormGroupControls.get('streetNo').valueChanges.subscribe((streetNo) => {
    //   this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    // });
    // this.addressInfoFormGroupControls.get('estateStreetNo').valueChanges.subscribe((estateStreetNo) => {
    //   this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    // });
    // this.addressInfoFormGroupControls.get('estateStreetName').valueChanges.subscribe((estateStreetName) => {
    //   this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    // });
    // this.addressInfoFormGroupControls.get('estateName').valueChanges.subscribe((estateName) => {
    //   this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    // });
    this.getAddressListFromAfrigis();
    // this.getAddressListFromLatLongSearch();
    this.onCustomerTypeChanges();
    this.contactInfoFormGroupControls
      .get("mobile2CountryCode")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((mobile2CountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(mobile2CountryCode);
        setTimeout(() => {
          this.row.nativeElement.focus();
          this.row.nativeElement.blur();
        });
      });

    this.contactInfoFormGroupControls
      .get("mobile2")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.contactInfoFormGroupControls.get("mobile2CountryCode").value
        );
      });

    this.addressInfoFormGroupControls
      .get("isAddressComplex")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((isAddressComplex: boolean) => {
        if (!isAddressComplex) {
          this.addressInfoFormGroupControls.get("buildingNo").setValue(null);
          this.addressInfoFormGroupControls.get("buildingName").setValue(null);
        }
      });
    this.addressInfoFormGroupControls
      .get("isAddressExtra")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((isAddressExtra: boolean) => {
        if (isAddressExtra) {
          this.leadForm.controls["addressInfo"] = enableFormControls(
            this.leadForm.controls["addressInfo"] as FormGroup,
            ["estateStreetNo", "estateStreetName", "estateName"]
          );
          this.addressInfoFormGroupControls.get("estateStreetNo").enable();
          this.addressInfoFormGroupControls.get("estateStreetName").enable();
          this.addressInfoFormGroupControls.get("estateName").enable();
        } else {
          this.leadForm.controls["addressInfo"] = disableFormControls(
            this.leadForm.controls["addressInfo"] as FormGroup,
            ["estateStreetNo", "estateStreetName", "estateName"]
          );
          this.addressInfoFormGroupControls
            .get("estateStreetNo")
            .setValue(null);
          this.addressInfoFormGroupControls
            .get("estateStreetName")
            .setValue(null);
          this.addressInfoFormGroupControls.get("estateName").setValue(null);
        }
      });
    this.leadForm.get('dealerBranchId').valueChanges.pipe(
      debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged()).subscribe(val => {
      if (val) {
        let findBranch = this.dealerBranchData.find(item => item.dealerBranchId == val);
        if (findBranch) {
          this.leadForm.get("dealerBranchId").setValue(findBranch.dealerBranchId)
          this.leadForm.get("dealerBranchCode").setValue(findBranch.dealerBranchCode)
          this.leadForm.get('dealerName').setValue(findBranch.dealerBranchName);
        }
      }
    })
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.contactInfoFormGroupControls
          .get("mobile2")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.contactInfoFormGroupControls
          .get("mobile2")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  }

  onCustomerTypeChanges(): void {
    this.basicInfoFormGroupControls
      .get("customerTypeId")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((customerTypeId: string) => {
        if (!customerTypeId) return;
        this.customerTypeName = this.customerTypes.find(
          (s) => s["id"] == customerTypeId
        ).displayName;
        if (this.customerTypeName === this.commercialCustomerType) {
          this.leadForm.controls["basicInfo"] = addFormControls(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["companyRegNo", "companyName"]
          );
          this.leadForm.controls["basicInfo"] = setRequiredValidator(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["companyName"]
          );
          this.leadForm.controls["basicInfo"] = removeFormControls(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["said"]
          );
        } else {
          this.leadForm.controls["basicInfo"] = addFormControls(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["said"]
          );
          this.leadForm.controls["basicInfo"] = removeFormControls(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["companyRegNo", "companyName"]
          );
        }
      });
  }

  get addressInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get("addressInfo") as FormGroup;
  }

  get basicInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get("basicInfo") as FormGroup;
  }

  get contactInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get("contactInfo") as FormGroup;
  }

  getAddressListFromAfrigis(): void {
    var searchText: string;
    this.addressInfoFormGroupControls
      .get("formatedAddress")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap((searchKeyword) => {
          if (!searchKeyword || this.isCustomAddressSaved == true) {
            return of();
          }
          if (searchKeyword === "") {
            this.addressInfoFormGroupControls
              .get("formatedAddress")
              .setErrors({ invalid: false });
            this.addressInfoFormGroupControls
              .get("formatedAddress")
              .setErrors({ required: true });
          } else if (searchKeyword.length < 3) {
            this.addressInfoFormGroupControls.get("formatedAddress").setErrors({
              minlength: {
                actualLength: searchKeyword.length,
                requiredLength: 3,
              },
            });
          } else if (isUndefined(this.selectedAddressOption)) {
            this.addressInfoFormGroupControls
              .get("formatedAddress")
              .setErrors({ invalid: true });
          } else if (
            this.selectedAddressOption["description"] != searchKeyword &&
            this.addressInfoFormGroupControls.get("isAfrigisSearch").value
          ) {
            //this.addressInfoFormGroupControls.get('formatedAddress').setErrors({ 'invalid': true });
            this.clearAddressFormGroupValues();
          } else if (
            this.selectedAddressOption["fullAddress"] != searchKeyword &&
            !this.addressInfoFormGroupControls.get("isAfrigisSearch").value
          ) {
            this.addressInfoFormGroupControls
              .get("formatedAddress")
              .setErrors({ invalid: true });
            this.clearAddressFormGroupValues();
          }
          searchText = searchKeyword;
          if (!searchText) {
            return this.addressList;
          } else if (typeof searchText === "object") {
            return (this.addressList = []);
          } else {
            return this.filterAddressByKeywordSearch(searchText, "address");
          }
        })
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.addressList = response.resources;
          if (isUndefined(this.selectedAddressOption) && searchText !== "") {
            this.addressInfoFormGroupControls
              .get("formatedAddress")
              .setErrors({ invalid: true });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openFindPinMap(type: string) {
    this.openFullMapViewWithConfig(type);
  }

  openFullMapViewWithConfig(fromUrl) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(LeafLetFullMapViewModalComponent, {
      width: "100vw",
      maxHeight: "100vh",
      disableClose: true,
      data: {
        fromUrl: 'Two Table Architecture Custom Address',
        boundaryRequestId: null,
        boundaryRequestRefNo: null,
        isDisabled: false,
        boundaryRequestDetails: {},
        boundaries: [],
        latLong: this.latLongObj,
        shouldShowLegend: false
      },
    });
    dialogReff.afterClosed().subscribe((result) => {
      if (result?.hasOwnProperty('latLong')) {
        this.latLongObj = result.latLong;
        this.addressInfoFormGroupControls
          .get("latLong").setValue(`${result.latLong.lat.toFixed(4)}, ${result.latLong.lng.toFixed(4)}`)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getAddressListFromLatLongSearch(): void {
    var searchText: string;
    this.addressInfoFormGroupControls
      .get("latLong")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap((searchKeyword) => {
          if (!searchKeyword || searchKeyword == "-") {
            return of();
          }
          if (searchKeyword === "") {
            this.addressInfoFormGroupControls
              .get("latLong")
              .setErrors({ invalid: false });
            this.addressInfoFormGroupControls
              .get("latLong")
              .setErrors({ required: true });
          } else if (
            isUndefined(this.selectedLocationOption) ||
            validateLatLongFormat(searchKeyword)
          ) {
            this.addressInfoFormGroupControls
              .get("latLong")
              .setErrors({ invalid: true });
          }
          // else if (this.selectedLocationOption['description'] !== searchKeyword) {
          //   this.addressInfoFormGroupControls.get('latLong').setErrors({ 'invalid': true });
          //   this.clearAddressFormGroupValues();
          // }
          searchText = searchKeyword;
          if (!searchText) {
            return this.addressList;
          } else if (typeof searchText === "object") {
            return (this.addressList = []);
          } else {
            return this.filterAddressByKeywordSearch(searchText, "location");
          }
        })
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.addressList = response.resources;
          if (isUndefined(this.selectedLocationOption) && searchText !== "") {
            this.addressInfoFormGroupControls
              .get("latLong")
              .setErrors({ invalid: true });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  clearAddressFormGroupValues(): void {
    this.addressInfoFormGroupControls.patchValue({
      latitude: null,
      longitude: null,
      suburbName: null,
      cityName: null,
      provinceName: null,
      postalCode: null,
      streetName: null,
      streetNo: null,
      buildingNo: null,
      buildingName: null,
      estateName: null,
      estateStreetName: null,
      estateStreetNo: null,
    });
  }

  getExistingLeads(actionType: string = 'get'): void {
    const emailFormControl = this.contactInfoFormGroupControls.get("email");
    const mobile1FormControl = this.contactInfoFormGroupControls.get("mobile1");
    const firstNameFormControl = this.basicInfoFormGroupControls.get("firstName");
    const lastNameFormControl = this.basicInfoFormGroupControls.get("lastName");
    if (actionType == 'get') {
      if (
        emailFormControl.invalid ||
        firstNameFormControl.invalid ||
        lastNameFormControl.invalid ||
        mobile1FormControl.invalid ||
        (!emailFormControl.dirty && !mobile1FormControl.dirty && !firstNameFormControl.dirty && !lastNameFormControl.dirty) ||
        this.isExistingLeadModalClosed
      ) {
        return;
      }
    }
   // this.addressInfoFormGroupControls.get("formatedAddress").disable({ emitEvent: false, onlySelf: true });
    this.crudService
      .get(
        ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_CUSTOMER_EXISTING_DETAILS,
        null,
        null,
        prepareRequiredHttpParams({
          firstName: firstNameFormControl.value,
          lastName: lastNameFormControl.value,
          email: emailFormControl.value,
          mobile1: mobile1FormControl.value.toString().replace(/\s/g, ""),
        }),
        1
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          response.resources.forEach((resp, index) => {
            resp.index = index;
            resp.isChecked = false;
          });
          this.dataList = response.resources;
          this.dataListCopy = response.resources;
          this.dataListSecondCopy = response.resources;
          this.totalRecords = response.resources.length;
          this.isNewProfileBtnDisabled = this.totalRecords == 0 ? true : false;
          if (response.resources.length > 0) {
            this.existing_lead_modal = true
            this.onBootstrapModalEventChanges()

          }
          else {
            this.addressInfoFormGroupControls.get("formatedAddress").enable({ emitEvent: false, onlySelf: true });
          }
        }
        else {
          this.addressInfoFormGroupControls.get("formatedAddress").enable({ emitEvent: false, onlySelf: true });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onChangeCustomerSelection(rowData) {
    this.isResetBtnDisabled = false;
    this.selectedExistingCustomerProfile = rowData;
    if (
      this.contactInfoFormGroupControls.get("email").value ==
      rowData["email"] &&
      this.contactInfoFormGroupControls
        .get("mobile1")
        .value.replace(/\s/g, "") == rowData["mobile1"] &&
      this.basicInfoFormGroupControls.get("firstName").value ==
      rowData["firstName"] &&
      this.basicInfoFormGroupControls.get("lastName").value ==
      rowData["lastName"]
    ) {
      this.isNewProfileBtnDisabled = true;
    } else {
      this.isNewProfileBtnDisabled = false;
    }
    this.dataList.forEach((dataObj) => {
      dataObj["isChecked"] =
        rowData["index"] == dataObj["index"] ? true : false;
    });
  }

  onExistingCustomerPopupBtnClick(type) {
    this.leadForm.get("leadValidationType").setValue(type);
    let selectedExistingCustomerProfileCopy = JSON.parse(
      JSON.stringify(this.selectedExistingCustomerProfile)
    );

    switch (type) {
      case "Use Existing Customer":
        delete selectedExistingCustomerProfileCopy.isOpenLeads;
        this.contactInfoFormGroupControls.patchValue(
          new DealerContactInfoModel(selectedExistingCustomerProfileCopy)
        );
        this.basicInfoFormGroupControls.patchValue(
          this.selectedExistingCustomerProfile
        );
        this.leadForm
          .get("customerId")
          .setValue(this.selectedExistingCustomerProfile["customerId"]);
        this.isDataFetchedFromSelectedCustomerProfile = true;
        this.rxjsService.setFormChangeDetectionProperty(true);
        break;
      case "Use Existing Lead":
        this.isFormChangeDetected = false;
        this.router.navigate(["/sales/task-management/leads/lead-info/view"], {
          queryParams: {
            leadId: this.selectedExistingCustomerProfile["leadId"],
          },
        });
        break;
      case "Use Existing Customer Detail":
        this.isFormChangeDetected = false;
        this.router.navigate(
          ["customer/manage-customers/view/" +
            this.selectedExistingCustomerProfile["customerId"]], { queryParams: { addressId: this.addressId } }
        );
        break;
    }
    this.existing_lead_modal = false
    this.onBootstrapModalEventChanges()

  }

  actionAgainstExistingLead(): void {
    this.existing_lead_modal = false;
    this.onBootstrapModalEventChanges()

  }

  onSelectExistingCustomerAddress(existingCustomerAddressObj): void {
    this.existingCustomerAddress = existingCustomerAddressObj;
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.ADDRESS_ITEMS,
        this.existingCustomerAddress["addressId"]
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.installedRentedItems = response.resources;
          this.installedRentedItems.forEach((installedRentedItem) => {
            installedRentedItem.isChecked = false;
          });
        }
      });
  }

  onChangeInstalledItems(installedItemObj): void {
    installedItemObj.isChecked = !installedItemObj.isChecked;
    if (installedItemObj.isChecked) {
      this.selectedInstalledRentedItems.push(installedItemObj);
    } else {
      this.selectedInstalledRentedItems.splice(
        this.selectedInstalledRentedItems.indexOf(installedItemObj.itemId),
        1
      );
    }
  }

  onLinkExistingAddress(): void {
    this.existing_address_footprint_modal = false
    if (this.installedRentedItems.length > 0) {
      this.existing_address_modal_installed_items = true
    }
  }

  onSelectedInstalledItems(): void {
    this.existing_address_modal_installed_items = false
  }

  getAddressFullDetails(seoid: string): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS,
        undefined,
        false,
        prepareRequiredHttpParams({ seoid })
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.leadForm.controls["addressInfo"]
            .get("jsonObject")
            .setValue(JSON.stringify(response.resources));
          this.addressInfoFormGroupControls
            .get("addressConfidentLevel")
            .setValue(response.resources.addressConfidenceLevel);
          response.resources.addressDetails.forEach((addressObj) => {
            this.addressInfoFormGroupControls
              .get("addressConfidentLevelId")
              .setValue(addressObj.confidence);
            this.patchAddressFormGroupValues(
              "address",
              addressObj,
              addressObj["address_components"]
            );
          });
          this.addressInfoFormGroupControls.updateValueAndValidity();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  patchAddressFormGroupValues(
    type: string,
    addressObj,
    addressComponents: Object[]
  ) {
    addressObj.geometry.location.lat = addressObj.geometry.location.lat
      ? addressObj.geometry.location.lat
      : "";
    addressObj.geometry.location.lng = addressObj.geometry.location.lng
      ? addressObj.geometry.location.lng
      : "";

    if (addressObj.geometry.location.lat && addressObj.geometry.location.lng) {
      addressObj.geometry.location.latLong = `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`;
    } else {
      addressObj.geometry.location.latLong = "";
    }

    let {
      suburbName,
      cityName,
      provinceName,
      postalCode,
      streetName,
      streetNo,
      buildingNo,
      buildingName,
      estateName,
      estateStreetName,
      estateStreetNo,
    } = destructureAfrigisObjectAddressComponents(addressComponents);
    if (type == "address") {
      this.addressInfoFormGroupControls.patchValue(
        {
          latitude: addressObj.geometry.location.lat,
          longitude: addressObj.geometry.location.lng,
          latLong: addressObj.geometry.location.latLong,
          buildingName,
          buildingNo,
          estateName,
          estateStreetName,
          estateStreetNo,
          suburbName,
          cityName,
          provinceName,
          postalCode,
          streetName,
          streetNo,
        },
        { emitEvent: false, onlySelf: true }
      );
    }
  }

  onSelectedItemOption(
    isSelected: boolean,
    type: string,
    selectedObject: object
  ): void {
    if (isSelected) {
      if (type == "address") {
        if (selectedObject["seoid"]) {
          this.leadForm.controls["addressInfo"]
            .get("seoid")
            .setValue(selectedObject["seoid"]);
          this.getAddressFullDetails(selectedObject["seoid"]);
          setTimeout(() => {
            const addressList = this.addressList.filter(
              (bt) => bt["seoid"] === selectedObject["seoid"]
            );
            if (addressList.length > 0) {
              this.selectedAddressOption = addressList[0];
            }
          }, 200);
        } else {
          // selectedObject['longitude'] = '';
          // selectedObject['latitude'] = '';
          selectedObject["longitude"] = selectedObject["longitude"]
            ? selectedObject["longitude"]
            : "";
          selectedObject["latitude"] = selectedObject["latitude"]
            ? selectedObject["latitude"]
            : "";
          if (selectedObject["latitude"] && selectedObject["longitude"]) {
            selectedObject[
              "latLong"
            ] = `${selectedObject["latitude"]}, ${selectedObject["longitude"]}`;
          } else {
            selectedObject["latLong"] = "";
          }
          this.addressInfoFormGroupControls.patchValue(
            {
              seoid: "",
              jsonObject: "",
              latitude: selectedObject["latitude"],
              longitude: selectedObject["longitude"],
              latLong: selectedObject["latLong"],
              suburbName: selectedObject["suburbName"],
              cityName: selectedObject["cityName"],
              provinceName: selectedObject["provinceName"],
              postalCode: selectedObject["postalCode"],
              streetName: selectedObject["streetName"],
              streetNo: selectedObject["streetNo"],
              estateName: selectedObject["estateName"],
              estateStreetName: selectedObject["estateStreetName"],
              estateStreetNo: selectedObject["estateStreetNo"],
              buildingName: selectedObject["buildingName"],
              buildingNo: selectedObject["buildingNo"],
              addressId: selectedObject["addressId"],
              addressConfidentLevel:
                selectedObject["addressConfidentLevelName"],
              addressConfidentLevelId:
                selectedObject["addressConfidentLevelId"],
            },
            { emitEvent: false, onlySelf: true }
          );
          setTimeout(() => {
            const addressList = this.addressList.filter(
              (bt) => bt["addressId"] === selectedObject["addressId"]
            );
            if (addressList.length > 0) {
              this.selectedAddressOption = addressList[0];
            }
          }, 200);
        }
      } else {
        setTimeout(() => {
          const latLongList = this.latLongList.filter(
            (ll) => ll["seoid"] === selectedObject["seoid"]
          );
          if (latLongList.length > 0) {
            this.selectedLocationOption = latLongList[0];
          }
        }, 200);
      }
    }
  }

  filterAddressByKeywordSearch(
    searchtext: string,
    type: string
  ): Observable<IApplicationResponse> {
    if (type == "address") {
      if (searchtext.length < 3) {
        return of();
      }
      return this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS,
        null,
        true,
        prepareRequiredHttpParams({
          searchtext,
          isAfrigisSearch: this.addressInfoFormGroupControls.get(
            "isAfrigisSearch"
          ).value,
        })
      );
    } else {
      return this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS,
        null,
        true,
        prepareRequiredHttpParams({
          searchtext,
          isAfrigisSearch: this.addressInfoFormGroupControls.get(
            "isAfrigisSearch"
          ).value,
        }),
        1
      );
    }
  }

  getRawLeadOrLeadOrCustomerById(type: string): void {
    if (!this.customerId) return;
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService
      .get(
        ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_CUSTOMER_DETAILS,
        undefined,
        false,
        prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.addressId })
      )
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200) {

          if (resp.resources?.basicInfo?.customerTypeId) {
            this.customerTypeName = this.customerTypes.find(
              (s) => s["id"] == resp.resources.basicInfo.customerTypeId
            ).displayName;
          }
          if (this.customerTypeName == this.residentialCustomerType && resp.resources?.basicInfo?.said) {
            this.leadForm.controls["basicInfo"].get('said').disable();
          }
          else if (this.customerTypeName == this.commercialCustomerType) {
            if (resp.resources?.basicInfo?.companyRegNo) {
              this.leadForm.controls["basicInfo"].get('companyRegNo').disable();
            }
            if (resp.resources?.basicInfo?.companyName) {
              this.leadForm.controls["basicInfo"].get('companyName').disable();
            }
          }
          // end of disable fields once created and when come for an update
          if (this.customerId) {
            this.isDataFetchedFromSelectedCustomerProfile = true;
          }
          let leadInfoModel = new DealerLeadInfoModel(resp.resources);
          leadInfoModel.siteTypeId = resp.resources.siteTypeId;
          leadInfoModel.documentName = resp.resources.documentName;
          leadInfoModel.documentName = resp.resources.documentName;
          leadInfoModel.dealerName = resp.resources.dealerName;
          leadInfoModel.dealerId = resp.resources.dealerId;
          // leadInfoModel.dealerCode =  resp.resources.dealerCode
          leadInfoModel.dealerCode = { "dealerName": resp.resources.dealerCode }
          // leadInfoModel.branchCode =  {"dealerBranchCode":resp.resources.dealerBranchCode}
          leadInfoModel.dealerBranchCode = resp.resources.dealerBranchCode
          this.getDealerBranchByCode(resp.resources.dealerId)
          leadInfoModel.dealerCustomerDocumentId = resp.resources.dealerCustomerDocumentId;
          if (leadInfoModel.documentName != null) {
            this.isFileExist = true;
          }
          if (this.customerId) {
            if (
              leadInfoModel.addressInfo.latitude &&
              leadInfoModel.addressInfo.longitude
            ) {
              leadInfoModel.addressInfo.latLong =
                leadInfoModel.addressInfo.latitude +
                "," +
                leadInfoModel.addressInfo.longitude;
            }
          } else {
            leadInfoModel.addressInfo.latitude = leadInfoModel?.addressInfo
              ?.latitude
              ? leadInfoModel.addressInfo.latitude
              : " ";
            leadInfoModel.addressInfo.longitude = leadInfoModel?.addressInfo
              ?.longitude
              ? leadInfoModel.addressInfo.longitude
              : " ";

            leadInfoModel.addressInfo = new DealerAddressModel(
              leadInfoModel.addressInfo
            );
          }
          this.leadForm.patchValue(leadInfoModel, {
            emitEvent: false,
            onlySelf: true,
          });

          this.basicInfoFormGroupControls
            .get("customerTypeId")
            .setValue(leadInfoModel.basicInfo.customerTypeId);
          if (this.rawLeadId && resp.resources.basicInfo.titleId == 0) {
            this.basicInfoFormGroupControls.get("titleId").setValue(null);
          }
          let contactInfo = new DealerContactInfoModel(leadInfoModel.contactInfo);
          if (this.rawLeadId) {
            if (resp.resources.addressInfo.buildingNo) {
              resp.resources.addressInfo.isAddressComplex = true;
            }
            if (resp.resources.addressInfo.estateStreetNo) {
              resp.resources.addressInfo.isAddressExtra = true;
            }
          }
          this.contactInfoFormGroupControls.patchValue(contactInfo);
          if (!this.customerId) {
            this.addressInfoFormGroupControls.patchValue(
              resp.resources.addressInfo,
              { emitEvent: false, onlySelf: true }
            );
            this.addressInfoFormGroupControls
              .get("isAddressComplex")
              .setValue(resp.resources.addressInfo.isAddressComplex, { emitEvent: false, onlySelf: true });
            this.addressInfoFormGroupControls
              .get("isAddressExtra")
              .setValue(resp.resources.addressInfo.isAddressExtra, { emitEvent: false, onlySelf: true });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  validateAtleastOneFieldToBeMandatory(type: string): void {
    if (type === "contactInfo") {
      if (
        !this.contactInfoFormGroupControls.get("mobile2").value &&
        !this.contactInfoFormGroupControls.get("officeNo").value &&
        !this.contactInfoFormGroupControls.get("premisesNo").value
      ) {
        this.setAtleastOneFieldRequiredError(type);
      } else {
        this.leadForm.controls["contactInfo"] = removeFormControlError(
          this.leadForm.controls["contactInfo"] as FormGroup,
          "atleastOneOfTheFieldsIsRequired"
        );
      }
    } else {
      if (
        (this.addressInfoFormGroupControls.get("isAddressComplex").value &&
          !this.addressInfoFormGroupControls.get("buildingNo").value &&
          !this.addressInfoFormGroupControls.get("buildingName").value) ||
        (
          this.addressInfoFormGroupControls.get("isAddressExtra").value &&
          !this.addressInfoFormGroupControls.get("estateName").value &&
          !this.addressInfoFormGroupControls.get("estateStreetName").value &&
          !this.addressInfoFormGroupControls.get("estateStreetNo").value
        )
      ) {
        this.setAtleastOneFieldRequiredError(type);
      }
      else {
        this.leadForm.controls["addressInfo"] = removeFormControlError(
          this.leadForm.controls["addressInfo"] as FormGroup,
          "atleastOneOfTheFieldsIsRequired"
        );
      }
    }
  }

  setAtleastOneFieldRequiredError(type: string): void {
    if (type === "contactInfo") {
      this.contactInfoFormGroupControls
        .get("mobile2")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.contactInfoFormGroupControls
        .get("officeNo")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.contactInfoFormGroupControls
        .get("premisesNo")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
    } else {
      if (this.addressInfoFormGroupControls.get("isAddressComplex").value) {
        this.addressInfoFormGroupControls
          .get("buildingNo")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
        this.addressInfoFormGroupControls
          .get("buildingName")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
        // this.addressInfoFormGroupControls
        //   .get("streetNo")
        //   .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      }
      if (this.addressInfoFormGroupControls.get("isAddressExtra").value) {
        this.addressInfoFormGroupControls
          .get("estateName")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
        this.addressInfoFormGroupControls
          .get("estateStreetNo")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
        this.addressInfoFormGroupControls
          .get("estateStreetName")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      }
    }
  }

  onChangeReconnNewCustomerSelection(selectedReconnCustomerProfile) {
    this.selectedReconnCustomerProfile = selectedReconnCustomerProfile;
    this.dataList.forEach((data) => {
      data.isChecked = data.index == selectedReconnCustomerProfile.index ? true : false;
    });
  }

  onSubmit(type = 'others'): void {
    this.isFormSubmitted = true;
    if (type == 'new customer reconnection') {
      this.isFormChangeDetected = true;
      this.leadForm.get('isReconnectionAddressConfirmation').setValue(true);
      this.rxjsService.setDialogOpenProperty(false);
      this.reconn_new_customer_modal = false;
    }
    this.confirm_reconnection_modal = false;

    this.validateAtleastOneFieldToBeMandatory("contactInfo");
    this.validateAtleastOneFieldToBeMandatory("addressInfo");
    const isMobile1Duplicate = new FindDuplicatePipe().transform(
      this.contactInfoFormGroupControls.get("mobile1").value,
      this.contactInfoFormGroupControls,
      "mobile1"
    );
    const isMobile2Duplicate = new FindDuplicatePipe().transform(
      this.contactInfoFormGroupControls.get("mobile2").value,
      this.contactInfoFormGroupControls,
      "mobile2",
      this.contactInfoFormGroupControls.get("mobile2CountryCode").value
    );
    const isOfficeNoDuplicate = new FindDuplicatePipe().transform(
      this.contactInfoFormGroupControls.get("officeNo").value,
      this.contactInfoFormGroupControls,
      "officeNo"
    );
    const isPremisesNoDuplicate = new FindDuplicatePipe().transform(
      this.contactInfoFormGroupControls.get("premisesNo").value,
      this.contactInfoFormGroupControls,
      "premisesNo"
    );
    if (
      this.leadForm.invalid ||
      isMobile1Duplicate ||
      isMobile2Duplicate ||
      isOfficeNoDuplicate ||
      isPremisesNoDuplicate
    ) {
      this.leadForm.get("leadCategoryId").markAllAsTouched();
      return;
    }
    let addressFormGroupRawValues = this.addressInfoFormGroupControls.getRawValue();
    if (
      !addressFormGroupRawValues.provinceName ||
      !addressFormGroupRawValues.suburbName ||
      !addressFormGroupRawValues.cityName
    ) {
      this.snackbarService.openSnackbar(
        "Province Name / Suburb Name / City Name is required..!!",
        ResponseMessageTypes.ERROR
      );
      return;
    }
    this.leadForm.value.createdUserId = this.loggedInUserData.userId;
    this.leadForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.leadForm.value.leadGroupId = this.leadForm.value.leadGroupId ? this.leadForm.value.leadGroupId : this.leadGroupTypes.NEW; //remove later
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.leadForm.value.addressInfo = (this.leadForm.controls['addressInfo'] as FormGroup).getRawValue();
    this.leadForm.value.contactInfo = (this.leadForm.controls['contactInfo'] as FormGroup).getRawValue();
    this.leadForm.value.basicInfo = (this.leadForm.controls['basicInfo'] as FormGroup).getRawValue();
    if (!this.contactInfoFormGroupControls.get("mobile2").value) {
      delete this.leadForm.value.contactInfo.mobile2CountryCode;
    }
    if (!this.contactInfoFormGroupControls.get("premisesNo").value) {
      delete this.leadForm.value.contactInfo.premisesNoCountryCode;
    }
    if (!this.contactInfoFormGroupControls.get("officeNo").value) {
      delete this.leadForm.value.contactInfo.officeNoCountryCode;
    }
    this.leadForm.value.contactInfo.mobile1 = this.leadForm.value.contactInfo.mobile1
      .toString()
      .replace(/\s/g, "");
    if (this.leadForm.value.contactInfo.mobile2) {
      this.leadForm.value.contactInfo.mobile2 = this.leadForm.value.contactInfo.mobile2
        .toString()
        .replace(/\s/g, "");
    }
    if (this.leadForm.value.contactInfo.officeNo) {
      this.leadForm.value.contactInfo.officeNo = this.leadForm.value.contactInfo.officeNo
        .toString()
        .replace(/\s/g, "");
    }
    if (this.leadForm.value.contactInfo.premisesNo) {
      this.leadForm.value.contactInfo.premisesNo = this.leadForm.value.contactInfo.premisesNo
        .toString()
        .replace(/\s/g, "");
    }
    if (this.leadId !== "" && this.leadId) {
      this.leadForm.value.isReconnectionLead = false;
    } else {
      this.leadForm.value.isReconnectionLead =
        this.reconnectionMessage !== "" ? true : false;
    }
    this.leadForm.value.customerId = this.leadForm.value.customerId
      ? this.leadForm.value.customerId
      : this.customerId;
    this.leadForm.value.afrigisAddressInfo = {
      buildingName: this.leadForm.value.buildingName,
      buildingNo: this.leadForm.value.buildingNo,
      streetNo: this.leadForm.value.streetNo,
    };
    if (this.leadForm.value.addressInfo.latLong) {
      this.leadForm.value.addressInfo.latitude = this.leadForm.value.addressInfo.latLong.split(
        ","
      )[0];
      this.leadForm.value.addressInfo.longitude = this.leadForm.value.addressInfo.latLong.split(
        ","
      )[1];
    }
    let obj = this.leadForm.getRawValue();
    this.leadForm.value.dealerBranchCode = obj.dealerBranchCode
    this.leadForm.value.dealerBranchId = obj.dealerBranchId
    this.leadForm.value.dealerCode = obj.dealerCode.dealerName
    this.leadForm.value.dealerName = obj.dealerName
    this.formData.delete('data')
    this.formData.append('data', JSON.stringify(this.leadForm.value))
    // return;
    let crudService: Observable<IApplicationResponse> = this.crudService.create(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CUSTOMER,
      this.formData,
    );

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.customerId = response.resources.customerId;
        this.addressId = response.resources.addressId;
        // this.customerId = response.resources;
        // this.addressId = '1fd505c7-4312-481c-a58c-96d002ecaca0';

        // this.btnName = "Next";
        this.nextOrPrevStepper("submit");
      } else if (
        !response.isSuccess &&
        response.statusCode == 409 &&
        response.message &&
        response.message.includes("Lead This address exists in")
      ) {
        this.confirm_reconnection_modal = true
        this.onBootstrapModalEventChanges()
        this.reconnectionMessage = response.message;
      } else if (
        !response.isSuccess &&
        response.statusCode == 409 &&
        response.exceptionMessage &&
        response.exceptionMessage.includes(
          "Already customer profile exist, Please link customer"
        )
      ) {
        this.existing_lead_modal = true
        this.onBootstrapModalEventChanges()
        response.resources.forEach((resp, index) => {
          resp.index = index;
        });
        this.dataList = [];
        this.dataList = response.resources;
        this.selectedExistingCustomerProfile = this.dataList.find(
          (d) => d["isChecked"]
        );
        this.isNewProfileBtnDisabled = true;
      }
      else if (
        !response.isSuccess &&
        response.statusCode == 409 &&
        response.exceptionMessage &&
        response.exceptionMessage.includes(
          "List of Customer Associated with this Address"
        )
      ) {
      this.reconn_new_customer_modal = true;
      this.onBootstrapModalEventChanges()
        response.resources.forEach((dataObj, index) => {
          dataObj["index"] = index;
          dataObj["isChecked"] = false;
        });
        this.dataList = response.resources;
      }
    });
  }

  nextOrPrevStepper(type?: string): void {
    if (type === btnActionTypes.SUBMIT) {
      this.navigateToDealerDebtorPage();
    }
    if (type === btnActionTypes.PREVIOUS) {
      this.navigateToList();
    }
  }

  navigateToDealerDebtorPage() {
    this.router.navigate(["/dealer/dealer-contract/debtor-creation"], {
      queryParams: { customerId: this.customerId, addressId: this.addressId, update: true },
    });
  }

  navigateToList(): void {
    this.router.navigate(["/dealer/dealer-contract"]);
  }

  openAddressPopup(): void {
    this.leadForm.value.createdUserId = this.loggedInUserData.userId;
    this.leadForm.value.modifiedUserId = this.loggedInUserData.userId;
    let leadForm = { ...this.leadForm.value, ...this.leadForm.getRawValue() };
    const dialogReff = this.dialog.open(NewAddressPopupComponent, {
      width: "750px",
      disableClose: true,
      data: leadForm,
    });

    dialogReff.afterClosed().subscribe((result) => {
      if (result) {
        this.isCustomAddressSaved = false;
      }
    });
    dialogReff.componentInstance.outputData.subscribe((result) => {
      if (result) {
        this.isCustomAddressSaved = true;
        this.shouldShowLocationPinBtn = result.isAddressType == false ? true : false;
        this.addressInfoFormGroupControls.patchValue({
          formatedAddress: getFullFormatedAddress(result),
          addressConfidentLevelId: result.addressConfidentLevelId,
          // addressId:result.addressId,
          suburbName: result.suburbName,
          cityName: result.cityName,
          provinceName: result.provinceName,
          postalCode: result.postalCode,
          streetName: result.streetName,
          streetNo: result.streetNo,
          buildingNo: result.buildingNo,
          buildingName: result.buildingName,
          estateName: result.estateName,
          estateStreetName: result.estateStreetName,
          estateStreetNo: result.estateStreetNo,
        });
        this.rxjsService.setFormChangeDetectionProperty(true);
      }
      else {
        this.isCustomAddressSaved = false;
      }
    });

  }

  getDebtorDataByUser() {
    this.crudService.get(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CUSTOMER_CODE, null, false, prepareRequiredHttpParams({userId: this.loggedInUserData?.userId })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          if(response.resources.length !=0){
            this.selectedDebtor(response.resources[0])
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  dealerData = [];
  dealerBranchData = []
  filterSingle(type, event) {
    this.crudService.get(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CUSTOMER_CODE, null, false, prepareRequiredHttpParams({ dealerCode: event.query, userId: this.loggedInUserData?.userId })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dealerData = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  getDealerBranchByCode(dealerId) {
    this.crudService.get(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CUSTOMER_BRANCH, null, false, prepareRequiredHttpParams({ dealerId: dealerId, userId: this.loggedInUserData?.userId })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dealerBranchData = response.resources;
          if (this.dealerBranchData.length == 1) {
            this.leadForm.get("dealerBranchCode").setValue(this.dealerBranchData[0].dealerBranchCode)
            this.leadForm.get("dealerBranchId").setValue(this.dealerBranchData[0].dealerBranchId)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  selectedDebtor(event) {
    this.leadForm.get('dealerCode').setValue({ dealerName: event.dealerName });
    this.leadForm.get('dealerId').setValue(event.dealerId);
    this.leadForm.get('dealerCustomerDocumentId').setValue(event.dealerId);
    this.getDealerBranchByCode(event.dealerId)
  }
  selectBranchCode() {
    let _id = this.leadForm.get("dealerBranchId").value;
    let data = this.dealerBranchData.find(item => item.dealerBranchId == _id)
    if (data) {
      this.leadForm.get("dealerBranchId").setValue(data.dealerBranchId)
      this.leadForm.get("dealerBranchCode").setValue(data.dealerBranchCode)
    }
  }

  deleteDocument() {

    let reqObject = {
      dealerCustomerDocumentId: this.leadForm.get('dealerCustomerDocumentId').value,
      customerId: this.customerId,
      modifiedUserId: this.loggedInUserData?.userId
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        dataObject: reqObject,
        modifiedUserId: this.loggedInUserData?.userId,
        moduleName: ModulesBasedApiSuffix.DEALER,
        apiSuffixModel: DealerModuleApiSuffixModels.DEALER_CUSTOMER_DOCUMENT,
        method: 'delete'
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.getRawLeadOrLeadOrCustomerById('');
        this.isFileExist = false;
      }
    });

  }

}
