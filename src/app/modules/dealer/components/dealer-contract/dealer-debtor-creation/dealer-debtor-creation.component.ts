
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, selectStaticEagerLoadingTitlesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, removeFormControlError, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { DealerDebterInfoModel } from '@modules/dealer';
import { DebterListComponent, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { DealerCustomerHeaderState$ } from '../dealer-header/dealer-header-ngrx-files';

@Component({
  selector: 'app-debtor-creation',
  templateUrl: './dealer-debtor-creation.component.html'
})

export class DealerDebtorCreationComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  debtorForm: FormGroup;
  loggedUser: LoggedInUserModel;
  titles = []
  customerId: string
  addressId: string
  siteTypeId: any
  communityId: any
  contractTypeId = 1
  isDisableTechnicalDebtor = false
  disableFlag = false;
  details: any = {}
  serviceAgreementStepsParams: any
  isSubmited = false
  customerDetails: any
  debtorId  = ""
  constructor(
    private router: Router,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private store: Store<AppState>, private rxjsService: RxjsService) {

    this.activatedRoute.queryParams.subscribe(param => {
      this.customerId = param.customerId
      this.siteTypeId = param.customerTypeId
      this.addressId = param.addressId
      this.communityId = param.communityId
    })

  }

  ngOnInit(): void {
    this.getLatestData()
    this.createDebterForm();

    this.getdebtorDetailsById(1)
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getLatestData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(DealerCustomerHeaderState$)
    ])
      .subscribe((response) => {
        this.loggedUser = new LoggedInUserModel(response[0]);
        this.titles = response[1];
        this.customerDetails = response[2];
      });
  }

  createDebterForm() {
    let serviceDebterModel = new DealerDebterInfoModel();
    this.debtorForm = this.formBuilder.group({});
    Object.keys(serviceDebterModel).forEach((key) => {
      this.debtorForm.addControl(key,
        new FormControl(serviceDebterModel[key]));
    });
    this.debtorForm = setRequiredValidator(this.debtorForm, ["titleId", "firstName",
      "lastName", "email", "addressLine1", "addressLine2", "postalCode", "mobile1",]);
    this.debtorForm.get('premisesNoCountryCode').setValue('+27')
  }


  getdebtorDetailsById(contractTypeId: number) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.DEALER_DEBTORS, undefined, false, prepareGetRequestHttpParams(null, null, {
      customerId: this.customerId,
      addressId: this.addressId,
      contractTypeId: contractTypeId

    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        if (this.details.customerAddress) {
          this.details.customerAddress.addressLine1 = this.details.customerAddress.addressLine1 ? this.details.customerAddress.addressLine1.replace(/,/g, ' ') : '';
          this.details.customerAddress.addressLine2 = this.details.customerAddress.addressLine2 ? this.details.customerAddress.addressLine2.replace(/,/g, ' ') : '';
          this.details.customerAddress.addressLine3 = this.details.customerAddress.addressLine3 ? this.details.customerAddress.addressLine3.replace(/,/g, ' ') : '';
          this.details.customerAddress.addressLine4 = this.details.customerAddress.addressLine4 ? this.details.customerAddress.addressLine4.replace(/,/g, ' ') : '';
        }
        this.debtorForm.get('debtorTypeId').setValue(response.resources.debtorTypeId);
        if (this.details.debtorDetails) {
          this.debtorForm.patchValue(this.details.debtorDetails);
        }
        let check = this.debtorForm.value.isExistingDebtor;
        if (check) {
          this.debtorForm.get('premisesNoCountryCode').setValue('+27')
          this.debtorForm.get('mobile2CountryCode').setValue('+27')
          this.debtorForm.get('mobile1CountryCode').setValue('+27')
          this.debtorForm.get('officeNoCountryCode').setValue('+27')

          this.debtorForm.get('isExistingDebtor').setValue(true)
        } else {
          this.disableFlag = false;
          this.debtorForm.get('premisesNoCountryCode').setValue('+27')
          this.debtorForm.get('mobile2CountryCode').setValue('+27')
          this.debtorForm.get('mobile1CountryCode').setValue('+27')
          this.debtorForm.get('officeNoCountryCode').setValue('+27')
        }
        if (this.details.basicInfo) {
          this.debtorForm.patchValue(this.details.basicInfo);
        }
        if (this.details.address) {
          this.debtorForm.patchValue(this.details.address);
        }
        if ((this.details?.debtorDetails && this.isSubmited) && (this.details?.debtorDetails?.isTechnicalDebtorSameAsServiceDebtor || this.details.isTechnicalDebtoDataAdded)) {
          this.router.navigate(['/dealer/dealer-contract/debtor-banking-creation'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      }
    });

  }

  isDuplicate: boolean = false;
  onChange(str) {
    this.isDuplicate = false;
    let contact1 = this.debtorForm.value.mobile1;
    let contact2 = this.debtorForm.value.mobile2;
    let contact3 = this.debtorForm.value.premisesNo;
    if(contact1 || contact3 || contact2){
    if (str == 'contact1') {
      if ((contact1 == contact3) || (contact1 == contact2)) {
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
      } else {
        this.isDuplicate = false;
      }
    } else if (str == 'contact2') {
      if ((contact2 == contact1) || (contact2 == contact1)) {
        this.isDuplicate = true;
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
      } else {
        this.isDuplicate = false;
      }
    } else {
      if ((contact3 == contact1) || (contact3 == contact2)) {
        this.isDuplicate = true;
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
      } else {
        this.isDuplicate = false;
      }
    }
  }
}

  onFormControlChanges() {
    this.debtorForm.get('debtorTypeId').valueChanges.subscribe((debtorTypeId: number) => {
      if (!this.disableFlag && !this.debtorForm.value.isExistingDebtor) {
        if (debtorTypeId == 1) {
          this.debtorForm = setRequiredValidator(this.debtorForm, ["companyRegNo", "companyName"]);
          if (this.debtorForm.value.isDebtorSameAsCustomer) {
            this.debtorForm.get('companyName').setValue(this.details.customerBasicInfo.companyName)
            this.debtorForm.get('companyRegNo').setValue(this.details.customerBasicInfo.companyRegNo)
          }
          this.debtorForm = setRequiredValidator(this.debtorForm, ["companyRegNo", "companyName"]);
          this.debtorForm = clearFormControlValidators(this.debtorForm, ["said"]);
        } else if (debtorTypeId == 2) {
          if (this.debtorForm.value.isDebtorSameAsCustomer) {
            this.debtorForm.get('said').setValue(this.details.customerBasicInfo.said)
          }
          this.debtorForm = clearFormControlValidators(this.debtorForm, ["companyRegNo", "companyName"]);
          this.debtorForm = setRequiredValidator(this.debtorForm, ["said"]);
        }
      }
    });
  }

  changeIsServiceDebtorIsSame() {
    let isTechnicalDebtorSameAsServiceDebtor = this.debtorForm.value.isTechnicalDebtorSameAsServiceDebtor;
    if (isTechnicalDebtorSameAsServiceDebtor) {

      this.isDisableTechnicalDebtor = true
      if (this.contractTypeId == 2) {
        this.getdebtorDetailsById(1)
      }
      this.contractTypeId = 1;
    } else {
      this.isDisableTechnicalDebtor = false
    }
  }
  debtorChange: boolean = false;
  changeIsDebtorSame() {
    this.debtorChange = true;
    let isDebtorSameAsCustomer = this.debtorForm.value.isDebtorSameAsCustomer;
    if (isDebtorSameAsCustomer) {
      this.debtorForm.patchValue({
        titleId: this.details.customerBasicInfo.titleId,
        isExistingDebtor: false,
        firstName: this.details.customerBasicInfo.firstName,
        email: this.details.customerBasicInfo.email,
        lastName: this.details.customerBasicInfo.lastName,
        mobile1: this.details.customerBasicInfo.mobile1,
        mobile1CountryCode: this.details.customerBasicInfo.mobile1CountryCode ? this.details.customerBasicInfo.mobile1CountryCode : '+27',
        mobile2CountryCode: this.details.customerBasicInfo.mobile2CountryCode ? this.details.customerBasicInfo.mobile2CountryCode : '+27',
        mobile2: this.details.customerBasicInfo.mobile2,
        officeNoCountryCode: this.details.customerBasicInfo.officeNoCountryCode ? this.details.customerBasicInfo.officeNoCountryCode : '+27',
        officeNo: this.details.customerBasicInfo.officeNo,
        premisesNoCountryCode: this.details.customerBasicInfo.premisesNoCountryCode ? this.details.customerBasicInfo.premisesNoCountryCode : '+27',
        premisesNo: this.details.customerBasicInfo.premisesNo,
        contactId: this.details.customerBasicInfo.contactId,
        said: this.details.customerBasicInfo.said,
        debtorTypeId: this.details.debtorTypeId ? this.details.debtorTypeId : 1,
        debtorId: this.details.customerBasicInfo.debtorId,
        debtorContactId: this.details.customerBasicInfo.debtorContactId,
        debtorAdditionalInfoId: this.details.customerBasicInfo.debtorAdditionalInfoId
      });
    } else {
      this.debtorForm.patchValue({
        titleId: "",
        firstName: "",
        email: "",
        lastName: "",
        mobile2: "",
        mobile1: "",
        officeNo: "",
        premisesNo: "",
        contactId: null,
        // said: "",
        // debtorId: "",
        debtorContactId: "",
        debtorAdditionalInfoId: "",
      });
    }
  }

  changeIsBillingAddress() {
    let isBillingSameAsSiteAddress = this.debtorForm.value.isBillingSameAsSiteAddress;
    if (isBillingSameAsSiteAddress) {
      this.debtorForm.patchValue({
        addressLine1: this.details.customerAddress.addressLine1,
        addressLine2: this.details.customerAddress.addressLine2,
        addressLine3: this.details.customerAddress.addressLine3,
        addressLine4: this.details.customerAddress.addressLine4,
        postalCode: this.details.customerAddress.postalCode,
      })
    } else {
      this.debtorForm.patchValue({
        addressLine1: "",
        addressLine2: "",
        addressLine3: "",
        addressLine4: "",
        postalCode: "",
      })
    }
  }

  changeIsExisting() {
    this.debtorChange = true;
    let check = this.debtorForm.value.isExistingDebtor;
    if (check) {
      this.debtorForm.reset();
      this.debtorForm.get('premisesNoCountryCode').setValue('+27')
      this.debtorForm.get('titleId').setValue("")
      this.debtorForm.get('mobile2CountryCode').setValue('+27')
      this.debtorForm.get('mobile1CountryCode').setValue('+27')
      this.debtorForm.get('officeNoCountryCode').setValue('+27')
      this.debtorForm.get('isDebtorSameAsCustomer').setValue(false)
      this.debtorForm.get('isExistingDebtor').setValue(true)
      this.debtorForm = removeFormControlError(this.debtorForm as FormGroup, 'invalid');
    } else {
      this.disableFlag = false;
      this.debtorForm.reset();
      this.debtorForm.get('titleId').setValue("")
      this.debtorForm.get('premisesNoCountryCode').setValue('+27')
      this.debtorForm.get('officeNoCountryCode').setValue('+27')
      this.debtorForm.get('mobile2CountryCode').setValue('+27')
      this.debtorForm.get('mobile1CountryCode').setValue('+27')
      this.debtorForm.get('debtorTypeId').setValue(this.details.debtorTypeId)
    }
  }

  dataFromLinkDEbtor: any
  getDepterIdDetails(id, debtorAccountDetailId) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_DEBTER_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, {
      debtorId: id,
      debtorAccountDetailId: debtorAccountDetailId,
      isLinkDebtor: true
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataFromLinkDEbtor = response.resources;
        this.disableFlag = true;
        if (this.details.address) {
          this.dataFromLinkDEbtor.addressLine1 = this.dataFromLinkDEbtor.addressLine1 ? this.dataFromLinkDEbtor.addressLine1.replace(/,/g, ' ') : '';
          this.dataFromLinkDEbtor.addressLine2 = this.dataFromLinkDEbtor.addressLine2 ? this.dataFromLinkDEbtor.addressLine2.replace(/,/g, ' ') : '';
          this.dataFromLinkDEbtor.addressLine3 = this.dataFromLinkDEbtor.addressLine3 ? this.dataFromLinkDEbtor.addressLine3.replace(/,/g, ' ') : '';
          this.dataFromLinkDEbtor.addressLine4 = this.dataFromLinkDEbtor.addressLine4 ? this.dataFromLinkDEbtor.addressLine4.replace(/,/g, ' ') : '';
        }
        this.debtorForm.patchValue(response.resources);
        if (this.dataFromLinkDEbtor.premisesNo == 0) {
          this.debtorForm.get('premisesNo').setValue('')
        }
        this.debtorForm.get('premisesNoCountryCode').setValue('+27')
        this.debtorForm.get('officeNoCountryCode').setValue('+27')
        this.debtorForm.get('isExistingDebtor').setValue(true);
        this.debtorForm.get('isBillingSameAsSiteAddress').setValue(true);
        let check = this.debtorForm.value.isExistingDebtor;
        if (check) {
          this.debtorForm.get('premisesNoCountryCode').setValue('+27')
          this.debtorForm.get('mobile2CountryCode').setValue('+27')
          this.debtorForm.get('mobile1CountryCode').setValue('+27')
          this.debtorForm.get('officeNoCountryCode').setValue('+27')
          this.debtorForm.get('isExistingDebtor').setValue(true)
        } else {
          this.disableFlag = false;
          this.debtorForm.reset();
          this.debtorForm.get('premisesNoCountryCode').setValue('+27')
          this.debtorForm.get('mobile2CountryCode').setValue('+27')
          this.debtorForm.get('mobile1CountryCode').setValue('+27')
          this.debtorForm.get('officeNoCountryCode').setValue('+27')
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  // openModal() {
  //   const dialogReff = this.dialog.open(SearchDebterComponent, { width: '960px', disableClose: true });
  //   dialogReff.afterClosed().subscribe(result => {
  //   });
  //   dialogReff.componentInstance.outputData.subscribe(ele => {
  //     if (ele) {
  //       this.openDebterExistModal(ele);
  //     }
  //   })
  // }

  openDebterExistModal(data) {
    const dialogReff = this.dialog.open(DebterListComponent, { width: '960px', data: data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData1.subscribe(ele => {
      if (ele) {
        this.debtorId = ele.debtorId
      }
    })
  }


  onArrowClick(action) {

  }

  onChangeContractType(id) {
    this.contractTypeId = id;
    this.resetForm(this.debtorForm);
    // this.createDebterForm()
    // this.debtorForm.get('addressLine1').setErrors(null);
    // this.debtorForm.get('addressLine2').setErrors(null);
    // this.debtorForm.get('said').setErrors(null);
    // this.debtorForm.get('titleId').setErrors(null);
    // this.debtorForm.get('firstName').setErrors(null);
    // this.debtorForm.get('lastName').setErrors(null);
    // this.debtorForm.get('email').setErrors(null);
    // this.debtorForm.get('postalCode').setErrors(null);
    // this.debtorForm.get('mobile1').setErrors(null);
    // this.debtorForm.updateValueAndValidity()
    this.getdebtorDetailsById(id)
    this.debtorForm.get('siteTypeId').setValue(this.customerDetails.siteTypeId)
    this.debtorForm.get('dealerId').setValue(this.customerDetails.dealerId)
    this.debtorForm.get('isTechnicalDebtorSameAsServiceDebtor').setValue(false)



  }
  resetForm(form: FormGroup) {
    form.reset();
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
      form.get(key).updateValueAndValidity()
    });
  }

  onSubmit() {
    this.debtorForm.get('siteTypeId').setValue(this.customerDetails.siteTypeId)
    this.debtorForm.get('dealerId').setValue(this.customerDetails.dealerId)
    if (this.debtorForm.value.debtorTypeId == 1) {
      this.debtorForm = setRequiredValidator(this.debtorForm, ["companyRegNo", "companyName"]);
      this.debtorForm = clearFormControlValidators(this.debtorForm, ["said"]);
    } else if (this.debtorForm.value.debtorTypeId == 2) {
      this.debtorForm = clearFormControlValidators(this.debtorForm, ["companyRegNo", "companyName"]);
      this.debtorForm = setRequiredValidator(this.debtorForm, ["said"]);
    }
    if (this.debtorForm.invalid) {
      return;
    }
    let obj = this.debtorForm.getRawValue()
    obj.createdUserId = this.loggedUser?.userId;
    obj.contractTypeId = this.contractTypeId
    obj.customerId = this.customerId;
    obj.debtorTypeId = this.debtorForm.value.debtorTypeId
    obj.addressId = this.addressId
    // obj.siteTypeId = 1;
    // obj.dealerId = 'f8d72670-9ca7-4176-b5cf-5053c4aba5cf';


    if (typeof obj.mobile1 == 'string') {
      obj.mobile1 = obj.mobile1.split(" ").join("")
    }
    if (typeof obj.mobile2 == 'string') {
      obj.mobile2 = obj.mobile2.split(" ").join("")
    }
    if (typeof obj.premisesNo == 'string') {
      obj.premisesNo = obj.premisesNo.split(" ").join("")
    }

    if (typeof obj.officeNo == 'string') {
      obj.officeNo = obj.officeNo.split(" ").join("")
    }


    if (!obj.isBillingSameAsSiteAddress) {
      obj.isBillingSameAsSiteAddress = false;
    }
    if (!obj.isDebtorSameAsCustomer) {
      obj.isDebtorSameAsCustomer = false;
      obj.contactId = null;
    } else {
      obj.contactId = this.details.customerBasicInfo ? this.details.customerBasicInfo.contactId : null;

    }
    if (!obj.isEmailCommunication) {
      obj.isEmailCommunication = false;
    }
    if (!obj.isExistingDebtor) {
      obj.isExistingDebtor = false;
    }
    if (!obj.isPhoneCommunication) {
      obj.isPhoneCommunication = false;
    }
    if (!obj.isPostCommunication) {
      obj.isPostCommunication = false;
    }
    if (!obj.isSMSCommunication) {
      obj.isSMSCommunication = false;
    }
    if (this.debtorChange) {
      obj.debtorId = null;
      obj.debtorRefNo = null;
      obj.bdiNumber = null;
      obj.billingAddressId = null;
      obj.debtorContactId = this.details.debtorDetails ? this.details.debtorDetails.debtorContactId : null;
      obj.debtorAdditionalInfoId = this.details.debtorDetails ? this.details.debtorDetails.debtorAdditionalInfoId : null;
      obj.debtorAccountDetailId = null
      obj.contactId = this.details.customerBasicInfo ? this.details.customerBasicInfo.contactId : null;;

    } else {
      obj.debtorId = this.details?.debtorDetails ? this.details?.debtorDetails?.debtorId : null;
      obj.debtorContactId = this.details?.debtorDetails ? this.details?.debtorDetails?.debtorContactId : null;
      obj.debtorAdditionalInfoId = this.details?.debtorDetails ? this.details?.debtorDetails?.debtorAdditionalInfoId : null;
      obj.debtorAccountDetailId = this.details?.debtorDetails ? this.details?.debtorDetails?.debtorAccountDetailId : null;
    }
    if (obj.isExistingDebtor && this.dataFromLinkDEbtor) {
      obj.billingAddressId = this.dataFromLinkDEbtor.billingAddressId ? this.dataFromLinkDEbtor.billingAddressId : null;
      obj.isBillingSameAsSiteAddress = false;
      obj.contactId = this.dataFromLinkDEbtor.contactId ? this.dataFromLinkDEbtor.contactId : null;
      obj.debtorId = this.dataFromLinkDEbtor.debtorId ? this.dataFromLinkDEbtor.debtorId : null;
      obj.debtorRefNo = this.dataFromLinkDEbtor.debtorRefNo ? this.dataFromLinkDEbtor.debtorRefNo : null;
      obj.bdiNumber = this.dataFromLinkDEbtor.bdiNumber ? this.dataFromLinkDEbtor.bdiNumber : null;
      // obj.debtorContactId = this.dataFromLinkDEbtor.debtorContactId ? this.dataFromLinkDEbtor.debtorContactId : null;
      obj.debtorAccountDetailId = this.dataFromLinkDEbtor.debtorAccountDetailId ? this.dataFromLinkDEbtor.debtorAccountDetailId : null;
    }
    obj.debtorId  = obj.debtorId? obj.debtorId : this.debtorId
    obj.officeNoCountryCode = this.details.basicInfo ? this.details.basicInfo.officeNoCountryCode : null;
    obj.officeNoCountryCode = obj.officeNoCountryCode ? obj.officeNoCountryCode : '+27'
    obj.mobile1CountryCode = this.details.basicInfo ? this.details.basicInfo.mobile1CountryCode : null;
    obj.mobile1CountryCode = obj.mobile1CountryCode ? obj.mobile1CountryCode : '+27'
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.DEALER_DEBTORS, obj, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        if (response.isSuccess && response.statusCode === 200 && response.message.includes("SA ID already exist")) {
           this.openDebterExistModal(response.resources)
        } else {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.isSubmited = true;
          this.deceideWhatNext()
        }
      }
    });
  }


  goBack() {
    this.router.navigate(['/dealer/dealer-contract/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
  }

  deceideWhatNext() {
    if (this.debtorForm.get("isTechnicalDebtorSameAsServiceDebtor").value || this.details.isTechnicalDebtoDataAdded) {
      this.router.navigate(['/dealer/dealer-contract/debtor-banking-creation'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
    } else {
      this.contractTypeId = this.contractTypeId == 1 ? 2 : 1;
      this.onChangeContractType(this.contractTypeId)
    }
  }


}
