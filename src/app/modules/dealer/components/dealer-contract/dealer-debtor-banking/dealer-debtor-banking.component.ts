import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  ConfirmDialogModel,
  ConfirmDialogPopupComponent,
  CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, generateCurrentYearToNext99Years, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  monthsByNumber,
  prepareGetRequestHttpParams, removeAllFormValidators, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { DealerDebtorBankingDetailsModel, DealerHeader } from '@modules/dealer';
import { loggedInUserData, selectStaticEagerLoadingBillingIntervalsState$, selectStaticEagerLoadingCardTypesState$, selectStaticEagerLoadingComplimentaryMonthsState$, selectStaticEagerLoadingContractPeriodsState$, selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
import {
  SalesModuleApiSuffixModels,

} from '@modules/sales';
import { Store } from '@ngrx/store';
import { throws } from 'assert';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { DealerCustomerHeaderState$ } from '../dealer-header/dealer-header-ngrx-files';
@Component({
  selector: 'dealer-debtor-banking',
  templateUrl: './dealer-debtor-banking.component.html'
})

export class DealerDebtorBankingomponent implements OnInit {
  leadStatusCssClass: any;
  currentAgreementTabObj;
  isExistingDebtor = false;
  createNewBankAccount = false;
  details: any;
  bankSelected = true;
  isSubmited = false;
  monthArray = monthsByNumber;
  yearArray = generateCurrentYearToNext99Years();
  titleList = [];
  bankList = [];
  branchList = [];
  accountTypeList = [];
  cardTypeList = [];
  selectedBranch: any;
  debterBankingDetailsForm: FormGroup;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isAccountNumber = new CustomDirectiveConfig({ isAccountNumber: true });
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  customerData: any = {}
  customerId: string;
  addressId: string;
  contractTypeId = 1
  paymentMethod = [];
  paymentFrequency = [];
  contractPeriod = [];
  loggedInUserData: LoggedInUserModel
  paymentDates: any = []
  isShowAccount = true
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  selectedBranchOption;
  @ViewChild('cardNo', { static: true }) cardNoElement: ElementRef;
  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder,
    public rxjsService: RxjsService, private snackbarService: SnackbarService, private dialog: MatDialog, private router: Router, private activatedRoute: ActivatedRoute) {
    this.combineLatestNgrxStoreData();
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingCardTypesState$),
      this.store.select(selectStaticEagerLoadingContractPeriodsState$),
      this.store.select(selectStaticEagerLoadingBillingIntervalsState$),
      this.store.select(selectStaticEagerLoadingPaymentMethodsState$),
      this.store.select(loggedInUserData),
      this.store.select(DealerCustomerHeaderState$)
    ]
    ).subscribe((response) => {
      this.titleList = response[0];
      this.cardTypeList = response[1];
      this.contractPeriod = response[2];
      this.paymentFrequency = response[3];
      this.paymentMethod = response[4];
      this.loggedInUserData = new LoggedInUserModel(response[5]);
      this.customerData = new DealerHeader(response[6]);
    });
  }

  setDefaultSelect(){
    let _contractPeriod = this.contractPeriod.find(item => item.displayName == "36 Months");
    let _paymentFrequency = this.paymentFrequency.find(item => item.displayName == "Monthly");
    let _paymentMethod = this.paymentMethod.find(item => item.displayName == "Debit Order");

    this.debterBankingDetailsForm.get("contractPeriodId").setValue(_contractPeriod?.id);
    this.debterBankingDetailsForm.get("billingIntervalId").setValue(_paymentFrequency?.id);
    this.debterBankingDetailsForm.get("paymentMethodId").setValue(_paymentMethod?.id);
    this.debterBankingDetailsForm.get("paymentDate").setValue(0);

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.generatePaymentDate()
    this.getBankList();
    this.getAccountTypeList();
    this.createDebterForm();
    // if (this.serviceAgreementStepsParams.debtorAdditionalInfoId) {
    this.getDebtorBankingDetailsById(1);
    // }
    this.onFormControlChanges();
    if (this.debterBankingDetailsForm.value.isBankAccount) {
      this.debterBankingDetailsForm.get('isCardSelected').setValue(false);
      this.debterBankingDetailsForm.get('isBankAccount').setValue(true);
      this.debterBankingDetailsForm.get('cardTypeId').disable();
      this.debterBankingDetailsForm.get('cardNo').disable();
      this.debterBankingDetailsForm.get('selectedMonth').disable();
      this.debterBankingDetailsForm.get('selectedYear').disable();
      this.debterBankingDetailsForm.get('cardTypeId').clearValidators();
      this.debterBankingDetailsForm.get('selectedMonth').clearValidators();
      this.debterBankingDetailsForm.get('selectedYear').clearValidators();
      this.debterBankingDetailsForm.get('cardTypeId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
      this.debterBankingDetailsForm.get('selectedMonth').updateValueAndValidity();
      this.debterBankingDetailsForm.get('selectedYear').updateValueAndValidity();
      this.debterBankingDetailsForm.get('accountNo').enable();
      this.debterBankingDetailsForm.get('bankId').enable();
      this.debterBankingDetailsForm.get('bankBranchId').enable();
      this.debterBankingDetailsForm.get('bankBranchCode').enable();
      this.debterBankingDetailsForm.get('accountTypeId').enable();
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
        ["accountNo", "bankId", "bankBranchId", "bankBranchCode", "accountTypeId"]);
    }
    // if (this.serviceAgreementStepsParams.debtorTypeId == '1') {
    //   this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
    //     ["companyName"]);
    // }
    else {
      this.debterBankingDetailsForm.get('companyName').patchValue('');
      this.debterBankingDetailsForm.get('companyName').clearValidators();
      this.debterBankingDetailsForm.get('companyName').updateValueAndValidity();
    }
  }
  getBranchesByBankIdAndBranchKeyword(): void {
    this.debterBankingDetailsForm.get('bankBranchId')
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext) {
            this.getLoopableObjectRequestObservable = of();
          }
          else {
            this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, undefined, false,
              prepareGetRequestHttpParams(null, null, { bankId: this.debterBankingDetailsForm.get('bankId').value, searchText: searchtext }));
          }
        });
  }
  onSelectedBranchOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedBranchOption = selectedObject;
    this.debterBankingDetailsForm.get('bankBranchCode').setValue(this.selectedBranchOption?.bankBranchCode)
  }

  onChangeContractType(id) {
    this.contractTypeId = id;
    this.resetForm(this.debterBankingDetailsForm)
    this.getDebtorBankingDetailsById(id)
  }

  onChangePaymentType() {
    if (this.debterBankingDetailsForm.get('paymentMethodId').value == 1) {
      this.isShowAccount = false
      this.debterBankingDetailsForm = removeAllFormValidators(this.debterBankingDetailsForm);
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
        ["contractPeriodId", "billingIntervalId", "paymentMethodId"]);
      this.debterBankingDetailsForm.updateValueAndValidity();

    } else {
      this.isShowAccount = true;
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm, ["titleId", "firstName", "lastName", "accountNo", "bankId", "bankBranchId", "accountTypeId", "cardTypeId", "cardNo", "selectedMonth", "selectedYear"]);
      this.debterBankingDetailsForm.get('cardNo').setValidators(Validators.compose([
        Validators.minLength(formConfigs.creditCardNumberWithSpaceMask),
        Validators.maxLength(formConfigs.creditCardNumberWithSpaceMask)
      ]));
      this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
    }
  }


  resetForm(form: FormGroup) {
    form.reset();
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
      form.get(key).updateValueAndValidity()
    });
  }

  accountNumberBeforeMask: string;


  kycDetails() {
    if (this.details.debtorId && this.customerId) {
      let queryParams = { queryParams: { dealerId: this.customerData.dealerId, customerId: this.customerId, addressId: this.addressId, debtorId: this.details.debtorId, debtorAccountDetailId: this.debterBankingDetailsForm.value.debtorAccountDetailId,paymentMethodId :this.debterBankingDetailsForm.get('paymentMethodId').value } };
      this.router.navigate(['/dealer/dealer-contract/debtor-credit-vetting'], queryParams)
    } else {
      this.snackbarService.openSnackbar("Debtor Or Debtor Account Details is required", ResponseMessageTypes.WARNING);
    }
  }

  viewResult() {
    if (this.details.debtorId && this.customerId) {
      let queryParams = { queryParams: { dealerId: this.customerData.dealerId, customerId: this.customerId, addressId: this.addressId, debtorId: this.details.debtorId, debtorAccountDetailId: this.debterBankingDetailsForm.value.debtorAccountDetailId , paymentMethodId :this.debterBankingDetailsForm.get('paymentMethodId').value} };
      this.router.navigate(['/dealer/dealer-contract/debtor-credit-vetting-result'], queryParams)
    } else {
      this.snackbarService.openSnackbar("Debtor Or Debtor Account Details is required", ResponseMessageTypes.WARNING);
    }
  }

  createDebterForm() {
    let debtorBankingDetailsModel = new DealerDebtorBankingDetailsModel();
    this.debterBankingDetailsForm = this.formBuilder.group({});
    Object.keys(debtorBankingDetailsModel).forEach((key) => {
      this.debterBankingDetailsForm.addControl(key,
        new FormControl(debtorBankingDetailsModel[key]));
    });
    this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm, ["titleId", "firstName", "lastName", "accountNo", "bankId", "bankBranchId", "accountTypeId", "cardTypeId", "cardNo", "selectedMonth", "selectedYear", "contractPeriodId", "paymentMethodId", "billingIntervalId"]);
    this.debterBankingDetailsForm.get('cardNo').setValidators(Validators.compose([
      Validators.minLength(formConfigs.creditCardNumberWithSpaceMask),
      Validators.maxLength(formConfigs.creditCardNumberWithSpaceMask)
    ]));
    this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
  }



  disabledKyc() {
    if (!this.createNewBankAccount) {
      if (this.details?.debtorId) {
        if (this.details?.isDebtorAccountVetted && this.details?.isDebtorVetted) {
          return false;
        } else {
          return true;
        }
      } else {
        if (this.details?.isDebtorAccountVetted) {
          return false;
        } else {
          return true;
        }
      }
    } else {
      return true;
    }
  }



  getDebtorBankingDetailsById(contractTypeId: number) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.DEALER_DEBTORS_BANKING, undefined, false, prepareGetRequestHttpParams(null, null, {
      // debtorAdditionalInfoId: this.details.debtorAdditionalInfoId ? this.details.debtorAdditionalInfoId : '',
      customerId: this.customerId,
      addressId: this.addressId,
      contractTypeId: contractTypeId
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;

        if (this.details && this.details.debtorAccountDetailId && this.details.debtorAccountDetailId) {
          if (this.details.isExistingDebtor) {
            this.isExistingDebtor = true;
          }
          if (this.details.expiryDate) {
            let expiryDate = this.details.expiryDate.split("/");
            this.details.selectedMonth = expiryDate[0];
            this.details.selectedYear = expiryDate[1];
          }
          if (this.details.cardNo) {
            let cardNo = this.details.cardNo
            let cardNumber = cardNo.match(/.{4}/g);
            this.details.cardNo = `${cardNumber[0]} ${cardNumber[1]} ${cardNumber[2]} ${cardNumber[3]}`
          }
          //

          this.debterBankingDetailsForm.get('debtorAdditionalInfoId').setValue(this.details.debtorAdditionalInfoId);
        }
        this.debterBankingDetailsForm.patchValue(this.details);
        if(!this.details.debtorAccountDetailId){
          this.setDefaultSelect()
        }
        if ((this.isSubmited && this.details) && this.details.isDebtorVetted && (this.details.isTechnicalDebtorAccountSameAsServiceDebtorAccount || this.details.isTechnicalDebtorAccountDataAdded)) {
          // this.router.navigate(['/dealer/dealer-contract/dealer-service-info'], { queryParams: { customerId: this.customerId, addressId: this.addressId, update: true, debtorAccountDetailId: this.debterBankingDetailsForm.get('debtorAccountDetailId')?.value } });
        }


        if (this.details && this.details.isBankAccount) {
          this.selectedBranchOption = { displayName: response.resources?.bankBranchName, id: response.resources?.bankBranchId };
          this.debterBankingDetailsForm.get('bankBranchId').setValue(response.resources?.bankBranchName, { emitEvent: false, onlySelf: true });

          this.bankSelected = true;
          this.debterBankingDetailsForm.get('isCardSelected').setValue(false);
          this.debterBankingDetailsForm.get('isBankAccount').setValue(true);
          this.debterBankingDetailsForm.get('cardTypeId').disable();
          this.debterBankingDetailsForm.get('cardNo').disable();
          this.debterBankingDetailsForm.get('selectedMonth').disable();
          this.debterBankingDetailsForm.get('selectedYear').disable();
          this.debterBankingDetailsForm.get('cardTypeId').clearValidators();
          this.debterBankingDetailsForm.get('selectedMonth').clearValidators();
          this.debterBankingDetailsForm.get('selectedYear').clearValidators();
          this.debterBankingDetailsForm.get('cardTypeId').updateValueAndValidity();
          this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
          this.debterBankingDetailsForm.get('selectedMonth').updateValueAndValidity();
          this.debterBankingDetailsForm.get('selectedYear').updateValueAndValidity();
          this.debterBankingDetailsForm.get('accountNo').enable();
          this.debterBankingDetailsForm.get('bankId').enable();
          this.debterBankingDetailsForm.get('bankBranchId').enable();
          this.debterBankingDetailsForm.get('bankBranchCode').enable();
          this.debterBankingDetailsForm.get('accountTypeId').enable();
          this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
            ["accountNo", "bankId", "bankBranchId", "bankBranchCode", "accountTypeId"]);

        } else {
          this.bankSelected = false;

          this.debterBankingDetailsForm.get('isBankAccount').setValue(false);
          this.debterBankingDetailsForm.get('isCardSelected').setValue(true);
          this.debterBankingDetailsForm.get('accountNo').disable();
          this.debterBankingDetailsForm.get('bankId').disable();
          this.debterBankingDetailsForm.get('bankBranchId').disable();
          this.debterBankingDetailsForm.get('bankBranchCode').disable();
          this.debterBankingDetailsForm.get('accountTypeId').disable();
          this.debterBankingDetailsForm.get('accountNo').clearValidators();
          this.debterBankingDetailsForm.get('bankId').clearValidators();
          this.debterBankingDetailsForm.get('bankBranchId').clearValidators();
          this.debterBankingDetailsForm.get('bankBranchCode').clearValidators();
          this.debterBankingDetailsForm.get('accountTypeId').clearValidators();
          this.debterBankingDetailsForm.get('accountNo').updateValueAndValidity();
          this.debterBankingDetailsForm.get('bankId').updateValueAndValidity();
          this.debterBankingDetailsForm.get('bankBranchId').updateValueAndValidity();
          this.debterBankingDetailsForm.get('bankBranchCode').updateValueAndValidity();
          this.debterBankingDetailsForm.get('accountTypeId').updateValueAndValidity();
          this.debterBankingDetailsForm.get('cardTypeId').enable();
          this.debterBankingDetailsForm.get('cardNo').enable();
          this.debterBankingDetailsForm.get('selectedMonth').enable();
          this.debterBankingDetailsForm.get('selectedYear').enable();

          setTimeout(() => {
          }, 2000);
          this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
            ["cardTypeId", "cardNo", "selectedMonth", "selectedYear"]);

        }
        this.onChangePaymentType();

        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getBankList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.SALES_API_BANKS, undefined, false, prepareGetRequestHttpParams(null, null, {
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.bankList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getAccountTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_ACCOUNTTYPE
      , undefined, false, prepareGetRequestHttpParams(null, null, {
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.accountTypeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onFormControlChanges(): void {
    this.getBranchesByBankIdAndBranchKeyword();
    this.debterBankingDetailsForm.get('bankId').valueChanges.subscribe((bankId: string) => {

      if (bankId) {
        this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, undefined, false, prepareGetRequestHttpParams(null, null, {
          bankId: bankId
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.branchList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
    });

    this.debterBankingDetailsForm.get('isBankAccount').valueChanges.subscribe((isBankAccount: boolean) => {
      if (isBankAccount) {
      }
    });

    this.debterBankingDetailsForm.get('isCardSelected').valueChanges.subscribe((isCardSelected: boolean) => {
      if (isCardSelected) {
        this.debterBankingDetailsForm.get('cardNo').setValidators(Validators.compose([
          Validators.minLength(formConfigs.creditCardNumberWithSpaceMask),
          Validators.maxLength(formConfigs.creditCardNumberWithSpaceMask)
        ]));
      }
    });
  }

  onBankSelected() {
    this.debterBankingDetailsForm.get('bankBranchId').patchValue(null);
    this.debterBankingDetailsForm.get('bankBranchCode').patchValue('');
  }

  onOptionsSelected(e) {
    this.selectedBranch = this.branchList.filter(element => {
      return element.id == e.target.value;
    })
    this.debterBankingDetailsForm.get('bankBranchCode').setValue(this.selectedBranch[0].bankBranchCode);
  }

  isSelectedCard(e, selected) {
    if (e.value && selected === 'selectedCard') {
      this.bankSelected = false;
      this.debterBankingDetailsForm.get('isBankAccount').setValue(false);
      this.debterBankingDetailsForm.get('isCardSelected').setValue(true);
      this.debterBankingDetailsForm.get('accountNo').patchValue('');
      this.debterBankingDetailsForm.get('bankId').patchValue(null);
      this.debterBankingDetailsForm.get('bankBranchId').patchValue(null);
      this.debterBankingDetailsForm.get('bankBranchCode').patchValue(null);
      this.debterBankingDetailsForm.get('accountTypeId').patchValue(null);
      this.debterBankingDetailsForm.get('accountNo').disable();
      this.debterBankingDetailsForm.get('bankId').disable();
      this.debterBankingDetailsForm.get('bankBranchId').disable();
      this.debterBankingDetailsForm.get('bankBranchCode').disable();
      this.debterBankingDetailsForm.get('accountTypeId').disable();
      this.debterBankingDetailsForm.get('accountNo').clearValidators();
      this.debterBankingDetailsForm.get('bankId').clearValidators();
      this.debterBankingDetailsForm.get('bankBranchId').clearValidators();
      this.debterBankingDetailsForm.get('bankBranchCode').clearValidators();
      this.debterBankingDetailsForm.get('accountTypeId').clearValidators();
      this.debterBankingDetailsForm.get('accountNo').updateValueAndValidity();
      this.debterBankingDetailsForm.get('bankId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('bankBranchId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('bankBranchCode').updateValueAndValidity();
      this.debterBankingDetailsForm.get('accountTypeId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('cardTypeId').enable();
      this.debterBankingDetailsForm.get('cardNo').enable();
      this.debterBankingDetailsForm.get('selectedMonth').enable();
      this.debterBankingDetailsForm.get('selectedYear').enable();
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
        ["cardTypeId", "cardNo", "selectedMonth", "selectedYear"]);
    }

    if (e.value && selected === 'selectedBank') {
      this.bankSelected = true;
      this.debterBankingDetailsForm.get('isCardSelected').setValue(false);
      this.debterBankingDetailsForm.get('isBankAccount').setValue(true);
      this.debterBankingDetailsForm.get('cardTypeId').patchValue(null);
      this.debterBankingDetailsForm.get('cardNo').patchValue('');
      this.debterBankingDetailsForm.get('selectedMonth').patchValue('');
      this.debterBankingDetailsForm.get('selectedYear').patchValue('');
      this.debterBankingDetailsForm.get('cardTypeId').disable();
      this.debterBankingDetailsForm.get('cardNo').disable();
      this.debterBankingDetailsForm.get('selectedMonth').disable();
      this.debterBankingDetailsForm.get('selectedYear').disable();
      this.debterBankingDetailsForm.get('cardTypeId').clearValidators();
      this.debterBankingDetailsForm.get('cardNo').clearValidators();
      this.debterBankingDetailsForm.get('selectedMonth').clearValidators();
      this.debterBankingDetailsForm.get('selectedYear').clearValidators();
      this.debterBankingDetailsForm.get('cardTypeId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
      this.debterBankingDetailsForm.get('selectedMonth').updateValueAndValidity();
      this.debterBankingDetailsForm.get('selectedYear').updateValueAndValidity();
      this.debterBankingDetailsForm.get('accountNo').enable();
      this.debterBankingDetailsForm.get('bankId').enable();
      this.debterBankingDetailsForm.get('bankBranchId').enable();
      this.debterBankingDetailsForm.get('bankBranchCode').enable();
      this.debterBankingDetailsForm.get('accountTypeId').enable();
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
        ["accountNo", "bankId", "bankBranchId", "bankBranchCode", "accountTypeId"]);
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onChngePaymentType() {
    let method = this.debterBankingDetailsForm.get('paymentMethodId').value;
    if (method == 2) {
      this.debterBankingDetailsForm.get('paymentDate').setValidators([Validators.required]);
      this.debterBankingDetailsForm.updateValueAndValidity();
    } else {
      this.debterBankingDetailsForm.get('paymentDate').clearAsyncValidators()
      this.debterBankingDetailsForm.get('paymentDate').setValue("");
      this.debterBankingDetailsForm.updateValueAndValidity()
    }
    this.onChangePaymentType()
  }
  addNewBankAccount() {
    const message = `Are you sure you want to add new bank account?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) {
        this.isExistingDebtor = true;
        return;
      }
      this.debterBankingDetailsForm.reset();
      this.isExistingDebtor = false;
      this.createNewBankAccount = true;
      this.debterBankingDetailsForm.get('isBankAccount').setValue(true);
      this.bankSelected = true;
    });
  }

  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.debterBankingDetailsForm.invalid) {
      return;
    }

    if (this.debterBankingDetailsForm.value.isExistingDebtor && this.createNewBankAccount) {
      this.debterBankingDetailsForm.get('debtorAccountDetailId').setValue(null);
    }

    this.debterBankingDetailsForm.value.cardNo = this.debterBankingDetailsForm.value.cardNo && this.debterBankingDetailsForm.value.cardNo.replace(/\s/g, '');
    this.debterBankingDetailsForm.value.debtorId = this.details.debtorId;
    this.debterBankingDetailsForm.value.createdUserId = this.loggedInUserData?.userId;
    this.debterBankingDetailsForm.value.customerId = this.customerId;
    this.debterBankingDetailsForm.value.paymentMethodId = +this.debterBankingDetailsForm.value.paymentMethodId
      this.debterBankingDetailsForm.value.paymentDate = +this.debterBankingDetailsForm.value.paymentDate
    this.debterBankingDetailsForm.value.addressId = this.addressId;
    this.debterBankingDetailsForm.value.contractTypeId = this.contractTypeId;

    this.debterBankingDetailsForm.value.debtorAdditionalInfoId = this.details.debtorAdditionalInfoId;
    if (this.debterBankingDetailsForm.value.isCardSelected) {
      this.debterBankingDetailsForm.value.expiryDate = `${this.debterBankingDetailsForm.value.selectedMonth}/${this.debterBankingDetailsForm.value.selectedYear}`
    }
    if (this.debterBankingDetailsForm.get('isBankAccount').value) {
      this.debterBankingDetailsForm.value.bankBranchId = this.selectedBranchOption.id;
    }

    let obj = this.debterBankingDetailsForm.value;
    obj.systemOwnershipId = this.details.systemOwnershipId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.DEALER_DEBTORS_BANKING, obj).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (!this.debterBankingDetailsForm.value.debtorAccountDetailId) {
          this.debterBankingDetailsForm.get('debtorAccountDetailId').setValue(response.resources.debtorAccountDetailId);
        }
        this.debterBankingDetailsForm.get('isOnlineDebtorAccepted').setValue(true);
        this.isSubmited = true;
        this.deceideWhatNext()
        this.getDebtorBankingDetailsById(this.contractTypeId)

      }
    })
  }

  deceideWhatNext() {

    if (this.debterBankingDetailsForm.get("isTechnicalDebtorAccountSameAsServiceDebtorAccount").value || this.details.isTechnicalDebtorAccountDataAdded) {
      if (!this.details.isDebtorVetted) {
        return this.snackbarService.openSnackbar("Please Verify Kyc before proceeding to add services", ResponseMessageTypes.WARNING);
      } else {
        this.router.navigate(['/dealer/dealer-contract/dealer-service-info'], { queryParams: { customerId: this.customerId, addressId: this.addressId, update: true, debtorAccountDetailId: this.debterBankingDetailsForm.get('debtorAccountDetailId')?.value } });
      }
    } else {
      this.contractTypeId = this.contractTypeId == 1 ? 2 : 1;
      this.onChangeContractType(this.contractTypeId);
    }
  }

  goBack() {
    this.router.navigate(['/dealer/dealer-contract/debtor-creation'], { queryParams: { customerId: this.customerId, addressId: this.addressId } });
  }

  generatePaymentDate() {
    this.paymentDates = []
    for (let index = 0; index <= 28; index++) {
      this.paymentDates.push(index);
    }
  }



}
