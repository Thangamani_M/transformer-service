import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services/crud.service';
import { DealerHeader, DealerModuleApiSuffixModels } from '@modules/dealer';
import { select, Store } from '@ngrx/store';
import { DealerCustomerHeaderCreateAction, DealerCustomerHeaderState$ } from './dealer-header-ngrx-files';

enum DealerSalesStep {
  CUSTOMER = 1,
  DEBTOR = 2,
  DEBTOR_BANKING = 3,
  SERVICE = 4,
  AGREEMENT_SUMMARY = 5
}
@Component({
  selector: 'app-shared-dealer-header',
  templateUrl: './dealer-header.component.html',
  styleUrls: ['./dealer-header.component.scss']
})
export class DealerHeaderComponent implements OnInit {

  @Input() pageHeading: string;
  @Input() showDetails: any;
  @Input() currentPage: number;
  @Input() totalSteps: number = 5;
  @Input() naviationsData: any = {};

  stepperArray = []
  customerData: any = {};
  customerId: string
  addressId: string
  update: boolean
  dealerHeader: DealerHeader;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private crudService: CrudService,  private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId
    this.update = this.activatedRoute.snapshot.queryParams.update
    this.store.pipe(select(DealerCustomerHeaderState$)).subscribe((dealerHeader: DealerHeader) => {
      this.dealerHeader = dealerHeader;
    });
  }

  // DEALER_CUSTOMER_HEADER_DETAILS
  ngOnInit(): void {
    this.decideApiCall()

    for (let index = 1; index <= this.totalSteps; index++) {
      let isActive = index <= this.currentPage
      this.stepperArray.push({
        index: index,
        active: isActive
      })
    }
  }

  decideApiCall() {
    if (!this.dealerHeader && this.customerId) {
      this.getCustomerDetails();
    } else if (this.customerId && this.customerId != this.dealerHeader.customerId) {
      this.getCustomerDetails()
    }
    if (this.update) {
      this.getCustomerDetails()
    }
    if (this.pageHeading == "CAF No :") {
      this.pageHeading = `CAF No : ${this.dealerHeader.customerRefNo}`
    }
  }

  getCustomerDetails() {
    this.crudService.get(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CUSTOMER_HEADER_DETAILS,
      null,
      false,
      prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.addressId })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.customerData = response.resources;

          if (this.pageHeading == "CAF No :") {
            this.pageHeading = `CAF No : ${this.customerData.customerRefNo}`
          }
          this.customerData.customerId = this.customerId
          this.store.dispatch(new DealerCustomerHeaderCreateAction({ DealerCustomerHeaderModel: this.customerData }));
        }
      })
  }
  navigateStpes = (page) => {
    switch (page) {
      case DealerSalesStep.CUSTOMER:
        // this.router.navigate(['/dealer/dealer-contract/add-edit'], {queryParams:{customerId :this.naviationsData.customerId, addressId: this.naviationsData.addressId}})
        break;
      case DealerSalesStep.DEBTOR:
        // this.router.navigate([])
        break;
      case DealerSalesStep.DEBTOR_BANKING:
        // this.router.navigate([])
        break;
      case DealerSalesStep.SERVICE:
        // this.router.navigate([])
        break;
      case DealerSalesStep.AGREEMENT_SUMMARY:
        // this.router.navigate([])
        break;

      default:
        break;
    }
  }
  navigateToCustomer() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }
}
