import { Injectable } from '@angular/core';
import { AppDataService } from '@app/shared/services';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { defer, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DealerCustomerHeaderActionTypes, DealerCustomerHeaderChangeAction, DealerCustomerHeaderCreateAction, DealerCustomerHeaderRemoveAction } from './dealer-customer-creation.actions';

@Injectable()
export class DealerHeaderEffects {
  @Effect({ dispatch: false })
  createData$ = this.actions$.pipe(
    ofType<DealerCustomerHeaderCreateAction>(DealerCustomerHeaderActionTypes.DealerCustomerHeaderCreateAction),
    tap(action =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = action.payload.DealerCustomerHeaderModel)
  );

  @Effect({ dispatch: false })
  updateData$ = this.actions$.pipe(
    ofType<DealerCustomerHeaderChangeAction>(DealerCustomerHeaderActionTypes.DealerCustomerHeaderChangeAction),
    tap(action =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = action.payload.DealerCustomerHeaderModel)
  );

  @Effect({ dispatch: false })
  removeData$ = this.actions$.pipe(
    ofType<DealerCustomerHeaderRemoveAction>(DealerCustomerHeaderActionTypes.DealerCustomerHeaderRemoveAction),
    tap(() =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = null
    )
  );

  @Effect()
  initUserData$ = defer(() => {
    const DealerCustomerHeaderModel = this.appDataService.dealerCustomerCreationServiceAgreementData;
    if (DealerCustomerHeaderModel) {
      return of(new DealerCustomerHeaderCreateAction({ DealerCustomerHeaderModel }));
    }
    else {
      return <any>of(new DealerCustomerHeaderRemoveAction());
    }
  });

  constructor(private actions$: Actions, private appDataService: AppDataService) {

  }

}
