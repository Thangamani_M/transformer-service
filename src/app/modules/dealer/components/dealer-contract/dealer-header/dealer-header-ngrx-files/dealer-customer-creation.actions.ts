import { DealerHeader } from '@modules/dealer/models/dealer-sales-customer.model';
import { Action } from '@ngrx/store';

enum DealerCustomerHeaderActionTypes {
  DealerCustomerHeaderCreateAction = '[Dealer Customer Creation]  Create Action',
  DealerCustomerHeaderChangeAction = '[Dealer Customer Creation] Change Action',
  DealerCustomerHeaderRemoveAction = '[[Dealer Customer Creation]  Remove Action',
}
class DealerCustomerHeaderCreateAction implements Action {
  readonly type = DealerCustomerHeaderActionTypes.DealerCustomerHeaderCreateAction;
  constructor(public payload: { DealerCustomerHeaderModel: DealerHeader }) {
  }
}
class DealerCustomerHeaderChangeAction implements Action {
  readonly type = DealerCustomerHeaderActionTypes.DealerCustomerHeaderChangeAction;
  constructor(public payload: { DealerCustomerHeaderModel: DealerHeader }) {
  }

}

class DealerCustomerHeaderRemoveAction implements Action {
  readonly type = DealerCustomerHeaderActionTypes.DealerCustomerHeaderRemoveAction;
}

type DealerCustomerHeaderActions = DealerCustomerHeaderCreateAction | DealerCustomerHeaderChangeAction | DealerCustomerHeaderRemoveAction;

export {
  DealerCustomerHeaderCreateAction, DealerCustomerHeaderChangeAction,
  DealerCustomerHeaderRemoveAction,  DealerCustomerHeaderActions, DealerCustomerHeaderActionTypes
};

