import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DealerHeader } from '@modules/dealer/models/dealer-sales-customer.model';
const DealerCustomerHeaderState = createFeatureSelector<DealerHeader>("dealerCustomerHeader");

const DealerCustomerHeaderState$ = createSelector(
  DealerCustomerHeaderState,
  DealerCustomerHeaderState => DealerCustomerHeaderState
);

export {
  DealerCustomerHeaderState$
};

