import { DealerHeader } from '@modules/dealer/models/dealer-sales-customer.model';import { DealerCustomerHeaderActions, DealerCustomerHeaderActionTypes } from './dealer-customer-creation.actions';

export function DealerCustomerHeaderReducer(state = new DealerHeader(),
  action: DealerCustomerHeaderActions): DealerHeader {
  switch (action.type) {
    case DealerCustomerHeaderActionTypes.DealerCustomerHeaderCreateAction:
      return action.payload.DealerCustomerHeaderModel;

      case DealerCustomerHeaderActionTypes.DealerCustomerHeaderChangeAction:
        return {...state,...action.payload.DealerCustomerHeaderModel};

    case DealerCustomerHeaderActionTypes.DealerCustomerHeaderRemoveAction:
      return null;

    default:
      return state;
  }
}

