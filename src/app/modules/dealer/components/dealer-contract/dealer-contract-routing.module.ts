import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerAddEditComponent } from './dealer-add-edit/dealer-add-edit.component';
import { DealerAgreementSummaryComponent } from './dealer-agreement-summary';
import { DealerAccessToPremisesComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-access-to-premises.component';
import { DealerCellPanicComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-cell-panic.component';
import { DealerContractCreationComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-contract-creation.component';
import { DealerDomesticWorkersComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-domestic-workers.component';
import { DealerFinduComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-findu.component';
import { DealerKeyholderInfoComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-keyholder-info.component';
import { DealerOpenAndCloseMonitoringComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-open-close-monitoring.component';
import { DealerPasswordFlowComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-password-flow.component';
import { DealerSiteHazardComponent } from './dealer-agreement-summary/dealer-service-agreement/dealer-site-hazard.component';
import { DealerContractListComponent } from './dealer-contract-list.component';
import { DealerCreditVettingResultComponent } from './dealer-credit-vetting-result/dealer-credit-vetting-result.component';
import { DealerCreditVettingInfoComponent } from './dealer-credit-vetting/dealer-credit-vetting.component';
import { DealerDebtorBankingomponent } from './dealer-debtor-banking/dealer-debtor-banking.component';
import { DealerDebtorCreationComponent } from './dealer-debtor-creation/dealer-debtor-creation.component';
import { DealerLatestServiceInfoComponent } from './dealer-service/dealer-latest-service-info.component';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';


const routes: Routes = [
  { path: '', component: DealerContractListComponent, canActivate:[AuthGuard],data: { title: 'Dealer Contract List' } },
  { path: 'add-edit', component: DealerAddEditComponent,canActivate:[AuthGuard], data: { title: 'Dealer Sales Add/Edit' } },
  { path: 'debtor-creation', component: DealerDebtorCreationComponent,canActivate:[AuthGuard], data: { title: 'Dealer Debtor Creation' } },
  { path: 'debtor-banking-creation', component: DealerDebtorBankingomponent,canActivate:[AuthGuard], data: { title: 'Dealer Debtor Banking' } },
  { path: 'debtor-credit-vetting', component: DealerCreditVettingInfoComponent,canActivate:[AuthGuard], data: { title: 'Dealer Credit Vetting' } },
  { path: 'debtor-credit-vetting-result', component: DealerCreditVettingResultComponent,canActivate:[AuthGuard], data: { title: 'Dealer Credit Vetting Result' } },
  { path: 'dealer-service-info', component: DealerLatestServiceInfoComponent,canActivate:[AuthGuard], data: { title: 'Dealer Service Info' } },
  { path: 'dealer-agreement-summary', component: DealerAgreementSummaryComponent,canActivate:[AuthGuard], data: { title: 'Dealer Agreement Summary' } },
  { path: 'service-agreement/access-to-premises', component: DealerAccessToPremisesComponent,canActivate:[AuthGuard], data: { title: 'Dealer Service Agreement Access To Premises' } },
  { path: 'service-agreement/keyholder-information', component: DealerKeyholderInfoComponent, canActivate:[AuthGuard],data: { title: 'Dealer Service Agreement Keyholder Information' } },
  { path: 'service-agreement/password-special-instructions', component: DealerPasswordFlowComponent,canActivate:[AuthGuard], data: { title: 'Dealer Service Agreement Password Special Instructions' } },
  { path: 'service-agreement/domestic-workers', component: DealerDomesticWorkersComponent, canActivate:[AuthGuard],data: { title: 'Dealer Service Agreement Domestic Workers' } },
  { path: 'service-agreement/open-close-monitoring', component: DealerOpenAndCloseMonitoringComponent, data: { title: 'Dealer Service Agreement Arming And Disarming' } },
  { path: 'service-agreement/site-hazard', component: DealerSiteHazardComponent, canActivate:[AuthGuard],data: { title: 'Dealer Service Agreement Site Hazard' } },
  { path: 'service-agreement/cell-panic', component: DealerCellPanicComponent, canActivate:[AuthGuard],data: { title: 'Dealer Service Agreement Cell Panic' } },
  { path: 'service-agreement/checklist', component: DealerContractCreationComponent, canActivate:[AuthGuard],data: { title: 'Dealer Service Agreement Checklist' } },
  { path: 'service-agreement/findu', component: DealerFinduComponent, canActivate:[AuthGuard],data: { title: 'Dealer Service Agreement Find U' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class DealerContractRoutingModule { }
