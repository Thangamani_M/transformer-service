import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustAppBannerListComponent } from './cust-app-banner-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: CustAppBannerListComponent, canActivate:[AuthGuard],data: { title: 'Customer App Banner Config' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})
export class CustAppBannerConfigRoutingModule { }
