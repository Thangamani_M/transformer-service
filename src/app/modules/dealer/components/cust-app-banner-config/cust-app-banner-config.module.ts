import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustAppBannerAddEditComponent, CustAppBannerListComponent } from '@modules/dealer';
import { CustAppBannerConfigRoutingModule } from './cust-app-banner-config-routing.module';

@NgModule({
    declarations: [CustAppBannerListComponent, CustAppBannerAddEditComponent],
    imports: [
        CommonModule,MaterialModule,LayoutModule,SharedModule,ReactiveFormsModule,FormsModule,
        CustAppBannerConfigRoutingModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [CustAppBannerAddEditComponent]
})
export class CustAppBannerConfigModule { }