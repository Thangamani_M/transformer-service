import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxDateValidator, setRequiredValidator, setSecValidator, SnackbarService } from '@app/shared';
import { CustAppBannerDetailModel } from '@modules/dealer/models/cust-app-banner.model';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-cust-app-banner-add-edit',
  templateUrl: './cust-app-banner-add-edit.component.html',
})
export class CustAppBannerAddEditComponent implements OnInit {

  custAppBannerDialogForm: FormGroup;
  isSubmitted: boolean;
  startTodayDate: any = new Date();
  endTodayDate: any = new Date();
  marketingTypeList = [];
  userGroupList = [];
  suburbList = [];
  provinceList = [];
  cityList = [];
  listOfFiles = [];
  provincelistSubscription: any;
  citylistSubscription: any;
  formData = new FormData();
  showBannerError: boolean;
  error: any;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
    this.provinceList =  this.config?.data?.provinceList;
    this.marketingTypeList = this.config?.data?.marketTypeList;
    if (!Object.keys(this.config?.data?.row)?.length) {
      this.startTodayDate = new Date();
      this.endTodayDate = new Date();
    }
  }

  ngAfterViewInit() {
    if (Object.keys(this.config?.data?.row)?.length) {
      setTimeout(() => {
        const startDate = this.config?.data?.row?.startDate?.split('-')[2] + '-' + this.config?.data?.row?.startDate?.split('-')[1] + '-' + this.config?.data?.row?.startDate?.split('-')[0];
        const endDate = this.config?.data?.row?.endDate?.split('-')[2] + '-' + this.config?.data?.row?.endDate?.split('-')[1] + '-' + this.config?.data?.row?.endDate?.split('-')[0];
        this.startTodayDate = new Date(startDate);
        this.endTodayDate = new Date(endDate);
        this.custAppBannerDialogForm.get('startDate').patchValue(this.startTodayDate);
        this.custAppBannerDialogForm.get('endDate').patchValue(this.endTodayDate);
      }, 500);
    }
  }

  initForm(custAppBannerFormModel?: CustAppBannerDetailModel) {
    let custAppBannerModel = new CustAppBannerDetailModel(custAppBannerFormModel);
    this.custAppBannerDialogForm = this.formBuilder.group({});
    Object.keys(custAppBannerModel).forEach((key) => {
      if (typeof custAppBannerModel[key] === 'object') {
        this.custAppBannerDialogForm.addControl(key, new FormArray(custAppBannerModel[key]));
      } else {
        this.custAppBannerDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : custAppBannerModel[key]));
      }
    });
    this.custAppBannerDialogForm = setRequiredValidator(this.custAppBannerDialogForm, ["marketingType", "marketingName", "startDate", "endDate", "refreshPeriod", "imageName"]);
    this.custAppBannerDialogForm = setMinMaxDateValidator(this.custAppBannerDialogForm, [
      { formControlName: 'startDate', compareWith: 'endDate', type: 'min' },
      { formControlName: 'endDate', compareWith: 'startDate', type: 'max' }
    ]);
    this.custAppBannerDialogForm = setSecValidator(this.custAppBannerDialogForm, ["refreshPeriod"]);
    this.onValueChanges();
  }

  onValueChanges() {
    this.custAppBannerDialogForm.get('provinceId').valueChanges
      // .pipe(debounceTime(800))
      .subscribe((res: any) => {
        if (res?.length) {
          if (typeof res == 'object') {
            res = res?.toString();
          }
          this.rxjsService.setGlobalLoaderProperty(true);
          if (this.provincelistSubscription && !this.provincelistSubscription.closed) {
            this.provincelistSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.provincelistSubscription = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, DealerModuleApiSuffixModels.MARKETING_BANNERS_CITIES, null, null, prepareRequiredHttpParams({ ProvinceId: res }))
            .subscribe((resp: IApplicationResponse) => {
              if (resp?.isSuccess && resp?.statusCode == 200) {

                this.cityList = getPDropdownData(resp.resources,"cityName","cityId");
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
      });
    this.custAppBannerDialogForm.get('cityId').valueChanges
      // .pipe(debounceTime(800))
      .subscribe((res: any) => {
        if (res?.length) {
          if (typeof res == 'object') {
            res = res?.toString();
          }
          this.rxjsService.setGlobalLoaderProperty(true);
          if (this.citylistSubscription && !this.citylistSubscription.closed) {
            this.citylistSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.citylistSubscription = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, DealerModuleApiSuffixModels.MARKETING_BANNERS_SUBURBS, null, null, prepareRequiredHttpParams({ ProvinceId: this.custAppBannerDialogForm.get('provinceId')?.value?.toString(), CityId: res }))
            .subscribe((resp: IApplicationResponse) => {
              if (resp?.isSuccess && resp?.statusCode == 200) {
                this.suburbList = getPDropdownData(resp.resources,"suburbName","suburbId");
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
      });
  }

  btnCloseClick() {
    this.custAppBannerDialogForm.reset();
    this.ref.close(false);
  }

  imageUploadFiles(event) {
    let reader = new FileReader();
    this.showBannerError = false;
    this.error = "";
    this.formData.delete('image');
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      img.src = window.URL.createObjectURL(file);
      reader.readAsDataURL(file);
      reader.onload = () => {
        const width = img.naturalWidth;
        const height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width !== 900 && height !== 600) {
          this.showBannerError = true;
          this.error = "photo should be 900 x 600 size";
          return;
        }

      };
    }
    this.custAppBannerDialogForm?.get('imageName')?.setValue(event.target.files[0].name);
    this.custAppBannerDialogForm?.get('imageURL').setValue("");
    this.formData.append('image', event?.target?.files ? event.target.files[0] : '');
  }

  onSubmitcustAppBannerDialog() {
    if (this.isSubmitted || !this.custAppBannerDialogForm?.valid || this.showBannerError) {
      this.custAppBannerDialogForm?.markAllAsTouched();
      return;
    } else if (!this.custAppBannerDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const custAppBannerObject = {
      ...this.custAppBannerDialogForm.value,
    }
    custAppBannerObject['startDate'] = this.datePipe.transform(this.custAppBannerDialogForm.get('startDate').value, 'yyyy-MM-dd');
    custAppBannerObject['endDate'] = this.datePipe.transform(this.custAppBannerDialogForm.get('endDate').value, 'yyyy-MM-dd');
    delete custAppBannerObject['userGroupId'];
    if (this.custAppBannerDialogForm.value?.bannerId) {
      custAppBannerObject['modifiedUserId'] = this.config?.data?.createdUserId;
    } else {
      custAppBannerObject['createdUserId'] = this.config?.data?.createdUserId;

    }
    this.rxjsService.setDialogOpenProperty(true);
    custAppBannerObject['isActive'] = true;
    this.formData.append('', JSON.stringify(custAppBannerObject));
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    let api = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, DealerModuleApiSuffixModels.MARKETING_BANNERS, this.formData)
    if (this.custAppBannerDialogForm.value?.bannerId) {
      api = this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, DealerModuleApiSuffixModels.MARKETING_BANNERS, this.formData);
    }
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.custAppBannerDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
      this.formData = new FormData();
    })
  }
}
