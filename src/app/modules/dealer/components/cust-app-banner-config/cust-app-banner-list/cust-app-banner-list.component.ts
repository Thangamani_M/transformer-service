import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CustAppBannerAddEditComponent } from '@modules/dealer';
import { CustAppBannerFilterModel } from '@modules/dealer/models/cust-app-banner.model';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-cust-app-banner-list',
  templateUrl: './cust-app-banner-list.component.html',
  styleUrls: ['./cust-app-banner-list.component.scss']
})
export class CustAppBannerListComponent extends PrimeNgTableVariablesModel implements OnInit {

  showFilterForm: boolean;
  observableResponse: any;
  custAppBannerFilterForm: FormGroup;
  primengTableConfigProperties: any;
  row: any = {};
  multipleSubscription: any;
  listSubscription: any;
  marketTypeListFilter = [];
  userGroupListFilter = [];
  suburbListFilter = [];
  provinceListFilter = [];
  cityListFilter = [];
  marketTypeList = [];
  provinceList = [];
  first: any = 0;
  filterData: any;
  provincelistSubscription: any;
  citylistSubscription: any;

  constructor(
    private crudService: CrudService,
    private dialogService: DialogService,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe) {
     super();
    this.primengTableConfigProperties = {
      tableCaption: "Customer App Banner Config",
      breadCrumbItems: [{ displayName: 'Marketing', relativeRouterUrl: '' }, { displayName: 'Customer App Banner Config' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Customer App Banner Config',
            dataKey: 'bannerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'marketingTypeName', header: 'Marketing Type', width: '200px' },
              { field: 'marketingName', header: 'Marketing Name', width: '200px' },
              { field: 'suburbName', header: 'Suburb', width: '200px' },
              { field: 'provinceName', header: 'Province', width: '200px' },
              { field: 'cityName', header: 'City', width: '200px' },
              { field: 'startDate', header: 'Start Date', width: '200px' },
              { field: 'endDate', header: 'End Date', width: '200px' },
              { field: 'refreshPeriod', header: 'Refresh Time (Sec)', width: '200px' },
              { field: 'imageURL', header: 'Banner URL', width: '200px', isUrlLink: true },
              { field: 'status', header: 'Status', width: '200px' },
            ],
            enableMultiDeleteActionBtn: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.MARKETING_BANNERS,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getCustAppBannerListData();
    this.combineLatestNgrxStoreData();
    this.createCustAppBanFilterForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1]['Marketing Banner']
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createCustAppBanFilterForm(custAppBannerFilterModel?: CustAppBannerFilterModel) {
    let custAppBannerModel = new CustAppBannerFilterModel(custAppBannerFilterModel);
    this.custAppBannerFilterForm = this.formBuilder.group({});
    Object.keys(custAppBannerModel).forEach((key) => {
      this.custAppBannerFilterForm.addControl(key, new FormControl(custAppBannerModel[key]));
    });
    this.onFilterValueChanges();
  }

  onFilterValueChanges() {
    this.custAppBannerFilterForm.get('ProvinceId').valueChanges
      .subscribe((res: any) => {
        if (res?.length) {
          if (typeof res == 'object') {
            res = res?.toString();
          }
          this.rxjsService.setGlobalLoaderProperty(true);
          if (this.provincelistSubscription && !this.provincelistSubscription.closed) {
            this.provincelistSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.provincelistSubscription = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, DealerModuleApiSuffixModels.MARKETING_BANNERS_CITIES, null, null, prepareRequiredHttpParams({ ProvinceId: res }))
            .subscribe((resp: IApplicationResponse) => {
              if (resp?.isSuccess && resp?.statusCode == 200) {
                this.cityListFilter = getPDropdownData(resp.resources,"cityName","cityId");
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
      });
    this.custAppBannerFilterForm.get('CityId').valueChanges
      // .pipe(debounceTime(800))
      .subscribe((res: any) => {
        if (res?.length) {
          if (typeof res == 'object') {
            res = res?.toString();
          }
          this.rxjsService.setGlobalLoaderProperty(true);
          if (this.citylistSubscription && !this.citylistSubscription.closed) {
            this.citylistSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.citylistSubscription = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, DealerModuleApiSuffixModels.MARKETING_BANNERS_SUBURBS, null, null, prepareRequiredHttpParams({ ProvinceId: this.custAppBannerFilterForm.get('ProvinceId')?.value?.toString(), CityId: res }))
            .subscribe((resp: IApplicationResponse) => {
              if (resp?.isSuccess && resp?.statusCode == 200) {
                this.suburbListFilter = getPDropdownData(resp.resources,"suburbName","suburbId");
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
      });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col)
    }
  }

  getCustAppBannerListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.listSubscription = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.startDate = this.datePipe.transform(val.startDate, 'dd-MM-yyyy');
          val.endDate = this.datePipe.transform(val.endDate, 'dd-MM-yyyy');
          val.cssClass = val.status?.toLowerCase() == 'expired' ? 'status-label-pink' : '';
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getCustAppBannerListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:

        if (unknownVar != 'imageURL' && !this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (unknownVar == 'imageURL') {
          this.openAddEditPopup('View Banner Image', row, false);
          return;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes([
      this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        DealerModuleApiSuffixModels.MARKETING_BANNERS_PROVINCES),
      this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        DealerModuleApiSuffixModels.MARKETING_TYPE)
    ], false, editableObject, type);
  }

  openAddEditPopup(header, row, isAddEdit = true) {
    const rowData = { ...row };
    let data = {
      createdUserId: this.loggedInUserData?.userId,
      row: rowData,
      isAddEdit: isAddEdit
    };
    if (isAddEdit) {
      data['provinceList'] = this.provinceList;
      data['marketTypeList'] = this.marketTypeList;
    }
    if (Object.keys(rowData)?.length) {
      rowData['marketingType'] = rowData['marketingTypeId']?.toString();
      delete rowData['marketingTypeId'];
    }
    const ref = this.dialogService.open(CustAppBannerAddEditComponent, {
      header: header,
      baseZIndex: 1000,
      width: isAddEdit ? '700px' : '80vw',
      closable: false,
      showHeader: false,
      data: data,
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getCustAppBannerListData();
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
        isDeleted: true,
        modifiedUserId: this.loggedInUserData.userId,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.getCustAppBannerListData();
      }
    });
  }

  submitFilter() {
    this.filterData = Object.assign({},
      this.custAppBannerFilterForm.get('MarketingTypeId').value.length ==0 ? null : { MarketingTypeId: this.custAppBannerFilterForm.get('MarketingTypeId').value },
      !this.custAppBannerFilterForm.get('MarketingName').value ? '' : { MarketingName: this.custAppBannerFilterForm.get('MarketingName').value },
      this.custAppBannerFilterForm.get('SuburbId').value.length ==0 ? null : { SuburbId: this.custAppBannerFilterForm.get('SuburbId').value },
      this.custAppBannerFilterForm.get('ProvinceId').value.length ==0 ? null : { ProvinceId: this.custAppBannerFilterForm.get('ProvinceId').value },
      this.custAppBannerFilterForm.get('CityId').value.length ==0? null : { CityId: this.custAppBannerFilterForm.get('CityId').value },
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.observableResponse = this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.showFilterForm) {
      this.marketTypeListFilter = [];
      this.userGroupListFilter = [];
      this.suburbListFilter = [];
      this.provinceListFilter = [];
      this.cityListFilter = [];
      this.loadActionTypes([
        this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          DealerModuleApiSuffixModels.MARKETING_BANNERS_PROVINCES),
        this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          DealerModuleApiSuffixModels.MARKETING_TYPE, prepareRequiredHttpParams({ isAll: true }))
      ]);
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              if (filter) {
                this.provinceListFilter = getPDropdownData(resp.resources,"provinceName","provinceId");;
              } else {
                this.provinceList = getPDropdownData(resp.resources,"provinceName","provinceId");;
              }
              break;
            case 1:
              if (filter) {
                this.marketTypeListFilter = getPDropdownData(resp.resources,"marketingTypeName","marketingTypeId");
              } else {
                this.marketTypeList =getPDropdownData(resp.resources,"marketingTypeName","marketingTypeId");
                switch (type) {
                  case CrudType.CREATE:
                    this.openAddEditPopup('Add Banner', editableObject);
                    break;
                  case CrudType.VIEW:
                    switch (this.selectedTabIndex) {
                      case 0:
                        this.openAddEditPopup('View/Edit Banner', editableObject);
                        break;
                    }
                    break;
                }
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
      this.setFilteredValue();
    });
  }

  setFilteredValue() {
    this.custAppBannerFilterForm.get('MarketingTypeId').value == '' ? null : this.custAppBannerFilterForm.get('MarketingTypeId').value;
    this.custAppBannerFilterForm.get('MarketingName').value == '' ? null : this.custAppBannerFilterForm.get('MarketingName').value;
    this.custAppBannerFilterForm.get('SuburbId').value == '' ? null : this.custAppBannerFilterForm.get('SuburbId').value;
    this.custAppBannerFilterForm.get('ProvinceId').value == '' ? null : this.custAppBannerFilterForm.get('ProvinceId').value;
    this.custAppBannerFilterForm.get('CityId').value == '' ? null : this.custAppBannerFilterForm.get('CityId').value;
  }

  resetForm() {
    this.custAppBannerFilterForm.reset();
    this.filterData = null;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.marketTypeListFilter = [];
    this.userGroupListFilter = [];
    this.suburbListFilter = [];
    this.provinceListFilter = [];
    this.cityListFilter = [];
    this.observableResponse = this.onCRUDRequested('get', { pageIndex: this.row["pageIndex"], pageSize: this.row["pageSize"] });
    this.showFilterForm = !this.showFilterForm;
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
    if (this.provincelistSubscription) {
      this.provincelistSubscription.unsubscribe();
    }
    if (this.citylistSubscription) {
      this.citylistSubscription.unsubscribe();
    }
  }
}
