import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DealerBillingConfigurationAddEditComponent } from './dealer-billing-configuration-add-edit.component';
import { DealerBillingConfigurationViewComponent } from './dealer-billing-configuration-view.component';
import { DealerBillingConfigurationComponent } from './dealer-billing-configuration.component';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: DealerBillingConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Dealer Billing Configuration List' } },
  { path: 'add-edit', component: DealerBillingConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Dealer Billing Configuration Add/Edit' } },
  { path: 'view', component: DealerBillingConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Dealer Billing Configuration View' } }
]
@NgModule({
  declarations: [DealerBillingConfigurationComponent, DealerBillingConfigurationAddEditComponent, DealerBillingConfigurationViewComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule, SharedModule, RouterModule.forChild(routes)],

  entryComponents: [],
  providers: [DatePipe]
})

export class DealerBillingConfigurationModule { }
