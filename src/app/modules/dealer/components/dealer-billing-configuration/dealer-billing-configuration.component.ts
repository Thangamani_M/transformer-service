import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { DealerModuleApiSuffixModels, DEALER_COMPONENT, FEATURE_BILLING_NAME } from "@modules/dealer";
import { loggedInUserData } from "@modules/others";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
    selector: "app-dealer-billing-configuration",
    templateUrl: "./dealer-billing-configuration.component.html",
})
export class DealerBillingConfigurationComponent extends PrimeNgTableVariablesModel {
    primengTableConfigProperties: any;
    row: any = {};
    first: any = 0;
    filterData: any;
    FEATURE_NAME = FEATURE_BILLING_NAME;
    constructor(
        private crudService: CrudService,
        private router: Router,
        private rxjsService: RxjsService,
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>,
        private snackbarService: SnackbarService
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Dealer Billing Configuration",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Billing Configuration', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "WEEKLY",
                        dataKey: "dealerBillingWeeklyConfigId",
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: "billingDay", header: "Payment Calculation Day", width: "150px" },
                            { field: "time", header: "Time", width: "150px" },
                            { field: "isActive", header: "Status", width: "150px" },
                        ],
                        enableAction:true,
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        apiSuffixModel: DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_WEEKLY,
                        moduleName: ModulesBasedApiSuffix.DEALER,
                        featureName: this.FEATURE_NAME.WEEKLY
                    },
                    {
                        caption: "MONTHLY",
                        dataKey: "dealerBillingMonthlyConfigId",
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: "billingDate", header: "Recurring Billing Processing Date", width: "150px" },
                            { field: "time", header: "Time", width: "150px" },
                            { field: "isActive", header: "Status", width: "150px" }
                        ],
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        apiSuffixModel: DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_MONTHLY,
                        moduleName: ModulesBasedApiSuffix.DEALER,
                        featureName: this.FEATURE_NAME.MONTHLY
                    },
                    {
                        caption: "Quarterly",
                        dataKey: "dealerBillingQuaterlyConfigId",
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: "billingStartMonth", header: "Billing Start Month", width: "150px" },
                            { field: "billingDate", header: "Billing Date", width: "150px" },
                            { field: "time", header: "Time", width: "150px" },
                            { field: "isActive", header: "Status", width: "150px" }
                        ],
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        apiSuffixModel: DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_QUATERLY,
                        moduleName: ModulesBasedApiSuffix.DEALER,
                        featureName: this.FEATURE_NAME.QUATERLY
                    },
                    {
                        caption: "Yearly",
                        dataKey: "dealerBillingYearlyConfigId",
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: "billingMonth", header: "Billing Month", width: "150px" },
                            { field: "billingDate", header: "Billing Date", width: "150px" },
                            { field: "time", header: "Time", width: "150px" },
                            { field: "isActive", header: "Status", width: "150px" }
                        ],
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        apiSuffixModel: DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_YEARLY,
                        moduleName: ModulesBasedApiSuffix.DEALER,
                        featureName: this.FEATURE_NAME.YEARLY,
                        // disabled:true
                    },

                ],
            },
        };
        this.status = [
            { label: "Active", value: true },
            { label: "In-Active", value: false },
        ];
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex =
                Object.keys(params["params"]).length > 0 ? +params["params"]["tab"] : 0;
            this.primengTableConfigProperties.selectedTabIndex =
                this.selectedTabIndex;
        });

    }
    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(loggedInUserData),
        this.store.select(currentComponentPageBasedPermissionsSelector$),
      this.activatedRoute.queryParams]
      ).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        let permission = response[1][DEALER_COMPONENT.DEALER_BILLING_MANAGEMENT]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
          if (Object.keys(response[2]).length > 0) {
            this.selectedTabIndex = +response[2]['tab'];
          }
          else {
            this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'];
          }
          this.prepareDynamicBreadcrumbs();
          }
        });
    }

    prepareDynamicBreadcrumbs() {
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Dealer Management', relativeRouterUrl: '' },
      { displayName: 'Dealer Billing Configuration', relativeRouterUrl: '' },{ displayName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption, relativeRouterUrl: '' }]

    }

    ngOnInit(): void {
        this.getRequiredListData();
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object
    ) {
        this.loading = true;
        let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
        dealerModuleApiSuffixModels =
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[
                this.selectedTabIndex
            ].apiSuffixModel;

        this.crudService
            .get(
                ModulesBasedApiSuffix.DEALER,
                dealerModuleApiSuffixModels,
                undefined,
                false,
                prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
            )
            .subscribe((data: IApplicationResponse) => {
                this.loading = false;
                this.rxjsService.setGlobalLoaderProperty(false);
                if (data.isSuccess) {
                    this.dataList = data.resources;
                    this.totalRecords = data.totalCount;
                } else {
                    this.dataList = null;
                    this.totalRecords = 0;
                }
            });
    }
    onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {

        switch (type) {
            case CrudType.CREATE:
                this.openAddEditPage(CrudType.CREATE, row);
                break;
            case CrudType.GET:
                // this.getRequiredListData(
                //   row["pageIndex"], row["pageSize"], otherParams);
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                unknownVar = { ...this.filterData, ...unknownVar };
                this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
                break;
            case CrudType.EDIT:
                this.openAddEditPage(CrudType.EDIT, row);
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.EDIT, row);
                break;
        }
    }
    openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
        switch (type) {
            case CrudType.CREATE:
              if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
                switch (this.selectedTabIndex) {
                    case 0:
                        this.router.navigate(["dealer/dealer-billing-configuration/add-edit"], { queryParams: { feature: this.FEATURE_NAME.WEEKLY } });
                        break;
                    case 1:
                        this.router.navigate(["dealer/dealer-billing-configuration/add-edit"], { queryParams: { feature: this.FEATURE_NAME.MONTHLY } });
                        break;
                    case 2:
                        this.router.navigate(["dealer/dealer-billing-configuration/add-edit"], { queryParams: { feature: this.FEATURE_NAME.QUATERLY } });
                        break;
                    case 3:
                        this.router.navigate(["dealer/dealer-billing-configuration/add-edit"], { queryParams: { feature: this.FEATURE_NAME.YEARLY } });
                        break;
                }
                break;
            case CrudType.EDIT:
                let id = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey;
                switch (this.selectedTabIndex) {
                    case 0:
                        this.router.navigate(["dealer/dealer-billing-configuration/view"], { queryParams: { feature: this.FEATURE_NAME.WEEKLY, id: editableObject[id], tab: this.selectedTabIndex } });
                        break;
                    case 1:
                        this.router.navigate(["dealer/dealer-billing-configuration/view"], { queryParams: { feature: this.FEATURE_NAME.MONTHLY, id: editableObject[id],tab: this.selectedTabIndex } });
                        break;
                    case 2:
                        this.router.navigate(["dealer/dealer-billing-configuration/view"], { queryParams: { feature: this.FEATURE_NAME.QUATERLY, id: editableObject[id],tab: this.selectedTabIndex } });
                        break;
                    case 3:
                        this.router.navigate(["dealer/dealer-billing-configuration/view"], { queryParams: { feature: this.FEATURE_NAME.YEARLY, id: editableObject[id],tab: this.selectedTabIndex } });
                        break;
                }
                break;
        }
    }

    tabClick(e) {
        this.selectedTabIndex = e?.index;
        this.dataList = [];
        let queryParams = {};
        queryParams['tab'] = this.selectedTabIndex;
        this.onAfterTabChange(queryParams);
        this.getRequiredListData();
    }
    onAfterTabChange(queryParams) {
        if (this.selectedTabIndex != 0) {
            this.router.navigate([`../`], { relativeTo: this.activatedRoute, queryParams: queryParams });
        }
    }
    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    onBreadCrumbClick(breadCrumbItem: object): void {
        if (breadCrumbItem.hasOwnProperty('queryParams')) {
          this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
            { queryParams: breadCrumbItem['queryParams'] })
        }
        // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
        else {
          this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
        }
      }
}
