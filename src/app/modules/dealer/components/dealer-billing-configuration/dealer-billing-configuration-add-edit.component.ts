import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { DealerModuleApiSuffixModels, FEATURE_BILLING_NAME } from "@modules/dealer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import moment from "moment";

@Component({
    selector: 'app-dealer-billing-configuration-add-edit',
    templateUrl: './dealer-billing-configuration-add-edit.component.html',
})
export class DealerBillingConfigurationAddEditComponent {
    primengTableConfigProperties: any;
    dealerBillingConfigAddEditForm: FormGroup;
    loggedUser;
    billingDayList: any = [];
    billingMonthList: any = [];
    id;
    tab; params;
    feature: string;
    createAPI;
    updateAPI;
    getAPI;
    selectedTabIndex = 0;
    statusList = [
        { displayName: "Active", id: true },
        { displayName: "In-Active", id: false },
    ];
    billingDaysList: any = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
    montList = [{ id: 1, displayName: "January" }, { id: 2, displayName: "February" }, { id: 3, displayName: "March" },
    { id: 4, displayName: "April" }, { id: 5, displayName: "May" }, { id: 6, displayName: "June" },
    { id: 7, displayName: "July" }, { id: 8, displayName: "August" }, { id: 9, displayName: "September" },
    { id: 10, displayName: "October" }, { id: 11, displayName: "November" }, { id: 12, displayName: "December" }]
    constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>,
        private crudService: CrudService, private router: Router) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: 'Add/Edit Billing Configuration',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '', queryParams: { tab: 1 } }, { displayName: 'Dealer Billing Configuration List', relativeRouterUrl: '/dealer/dealer-billing-configuration' }, { displayName: 'Add Dealer Billing Configuration', relativeRouterUrl: '', }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Create',
                        dataKey: 'branchId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        url: '',
                    },
                ]
            }
        }
        this.feature = this.activatedRoute.snapshot.queryParams.feature;
        this.id = this.activatedRoute.snapshot.queryParams.id;
        if (this.id) {
            this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'Edit Dealer Billing Configuration for ' + this.feature;
            this.primengTableConfigProperties.tableCaption = "Edit Dealer Billing Configuration for " + this.feature
        } else {
            this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'Add Dealer Billing Configuration for ' + this.feature;
            this.primengTableConfigProperties.tableCaption = "Add Dealer Billing Configuration for " + this.feature
        }
    }
    ngOnInit(): void {
        this.initForm();
        this.getBillingConfigDays();
        this.getBillingConfigMonths();
        this.getFeatureDetails(this.feature);
        if (this.id)
            this.getBillingDetail();
    }
    initForm() {
        this.dealerBillingConfigAddEditForm = new FormGroup({
            'dayId': new FormControl(null),
            'monthId': new FormControl(null),
            'monthDays': new FormControl(null),
            'executionTime': new FormControl(null),
            'isActive': new FormControl(null),
        });
        this.dealerBillingConfigAddEditForm = setRequiredValidator(this.dealerBillingConfigAddEditForm, ["executionTime", "isActive"]);
        if (this.feature != FEATURE_BILLING_NAME.WEEKLY)
            this.dealerBillingConfigAddEditForm = setRequiredValidator(this.dealerBillingConfigAddEditForm, ["monthDays"]);
        if (this.feature == FEATURE_BILLING_NAME.YEARLY || this.feature == FEATURE_BILLING_NAME.QUATERLY)
            this.dealerBillingConfigAddEditForm = setRequiredValidator(this.dealerBillingConfigAddEditForm, ["monthId"]);
        if (this.feature == FEATURE_BILLING_NAME.WEEKLY)
            this.dealerBillingConfigAddEditForm = setRequiredValidator(this.dealerBillingConfigAddEditForm, ["dayId"]);
    }
    getBillingDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.DEALER,
            this.getAPI, undefined, false,
            prepareRequiredHttpParams(this.params)
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.setValue(data.resources);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    setValue(data) {
        this.dealerBillingConfigAddEditForm.get('dayId').setValue(data.dayId);
        if (data.monthId)
            this.dealerBillingConfigAddEditForm.get('monthId').setValue(data.monthId);
        if (data.billingStartMonth) {
            let filter = this.montList.filter(x => x.displayName == data.billingStartMonth);
            this.dealerBillingConfigAddEditForm.get('monthId').setValue(filter[0].id);
        }
        if (data.billingMonth) {
            let filter = this.montList.filter(x => x.displayName == data.billingMonth);
            this.dealerBillingConfigAddEditForm.get('monthId').setValue(filter[0].id);
        }
        this.dealerBillingConfigAddEditForm.get('monthDays').setValue(data.billingDate);
        this.dealerBillingConfigAddEditForm.get('executionTime').setValue(moment(data.time, ["h:mm A"]).format("HH:mm:ss"));
        this.dealerBillingConfigAddEditForm.get('isActive').setValue(data.isActive);
    }
    toTime(timeString) {
        var timeTokens = timeString.split(':');
        return new Date(1970, 0, 1, timeTokens[0], timeTokens[1], timeTokens[2]);
    }
    getFeatureDetails(feature) {
        switch (feature) {
            case FEATURE_BILLING_NAME.WEEKLY:
                this.getAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_WEEKLY_DETAIL;
                this.createAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_WEEKLY_CREATE;
                this.updateAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_WEEKLY_UPDATE;
                this.params = { DealerBillingWeeklyConfigId: this.id };
                this.tab = 0;
                break;
            case FEATURE_BILLING_NAME.MONTHLY:
                this.getAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_MONTHLY_DETAIL;
                this.createAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_MONTHLY_CREATE;
                this.updateAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_MONTHLY_UPDATE;
                this.params = { DealerBillingMonthlyConfigId: this.id };
                this.tab = 1;
                break;
            case FEATURE_BILLING_NAME.QUATERLY:
                this.getAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_QUATERLY_DETAIL;
                this.createAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_QUATERLY_CREATE;
                this.updateAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_QUATERLY_UPDATE;
                this.params = { DealerBillingQuaterlyConfigId: this.id };
                this.tab = 2;
                break;
            case FEATURE_BILLING_NAME.YEARLY:
                this.getAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_YEARLY_DETAIL;
                this.createAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_YEARLY_CREATE;
                this.updateAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_YEARLY_UPDATE;
                this.params = { DealerBillingYearlyConfigId: this.id };
                this.tab = 3;
                break;
        }
    }
    getBillingConfigDays() {
        this.crudService.get(
            ModulesBasedApiSuffix.DEALER,
            DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_DAYS,
            undefined,
            false, null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.billingDayList = data.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    getBillingConfigMonths() {
        this.crudService.get(
            ModulesBasedApiSuffix.DEALER,
            DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_MONTHS,
            undefined,
            false, null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.billingMonthList = data.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    toHoursMints24(date) {
        return moment(date).format('HH:mm');
    }
    onBreadCrumbClick(breadCrumbItem: object): void {
        if (breadCrumbItem.hasOwnProperty('queryParams')) {
            this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
                { queryParams: breadCrumbItem['queryParams'] })
        }
        else {
            let tab = 0;
            if (this.feature == 'weekly')
                tab = 0;
            else if (this.feature == 'monthly')
                tab = 1;
            else if (this.feature == 'quaterly')
                tab = 2;
            else if (this.feature == 'yearly')
                tab = 3;
            this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
                { queryParams: { tab: tab } })
        }
    }
    tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }
    onSubmit() {
        if (this.dealerBillingConfigAddEditForm.invalid) {
            this.dealerBillingConfigAddEditForm.markAllAsTouched();
            return;
        }
        let formValue = this.dealerBillingConfigAddEditForm.getRawValue();
        formValue.executionTime = (formValue.executionTime.toString().includes("GMT")) ? this.toHoursMints24(formValue.executionTime) : this.tConvert(formValue.executionTime);
        let finalObject = {
            dayId: formValue.dayId ? Number(formValue.dayId) : null,
            executionTime: formValue.executionTime,
            isActive: formValue.isActive,
            createdUserId: !this.id ? this.loggedUser.userId : null,
            monthId: formValue.monthId ? Number(formValue.monthId) : null,
            monthDays: formValue.monthDays ? Number(formValue.monthDays) : null,
            modifiedUserId: this.loggedUser.userId,
            dealerBillingWeeklyConfigId: null,
            dealerBillingMonthlyConfigId: null,
            dealerBillingQuaterlyConfigId: null,
            dealerBillingYearlyConfigId: null
        }
        if (this.feature == 'weekly')
            finalObject.dealerBillingWeeklyConfigId = this.id;
        else if (this.feature == 'monthly')
            finalObject.dealerBillingMonthlyConfigId = this.id;
        else if (this.feature == 'quaterly')
            finalObject.dealerBillingQuaterlyConfigId = this.id;
        else if (this.feature == 'yearly')
            finalObject.dealerBillingYearlyConfigId = this.id;

        Object.keys(finalObject).forEach(key => {
            if (finalObject[key] == "" || finalObject[key] == null) {
                delete finalObject[key]
            }
        });
        let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.DEALER, this.updateAPI, finalObject) :
            this.crudService.create(ModulesBasedApiSuffix.DEALER, this.createAPI, finalObject, 1);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });

    }
    navigate() {
        this.router.navigate(["dealer/dealer-billing-configuration"], { queryParams: { tab: this.tab } });
    }
}
