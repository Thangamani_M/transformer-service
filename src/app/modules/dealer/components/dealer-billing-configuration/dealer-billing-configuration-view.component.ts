import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { DealerModuleApiSuffixModels, DEALER_COMPONENT, FEATURE_BILLING_NAME } from "@modules/dealer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: 'app-dealer-billing-configuration-view',
  templateUrl: './dealer-billing-configuration-view.component.html',
})
export class DealerBillingConfigurationViewComponent {
  primengTableConfigProperties: any;
  loggedUser: any;
  getAPI;
  id: string;
  feature: FEATURE_BILLING_NAME;
  params;
  billingDetails;
  tab;
  selectedTabIndex = 0;
  viewData = []
  pageLevelAccessConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}]
    }
  }

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.feature = this.activatedRoute.snapshot.queryParams.feature;
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Billing Configuration for " + this.feature,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Billing Configuration', relativeRouterUrl: 'dealer/dealer-billing-configuration' }, { displayName: 'View Dealer Billing Configuration for ' + this.feature, relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "View Dealer Billing Configuration for " + this.feature,
            dataKey: "d",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableAction: true,
            enableViewBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getFeatureDetails(this.feature);
    this.getBillingDetail();
    this.viewData = [
      { name: 'Billing Day', value: "" },
      { name: 'Time', value: "" },
      { name: 'Status', value: "" },
    ]
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_BILLING_MANAGEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelAccessConfigProperties, permission);
        this.pageLevelAccessConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getBillingDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      this.getAPI, undefined, false,
      prepareRequiredHttpParams(this.params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.billingDetails = data.resources;
        this.viewData = [
          { name: 'Billing Day', value: "" },
          { name: 'Time', value: this.billingDetails?.time },
          { name: 'Status', value: this.billingDetails?.isActive == true ? 'Active' : 'In-Active', statusClass: this.billingDetails?.isActive == true ? "status-label-green" : 'status-label-red' },
        ]

        switch (this.feature) {
          case FEATURE_BILLING_NAME.WEEKLY:
            this.viewData[0] = { name: 'Billing Day', value: this.billingDetails?.billingDay };
            break;
          case FEATURE_BILLING_NAME.MONTHLY:
            this.viewData[0] = { name: 'Billing Date', value: this.billingDetails?.billingDate, isValue: true };
            break;
          case FEATURE_BILLING_NAME.QUATERLY:
            this.viewData[0] = { name: 'Billing Start Month', value: this.billingDetails?.billingStartMonth };
            this.viewData.splice(1, 0, { name: 'Billing Date', value: this.billingDetails?.billingDate, isValue: true });
            break;
          case FEATURE_BILLING_NAME.YEARLY:
            this.viewData[0] = { name: 'Billing Month', value: this.billingDetails?.billingMonth };
            this.viewData.splice(1, 0, { name: 'Billing Date', value: this.billingDetails?.billingDate, isValue: true });
            break;
        }

        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }
  getFeatureDetails(feature) {
    switch (feature) {
      case FEATURE_BILLING_NAME.WEEKLY:
        this.getAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_WEEKLY_DETAIL;
        this.params = { DealerBillingWeeklyConfigId: this.id };
        this.tab = 0;
        break;
      case FEATURE_BILLING_NAME.MONTHLY:
        this.getAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_MONTHLY_DETAIL;
        this.params = { DealerBillingMonthlyConfigId: this.id };
        this.tab = 1;
        break;
      case FEATURE_BILLING_NAME.QUATERLY:
        this.getAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_QUATERLY_DETAIL;
        this.params = { DealerBillingQuaterlyConfigId: this.id };
        this.tab = 2;
        break;
      case FEATURE_BILLING_NAME.YEARLY:
        this.getAPI = DealerModuleApiSuffixModels.DEALER_BILLING_CONFIG_YEARLY_DETAIL;
        this.params = { DealerBillingYearlyConfigId: this.id };
        this.tab = 3;
        break;
    }
  }
  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      let tab = 0;
      if (this.feature == 'weekly')
        tab = 0;
      else if (this.feature == 'monthly')
        tab = 1;
      else if (this.feature == 'quaterly')
        tab = 2;
      else if (this.feature == 'yearly')
        tab = 3;
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: { tab: tab } });
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;

    }
  }
  navigate() {
    this.router.navigate(["dealer/dealer-billing-configuration"], { queryParams: { tab: this.tab } });
  }
  edit() {
    if (!this.pageLevelAccessConfigProperties.tableComponentConfigs.tabsList[this.tab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["dealer/dealer-billing-configuration/add-edit"], { queryParams: { feature: this.feature, id: this.id } });
  }
}
