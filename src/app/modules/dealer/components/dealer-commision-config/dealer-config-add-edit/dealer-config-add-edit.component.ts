import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setPercentageValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { DealerCommissionDetailModel } from '@modules/dealer/models/dealer-commission-config.model';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-dealer-config-add-edit',
  templateUrl: './dealer-config-add-edit.component.html',
  styles:[`
  ::ng-deep label.ui-dropdown-label.ui-inputtext.ui-corner-all.ui-placeholder {
    height: unset !important;
  }`]
})
export class DealerConfigAddEditComponent implements OnInit {

  dealerCommissionDialogForm: FormGroup;
  isSubmitted: boolean;
  categorylistSubscription: any;
  dealerCategoryTypeList = [];
  _dealerCategoryTypeList = [];
  dealerTypeList = [];
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 4 } });

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
    this._dealerCategoryTypeList = this.config?.data?.dealerCategoryTypeList;
    this.dealerCategoryTypeList = getPDropdownData(this.config?.data?.dealerCategoryTypeList,"displayName",'id');
    this.dealerTypeList = this.config?.data?.dealerTypeList;
  }

  ngAfterViewInit() {
  }

  initForm(dealerCommissionDetailModel?: DealerCommissionDetailModel) {
    let dealerCommissionModel = new DealerCommissionDetailModel(dealerCommissionDetailModel);
    this.dealerCommissionDialogForm = this.formBuilder.group({});
    Object.keys(dealerCommissionModel).forEach((key) => {
      if (typeof dealerCommissionModel[key] === 'object') {
        this.dealerCommissionDialogForm.addControl(key, new FormArray(dealerCommissionModel[key]));
      } else {
        this.dealerCommissionDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : dealerCommissionModel[key]));
      }
    });
    if(this.config.data?.row){
      this.dealerCommissionDialogForm.get("dealerCategoryTypeId").setValue(+this.config?.data?.row?.dealerCategoryTypeId)
      this.dealerCommissionDialogForm.get("dealerTypeId").setValue(+this.config?.data?.row?.dealerTypeId)
    }
    this.dealerCommissionDialogForm.get('description').disable();
    this.dealerCommissionDialogForm = setRequiredValidator(this.dealerCommissionDialogForm, ["dealerTypeId", "dealerCategoryTypeId", "commission"]);
    this.dealerCommissionDialogForm = setPercentageValidator(this.dealerCommissionDialogForm, ["commission"]);
    this.onValueChanges();
  }

  onValueChanges() {
    this.dealerCommissionDialogForm.get('dealerCategoryTypeId').valueChanges.subscribe(res => {
      console.log("res",res)
      if (res) {
        const desc = this._dealerCategoryTypeList.find(el => el?.id == res);
        this.dealerCommissionDialogForm.get('description').patchValue(desc?.description);
      }
    })
  }

  btnCloseClick() {
    this.dealerCommissionDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitdealerCommissionDialog() {
    if (this.isSubmitted || !this.dealerCommissionDialogForm?.valid) {
      this.dealerCommissionDialogForm?.markAllAsTouched();
      return;
    } else if (!this.dealerCommissionDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const dealerCommissionObject = {
      ...this.dealerCommissionDialogForm.value,
    }
    if (this.dealerCommissionDialogForm.value?.commissionConfigId) {
      dealerCommissionObject['createdUserId'] = this.config?.data?.createdUserId;
    } else {
      dealerCommissionObject['createdUserId'] = this.config?.data?.createdUserId;
      delete dealerCommissionObject['commissionConfigId'];
    }
    dealerCommissionObject['dealerCategoryTypeId'] = +dealerCommissionObject['dealerCategoryTypeId'];
    dealerCommissionObject['dealerTypeId'] = +dealerCommissionObject['dealerTypeId'];
    dealerCommissionObject['commission'] = +dealerCommissionObject['commission'];
    delete dealerCommissionObject['categoryDescription'];
    this.rxjsService.setDialogOpenProperty(true);
    dealerCommissionObject['isActive'] = true;
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    const api = this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_COMMISSION_TYPE, [dealerCommissionObject])
    // if (this.dealerCommissionDialogForm.value?.commissionConfigId) {
    //   api = this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_COMMISSION_TYPE, [dealerCommissionObject]);
    // }
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.dealerCommissionDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }
}
