import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { DealerConfigAddEditComponent } from '@modules/dealer';
import { DealerCommissionFilterModel } from '@modules/dealer/models/dealer-commission-config.model';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-dealer-commission-config',
  templateUrl: './dealer-commission-config-list.component.html',
  styles:[`
  ::ng-deep label.ui-dropdown-label.ui-inputtext.ui-corner-all.ui-placeholder {
    height: unset !important;
  }`]
})
export class DealerCommissionConfigListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  isLoading: boolean;
  isSubmitted: boolean;
  showFilterForm = false;
  dealerCommissionFilterForm: FormGroup;
  dealerCategoryTypeFilterList: any = [];
  dealerCategoryTypeList: any = [];
  dealerTypeList: any = [];
  listSubscribtion: any;
  dateFormat = 'MMM dd, yyyy';
  row: any = {};
  first: any = 0;
  filterData: any;
  multipleSubscription: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialogService: DialogService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: 'Dealer Commission Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Commission Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Commission Config',
            dataKey: 'commissionConfigId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'commissionConfigId', header: 'Commission Config ID', width: '150px' },
              { field: 'dealerTypeName', header: 'Dealer Type', width: '100px' },
              { field: 'dealerCategoryTypeName', header: 'Category Type', width: '100px' },
              { field: 'description', header: 'Category Description', width: '300px' },
              { field: 'commission', header: 'Percentage Commission', width: '150px' },
              { field: 'isActive', header: 'Status', width: '150px' }
            ],
            enableMultiDeleteActionBtn: true,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: true,
            enableStatusActiveAction: true,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_COMMISSION_TYPE,
            deleteAPISuffixModel: DealerModuleApiSuffixModels.COMMISSION_CONFIG_DELETE,
            moduleName: ModulesBasedApiSuffix.DEALER,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getDealerCommissionListData(null, null, { sortOrder: 'ASC' });
    this.combineLatestNgrxStoreData();
    this.createDealerCommFilterForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][DEALER_COMPONENT.DEALER_COMMISSION_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getDealerCommissionListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      case CrudType.STATUS_POPUP:

        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onChangeStatus(row, unknownVar)
        break;
      case CrudType.DELETE:

        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      default:
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let dealerCategoryTypeFilterArray = resp.resources;
              if (filter) {
                this.dealerCategoryTypeFilterList = getPDropdownData(dealerCategoryTypeFilterArray);
              } else {
                this.dealerCategoryTypeList = dealerCategoryTypeFilterArray;
              }
              break;
            case 1:
              let dealerTypeFilterArray = getPDropdownData(resp.resources)
              if (!filter) {
                this.dealerTypeList = dealerTypeFilterArray;
                switch (type) {
                  case CrudType.CREATE:
                    this.openAddEditPopup('Create Dealer Commission Config', editableObject);
                    break;
                  case CrudType.VIEW:
                    switch (this.selectedTabIndex) {
                      case 0:
                        this.openAddEditPopup('View/Edit Commission Config', editableObject);
                        break;
                    }
                    break;
                }
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createDealerCommFilterForm(dealerCommissionFilterModel?: DealerCommissionFilterModel) {
    let dealerCommissionModel = new DealerCommissionFilterModel(dealerCommissionFilterModel);
    this.dealerCommissionFilterForm = this.formBuilder.group({});
    Object.keys(dealerCommissionModel).forEach((key) => {
      this.dealerCommissionFilterForm.addControl(key, new FormControl(dealerCommissionModel[key]));
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes([
      this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALET_CATEGORY_DROPDOWN, prepareRequiredHttpParams({ isAll: true })),
      this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_TYPE_DROPDOWN, prepareRequiredHttpParams({ isAll: true }))
    ], false, editableObject, type);
  }

  openAddEditPopup(header, row) {
    let rowData;
    if (row) {
      rowData = { ...row };
      rowData['dealerTypeId'] = rowData['dealerTypeId']?.toString();
      rowData['dealerCategoryTypeId'] = rowData['dealerCategoryTypeId']?.toString();
    }
    let data = {
      createdUserId: this.loggedInUserData?.userId,
      row: rowData,
    };
    data['dealerCategoryTypeList'] = this.dealerCategoryTypeList;
    data['dealerTypeList'] = this.dealerTypeList;
    const ref = this.dialogService.open(DealerConfigAddEditComponent, {
      header: header,
      baseZIndex: 500,
      width: '700px',
      closable: false,
      showHeader: false,
      data: data,
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getDealerCommissionListData();
      }
    });
  }

  onChangeStatus(rowData?, index?) {
    let status;
    if (rowData['isActive']) {
      status = true
    } else {
      status = false
    }
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: status,
        // activeText: 'Enable',
        // inActiveText: 'Disable',
        modifiedUserId: this.loggedInUserData?.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        // this.dataList[index].status = this.dataList[index].status == 'Enable' ? 'Disable' : 'Enable';
        this.getDealerCommissionListData(this.row["pageIndex"], this.row["pageSize"]);
      } else {
        this.getDealerCommissionListData(this.row["pageIndex"], this.row["pageSize"]);
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].deleteAPISuffixModel,
        method: 'delete',
        dataObject: {
          ids: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
          isDeleted: true,
        }
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.getDealerCommissionListData();
      }
    });
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.showFilterForm) {
      this.dealerCategoryTypeFilterList = [];
      this.loadActionTypes([
        this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
          DealerModuleApiSuffixModels.DEALET_CATEGORY_DROPDOWN, prepareRequiredHttpParams({ isAll: true }))
      ]);
    }
  }
  submitFilter() {
    this.filterData = Object.assign({},
      this.dealerCommissionFilterForm.get('dealerCategoryTypeId').value == '' ? null : { dealerCategoryTypeId: this.dealerCommissionFilterForm.get('dealerCategoryTypeId').value },
      this.dealerCommissionFilterForm.get('description').value ? { description: this.dealerCommissionFilterForm.get('description').value } : '',
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.dealerCommissionFilterForm.reset();
    this.filterData = null;
    this.showFilterForm = !this.showFilterForm;
    if (!this.showFilterForm) {
      this.getDealerCommissionListData();
    }
  }

  getDealerCommissionListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    const moduleName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName;
    this.listSubscribtion = this.crudService.get(
      moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
