import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerCommissionConfigListComponent } from './dealer-commission-config-list.component';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: DealerCommissionConfigListComponent, canActivate:[AuthGuard],  data: { title: 'Dealer commission Config' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class DealerCommissionConfigRoutingModule { }
