import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerCommissionConfigListComponent, DealerCommissionConfigRoutingModule, DealerConfigAddEditComponent } from '@modules/dealer';
import { InputSwitchModule } from 'primeng/inputswitch';

@NgModule({
    declarations: [DealerCommissionConfigListComponent, DealerConfigAddEditComponent,],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        InputSwitchModule,
        ReactiveFormsModule,
        FormsModule,
        DealerCommissionConfigRoutingModule
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerConfigAddEditComponent]
})
export class DealerCommissionConfigModule { }
