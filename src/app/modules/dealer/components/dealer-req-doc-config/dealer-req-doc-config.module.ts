import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { DealerReqDocConfigListComponent } from '@modules/dealer';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';
@NgModule({
  declarations: [DealerReqDocConfigListComponent],
  imports: [
    CommonModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: DealerReqDocConfigListComponent, canActivate: [AuthGuard], data: { title: 'Dealer Required Document Config' } }
    ])
  ],
  providers: []
})
export class DealerReqDocConfigModule { }
