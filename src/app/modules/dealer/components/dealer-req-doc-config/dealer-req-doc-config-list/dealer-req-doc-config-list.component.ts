import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { DealerReqDocConfigFilterModel } from '@modules/dealer/models/dealer-req-doc-config.model';
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-dealer-req-doc-config-list',
  templateUrl: './dealer-req-doc-config-list.component.html'
})
export class DealerReqDocConfigListComponent extends PrimeNgTableVariablesModel implements OnInit {

  dealerReqDocConfigForm: FormGroup;
  primengTableConfigProperties: any;
  row: any = {};
  dealerTypeList = [];
  listSubscription: any;
  first: any = 0;

  constructor(
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Required Document Config",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Dealer Required Document Config' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Required Document Config',
            dataKey: 'dealerDocumentConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealerDocumentTypeName', header: 'Document Type' },
              { field: 'isRequired', header: 'Required', width: '200px', isCheckbox: true, isDisabled: false },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_DOC_CONFIG,
            moduleName: ModulesBasedApiSuffix.DEALER,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.createDealerReqDocConfigForm();
    this.combineLatestNgrxStoreData();
    this.onLoadDealerTypeDropDown();
  }

  onLoadDealerTypeDropDown() {
    this.loading = true;
    this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_TYPE_DROPDOWN, prepareGetRequestHttpParams(null, null,
        { IsAll: false })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            this.dealerTypeList = response.resources;
            this.dealerReqDocConfigForm.get('dealerTypeId')?.setValue(response.resources[0]);
          }
        });
    this.onValueChanges();
  }

  onValueChanges() {
    this.dealerReqDocConfigForm.get('dealerTypeId')
      .valueChanges.subscribe(res => {
        if (res) {
          this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
          this.onCRUDRequested('get', this.row);
        }
      })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
    this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
     this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][DEALER_COMPONENT.DEALER_REQUIRED_DOCUMENT]
      if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
      this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
    });
    }

  createDealerReqDocConfigForm(dealerReqDocConfigFilterModel?: DealerReqDocConfigFilterModel) {
    let dealerReqDocConfigModel = new DealerReqDocConfigFilterModel(dealerReqDocConfigFilterModel);
    this.dealerReqDocConfigForm = this.formBuilder.group({});
    Object.keys(dealerReqDocConfigModel).forEach((key) => {
      this.dealerReqDocConfigForm.addControl(key, new FormControl(dealerReqDocConfigModel[key]));
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col)
    }
  }

  getdealerReqDocConfigListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {
      ...otherParams,
      isAll: true,
      dealerTypeId: this.dealerReqDocConfigForm.get('dealerTypeId')?.value?.id
    }
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
      this.onAfterLoading();
    }
    this.listSubscription = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.onAfterLoading();
    }, error => {
      this.onHandleError();
    })
  }

  onAfterLoading() {
    this.loading = false;
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...unknownVar };
        this.getdealerReqDocConfigListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      default:
    }
  }

  onSubmit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let obj = [];
    this.dataList.forEach((el, i) => {
      obj.push({
        dealerDocumentConfigId: el.dealerDocumentConfigId,
        dealerTypeId: el.dealerTypeId ? el.dealerTypeId : this.dealerReqDocConfigForm.get('dealerTypeId')?.value?.id,
        dealerDocumentTypeId: el.dealerDocumentTypeId,
        isRequired: el.isRequired,
        isActive: el.isActive,
        createdUserId: this.loggedInUserData.userId,
      })
    });
    this.loading = true;
    this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_DOC_CONFIG, obj, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
      }
    }, error => {
      this.onHandleError();
    })
  }

  onHandleError() {
    this.loading = false;
    this.dataList = [];
  }

  onCancel() {
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }
}
