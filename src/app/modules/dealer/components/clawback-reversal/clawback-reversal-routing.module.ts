import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClawbackReversalAddComponent } from './clawback-reversal-add.component';
import { ClawbackReversalListComponent } from './clawback-reversal-list.component';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: ClawbackReversalListComponent, canActivate: [AuthGuard], data: { title: "Clawback Reversal List" } },
  { path: 'create', component: ClawbackReversalAddComponent, canActivate: [AuthGuard], data: { title: "Create Clawback Reversal" } },
]
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ClawbackReversalRoutingModule { }
