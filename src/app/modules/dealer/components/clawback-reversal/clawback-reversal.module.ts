import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ClawbackReversalAddComponent } from './clawback-reversal-add.component';
import { ClawbackReversalListComponent } from './clawback-reversal-list.component';
import { ClawbackReversalRoutingModule } from './clawback-reversal-routing.module';
@NgModule({
  declarations: [ClawbackReversalListComponent, ClawbackReversalAddComponent],
  imports: [
    CommonModule,
    ClawbackReversalRoutingModule,
    LayoutModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ClawbackReversalModule { }
