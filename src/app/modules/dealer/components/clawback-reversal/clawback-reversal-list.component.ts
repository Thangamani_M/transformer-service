import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, fileUrlDownload, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from "@modules/dealer";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-clawback-reversal-list',
  templateUrl: './clawback-reversal-list.component.html'
})
export class ClawbackReversalListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};

  constructor(private router: Router, private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    super();

    this.primengTableConfigProperties = {
      tableCaption: "Clawback Reversal List",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '/dealer' }, { displayName: 'Clawback Reversal', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Clawback Reversal List',
            dataKey: 'dealerBillingClawbackReversalId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'dealerBillingClawbackReversalRefNo', header: 'Reversal Number', width: '180px' },
              // { field: 'dealerBillingClawbackRefNo', header: 'Clawback Number', width: '180px' },
              { field: 'dealerIIPCode', header: 'Dealer Code', width: '150px' },
              { field: 'dealerBranchCode', header: 'Dealer Branch Code', width: '180px' },
              { field: 'dealerDebtorCode', header: 'Dealer Debtor Code', width: '180px' },
              { field: 'dealerName', header: 'Dealer Name', width: '150px' },
              { field: 'contractRefNo', header: 'Caf No', width: '150px' },
              { field: 'customerRefNo', header: 'Customer Code', width: '150px' },
              { field: 'customerDebtorCode', header: 'Customer Debtor Code', width: '180px' },
              { field: 'customerName', header: 'Customer Name', width: '150px' },
              { field: 'clawbackReasonName', header: 'Clawback Reason', width: '150px' },
              { field: 'invoiceRefNo', header: 'Reference Invoice Number', width: '180px' },
              // { field: 'invoiceRefNo', header: 'Clawback Reference', width: '180px' },
              { field: 'documentPath', header: 'Download', width: '200px', isDownloadLink: true, nofilter: true },
            ],
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_BILLING_CLAWBACK_REVERSALS,
            moduleName: ModulesBasedApiSuffix.DEALER,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableAction: true,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.CLAWBACK_REVERSAL]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let apiVersion = 1;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      DealerModuleApiSuffixModels.DEALER_BILLING_CLAWBACK_REVERSALS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams),
      apiVersion
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/dealer/clawback-reversal/create']);
        break;
      case CrudType.GET:
        this.getRequiredListData(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canDownload) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (otherParams == "documentPath" && row['documentPath']) {
          fileUrlDownload(row['documentPath'], row['documentPath'])
        }
        break;
      case CrudType.VIEW:
        break;
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
