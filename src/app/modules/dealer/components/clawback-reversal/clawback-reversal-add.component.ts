
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';


@Component({
  selector: 'app-clawback-reversal-add',
  templateUrl: './clawback-reversal-add.component.html',
  //styleUrls: ['./clawback-reversal-add.component.scss']
  styles: [
    `::ng-deep .autocomplete-customer{
      mat-form-field{
    .mat-form-field-wrapper{
      width: 100% !important;
    }
      }
    }
    `,
  ],
})
export class ClawbackReversalAddComponent implements OnInit {


  clawbackId: any;
  clawbackForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  search: FormControl = new FormControl();
  isRequestInProgress = false;
  customersList = [];
  selectedCustomerObject: any;
  reasonsList = [];
  reversalCustomerList: any = [];
  clawbackDetails: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.clawbackId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createClawbackForm()
    this.customerDataAutocomplete()
    this.getclawbackReasons()
    // this.getclawbackReversalCustomer()
  }

  createClawbackForm(): void {
    this.clawbackForm = this.formBuilder.group({
      dealerBillingClawbackId: ['', Validators.required],
      clawbackReversalReasonId: ['', Validators.required],
      notes: ['', Validators.required],
      createdUserId: ['', Validators.required],
    });
    this.clawbackForm = setRequiredValidator(this.clawbackForm, ["dealerBillingClawbackId", "clawbackReversalReasonId", "notes"]);
    this.clawbackForm.get('createdUserId').setValue(this.loggedUser?.userId)

  }

  customerDataAutocomplete() {
    this.search.valueChanges
      .pipe(tap(() => {
        this.isRequestInProgress = true
      }),
        distinctUntilChanged(),
        debounceTime(debounceTimeForSearchkeyword),
        switchMap((search: string) => {
          if (search == '') {
            return of();
          }
          return this.getCustomersListByKeyword(search)
        })
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.customersList = response.resources;
        }
        else {
          this.customersList = [];
        }
        this.isRequestInProgress = false;
      });
  }

  getCustomersListByKeyword(search: string): Observable<IApplicationResponse> {
    search = encodeURIComponent(search)
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, DealerModuleApiSuffixModels.UX_DEALER_BILLING_CLAWBACK_REVERSALS_CUSTOMERS, undefined,
      false, prepareGetRequestHttpParams(undefined, undefined, { DealerBillingClawbackRefNo: search }), 1);
  }

  onSelectionChanged(event: any) {
    this.selectedCustomerObject = event.option.value;

    this.crudService.get(ModulesBasedApiSuffix.BILLING, DealerModuleApiSuffixModels.DEALER_BILLING_CLAWBACK_REVERSALS, this.selectedCustomerObject.id).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        // this.clawbackForm.patchValue(response.resources);
        this.clawbackForm.get('dealerBillingClawbackId').setValue(event.option.value.id);
        this.clawbackDetails = response.resources
      }
    })
    this.search.setValue(this.selectedCustomerObject.displayName, { emitEvent: false, onlySelf: true });
  }

  getclawbackReversalCustomer() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, DealerModuleApiSuffixModels.UX_DEALER_BILLING_CLAWBACK_REVERSALS_CUSTOMERS).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.rxjsService.setGlobalLoaderProperty(false)
        this.reversalCustomerList = response.resources;
      }
    })
  }

  getclawbackReasons() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, DealerModuleApiSuffixModels.DEALER_BILLING_CLAWBACK_REASONS, undefined,
      false, prepareGetRequestHttpParams(undefined, undefined, { IsReversal: true })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.rxjsService.setGlobalLoaderProperty(false)
          this.reasonsList = response.resources;
        }
      })
  }

  onSubmit(): void {
    if (this.clawbackForm.invalid) {
      return
    }

    this.crudService.create(ModulesBasedApiSuffix.BILLING, DealerModuleApiSuffixModels.DEALER_BILLING_CLAWBACK_REVERSALS, this.clawbackForm.value).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }

  goBack() {
    this.router.navigate(['/dealer/clawback-reversal']);
  }

}

