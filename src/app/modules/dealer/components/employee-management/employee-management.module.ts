import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerEmployeeReinstateModule } from './dealer-employee-reinstate/dealer-employee-reinstate.module';
import { DealerEmployeeSuspensionModule } from './dealer-employee-suspension/dealer-employee-suspension.module';
import { DealerEmployeeTerminationModule } from './dealer-employee-termination/dealer-employee-termination.module';
import { DealerEmployeeAddEditComponent } from './employee-add-edit/employee-add-edit-component';
import { DealerEmployeeListComponent } from './employee-list/employee-list-component';
import { EmployeeManagementRoutingModule } from './employee-management-routing.module';
import { DealerEmployeeViewComponent } from './employee-view/employee-view-component';

@NgModule({
    declarations: [DealerEmployeeListComponent,DealerEmployeeAddEditComponent,DealerEmployeeViewComponent ],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        EmployeeManagementRoutingModule,
        DealerEmployeeSuspensionModule,
        DealerEmployeeReinstateModule,
        DealerEmployeeTerminationModule,
    ],
    entryComponents: [DealerEmployeeListComponent,DealerEmployeeAddEditComponent,DealerEmployeeViewComponent],
    providers: [
        DatePipe
    ]
})
export class EmployeeManagementModule { }
