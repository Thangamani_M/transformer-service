import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from "@modules/dealer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list-component.html'
})
export class DealerEmployeeListComponent extends PrimeNgTableVariablesModel {

  primengTableConfigProperties: any;
  row: any = {}
  loading: boolean;
  loggedUser: any;
  dealerId = '';

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private router: Router) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Employee List",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Employee List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Employee List',
            dataKey: 'employeeId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'firstName', header: 'Name', width: '200px' },
              { field: 'lastName', header: 'Surname', width: '200px' },
              { field: 'dealerName', header: 'Dealer Name', width: '200px' },
              { field: 'branchName', header: 'Branch Name', width: '200px' },
              { field: 'said', header: 'SA ID', width: '200px' },
              { field: 'contactNumber', header: 'Contact Number', width: '200px' },
              { field: 'roleName', header: 'Role Name', width: '200px' },
              { field: 'psiraRegistrationNumber', header: 'PSIRA Registration Number', width: '210px' },
              { field: 'psiraExpiryDate', header: 'PSIRA Expiry Date', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: false,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALERS_EMPLOYEE,
            moduleName: ModulesBasedApiSuffix.DEALER,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getDealerId();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_STAFF_REGISTRATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDealerId() {
    let dealerModuleApiSuffixModels = DealerModuleApiSuffixModels.DEALER_EMPLOYE_USER_DETAILS;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareRequiredHttpParams({
        UserId: this.loggedUser.userId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.dealerId = response.resources.dealerId ? response.resources.dealerId : '';
          this.getDealerEmployeeListData();
        } else {
          this.getDealerEmployeeListData();
        }
      });
  }
  getDealerEmployeeListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels;
    let url = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel + '/list';
    dealerModuleApiSuffixModels = url;
    otherParams = { ...otherParams, ...{ dealerId: this.dealerId } }
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getDealerEmployeeListData(row["pageIndex"], row["pageSize"], unknownVar);
        break;
      default:
    }
  }
  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/dealer/employee-management/add-edit'], { queryParams: { dealerId: this.dealerId } });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/dealer/employee-management/view'], { queryParams: { id: editableObject['employeeId'], dealerId: this.dealerId } })
        break;
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
