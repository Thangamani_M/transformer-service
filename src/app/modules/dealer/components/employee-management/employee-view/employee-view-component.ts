import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { DealerModuleApiSuffixModels, DEALER_COMPONENT } from "@modules/dealer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/components/dynamicdialog/dialogservice";
import { combineLatest } from "rxjs";
import { DealerEmployeeReinstateComponent } from "../dealer-employee-reinstate/dealer-employee-reinstate-component";
import { DealerEmployeeSuspensionComponent } from "../dealer-employee-suspension/dealer-employee-suspension-component";
import { DealerEmployeeTerminationComponent } from "../dealer-employee-termination/dealer-employee-termination-component";

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view-component.html',
})
export class DealerEmployeeViewComponent {

  stockIdDetails: any;
  referenceId: any;
  primengTableConfigProperties;
  userData;
  employeeId: any;
  selectedTabIndex = 0;
  dealerId = '';
  employpeeDetails;
  suspensionId: string;
  techTestingPassword;
  said;
  isShowSaid: boolean = false;
  isShowTechTestingPassword: boolean = false;
  constructor(public dialogService: DialogService,
    private rxjsService: RxjsService, private router: Router,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private crudService: CrudService) {
    this.employeeId = this.activatedRoute.snapshot.queryParams.id;
    this.dealerId = this.activatedRoute.snapshot.queryParams.dealerId ? this.activatedRoute.snapshot.queryParams.dealerId : '';
    this.primengTableConfigProperties = {
      tableCaption: 'View Employee',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Employee List', relativeRouterUrl: '/dealer/employee-management', }, { displayName: 'View Employee', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'View Employee',
            dataKey: 'employeeId',
            enableBreadCrumb: true,
            enableAction: true,
            url: '',
            enableEditActionBtn: true,
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getEmployeeDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_STAFF_REGISTRATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  details;
  getEmployeeDetails() {

    this.details = DealerModuleApiSuffixModels.DEALERS_EMPLOYEE + '/details';
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      this.details,
      undefined,
      false, prepareRequiredHttpParams({ DealerId: this.dealerId, EmployeeId: this.employeeId })
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.resources) {
        this.employpeeDetails = data.resources;
        this.said = this.employpeeDetails?.said;
        this.techTestingPassword = this.employpeeDetails?.techTestingPassword;
        this.employpeeDetails.said = this.hasing(this.employpeeDetails?.said);
        this.employpeeDetails.techTestingPassword = this.hasing(this.employpeeDetails?.techTestingPassword);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  hasing(val: string): any {
    let temp = ''; let len = val?.length;
    for (let i = 0; i < len; i++) {
      temp = temp + '*';
    }
    return temp;
  }
  showSaid() {
    this.employpeeDetails.said = this.said;
    this.isShowSaid = !this.isShowSaid;
  }
  hideSaid() {
    this.employpeeDetails.said = this.hasing(this.employpeeDetails.said);
    this.isShowSaid = !this.isShowSaid;
  }
  showTechTestingPassword() {

    this.employpeeDetails.techTestingPassword = this.techTestingPassword;

    this.isShowTechTestingPassword = !this.isShowTechTestingPassword;
  }
  hideTechTestingPassword() {
    this.employpeeDetails.techTestingPassword = this.hasing(this.employpeeDetails.techTestingPassword);

    this.isShowTechTestingPassword = !this.isShowTechTestingPassword;
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.GET:
        break;
      default:
        break;
    }
  }
  openReinstatePopup() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canReinstate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const ref = this.dialogService.open(DealerEmployeeReinstateComponent, {
      header: 'Reinstate (Dealer and IIP)',
      showHeader: false,
      baseZIndex: 1000,
      width: '400px',
      height: '300px',
      data: {
        row: { employeeId: this.employeeId, dealerEmployeeSuspensionId: this.employpeeDetails.dealersEmployeeSuspensionId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        //  this.viewValue();
        this.getEmployeeDetails()
      }
    });
  }
  openSuspensionPopup() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSuspend) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const ref = this.dialogService.open(DealerEmployeeSuspensionComponent, {
      header: 'Suspensions (Dealer and IIP)',
      showHeader: false,
      baseZIndex: 1000,
      width: '63vw',
      height: '470px',
      data: {
        row: { employeeId: this.employeeId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        // this.viewValue();
        this.getEmployeeDetails()
      }
    });
  }
  openTerminationPopup() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canTermination) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    //DealerTerminationComponent
    const ref = this.dialogService.open(DealerEmployeeTerminationComponent, {
      header: 'Dealer Termination',
      showHeader: false,
      baseZIndex: 1000,
      width: '63vw',
      height: '470px',
      data: {
        row: { employeeId: this.employeeId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        // this.viewValue();
        this.getEmployeeDetails()
      }
    });
  }
  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/dealer/employee-management/add-edit'], { queryParams: { employeeId: this.employeeId, dealerId: this.dealerId } })
  }
}
