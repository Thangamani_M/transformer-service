import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { AppState } from "@app/reducers";
import { countryCodes, CrudService, formConfigs, ModulesBasedApiSuffix, RxjsService, setEmailValidator, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { DealerEmployeeSuspensionModel } from "@modules/dealer/models/dealer-employee-suspension.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-dealer-employee-suspension',
    templateUrl: './dealer-employee-suspension-component.html',
    styleUrls: ['./dealer-employee-suspension-component.scss']
  })
  
  export class DealerEmployeeSuspensionComponent implements OnInit { 

    dealerSuspensionForm: FormGroup;
    reasonSuspensionList=[];
    fileName = '';
    isFileSelected: boolean = false;
    loggedUser: any;
    formData = new FormData();
    showFailedImport: boolean = false;
    countryCodess = countryCodes;
    suspensionReasonList=[];
    startTodayDate= new Date();
    formConfigs = formConfigs;

    constructor(public ref: DynamicDialogRef ,  public config: DynamicDialogConfig,private momentService: MomentService,
      private formBuilder: FormBuilder,private store: Store<AppState>,private rxjsService: RxjsService, private crudService: CrudService) {
       this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
          this.loggedUser = userData;
       });
    }
    ngOnInit(): void {
      this.createForm();
      this.getSuspensionReasonListDropdown();
    }
    getSuspensionReasonListDropdown() {
      this.crudService.dropdown(
        ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_SUSPENSION_REASONS,
        undefined
      ).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
           this.suspensionReasonList = data.resources;
        }
          this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    createForm(dealerSuspensionFormModel?: DealerEmployeeSuspensionModel) {
      let dealerTypeModel = new DealerEmployeeSuspensionModel(dealerSuspensionFormModel);
      this.dealerSuspensionForm = this.formBuilder.group({});
      Object.keys(dealerTypeModel).forEach((key) => {
        if (dealerTypeModel[key] === 'dealerBranchAddEditList') {
          this.dealerSuspensionForm.addControl(key, new FormArray(dealerTypeModel[key]));
        } else {
          this.dealerSuspensionForm.addControl(key, new FormControl());
  
        }
      });     
      this.dealerSuspensionForm.get('phoneNoCountryCode').disable();
      this.dealerSuspensionForm.get('phoneNoCountryCode').setValue('+27');
      this.dealerSuspensionForm.get('isServiceCustomer').setValue("true");  
      this.dealerSuspensionForm = setRequiredValidator(this.dealerSuspensionForm, ["dealerSuspensionReasonId","suspensionDate","isServiceCustomer","reinstatementDate","investigatorPhone","suspensionNotes"]);
      this.dealerSuspensionForm = setEmailValidator(this.dealerSuspensionForm, ["investigatorEmail"]);
      this.onValueChanges();
    }
 
    onValueChanges() {
     //based on immediate effect set date as system generated date as current
     this.dealerSuspensionForm
     .get("isSuspensionImmediateEffect")
     .valueChanges.subscribe((isSuspensionImmediateEffect: boolean) => {
         if(isSuspensionImmediateEffect)
            this.dealerSuspensionForm.get('suspensionDate').setValue(new Date());
         else
            this.dealerSuspensionForm.get('suspensionDate').reset();
     });
     //phn no validation     
      this.dealerSuspensionForm
        .get("phoneNoCountryCode")
        .valueChanges.subscribe((phoneNoCountryCode: string) => {
          this.setPhoneNumberLengthByCountryCode(phoneNoCountryCode, "contact");
        });
      this.dealerSuspensionForm
        .get("investigatorPhone")
        .valueChanges.subscribe((phoneNo: string) => {
          this.setPhoneNumberLengthByCountryCode(
            this.dealerSuspensionForm.get("phoneNoCountryCode").value,
            "contact"
          );
        });
    }
    setPhoneNumberLengthByCountryCode(countryCode: string, str: string) {
      switch (countryCode) {
        case "+27":
          if (str == "contact") {
            this.dealerSuspensionForm
              .get("investigatorPhone")
              .setValidators([
                Validators.minLength(
                  formConfigs.southAfricanContactNumberMaxLength
                ),
                Validators.maxLength(
                  formConfigs.southAfricanContactNumberMaxLength
                ),
              ]);
          }  
          break;
        default:
          if (str == "contact") {
            this.dealerSuspensionForm
              .get("investigatorPhone")
              .setValidators([
                Validators.minLength(formConfigs.indianContactNumberMaxLength),
                Validators.maxLength(formConfigs.indianContactNumberMaxLength),
              ]);
          }
          break;
      }
    }
    obj: any;
    onFileSelected(files: FileList): void {
      this.showFailedImport = false;
      const fileObj = files.item(0);
      this.fileName = fileObj.name;
      if (this.fileName) {
        this.isFileSelected = true;
      }
      const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
      if (fileExtension !== '.xlsx') {
       // this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
       // return;
      }
      this.obj = fileObj;
      if (this.obj.name) {
      }
    }
   
    onSubmit() {
      if (!this.dealerSuspensionForm?.valid) {
        return;
      }
      let formValue = this.dealerSuspensionForm.value;   
      formValue.suspensionDate = (formValue.suspensionDate && formValue.suspensionDate!=null)  ? this.momentService.toMoment(formValue.suspensionDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;  
      let dealerObject = Object.assign({},
        { employeeId:   this.config?.data?.row?.employeeId},
        { suspensionReasonId: formValue.dealerSuspensionReasonId },
        { suspensionDate:formValue.suspensionDate},
        { isImmediateEffect: formValue.isSuspensionImmediateEffect },
        { investigatorName: formValue.investigatorName },
        { investigatorMobileNumber: formValue.investigatorPhone },
        { investigatorEMail: formValue.investigatorEmail },
        { reinstatementDate : formValue.reinstatementDate},
        { isDealerWarrenty: formValue.isServiceCustomer },
        { notes: formValue.suspensionNotes },  
        { isActive:true},
        { createdUserId: this.loggedUser.userId }
      );
      // this.isSubmitted = true;

      let filterdNewData = Object.entries(dealerObject).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

      if(this.obj)
          this.formData.append("file", this.obj);  
  
      this.formData.append('Obj', JSON.stringify(filterdNewData));
      let api =  this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_EMPLOYEE_SUSPENSION, this.formData);
       api.subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
        }
        this.ref.close(res);
        this.rxjsService.setDialogOpenProperty(false);
      })
    }
    btnCloseClick() {
      this.ref.close(false);
    }
  }