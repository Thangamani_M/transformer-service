import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerEmployeeReinstateComponent } from './dealer-employee-reinstate-component';

@NgModule({
    declarations: [DealerEmployeeReinstateComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerEmployeeReinstateComponent],
    exports: [DealerEmployeeReinstateComponent],
})
export class DealerEmployeeReinstateModule { }
