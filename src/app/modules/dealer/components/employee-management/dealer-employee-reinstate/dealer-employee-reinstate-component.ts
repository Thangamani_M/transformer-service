import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { DealerEmployeeReinstateModel } from "@modules/dealer/models/dealer-employee-reinstate.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-dealer-employee-reinstate',
    templateUrl: './dealer-employee-reinstate-component.html',
    styleUrls: ['./dealer-employee-reinstate-component.scss']
  })
  
  export class DealerEmployeeReinstateComponent implements OnInit { 

    dealerReinstateForm:FormGroup;
    reasonSuspensionList=[];
    fileName = '';
    isFileSelected: boolean = false;
    loggedUser:any;
    startTodayDate= new Date();

    constructor(private rxjsService: RxjsService, private crudService: CrudService,private ref: DynamicDialogRef, private momentService: MomentService, private formBuilder: FormBuilder,private store: Store<AppState>, public config: DynamicDialogConfig){
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
          this.loggedUser = userData;
       });
     }
    ngOnInit(): void {
       this.createForm();
    }
    createForm(dealerReinstateFormModel?: DealerEmployeeReinstateModel) {
      let dealerReinstateModel = new DealerEmployeeReinstateModel(dealerReinstateFormModel);
      this.dealerReinstateForm = this.formBuilder.group({});
      Object.keys(dealerReinstateModel).forEach((key) => {
          this.dealerReinstateForm.addControl(key, new FormControl());  
      });     
      this.dealerReinstateForm = setRequiredValidator(this.dealerReinstateForm, ["reinstatementDate","reinstatementNotes"]);
      this.onValueChanges();
    }
    onValueChanges() {
      //based on immediate effect set date as system generated date as current
      this.dealerReinstateForm
      .get("isReinstatementImmediateEffect")
      .valueChanges.subscribe((isReinstatementImmediateEffect: boolean) => {
          if(isReinstatementImmediateEffect)
             this.dealerReinstateForm.get('reinstatementDate').setValue(new Date());
          else
             this.dealerReinstateForm.get('reinstatementDate').reset();
      });
    }
    onSubmit() {
      if (!this.dealerReinstateForm?.valid) {
        return;
      }
      let formValue = this.dealerReinstateForm.value;   
      formValue.reinstatementDate = (formValue.reinstatementDate && formValue.reinstatementDate!=null)  ? this.momentService.toMoment(formValue.reinstatementDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;  
      let dealerReinstateObject = {
          employeeId:   this.config?.data?.row?.employeeId,
          reinstatementDate: formValue.reinstatementDate ,
          dealerEmployeeSuspensionId :  this.config?.data?.row?.dealerEmployeeSuspensionId,
          isImmediateEffect: formValue.isReinstatementImmediateEffect ? formValue.isReinstatementImmediateEffect : false ,
          notes: formValue.reinstatementNotes ,
          isActive:true,
          modifiedUserId: this.loggedUser.userId,
      }
      let api = this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_EMPLOYE_REINSTATE,dealerReinstateObject);
      api.subscribe((res: any) => {
       if (res?.isSuccess == true && res?.statusCode == 200) {
       }
       this.ref.close(res);
       this.rxjsService.setDialogOpenProperty(false);
     })
    }
    btnCloseClick() {
      this.ref.close(false);
    }
  }