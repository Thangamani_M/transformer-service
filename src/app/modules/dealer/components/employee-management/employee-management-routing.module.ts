import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerEmployeeAddEditComponent } from './employee-add-edit/employee-add-edit-component';
import { DealerEmployeeListComponent } from './employee-list/employee-list-component';
import { DealerEmployeeViewComponent } from './employee-view/employee-view-component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: DealerEmployeeListComponent, canActivate: [AuthGuard], data: { title: 'Dealer Employee List' } },
  { path: 'add-edit', component: DealerEmployeeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Dealer Add/Edit Order' } },
  { path: 'view', component: DealerEmployeeViewComponent, canActivate: [AuthGuard], data: { title: 'Dealer View Order' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class EmployeeManagementRoutingModule { }
