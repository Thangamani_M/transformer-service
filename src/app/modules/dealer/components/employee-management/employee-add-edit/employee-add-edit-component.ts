import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { countryCodes, CrudService, CrudType, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, removeAllFormValidators, RxjsService, setRequiredValidator, setSAIDValidator, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { DealerEmployeeAddEditModel } from "@modules/dealer/models/dealer-employee.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-employee-add-edit',
  templateUrl: './employee-add-edit-component.html',
  styleUrls: ['./employee-add-edit-component.scss']
})
export class DealerEmployeeAddEditComponent {


  primengTableConfigProperties;
  viewable: boolean;
  userData;
  selectedTabIndex = 0;
  dealerEmployeeAddEditForm: FormGroup;
  roleList = [];
  dealerBranchList = [];
  countryCodes = countryCodes;
  startTodayDate = new Date();
  showFailedImport: boolean = false;
  codeofConductFileName = '';
  psiraRegisterationDocumentFileName = '';
  profilePictureFileName = '';
  otherDocumentFileName = '';
  isFileSelected: boolean = false;
  dealerId = '';
  formData = new FormData();
  employpeeDetails;
  employeeId;
  dealerBranchId;
  dealerBranchAddEditForm: any;
  imgArr = ['jpg', 'jpeg', 'jpe', 'jif', 'jfi', 'png', 'gif', 'tif'];
  isProfileImg: boolean = true;
  hideSaid: boolean = true;
  hideTecPwd: boolean = true;
  isSubmit = false;
  codeofConductObj; psiraDocumentObj; profilePictureObj; OtherDocumentObj;
  codeofConductObjTemp = { name: null }; psiraDocumentObjTemp = { name: null };
  profilePictureObjTemp = { name: null }; OtherDocumentObjTemp = { name: null };
  mandatoryFields = {
    Name: false, Surname: false, SAID: false, ContactNumber: false,
    PSIRARegistrationNumber: false, PSIRAExpiryDate: false, DeviceUUID: false,
    TechTestingMobileNumber: false, TechTestingPassword: false,
    EmployedDate: false, PSIRAGrade: false, CodeofConduct: false,
    PSIRARegistrationDocument: false, ProfilePicture: false,
    OtherDocument: false
  };

  constructor(private datePipe: DatePipe, private momentService: MomentService, private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,) {
    this.employeeId = this.activatedRoute.snapshot.queryParams.employeeId;
    this.dealerId = this.activatedRoute.snapshot.queryParams.dealerId ? this.activatedRoute.snapshot.queryParams.dealerId : '';
    this.primengTableConfigProperties = {
      tableCaption: 'Create Employee',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Employee List', relativeRouterUrl: '/dealer/employee-management', }, { displayName: 'Create Employee', relativeRouterUrl: '', queryParams: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Create Employee',
            dataKey: 'branchId',
            enableBreadCrumb: true,
            enableAction: false,
            url: '',
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })

  }
  ngOnInit(): void {
    let list = this.getDealerBranch();
    this.createForm();
    this.getRole();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.employeeId) {
      this.getEmployeeDetails();
      this.primengTableConfigProperties.tableCaption = 'Update Employee';
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'View Employee';
      this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = '/dealer/employee-management/view';
      this.primengTableConfigProperties.breadCrumbItems[2].queryParams = { id: this.employeeId, dealerId: this.dealerId };;
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Update Employee', relativeRouterUrl: '' });
    }
  }

  createForm(dealerEmployeeAddEditModel?: DealerEmployeeAddEditModel) {
    let dealerTypeModel = new DealerEmployeeAddEditModel(dealerEmployeeAddEditModel);
    this.dealerEmployeeAddEditForm = this.formBuilder.group({});
    Object.keys(dealerTypeModel).forEach((key) => {
      this.dealerEmployeeAddEditForm.addControl(key, new FormControl());
    });
    this.dealerEmployeeAddEditForm.get('contactNumberCountryCode').setValue('+27');
    this.dealerEmployeeAddEditForm.get('techTestingMobileNumberCountryCode').setValue('+27');
    this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, [
      "roleId", "dealerBranchId", "emailAddress"
    ]);
    // if(this.employeeId) {
    //   this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, [
    //     "roleId", "firstName", "surname", "said", "contactNumber","employedDate","psiraExpiryDate","psiraRegistrationNumber", "deviceUUID", "techTestingMobileNumber", "techTestingPassword"
    //   ]);
    // } else {
    //   this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, [
    //     "roleId", "firstName", "surname", "said", "contactNumber","employedDate","psiraExpiryDate","psiraRegistrationNumber", "dealerBranchId"
    //   ]);
    // }
    this.onValueChanges();
  }
  onValueChanges() {
    this.dealerEmployeeAddEditForm
      .get("roleId")
      .valueChanges.subscribe((roleId: string) => {
        this.getMandatoryFields(roleId);
      });
    this.dealerEmployeeAddEditForm
      .get("contactNumberCountryCode")
      .valueChanges.subscribe((phoneNoCountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(phoneNoCountryCode, "contact1");
      });
    this.dealerEmployeeAddEditForm
      .get("contactNumber")
      .valueChanges.subscribe((phoneNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dealerEmployeeAddEditForm.get("contactNumberCountryCode").value,
          "contact1"
        );
      });
    this.dealerEmployeeAddEditForm
      .get("techTestingMobileNumberCountryCode")
      .valueChanges.subscribe((phoneNoCountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(phoneNoCountryCode, "contact2");
      });
    this.dealerEmployeeAddEditForm
      .get("techTestingMobileNumber")
      .valueChanges.subscribe((phoneNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dealerEmployeeAddEditForm.get("techTestingMobileNumberCountryCode").value,
          "contact2"
        );
      });

  }
  setPhoneNumberLengthByCountryCode(countryCode: string, type: any) {
    switch (countryCode) {
      case "+27":
        if (type == 'contact1') {
          this.dealerEmployeeAddEditForm.get('contactNumber').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
          Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        } else if (type == 'contact2') {
          this.dealerEmployeeAddEditForm.get('techTestingMobileNumber').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
          Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        }
        break;
      default:
        if (type == 'contact1') {
          this.dealerEmployeeAddEditForm.get('contactNumber').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
          Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        } else if (type == 'contact2') {
          this.dealerEmployeeAddEditForm.get('techTestingMobileNumber').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
          Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        }
        break;
    }
  }
  getRole() {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.UX_DEALER_EMPLOYEE_ROLES,
      undefined
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.roleList = data.resources;
      }
    });
  }
  getMandatoryFields(roleId) {
    this.mandatoryFields = {
      Name: false, Surname: false, SAID: false, ContactNumber: false,
      PSIRARegistrationNumber: false, PSIRAExpiryDate: false, DeviceUUID: false,
      TechTestingMobileNumber: false, TechTestingPassword: false,
      EmployedDate: false, PSIRAGrade: false, CodeofConduct: false,
      PSIRARegistrationDocument: false, ProfilePicture: false, OtherDocument: false
    };
    // this.dealerEmployeeAddEditForm.reset();
    this.dealerEmployeeAddEditForm = removeAllFormValidators(this.dealerEmployeeAddEditForm);
    this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, [
      "roleId", "dealerBranchId", "emailAddress"
    ]);
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_MANDATORY_FIELDS_CONFIG,
      roleId
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        // this.roleList = data.resources;
        if (data.resources && data.resources.length > 0) {
          data.resources.forEach(element => {
            if (element.isRequired) {
              switch (element.mandatoryFieldName) {
                case "Name":
                  this.mandatoryFields.Name = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, [
                    "firstName"
                  ]);
                  break;
                case "Surname":
                  this.mandatoryFields.Surname = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["surname"]);
                  break;
                case "SAID":
                  this.mandatoryFields.SAID = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["said"]);
                  this.dealerEmployeeAddEditForm = setSAIDValidator(this.dealerEmployeeAddEditForm, ["said"]);
                  break;
                case "Contact Number":
                  this.mandatoryFields.ContactNumber = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["contactNumber"]);
                  break;
                case "PSIRA Registration Number":
                  this.mandatoryFields.PSIRARegistrationNumber = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["psiraRegistrationNumber"]);
                  break;
                case "PSIRA Expiry Date":
                  this.mandatoryFields.PSIRAExpiryDate = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["psiraExpiryDate"]);
                  break;
                case "Device UUID":
                  this.mandatoryFields.DeviceUUID = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["deviceUUID"]);
                  break;
                case "Tech Testing Mobile Number":
                  this.mandatoryFields.TechTestingMobileNumber = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["techTestingMobileNumber"]);
                  break;
                case "Tech Testing Password":
                  this.mandatoryFields.TechTestingPassword = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["techTestingPassword"]);
                  break;
                case "Employed Date":
                  this.mandatoryFields.EmployedDate = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["employedDate"]);
                  break;
                case "PSIRA Grade":
                  this.mandatoryFields.PSIRAGrade = true;
                  this.dealerEmployeeAddEditForm = setRequiredValidator(this.dealerEmployeeAddEditForm, ["psiraGradeNo"]);
                  break;
                case "Code of Conduct":
                  this.mandatoryFields.CodeofConduct = true;
                  break;
                case "PSIRA Registration Document":
                  this.mandatoryFields.PSIRARegistrationDocument = true;
                  break;
                case "Profile Picture":
                  this.mandatoryFields.ProfilePicture = true;
                  break;
                case "Other Document":
                  this.mandatoryFields.OtherDocument = true;
                  break;
              }
            }
          });
        }

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  getDealerBranch() {
    return this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.UX_DEALER_EMPLOYEE_BRANCH, prepareRequiredHttpParams({
        dealerId: this.dealerId
      }), 1).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
          this.dealerBranchList = data.resources;
          return data.resources;
        }
      });
  }
  details;
  getEmployeeDetails() {

    this.details = DealerModuleApiSuffixModels.DEALERS_EMPLOYEE + '/details';
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      this.details,
      undefined,
      false, prepareRequiredHttpParams({ DealerId: this.dealerId, EmployeeId: this.employeeId })
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.resources) {
        this.employpeeDetails = data.resources;
        this.setValue();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  setValue() {
    this.dealerEmployeeAddEditForm.get("roleId").setValue(this.employpeeDetails.roleId);
    this.dealerEmployeeAddEditForm.get("firstName").setValue(this.employpeeDetails.firstName);
    this.dealerEmployeeAddEditForm.get("surname").setValue(this.employpeeDetails.lastName);
    this.dealerEmployeeAddEditForm.get("said").setValue(this.employpeeDetails.said);
    this.dealerEmployeeAddEditForm.get("deviceUUID").setValue(this.employpeeDetails.deviceUUID);
    this.dealerEmployeeAddEditForm.get("contactNumber").setValue(this.employpeeDetails.contactNumber);
    this.dealerEmployeeAddEditForm.get("psiraGradeNo").setValue(this.employpeeDetails.psiraGradeNo);
    this.dealerEmployeeAddEditForm.get("psiraRegistrationNumber").setValue(this.employpeeDetails.psiraRegistrationNumber);
    this.dealerEmployeeAddEditForm.get("techTestingMobileNumber").setValue(this.employpeeDetails.techTestingMobileNumber);
    this.dealerEmployeeAddEditForm.get("techTestingPassword").setValue(this.employpeeDetails.techTestingPassword);
    this.dealerEmployeeAddEditForm.get("emailAddress").setValue(this.employpeeDetails.emailAddress);
    this.dealerEmployeeAddEditForm.get("psiraExpiryDate").setValue(new Date(this.employpeeDetails.psiraExpiryDate));
    this.dealerEmployeeAddEditForm.get("employedDate").setValue(new Date(this.employpeeDetails.employedDate));

    this.codeofConductObjTemp.name = this.employpeeDetails?.conductDocumentName;
    this.psiraDocumentObjTemp.name = this.employpeeDetails?.psiraRegistrationDocumentName;
    this.profilePictureObjTemp.name = this.employpeeDetails?.conductDocumentName;
    this.OtherDocumentObjTemp.name = this.employpeeDetails?.otherDocumentName;

    //set branch details
    let branchDetails = [];
    if (this.employpeeDetails?.branchId && this.employpeeDetails?.branchId.length > 0) {
      this.getData().then(data => {
        this.dealerBranchList = data.resources;
        this.employpeeDetails.branchId.forEach((element) => {
          if (this.dealerBranchList && this.dealerBranchList.length > 0) {
            let filter = this.dealerBranchList.filter(x => x.id == element);
            if (filter && filter.length > 0)
              branchDetails.push({ id: element, displayName: filter[0].displayName });
          }
        });
        this.dealerEmployeeAddEditForm.get("dealerBranchId").setValue(branchDetails);
      });
    }
    this.codeofConductFileName = this.employpeeDetails.conductDocumentName;
    this.psiraRegisterationDocumentFileName = this.employpeeDetails.psiraRegistrationDocumentName;
    this.profilePictureFileName = this.employpeeDetails.profileImage;
    this.otherDocumentFileName = this.employpeeDetails.otherDocumentName;
  }

  showSaid() {
    this.hideSaid = !this.hideSaid;
  }
  showTecPwd() {
    this.hideTecPwd = !this.hideTecPwd;
  }
  getData() {
    return this.crudService.dropdown(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.UX_DEALER_EMPLOYEE_BRANCH, prepareRequiredHttpParams({
        dealerId: this.dealerId
      }), 1).toPromise();
  }

  onFileSelected(files: FileList, num: Number): void {
    this.showFailedImport = false;
    const fileObj = files.item(0);
    if (num == 0) {
      this.codeofConductFileName = 'CodeofConduct - ' + fileObj.name;
      this.codeofConductObj = fileObj;
      this.codeofConductObjTemp = fileObj;
    } else if (num == 1) {
      this.psiraRegisterationDocumentFileName = 'PsiraRegisteration - ' + fileObj.name;
      this.psiraDocumentObj = fileObj;
      this.psiraDocumentObjTemp = fileObj;
    }
    else if (num == 2) {
      this.profilePictureFileName = fileObj.name;
      this.profilePictureObj = fileObj;
      this.profilePictureObjTemp = fileObj;
      this.isProfileImg = this.check(fileObj);
      if (!this.isProfileImg)
        return;
    }
    else if (num == 3) {
      this.otherDocumentFileName = 'Other - ' + fileObj.name;
      this.OtherDocumentObj = fileObj;
      this.OtherDocumentObjTemp = fileObj;
    }
    if (this.codeofConductFileName || this.psiraRegisterationDocumentFileName ||
      this.profilePictureFileName || this.otherDocumentFileName) {
      this.isFileSelected = true;
    }

  }
  check(fileObj) {
    let fileExtension; fileExtension = fileObj.name.split("."); fileExtension = fileExtension[fileExtension.length - 1];
    let len = this.imgArr.filter(x => x == fileExtension.toLocaleLowerCase());
    if (len.length == 0)
      return false;
    else
      return true;
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) { }
  }
  resendVerficationLink() {
    this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.RESEND_VERIFICATION_LINK, {
      modifiedUserId: this.userData.userId,
      employeeId: this.employeeId
    })
      .subscribe((response: IApplicationResponse) => {

      });
  }
  creatAPI; updateAPI;
  onSubmit() {
    // if (this.dealerEmployeeAddEditForm.invalid || !this.isProfileImg) {
    this.isSubmit = true;
    this.formData = new FormData();
    if (this.dealerEmployeeAddEditForm.invalid) {
      this.dealerEmployeeAddEditForm.markAllAsTouched();
      return;
    }
    if ((this.mandatoryFields.CodeofConduct && !this.codeofConductObjTemp?.name) || (this.mandatoryFields.PSIRARegistrationDocument && !this.psiraDocumentObjTemp?.name)
      || (this.mandatoryFields.ProfilePicture && !this.profilePictureObjTemp?.name) || (this.mandatoryFields.OtherDocument && !this.OtherDocumentObjTemp?.name)
    ) {

      return;
    }
    let formValue = this.dealerEmployeeAddEditForm.getRawValue();
    formValue.psiraExpiryDate = formValue.psiraExpiryDate ? this.momentService.toMoment(formValue.psiraExpiryDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    formValue.employedDate = formValue.employedDate ? this.momentService.toMoment(formValue.employedDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    if (formValue.contactNumber)
      formValue.contactNumber = formValue?.contactNumber?.toString()?.replace(/\s/g, "");
    else
      formValue.contactNumber = formValue.contactNumber;

    if (formValue.techTestingMobileNumber)
      formValue.techTestingMobileNumber = formValue?.techTestingMobileNumber?.toString()?.replace(/\s/g, "");
    else
      formValue.techTestingMobileNumber = formValue.techTestingMobileNumber;

    let dealerBranchIdArr = [];
    if (formValue.dealerBranchId && formValue.dealerBranchId.length > 0) {
      formValue.dealerBranchId.forEach(element => {
        dealerBranchIdArr.push(element.id);
      });
    }

    let finalObject = {
      DealerNo: null,
      employeeId: this.employeeId ? this.employeeId : null,
      firstName: formValue.firstName,
      Surname: formValue.surname,
      emailAddress: formValue.emailAddress,
      contactNumber: formValue.contactNumber,
      contactNumberCountryCode: formValue.contactNumberCountryCode,
      dealerBranchId: dealerBranchIdArr,
      roleId: formValue.roleId,
      status: "Active",
      said: formValue.said,
      psiraGradeNo: formValue.psiraGradeNo,
      psiraRegistrationNumber: formValue.psiraRegistrationNumber,
      psiraExpiryDate: formValue.psiraExpiryDate,
      deviceUUID: formValue.deviceUUID,
      mobileNo: formValue.techTestingMobileNumber, //pass same tech num
      mobileNoCountryCode: formValue.techTestingMobileNumberCountryCode, //pass same tech num
      techTestingMobileNumber: formValue.techTestingMobileNumber,
      techTestingMobileNumberCountryCode: formValue.techTestingMobileNumberCountryCode,
      techTestingPassword: formValue.techTestingPassword,
      employedDate: formValue.employedDate,
      createdUserId: this.userData.userId,
      dealerId: this.dealerId
    }


    if (this.codeofConductObj)
      this.formData.append('file', this.codeofConductObj, this.codeofConductFileName);
    if (this.profilePictureObj)
      this.formData.append('file', this.profilePictureObj, this.profilePictureFileName);
    if (this.psiraDocumentObj)
      this.formData.append('file', this.psiraDocumentObj, this.psiraRegisterationDocumentFileName);
    if (this.OtherDocumentObj)
      this.formData.append('file', this.OtherDocumentObj, this.otherDocumentFileName);

    this.formData.append('data', JSON.stringify(finalObject));
    let isPrincpleIdExists = this.employeeId ? true : false;
    this.creatAPI = DealerModuleApiSuffixModels.DEALERS_EMPLOYEE + '/create';
    this.updateAPI = DealerModuleApiSuffixModels.DEALERS_EMPLOYEE + '/update';
    let api = isPrincpleIdExists ? this.crudService.update(ModulesBasedApiSuffix.DEALER, this.updateAPI, this.formData) : this.crudService.create(ModulesBasedApiSuffix.DEALER, this.creatAPI, this.formData);
    api.subscribe((res: any) => {
      this.isSubmit = false;
      this.formData = new FormData();
      if (res?.isSuccess == true && res?.statusCode == 200) {
        if (isPrincpleIdExists == true) {
          this.getEmployeeDetails();
          this.hideSaid = true
          this.hideTecPwd = true;
        }
        else if (isPrincpleIdExists == false)
          this.router.navigate(['dealer/employee-management']);
      }
      this.rxjsService.setDialogOpenProperty(false);
    })
  }
}