import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerEmployeeTerminationComponent } from './dealer-employee-termination-component';

@NgModule({
    declarations: [DealerEmployeeTerminationComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [DealerEmployeeTerminationComponent],
    exports: [DealerEmployeeTerminationComponent],
})
export class DealerEmployeeTerminationModule { }
