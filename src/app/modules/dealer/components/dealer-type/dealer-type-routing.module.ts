import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerTypeListComponent } from '@modules/dealer';
import { AuthenticationGuard as AuthGuard, } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: DealerTypeListComponent,canActivate:[AuthGuard],data: { title: 'Dealer Type' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class DealerTypeRoutingModule { }
