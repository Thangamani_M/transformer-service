import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, MaterialModule, SharedModule } from '@app/shared';
import { DealerTypeAddEditComponent, DealerTypeListComponent } from '@modules/dealer';
import { DealerTypeRoutingModule } from './dealer-type-routing.module';

@NgModule({
    declarations: [DealerTypeListComponent, DealerTypeAddEditComponent],
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        DealerTypeRoutingModule
    ],
    entryComponents: [DealerTypeAddEditComponent],
})
export class DealerTypeModule { }
