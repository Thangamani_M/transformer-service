import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { DealerModuleApiSuffixModels, DealerTypeAddEditComponent, DEALER_COMPONENT } from '@modules/dealer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-dealer-type',
  templateUrl: './dealer-type-list.component.html'
})
export class DealerTypeListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {}
  loggedUser: any;


  constructor(private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private dialogService: DialogService, private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
     super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Type",
      breadCrumbItems: [{ displayName: 'Dealer Management', relativeRouterUrl: '' }, { displayName: 'Configuration', relativeRouterUrl: '' },{ displayName: 'Dealer Type ', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Type',
            dataKey: 'dealerTypeId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'dealerTypeName', header: 'Dealer Type Name', width: '200px' },
              { field: 'description', header: 'Description', width: '200px' }
            ],
            enableMultiDeleteActionBtn: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_TYPE_LIST,
            moduleName: ModulesBasedApiSuffix.DEALER,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getDealerTypeListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][DEALER_COMPONENT.DEALER_TYPE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getDealerTypeListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getDealerTypeListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
        case CrudType.DELETE:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }else {
            this.onOneOrManyRowsDelete(row);
          }

          // this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          //   this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {

          //     if (result) {
          //       this.selectedRows = [];
          //       this.getDealerTypeListData();
          //     }
          //   });
          break;
      default:
    }
  }
  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
        method: 'delete',
        dataObject: {
          ids: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
          modifiedUserId: this.loggedUser.userId,
          isDeleted: true,
        }
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.getDealerTypeListData();
      }
    });
  }
  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPopup('Create Dealer Type', editableObject, 'create');
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.openAddEditPopup('Update Dealer Type', editableObject, 'view');
            break;
        }
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }

  openAddEditPopup(header, row, screen) {
    const rowData = { ...row };
    const ref = this.dialogService.open(DealerTypeAddEditComponent, {
      header: header,
      baseZIndex: 1000,
      width: '650px',
      closable: false,
      showHeader: false,
      data: {
        createdUserId: this.loggedUser ? this.loggedUser?.userId : null,
        row: rowData,
        screen: screen
      },
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getDealerTypeListData();
      }
    });
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}
