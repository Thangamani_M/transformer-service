import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DealerTypeModel } from '@modules/dealer/models/dealer-type.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-dealer-type-add-edit',
  templateUrl: './dealer-type-add-edit.component.html'
})
export class DealerTypeAddEditComponent implements OnInit {

  dealerTypeDialogForm: FormGroup;
  isSubmitted: boolean;
  loggedUser: any;
  isSubmit: boolean = true;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, private store: Store<AppState>,
    private crudService: CrudService, private formBuilder: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(dealerTypeFormModel?: DealerTypeModel) {
    let dealerTypeModel = new DealerTypeModel(dealerTypeFormModel);
    this.dealerTypeDialogForm = this.formBuilder.group({});
    Object.keys(dealerTypeModel).forEach((key) => {
      if (dealerTypeModel[key] === 'dealerTypeCodeList') {
        this.dealerTypeDialogForm.addControl(key, new FormArray(dealerTypeModel[key]));
      } else {
        this.dealerTypeDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : dealerTypeModel[key]));
        if (this.config?.data?.screen == 'view') {

        }
      }
    });
    this.dealerTypeDialogForm = setRequiredValidator(this.dealerTypeDialogForm, ["dealerTypeName", "description"]);
  }
  btnCloseClick() {
    this.ref.close(false);
  }

  onSubmitDelaerTypeDialog() {
    if (this.isSubmitted || !this.dealerTypeDialogForm?.valid) {
      return;
    }
    let dealerTypeObject = {
      dealerTypeName: this.dealerTypeDialogForm.value.dealerTypeName ? this.dealerTypeDialogForm.value.dealerTypeName : null,
      description: this.dealerTypeDialogForm.value.description ? this.dealerTypeDialogForm.value.description : null,
      // isActive: this.dealerTypeDialogForm.value.isActive ?  this.dealerTypeDialogForm.value.isActive : false,
      isActive: true,
      createdUserId: this.loggedUser.userId,
      dealerTypeId: this.config?.data?.row?.dealerTypeId ? this.config?.data?.row?.dealerTypeId : null,
    };
    this.isSubmitted = true;
    let api = dealerTypeObject.dealerTypeId == null ? this.crudService.create(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_TYPE_LIST, dealerTypeObject) : this.crudService.update(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_TYPE_LIST, dealerTypeObject);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
      }
      this.ref.close(res);
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }

}