export enum DealerTaskRequestType {
    DEALER_APPROVAL = 'Dealer Approval',
    DEALER_INSPECTION_FORM = 'Dealer Inspection Form',
    DEALER_CALL_ESCALATION= 'Dealer Call Escalation',
}