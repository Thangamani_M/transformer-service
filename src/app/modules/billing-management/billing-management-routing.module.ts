import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [

  {
    path: 'manual-invoice-approval', loadChildren: () => import('../billing-management/components/manual-invoice/manual-invoice-approval.module').then(m => m.ManualInvoiceApprovalModule)
  },
  {
    path: 'stock-id', loadChildren: () => import('../billing-management/components/stock-id/stock-id.module').then(m => m.StockIdModule)
  },
  {
    path: 'stock-id-approval', loadChildren: () => import('../billing-management/components/stock-id-approval/stock-id-approval.module').then(m => m.BillingStockIdApprovalModule)
  },
  {
    path: 'stock-id-request', loadChildren: () => import('../billing-management/components/stock-id-request/stock-id-request.module').then(m => m.StockIdRequestModule)
  },
  {
    path: 'stock-id-mapping', loadChildren: () => import('../billing-management/components/stock-id-mapping/stock-id-mapping.module').then(m => m.StockIdMappingModule)
  },
  // {
  //   path: 'same-day-run-request', loadChildren: () => import('../billing-management/components/same-day-run-request/same-day-run-request.module').then(m => m.SameDayRunRequestModule)
  // },
  {
    path: 'annual-network-fee', loadChildren: () => import('../billing-management/components/annual-network-fee/annual-network-fee-list.module').then(m => m.AnnualFeeModule)
  },
  {
    path: 'task-list', loadChildren: () => import('../billing-management/components/task-list/task-list.module').then(m => m.TaskListModule)
  },
  {
    path: 'transaction-configuration', loadChildren: () => import('../billing-management/components/transaction-configuration/transaction-configuration-list.module').then(m => m.TransctionDescriptionModule)
  },
  {
    path: 'pricing-run-date-configuration', loadChildren: () => import('../billing-management/components/pricing-run-date-config/pricing-run-date-configuration.module').then(m => m.PricingRunDateConfigurationModule)
  },
  {
    path: 'price-increase-configuration', loadChildren: () => import('../billing-management/components/price-increase-configuration/price-increase-configuration.module').then(m => m.PriceIncreaseConfigurationModule)
  },
  {
    path: 'statement-configuration', loadChildren: () => import('../billing-management/components/statement-config/statement-configuration-list.module').then(m => m.StatementConfigurationModule)
  },
  // {
  //   path: 'adhoc-price-increase-configuration', loadChildren: () => import('../billing-management/components/adhoc-price-increase-configuration/adhoc-price-increase-configuration.module').then(m => m.AdhocPriceIncreaseConfigurationModule)
  // },
  {
    path: 'risk-watch-configuration', loadChildren: () => import('../billing-management/components/risk-watch-configuration/risk-watch-config.module').then(m => m.RiskWatchConfigModule)
  },
  {
    path: 'risk-watch-guard-service', loadChildren: () => import('../billing-management/components/risk-watch-guard-service/risk-watch-config.module').then(m => m.RiskWatchGuardServiceModule)
  },
  // {
  //   path: 'shift-cancel-reason', loadChildren: () => import('../billing-management/components/shift-cancel-reason/shift-cancel-reason-config.module').then(m => m.ShiftCancelReasonModule)
  // },
  {
    path: 'pro-forma-invoice', loadChildren: () => import('../billing-management/components/pro-forma-invoice-repo/pro-forma-invoice-repo.module').then(m => m.ProFormaInvoiceRepoModule)
  },
  {
    path: 'charge-list', loadChildren: () => import('../billing-management/components/charge-list/charge-list.component.module').then(m => m.BillingChargeListModule)
  },
  {
    path: "debi-check-mandate", loadChildren: () => import('../others/configuration/components/debi-check-mandate/debi-check-mandate.module').then(m => m.DebiCheckMandateModule)
  },
  {
    path: 'annual-network-fees-request', loadChildren: () => import('../billing-management/components/annual-network-fees-request/annual-network-fees-request.module').then(m => m.AnnualFeeRequestModule)
  },
  { path: "monthly-test-run-final", loadChildren: () => import('../billing-management/components/monthly-test-run-final/monthly-test-run-final.module').then(m => m.MonthlyTestRunFinalModule) },
  { path: "same-day-run-report", loadChildren: () => import('../billing-management/components/same-day-run-report/same-day-run-report.module').then(m => m.SameDayRunReportModule) },
  { path: "override-report", loadChildren: () => import('../billing-management/components/override-report/override-report.module').then(m => m.OverrideReportModule) },
  {
    path: 'back-dating', loadChildren: () => import('../billing-management/components/back-dating-bulk-import/back-dating.module').then(m => m.BackDatingBulkImportModule)
  },
  {
    path: 'doa-tasklist', loadChildren: () => import('../billing-management/components/doa-tasklist/doa-tasklist.module').then(m => m.DoaTasklistDetailsModule)
  },
  {
    path: 'ammortization-schedule', loadChildren: () => import('../billing-management/components/ammortization-schedule/ammortization-schedule.module').then(m => m.AmmortizationScheduleModule)
  },
  {
    path: 'process-type-transaction-description-mapping', loadChildren: () => import('../billing-management/components/process-type-transaction-description-mapping/process-type-transaction-description-mapping.module').then(m => m.ProcessTypeTransactionDescriptionMappingModule)
  },
  {
    path: 'price-increase-letters', loadChildren: () => import('../billing-management/components/price-increase-letters/price-increase-letters.module').then(m => m.PriceIncreaseLettersModule)
  },
  {
    path: 'fual-price-increase-letter-config', loadChildren: () => import('../billing-management/components/fual-price-incress-letter/fual-price-incress-letter.module').then(m => m.FualPriceIncressLetterModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class BillingManagementRoutingModule { }
