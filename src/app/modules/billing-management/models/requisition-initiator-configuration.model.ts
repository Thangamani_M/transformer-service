class RequisitionInitiatorConfigurationModel {
    createdDate: string;
    fromValue:string;
    toValue:string;
    roleId:string;
    description:string;
    status:boolean;
    constructor(billingChargeListModel: RequisitionInitiatorConfigurationModel) {
        this.createdDate = billingChargeListModel?.createdDate != undefined ? billingChargeListModel?.createdDate : '';
        this.fromValue = billingChargeListModel?.fromValue != undefined ? billingChargeListModel?.fromValue : '';
        this.toValue = billingChargeListModel?.toValue != undefined ? billingChargeListModel?.toValue : '';
        this.roleId = billingChargeListModel?.roleId != undefined ? billingChargeListModel?.roleId : null;
        this.description = billingChargeListModel?.description != undefined ? billingChargeListModel?.description : '';
        this.status =  billingChargeListModel?.status != undefined ? billingChargeListModel?.status : false;

    }
}

export { RequisitionInitiatorConfigurationModel };

