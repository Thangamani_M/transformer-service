class BillingChargeListModel {
    startDate: string;
    endDate: string;
    divisionIds: [];
    debitOrderRunCodeIds: [];
    chargeListTypeId:string;
    originIds: [];
    includeANF:boolean;
    includeERB:boolean;
    isFinalSubmit: boolean;
    constructor(billingChargeListModel: BillingChargeListModel) {
        this.startDate = billingChargeListModel?.startDate != undefined ? billingChargeListModel?.startDate : '';
        this.endDate = billingChargeListModel?.endDate != undefined ? billingChargeListModel?.endDate : '';
        this.divisionIds = billingChargeListModel?.divisionIds != undefined ? billingChargeListModel?.divisionIds : [];
        this.debitOrderRunCodeIds = billingChargeListModel?.debitOrderRunCodeIds != undefined ? billingChargeListModel?.debitOrderRunCodeIds : [];
        this.originIds = billingChargeListModel?.originIds != undefined ? billingChargeListModel?.originIds : [];
        this.chargeListTypeId  = billingChargeListModel?.chargeListTypeId != undefined ? billingChargeListModel?.chargeListTypeId : '';
        this.includeERB =  billingChargeListModel?.includeERB != undefined ? billingChargeListModel?.includeERB : false;
        this.includeANF =   billingChargeListModel?.includeANF != undefined ? billingChargeListModel?.includeANF : false;
        this.isFinalSubmit =   billingChargeListModel?.isFinalSubmit != undefined ? billingChargeListModel?.isFinalSubmit : false;
    }
}

export { BillingChargeListModel };

