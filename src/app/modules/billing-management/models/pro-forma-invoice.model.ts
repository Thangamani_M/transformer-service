class ProFormaInvoiceRepoFilterModel {
    customerRefNo: string;
    customerName: string;
    customerAddress: string;
    region: string;
    division: string;
    branch: string;
    debtorsCode: string;
    debtorsName: string;
    quotationNumber: string;
    proformaInvoiceNumber: string;
    proformaInvoiceDate: string;
    proformaInvoiceAmount: string;

    constructor(dealerMaintenanceFilterModel: ProFormaInvoiceRepoFilterModel) {
        this.customerRefNo = dealerMaintenanceFilterModel?.customerRefNo != undefined ? dealerMaintenanceFilterModel?.customerRefNo : '';
        this.customerName = dealerMaintenanceFilterModel?.customerName != undefined ? dealerMaintenanceFilterModel?.customerName : '';
        this.customerAddress = dealerMaintenanceFilterModel?.customerAddress != undefined ? dealerMaintenanceFilterModel?.customerAddress : '';
        this.region = dealerMaintenanceFilterModel?.region != undefined ? dealerMaintenanceFilterModel?.region : '';
        this.division = dealerMaintenanceFilterModel?.division != undefined ? dealerMaintenanceFilterModel?.division : '';
        this.branch = dealerMaintenanceFilterModel?.branch != undefined ? dealerMaintenanceFilterModel?.branch : '';
        this.debtorsCode = dealerMaintenanceFilterModel?.debtorsCode != undefined ? dealerMaintenanceFilterModel?.debtorsCode : '';
        this.debtorsName = dealerMaintenanceFilterModel?.debtorsName != undefined ? dealerMaintenanceFilterModel?.debtorsName : '';
        this.quotationNumber = dealerMaintenanceFilterModel?.quotationNumber != undefined ? dealerMaintenanceFilterModel?.quotationNumber : '';
        this.proformaInvoiceNumber = dealerMaintenanceFilterModel?.proformaInvoiceNumber != undefined ? dealerMaintenanceFilterModel?.proformaInvoiceNumber : '';
        this.proformaInvoiceDate = dealerMaintenanceFilterModel?.proformaInvoiceDate != undefined ? dealerMaintenanceFilterModel?.proformaInvoiceDate : '';
        this.proformaInvoiceAmount = dealerMaintenanceFilterModel?.proformaInvoiceAmount != undefined ? dealerMaintenanceFilterModel?.proformaInvoiceAmount : '';
    }
}

export { ProFormaInvoiceRepoFilterModel };

