class SameDayRunReportAddModel {
    constructor(sameDayRunReportAddModel?: SameDayRunReportAddModel) {
        this.dailytestrunId = sameDayRunReportAddModel ? sameDayRunReportAddModel.dailytestrunId == undefined ? '' : sameDayRunReportAddModel.dailytestrunId : '';
        this.divisionIds = sameDayRunReportAddModel ? sameDayRunReportAddModel.divisionIds == undefined ? null : sameDayRunReportAddModel.divisionIds : null;
        this.divisionId = sameDayRunReportAddModel ? sameDayRunReportAddModel.divisionId == undefined ? null : sameDayRunReportAddModel.divisionId : null;
        this.runTime = sameDayRunReportAddModel ? sameDayRunReportAddModel.runTime == undefined ? null : sameDayRunReportAddModel.runTime : null;
        this.createdUserId = sameDayRunReportAddModel ? sameDayRunReportAddModel.createdUserId == undefined ? null : sameDayRunReportAddModel.createdUserId : null;
        this.includeERB =  sameDayRunReportAddModel?.includeERB != undefined ? sameDayRunReportAddModel?.includeERB : false;
        this.isFinalSubmit =   sameDayRunReportAddModel?.isFinalSubmit != undefined ? sameDayRunReportAddModel?.isFinalSubmit : false;
        this.debitOrderRunCodeIds = sameDayRunReportAddModel ? sameDayRunReportAddModel.debitOrderRunCodeIds == undefined ? null : sameDayRunReportAddModel.debitOrderRunCodeIds : null;
    }
    dailytestrunId?: string
    divisionIds?: string
    divisionId?: string
    runTime?: Date;
    createdUserId?: string;
    includeERB:boolean;
    isFinalSubmit: boolean;
    debitOrderRunCodeIds: string;
}
export { SameDayRunReportAddModel };

