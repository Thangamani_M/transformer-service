import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallEscalationModule } from '@modules/customer/components/customer/technician-installation/call-escalation-dialog/call-escalation-dialog.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BillingManagementRoutingModule } from './billing-management-routing.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BillingManagementRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    CallEscalationModule
  ],
  providers: []
})
export class BillingManagementModule { }
