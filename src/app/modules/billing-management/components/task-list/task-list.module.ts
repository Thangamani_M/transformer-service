import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallEscalationModule } from '@modules/customer/components/customer/technician-installation/call-escalation-dialog/call-escalation-dialog.module';
import { TaskListStatusModalComponent } from './task-list-modal';
import { TaskListRoutingModule } from './task-list-routing.module';
import { TaskListComponent } from './task-list.component';

@NgModule({
  declarations: [TaskListComponent, TaskListStatusModalComponent],
  imports: [
    CommonModule,
    TaskListRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    CallEscalationModule,
  ],
  exports: [TaskListComponent],
  providers:[DatePipe],
  entryComponents: [TaskListStatusModalComponent]
})
export class TaskListModule { }
