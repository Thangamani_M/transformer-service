import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaskListComponent } from './task-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: TaskListComponent,canActivate:[AuthGuard], data: { title: 'Task List' }},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class TaskListRoutingModule { }
