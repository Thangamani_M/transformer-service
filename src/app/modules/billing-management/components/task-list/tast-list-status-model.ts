class TastListStatusModel {
    escalationId?: string;
    modifiedUserId?: string;
    escalationStatusId?: string
    comments?: string

    constructor(tastListStatusModel?: TastListStatusModel) {
      this.modifiedUserId = tastListStatusModel == undefined ? null : tastListStatusModel.modifiedUserId == undefined ? null : tastListStatusModel.modifiedUserId;
      this.escalationId = tastListStatusModel == undefined ? null : tastListStatusModel.escalationId == undefined ? null : tastListStatusModel.escalationId;
      this.escalationStatusId = tastListStatusModel == undefined ? null : tastListStatusModel.escalationStatusId == undefined ? null : tastListStatusModel.escalationStatusId;
      this.comments = tastListStatusModel == undefined ? null : tastListStatusModel.comments == undefined ? null : tastListStatusModel.comments;

    }
  }

  export { TastListStatusModel };
