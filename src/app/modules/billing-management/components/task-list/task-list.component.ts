import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CallEscalationDialogComponent } from '@modules/customer/components/customer/technician-installation/call-escalation-dialog';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { TaskListStatusModalComponent } from './task-list-modal';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { combineLatest } from 'rxjs';
import { DialogService } from 'primeng/api';
@Component({
    selector: 'task-list.component',
    templateUrl: './task-list.component.html',
})
export class TaskListComponent extends PrimeNgTableVariablesModel {
    userData: UserLogin;
    primengTableConfigProperties: any;
    row: any = {}
    callEscalationsubscritption: any;
    Statuses: any = [];
    preParams: any;
    openAvs = false;
    dataToShow: any;
    constructor(
        private crudService: CrudService,
        private router: Router,
        private rxjsService: RxjsService,
        private store: Store<AppState>,
        private dialog: MatDialog,
        private snackbarService : SnackbarService,
        private dialogService: DialogService,
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Task List",
            breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Task List' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Task List',
                        dataKey: 'refertoSalesFlagTypeId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'escalationRefNo', header: 'Request Number', width: '200px' },
                            { field: 'customerRefNo', header: 'Customer ID', width: '200px' },
                            { field: 'debtorRefNo', header: 'Debtors Code', width: '200px' },
                            { field: 'referenceTypeName', header: 'Process Name', width: '200px' },
                            { field: 'requestType', header: 'Process Descriptions', width: '200px' },
                            { field: 'transactionType', header: 'Transaction Type', width: '200px' },
                            { field: 'transactionId', header: 'Transaction ID', width: '200px' },
                            { field: 'requestType', header: 'Request Type', width: '200px' },
                            { field: 'requestMotivation', header: 'Request Motivation', width: '200px' },
                            { field: 'amount', header: 'Value', width: '200px' },
                            { field: 'divisionCode', header: 'Division', width: '200px' },
                            { field: 'branchName', header: 'Branch', width: '200px' },
                            { field: 'subArea', header: 'Sub Area', width: '200px' },
                            { field: 'techArea', header: 'Tech Area', width: '200px' },
                            { field: 'saleArea', header: 'Sales Area', width: '200px' },
                            { field: 'fullAddress', header: 'Address', width: '200px' },
                            { field: 'creatorRole', header: 'Creator Role', width: '200px' },
                            { field: 'creatorDate', header: 'Creator Date', width: '200px' },
                            { field: 'currentLevel', header: 'Current Level', width: '200px' },
                            { field: 'lastApprovalDoneBy', header: 'Last Approval Done By', width: '200px' },
                            { field: 'lastApprovalDate', header: 'Last Approval Date', width: '200px' },
                            { field: 'escalated', header: 'Escalated', width: '200px' },
                            { field: 'actionedDate', header: 'Actioned Date', width: '200px' },
                            { field: 'dueDate', header: 'Due Date', width: '200px' },
                            { field: 'createdBy', header: 'Created By', width: '200px' },
                            { field: 'escalationStatusName', header: 'Status', width: '200px' },
                        ],
                        shouldShowDeleteActionBtn: false,
                        enableAddActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.DEFAULT_COSUMABLE_CONFIGURATION,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                    }
                ]
            }
        }
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit(): void {
      this.combineLatestNgrxStoreData()
        this.getTaskList();
        this.getStatuses();
    }
    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][BILLING_MODULE_COMPONENT.TASKS]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    getStatuses(): void {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.TECHNICAL_AREA_MANAGEMENT_WORKLIST_STATUS, null, false, null, 1).subscribe((response) => {
                this.Statuses = response.resources;
            });
    }

    getTaskList(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        const preparams = {
            userId: this.userData.userId
            // userId: 'B50B52F8-CDAF-4EB5-A4EE-AA7864402A9B'
        }
        this.preParams = preparams;
        otherParams = { ...otherParams, ...this.preParams };
        this.crudService.get(
            ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.ESCALATION,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                data.resources = null;
                this.dataList = data.resources
                this.totalRecords = 0;
            }
        })
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    onChangeSelecedRows(e) {
      this.selectedRows = e;
    }

    getFeedbackListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FEEDBACK,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                data.resources = null;
                this.dataList = data.resources
                this.totalRecords = 0;
            }
        })
    }

    openSwapApprovalPage(row) {
        this.router.navigate(['/technical-management', 'technical-area-manager-worklist', 'stock-swap-approval'], {
            queryParams: {
                stockSwapId: row['requestId'],
            }
        });
    }

    openPrCallApprovalPage(row, type) {
        this.router.navigate(['/technical-management', 'technical-area-manager-worklist', 'pr-approval'], {
            queryParams: {
                serviceCallApprovalId: row['referenceId'],
                type: type
            }
        });
    }

    openSpecialProjectPage(row) {
        this.router.navigate(['/technical-management', 'technical-area-manager-worklist', 'special-project'], {
            queryParams: {
                specialProjectId: row['requestId'],
            }
        });
    }

    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
        switch (type) {
            case CrudType.CREATE:
                break;
            case CrudType.GET:
                    if (this.row['sortOrderColumn']) {
                        unknownVar['sortOrder'] = this.row['sortOrder'];
                        unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
                }
                this.getTaskList(this.row["pageIndex"], this.row["pageSize"], unknownVar)
                break;
            case CrudType.VIEW:
              if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canApprove) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
                if (row && row['processTypeName'] === 'Manual Invoice Approval') {
                    this.router.navigate(['billing/manual-invoice-approval'], { queryParams: { escalationId: row['escalationId'], createdUserId: this.userData.userId } });

                } else if (row && row['referenceTypeName'].toLowerCase() === 'newbankcreation') {
                    this.openAvs = true;
                    this.dataToShow = row['avsCheckResult'];
                }
                else if(row && row['referenceTypeName'].toLowerCase() == 'priceincreasedatesescalation') {
                    this.downloadFile(row);
                }
                else if (row && row['processTypeName'] == 'Small Balance WriteOff') {
                    this.router.navigate(['collection/small-balance-writeoff/debators'], { queryParams: { escalationId: row['escalationId'] ,type:'task-list'} });
                }
                else if (row && row['processTypeName'] == 'Price Increase Letter') {
                    this.router.navigate(['/billing/price-increase-letters']);
                }
                break;
            case CrudType.EDIT:
                if (row && row['processTypeName'] === 'Manual Invoice Approval') {
                    this.router.navigate(['billing/manual-invoice-approval'], { queryParams: { escalationId: row['escalationId'], createdUserId: this.userData.userId } });

                } else if (row && row['referenceTypeName'].toLowerCase() === 'newbankcreation') {
                    this.openAvs = true;
                    this.dataToShow = row['avsCheckResult'];
                }
                else if(row && row['referenceTypeName'].toLowerCase() == 'priceincreasedatesescalation') {
                    this.downloadFile(row);
                }
                else if (row && row['processTypeName'] == 'Small Balance WriteOff') {
                    this.router.navigate(['collection/small-balance-writeoff/debators'], { queryParams: { escalationId: row['escalationId'] ,type:'task-list'} });
                }
                else if (row && row['processTypeName'] == 'Price Increase Letter') {
                  this.router.navigate(['/billing/price-increase-letters']);
              }
                break;
            case CrudType.STATUS_POPUP:
                this.changeStatus(row);
                break;

            default:
        }
    }
    downloadFile(row) {
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.DOWNLOAD_PRICE_INCREASE_FILE, null, false, prepareRequiredHttpParams({ EscalationId: row['escalationId'] })).subscribe((res: any) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (res?.isSuccess == true && res?.resources) {
                    this.downloadExportedFile(res?.resources,'PriceIncreasedatesEscalation');
                }
        });
    }
    downloadExportedFile(url,fileName){
        var link = document.createElement("a");
        if (link.download !== undefined) {
            link.setAttribute("href", url);
            link.setAttribute("download", fileName);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
      }
    /* --- Open Call Escalation Dialog ---- */
    openCallEscalationDialog(row?) {
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.callEscalationsubscritption && !this.callEscalationsubscritption.closed) {
            this.callEscalationsubscritption.unsubscribe();
        }
        this.callEscalationsubscritption = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.QUICK_CALL_ESCALATION, null, false, prepareRequiredHttpParams({ CallInitiationId: row['requestId'], RoleId: this.userData?.roleId })).subscribe((res: any) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (res?.isSuccess == true && res?.statusCode == 200 && res?.resources) {
                    const ref = this.dialogService.open(CallEscalationDialogComponent, {
                        width: '400px',
                        header: 'Service Call Escalation',
                        data: {
                            
                            ...res?.resources,
                            createdUserId: this.userData?.userId,
                            isServiceCallEscalation: true,
                            row: row
                        },
                        showHeader: false,
                    });
                    ref.onClose.subscribe((res) => {
                        if (res) {

                        }
                    });
                }
            });
    }
    /* --- Close Call Escalation Dialog ---- */
    changeStatus(data) {
        const dialogReff = this.dialog.open(TaskListStatusModalComponent, { width: '700px', disableClose: true, data });
        dialogReff.afterClosed().subscribe(result => {
            if (result) return;
            this.getTaskList();
        });
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    clickCancel() {
        this.openAvs=false;
        this.dataToShow = '';
    }

}
