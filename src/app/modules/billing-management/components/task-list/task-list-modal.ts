import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from "@app/reducers";
import { HttpCancelService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { clearFormControlValidators, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from "@modules/others/auth.selectors";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from "@ngrx/store";
import { TastListStatusModel } from './tast-list-status-model';
@Component({
  selector: 'task-list-modal',
  templateUrl: './task-list-modal.html',
  // styleUrls: ['./raw-lead.component.scss']
})

export class TaskListStatusModalComponent implements OnInit {
  leadObj: any;
  changeStatusForm: FormGroup;
  Statuses = [];
  userId = "";
  show :boolean= false;
  isCommentsRequired  =false

  constructor(private httpService: CrudService, private rxjsService: RxjsService,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public statusObj: any) {
    this.changeStatusForm = this.formBuilder.group({});

    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) {
          return;
        }
        this.userId = userData["userId"];
      });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createChangeStatusForm();
    this.getStatuses();

  }

  createChangeStatusForm(): void {
    let changeStatusModel = new TastListStatusModel();
    this.changeStatusForm = this.formBuilder.group({});
    Object.keys(changeStatusModel).forEach((key) => {
      this.changeStatusForm.addControl(key, new FormControl(changeStatusModel[key]));
    });
    this.changeStatusForm.controls['modifiedUserId'].setValue(this.userId);
    this.changeStatusForm.controls['escalationId'].setValue(this.statusObj.escalationId);
    this.changeStatusForm = setRequiredValidator(this.changeStatusForm, ['escalationStatusId']);
  }

  getStatuses(): void {
    this.httpService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_ESCALATION_STATUS,null,false,null,1).subscribe((response) => {
        this.Statuses = response.resources;
      });
  }

  statusOnChange(){
      let _id = this.changeStatusForm.get('escalationStatusId').value;
      let findData =  this.Statuses.find(item => item.id == _id);
      if(findData && findData?.displayName =="Declined"){
        this.isCommentsRequired = true
        this.changeStatusForm = setRequiredValidator(this.changeStatusForm, ['comments']);
      }else{
        this.isCommentsRequired = false
        this.changeStatusForm = clearFormControlValidators(this.changeStatusForm, ['comments']);
        this.changeStatusForm.updateValueAndValidity()
      }
  }

  submit() {
    if (this.changeStatusForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ESCALATION, this.changeStatusForm.value, 1)
    .subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/billing/task-list']);
        this.rxjsService.setDialogOpenProperty(false);
        this.dialog.closeAll();
      }
    })
  }

  ngOnDestroy():void{
    this.rxjsService.setDialogOpenProperty(false);
  }

}
