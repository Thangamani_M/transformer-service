import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-risk-watch-config-list',
  templateUrl: './risk-watch-config-list.component.html',
  styleUrls: ['./risk-watch-config-list.component.scss']
})
export class RiskWatchConfigListComponent extends PrimeNgTableVariablesModel  implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  public bradCrum: MatMenuItem[];
  status: any = [];
  activeStatus: any = []
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  today: any = new Date()
  searchColumns: any
  selectedFilterDate: any
  row: any = {}
  pageSize: number = 10;
  serviceMappingForm: FormGroup;
  safeEntryserviceMappingForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  serviceList = []
  proData=[]
  constructor(private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
      super();
    this.primengTableConfigProperties = {
      tableCaption: "Risk Watch Configuration",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },{ displayName: 'Risk Watch Configuration', relativeRouterUrl: '' }, { displayName: 'Risk Watch Configuration List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Service Maintenance',
            dataKey: 'riskWatchConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'retainerName', header: 'Retainer Name', width: '200px' },
            { field: 'stockIdName', header: 'Stock ID', width: '200px' },
            { field: 'riskWatchConfSiTeTypes', header: 'Site Type', width: '150px' },
            { field: 'monthlyFee', header: 'Monthly Fee', width: '150px' },
            { field: 'cycleMonths', header: 'Cycle (Months)', width: '150px' },
            { field: 'shiftsPerCycle', header: 'Shift Per Cycle', width: '150px' },
            { field: 'shiftValue', header: 'Shift Value', width: '200px' },
            { field: 'isActive', header: 'Status', width: '150px' },
            { field: 'isProRataRequired', header: 'Pro Data', width: '150px' },
            { field: 'signUpPeriod', header: 'Sign Up Period', width: '150px' },
            { field: 'serviceDelay', header: 'Service Delay', width: '150px' },
            { field: 'proRataCutOff', header: 'Pro Rata Cut Off', width: '180px', },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.RISK_WATCH,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled:true
          },
          {
            caption: 'E-Mail Distribution List',
            dataKey: 'riskWatchEmailDistributionId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            enableAction: true,
            enableAddActionBtn: true,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'recipientName', header: 'Recipient Name', width: '200px' },
            { field: 'designation', header: 'Designation', width: '200px' },
            { field: 'emailAddress', header: 'E-Mail Address', width: '150px' },
            { field: 'riskWatchEmailDistributionDivisions', header: 'Division', width: '150px' },
            { field: 'isActive', header: 'Status', width: '150px' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.EMAIL_DISTRIBUTION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled:true
          },
          {
            caption: "Shift Cancel Reason",
            dataKey: "shiftCancelReasonTypeId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableAction: true,
            enableAddActionBtn: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "shiftCancelReasonTypeNames", header: "Reason" },
              { field: "shiftCancelReasonNameDescription", header: "Description" },
            ],
            enableMultiDeleteActionBtn: true,
            ebableAddActionBtn: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.SHIFT_CANCEL_REASON,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            disabled:true
          },
          {
            caption: "Service Mapping",
            dataKey: "",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "serviceId", header: "serviceId" },
            ],
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.RISK_WATCH_SERVICE_MAPPING,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            disabled:true
          }
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
      if (this.selectedTabIndex == 3) {
        this.createServiceMappingForm()
        this.createSafeEntryServiceMappingForm()
      }
    });
    if (this.selectedTabIndex == 3) {
      this.createServiceMappingForm()
    }
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
    this.proData = [
      { label: 'Yes', value: true },
      { label: 'No', value: false },
    ]
    this.activeStatus = [
      { label: 'Enabled', value: 'Enabled' },
      { label: 'Disabled', value: 'Disabled' },
    ]
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.getServicesList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][BILLING_MODULE_COMPONENT.RISK_WATCH_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    let moduleName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName;
    this.crudService.get(
      moduleName,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if(this.selectedTabIndex ==0){
        data.resources.forEach(item=>{
          item.proRataCutOff = item.proRataCutOffDate
        })
      }
      if (data.isSuccess) {
        this.dataList =  data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList =  data.resources
        this.totalRecords = 0;
      }

      if (this.selectedTabIndex == 3) {
        let riskwatchIds = []
        let safeEntryIds = []
        this.dataList.map(item => {
          if(item.isSafeEntryRequest){
            safeEntryIds.push(item.serviceId)
          }else{
            riskwatchIds.push(item.serviceId)
          }
        })
        this.serviceMappingForm.patchValue({ serviceId: riskwatchIds })
        this.safeEntryserviceMappingForm.patchValue({ serviceId: safeEntryIds })
      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date') && key !="proRataCutOff") {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("billing/risk-watch-configuration/add-edit");
            break;
          case 1:
            this.router.navigateByUrl("billing/risk-watch-configuration/email-distribution/add-edit");
            break;
          case 2:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigateByUrl("billing/risk-watch-configuration/shift-cancel-reason/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["billing/risk-watch-configuration/view"], { queryParams: { id: editableObject['riskWatchConfigId'] } });
            break;
          case 1:
            this.router.navigate(["billing/risk-watch-configuration/email-distribution/view"], { queryParams: { id: editableObject['riskWatchEmailDistributionId'] } });
            break;
          case 2:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(["billing/risk-watch-configuration/shift-cancel-reason/add-edit"], { queryParams: { id: editableObject['shiftCancelReasonTypeId'] } });
            break;
        }
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => { //to set default row count list
      table.rows = 10
    })
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/billing/risk-watch-configuration/list'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
    if (this.selectedTabIndex == 3) {
      this.createServiceMappingForm()
      this.createSafeEntryServiceMappingForm()
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }

  getServicesList = () => {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICES, null, false,
      prepareGetRequestHttpParams(undefined, null, null))
      .subscribe((response: IApplicationResponse) => {
        this.serviceList = getPDropdownData(response.resources);
      })
  }
  createServiceMappingForm = () => {
    this.serviceMappingForm = this._fb.group({
      serviceId: [[], Validators.required],
      createdUserId: [''],
      isSafeEntryRequest:  [false]
    });
  }
  createSafeEntryServiceMappingForm = () => {
    this.safeEntryserviceMappingForm = this._fb.group({
      serviceId: [[], Validators.required],
      createdUserId: [''],
      isSafeEntryRequest:  [true]
    });
  }

  onSubmit = () => {
    if(this.serviceMappingForm.invalid) return ''
    this.serviceMappingForm.get('createdUserId').setValue(this.loggedInUserData?.userId)
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.RISK_WATCH_SERVICE_MAPPING, this.serviceMappingForm.value).subscribe(response => {
      if (response.isSuccess) {
        this.getRequiredListData();
      }
    })
  }
  onSubmitSafeEntry = () => {
    if(this.safeEntryserviceMappingForm.invalid) return ''
    this.safeEntryserviceMappingForm.get('createdUserId').setValue(this.loggedInUserData?.userId)
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.RISK_WATCH_SERVICE_MAPPING, this.safeEntryserviceMappingForm.value).subscribe(response => {
      if (response.isSuccess) {
        this.getRequiredListData();
      }
    })
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
