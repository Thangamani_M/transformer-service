
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-risk-watch-config-view',
  templateUrl: './risk-watch-config-view.component.html',
  styleUrls: ['./risk-watch-config-list.component.scss']
})
export class RiskWatchConfigViewComponent implements OnInit {

  riskWatchConfigId: any
  riskWatchConfigDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}]
    }
  }

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.riskWatchConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.primengTableConfigProperties = {
      tableCaption: this.riskWatchConfigId && 'Service Maintenance',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Risk Watch Configuration', relativeRouterUrl: '/billing/risk-watch-configuration', queryParams: { tab: 0 } }, { displayName: 'View Service Maintenance', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.onPrimeTitleChanges();
    this.viewData()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.riskWatchConfigId) {
      this.getEmailDistributionDetailsById().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.riskWatchConfigDetails = response.resources;
        this.riskWatchConfigDetails = [
          { name: 'Retainer Name', value: response.resources.retainerName },
          { name: 'Stock Id', value: response.resources.stockIdName },
          { name: 'Site Type', value: response.resources.riskWatchConfSiTeTypes },
          { name: 'Monthly Fee', value: response.resources.monthlyFee },
          { name: 'Cycle (Months)', value: response.resources.cycleMonths },
          { name: 'Shift Value', value: response.resources.shiftValue },
          { name: 'Shift Per Cycle', value: response.resources.shiftsPerCycle },
          { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
          { name: 'Pro Data is Required?', value: response.resources.isProRataRequired == true ? "Yes" : "No" },
          { name: 'SignUp Period', value: response.resources.signUpPeriod },
          { name: 'Service Delay', value: response.resources.serviceDelay },
          { name: 'Pro Rata CutOff', value: response.resources.proRataCutOffDate },
          { name: 'Retainer Details', value: response.resources.retainerDetails },
          { name: 'Signup SOP', value: response.resources.signupSOP },
          { name: 'Request SOP', value: response.resources.requestSOP },
          { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
          { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
          { name: 'Created By', value: response.resources.createdUserName },
          { name: 'Modified By', value: response.resources.modifiedUserName },
        ];
      })
    }
  }

  viewData() {
    this.riskWatchConfigDetails = [
      { name: 'Retainer Name', value: '' },
      { name: 'Stock Id', value: '' },
      { name: 'Site Type', value: '' },
      { name: 'Monthly Fee', value: '' },
      { name: 'Shift Value', value: '' },
      { name: 'Cycle (Months)', value: '' },
      { name: 'Shift Per Cycle', value: '' },
      { name: 'Status', value: '' },
      { name: 'Pro Data is Required?', value: '' },
      { name: 'SignUp Period', value: '' },
      { name: 'Service Delay', value: '' },
      { name: 'pro Rata CutOff Date', value: '' },
      { name: 'Retainer Details', value: '' },
      { name: 'Signup SOP', value: '' },
      { name: 'Request SOP', value: '' },
      { name: 'Created On', value: '' },
      { name: 'Modified On', value: '' },
      { name: 'Created By', value: '' },
      { name: 'Modified By', value: '' },
    ]
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.RISK_WATCH_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onPrimeTitleChanges() {
    this.primengTableConfigProperties.tableCaption = 'View Service Maintenance'
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = 'Service Maintenance'
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['/billing/risk-watch-configuration/add-edit'], { queryParams: { id: this.riskWatchConfigId } })
  }
  //Get Details
  getEmailDistributionDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.RISK_WATCH,
      this.riskWatchConfigId
    );
  }
  goBack() {
    this.router.navigate(['billing/risk-watch-configuration/list'], { queryParams: { tab: 0 } });
  }
}
