
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData, RiskWatchConfigAddEditModel } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-risk-watch-config-add-edit',
  templateUrl: './risk-watch-config-add-edit.component.html',
  styleUrls: ['./risk-watch-config-list.component.scss']
})
export class riskWatchConfigAddEditComponent implements OnInit {

  riskWatchConfigId: any
  riskWatchConfigForm: FormGroup;
  rOCommunityPatrolTypesList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  stockIdList = []
  siteTypeList = []
  datesList = [];
  risckwatchDetails: any;
  primengTableConfigProperties:any
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.riskWatchConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.datesList = []
    for (let index = 1; index <= 31; index++) {
      this.datesList.push({ value: index });
    }
    this.primengTableConfigProperties = {
      tableCaption: `${this.riskWatchConfigId ? 'Update' : 'Add'} Service Maintenance`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },{ displayName: 'Risk Watch Configuration', relativeRouterUrl: '' }, { displayName: 'Service Maintenance' ,relativeRouterUrl: '/billing/risk-watch-configuration',queryParams:{tab:0}},{ displayName: `${this.riskWatchConfigId ? 'Update' : 'Add'} Service Maintenance`,}],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Service Maintenance',
            dataKey: 'riskWatchConfigId',
            enableBreadCrumb: true,
          }]
        }
      }
  }

  ngOnInit() {
    this.createriskWatchConfigForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
      this.stockIdList = response[0].resources;
      this.siteTypeList = getPDropdownData(response[1].resources);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    if (this.riskWatchConfigId) {
      this.getriskWatchConfigDetailsById().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.risckwatchDetails = response.resources
        let siteTypeId = []
        response.resources.riskWatchConfSiTeTypeList.map(item => {
          siteTypeId.push(item.siteTypeId)
        })
        response.resources.riskWatchConfSiTeTypeList = siteTypeId
        let riskWatchConfigAddEditModel = new RiskWatchConfigAddEditModel(response.resources);
        this.riskWatchConfigForm.patchValue(riskWatchConfigAddEditModel);
        this.onProRateRadioChange();
      })
    }
  }

  createriskWatchConfigForm(): void {
    let riskWatchConfigAddEditModel = new RiskWatchConfigAddEditModel();
    this.riskWatchConfigForm = this.formBuilder.group({
    });
    Object.keys(riskWatchConfigAddEditModel).forEach((key) => {
      this.riskWatchConfigForm.addControl(key, new FormControl(riskWatchConfigAddEditModel[key]));
    });
    this.riskWatchConfigForm = setRequiredValidator(this.riskWatchConfigForm, ["retainerName", "stockId", "monthlyFee", "shiftsPerCycle", "shiftValue", "signUpPeriod", "serviceDelay", "retainerDetails", "signupSOP", "requestSOP", "riskWatchConfSiTeTypeList"]);
    this.riskWatchConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.riskWatchConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.riskWatchConfigForm.get('proRataCutOffDate').disable()
    this.riskWatchConfigForm.get("cycleMonths").setValidators([Validators.required, Validators.min(1), Validators.max(12)])
    this.riskWatchConfigForm.get("signUpPeriod").setValidators([Validators.required, Validators.min(1), Validators.max(12)])
  }
  //Get Details
  getriskWatchConfigDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.RISK_WATCH,
      this.riskWatchConfigId
    );
  }
  isProRataRequired = false;
  onProRateRadioChange() {
    let _value = this.riskWatchConfigForm.get("isProRataRequired").value;
    this.isProRataRequired = _value;
    if (_value == true) {
      this.riskWatchConfigForm.controls['proRataCutOffDate'].enable()
      this.riskWatchConfigForm.controls['proRataCutOffDate'].setValidators([Validators.required])
      this.riskWatchConfigForm.controls['proRataCutOffDate'].updateValueAndValidity()
    } else {
      this.riskWatchConfigForm.controls['proRataCutOffDate'].disable()
      this.riskWatchConfigForm.controls['proRataCutOffDate'].clearValidators()
      this.riskWatchConfigForm.controls['proRataCutOffDate'].updateValueAndValidity();
      this.riskWatchConfigForm.controls['proRataCutOffDate'].setValue(null);
    }
  }
  // Get Stock Id, Site Types Dropdown data
  getAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_RISK_WATCH_STOCK_ID, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_RISK_WATCH_SITE_TYPES, undefined, true),
    )
  }

  onSubmit() {
    if (this.riskWatchConfigForm.invalid) {
      return;
    }
    let formValue = this.riskWatchConfigForm.getRawValue();
    let ids = []
    formValue.riskWatchConfSiTeTypeList.map(id => {
      ids.push({ siteTypeId: id ,riskWatchConfigId: this.riskWatchConfigId,isActive:true,createdUserId:this.loggedUser?.userId,createdDate:new Date().toISOString(),modifiedUserId:this.loggedUser?.userId,modifiedDate:new Date().toISOString()})
    })
    let reqObject = { ...formValue, riskWatchConfigid: this.riskWatchConfigId }
    reqObject.riskWatchConfSiTeTypeList = ids
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.riskWatchConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RISK_WATCH, reqObject) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RISK_WATCH, reqObject)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.goBack();
      }
    })
  }
  goBack() {
    this.router.navigate(['billing/risk-watch-configuration/list'], { queryParams: { tab: 0 } });
  }
}
