
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
    selector: 'app-email-distribution-view',
    templateUrl: './email-distribution-view.component.html',
    styleUrls: ['./risk-watch-config-list.component.scss']
})
export class EmailDistributionViewComponent implements OnInit {

    emailDistributionId: any
    emailDistributionDetails: any;
    selectedTabIndex = 0;
    primengTableConfigProperties: any
    primengTableConfigPropertiesObj: any = {
      tableComponentConfigs: {
        tabsList: [{}, {}, {}, {}]
      }
    }
    constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
      private store: Store<AppState>, private snackbarService: SnackbarService) {
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.emailDistributionId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
            this.rxjsService.setGlobalLoaderProperty(false);
        });
        this.primengTableConfigProperties = {
            tableCaption: this.emailDistributionId && 'View Email Distribution',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Email Distribution', relativeRouterUrl: '/billing/risk-watch-configuration', queryParams: { tab: 1 } }, { displayName: 'View Email Distribution', relativeRouterUrl: '', }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableAction: false,
                        enableBreadCrumb: true,
                    }
                ]
            }
        }
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData()
        this.onPrimeTitleChanges();
        this.viewData()
        this.rxjsService.setGlobalLoaderProperty(false);
        if (this.emailDistributionId) {
            this.getEmailDistributionDetailsById().subscribe((response: IApplicationResponse) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                this.emailDistributionDetails = response.resources;
                this.emailDistributionDetails = [
                    { name: 'Recipien tName', value: response.resources.recipientName },
                    { name: 'Designation', value: response.resources.designation },
                    { name: 'Email Address', value: response.resources.emailAddress },
                    { name: 'Division', value: response.resources.riskWatchEmailDistributionDivisions },
                    { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive==true?"status-label-green":'status-label-red'},
                    { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
                    { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
                    { name: 'Created By', value: response.resources.createdUserName },
                    { name: 'Modified By', value: response.resources.modifiedUserName },
                ];
            })
        }
    }

    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][BILLING_MODULE_COMPONENT.RISK_WATCH_CONFIGURATION]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
          this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    viewData() {
        this.emailDistributionDetails = [
            { name: 'Recipient Name', value: '' },
            { name: 'Designation', value: '' },
            { name: 'Email Address', value: '' },
            { name: 'Division', value: '' },
            { name: 'Status', value: '' },
            { name: 'Created On', value: '' },
            { name: 'Modified On', value: '' },
            { name: 'Created By', value: '' },
            { name: 'Modified By', value: '' },
        ]
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
    }

    onPrimeTitleChanges() {
        this.primengTableConfigProperties.tableCaption = 'View Email Distribution'
        this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = 'View Email Distribution'
    }

    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
        switch (type) {
            case CrudType.EDIT:
              if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
                this.onEditButtonClicked();
                break;
            default:
                break;
        }
    }

    onEditButtonClicked(): void {
        this.router.navigate(['/billing/risk-watch-configuration/email-distribution/add-edit'], { queryParams: { id: this.emailDistributionId } })
    }
    //Get Details
    getEmailDistributionDetailsById(): Observable<IApplicationResponse> {
        return this.crudService.get(
            ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            EventMgntModuleApiSuffixModels.EMAIL_DISTRIBUTION,
            this.emailDistributionId
        );
    }

    goBack() {
        this.router.navigate(['billing/risk-watch-configuration/list'], { queryParams: { tab: 1 } });
    }

}
