import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { EmailDistributionAddEditComponent } from './email-distribution-add-edit.component';
import { EmailDistributionViewComponent } from './email-distribution-view.component';
import { riskWatchConfigAddEditComponent } from './risk-watch-config-add-edit.component';
import { RiskWatchConfigListComponent } from './risk-watch-config-list.component';
import { RiskWatchConfigRoutingModule } from './risk-watch-config-routing.module';
import { RiskWatchConfigViewComponent } from './risk-watch-config-view.component';

@NgModule({
  declarations: [RiskWatchConfigListComponent,
    riskWatchConfigAddEditComponent,
    EmailDistributionAddEditComponent,
    EmailDistributionViewComponent,
    RiskWatchConfigViewComponent],
  imports: [
    CommonModule,
    RiskWatchConfigRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ],
})
export class RiskWatchConfigModule { }
