
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EmailDistributionAddEditModel, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-email-distribution-add-edit',
  templateUrl: './email-distribution-add-edit.component.html',
  styleUrls: ['./risk-watch-config-list.component.scss']
})
export class EmailDistributionAddEditComponent implements OnInit {

  emailDistributionId: any
  emailDistributionForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  divisionList = []
  primengTableConfigProperties:any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.emailDistributionId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: `${this.emailDistributionId ? 'Update' : 'Add'} Email Distribution`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },{ displayName: 'Risk Watch Configuration', relativeRouterUrl: '' }, { displayName: 'Email Distribution List' ,relativeRouterUrl: '/billing/risk-watch-configuration',queryParams:{tab:1}},{ displayName: `${this.emailDistributionId ? 'Update' : 'Add'} Email Distribution`,}],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Service Maintenance',
            dataKey: 'riskWatchConfigId',
            enableBreadCrumb: true,
          }]
        }
      }
  }

  ngOnInit() {
    this.createemailDistributionForm();
    this.getDivisionDropdown();
    if (this.emailDistributionId) {
      this.getEmailDistributionDetailsById().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        let divisionId = []
        response.resources.riskWatchEmailDistributionDivisionList.map(item => {
          divisionId.push(item.divisionId)
        })
        response.resources.riskWatchEmailDistributionDivisionList = divisionId
        let emailDistributionAddEditModel = new EmailDistributionAddEditModel(response.resources);
        this.emailDistributionForm.patchValue(emailDistributionAddEditModel);
      })
    }
  }

  createemailDistributionForm(): void {
    let emailDistributionAddEditModel = new EmailDistributionAddEditModel();
    this.emailDistributionForm = this.formBuilder.group({
    });
    Object.keys(emailDistributionAddEditModel).forEach((key) => {
      this.emailDistributionForm.addControl(key, new FormControl(emailDistributionAddEditModel[key]));
    });
    this.emailDistributionForm = setRequiredValidator(this.emailDistributionForm, ["recipientName", "designation", "emailAddress"]);
    this.emailDistributionForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.emailDistributionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.emailDistributionForm.get("riskWatchEmailDistributionDivisionList").setValidators([Validators.required])
  }
  //Get Details
  getEmailDistributionDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.EMAIL_DISTRIBUTION,
      this.emailDistributionId
    );
  }
  // Get Division dropdown data
  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, undefined, true).subscribe(response => {
      if (response.isSuccess) {
        this.divisionList = getPDropdownData(response.resources);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  onSubmit(): void {
    if (this.emailDistributionForm.invalid) {
      return;
    }
    let formValue = this.emailDistributionForm.value;
    let ids = []
    formValue.riskWatchEmailDistributionDivisionList.map(id => {
      ids.push({ divisionId: id, riskWatchEmailDistributionId:this.emailDistributionId,isActive:true,createdUserId:this.loggedUser?.userId,createdDate:new Date().toISOString(),modifiedUserId:this.loggedUser?.userId,modifiedDate:new Date().toISOString()})
    })

    let reqObject = { ...formValue, riskWatchEmailDistributionId: this.emailDistributionId }
    reqObject.riskWatchEmailDistributionDivisionList = ids
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.emailDistributionId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EMAIL_DISTRIBUTION, reqObject) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EMAIL_DISTRIBUTION, reqObject)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack();
      }
    })
  }

  goBack() {
    this.router.navigate(['billing/risk-watch-configuration/list'], { queryParams: { tab: 1 } });
  }
}
