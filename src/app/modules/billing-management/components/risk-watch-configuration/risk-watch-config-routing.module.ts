import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailDistributionAddEditComponent } from './email-distribution-add-edit.component';
import { EmailDistributionViewComponent } from './email-distribution-view.component';
import { riskWatchConfigAddEditComponent } from './risk-watch-config-add-edit.component';
import { RiskWatchConfigListComponent } from './risk-watch-config-list.component';
import { RiskWatchConfigViewComponent } from './risk-watch-config-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', redirectTo: 'list', canActivate:[AuthGuard], pathMatch: 'full' },
  { path: 'list', component: RiskWatchConfigListComponent,canActivate:[AuthGuard], data: { title: 'Risk Watch Configuration list' } },
  { path: 'add-edit', component: riskWatchConfigAddEditComponent,canActivate:[AuthGuard], data: { title: 'Risk Watch Configuration Add/Edit' } },
  { path: 'view', component: RiskWatchConfigViewComponent,canActivate:[AuthGuard], data: { title: 'Risk Watch Configuration View' } },
  { path: 'email-distribution/add-edit', component: EmailDistributionAddEditComponent, canActivate:[AuthGuard],data: { title: 'Email Distribution Add/Edit' } },
  { path: 'email-distribution/view', component: EmailDistributionViewComponent, canActivate:[AuthGuard],data: { title: 'Email Distribution View' } },
  { path: 'shift-cancel-reason', loadChildren: () => import("../shift-cancel-reason/shift-cancel-reason-config.module").then(m => m.ShiftCancelReasonModule), data: { title: 'Email Distribution View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class RiskWatchConfigRoutingModule { }
