import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmmortizationScheduleViewComponent,AmmortizationScheduleListComponent} from './';
const routes: Routes = [
     { path: 'view', component: AmmortizationScheduleViewComponent, data: { title: 'Ammortization Schedule View' }},
    { path: '', component: AmmortizationScheduleListComponent, data: { title: 'Ammortization Schedule List' }},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AmmortizationScheduleRoutingModule { }
