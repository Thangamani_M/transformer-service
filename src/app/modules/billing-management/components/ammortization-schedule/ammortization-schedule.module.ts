import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AmmortizationScheduleListComponent, AmmortizationScheduleRoutingModule, AmmortizationScheduleViewComponent } from './';
@NgModule({
  declarations: [AmmortizationScheduleViewComponent, AmmortizationScheduleListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    AmmortizationScheduleRoutingModule
  ]
})
export class AmmortizationScheduleModule { }
