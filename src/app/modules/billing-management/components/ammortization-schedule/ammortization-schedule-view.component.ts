import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import {CommonPaginationConfig,CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, RxjsService} from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'ammortization-schedule-view',
  templateUrl: './ammortization-schedule-view.component.html',
  styleUrls: ['./ammortization-schedule.component.scss'],
})
export class AmmortizationScheduleViewComponent extends PrimeNgTableVariablesModel  implements OnInit {
  alarmSystemModelTypeId = '';
  alarmSystemAmmortizationPeriodId = '';
  ammortizationData;
  pageSize: number = 10;
  primengTableConfigProperties: any = {
    tableCaption: "Amortization Schedule View",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Billing management', relativeRouterUrl: '' }, { displayName: 'Amortization Schedule', relativeRouterUrl: '/billing/ammortization-schedule'}, { displayName: 'Amortization Schedule View', relativeRouterUrl: '', }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Amortization Schedule View',
          dataKey: 'month',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: false,
          enableAddActionBtn: false,
          enableFieldsSearch: false,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [{ field: 'month', header: 'Months' },
          { field: 'inlandReleaseAmount', header: 'Inland Release' },
          { field: 'inlandRemainingBalance', header: 'Inland Remaining Balance' }, { field: 'coastalReleaseAmount', header: 'Coastal Release' },
          { field: 'coastalRemainingBalance', header: 'Coastal Remaining Balance' }],
          shouldShowCreateActionBtn: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.PURCHASE_OF_ALARM_AMMORTIZATION_SCHEDULE_DETAIL,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS
        }]
    }
  }
  constructor(private crudService: CrudService,
    private rxjsService: RxjsService, private router: Router,private store: Store<AppState>,public dialogService: DialogService, private activatedRoute: ActivatedRoute) {
      super();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getAmmortizationPeriodData();
  }
  loadPaginationLazy(event) {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.onCRUDRequested(CrudType.GET, row);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.alarmSystemModelTypeId = response[1]['alarmSystemModelTypeId'];
      this.alarmSystemAmmortizationPeriodId = response[1]['alarmSystemAmmortizationPeriodId'];
    });
  }
  getAmmortizationPeriodData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['alarmSystemModelTypeId'] = this.alarmSystemModelTypeId;
    otherParams['alarmSystemAmmortizationPeriodId'] = this.alarmSystemAmmortizationPeriodId;
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PURCHASE_OF_ALARM_AMMORTIZATION_SCHEDULE_DETAIL,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ammortizationData = response.resources;
        this.dataList = response.resources?.ammortizedMonthlyDetails;
        this.totalRecords = this.dataList.length;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(row).length > 0 && row['searchColumns']) {
          Object.keys(row['searchColumns']).forEach((key) => {
            otherParams[key] = row['searchColumns'][key]['id'] ?
              row['searchColumns'][key]['id'] : row['searchColumns'][key];
          });
        }
        let pageIndex, maximumRows;
        if (!row['maximumRows']) {
          maximumRows = CommonPaginationConfig.defaultPageSize;
          pageIndex = CommonPaginationConfig.defaultPageIndex;
        }
        else {
          maximumRows = row["maximumRows"];
          pageIndex = row["pageIndex"];
        }
        delete row['maximumRows'] && row['maximumRows'];
        delete row['pageIndex'] && row['pageIndex'];
        delete row['searchColumns'];
        this.getAmmortizationPeriodData(pageIndex, maximumRows, { ...otherParams, ...row });
        break;
    }
  }
  redirectToAmmortizationScheduleList() {
    this.router.navigate(['/billing/ammortization-schedule'], {
      queryParams: {
        alarmSystemAmmortizationPeriodId: this.alarmSystemAmmortizationPeriodId,
        alarmSystemModelTypeId: this.alarmSystemModelTypeId
      }
    });
  }
}