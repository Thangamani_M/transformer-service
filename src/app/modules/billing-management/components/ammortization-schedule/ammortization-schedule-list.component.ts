import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {CommonPaginationConfig,CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-ammortization-schedule-list',
  templateUrl: './ammortization-schedule-list.component.html',
  styleUrls: ['./ammortization-schedule.component.scss'],
})
export class AmmortizationScheduleListComponent extends PrimeNgTableVariablesModel implements OnInit {
  alarmSystemModelTypeId = '';
  alarmSystemAmmortizationPeriodId = '';
  ammortizationPeriods = [];
  modelTypes = [];
  isBtnDisabled = true;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns;
  row;
  totalAmountFooterObj;
  isFormSubmitted = false;
  primengTableConfigProperties: any = {
    tableCaption: "Amortization Schedule",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{displayName:"Billing management"},{displayName:"Amortization Schedule"}],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Amortization Schedule',
          dataKey: 'alarmSystemCostTypeName',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: false,
          enableAddActionBtn: false,
          enableFieldsSearch: false,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [{ field: 'alarmSystemCostTypeName', header: 'Type Of Costs' },
          { field: 'inlandAmount', header: 'Inland' }, { field: 'coastalAmount', header: 'Coastal' }],
          shouldShowCreateActionBtn: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.PURCHASE_OF_ALARM_AMMORTIZATION_SCHEDULE,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS
        }]
    }
  }
  constructor(private crudService: CrudService,
    private rxjsService: RxjsService, private router: Router,private store: Store<AppState>, private activatedRoute: ActivatedRoute,public dialogService: DialogService, private _fb: FormBuilder, private tableFilterFormService: TableFilterFormService) {
    super();
  }
  ngOnInit(): void {
    this.getForkJoinRequests();
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1] && Object.keys(response[1]).length > 0) {
        this.alarmSystemModelTypeId = response[1]['alarmSystemModelTypeId'];
        this.alarmSystemAmmortizationPeriodId = response[1]['alarmSystemAmmortizationPeriodId'];
        this.isBtnDisabled = false;
        this.getAmmortizationViewData();
      }
    });
  }
  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {})
          if (!this.row) {
            this.row = {};
          }
          this.row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_ALARM_SYSTEM_MODEL_TYPE),
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_ALARM_SYSTEM_AMMORTIZATION_PERIOD)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200) {
            switch (ix) {
              case 0:
                this.modelTypes = respObj.resources;
                break;
              case 1:
                this.ammortizationPeriods = respObj.resources;
                break;
            }
          }
          setTimeout(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        });
      });
  }

  onModelChanged() {
    setTimeout(() => {
      if (this.alarmSystemModelTypeId) {
        this.isBtnDisabled = false;
        this.onCRUDRequested(CrudType.GET, { pageIndex: CommonPaginationConfig.defaultPageIndex, pageSize: CommonPaginationConfig.defaultPageSize });
      }
    });
  }

  loadPaginationLazy(event) {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.onCRUDRequested(CrudType.GET, row);
  }

  getAmmortizationViewData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['alarmSystemModelTypeId'] = this.alarmSystemModelTypeId;
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PURCHASE_OF_ALARM_AMMORTIZATION_SCHEDULE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.dataList = response.resources.ammortizationScheduleList;
        this.totalAmountFooterObj = {
          totalCoastalAmount: response.resources.totalCoastalAmount,
          totalInlandAmount: response.resources.totalInlandAmount
        };
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(row).length > 0 && row['searchColumns']) {
          Object.keys(row['searchColumns']).forEach((key) => {
            otherParams[key] = row['searchColumns'][key]['id'] ?
              row['searchColumns'][key]['id'] : row['searchColumns'][key];
          });
        }
        else if (this.searchColumns && Object.keys(this.searchColumns).length > 0) {
          Object.keys(this.searchColumns).forEach((key) => {
            otherParams[key] = this.searchColumns[key];
          });
        }
        let pageIndex, maximumRows;
        if (!row['maximumRows']) {
          maximumRows = CommonPaginationConfig.defaultPageSize;
          pageIndex = CommonPaginationConfig.defaultPageIndex;
        }
        else {
          maximumRows = row["maximumRows"];
          pageIndex = row["pageIndex"];
        }
        delete row['maximumRows'] && row['maximumRows'];
        delete row['pageIndex'] && row['pageIndex'];
        delete row['searchColumns'];
        this.getAmmortizationViewData(pageIndex, maximumRows, { ...otherParams, ...row });
        break;
    }
  }

  viewAmmortizationSchedule() {
    this.isFormSubmitted = true;
    if (!this.alarmSystemAmmortizationPeriodId) {
      return;
    }
    this.router.navigate(['/billing/ammortization-schedule/view'], { queryParams: { alarmSystemModelTypeId: this.alarmSystemModelTypeId, alarmSystemAmmortizationPeriodId: this.alarmSystemAmmortizationPeriodId } })
  }
}
