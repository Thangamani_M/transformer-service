import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, SnackbarService } from '@app/shared';
import { ProcessTypeDesMappingListModel, ProcessTypeDesMappingModel } from '@modules/event-management/models/configurations/dropdown-config.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DropdownGroupName } from '@modules/event-management/shared/enums/dropdown-groupname.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from "@modules/out-of-office/enums/out-of-office-employee-request.enum";
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-process-type-transaction-description-mapping',
  templateUrl: './process-type-transaction-description-mapping.component.html'
})

export class ProcessTypeTransactionDescriptionMappingComponent extends PrimeNgTableVariablesModel implements OnInit {
  dropdownId: any
  distributionGroupId: any
  dropdownForm: FormGroup;
  callCategoryList: FormArray;
  loggedUser: any;
  dropDownConfigDetails: any;
  @ViewChildren('input') rows: QueryList<any>;
  processTypeDropdown: any = []
  transationTypesDropdown: any = []
  primengTableConfigProperties: any
  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    super();
    this.dropdownId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Process Type Transaction Description Mapping',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Transaction', relativeRouterUrl: '/billing/transaction-configuration/list', }, { displayName: 'Process Type Transaction Description List', relativeRouterUrl: '' },],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableEditActionBtn: false,
            enableBreadCrumb: true,
            enableExportBtn: false,
            enablePrintBtn: false,
          }
        ]
      }
    }
    this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit() {
    this.createdropdownForm();
    this.getprocessTypeDropdown()
    this.getUxTransactionType()
    setTimeout(() => {
      this.getDropDownConfigDetails();
    }, 1500);
  }

  createdropdownForm(): void {
    let dropDownConfigModel = new ProcessTypeDesMappingModel();
    this.dropdownForm = this.formBuilder.group({
      callCategories: this.formBuilder.array([]),
    });
    Object.keys(dropDownConfigModel).forEach((key) => {
      this.dropdownForm.addControl(key, new FormControl(dropDownConfigModel[key]));
    });
  }

  //Create FormArray
  get getcallCategoryListArray(): FormArray {
    if (!this.dropdownForm) return;
    return this.dropdownForm.get("callCategories") as FormArray;
  }

  //Create FormArray controls
  createcallCategoryListModel(callCategoryListModel?: ProcessTypeDesMappingListModel, groupName?: DropdownGroupName): FormGroup {
    let callCategoryListFormControlModel = new ProcessTypeDesMappingListModel(callCategoryListModel);
    let formControls = {};
    Object.keys(callCategoryListFormControlModel).forEach((key) => {
      if (key === 'invoiceTransactionTypeDescriptionMappingId' || key === 'transationDescriptionsDropdown' || key === 'subTypeDescriptionsDropdown' || key === 'invoiceTransactionDescriptionSubTypeIds') {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    let formListControl = this.formBuilder.group(formControls);
    if (formListControl.get('invoiceTransactionTypeId').value) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_DESCRIPTION, null, false, prepareRequiredHttpParams({
        InvoiceTransactionTypeId: formListControl.value.invoiceTransactionTypeId
      }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            let transationDescriptionsDropdown = []
            response.resources.forEach(element => {
              let data = { label: element.displayName, value: { invoiceTransactionDescriptionId: element.id } }
              transationDescriptionsDropdown.push(data)
            });
            formListControl.get('transationDescriptionsDropdown').setValue(transationDescriptionsDropdown)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    formListControl.get('invoiceTransactionTypeId').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_DESCRIPTION, null, false, prepareRequiredHttpParams({
        InvoiceTransactionTypeId: val
      }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            let transationDescriptionsDropdown = []
            response.resources.forEach(element => {
              let data = { label: element.displayName, value: { invoiceTransactionDescriptionId: element.id } }
              transationDescriptionsDropdown.push(data)
            });
            formListControl.get('invoiceTransactionDescriptionIds').setValue([])
            formListControl.get('transationDescriptionsDropdown').setValue(transationDescriptionsDropdown)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    })

    if (formListControl.get('invoiceTransactionDescriptionIds').value.length > 0) {
      let ids = []
      formListControl.get('invoiceTransactionDescriptionIds').value.forEach(element => {
        ids.push(element.invoiceTransactionDescriptionId)
      });
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PROCESS_TYPE_TRANSACTION_MAPPINGS_SUBTYPE, null, false, prepareRequiredHttpParams({
        invoiceTransactionDescriptionIds: ids.toString()
      }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            let subTypeDescriptionsDropdown = []
            response.resources.forEach(element => {
              let data = { label: element.invoiceTransactionDescriptionSubTypeName, value: { invoiceTransactionDescriptionSubTypeId: element.invoiceTransactionDescriptionSubTypeId, invoiceTransactionDescriptionId: element.invoiceTransactionDescriptionId } }
              subTypeDescriptionsDropdown.push(data)
            });
            formListControl.get('subTypeDescriptionsDropdown').setValue(subTypeDescriptionsDropdown)
            var invoiceTransactionDescriptionSubTypeIds = []
            formListControl.get('invoiceTransactionDescriptionSubTypeIds').value.forEach(element => {
              let data = formListControl.get('subTypeDescriptionsDropdown').value.find(x => x.value.invoiceTransactionDescriptionSubTypeId == element.invoiceTransactionDescriptionSubTypeId)
              if (data) {
                invoiceTransactionDescriptionSubTypeIds.push(data.value)
              }
            });
            formListControl.get('invoiceTransactionDescriptionSubTypeIds').setValue(null)
            formListControl.get('invoiceTransactionDescriptionSubTypeIds').setValue(invoiceTransactionDescriptionSubTypeIds)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    formListControl.get('invoiceTransactionDescriptionIds').valueChanges.subscribe(val => {
      if (val.length == 0) {
        return
      }
      let ids = []
      val.forEach(element => {
        ids.push(element.invoiceTransactionDescriptionId)
      });
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PROCESS_TYPE_TRANSACTION_MAPPINGS_SUBTYPE, null, false, prepareRequiredHttpParams({
        invoiceTransactionDescriptionIds: ids.toString()
      }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            let subTypeDescriptionsDropdown = []
            response.resources.forEach(element => {
              let data = { label: element.invoiceTransactionDescriptionSubTypeName, value: { invoiceTransactionDescriptionSubTypeId: element.invoiceTransactionDescriptionSubTypeId, invoiceTransactionDescriptionId: element.invoiceTransactionDescriptionId } }
              subTypeDescriptionsDropdown.push(data)
            });
            formListControl.get('invoiceTransactionDescriptionSubTypeIds').setValue([])
            formListControl.get('subTypeDescriptionsDropdown').setValue(subTypeDescriptionsDropdown)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    })
    return formListControl;
  }

  getprocessTypeDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
      OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_TYPE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.processTypeDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getUxTransactionType() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.transationTypesDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  //Get Details
  getcallCategoryListDetailsById() {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.COORDINATOR_DEFAULT_AREA,
      undefined,
      false, prepareGetRequestHttpParams(null)
    )
  }
  // Get Details
  getDropDownConfigDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PROCESS_TYPE_TRANSACTION_MAPPINGS,
      undefined,
      false, prepareGetRequestHttpParams(null)
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      let dropDownConfigModel = new ProcessTypeDesMappingModel(response.resources);
      this.dropDownConfigDetails = response.resources;

      if (response.resources.length == 0) {
        this.callCategoryList = this.getcallCategoryListArray;
        let callCategoryListModel = new ProcessTypeDesMappingListModel();
        callCategoryListModel.createdUserId = this.loggedUser.userId
        callCategoryListModel.modifiedUserId = this.loggedUser.userId
        this.callCategoryList.push(this.createcallCategoryListModel(callCategoryListModel, DropdownGroupName.CALL_CATEGORY));
      } else {
        this.callCategoryList = this.getcallCategoryListArray;
        response.resources.forEach((callCategoryListModel: ProcessTypeDesMappingListModel) => {
          if (callCategoryListModel.invoiceTransactionDescriptionIds) {
            let desc = callCategoryListModel.invoiceTransactionDescriptionIds.split(',');
            callCategoryListModel.invoiceTransactionDescriptionIds = []
            desc.forEach(element => {
              callCategoryListModel.invoiceTransactionDescriptionIds.push({ invoiceTransactionDescriptionId: element.toLowerCase() })
            });
          }
          if (callCategoryListModel.invoiceTransactionDescriptionSubTypeIds) {
            let type = callCategoryListModel.invoiceTransactionDescriptionSubTypeIds.split(',');
            callCategoryListModel.invoiceTransactionDescriptionSubTypeIds = []
            type.forEach(element => {
              callCategoryListModel.invoiceTransactionDescriptionSubTypeIds.push({ invoiceTransactionDescriptionSubTypeId: element.toLowerCase() })
            });
          }
          callCategoryListModel.createdUserId = this.loggedUser.userId
          callCategoryListModel.modifiedUserId = this.loggedUser.userId
          this.callCategoryList.push(this.createcallCategoryListModel(callCategoryListModel, DropdownGroupName.CALL_CATEGORY));
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  add(groupName: DropdownGroupName | string): void {
    switch (groupName) {
      case DropdownGroupName.CALL_CATEGORY:
        if (this.getcallCategoryListArray.invalid) {
          this.focusInAndOutFormArrayFields();
          return;
        };
        this.callCategoryList = this.getcallCategoryListArray;
        let callCategoryListModel = new ProcessTypeDesMappingListModel();
        callCategoryListModel.createdUserId = this.loggedUser.userId
        callCategoryListModel.modifiedUserId = this.loggedUser.userId
        this.callCategoryList.insert(0, this.createcallCategoryListModel(callCategoryListModel, groupName));
        this.rxjsService.setFormChangeDetectionProperty(true);
        break
    }
  }

  remove(groupName: DropdownGroupName | string, i: number): void {
    switch (groupName) {
      case DropdownGroupName.CALL_CATEGORY:
        if (!this.getcallCategoryListArray.controls[i].value.invoiceTransactionTypeDescriptionMappingId) {
          this.getcallCategoryListArray.removeAt(i);
          return;
        }
        break;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      switch (groupName) {
        case DropdownGroupName.CALL_CATEGORY:
          if (this.getcallCategoryListArray.controls[i].value.invoiceTransactionTypeDescriptionMappingId) {
            this.crudService.delete(ModulesBasedApiSuffix.BILLING,
              BillingModuleApiSuffixModels.PROCESS_TYPE_TRANSACTION_MAPPINGS,
              '',
              prepareRequiredHttpParams({
                ids: this.getcallCategoryListArray.controls[i].value.invoiceTransactionTypeDescriptionMappingId,
                modifiedUserId: this.loggedUser.userId,
                isDeleted: true
              })
            ).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.getcallCategoryListArray.removeAt(i);
              }
              if (this.getcallCategoryListArray.length === 0) {
                this.add(DropdownGroupName.CALL_CATEGORY);
              };
            });
          } else {
            this.getcallCategoryListArray.removeAt(i);
          }
          break;
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    if (this.dropdownForm.invalid) {
      return;
    }
    let formValue = this.dropdownForm.getRawValue();
    formValue.callCategories.forEach(element => {
      var transactionDescriptions = []
      if (element.invoiceTransactionDescriptionSubTypeIds.length > 0) {
        element.invoiceTransactionDescriptionSubTypeIds.forEach(element => {
          let data = {
            invoiceTransactionDescriptionId: element.invoiceTransactionDescriptionId,
            invoiceTransactionDescriptionSubTypes: [element]
          }
          let existIndex = transactionDescriptions.findIndex(x => x.invoiceTransactionDescriptionId == element.invoiceTransactionDescriptionId)
          if (existIndex >= 0) {
            transactionDescriptions[existIndex].invoiceTransactionDescriptionSubTypes.push(element)
          } else {
            transactionDescriptions.push(data)
          }
        });
        element.invoiceTransactionDescriptionIds.forEach(element => {
          let data = {
            invoiceTransactionDescriptionId: element.invoiceTransactionDescriptionId,
            invoiceTransactionDescriptionSubTypes: []
          }
          let existIndex = transactionDescriptions.findIndex(x => x.invoiceTransactionDescriptionId == element.invoiceTransactionDescriptionId)
          if (existIndex >= 0) {
          } else {
            transactionDescriptions.push(data)
          }
        });
      } else {
        element.invoiceTransactionDescriptionIds.forEach(element => {
          let data = {
            invoiceTransactionDescriptionId: element.invoiceTransactionDescriptionId,
            invoiceTransactionDescriptionSubTypes: []
          }
          transactionDescriptions.push(data)
        });
      }
      element.transactionDescriptions = transactionDescriptions
      // delete element.invoiceTransactionDescriptionIds
      // delete element.invoiceTransactionDescriptionSubTypeIds
      delete element.subTypeDescriptionsDropdown
      delete element.transationDescriptionsDropdown
    });
    // formValue
    // return
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PROCESS_TYPE_TRANSACTION_MAPPINGS, formValue.callCategories)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.createdropdownForm()
        this.getDropDownConfigDetails()
      }
    })
  }
}
