import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcessTypeTransactionDescriptionMappingComponent } from './process-type-transaction-description-mapping.component';

const routes: Routes = [
  { path: '', redirectTo:'add-edit',pathMatch:'full'},
  { path: 'add-edit', component: ProcessTypeTransactionDescriptionMappingComponent, data: { title: 'Process Type Transaction Description Mapping list' }},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class ProcessTypeTransactionDescriptionMappingRoutingModule { }
