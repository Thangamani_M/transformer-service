import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcessTypeTransactionDescriptionMappingRoutingModule } from './process-type-transaction-description-mapping-routing.module';
import { ProcessTypeTransactionDescriptionMappingComponent } from './process-type-transaction-description-mapping.component';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [ProcessTypeTransactionDescriptionMappingComponent],
  imports: [
    CommonModule,
    ProcessTypeTransactionDescriptionMappingRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})

export class ProcessTypeTransactionDescriptionMappingModule { }
