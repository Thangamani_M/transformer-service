import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-id-request-list',
  templateUrl: './stock-id-request-list.component.html'
})
export class StockIdRequestListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  observableResponse;
  selectedTabIndex: any = 0;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  public bradCrum: MatMenuItem[];
  status: any = [];
  activeStatus: any = []
  selectedColumns: any[];
  selectedRows: string[] = [];
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  today: any = new Date()
  searchColumns: any
  row: any = {}
  pageSize: number = 10;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Stock ID Request",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Stock ID Request List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock ID Request List',
            dataKey: 'stockId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            enableAction: true,
            enableAddActionBtn: true,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stockIdName', header: 'Stock ID', width: '200px' }, { field: 'descriptionCount', header: 'Description Count', width: '200px' }, { field: 'glCode', header: 'GL Code', width: '150px' }, { field: 'pc', header: 'PC', width: '150px' }, { field: 'cngl', header: 'CNGL', width: '150px' }, { field: 'status', header: 'Status', width: '150px' }],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_ID_REQUEST,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true
          },

        ]

      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
    this.activeStatus = [
      { label: 'Enabled', value: 'Enabled' },
      { label: 'Disabled', value: 'Disabled' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][BILLING_MODULE_COMPONENT.STOCK_ID_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
          if (this.row['sortOrderColumn']) {
            unknownVar['sortOrder'] = this.row['sortOrder'];
            unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
        case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("billing/stock-id/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["billing/stock-id-request/view"], { queryParams: { id: editableObject['stockId'] } });
            break;
        }
    }
  }

onChangeSelecedRows(e) {
  this.selectedRows = e;
}
}

