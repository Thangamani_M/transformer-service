
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, CrudType, RxjsService, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-stock-id-request-view',
  templateUrl: './stock-id-request-view.component.html'
})
export class StockIdRequestViewComponent implements OnInit {
  stockId: string;
  stockIdDetails: any;
  primengTableConfigProperties: any;
  StockIDRequestViewDetail: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.stockId = this.activatedRoute.snapshot.queryParams.id
    this.primengTableConfigProperties = {
      tableCaption: "View Stock ID Request",
      breadCrumbItems: [{ displayName: 'Billing Management', relativeRouterUrl: '' }, { displayName: 'Stock ID Request List', relativeRouterUrl: '/billing/stock-id-request' },
      { displayName: 'View Stock ID Request', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.StockIDRequestViewDetail = [
      { name: 'Stock ID', value: '' },
      { name: 'Type Of Fee', value: '' },
      { name: 'Stock ID Type', value: '' },
      { name: 'Stock Category', value: '' },
      { name: 'Tier Based Price', value: '' },
      { name: 'GL Code', value: '' },
      { name: 'PC', value: '' },
      { name: 'CNGL', value: '' },
      { name: 'Created By', value: '' },
      { name: 'Created On', value: '' },
      { name: 'Actioned By', value: '' },
      { name: 'Actioned On', value: '' },
      { name: 'Status', value: '' },
      { name: 'Reason', value: '' },
    ]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getstockIdDetailsById(this.stockId);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.STOCK_ID_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getstockIdDetailsById(stockId: string) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_REQUEST, stockId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stockIdDetails = response.resources;
          this.StockIDRequestViewDetail = [
            { name: 'Stock ID', value: response.resources?.stockIdName },
            { name: 'Type Of Fee', value: response.resources?.isFixedFee ? 'Fixed' : 'Calculated' },
            { name: 'Stock ID Type', value: response.resources?.stockIDTypeName },
            { name: 'Stock Category', value: response.resources?.stockCategoryName },
            { name: 'Tier Based Price', value: response.resources?.isTierBasedPrice ? 'Yes' : 'No' },
            { name: 'GL Code', value: response.resources?.glCode },
            { name: 'PC', value: response.resources?.pc },
            { name: 'CNGL', value: response.resources?.cngl },
            { name: 'Created By', value: response.resources?.createdBy },
            { name: 'Created On', value: response.resources?.createdOn, 'dd-MM-yyyy': '' },
            { name: 'Actioned By', value: response.resources?.actionedBy },
            { name: 'Actioned On', value: response.resources?.actionedOn, 'dd-MM-yyyy': '' },
            { name: 'Status', value: response.resources?.status, statusClass: response.resources?.cssClass },
            { name: 'Reason', value: response.resources?.reason },
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {

    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.stockId) {
      this.router.navigate(['billing/stock-id/add-edit'], { queryParams: { id: this.stockId } });
    }
  }
}

