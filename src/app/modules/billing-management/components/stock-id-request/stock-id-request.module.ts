import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RadioButtonModule } from 'primeng/radiobutton';
import { StockIdRequestAddEditComponent } from './stock-id-request-add-edit.component';
import { StockIdRequestListComponent } from './stock-id-request-list.component';
import { StockIdRequestRoutingModule } from './stock-id-request-routing.module';
import { StockIdRequestViewComponent } from './stock-id-request-view.component';



@NgModule({
  declarations: [StockIdRequestAddEditComponent, StockIdRequestViewComponent,StockIdRequestListComponent],
  imports: [
    CommonModule,
    StockIdRequestRoutingModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    RadioButtonModule
  ]
})
export class StockIdRequestModule { }
