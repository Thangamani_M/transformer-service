
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { StockDescriptionPricesListModel, StockDescriptionPricesListModel1, StockDescriptionPricesListModel2, StockIdRequestAddEditModel, StockIdRequestDescriptionListModel } from '@modules/others/configuration/models/stock-id-request.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
@Component({
  selector: 'app-stock-id-request-add-edit',
  templateUrl: './stock-id-request-add-edit.component.html',
  styleUrls: ['./stock-id-request-add-edit.component.scss']
})
export class StockIdRequestAddEditComponent implements OnInit {

  stockId: any
  stockIdDropDown: any = [];
  pcDropDown: any = [];
  cnglDropDown: any = [];
  glCodeDropDown: any = [];
  divisionDropDown: any = [];
  districtDropDown: any = [];
  dayDropDown = [];
  hourDropDown = [];
  shiftDropDown = [];
  gradeDropDown = [];
  dayName: any;
  hourName: any;
  gradeName: any;
  shiftName: any;
  stockIdForm: FormGroup;
  stockIdRequestDescriptions: FormArray;
  stockIdRequestDescriptionsPrices: FormArray
  loggedUser: any;
  dayHourShiftGrade = false;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  stockIdDetailsData: any
  stockIdDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  stokIdDropdown: any = [];
  geoLocationCodeId: any;
  profitCenterId: any;
  creditNoteGeoLocationId: any;
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  taxDetails: any;
  standardTax: any;
  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.stockId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createstockIdForm();
    this.getStockIdDropdown()
    this.getDivisionDropdown()
    this.getDistrictDropdown();
    this.getTaxCalcutation()
    this.getDayDropdown()
    this.getHourDropdown()
    this.getShiftDropdown();
    this.getGradeDropdown();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.stockIdForm.get('stockId').valueChanges.subscribe((stockId: string) => {
      this.onSelectStockId(stockId)
    })
    if (this.stockId) {
      this.getStockIdDetailsById().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        let stockIdRequestAddEditModel = new StockIdRequestAddEditModel(response.resources);
        this.stockIdDetailsData = response.resources;
        this.stockIdForm.patchValue(stockIdRequestAddEditModel);
        this.geoLocationCodeId = response.resources.geoLocationCodeId;
        this.profitCenterId = response.resources.profitCenterId;
        this.creditNoteGeoLocationId = response.resources.creditNoteGeoLocationId;
        this.stockIdForm.controls['geoLocationCodeId'].setValue(response.resources.glCode);
        this.stockIdForm.controls['profitCenterId'].setValue(response.resources.pc);
        this.stockIdForm.controls['creditNoteGeoLocationId'].setValue(response.resources.cngl);
        if (this.stockIdDetailsData.stockIdRequestDescriptions.length == 0) {
          this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray;
          let StockIdRequestDescriptionListModelControl = new StockIdRequestDescriptionListModel();
          this.stockIdRequestDescriptions.push(this.createStockIdRequestDescriptionListModel(StockIdRequestDescriptionListModelControl));
          if (!response.resources.isFixedFee && !response.resources.isTierBasedPrice) { //calculated,no
            this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
            this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel());
          } else if (response.resources.isFixedFee && !response.resources.isTierBasedPrice) { // fixed,no
            this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
            this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel1());
          } else if (response.resources.isFixedFee && response.resources.isTierBasedPrice) { // fixed,yes
            this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
            this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel2());
          }
        }

        this.stockIdForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.stockIdForm.get('modifiedUserId').setValue(this.loggedUser.userId)
      })

    } else {
      this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray
      this.stockIdRequestDescriptions.push(this.createStockIdRequestDescriptionListModel());
      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
      this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel());
    }
    this.onFormControlChange();
  }

  filterServiceTypeDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_GEO_LOCATION_CODE, null, true, prepareGetRequestHttpParams(null, null, {
      search: serachValue
    }))
  }

  filterPcDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PC, null, true, prepareGetRequestHttpParams(null, null, {
      search: serachValue
    }))
  }

  filterCnglDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CNGL_LOCATION, null, true, prepareGetRequestHttpParams(null, null, {
      search: serachValue
    }))
  }

  onFormControlChange() {
    this.stockIdForm.get('geoLocationCodeId').valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      tap(x => console.log(x)),
      switchMap(search => search ? this.filterServiceTypeDetailsBySearchOptions(search) : this.glCodeDropDown = []
      )
    ).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode == 200) {
        if (response.resources.length === 0) {
        }
        if (response.resources != null) {
          this.glCodeDropDown = response.resources;
          if (this.glCodeDropDown.length == 0) {
            this.glCodeDropDown = [];
          }
        } else {
          this.glCodeDropDown = [];
        }
      }
    });

    this.stockIdForm.get('profitCenterId').valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      tap(x => console.log(x)),
      switchMap(search => search ? this.filterPcDetailsBySearchOptions(search) : this.pcDropDown = []
      )
    ).subscribe((response: any) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (response.resources.length === 0) {
        }
        if (response.resources != null) {
          this.pcDropDown = response.resources;
          if (this.pcDropDown.length == 0) {
            this.pcDropDown = [];
          }
        } else {
          this.pcDropDown = [];
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.stockIdForm.get('creditNoteGeoLocationId').valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      tap(x => console.log(x)),
      switchMap(search => search ? this.filterCnglDetailsBySearchOptions(search) : this.cnglDropDown = []
      )
    ).subscribe((response: any) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (response.resources.length === 0) {
        }
        if (response.resources != null) {
          this.cnglDropDown = response.resources;
          if (this.cnglDropDown.length == 0) {
            this.cnglDropDown = [];
          }
        } else {
          this.cnglDropDown = [];
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onSelectedAddressFromAutoComplete(value, cdm, id) {
    this.stockIdForm.get('geoLocationCodeId').setValue(cdm.displayName);
    this.geoLocationCodeId = id;
  }

  onSelectedPcFromAutoComplete(value, cdm, id) {
    this.stockIdForm.get('profitCenterId').setValue(cdm.displayName);
    this.profitCenterId = id;
  }

  onSelectedCnglFromAutoComplete(value, cdm, id) {
    this.stockIdForm.get('creditNoteGeoLocationId').setValue(cdm.displayName);
    this.creditNoteGeoLocationId = id;
  }

  createstockIdForm(): void {
    let stockIdRequestAddEditModel = new StockIdRequestAddEditModel();
    this.stockIdForm = this.formBuilder.group({
      stockIdRequestDescriptions: this.formBuilder.array([])
    });
    Object.keys(stockIdRequestAddEditModel).forEach((key) => {
      this.stockIdForm.addControl(key, new FormControl(stockIdRequestAddEditModel[key]));
    });
    this.stockIdForm = setRequiredValidator(this.stockIdForm, ["stockId", "geoLocationCodeId", "profitCenterId", "creditNoteGeoLocationId"]);
    this.stockIdForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.stockIdForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  //Create FormArray controls
  createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel?: StockIdRequestDescriptionListModel): FormGroup {
    let StockIdRequestDescriptionListModelControl = new StockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionListModelControl).forEach((key) => {
      if (key === 'stockDescriptionPrices') {
        formControls[key] = this.formBuilder.array([])
      } else if (key === 'stockDescription') {
        formControls[key] = [{ value: StockIdRequestDescriptionListModelControl[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionListModelControl[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createStockIdRequestDescriptionPriceListModel(stockDescriptionPricesListModel?: StockDescriptionPricesListModel): FormGroup {
    let StockIdRequestDescriptionPriceListModelControl = new StockDescriptionPricesListModel(stockDescriptionPricesListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionPriceListModelControl).forEach((key) => {
      if (key === 'stockDescriptionPriceId' || key == 'districtList') {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createStockIdRequestDescriptionPriceListModel1(stockDescriptionPricesListModel?: StockDescriptionPricesListModel1): FormGroup {
    let StockIdRequestDescriptionPriceListModelControl = new StockDescriptionPricesListModel1(stockDescriptionPricesListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionPriceListModelControl).forEach((key) => {

      if (key === 'stockDescriptionPriceId' || key === 'tierId' || key == "stockDescriptionPriceId1" || key == "stockDescriptionPriceId2" || key == "stockDescriptionPriceId3" || key == "districtList") {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }]
      }
      else {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    let stockIdReqDescFormGroup = this.formBuilder.group(formControls);
    stockIdReqDescFormGroup.get('exclVAT').valueChanges.subscribe(data => {
      data = +data;
      stockIdReqDescFormGroup.get('vatAmount').setValue(data * (this.standardTax / 100));
      stockIdReqDescFormGroup.get('inclVAT').setValue(data + stockIdReqDescFormGroup.get('vatAmount').value);
    });
    return stockIdReqDescFormGroup;
    // return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createStockIdRequestDescriptionPriceListModel2(stockDescriptionPricesListModel?: StockDescriptionPricesListModel2): FormGroup {
    let StockIdRequestDescriptionPriceListModelControl = new StockDescriptionPricesListModel2(stockDescriptionPricesListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionPriceListModelControl).forEach((key) => {
      // if (key === 'tierDetails') {
      //   formControls[key] = this.formBuilder.array([])
      if (key === 'stockDescriptionPriceId' || key === 'stockDescriptionPriceId1' || key === 'stockDescriptionPriceId2' || key === 'stockDescriptionPriceId3' || key === 'tierId') {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }]
        // } else if(key === 'vatAmount1' || key === 'inclVAT1' || key === 'vatAmount2' || key === 'inclVAT2' || key === 'vatAmount3' || key === 'inclVAT3') {
        //   formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: true }]
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    let stockIdReqDescFormGroup = this.formBuilder.group(formControls);
    stockIdReqDescFormGroup.get('exclVAT1').valueChanges.subscribe(data => {
      data = +data;
      stockIdReqDescFormGroup.get('vatAmount1').setValue(data * (this.standardTax / 100));
      stockIdReqDescFormGroup.get('inclVAT1').setValue(data + stockIdReqDescFormGroup.get('vatAmount1').value, { emitEvent: false });
    });

    stockIdReqDescFormGroup.get('inclVAT1').valueChanges.subscribe(data => {
      data = +data;
      let a = `1.${this.standardTax}`;
      let b = parseFloat(a);
      stockIdReqDescFormGroup.get('exclVAT1').setValue(data / b, { emitEvent: false });
      stockIdReqDescFormGroup.get('vatAmount1').setValue(data - stockIdReqDescFormGroup.get('exclVAT1').value);
    });


    stockIdReqDescFormGroup.get('exclVAT2').valueChanges.subscribe(data => {
      data = +data;
      stockIdReqDescFormGroup.get('vatAmount2').setValue(data * (this.standardTax / 100));
      stockIdReqDescFormGroup.get('inclVAT2').setValue(data + stockIdReqDescFormGroup.get('vatAmount2').value, { emitEvent: false });
    });
    stockIdReqDescFormGroup.get('inclVAT2').valueChanges.subscribe(data => {
      data = +data;
      let a = `1.${this.standardTax}`;
      let b = parseFloat(a);
      stockIdReqDescFormGroup.get('exclVAT2').setValue(data / b, { emitEvent: false });
      stockIdReqDescFormGroup.get('vatAmount2').setValue(data - stockIdReqDescFormGroup.get('exclVAT2').value);
      return
    });
    stockIdReqDescFormGroup.get('exclVAT3').valueChanges.subscribe(data => {
      data = +data;
      stockIdReqDescFormGroup.get('vatAmount3').setValue(data * (this.standardTax / 100));
      stockIdReqDescFormGroup.get('inclVAT3').setValue(data + stockIdReqDescFormGroup.get('vatAmount3').value, { emitEvent: false });
    });

    stockIdReqDescFormGroup.get('inclVAT3').valueChanges.subscribe(data => {
      data = +data;
      let a = `1.${this.standardTax}`;
      let b = parseFloat(a);
      stockIdReqDescFormGroup.get('exclVAT3').setValue(data / b, { emitEvent: false });
      stockIdReqDescFormGroup.get('vatAmount3').setValue(data - stockIdReqDescFormGroup.get('exclVAT3').value);
    });
    return stockIdReqDescFormGroup;
    // return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getstockIdRequestDescriptionsListArray(): FormArray {
    if (!this.stockIdForm) return;
    return this.stockIdForm.get("stockIdRequestDescriptions") as FormArray;
  }


  onChange(value, i, selected) {
    if (!this.dayHourShiftGrade) {
      return
    }
    if (selected === 'day') {
      value = this.stockIdForm.get('stockIdRequestDescriptions')['controls'][i].controls['stockIdDayConfigId']
      let found = this.dayDropDown.find(e => e.id == value.value);
      this.dayName = found.displayName
    } else if (selected === 'hour') {
      value = this.stockIdForm.get('stockIdRequestDescriptions')['controls'][i].controls['stockIdHourConfigId']
      let found = this.hourDropDown.find(e => e.id == value.value);
      this.hourName = found.displayName
    } else if (selected === 'shift') {
      value = this.stockIdForm.get('stockIdRequestDescriptions')['controls'][i].controls['stockIdShiftConfigId']
      let found = this.shiftDropDown.find(e => e.id == value.value);
      this.shiftName = found.displayName
    } else if (selected === 'grade') {
      value = this.stockIdForm.get('stockIdRequestDescriptions')['controls'][i].controls['stockIdGradeConfigId']
      let found = this.gradeDropDown.find(e => e.id == value.value);
      this.gradeName = found.displayName
    }
    if (this.gradeName || this.shiftName || this.hourName || this.dayName) {
      // let finalDescription = `${this.dayName}-${this.hourName}-${this.shiftName}-${this.gradeName}`
      let finalDescription = `${this.dayName ? `${this.dayName}` : ''}${this.hourName ? `${this.dayName ? `-` : ''}${this.hourName}` : ''}${this.shiftName ? `${this.hourName || this.dayName ? `-` : ''}${this.shiftName}` : ''}${this.gradeName ? `${this.hourName || this.dayName || this.shiftName ? `-` : ''}${this.gradeName}` : ''}`
      this.stockIdForm.get('stockIdRequestDescriptions')['controls'][i].controls['stockDescription'].setValue(finalDescription)
    }

  }

  //Create FormArray
  getstockIdRequestDescriptionsPriceListArray(index: number): FormArray {
    if (!this.stockIdForm) return;
    return this.stockIdRequestDescriptions.at(index).get("stockDescriptionPrices") as FormArray

  }

  //Get Details
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_REQUEST,
      this.stockId
    );
  }

  getStockIdDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_STOCK_ID, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stokIdDropdown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTaxCalcutation() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      TechnicalMgntModuleApiSuffixModels.STANDARD_TAX, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.taxDetails = response.resources;
          this.standardTax = response.resources.taxPercentage;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getPCDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PC, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.pcDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCNGLDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CNGL_LOCATION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.cnglDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getGLCodeDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_GEO_LOCATION_CODE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.glCodeDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onDivisionChange(e, formArrayIndex, index?) {
    let divisionID = this.getstockIdRequestDescriptionsPriceListArray(formArrayIndex).controls[index].get("divisionId").value
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DISTRICTS, null, false,
      prepareGetRequestHttpParams(null, null,
        { divisionId: divisionID }))
      .subscribe((response: IApplicationResponse) => {
        this.districtDropDown = [];
        if (response && response.resources && response.isSuccess) {
          this.districtDropDown = response.resources;
          this.getstockIdRequestDescriptionsPriceListArray(formArrayIndex).controls[index].get("districtList").setValue([]);
          this.getstockIdRequestDescriptionsPriceListArray(formArrayIndex).controls[index].get("districtList").setValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })

  }

  getDistrictDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DISTRICTS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.districtDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDayDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DAY_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dayDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getHourDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_HOUR_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.hourDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getShiftDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_SHIFT_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.shiftDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getGradeDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_GRADE_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.gradeDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }



  onSelectStockId(stockId: string) {
    // this.getstockIdRequestDescriptionsListArray
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID, stockId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stockIdDetails = response.resources;
          if (this.stockIdDetails.isDay || this.stockIdDetails.isGrade || this.stockIdDetails.isHours || this.stockIdDetails.isShift) {
            this.dayHourShiftGrade = true;

          } else {
            this.dayHourShiftGrade = false;

          }
          this.stockIdForm.get('isFixedFee').setValue(response.resources.isFixedFee)
          this.stockIdForm.get('isTierBasedPrice').setValue(response.resources.isTierBasedPrice)

          if (this.stockId) {
            this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray
            this.stockIdDetailsData.stockIdRequestDescriptions.forEach((stockIdRequestDescriptionListModel: any, index) => {
              this.stockIdRequestDescriptions.push(this.createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel));
              if (!this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { //calculated,no
                stockIdRequestDescriptionListModel.divisionDetails.forEach((stockDescriptionPricesListModel: StockDescriptionPricesListModel, childIndex) => {
                  this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(index)
                  this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel(stockDescriptionPricesListModel));
                  this.onDivisionChange("", index, childIndex)
                });
              } else if (this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { // fixed,no
                stockIdRequestDescriptionListModel.divisionDetails.forEach((stockDescriptionPricesListModel: StockDescriptionPricesListModel1, childIndex) => {
                  this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(index)
                  this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel1(stockDescriptionPricesListModel));
                  this.onDivisionChange("", index, childIndex)

                });
              } else if (this.stockIdForm.get('isFixedFee').value && this.stockIdForm.get('isTierBasedPrice').value) { // fixed,yes
                stockIdRequestDescriptionListModel.divisionDetails.forEach((stockDescriptionPricesListModel: StockDescriptionPricesListModel2, childIndex) => {
                  this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(index)

                  this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel2(stockDescriptionPricesListModel));
                  this.onDivisionChange("", index, childIndex)
                });

              }

            });
          } else {
            this.stockIdRequestDescriptionsPrices.clear()
            if (!response.resources.isFixedFee && !response.resources.isTierBasedPrice) { //calculated,no

              this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
              this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel());

            } else if (response.resources.isFixedFee && !response.resources.isTierBasedPrice) { // fixed,no

              this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
              this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel1());

            } else if (response.resources.isFixedFee && response.resources.isTierBasedPrice) { // fixed,yes
              this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
              this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel2());

            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Add Employee Details
  addDescription(): void {
    this.dayName = '';
    this.hourName = '';
    this.gradeName = '';
    this.shiftName = '';


    if (this.getstockIdRequestDescriptionsListArray.invalid) {

      return;
    };

    if (!this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { //calculated,no
      this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray;
      let stockIdRequestDescriptionListModel = new StockIdRequestDescriptionListModel();
      this.stockIdRequestDescriptions.insert(0, this.createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel));

      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel(stockDescriptionPricesListModel));

    } else if (this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { // fixed,no
      this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray;
      let stockIdRequestDescriptionListModel = new StockIdRequestDescriptionListModel();
      this.stockIdRequestDescriptions.insert(0, this.createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel));

      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel1();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel1(stockDescriptionPricesListModel));

    } else if (this.stockIdForm.get('isFixedFee').value && this.stockIdForm.get('isTierBasedPrice').value) { // fixed,yes
      this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray;
      let stockIdRequestDescriptionListModel = new StockIdRequestDescriptionListModel();
      this.stockIdRequestDescriptions.insert(0, this.createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel));

      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel2();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel2(stockDescriptionPricesListModel));

    }
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeDescription(i: number): void {
    if (!this.getstockIdRequestDescriptionsListArray.controls[i].value.stockDescriptionId) {
      this.getstockIdRequestDescriptionsListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getstockIdRequestDescriptionsListArray.controls[i].value.stockDescriptionNameId && this.getstockIdRequestDescriptionsListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SMS_GROUP_EMPLOYEE,
          this.getstockIdRequestDescriptionsListArray.controls[i].value.stockDescriptionNameId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getstockIdRequestDescriptionsListArray.removeAt(i);
            }
            if (this.getstockIdRequestDescriptionsListArray.length === 0) {
              this.addDescription();
            };
          });
      }
      else {
        this.getstockIdRequestDescriptionsListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  //Add Employee Details
  addPrice(i, j): void {
    if (this.getstockIdRequestDescriptionsPriceListArray(i).invalid) {
      return;
    };
    if (!this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { //calculated,no
      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(i);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel(stockDescriptionPricesListModel));

    } else if (this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { // fixed,no

      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(i);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel1();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel1(stockDescriptionPricesListModel));

    } else if (this.stockIdForm.get('isFixedFee').value && this.stockIdForm.get('isTierBasedPrice').value) { // fixed,yes
      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(i);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel2();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel2(stockDescriptionPricesListModel));
    }
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removePrice(i, j): void {
    if (!this.getstockIdRequestDescriptionsPriceListArray(i).controls[j].value.stockDescriptionPriceId) {
      this.getstockIdRequestDescriptionsPriceListArray(i).removeAt(j);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getstockIdRequestDescriptionsListArray.controls[i].value.cmcsmsGroupEmployeeId && this.getstockIdRequestDescriptionsListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SMS_GROUP_EMPLOYEE,
          this.getstockIdRequestDescriptionsListArray.controls[i].value.cmcsmsGroupEmployeeId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getstockIdRequestDescriptionsListArray.removeAt(i);
            }
            if (this.getstockIdRequestDescriptionsListArray.length === 0) {
              this.addDescription();
            };
          });
      }
      else {
        this.getstockIdRequestDescriptionsListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  backPage() {
    this.router.navigate(['billing/stock-id/add-edit'], { queryParams: { id: this.stockId } });
  }

  onSubmit(): void {
    if (this.stockIdForm.invalid) {
      return;
    }

    let formValue = this.stockIdForm.value;
    formValue.geoLocationCodeId = this.geoLocationCodeId
    formValue.profitCenterId = this.profitCenterId
    formValue.creditNoteGeoLocationId = this.creditNoteGeoLocationId

    if (!this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { //calculated,no
      formValue = formValue
    } else if (this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { // fixed,no

      formValue.stockIdRequestDescriptions.forEach(element => {
        element.stockDescriptionPrices.forEach(element1 => {
          element1['tierDetails'] = [
            {
              "tierId": null,
              "exclVAT": element1.exclVAT,
              "inclVAT": element1.inclVAT,
              "vatAmount": element1.vatAmount,
              "stockDescriptionPriceId": element1.stockDescriptionPriceId,
              "stockDescriptionPriceId1": element1.stockDescriptionPriceId1 && element1.stockDescriptionPriceId1,
              "stockDescriptionPriceId2": element1.stockDescriptionPriceId2 && element1.stockDescriptionPriceId2,
              "stockDescriptionPriceId3": element1.stockDescriptionPriceId3 && element1.stockDescriptionPriceId3,
            }
          ]
        });
      });

    } else if (this.stockIdForm.get('isFixedFee').value && this.stockIdForm.get('isTierBasedPrice').value) { // fixed,yes

      formValue.stockIdRequestDescriptions.forEach(element => {
        element.stockDescriptionPrices.forEach(element1 => {
          element1['tierDetails'] = [
            {
              "tierId": 1,
              "exclVAT": element1.exclVAT1,
              "inclVAT": element1.inclVAT1,
              "vatAmount": element1.vatAmount1,
              "stockDescriptionPriceId": element1.stockDescriptionPriceId1 && element1.stockDescriptionPriceId1,

            },
            {
              "tierId": 2,
              "exclVAT": element1.exclVAT2,
              "inclVAT": element1.inclVAT2,
              "vatAmount": element1.vatAmount2,
              "stockDescriptionPriceId": element1.stockDescriptionPriceId2 && element1.stockDescriptionPriceId2
            },
            {
              "tierId": 3,
              "exclVAT": element1.exclVAT3,
              "inclVAT": element1.inclVAT3,
              "vatAmount": element1.vatAmount3,
              "stockDescriptionPriceId": element1.stockDescriptionPriceId3 && element1.stockDescriptionPriceId3,
            }
          ]
        });
      });
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.stockId) ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_REQUEST, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_REQUEST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/billing/stock-id-request?tab=0');
      }
    })
  }
}

