import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockIdRequestAddEditComponent } from './stock-id-request-add-edit.component';
import { StockIdRequestListComponent } from './stock-id-request-list.component';
import { StockIdRequestViewComponent } from './stock-id-request-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  // { path: '', redirectTo: 'list', canActivate: [AuthGuard], pathMatch: 'full' },
  { path: '', component: StockIdRequestListComponent, canActivate: [AuthGuard], data: { title: 'Stock ID Request list' } },
  { path: 'add-edit', component: StockIdRequestAddEditComponent, canActivate: [AuthGuard], data: { title: 'Stock ID Request Add/Edit' } },
  { path: 'view', component: StockIdRequestViewComponent, canActivate: [AuthGuard], data: { title: 'Stock ID Request View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class StockIdRequestRoutingModule { }
