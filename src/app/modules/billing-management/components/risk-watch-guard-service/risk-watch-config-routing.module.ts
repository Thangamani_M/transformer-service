import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuardServiceComponent } from './add-service/guard-service.component';
const routes: Routes = [
  { path: '', redirectTo: 'add-service', pathMatch: 'full' },
  { path: 'add-service', component: GuardServiceComponent, data: { title: 'Add Service - Risk Watch & Request Guard' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RiskWatchGuardServiceRoutingModule { }
