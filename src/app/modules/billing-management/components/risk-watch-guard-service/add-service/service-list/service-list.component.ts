import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { AddGuardServiceComponent } from '../../gaurd-service-request/add-guard-service/add-guard-service.component';
import { AddSubscriptionComponent } from '../add-subscription/add-risk-watch.component';
import { CancelSubscriptionComponent } from '../cancel-subscription/cancel-risk-watch.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-risk-watch-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss'],
})
export class ServiceListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @Input() selectedAddress: any
  @Input() customerId: any
  @Input() addressStatus: any
  @Output() emitEvent = new EventEmitter<any>();
  @Output() emitServiceData = new EventEmitter<any>();
  observableResponse;
  loading = false;
  statusConfirm = false
  deletConfirm = false;
  showDialogSpinner = false
  selectedFilterDate: any;
  customerObject :any ;
  primengTableConfigProperties: any = {
    tableCaption: "Service",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Risk Watch service',
          dataKey: 'riskWatchConfigId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: false,
          checkBox: false,
          enableRowDelete: false,
          enableStatusActiveAction: false,
          enableFieldsSearch: false,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [{ field: 'retainerName', header: 'Retainer Name', width:'110px',hideSortIcon:true },
          { field: 'stockId', header: 'Stock ID', width:'110px',hideSortIcon:true  },
          { field: 'monthlyFee', header: 'Monthly Fee', width:'110px',hideSortIcon:true  },
          { field: 'cycleMonths', header: 'Cycle (Months)' , width:'110px',hideSortIcon:true },
          { field: 'shiftsPerCycle', header: 'Shifts Per Cycle', width:'110px',hideSortIcon:true  },
          { field: 'shiftValue', header: 'Shift Value' , width:'110px',hideSortIcon:true },
          { field: 'isActive', header: 'Status' , width:'110px',hideSortIcon:true },
          { field: 'signUpPeriod', header: 'Sign Up Period', width:'110px',hideSortIcon:true  },
          { field: 'planStartDate', header: 'Start Date', width:'110px',isDateTime:true,hideSortIcon:true  },
          { field: 'planEndDate', header: 'End Date' , width:'110px',isDateTime:true,hideSortIcon:true },
          { field: 'usedShift', header: 'Used Shift' , width:'110px',hideSortIcon:true },
          { field: 'unUsedShift', header: 'Remaining Shift', width:'110px',hideSortIcon:true  },
          ],
          apiSuffixModel: "EventMgntModuleApiSuffixModels.RISK_WATCH",
          moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          enableMultiDeleteActionBtn: false,
          ebableAddActionBtn: true
        },
      ]
    }
  }

  constructor(private rxjsService: RxjsService,private store: Store<AppState>,public dialogService: DialogService,private router: Router,private activatedRoute : ActivatedRoute,private crudService: CrudService) {
      super();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.selectedAddress || changes.customerId) {
      this.selectedAddress = changes?.selectedAddress?.currentValue
      this.addressStatus =this.selectedAddress?.availability;
      this.customerObject= { customerId: this.customerId, siteAddressId: this.selectedAddress.addressId };
      if (this.customerObject.customerId && this.customerObject.siteAddressId) {
        this.getActiveService(null, null, this.customerObject);
      }
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getActiveService(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RISK_WATCH_RETAINER_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = [];
        if(this.observableResponse.retainerDetailsList && this.observableResponse.riskwatchconfigdetails){
          this.dataList = [{ ...this.observableResponse.retainerDetailsList, ...this.observableResponse.riskwatchconfigdetails }];
          this.router.navigate(
            [],
            {
              relativeTo: this.activatedRoute, // current route
              queryParams: { riskWatchRequestId:this.observableResponse?.riskwatchconfigdetails?.riskWatchRequestId, addressId : this.selectedAddress.addressId }, // new param
              queryParamsHandling: 'merge' // appending with old param's
            });
        }
        this.loading = false;
      }

      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.EDIT:
      this.emitServiceData.emit(this.observableResponse)
        break;
        case CrudType.VIEW:
          this.emitServiceData.emit(this.observableResponse)
            break;
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  async openAddEditPage(type: CrudType | string, object: any) {
    if(this.addressStatus == "Activation Pending"){
      let _text = "Service still in Cool down period"
      // this.openCreateRequestGuardDialog()
      this.openCustomDialog(_text, "Notification", false)
      return;
    }
    if(this.addressStatus == "Active" && this.dataList.length==0 ){
      this.openCreateServiceDialog()
      return;
    }
    if(this.addressStatus == "Active"){
      this.openCreateRequestGuardDialog();
      return;
    }
    if(this.addressStatus == "Eligible" && this.dataList.length ==0){
      this.openCreateServiceDialog();
      return;
    }
  }

  openCreateServiceDialog(){
    let customText = "Are you sure you would like to add the risk watch service to this customer?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable:true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        const ref = this.dialogService.open(AddSubscriptionComponent, {
          header: `Add Risk Watch`,
          showHeader: true,
          baseZIndex: 10000,
          width: "850px",
          data: {addressId : this.selectedAddress.addressId,customerId: this.customerId, riskWatchConfigId : this.observableResponse?.retainerDetailsList?.riskWatchConfigId },
        });
        ref.onClose.subscribe((resp) => {
          this.emitEvent.emit(true);
          this.getActiveService(null, null, this.customerObject);
          if (resp === false) {
            let _text = "Service Added successfully"
            this.openCustomDialog(_text, "Notification", false)
          }
        });
      }
    });
  }

  openCreateRequestGuardDialog(){
    this.rxjsService.setIsRefershRiskwatchRequestsProperty(true)
    let customText = "Are you sure you would like to request a guard for this site?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable:true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        const ref = this.dialogService.open(AddGuardServiceComponent, {
          header: `Request Guard`,
          showHeader: true,
          baseZIndex: 10000,
          width: "1200px",
          data: { customerId: this.customerId,riskWatchRequestId:this.observableResponse?.riskwatchconfigdetails?.riskWatchRequestId,
          addressId: this.selectedAddress.addressId},
        });
        ref.onClose.subscribe((resp) => {
          this.emitEvent.emit(true);
          if (resp === false) {
            this.rxjsService.setIsRefershRiskwatchRequestsProperty(true)

            let _text = "Guard Request Added successfully"
            this.openCustomDialog(_text, "Notification", false);

          }
        });
      }
    });
  }

  openCancelServiceDialog(){
    if(this.dataList.length==0) return '';
    let customText = "Are you sure you would like to cancel the risk watch Service to this customer?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable:true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        const ref = this.dialogService.open(CancelSubscriptionComponent, {
          header: `Service Cancellation`,
          showHeader: true,
          baseZIndex: 10000,
          width: "520px",
          data: { customerId: this.customerId,riskWatchRequestId:this.observableResponse?.riskwatchconfigdetails?.riskWatchRequestId },
        });
        ref.onClose.subscribe((resp) => {
          this.emitEvent.emit(true)
          this.getActiveService(null, null, this.customerObject);
          if (resp === false) {
            let _text = "Service has been cancelled successfully"
            this.openCustomDialog(_text, "Notification", false);
          }
        });
      }
    });
  }

  openCustomDialog (customText,header,boolean){
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: header,
      showHeader: false,
      closable:true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText, isShowNo : boolean},
    });
    confirm.onClose.subscribe((resp) => {
      if(!resp){
      }
    });
  }
}
