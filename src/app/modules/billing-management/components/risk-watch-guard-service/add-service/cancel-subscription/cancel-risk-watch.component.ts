import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CancelServiceModel, CustomerModuleApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-cancel-risk-watch-guard',
  templateUrl: './cancel-risk-watch.component.html',
  styleUrls: ['./cancel-risk-watch.component.scss'],
})

export class CancelSubscriptionComponent implements OnInit {
  formConfigs = formConfigs;
  cancelServiceForm: FormGroup;
  loggedUser: UserLogin;
  riskWatchDetails: any

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.getRiskWatchDetails();
    this.createCancelServiceForm();
  }

  getRiskWatchDetails() {
    let otherParams = {
      riskWatchRequestId: this.config.data.riskWatchRequestId
    }
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RISK_WATCH_CANCEL,
      undefined,
      false,
      prepareRequiredHttpParams(otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.riskWatchDetails = response.resources;
        this.cancelServiceForm.get("outStandingDueAmount").setValue(this.riskWatchDetails.totalAmount)
        this.cancelServiceForm.get("shiftUsedIncycle").setValue(this.riskWatchDetails.shiftUsed)
      }
    })
  }

  createCancelServiceForm(): void {
    let cancelServiceFormModel = new CancelServiceModel(this.config.data);
    this.cancelServiceForm = this.formBuilder.group({});
    Object.keys(cancelServiceFormModel).forEach((key) => {
      this.cancelServiceForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(cancelServiceFormModel[key]));
    });
  }

  onSubmit(): void {
    if (this.cancelServiceForm.invalid) return;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RISK_WATCH, this.cancelServiceForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialogClose(false);
        this.cancelServiceForm.reset();
      }
    })
  }

  dialogClose(event): void {
    this.ref.close(event);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
