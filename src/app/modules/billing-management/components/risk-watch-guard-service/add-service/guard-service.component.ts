import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';


@Component({
  selector: 'app-risk-watch-guard-service',
  templateUrl: './guard-service.component.html',
  styleUrls: ['./guard-service.component.scss']
})
export class GuardServiceComponent implements OnInit {


    customerId: string;
    addressId: string;
    selectedAddress :any = {}
    customerDetails: any ={ customerName:"", addressDetailslist : []};
    // Status lebel
    statusLabel :string
    statusClass: string
    addressIndex  = 0;

    changesDefected: Boolean = false;
    showViewDialog: Boolean = false;
    emitData : any = {retainerDetailsList : {}};
    constructor(private activatedRoute: ActivatedRoute,private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
      this.customerId = this.activatedRoute.snapshot.queryParams.customerId
      this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
     }

     ngOnInit() {
      this.getRiskWatchAvailability();
    }

    getRiskWatchAvailability() {
        let object = {customerId : this.customerId}
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.RISK_WATCH_AVAILABILITY, null, false, prepareRequiredHttpParams(object))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.customerDetails = response.resources;
            if(this.customerDetails?.addressDetailslist){
              let index  = this.customerDetails?.addressDetailslist.findIndex(item=> item.addressId == this.addressId)
              this.addressIndex = index
              this.selectedAddress = this.customerDetails?.addressDetailslist[index];

              this.decideStatusLabel(this.selectedAddress?.availability)
            }

          }
         this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    selectAddress (event?){
      let _index =  event?event.target.value : 0
      this.selectedAddress = this.customerDetails?.addressDetailslist[_index];
      this.decideStatusLabel(this.selectedAddress?.availability)
    }

    decideStatusLabel (key){
      this.statusLabel = key;
      switch (key) {
        case 'Not Eligible':
          this.statusClass =  "status-label-pink"
          break;
        case 'Eligible':
          this.statusClass =  "status-label-green"
          break;
        case 'Active':
          this.statusClass =  "status-label-green"
            break;
        case 'Activation Pending':
          this.statusClass =  "status-label-orange"
              break;
        default:
          break;
      }
    }

    emitEvent (event){
      this.getRiskWatchAvailability();
      this.changesDefected = true;
    }

    emitServiceData (event){
      this.showViewDialog = true
      this.emitData = event
    }


  navigateToCustomer (){
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }
  }
