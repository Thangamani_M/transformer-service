import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AddRiskWatchServiceModel, CustomerModuleApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-add-risk-watch-guard',
  templateUrl: './add-risk-watch.component.html',
  styleUrls: ['./add-risk-watch.component.scss'],
})

export class AddSubscriptionComponent implements OnInit {
  formConfigs = formConfigs;
  riskWatchForm: FormGroup;
  loggedUser: UserLogin;
  riskWatchDetails : any

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.getRiskWatchDetails();
    this.createriskWatchForm();
  }

  getRiskWatchDetails(){
    let otherParams = {customerId :  this.config.data.customerId,
         siteAddressId :  this.config.data.addressId,
         riskWatchConfigId :  this.config.data.riskWatchConfigId}
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RISK_WATCH,
      undefined,
      false,
      prepareRequiredHttpParams(otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.riskWatchDetails =  response.resources;
        this.riskWatchForm.get("riskWatchConfigId").setValue(this.config.data.riskWatchConfigId)
        this.riskWatchForm.get("customerId").setValue(this.config.data.customerId)
        this.riskWatchForm.get("addressId").setValue(this.config.data.addressId)
        this.riskWatchForm.get("billingFrequency").setValue(this.riskWatchDetails.billingIntervalId)
        this.riskWatchForm.get("debtorAccountDetailId").setValue(this.riskWatchDetails?.debtorDropdown[0]?.debtorAccountDetailId)
        this.riskWatchForm.get("currentSiteAmount").setValue(this.riskWatchDetails.currentTotalAmount)
        this.riskWatchForm.get("newSiteAmount").setValue(this.riskWatchDetails.newTotalAmount)
        this.riskWatchForm.get("nextBillDate").setValue(new Date(this.riskWatchDetails.nextBillDate))
        this.riskWatchForm.get("proRataAmount").setValue(this.riskWatchDetails.proRateAmount)
        this.riskWatchForm.get("createdUserId").setValue(this.loggedUser?.userId)
      }
    })
  }

  createriskWatchForm(): void {
    let riskWatchFormModel = new AddRiskWatchServiceModel(this.config.data);
    this.riskWatchForm = this.formBuilder.group({});
    Object.keys(riskWatchFormModel).forEach((key) => {
      this.riskWatchForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(riskWatchFormModel[key]));
    });
  }

  onSubmit(): void {
    if (this.riskWatchForm.invalid) return;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.riskWatchForm.value.riskWatchId ? this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RISK_WATCH, this.riskWatchForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.RISK_WATCH, this.riskWatchForm.value);
        crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialogClose(false);
        this.riskWatchForm.reset();
      }
    })
  }

  dialogClose(event) :void {
    this.ref.close(event);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
