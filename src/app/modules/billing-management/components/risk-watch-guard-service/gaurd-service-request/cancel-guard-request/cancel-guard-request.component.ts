import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CancelShifteModel, CustomerModuleApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-cancel-guard-request',
  templateUrl: './cancel-guard-request.component.html',
  styleUrls: ['./cancel-guard-request.component.scss'],
})

export class CancelGuardRequestSComponent implements OnInit {
  formConfigs = formConfigs;
  cancelShiftForm: FormGroup;
  loggedUser: UserLogin;
  selectedShifts : any = []
  cancelReasonDropdown = []
  riskWatchRequestGuardsShiftId :any = []

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createcancelShiftForm();
    this.selectedShifts =  this.config.data.selectedRows;
    this.getShiftCancelDropdown()
    this.geListOfIds();
  }

  createcancelShiftForm(): void {
    let cancelShiftFormModel = new CancelShifteModel(this.config.data);
    this.cancelShiftForm = this.formBuilder.group({});
    Object.keys(cancelShiftFormModel).forEach((key) => {
      this.cancelShiftForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(cancelShiftFormModel[key]));
    });
    this.cancelShiftForm = setRequiredValidator(this.cancelShiftForm, ["shiftCancelReasonTypeId","riskWatchRequestGuardsShiftId"]);

  }

  geListOfIds (){
    this.selectedShifts.map(item=>{
      if(item.requestStatus !="Cancelled Guard"){
        this.riskWatchRequestGuardsShiftId.push(item.riskWatchRequestGuardsShiftId);
      }
    })
    this.cancelShiftForm.get('riskWatchRequestGuardsShiftId').setValue(this.riskWatchRequestGuardsShiftId.join(","));
  }

  getShiftCancelDropdown (){
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_SHIFT_CANCEL_REASON).subscribe(response=>{
      if(response.isSuccess){
        this.cancelReasonDropdown = response.resources;
      }
    })
  }

  onSubmit(): void {

    if (this.cancelShiftForm.invalid) return;


    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =  this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RISK_WATCH_GUARD, this.cancelShiftForm.value)
        crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialogClose(false);
        this.cancelShiftForm.reset();
      }
    })
  }

  dialogClose(event) :void {
    this.ref.close(event);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
