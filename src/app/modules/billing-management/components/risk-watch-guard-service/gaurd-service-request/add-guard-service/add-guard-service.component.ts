import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomerModuleApiSuffixModels, loggedInUserData, RequestGuardModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-add-guard-service',
  templateUrl: './add-guard-service.component.html',
  styleUrls: ['./add-guard-service.component.scss'],
})

export class AddGuardServiceComponent implements OnInit {
  formConfigs = formConfigs;
  requestGuardForm: FormGroup;
  loggedUser: UserLogin;
  riskWatchServiceDetails: any
  keyHolderList: any = []
  shiftList = []
  shiftFormData = []
  dateMin = new Date()
  dateMax = new Date()
  _tempFormArrayValue: any
  totalServiceFee = 0
  globalAction = { index: 0, action: "create" };


  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private snackbarService : SnackbarService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.getriskWatchServiceDetails();
    this.createrequestGuardForm();
    this.getKeyHolderDropdown()
  }

  getriskWatchServiceDetails() {
    let otherParams = { RiskWatchRequestId: this.config.data.riskWatchRequestId }
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RISK_WATCH_GUARD,
      undefined,
      false,
      prepareRequiredHttpParams(otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.riskWatchServiceDetails = response.resources;
        this.riskWatchServiceDetails.usedShiftAtCurrent= 0
        this.riskWatchServiceDetails.oldUsed= this.riskWatchServiceDetails.usedShift
        this.riskWatchServiceDetails.oldShiftsPerCycle = response.resources?.shiftsPerCycle;
        this.dateMax = new Date(this.riskWatchServiceDetails.planEndDate)
        this.requestGuardForm.get("contactNumberCountryCode").setValue("+27");
      }
    })
  }

  createrequestGuardForm(): void {
    let requestGuardFormModel = new RequestGuardModel(this.config.data);
    this.requestGuardForm = this.formBuilder.group({});
    Object.keys(requestGuardFormModel).forEach((key) => {
      this.requestGuardForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(requestGuardFormModel[key]));
    });
    this.requestGuardForm.addControl('shiftData', this.formBuilder.array([]));
    let shiftFormArray = this.shiftsFormArray;
    let ShiftFormGroup = this.formBuilder.group({
      date: [null, [Validators.required]],
      shift: [null, [Validators.required]],
      requestType: [null, [Validators.required]],

    })
    shiftFormArray.push(ShiftFormGroup);
    this.requestGuardForm = setRequiredValidator(this.requestGuardForm, ["incidentReferenceNumber", "incidentDescription", "guardingComments", "contactName", "contactNumber"]);
  }

  onContactNameChange(event) {
    if (event.value.displayName) {
      this.requestGuardForm.get('contactNumber').setValue(event.value.number);
      this.requestGuardForm.get('contactName').setValue(event.value.displayName);
      this.requestGuardForm.get('contactNumberCountryCode').setValue(event.value.contryCode)
      this.requestGuardForm.get('contactNumber').setValue(event.value.mobile1)
      this.requestGuardForm.get('keyholderId').setValue(event.value.id)
    } else {
      this.requestGuardForm.get('contactName').setValue(event.value);
    }
  }

  get shiftsFormArray(): FormArray {
    return this.requestGuardForm.get('shiftData') as FormArray;
  }

  _tempShiftList = {};

  addShiftList() {
    if (!this.shiftsFormArray.valid) return "";

    let formValue = this.shiftsFormArray.value[0];

    if (this.globalAction.action == "create") {
      if (this.riskWatchServiceDetails.shiftsPerCycle == 0 && formValue.requestType.guardRequestTypeNames == "Service" ) {
        return this.snackbarService.openSnackbar("Shift's Exceed the limit.", ResponseMessageTypes.WARNING);
      }
    }

    this._tempFormArrayValue = this.shiftsFormArray.value;
    let _servicesFee = formValue.requestType.guardRequestTypeNames == "Paid" ? this.riskWatchServiceDetails.shiftValue : 0
    if (formValue.requestType.guardRequestTypeNames == "Service" && this.globalAction.action != "edit" && (this.riskWatchServiceDetails.usedShiftAtCurrent > this.riskWatchServiceDetails.shiftsPerCycle)) {
      return this.snackbarService.openSnackbar("Shift's Exceed the limit.", ResponseMessageTypes.WARNING);
    }


    /// Shift data for Table
    let _shiftvalue = {
      dateStart: formValue.date,
      timeStart: formValue.shift.shiftStartTime,
      dateEnd: formValue.date,
      timeEnd: formValue.shift.shiftEndTime,
      requestType: formValue.requestType.guardRequestTypeNames,
      serviceFee: _servicesFee,
      shiftType: formValue.shift.guardShiftTypeNames,
      actualData: [formValue]
    },
      _shiftForm = { // Shift data for FORM
        guardShiftTypeId: formValue.shift.guardShiftTypeId,
        guardRequestTypeId: formValue.requestType.guardRequestTypeId,
        ShiftDate: new Date(formValue.date).toDateString(),
        servicesFee: _servicesFee,
      };

    if (!this._tempShiftList[`${new Date(formValue.date).valueOf()}-${formValue.shift.guardShiftTypeNames}`] && this.globalAction.action != "edit") { // temp for seting validation
      this._tempShiftList[`${new Date(formValue.date).valueOf()}-${formValue.shift.guardShiftTypeNames}`] = "ADDED";

      // setting into form and table

      this.shiftList.push(_shiftvalue);
      this.shiftFormData.push(_shiftForm)
      this.shiftsFormArray.reset();
      this.calculateServiceFee();

    }
    // check if service count exceed from available shift
    if (this.globalAction.action == "edit") {
      const count = this.getServiceCount(_shiftvalue)
      if (this.riskWatchServiceDetails.shiftsPerCycle >= count && formValue.requestType.guardRequestTypeNames != "Service" ) {
        // Replacing form data by index
        this.shiftList[this.globalAction.index] = _shiftvalue;
        this.shiftFormData[this.globalAction.index] = _shiftForm;
        this.globalAction.action = "create";
        this.shiftsFormArray.reset();
        this.calculateServiceFee();
      } else {

        return this.snackbarService.openSnackbar("Shift's Exceed the limit.", ResponseMessageTypes.WARNING);
      }


    }

  }

  getServiceCount(currentItem): Number {
    let _count = 0
    const findItem = this.shiftList.forEach((item, index) => {
      if (item.requestType == "Service" && index != this.globalAction.index) {
        _count++;
      }
    })
    return _count + (currentItem.requestType == "Service" ? 1 : 0);
  }

  calculateServiceFee() {
    this.totalServiceFee = 0
    this.riskWatchServiceDetails.usedShift = this.riskWatchServiceDetails.oldUsed || 0
    this.riskWatchServiceDetails.usedShiftAtCurrent = 0
    this.riskWatchServiceDetails.shiftsPerCycle = this.riskWatchServiceDetails.oldShiftsPerCycle;
    this.shiftList.map(item => {
      if (item.requestType == "Service") {
        this.riskWatchServiceDetails.usedShift += 1;
        this.riskWatchServiceDetails.usedShiftAtCurrent += 1;
        this.riskWatchServiceDetails.shiftsPerCycle -= 1;

      }
      this.totalServiceFee += item.serviceFee;
    })
  }

  doAction(index, shift, action) {
    this.globalAction = { index: index, action: action };
    if (action == "edit") {
      this.requestGuardForm.get('shiftData').setValue(shift.actualData);
    }
    if (action == 'delete') {

      this.shiftFormData.splice(index, 1);
      this.shiftList.splice(index, 1);
      this._tempShiftList[`${new Date(shift.dateStart).valueOf()}-${shift.shiftType}`] = "";
      this.calculateServiceFee();
    }
  }

  getKeyHolderDropdown() {
    let otherParams = { customerId: this.config.data.customerId, addressId : this.config.data.addressId }
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID,
      undefined,
      false,
      prepareRequiredHttpParams(otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        let _arr = [];
        response.resources.forEach(item => {
          let _number = item.mobile1.split(" ");
          _arr.push({ id: item.keyHolderId, displayName: item.customerName, number: _number[1], contryCode: _number[0],
            mobile1:item.mobile1  })
        })
        this.keyHolderList = _arr;
      }
    })
  }
  onSubmit(): void {
    this.requestGuardForm.get("contactNumberCountryCode").setValue("+27");
    this.requestGuardForm.get('shiftData').setValue(this._tempFormArrayValue);
    this.requestGuardForm.get('requestGuardShiftPost').setValue(this.shiftFormData);
    let obj =  this.requestGuardForm.getRawValue();
    if(obj.contactNumber &&obj.contactNumber.includes("+") ){
      let _number = obj.contactNumber.split(" ");
      obj.contactNumber = _number[1];
    }

    if (this.requestGuardForm.invalid) return;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RISK_WATCH_GUARD, obj)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialogClose(false);
        this.requestGuardForm.reset();
      }
    })
  }

  dialogClose(event): void {
    this.ref.close(event);
  }


  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
