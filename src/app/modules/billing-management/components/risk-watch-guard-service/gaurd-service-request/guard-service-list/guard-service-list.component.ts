import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CommonPaginationConfig, CrudService, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, removeProperyFromAnObjectWithDeepCopy, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { CancelGuardRequestSComponent } from '../cancel-guard-request/cancel-guard-request.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { map } from 'rxjs/operators';
interface IHistory {
  sNo: number;
  riskWatchRequestGuardsShiftId: string;
  createdDate: Date;
  requestStatus: string;
  shiftStartTime: string;
  shiftEndTime: string;
  customerName: string;
  guardRequestTypeNames: string;
  servicesFee: number;
  divisionName: string;
  formatedAddress: string;
  keyHolderName: string;
  keyHolderContactNo: string;
  mobileNo: string;
  permanentNo: string;
  guardingComments: string;
  eta?: string;
  processedOn?: string;
  processedBy: string;
  processedRefNo: string;
  confirmedBy?: any;
  confirmedOn?: any;
  confirmedOB?: any;
  cancelledBy: string;
  cancelledOn?: string;
  cancelledReason: string;
  cssClass: string;
}

@Component({
  selector: 'app-guard-service-list',
  templateUrl: './guard-service-list.component.html',
  styles: [`
  .cus-select{
    width: 45% !important;
}
.custom-icon-service-guard .fidelity-btn-circle {
    width: 30px !important;
    height: 30px !important;
    margin-top: -13px;
}
`],
})
export class GuardServiceListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @Input() selectedAddress: any
  @Input() customerId: any
  @Input() addressStatus: any
  @Input() submitted: any
  observableResponse;
  status: any = [];
  dataList: IHistory[];
  selectedRows: IHistory[] = [];
  statusConfirm = false
  deletConfirm = false;
  showDialogSpinner = false
  selectedFilterDate: any;
  row: any
  searchColumns: any
  customerObject: any
  requestTypeClass = { 'Paid': 'label-color-orange', 'Service': 'status-label-blue' }
  riskWatchRequestId = "";
  primengTableConfigProperties: any = {
    tableCaption: "Service",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Service History',
          dataKey: 'sNo',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: false,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [{ field: 'sNo', header: 'Record', width: '100px' },
          { field: 'requestStatus', header: 'Request Status', width: '150px', statusKey: 'cssClass'  },
          { field: 'shiftStartTime', header: 'Shift Start Time', width: '150px' , isDateTime:true},
          { field: 'siteId', header: 'Site ID', width: '150px' },
          { field: 'customerName', header: 'Logged By', width: '150px' },
          { field: 'guardRequestTypeNames', header: 'Request Type', width: '150px' },
          { field: 'servicesFee', header: 'Services Fees', width: '150px' },
          { field: 'divisionName', header: 'Division Name', width: '150px' },
          { field: 'formatedAddress', header: 'Address', width: '150px' },
          { field: 'KeyHolderName', header: 'Contact Name', width: '150px' },
          { field: 'keyHolderContactNo', header: 'Contact Number', width: '150px' },
          { field: 'mobileNo', header: 'Cell Number ', width: '150px' },
          { field: 'permanentNo', header: 'Prem Number', width: '150px' },
          { field: 'guardingComments', header: 'Service Request Note', width: '150px' },
          { field: 'eta', header: 'ETA', width: '100px' },
          { field: 'processedOn', header: 'Processed Time', width: '150px' ,isDateTime:true},
          { field: 'processedBy', header: 'Processed By', width: '150px' },
          { field: 'processedRefNo', header: 'Processed Ref No', width: '150px' },
          { field: 'confirmedBy', header: 'Confirmed By', width: '150px' },
          { field: 'confirmedOn', header: 'Confirmed Time', width: '150px',isDateTime:true },
          { field: 'confirmedOB', header: 'Confirmed OB', width: '150px' },
          { field: 'cancelledBy', header: 'Cancelled By', width: '150px' },
          { field: 'cancelledOn', header: 'Cancelled Time', width: '150px', isDateTime:true },
          { field: 'cancelledReason', header: 'Cancelled Reason', width: '150px' },
          ],
          apiSuffixModel: "",
          moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          enableMultiDeleteActionBtn: false,
          ebableAddActionBtn: true
        },
      ]
    }
  }

  constructor(
    private rxjsService: RxjsService,private store: Store<AppState>,public dialogService: DialogService, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,private datePipe: DatePipe,private momentService: MomentService,private crudService: CrudService) {
      super();
    this.activatedRoute.queryParams.subscribe(param => {
      this.riskWatchRequestId = param['riskWatchRequestId'];
      if (this.riskWatchRequestId) {
        this.getRetainerDetails();
      }
    })

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
  }
  ngOnChanges(changes: SimpleChanges) {
    if ((changes.customerId && changes.selectedAddress)) {
      this.riskWatchRequestId = this.activatedRoute.snapshot.queryParams.riskWatchRequestId
      this.selectedAddress = changes.selectedAddress.currentValue
      this.addressStatus = changes.addressStatus.currentValue;
      this.customerObject = { customerId: this.customerId, siteAddressId: this.selectedAddress.addressId };
      if (this.customerObject.customerId && this.customerObject.siteAddressId) {
        this.getRetainerDetails(null, null, this.customerObject);
      }
    }
    if (changes.submitted) {
      this.getRetainerDetails(null, null, this.customerObject);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)
    ]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRetainerDetails(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    if (this.riskWatchRequestId) {
      otherParams = { ...otherParams, riskWatchRequestId: this.riskWatchRequestId }
      this.crudService.get(
        ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.RISK_WATCH_GUARD_REQUEST_HISTORY,undefined,false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
    .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.observableResponse = response.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = response.totalCount;
          this.loading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    let otherParams = {};
    switch (type) {
      case CrudType.GET:
        this.searchColumns  =row['searchColumns']
        if (this.searchColumns && Object.keys(this.searchColumns).length > 0) {
          Object.keys(this.searchColumns).forEach((key) => {
            if (key.toLowerCase().includes('date')|| key.toLowerCase().includes('time')) {
              otherParams[key] = this.momentService.localToUTCDateTime(row['searchColumns'][key]);
            } else {
              otherParams[key] = row['searchColumns'][key];
            }
          });
        }
        let pageIndex, maximumRows;
        if (!row['maximumRows']) {
          maximumRows = CommonPaginationConfig.defaultPageSize;
          pageIndex = CommonPaginationConfig.defaultPageIndex;
        }
        else {
          maximumRows = row["maximumRows"];
          pageIndex = row["pageIndex"];
        }
        delete row['maximumRows'] && row['maximumRows'];
        delete row['pageIndex'] && row['pageIndex'];
        delete row['searchColumns'];
        this.getRetainerDetails(pageIndex, maximumRows, { ...otherParams, ...row });
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openCancelShiftsDialog() {
    if (this.dataList.length == 0 || this.selectedRows.length == 0){
      this.snackbarService.openSnackbar("Please Select any One", ResponseMessageTypes.WARNING);
      return;
    }

    let customText = "Are you sure you would like to cancel the selected shifts for this customer?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        const ref = this.dialogService.open(CancelGuardRequestSComponent, {
          header: `Cancel Guard Request`,
          showHeader: true,
          baseZIndex: 10000,
          width: "950px",
          data: { selectedRows: this.selectedRows },
        });
        ref.onClose.subscribe((resp) => {
          if (resp === false) {
            let _text = "Selected shifts cancelled successfully"
            this.openCustomDialog(_text, "Notification", false)
            this.getRetainerDetails(null, null, this.customerObject);
            this.selectedRows = []
          }
        });
      }
    });
  }


  openCustomDialog(customText, header, boolean) {
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: header,
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText, isShowNo: boolean },
    });
    confirm.onClose.subscribe((resp) => {
      if (!resp) {

      }
    });
  }

  exportExcel() {
    if (this.dataList.length == 0) return '';

    let data: IHistory[] = (this.selectedRows.length > 0 ? this.selectedRows : this.dataList);
    let dataList = [];
    data.forEach((item) => {
      let copyItems = removeProperyFromAnObjectWithDeepCopy(item, ["riskWatchRequestGuardsShiftId", "cssClass"]);
      if (copyItems.processedOn) {
        copyItems.processedOn = this.datePipe.transform(copyItems.processedOn, 'yyyy-MM-dd hh:mm a');
      }
      if (copyItems.cancelledOn) {
        copyItems.cancelledOn = this.datePipe.transform(copyItems.cancelledOn, 'yyyy-MM-dd hh:mm a');
      }
      if (copyItems.eta) {
        copyItems.eta = this.datePipe.transform(copyItems.eta, 'yyyy-MM-dd hh:mm a');
      }
      dataList.push({
        ...copyItems,
        createdDate: this.datePipe.transform(copyItems.createdDate, 'yyyy-MM-dd hh:mm a'),
        shiftStartTime: this.datePipe.transform(copyItems.shiftStartTime, 'yyyy-MM-dd hh:mm a'),
        shiftEndTime: this.datePipe.transform(copyItems.shiftEndTime, 'yyyy-MM-dd hh:mm a'),
      })
    })

    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((dataList));
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
