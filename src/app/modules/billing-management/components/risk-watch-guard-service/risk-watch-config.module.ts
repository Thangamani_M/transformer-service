import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PanelModule } from 'primeng/panel';
import { AddSubscriptionComponent } from './add-service/add-subscription/add-risk-watch.component';
import { CancelSubscriptionComponent } from './add-service/cancel-subscription/cancel-risk-watch.component';
import { GuardServiceComponent } from './add-service/guard-service.component';
import { ServiceListComponent } from './add-service/service-list/service-list.component';
import { AddGuardServiceComponent } from './gaurd-service-request/add-guard-service/add-guard-service.component';
import { CancelGuardRequestSComponent } from './gaurd-service-request/cancel-guard-request/cancel-guard-request.component';
import { GuardServiceListComponent } from './gaurd-service-request/guard-service-list/guard-service-list.component';
import { RiskWatchGuardServiceRoutingModule } from './risk-watch-config-routing.module';

@NgModule({
  declarations: [GuardServiceComponent, ServiceListComponent, AddSubscriptionComponent,
    GuardServiceListComponent, AddGuardServiceComponent, CancelSubscriptionComponent, CancelGuardRequestSComponent],
  imports: [
    CommonModule,
    RiskWatchGuardServiceRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    PanelModule
  ],
  exports: [ServiceListComponent, AddSubscriptionComponent, GuardServiceListComponent, AddGuardServiceComponent,
    CancelSubscriptionComponent, CancelGuardRequestSComponent],
  entryComponents: [ServiceListComponent, AddSubscriptionComponent, GuardServiceListComponent, AddGuardServiceComponent,
    CancelSubscriptionComponent, CancelGuardRequestSComponent]
})
export class RiskWatchGuardServiceModule { }
