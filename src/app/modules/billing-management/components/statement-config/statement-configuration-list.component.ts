import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { TransactionTypeAddEditModel } from '@modules/others/configuration/models/stock-id.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { StatementFeeEditComponent } from './statement-fee-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-statement-description-list',
  templateUrl: './statement-configuration-list.component.html'
})
export class StatementConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {
  userData: UserLogin;
  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private dialog: MatDialog,
    private snackbarService: SnackbarService,
    private router: Router,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Statement Fee",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Statement Fee', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Statement Fee',
            dataKey: 'statementFeeConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [
              { field: 'statementFee', header: 'Statement Fee', width: '200px' },
              { field: 'createdDate', header: 'Created Date', width: '150px' , isDate:true},
              { field: 'isActive', header: 'Status', width: '150px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STATEMENT_CONFIG,
            moduleName: ModulesBasedApiSuffix.BILLING,
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredList();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.STATEMENT_FEE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(ModulesBasedApiSuffix.BILLING,billingModuleApiSuffixModels,undefined,false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            let editableObject = new TransactionTypeAddEditModel();
            const dialogReff = this.dialog.open(StatementFeeEditComponent, { width: '400px', data: editableObject, disableClose: true });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredList();
              }
            })
            break;
          case 1:
            this.router.navigate(["billing/transaction-configuration/transaction-description/add-edit"]);
            break;
          case 2:
            this.router.navigate(["billing/transaction-configuration/transaction-subtype/add-edit"]);
            break;
        }
        break;
      case CrudType.GET:
        this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["billing/transaction-configuration/transaction-type/add-edit"], { queryParams: { id: row['invoiceTransactionTypeId'] } });
            break;
          case 1:
            this.router.navigate(["billing/transaction-configuration/transaction-type/add-edit"], { queryParams: { id: row['invoiceTransactionTypeId'] } });
            break;
        }

        break;
      case CrudType.VIEW:
        row['canEdit'] = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit
        switch (this.selectedTabIndex) {
          case 0:
            const dialogReff = this.dialog.open(StatementFeeEditComponent, { width: '400px', data: row, disableClose: true });
            dialogReff.afterClosed().subscribe(result => {
            });

            dialogReff.componentInstance.outputData.subscribe(ele => {

              if (ele) {
                this.getRequiredList();
              }
            })
            break;
          case 1:
            this.router.navigate(["billing/transaction-configuration/view/transaction-description"], { queryParams: { id: row['invoiceTransactionTypeId'] } });
            break;
          case 2:
            this.router.navigate(["billing/transaction-configuration/transaction-subtype/add-edit"], { queryParams: { id: row['invoiceTransactionDescriptionId'] } });
            break;
        }

        break;
      case CrudType.FILTER:

        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[unknownVar].isActive = this.dataList[unknownVar].isActive ? false : true;
            }
          });
        break;
      default:
        break;
    }
  }
}
