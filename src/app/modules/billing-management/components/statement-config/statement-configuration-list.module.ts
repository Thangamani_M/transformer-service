import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StatementConfigRoutingModule } from './statement-configuration-list-routing.module';
import { StatementConfigurationListComponent } from './statement-configuration-list.component';
import { StatementFeeEditComponent } from './statement-fee-add-edit.component';


@NgModule({
  declarations: [StatementConfigurationListComponent,StatementFeeEditComponent],
  imports: [
    CommonModule,
    StatementConfigRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ],
  entryComponents:[StatementFeeEditComponent],
  providers: [DatePipe]
})
export class StatementConfigurationModule { }
