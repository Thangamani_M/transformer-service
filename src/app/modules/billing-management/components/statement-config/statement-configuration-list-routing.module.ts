import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatementConfigurationListComponent } from './statement-configuration-list.component';
import { StatementFeeEditComponent } from './statement-fee-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [

  { path: '', redirectTo: 'list',canActivate:[AuthGuard], pathMatch: 'full' },
  { path: 'add-edit', component: StatementFeeEditComponent, canActivate:[AuthGuard],data: { title: 'Annual Network Fee Add Edit' } },
  { path: 'list', component: StatementConfigurationListComponent,canActivate:[AuthGuard], data: { title: 'Statement Fee Configuration List' } },


];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class StatementConfigRoutingModule { }
