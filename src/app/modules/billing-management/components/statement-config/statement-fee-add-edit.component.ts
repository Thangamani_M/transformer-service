
import { Component, EventEmitter, Inject, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatOption, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';


@Component({
  selector: 'app-statement-fee-add-edit',
  templateUrl: './statement-fee-add-edit.component.html'
})
export class StatementFeeEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  id: any
  divisionDropDown: any = [];
  statementAddEditForm: FormGroup;
  descriptions: FormArray;
  descriptionDocuments: FormArray;
  rOCommunityPatrolTypesList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  stockIdDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });

  isDisabledEdit = true;
  @ViewChildren('input') rows: QueryList<any>;

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  transationTypes: any;
  details: any;
  loggedInUserData: LoggedInUserModel;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialog: MatDialog,
    private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService,
    private snackbarService  : SnackbarService) {
    this.id = this.data.statementFeeConfigId;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createStatementAddEditForm();
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.id) {
      this.getDetailsById();
    }
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STATEMENT_CONFIG, this.id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.details = response.resources;
          this.statementAddEditForm.patchValue(this.details);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createStatementAddEditForm(): void {
    let statementAddEditModel = {
      createdUserId: "",
      statementFee: "",
      statementFeeConfigId: ""
    }
    this.statementAddEditForm = this.formBuilder.group({

    });
    Object.keys(statementAddEditModel).forEach((key) => {
      this.statementAddEditForm.addControl(key, new FormControl(statementAddEditModel[key]));
    });
    this.statementAddEditForm = setRequiredValidator(this.statementAddEditForm, ["statementFee"]);
    this.statementAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
    if(this.id){
      this.statementAddEditForm.get('statementFee').disable()
    }else{
      this.isDisabledEdit = false
    }
  }
  //Get Details
  getStatmentFeeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STATEMENT_CONFIG,
      this.id
    );
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  enableField(){
    if (!this.data.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isDisabledEdit = false
    this.statementAddEditForm.get('statementFee').enable()
  }


  onSubmit(): void {

    if (this.statementAddEditForm.invalid) {
      return;
    }



    let formValue = this.statementAddEditForm.value;
    formValue.modifiedUserId = this.loggedUser.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.id ? this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STATEMENT_CONFIG, formValue) : this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STATEMENT_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.outputData.emit(true);
        this.dialog.closeAll();

      }
    })
  }

}
