import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-same-day-run-report-view',
  templateUrl: './same-day-run-report-view.component.html'
})
export class SameDayRunReportViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  SameDayReportViewDetail: any;
  row: any = {}
  observableResponse: any;
  loggedUser: any;
  chargeListId: any;
  chargeListDetails: any;
  constructor(private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Same Day Test Run",
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '/billing' }, { displayName: 'Same Day Test Run', relativeRouterUrl: '/billing/same-day-run-report' }, { displayName: 'View Same Day Test Run', relativeRouterUrl: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Same Day Test Run',
            dataKey: 'chargeListId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'debtorCode', header: 'Debtor Code', width: '200px' },
              { field: 'bdiNumber', header: 'BDI Number', width: '200px' },
              { field: 'bank', header: 'Bank', width: '200px' },
              { field: 'bankBranch', header: 'Bank Branch', width: '200px' },
              { field: 'accountNo', header: 'Bank Account No', width: '200px' },
              { field: 'transactionRefNo', header: 'Transaction No', width: '200px' },
              { field: 'transactionDate', header: 'Transaction Date', width: '200px' },
              { field: 'doRunCode', header: 'DoRun Code', width: '200px' },
              { field: 'division', header: 'Division', width: '200px' },
              { field: 'total', header: 'Master Total', width: '200px' },
              { field: 'crmStatus', header: 'CRM Status', width: '200px' },
              { field: 'errorMessage', header: 'Error Message', width: '200px' },
              { field: 'mercantileRejectionCode', header: 'Mercantile Rejection Code', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enablePrintBtn: false,
            printTitle: 'Pro Forma Invoice',
            printSection: 'print-section0',
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          },

        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.chargeListId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';

    });
  }
  ngOnInit(): void {
    this.getRequiredDetailData();
    this.rxjsService.setGlobalLoaderProperty(true);
  }

  // paramss: any;
  getRequiredDetailData() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CHARGE_LIST_SAME_DAY_RUN_REPORT, this.chargeListId, false)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode = 200 && response.resources) {
          this.chargeListDetails = response.resources;
          this.SameDayReportViewDetail = [
            { name: 'Batch Number', value: response.resources?.batchNumber },
            { name: 'File Send to Bank',  value: response.resources?.fileSentToBank },
            { name: 'D/O Filename', value: response.resources?.doFileName },
            { name: 'Run Date', value: response.resources?.runDate, 'dd-MM-yyyy': '' },
            { name: 'D/0 Date', value: response.resources?.doDate, 'dd-MM-yyyy': '' },
            { name: 'Total Transactions', value: response.resources?.totalTransactions },
            { name: 'For Debit Order Overrides', value: response.resources?.doTotal },
            { name: 'Run Type', value: response.resources?.runType },
            { name: 'Status', value: response.resources?.status },
            { name: 'Run by Name', value: response.resources?.runBy },
            { name: 'Posted by Name', value: response.resources?.postedBy },
            { name: 'Posted Date', value: response.resources?.postDate, 'dd-MM-yyyy': '' },
          ]
          this.observableResponse = this.chargeListDetails.reportDetail;
          this.dataList = this.observableResponse;
          this.totalRecords = this.observableResponse.length;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredDetailData()
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}