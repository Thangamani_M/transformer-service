import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { SameDayRunReportAddModel } from "@modules/billing-management/models/same-day-test-run.models";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales/shared";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";
import moment from "moment";
import { forkJoin, Observable } from "rxjs";

@Component({
    selector: 'app-same-day-run-report-add',
    templateUrl: './same-day-run-report-add.component.html',
    styleUrls: ['./same-day-run-report-add.component.scss']
  })
export class SameDayRunReportAddComponent {
    monthlyTestRunId: any;
    todayDate: Date = new Date();
    sameDayTestRunForm: FormGroup;
    divisionList: any = [];
    monthlytestrun: any = [];
    selectedOptions: any = [];
    runCodeList: any = [];
    errorMessage: string;
    userData: UserLogin;
    primengTableConfigProperties:any
    constructor(private momentService: MomentService,private router: Router, private store: Store<AppState>, private crudService: CrudService,
         private formBuilder: FormBuilder, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
        })
        this.primengTableConfigProperties = {
          tableCaption: `Add Same Day Test Run`,
          selectedTabIndex: 0,
          breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },{ displayName: 'Same Day Test Run List' ,relativeRouterUrl: '/billing/same-day-run-report'},{ displayName: `Add Same Day Test Run`,}],
          tableComponentConfigs: {
            tabsList: [
              {
                caption: 'Same Day Test Run',
                dataKey: 'riskWatchConfigId',
                enableBreadCrumb: true,
              }]
            }
          }
      }

    ngOnInit(): void {
        this.createSameDayRunTestRunForm();
        this.getDebitorCodeDropdown();
        this.loadAllDropdown().subscribe((response: IApplicationResponse[]) => {
          if (!response[0].isSuccess &&   response[0].resources) return;
            this.divisionList = getPDropdownData(response[0].resources);
            this.rxjsService.setGlobalLoaderProperty(false);
        });
        this.rxjsService.setGlobalLoaderProperty(false);
    }
    createSameDayRunTestRunForm(): void {
        let recurringBilling = new SameDayRunReportAddModel();
        this.sameDayTestRunForm = this.formBuilder.group({

        });
        Object.keys(recurringBilling).forEach((key) => {
        this.sameDayTestRunForm.addControl(key, new FormControl(recurringBilling[key]));
        });
        this.sameDayTestRunForm = setRequiredValidator(this.sameDayTestRunForm, ["divisionIds", "runTime"]);
        if (!this.sameDayTestRunForm) {
        this.rxjsService.setGlobalLoaderProperty(false);
        }

    }

    loadAllDropdown(): Observable<any> {
        return forkJoin(
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true),
        )
    }
    getDebitorCodeDropdown() {
        this.crudService.dropdown(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.UX_DEBIT_ORDER_RUN_CODES,
        undefined
        ).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
            this.runCodeList = data.resources;
        }

        });
    }
    onSubmit(): void {
        this.monthlytestrun = []
        if (this.sameDayTestRunForm.invalid) {
        this.sameDayTestRunForm.controls.divisionIds.markAllAsTouched();
        return;

        }
        let debitOrderRunCodeIds=[];
        if( this.sameDayTestRunForm.value.debitOrderRunCodeIds &&  this.sameDayTestRunForm.value.debitOrderRunCodeIds.length>0){
        this.sameDayTestRunForm.value.debitOrderRunCodeIds.forEach(element => {
            debitOrderRunCodeIds.push(element.id);
        });
        }
        let finalObject;
        let debitOrderRunCodeId = [];
        let formValue = this.sameDayTestRunForm.value;
        if(formValue.debitOrderRunCodeIds &&  formValue.debitOrderRunCodeIds.length) {
            formValue.debitOrderRunCodeIds.forEach(element => {
                debitOrderRunCodeId.push(element.id);
            });
        }
        formValue.runTime = formValue.runTime ? this.momentService.toMoment(formValue.runTime).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
        finalObject= {
            divisionIds: formValue.divisionIds,
            testRunDate:  formValue.runTime,
            debitOrderRunCodeIds: debitOrderRunCodeId,
            includeERB: formValue.includeERB,
            isFinalSubmit:  formValue.isFinalSubmit,
            createdUserId: this.userData.userId
          }

        this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.SAMEDAY_TEST_RUN, finalObject)
            .subscribe({
            next: response => {
                if (response.isSuccess) {
                this.router.navigate(['/billing/same-day-run-report'], { queryParams: { tab: 2 }, skipLocationChange: true });
                } else {
                }
            },
            error: err => this.errorMessage = err
        });


    }

}
