import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SameDayRunReportAddComponent } from './same-day-run-report-add.component';
import { SameDayRunReportListComponent } from './same-day-run-report-list.component';
import { SameDayRunReportRoutingModule } from './same-day-run-report-routing.module';
import { SameDayRunReportViewComponent } from './same-day-run-report-view.component';



@NgModule({
  declarations: [SameDayRunReportViewComponent,SameDayRunReportListComponent,SameDayRunReportAddComponent],
  imports: [
    CommonModule,
    SameDayRunReportRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ]
})
export class SameDayRunReportModule { }
