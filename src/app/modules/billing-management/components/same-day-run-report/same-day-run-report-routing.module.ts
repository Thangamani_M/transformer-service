import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SameDayRunReportAddComponent } from './same-day-run-report-add.component';
import { SameDayRunReportListComponent } from './same-day-run-report-list.component';
import { SameDayRunReportViewComponent } from './same-day-run-report-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: SameDayRunReportListComponent, canActivate: [AuthGuard], data: { title: 'Same Day Run Report list' } },
  { path: 'view', component: SameDayRunReportViewComponent, canActivate: [AuthGuard], data: { title: 'Same Day Run Report View' } },
  { path: 'add', component: SameDayRunReportAddComponent, canActivate: [AuthGuard], data: { title: 'Same Day Run Report Add' } }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class SameDayRunReportRoutingModule { }
