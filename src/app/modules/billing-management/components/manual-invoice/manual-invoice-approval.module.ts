import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ManualInvoiceApproveDialogModule } from '@modules/customer/components/customer/customer-management/manual-invoice/manual-invoice-approve-dialog/manual-invoice-approve-dialog.module';
import { ManualInvoiceDeclineDialogModule } from '@modules/customer/components/customer/customer-management/manual-invoice/manual-invoice-decline-dialog/manual-invoice-decline-dialog.module';
import { ManualInvoiceApprovalRoutingModule } from './manual-invoice-approval-routing.module';
import { ManualInvoiceApprovalComponent } from './manual-invoice-approval.component';



@NgModule({
  declarations: [ManualInvoiceApprovalComponent],
  imports: [
    CommonModule,
    ManualInvoiceApprovalRoutingModule,
    CommonModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    ManualInvoiceApproveDialogModule,
    ManualInvoiceDeclineDialogModule,
  ]
})
export class ManualInvoiceApprovalModule { }
