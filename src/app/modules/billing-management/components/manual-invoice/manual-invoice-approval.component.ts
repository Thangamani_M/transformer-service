import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService,  IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams,  RxjsService } from '@app/shared';
import { ManualInvoiceApproveDialogComponent } from '@modules/customer/components/customer/customer-management/manual-invoice/manual-invoice-approve-dialog/manual-invoice-approve-dialog.component';
import { ManualInvoiceDeclineDialogComponent } from '@modules/customer/components/customer/customer-management/manual-invoice/manual-invoice-decline-dialog/manual-invoice-decline-dialog.component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
declare var $;
@Component({
  selector: 'app-manual-invoice-approval',
  templateUrl: './manual-invoice-approval.component.html',
  styleUrls: ['./manual-invoice.component.scss'],
})

export class ManualInvoiceApprovalComponent {
  @ViewChild('approve_modal', { static: false }) approve_modal: ElementRef;
  public formData = new FormData();
  approveInvoiceForm: FormGroup;
  declineInvoiceForm: FormGroup;
  manualInvoicItems = [];
  manualInvoiceDocuments = [];
  loggedUser: any;
  escalationId: any;
  isSendCustomerCopy = false;
  comments: any;
  listOfFiles: any[] = [];
  fileList: File[] = [];
  listOfFiles1: any[] = [];
  fileList1: File[] = [];
  documentdetails: Array<any> = [];
  reasonList = [];
  docName: any;
  documentDetails: any;
  maxFilesUpload: Number = 5;
  manualInvoiceApprovalId: any;
  totalCountObj = {};
  totalExcAmount = 0;
  totalVatAmount = 0;
  totalIncAmount = 0;
  createdUserId: any;
  primengTableConfigProperties: any = {
    tableCaption: "Transaction Process",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Transaction ', relativeRouterUrl: '' }, {
      displayName: 'Manual Invoice - Approve'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Transaction Process',
          dataKey: 'Approve',
          enableBreadCrumb: true,
          enableAction: true,
        }]
    }
  }
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  constructor(private crudService: CrudService, private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private dialogService: DialogService,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(params => {
      this.escalationId = params.escalationId;
      this.createdUserId = params.createdUserId
    });
  }

  ngOnInit() {
    this.getManualInvoiceDetails();
    this.getReason();
  }

  getManualInvoiceDetails() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_APPROVAL_DETAILS,
      prepareGetRequestHttpParams(null, null, {
        manualInvoiceApprovalId: this.escalationId
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.manualInvoicItems = response.resources.manualInvoicItems;
          this.manualInvoicItems.forEach(element => {
            this.totalExcAmount += element.itemPrice;
            this.totalVatAmount += element.vatAmount;
            this.totalIncAmount += element.totalPrice;
          });
          this.manualInvoiceDocuments = response.resources.manualInvoiceDocuments;
          this.isSendCustomerCopy = response.resources.isSendCustomerCopy;
          this.comments = response.resources.comments;
          this.manualInvoiceApprovalId = response.resources.manualInvoiceApprovalId;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON,
      prepareGetRequestHttpParams(null, null, {
        manualInvoiceApprovalId: this.escalationId
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openApproveDialog() {
    const ref = this.dialogService.open(ManualInvoiceApproveDialogComponent, {
      header: "Approve Transaction",
      baseZIndex: 1000,
      width: '550px',
      closable: false,
      showHeader: false,
      data: {
        manualInvoiceApprovalId: this.manualInvoiceApprovalId,
        createdUserId: this.createdUserId,
      },
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.router.navigate(['billing/task-list']);
      }
    });
  }

  openDeclineDialog() {
    const ref = this.dialogService.open(ManualInvoiceDeclineDialogComponent, {
      header: "Decline Transaction",
      baseZIndex: 1000,
      width: '550px',
      closable: false,
      showHeader: false,
      data: {
        reasonList: this.reasonList,
        manualInvoiceApprovalId: this.manualInvoiceApprovalId,
        createdUserId: this.createdUserId,
      },
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.router.navigate(['billing/task-list']);
      }
    });
  }

  openLinkInNewTab(link?) {
    if (link) {
      window.open(link, '_blank');
    }
  }
}
