import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManualInvoiceApprovalComponent } from './manual-invoice-approval.component';

const routes: Routes = [
  { path: '', component: ManualInvoiceApprovalComponent, data: { title: 'Stock ID list' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ManualInvoiceApprovalRoutingModule { }
