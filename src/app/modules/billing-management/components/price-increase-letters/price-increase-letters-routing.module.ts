import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PriceIncreaseLettersDetailComponent } from './price-increase-letters-detail.component';
import { PriceIncreaseLettersComponent } from './price-increase-letters.component'
const routes: Routes = [
  { path: '', component: PriceIncreaseLettersComponent, data: { title: 'Price Increase Letters' } },
  { path: 'detail', component: PriceIncreaseLettersDetailComponent, data: { title: 'Detail - Price Increase Letters' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class PriceIncreaseLetterRoutingModule { }
