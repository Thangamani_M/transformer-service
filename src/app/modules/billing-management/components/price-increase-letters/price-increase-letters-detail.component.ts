import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-price-increase-letters-detail',
  templateUrl: './price-increase-letters-detail.component.html',
})
export class PriceIncreaseLettersDetailComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties = {
    tableCaption: "Price Increase Letter",
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },
    { displayName: 'Price Increase Letter', relativeRouterUrl: '' },
    { displayName: 'Details - Price Increase Letter', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Details - Price Increase Letter',
          dataKey: 'pricingFileNameId',
          enableBreadCrumb: true,
          enableAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableExportBtn: true,
          enableAddActionBtn: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableStatusActiveAction: true,
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          shouldShowFilterActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          columns: [{ field: 'bdiNumber', header: 'BDI', width: '150px' },
          { field: 'divisionName', header: 'Division', width: '150px' },
          { field: 'debtorRefNo', header: 'Debtor Code', width: '150px' },
          { field: 'titleName', header: 'Contact Title', width: '150px' },
          { field: 'firstName', header: 'Contact First Name', width: '170px' },
          { field: 'lastName', header: 'Contact Surname', width: '170px' },
          { field: 'address', header: 'Address', width: '250px' },
          { field: 'currentContractValuePM', header: 'Current Price', width: '150px' },
          { field: 'newContractValuePM', header: 'Increase Price', width: '150px' },
          { field: 'effectiveDate', header: 'Effective Date', isDate: true, width: '150px' }
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.PRICE_INCREASE_LETTER_DETAILS,
          moduleName: ModulesBasedApiSuffix.BILLING,
          downloadApiSuffixModel: BillingModuleApiSuffixModels.PRICE_INCREASE_LETTER_DOWNLOAD,
        }
      ]
    }
  }
  pricingFileNameId = ""
  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute
  ) {
    super()
    this.activatedRoute.queryParams.subscribe(param => {
      this.pricingFileNameId = param['id']
    })
  }

  ngOnInit(): void {
    this.getRequiredList();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, pricingFileNameId: this.pricingFileNameId }
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EXPORT:
        this.exportToExcel();
        break;
      case CrudType.VIEW:
        if (row['documentPath']) {
          var link = document.createElement("a");
          if (link.download !== undefined) {
            link.setAttribute("href", row['documentPath']);
            link.setAttribute("download", row['documentPath']);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }
        }
        break;
      default:
        break;
    }
  }

  exportToExcel() {
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].downloadApiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.BILLING,
        billingModuleApiSuffixModels,
        undefined,
        false,
        prepareRequiredHttpParams({ pricingFileNameId: this.pricingFileNameId })
      )
      .subscribe((data) => {
        if (data.isSuccess && data.resources) {
          window.open(data?.resources)
        }
      });
  }
}
