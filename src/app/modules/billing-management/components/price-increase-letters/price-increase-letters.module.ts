import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PriceIncreaseLettersComponent } from './price-increase-letters.component';
import { PriceIncreaseLetterRoutingModule } from './price-increase-letters-routing.module';
import { LayoutModule } from '@angular/cdk/layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '@app/shared/material.module';
import { SharedModule } from '@app/shared';
import { PriceIncreaseLettersDetailComponent } from './price-increase-letters-detail.component';
@NgModule({
  declarations: [PriceIncreaseLettersComponent,PriceIncreaseLettersDetailComponent],
  imports: [
    CommonModule,
    CommonModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    PriceIncreaseLetterRoutingModule,
    FormsModule,
    MaterialModule
  ],
})

export class PriceIncreaseLettersModule { }
