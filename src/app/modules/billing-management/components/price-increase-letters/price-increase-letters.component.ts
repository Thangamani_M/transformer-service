import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrudService, CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-price-increase-letters',
  templateUrl: './price-increase-letters.component.html',
})
export class PriceIncreaseLettersComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private router: Router,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Price Increase Letter",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },
      { displayName: 'Price Increase Letter', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Price Increase Letter',
            dataKey: 'pricingIncreaseRunDateConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: false,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'pricingFileName', header: 'Report Name' },
            { field: 'runDate', header: 'Run Date', isDate: true },
            { field: 'status', header: 'Status', type: 'dropdown', options: [{ label: 'Inprogress', value: 'Inprogress' }, { label: 'Completed', value: 'Completed' }] }],
            apiSuffixModel: BillingModuleApiSuffixModels.PRICE_INCREASE_LETTERS,
            moduleName: ModulesBasedApiSuffix.BILLING,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getRequiredList();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.router.navigate(['billing/price-increase-letters/detail'], { queryParams: { id: row['pricingFileNameId'] } });
        break;
      default:
        break;
    }
  }
}
