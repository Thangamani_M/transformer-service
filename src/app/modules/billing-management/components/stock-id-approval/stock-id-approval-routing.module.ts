import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockIdApprovalListComponent, StockIdApprovalViewComponent, StockIdApprovalUpdateComponent } from '.';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  // { path: '', redirectTo: 'list', canActivate: [AuthGuard], pathMatch: 'full' },
  { path: '', component: StockIdApprovalListComponent, canActivate: [AuthGuard], data: { title: 'Stock Id Approval List' } },
  { path: 'view', component: StockIdApprovalViewComponent, canActivate: [AuthGuard], data: { title: 'Stock Id Approval View' } },
  { path: 'edit', component: StockIdApprovalUpdateComponent, canActivate: [AuthGuard], data: { title: 'Stock Id Approval Update' } }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class BillingStockIdApprovalRoutingModule { }
