import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxPrintModule } from 'ngx-print';
import { RadioButtonModule } from 'primeng/radiobutton';
import { StockIdApprovalListComponent, StockIdApprovalUpdateComponent, StockIdApprovalViewComponent } from '.';
import { BillingStockIdApprovalRoutingModule } from './stock-id-approval-routing.module';

@NgModule({
  declarations: [
    StockIdApprovalListComponent,
    StockIdApprovalViewComponent,
    StockIdApprovalUpdateComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    SharedModule,
    NgxPrintModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    RadioButtonModule,
    BillingStockIdApprovalRoutingModule
  ],
  providers:[
      DatePipe
  ],
})
export class BillingStockIdApprovalModule { }
