
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { StockIdApprovalAddEditModel } from '@modules/others/configuration/models/stock-id-approval.model';
import { StockDescriptionPricesListModel, StockDescriptionPricesListModel1, StockDescriptionPricesListModel2, StockIdRequestAddEditModel, StockIdRequestDescriptionListModel } from '@modules/others/configuration/models/stock-id-request.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-stock-id-approval-update',
  templateUrl: './stock-id-approval-update.component.html'
})
export class StockIdApprovalUpdateComponent implements OnInit {
  stockId: any
  status: any = [];
  stockIdDropDown: any = [];
  pcDropDown: any = [];
  cnglDropDown: any = [];
  glCodeDropDown: any = [];
  divisionDropDown: any = [];
  districtDropDown: any = [];
  stockIdForm: FormGroup;
  stockIdRequestDescriptions: FormArray;
  stockIdRequestDescriptionsPrices: FormArray
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  stockIdDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  stokIdDropdown: any = []
  @ViewChildren('input') rows: QueryList<any>;

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  statusDropDown: any = []
  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.stockId = this.activatedRoute.snapshot.queryParams.stockId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.statusDropDown = [
      { id: 'Approved', displayName: 'Approved' },
      { id: 'declined', displayName: 'Declined' }
    ]
  }

  ngOnInit() {
    this.createstockIdForm();
    this.getStockIdDropdown()
    this.getPCDropdown()
    this.getGLCodeDropdown()
    this.getCNGLDropdown()
    this.getDivisionDropdown()
    this.getDistrictDropdown()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.stockId) {
      this.getStockIdDetailsById().subscribe((response: IApplicationResponse) => {

        this.rxjsService.setGlobalLoaderProperty(false);
        let stockIdRequestAddEditModel = new StockIdRequestAddEditModel(response.resources);
        this.stockIdDetails = response.resources;
        this.stockIdForm.patchValue(stockIdRequestAddEditModel);
        this.stockIdForm.get('stockDescriptionId').setValue(response.resources.stockDescriptionId)
        this.stockIdForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.stockIdForm.get('modifiedUserId').setValue(this.loggedUser.userId)

      })
    } else {

    }

  }

  createstockIdForm(): void {
    let stockIdApprovalAddEditModel = new StockIdApprovalAddEditModel();
    this.stockIdForm = this.formBuilder.group({
      // stockIdRequestDescriptions: this.formBuilder.array([])
    });
    Object.keys(stockIdApprovalAddEditModel).forEach((key) => {
      this.stockIdForm.addControl(key, new FormControl(stockIdApprovalAddEditModel[key]));
    });
    this.stockIdForm = setRequiredValidator(this.stockIdForm, ["stockId", "geoLocationCodeId", "profitCenterId", "creditNoteGeoLocationId", "stockDescriptionApprovalStatus"]);
    this.stockIdForm.get('actionedByName').setValue(this.loggedUser.displayName)
    this.stockIdForm.get('roleId').setValue(this.loggedUser.roleId)
    this.stockIdForm.get('approvedBy').setValue(this.loggedUser.userId)
    this.stockIdForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.stockIdForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.stockIdForm.get('stockDescriptionApprovalStatus').valueChanges.subscribe((outOfOfficeStatusId: any) => {
      if ((outOfOfficeStatusId == 'declined') && outOfOfficeStatusId) {
        this.stockIdForm = setRequiredValidator(this.stockIdForm, ['reason']);
        this.stockIdForm.get('reason').setValidators(Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(200)]))
      }
      else {
        this.stockIdForm = clearFormControlValidators(this.stockIdForm, ["reason"]);
        this.stockIdForm.get('reason').setValidators([Validators.minLength(3), Validators.maxLength(200)])
      }
      this.stockIdForm.get('reason').updateValueAndValidity()


    });
  }

  //Create FormArray controls
  createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel?: StockIdRequestDescriptionListModel): FormGroup {
    let StockIdRequestDescriptionListModelControl = new StockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionListModelControl).forEach((key) => {
      if (key === 'stockDescriptionPrices') {
        formControls[key] = this.formBuilder.array([])
      } else if (key === 'stockDescriptionId') {
        formControls[key] = [{ value: StockIdRequestDescriptionListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createStockIdRequestDescriptionPriceListModel(stockDescriptionPricesListModel?: StockDescriptionPricesListModel): FormGroup {
    let StockIdRequestDescriptionPriceListModelControl = new StockDescriptionPricesListModel(stockDescriptionPricesListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionPriceListModelControl).forEach((key) => {
      // if (key === 'tierDetails') {
      //   formControls[key] = this.formBuilder.array([])
      if (key === 'stockDescriptionPriceId') {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }


  //Create FormArray controls
  createStockIdRequestDescriptionPriceListModel1(stockDescriptionPricesListModel?: StockDescriptionPricesListModel1): FormGroup {
    let StockIdRequestDescriptionPriceListModelControl = new StockDescriptionPricesListModel1(stockDescriptionPricesListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionPriceListModelControl).forEach((key) => {
      // if (key === 'tierDetails') {
      //   formControls[key] = this.formBuilder.array([])
      if (key === 'stockDescriptionPriceId' || key === 'tierId') {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createStockIdRequestDescriptionPriceListModel2(stockDescriptionPricesListModel?: StockDescriptionPricesListModel2): FormGroup {
    let StockIdRequestDescriptionPriceListModelControl = new StockDescriptionPricesListModel2(stockDescriptionPricesListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionPriceListModelControl).forEach((key) => {
      // if (key === 'tierDetails') {
      //   formControls[key] = this.formBuilder.array([])
      if (key === 'stockDescriptionPriceId' || key === 'tierId') {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionPriceListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getstockIdRequestDescriptionsListArray(): FormArray {
    if (!this.stockIdForm) return;
    return this.stockIdForm.get("stockIdRequestDescriptions") as FormArray;
  }

  //Create FormArray
  getstockIdRequestDescriptionsPriceListArray(index: number): FormArray {
    if (!this.stockIdForm) return;
    return this.stockIdRequestDescriptions.at(index).get("stockDescriptionPrices") as FormArray

  }

  //Get Details
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_REQUEST,
      this.stockId
    );
  }

  getStockIdDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_STOCK_ID, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stokIdDropdown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getPCDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PC, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.pcDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCNGLDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CNGL_LOCATION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.cnglDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getGLCodeDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_GEO_LOCATION_CODE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.glCodeDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDistrictDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DISTRICTS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.districtDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSelectStockId(stockId: string) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID, stockId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stockIdDetails = response.resources;
          this.stockIdForm.get('isFixedFee').setValue(response.resources.isFixedFee)
          this.stockIdForm.get('isTierBasedPrice').setValue(response.resources.isTierBasedPrice)
          // this.stockIdRequestDescriptions.clear()
          this.stockIdRequestDescriptionsPrices.clear()

          if (!response.resources.isFixedFee && !response.resources.isTierBasedPrice) { //calculated,no

            this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
            this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel());

          } else if (response.resources.isFixedFee && !response.resources.isTierBasedPrice) { // fixed,no

            this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
            this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel1());

          } else if (response.resources.isFixedFee && response.resources.isTierBasedPrice) { // fixed,yes
            this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0)
            this.stockIdRequestDescriptionsPrices.push(this.createStockIdRequestDescriptionPriceListModel2());

          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Add Employee Details
  addDescription(): void {

    if (this.getstockIdRequestDescriptionsListArray.invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };

    if (!this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { //calculated,no
      this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray;
      let stockIdRequestDescriptionListModel = new StockIdRequestDescriptionListModel();
      this.stockIdRequestDescriptions.insert(0, this.createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel));

      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel(stockDescriptionPricesListModel));

    } else if (this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { // fixed,no
      this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray;
      let stockIdRequestDescriptionListModel = new StockIdRequestDescriptionListModel();
      this.stockIdRequestDescriptions.insert(0, this.createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel));

      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel1();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel1(stockDescriptionPricesListModel));

    } else if (this.stockIdForm.get('isFixedFee').value && this.stockIdForm.get('isTierBasedPrice').value) { // fixed,yes
      this.stockIdRequestDescriptions = this.getstockIdRequestDescriptionsListArray;
      let stockIdRequestDescriptionListModel = new StockIdRequestDescriptionListModel();
      this.stockIdRequestDescriptions.insert(0, this.createStockIdRequestDescriptionListModel(stockIdRequestDescriptionListModel));

      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(0);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel2();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel2(stockDescriptionPricesListModel));

    }

    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeDescription(i: number): void {
    if (!this.getstockIdRequestDescriptionsListArray.controls[i].value.stockDescriptionId) {
      this.getstockIdRequestDescriptionsListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getstockIdRequestDescriptionsListArray.controls[i].value.stockDescriptionNameId && this.getstockIdRequestDescriptionsListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SMS_GROUP_EMPLOYEE,
          this.getstockIdRequestDescriptionsListArray.controls[i].value.stockDescriptionNameId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getstockIdRequestDescriptionsListArray.removeAt(i);
              // this.isDuplicate = false;
              // this.isDuplicateNumber = false;
            }
            if (this.getstockIdRequestDescriptionsListArray.length === 0) {
              this.addDescription();
            };
          });
      }
      else {
        this.getstockIdRequestDescriptionsListArray.removeAt(i);
        // this.isDuplicate = false;
        // this.isDuplicateNumber = false;
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  //Add Employee Details
  addPrice(i, j): void {

    if (this.getstockIdRequestDescriptionsPriceListArray(i).invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };
    if (!this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { //calculated,no
      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(i);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel(stockDescriptionPricesListModel));

    } else if (this.stockIdForm.get('isFixedFee').value && !this.stockIdForm.get('isTierBasedPrice').value) { // fixed,no

      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(i);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel1();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel1(stockDescriptionPricesListModel));

    } else if (this.stockIdForm.get('isFixedFee').value && this.stockIdForm.get('isTierBasedPrice').value) { // fixed,yes
      this.stockIdRequestDescriptionsPrices = this.getstockIdRequestDescriptionsPriceListArray(i);
      let stockDescriptionPricesListModel = new StockDescriptionPricesListModel2();
      this.stockIdRequestDescriptionsPrices.insert(0, this.createStockIdRequestDescriptionPriceListModel2(stockDescriptionPricesListModel));

    }

    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removePrice(i, j): void {
    if (!this.getstockIdRequestDescriptionsPriceListArray(i).controls[j].value.stockDescriptionPriceId) {
      this.getstockIdRequestDescriptionsPriceListArray(i).removeAt(j);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getstockIdRequestDescriptionsListArray.controls[i].value.cmcsmsGroupEmployeeId && this.getstockIdRequestDescriptionsListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SMS_GROUP_EMPLOYEE,
          this.getstockIdRequestDescriptionsListArray.controls[i].value.cmcsmsGroupEmployeeId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getstockIdRequestDescriptionsListArray.removeAt(i);
              // this.isDuplicate = false;
              // this.isDuplicateNumber = false;
            }
            if (this.getstockIdRequestDescriptionsListArray.length === 0) {
              this.addDescription();
            };
          });
      }
      else {
        this.getstockIdRequestDescriptionsListArray.removeAt(i);
        // this.isDuplicate = false;
        // this.isDuplicateNumber = false;
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onSubmit(): void {
    if (this.stockIdForm.invalid) {
      return;
    }
    let formValue = this.stockIdForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_APPROVAL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/billing/stock-id-approval?tab=0');
      }
    })
  }

}

