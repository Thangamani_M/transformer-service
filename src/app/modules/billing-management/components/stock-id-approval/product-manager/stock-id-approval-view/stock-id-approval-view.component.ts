import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-stock-id-approval-view',
  templateUrl: './stock-id-approval-view.component.html'
})
export class StockIdApprovalViewComponent implements OnInit {
  stockId: string;
  stockIdDetails: any;
  loggedUser: any
  constructor(private rjxService: RxjsService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router) {
    this.stockId = this.activatedRoute.snapshot.queryParams.stockId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.getAdminFeeDetails()
  }

  getAdminFeeDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_APPROVAL_VIEW,
      this.stockId
    ).subscribe((response: IApplicationResponse) => {
      this.stockIdDetails = response.resources;
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  navigateToEdit() {
    if (this.stockId) {
      this.router.navigate(['/billing', 'stock-id-approval', 'edit'],
        { queryParams: { stockId: this.stockId } });
    }
  }
}