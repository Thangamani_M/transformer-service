import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverrideReportRoutingModule } from './override-report-routing.module';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverrideReportListComponent } from './override-report-list.component';
@NgModule({
  declarations: [OverrideReportListComponent],
  imports: [
    CommonModule,
    OverrideReportRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class OverrideReportModule { }
