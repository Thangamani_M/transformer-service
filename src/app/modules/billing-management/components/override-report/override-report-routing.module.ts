import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverrideReportListComponent } from './override-report-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: OverrideReportListComponent, canActivate: [AuthGuard], data: { title: 'Override Report list' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class OverrideReportRoutingModule { }
