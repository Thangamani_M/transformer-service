import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-override-report-list',
  templateUrl: './override-report-list.component.html'
})
export class OverrideReportListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {}
  loggedUser: any;
  first: any = 0;
  dateFormat = 'MMM dd, yyyy';

  constructor(private snackbarService: SnackbarService, private rxjsService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Override Report",
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '/billing' }, { displayName: 'Override Report', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Override Report',
            dataKey: 'chargeListId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'debtorCode', header: 'Debtor Code', width: '200px' },
            { field: 'bdiNumber', header: 'BDI Number', width: '200px' },
            { field: 'bank', header: 'Bank', width: '200px' },
            { field: 'bankBranch', header: 'Bank Branch Code', width: '200px' },
            { field: 'accountType', header: 'Account Type', width: '200px' },
            { field: 'accountNo', header: 'Account No', width: '200px' },
            { field: 'transactionRefNo', header: 'Transaction No', width: '200px' },
            { field: 'transactionDate', header: 'Trans Date', width: '200px', isDate:true},
            { field: 'doRunCode', header: 'DO Run Code', width: '200px' },
            { field: 'division', header: 'Division', width: '200px' },
            { field: 'district', header: 'District', width: '200px' },
            { field: 'branch', header: 'Branch', width: '200px' },
            { field: 'total', header: 'Master Total', width: '200px' }],
            apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_REPORT,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableAction: true,
            enableExportBtn: true
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.OVERRIDE_REPORT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let apiVersion = 1;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.INVOICE_REPORT,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams),
      apiVersion
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.exportList();
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  exportList() {
    if (this.dataList.length != 0) {
      let fileName = 'Override Report' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["Debtor Code", "BDI Number", "Bank", "Bank Branch Code", "Account Type", "Account No", "Transaction No", "Trans Date", "DO Run Code", "Division", "District", "Branch", "Master Total"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList.map(c => {
        csv += [c['debtorCode'], c['bdiNumber'], c['bank'], c['bankBranch'], c['accountType'], c['accountNo'], c['transactionRefNo'], c['transactionDate'], c['doRunCode'], c['division'], c['district'], c['branch'], c['total']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    } else {
      this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
    }
  }
}

