import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonthlyTestRunFinalRoutingModule } from './monthly-test-run-final-routing.module';
import { MonthlyTestRunFinalListComponent } from './monthly-test-run-final-list.component';
import { MonthlyTestRunFinalViewComponent } from './monthly-test-run-final-view.component';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [MonthlyTestRunFinalListComponent, MonthlyTestRunFinalViewComponent],
  imports: [
    CommonModule,
    MonthlyTestRunFinalRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class MonthlyTestRunFinalModule { }
