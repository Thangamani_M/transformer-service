import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonthlyTestRunFinalListComponent } from './monthly-test-run-final-list.component';
import { MonthlyTestRunFinalViewComponent } from './monthly-test-run-final-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: MonthlyTestRunFinalListComponent, canActivate: [AuthGuard], data: { title: 'Monthly Test Run list' } },
  { path: 'view', component: MonthlyTestRunFinalViewComponent, canActivate: [AuthGuard], data: { title: 'Monthly Test Run View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class MonthlyTestRunFinalRoutingModule { }
