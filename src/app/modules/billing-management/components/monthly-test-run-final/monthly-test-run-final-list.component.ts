import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-monthly-test-run-final-list',
  templateUrl: './monthly-test-run-final-list.component.html'
})
export class MonthlyTestRunFinalListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {}
  loggedUser: any;
  constructor(private router: Router, private rxjsService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Monthly Test Run",
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '/billing' }, { displayName: 'Monthly Test Run', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Monthly Test Run',
            dataKey: 'chargeListId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'chargeListNumber', header: 'Batch Number', width: '200px' }, { field: 'totalRecords', header: 'Total Records', width: '200px' },
            { field: 'totalInclVAT', header: 'Total Value(R)', width: '200px' }, { field: 'createdBy', header: 'Created By', width: '200px' },
            { field: 'runDate', header: 'Run Date', width: '200px', isDate:true }, { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'crmStatus', header: 'CRM Status', width: '200px' }, { field: 'bankStatus', header: 'Bank Status', width: '200px' },
            { field: 'status', header: 'Status', width: '200px' }],
            apiSuffixModel: BillingModuleApiSuffixModels.CHARGE_LIST_REPORT,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let apiVersion = 1;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.CHARGE_LIST_REPORT,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams),
      apiVersion
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['billing/charge-list/add-edit']);
        break;
      case CrudType.GET:
        this.getRequiredListData(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.VIEW:
        this.router.navigate(['billing/monthly-test-run-final/view'], { queryParams: { id: row["chargeListId"] } });
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
