import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-monthly-test-run-final-view',
  templateUrl: './monthly-test-run-final-view.component.html'
})
export class MonthlyTestRunFinalViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  dataList1: any
  row: any = {}
  MonthlyTestRunFinalDetail: any;
  chargeListNumber: [];
  firstInvoiceRefNo: []
  lastInvoiceRefNo: any = [];
  receiptAmount: any = [];
  paymentUnderrideAmount: any = [];
  invoiceTotal: any = [];
  paymentOverrideAmount: any = [];
  loggedUser: any;
  dataList: any;
  status: any = [];
  dateFormat = 'MMM dd, yyyy';
  chargeListId: any;
  chargeListDetails: any;

  constructor(private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private rxjsService: RxjsService, private snackbarService: SnackbarService, private crudService: CrudService, private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Monthly Test Run",
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '/billing' }, { displayName: 'Monthly Test Run', relativeRouterUrl: '/billing/monthly-test-run-final' }, { displayName: 'View Monthly Test Run', relativeRouterUrl: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Monthly Test Run',
            dataKey: 'chargeListId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'branchName', header: 'Branch Name', width: '200px' },
              { field: 'groupInvoice', header: 'Group', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor', width: '200px' },
              { field: 'bdiNumber', header: 'BDI', width: '200px' },
              { field: 'invoiceRefNo', header: 'Invoice No.', width: '200px' },
              { field: 'contractRefNo', header: 'Contract No.', width: '200px' },
              { field: 'paymentMethodName', header: 'Payment Method', width: '200px' },
              { field: 'debitOrderRunCodeName', header: 'Run Code', width: '200px' },
              { field: 'invTotalExc', header: 'Payment Fee', width: '200px' },
              { field: 'invTotalIncl', header: 'Invoice Total', width: '200px' },
              { field: 'radioAmountExc', header: 'Radio Amount Excl.', width: '200px' },
              { field: 'radioAmountIncl', header: 'Radio Amount Incl.', width: '200px' },
              { field: 'doNumber', header: 'DO Number', width: '200px' },
              { field: 'doTotal', header: 'DO Total', width: '200px' },
              { field: 'doOverrideAmount', header: 'DO Override', width: '200px' },
              { field: 'doUnderrideAmount', header: 'DO Underride', width: '200px' },
              { field: 'districtName', header: 'District', width: '200px' },
              { field: 'crmStatus', header: 'CRM Status', width: '200px' },
              { field: 'crmError', header: 'CRM Error', width: '200px' },
              { field: 'bankStatus', header: 'Bank Status', width: '200px' },
              { field: 'bankError', header: 'Bank Error', width: '200px' },
            ],
            enableAddActionBtn: false,
            enablePrintBtn: false,
            printTitle: 'Pro Forma Invoice',
            printSection: 'print-section0',
            enableEmailBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableAction: true,
            enableExportBtn: true
          },

        ]
      }
    }
    this.MonthlyTestRunFinalDetail = [
      { name: 'Transaction Batch', value: '' },
      { name: 'Total Records', value: '' },
      { name: 'Invoice Total', value: '' },
      { name: 'Division', value: '' },
      { name: 'Completion Date', value: '' },
      { name: 'Run Date', value: '' },
      { name: 'Financial Year', value: '' },
      { name: 'CRM Status', value: '' },
      { name: 'Bank Status', value: '' },
      { name: 'Start Date', value: '' },
      { name: 'Created By', value: '' },
      { name: 'File Send to Bank', value: '' },
      { name: 'Status', value: '' },
    ]
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.chargeListId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';

    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredDetailData();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.MONTHLY_TEST_RUN]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredDetailData() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CHARGE_LIST_REPORT, this.chargeListId, false)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode = 200 && response.resources) {
          this.chargeListDetails = response.resources;
          this.MonthlyTestRunFinalDetail = [
            { name: 'Transaction Batch', value: response.resources?.chargeListNumber },
            { name: 'Total Records', value: response.resources?.totalRecords },
            { name: 'Invoice Total', value: response.resources?.totalInclVAT },
            { name: 'Division', value: response.resources?.divisionName },
            { name: 'Completion Date', value: response.resources?.completionDate },
            { name: 'Run Date', value: response.resources?.runDate },
            { name: 'Financial Year', value: response.resources?.financialYear },
            { name: 'CRM Status', value: response.resources?.crmStatus },
            { name: 'Bank Status', value: response.resources?.bankStatus },
            { name: 'Start Date', value: response.resources?.runDate },
            { name: 'Created By', value: response.resources?.createdBy },
            { name: 'File Send to Bank', value: response.resources?.fileSentToBank },
            { name: 'Status', value: response.resources?.status },
          ]
          if (this.chargeListDetails.status == 'new' || !this.chargeListDetails.status) {
            this.updatedMonthlyList()
          }
          this.dataList = this.chargeListDetails.debitOrderTransaction;
          this.dataList1 = this.chargeListDetails.reportDetail
          this.totalRecords = this.chargeListDetails?.reportDetail.length;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  updatedMonthlyList() {
    let formValue = {
      chargeListId: this.chargeListId,
      modifiedUserId: this.loggedUser.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CHARGE_LIST, formValue).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
      }
      this.rxjsService.setDialogOpenProperty(false);
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredDetailData()
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.exportList();
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  exportList() {
    if (this.dataList.length != 0) {
      let fileName = 'Monthly Test Run ' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["Branch Name", "Group", "Debtor", "BDI", "Invoice No.", "Contract No.", "Payment Method", "Run Code", "Payment Fee", "Invoice Total", "Radio Amount Excl.", "Radio Amount Incl.", "DO Number", "DO Total", "DO Override", "DO Underride", "District", "CRM Status", "CRM Error", "Bank Status", "Bank Error"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList1.map(c => {
        csv += [c['branchName'], c['groupInvoice'], c['debtorRefNo'], c['bdiNumber'], c['invoiceRefNo'], c['contractRefNo'], c['paymentMethodName'], c['debitOrderRunCodeName'], c['invTotalExc'], c['invTotalIncl'], c['radioAmountExc'], c['radioAmountIncl'], c['doNumber'], c['doTotal'], c['doOverrideAmount'], c['doUnderrideAmount'], c['districtName'], c['crmStatus'], c['crmError'], c['bankStatus'], c['bankError']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    } else {
      this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
    }
  }
}
