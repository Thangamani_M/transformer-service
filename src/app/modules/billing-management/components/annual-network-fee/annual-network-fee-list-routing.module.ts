import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnnualNetworkFeeAddEditComponent } from './annual-network-fee-add-edit.component';
import { AnnualNetworkListComponent } from './annual-network-fee-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  // { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list/add-edit', component: AnnualNetworkFeeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Annual Network Fee Add Edit' } },
  { path: '', component: AnnualNetworkListComponent, canActivate: [AuthGuard], data: { title: 'Annual Network Fee List' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AnnualFeeRoutingModule { }
