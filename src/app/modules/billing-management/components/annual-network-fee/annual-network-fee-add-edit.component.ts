
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, removeFormControls, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AnnualFeeAddEditModel } from '@modules/others/configuration/models/same-day-test-run.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-annual-network-add-edit',
  templateUrl: './annual-network-fee-add-edit.component.html'
})
export class AnnualNetworkFeeAddEditComponent implements OnInit {
  stockId: any
  divisionDropDown: any = [];
  annualNetworkFeeAddEditForm: FormGroup;
  rOCommunityPatrolTypesList: FormArray;
  loggedUser: any;
  isLoading: boolean;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  stockIdDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  todayDate = new Date();
  dropdownsAndData = [];
  financialYearList = [];
  divisionList = [];
  inValid: boolean;
  addressDetails: any;
  debtorDetails: any;
  detailObj: any;
  dt: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  annualNetworkFeeWaiverId: any;
  userId: any;
  queryParamObj: any;
  data: any;
  show: boolean = false;
  customerNumberList: any;
  customerNameList: any;
  primengTableConfigProperties: any
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.queryParamObj = this.activatedRoute.snapshot.queryParams;
    this.annualNetworkFeeWaiverId = this.activatedRoute.snapshot.queryParams.id;
    this.userId = this.activatedRoute.snapshot.queryParams.userId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: `${this.annualNetworkFeeWaiverId ? "Update" : "Add"} Annual Network fee`,
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing Management', }, {
        displayName: 'Annual Network fee', relativeRouterUrl: '/billing/annual-network-fee'
      }, {
        displayName: `${this.annualNetworkFeeWaiverId ? "Update" : "Add"} Annual Network fee`,
        shouldShowBreadCrumb: false,
      }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Credit Controller Type Mapping',
            dataKey: 'creditControllerTypeId',
            enableBreadCrumb: true,
            enableAction: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.createAnnualNetworkFeeForm();
    this.onFormControlChanges();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.annualNetworkFeeWaiverId) {
      this.getStockIdDetailsById().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.data = response.resources;
        this.detailObj = response.resources;
        this.data.noOfConsecutiveWaiverYear = this.data.noOfConsecutiveWaiverYear == 0 ? "" : this.data.noOfConsecutiveWaiverYear;
        if (this.loggedUser.roleName == 'General Manager' || this.loggedUser.roleName == 'District Manager') {
          this.show = true;
        } else {
          this.show = false;
        }
        this.data.motivation = this.data['requestMotivation'];
        this.data.branch = this.data.branchName;
        this.data.division = this.data.divisionName;
        this.data.subArea = this.data.subareaName;
        this.annualNetworkFeeAddEditForm.patchValue(this.data);
        this.changeAddress();
      })
    }
  }

  onSelectedCustomer(event, data, str) {
    this.annualNetworkFeeAddEditForm.get('addressId').setValue('');
    this.annualNetworkFeeAddEditForm.get('customerId').setValue(data.id);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, UserModuleApiSuffixModels.CUSTOMER_DETAILS_ANNUAL, null, true, prepareGetRequestHttpParams(null, null, {
      customerId: data.id
    })).subscribe(res => {
      if (res.resources) {
        this.detailObj = res.resources;
        if (str == 'name') {
          this.annualNetworkFeeAddEditForm.get('customerRefNo').setValue(this.detailObj.customerRefNo);
        } else {
          this.annualNetworkFeeAddEditForm.get('customerName').setValue(this.detailObj.customerName);
        }
      }
    })
  }

  changeAddress() {
    let adId = this.annualNetworkFeeAddEditForm.value.addressId;
    if (adId) {
      let found = this.detailObj.addressDetails.find(e => e.addressId == adId);
      this.data = found;
      if (found) {
        this.annualNetworkFeeAddEditForm.get('division').setValue(found.divisionName);
        this.annualNetworkFeeAddEditForm.get('branch').setValue(found.branchName);
        this.annualNetworkFeeAddEditForm.get('subArea').setValue(found.subAreaName)
      }
    }
  }

  onFormControlChanges() {
    this.annualNetworkFeeAddEditForm.get('customerRefNo').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val) {
          this.customerList = [];
          return this.filterServiceTypeDetailsBySearchOptions(val);
        } else {
          this.annualNetworkFeeAddEditForm.get('customerName').setValue('')
          this.customerList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources) {
            this.customerNumberList = response.resources;
            this.inValid = false;
          } else {
            this.customerList = [];
            this.inValid = true;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.annualNetworkFeeAddEditForm.get('customerName').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val) {
          return this.filterServiceTypeDetailsBySearchOptions(val);
        } else {
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources) {
            this.customerNameList = response.resources;
          } else {
            this.customerList = [];
            this.inValid = true;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  filterServiceTypeDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, UserModuleApiSuffixModels.UX_CUSTOMERS, null, true, prepareGetRequestHttpParams(null, null, {
      customerRefNo: this.annualNetworkFeeAddEditForm.value.customerRefNo ? this.annualNetworkFeeAddEditForm.value.customerRefNo : null,
      customerName: this.annualNetworkFeeAddEditForm.value.customerName ? this.annualNetworkFeeAddEditForm.value.customerName : null,
    }))
  }
  customerList: any;
  onSelectedAddressFromAutoComplete() { }

  createAnnualNetworkFeeForm(): void {
    let annualFeeAddEditModel = new AnnualFeeAddEditModel();
    this.annualNetworkFeeAddEditForm = this.formBuilder.group({
      noOfConsecutiveWaiverYear: [null, [Validators.max(5)]]
    });
    Object.keys(annualFeeAddEditModel).forEach((key) => {
      this.annualNetworkFeeAddEditForm.addControl(key, new FormControl(annualFeeAddEditModel[key]));
    });
    this.annualNetworkFeeAddEditForm = setRequiredValidator(this.annualNetworkFeeAddEditForm, ["customerRefNo", "customerName", "debtorId", "addressId", "motivation", "excludeReason", "districtManagerComments", "generalManagerComments"]);
    this.annualNetworkFeeAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
  }

  changeIspermanent() {
    let isPerma = this.annualNetworkFeeAddEditForm.get('isPermanentExclusion').value;
    if (isPerma) {
      this.annualNetworkFeeAddEditForm.get('isOnceOfExclusion').setValue(false);
    }
  }

  changeIsonceoff() {
    let isOnce = this.annualNetworkFeeAddEditForm.get('isOnceOfExclusion').value;
    if (isOnce) {
      this.annualNetworkFeeAddEditForm.get('isPermanentExclusion').setValue(false);
    }
  }

  yearChange() {
    let yr = this.annualNetworkFeeAddEditForm.get('noOfConsecutiveWaiverYear').value;
  }
  //Get Details
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.ANNUAL_NETWORK_FEE,
      this.annualNetworkFeeWaiverId
    );
  }

  onSubmit(): void {
    let formValue = this.annualNetworkFeeAddEditForm.value;
    if (this.userId) {
      this.annualNetworkFeeAddEditForm = removeFormControls(this.annualNetworkFeeAddEditForm, ["excludeReason"]);
    } else {
      this.annualNetworkFeeAddEditForm = removeFormControls(this.annualNetworkFeeAddEditForm, ["districtManagerComments"]);
    }
    if (this.loggedUser.roleName != 'General Manager') {
      this.annualNetworkFeeAddEditForm = removeFormControls(this.annualNetworkFeeAddEditForm, ["generalManagerComments"]);
    }
    if (this.annualNetworkFeeAddEditForm.invalid) {
      return;
    }
    formValue.createdUserId = this.loggedUser.userId;
    formValue.noOfConsecutiveWaiverYear = formValue.noOfConsecutiveWaiverYear ? formValue.noOfConsecutiveWaiverYear : "";
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ANNUAL_NETWORK_FEE, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (!this.userId) {
            this.router.navigateByUrl('/billing/annual-network-fee');
          } else {
            this.router.navigateByUrl('/billing/annual-network-fees-request');
          }
        }
      })
  }
}
