import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-annual-network-fee-list',
  templateUrl: './annual-network-fee-list.component.html'
})
export class AnnualNetworkListComponent extends PrimeNgTableVariablesModel implements OnInit {
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  activeStatus: any = []
  row: any = {}
  constructor(private crudService: CrudService,
    public dialogService: DialogService, private router: Router,
    private store: Store<AppState>, private rxjsService: RxjsService, private snackbarService: SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Annual Network Fee",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Annual Network Fee' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Annual Network Fee',
            dataKey: 'annualNetworkFeeWaiverId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'requestNumber', header: 'Request Number', width: '160px' },
              { field: 'customerRefNo', header: 'Customer ID', width: '200px' },
              { field: 'customerName', header: 'Customer Name', width: '200px' },
              { field: 'debtorscode', header: 'Debtor Code', width: '150px' },
              { field: 'fullAddress', header: 'Address', width: '500px' },
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'branchName', header: 'Branch', width: '150px' },
              { field: 'subareaName', header: 'Sub Area', width: '200px' },
              { field: 'requestType', header: 'Request Type', width: '200px' },
              { field: 'requestMotivation', header: 'Request Motivation', width: '180px' },
              { field: 'creationDate', header: 'Creation Date', width: '150px' },
              { field: 'createdBy', header: 'Creation By', width: '150px' },
              { field: 'creatorsRole', header: 'Creatior Role', width: '150px' },
              { field: 'lastApprovedBy', header: 'Last Aprroved By', width: '150px' },
              { field: 'lastApprovalDate', header: 'Last Approval Date', width: '170px' },
              { field: 'actionedDate', header: 'Actioned Date', width: '150px' },
              { field: 'status', header: 'Status', width: '150px' },
              // { field: 'status', header: 'Status', width: '150px', type:"dropdown",options: [
              //   { label: 'Active', value: true },
              //   { label: 'In-Active', value: false },
              // ] },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.ANNUAL_NETWORK_FEE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true
          },
        ]
      }
    }
    this.activeStatus = [
      { label: 'Enabled', value: 'Enabled' },
      { label: 'Disabled', value: 'Disabled' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][BILLING_MODULE_COMPONENT.ANNUAL_NETWORK_FEE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.ANNUAL_NETWORK_FEE,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("billing/annual-network-fee/list/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["billing/annual-network-fee/list/add-edit"], { queryParams: { id: editableObject['annualNetworkFeeWaiverId'] } });
            break;
        }
    }
  }
}
