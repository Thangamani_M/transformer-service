import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AnnualNetworkFeeAddEditComponent } from './annual-network-fee-add-edit.component';
import { AnnualFeeRoutingModule } from './annual-network-fee-list-routing.module';
import { AnnualNetworkListComponent } from './annual-network-fee-list.component';
@NgModule({
  declarations: [AnnualNetworkListComponent, AnnualNetworkFeeAddEditComponent],
  imports: [
    CommonModule,
    AnnualFeeRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ]
})
export class AnnualFeeModule { }
