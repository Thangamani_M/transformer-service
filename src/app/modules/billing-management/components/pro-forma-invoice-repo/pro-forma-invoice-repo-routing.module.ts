import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProFormaInvoiceRepoComponent } from './pro-forma-invoice-repo.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: ProFormaInvoiceRepoComponent, canActivate:[AuthGuard],data: { title: ' Pro Forma Invoice Repository' }}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ProFormaInvoiceRepoRoutingModule { }
