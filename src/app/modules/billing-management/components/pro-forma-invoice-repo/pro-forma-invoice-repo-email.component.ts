import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
@Component({
  selector: 'app-pro-forma-invoice-repo-email',
  templateUrl: './pro-forma-invoice-repo-email.component.html',
  styleUrls: ['.//pro-forma-invoice-repo.component.scss']
})
export class ProformnaInvoiceRepoEmailComponent implements OnInit {

  creditCardExpiryFileId = null;
  userData: any;
  proFormaInvoiceForm: FormGroup;
  constructor(public ref: DynamicDialogRef, private activatedRoute: ActivatedRoute, public config: DynamicDialogConfig, private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService) {
    this.creditCardExpiryFileId = this.activatedRoute.snapshot?.queryParams?.id ? this.activatedRoute.snapshot?.queryParams?.id : null;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }
  ngOnInit(): void {
    this.createForm();
  }
  createForm(): void {
    this.proFormaInvoiceForm = new FormGroup({
      'email': new FormControl(null),
    });
    this.proFormaInvoiceForm = setRequiredValidator(this.proFormaInvoiceForm, ['email']);
  }

  onSubmit() {
    if (this.proFormaInvoiceForm.invalid) {
      return;
    }
    let formValue = this.proFormaInvoiceForm.value;
    let finalObject = {
      emailId: formValue.email
    }
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PROFORMA_INVOICE_SEND_MAIL, finalObject, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.ref.close(response);
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
  }

  btnCloseClick() {
    this.ref.close(false);
  }
}
