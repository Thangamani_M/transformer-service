import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ProformnaInvoiceRepoEmailComponent } from './pro-forma-invoice-repo-email.component';
import { ProFormaInvoiceRepoRoutingModule } from './pro-forma-invoice-repo-routing.module';
import { ProFormaInvoiceRepoComponent } from './pro-forma-invoice-repo.component';



@NgModule({
  declarations: [ProFormaInvoiceRepoComponent,ProformnaInvoiceRepoEmailComponent],
  imports: [
    CommonModule,
    ProFormaInvoiceRepoRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ],
  entryComponents:[ProformnaInvoiceRepoEmailComponent],
})
export class ProFormaInvoiceRepoModule { }
