import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { ProFormaInvoiceRepoFilterModel } from "../../models/pro-forma-invoice.model";
import { ProformnaInvoiceRepoEmailComponent } from "./pro-forma-invoice-repo-email.component";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-pro-forma-invoice-repo',
  templateUrl: './pro-forma-invoice-repo.component.html',
  styleUrls: ['./pro-forma-invoice-repo.component.scss']
})
export class ProFormaInvoiceRepoComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {}
  loggedUser: any;
  showFilterForm: any = false;
  proFormInvoiceFilterForm: FormGroup;
  startTodayDate = 0;
  filteredData: any = {};
  dateFormat = 'MMM dd, yyyy';

  constructor(private router: Router, private datePipe: DatePipe, private dialogService: DialogService,
    private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>, private snackbarService : SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Pro Forma Invoice Repository",
      breadCrumbItems: [{ displayName: 'Billing Management', relativeRouterUrl: '/billing' }, { displayName: 'Pro Forma Invoice Repository', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Pro Forma Invoice Repository',
            dataKey: 'proformaInvoiceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'customerRefNo', header: 'Customer ID', width: '200px' },
              { field: 'customerName', header: 'Customer Name', width: '200px' },
              { field: 'customerAddress', header: 'Customer Address', width: '200px' },
              { field: 'region', header: 'Region', width: '200px' },
              { field: 'division', header: 'Division', width: '200px' },
              { field: 'branch', header: 'Branch', width: '200px' },
              { field: 'debtorsCode', header: 'Debtors Code', width: '200px' },
              { field: 'debtorsName', header: 'Debtors Name', width: '200px' },
              { field: 'quotationNumber', header: 'Quotation Number', width: '200px' },
              { field: 'proformaInvoiceNumber', header: 'Pro Forma Invoice Number', width: '200px' },
              { field: 'proformaInvoiceDate', header: 'Pro Forma Invoice Date', width: '200px' },
              { field: 'proformaInvoiceAmount', header: 'Pro Forma Invoice Amount', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Pro Forma Invoice',
            printSection: 'print-section0',
            enableEmailBtn: true,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: BillingModuleApiSuffixModels.PROFORMA_INVOICE,
            moduleName: ModulesBasedApiSuffix.BILLING,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createDealerMaintenanceFilterForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.PRO_FORMA]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createDealerMaintenanceFilterForm(proFormaInvoiceRepoFilterModel?: ProFormaInvoiceRepoFilterModel) {
    let proFormaInvoiceModel = new ProFormaInvoiceRepoFilterModel(proFormaInvoiceRepoFilterModel);
    this.proFormInvoiceFilterForm = this.formBuilder.group({});
    Object.keys(proFormaInvoiceModel).forEach((key) => {
      this.proFormInvoiceFilterForm.addControl(key, new FormControl(proFormaInvoiceModel[key]));
    });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.GET:
        unknownVar = { ...this.filteredData, ...unknownVar };
        row['pageIndex'] = row['pageIndex'] ? row['pageIndex'] : 0;
        this.getRequiredListData(row['pageIndex'], row['pageSize'], unknownVar)
        break;
      case CrudType.EMAIL:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEmail) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openSendEmailPopup('Send Email', row, 'create');
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.export({ proformaInvoiceId: null });
        break;
      case CrudType.VIEW:
        this.rxjsService.setViewCustomerData({
          customerId: row['customerId'],
          addressId: row['addressId'],
          customerTab: 0,
          monitoringTab: null,
        })
        this.rxjsService.navigateToViewCustomerPage();
        break;
      default:
    }
  }

  openSendEmailPopup(header, row, screen) {
    const rowData = { ...row };
    const ref = this.dialogService.open(ProformnaInvoiceRepoEmailComponent, {
      header: header,
      baseZIndex: 1000,
      width: '650px',
      closable: false,
      showHeader: false,
      data: {
        row: rowData,
        screen: screen
      },
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        ref.close(false);
      }
    });
  }

  export(finalObject) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PROFORMA_INVOICE_EXPORT, null, false, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          var url = response.resources;
          let fileName = 'Pro Forma Invoice' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
          this.downloadExportedFile(url, fileName);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  downloadExportedFile(url, fileName) {
    var link = document.createElement("a");
    if (link.download !== undefined) {
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
  submitFilter() {
    this.filteredData = Object.assign({},
      { customerRefNo: this.proFormInvoiceFilterForm.get('customerRefNo').value ? this.proFormInvoiceFilterForm.get('customerRefNo').value : '' },
      { customerName: this.proFormInvoiceFilterForm.get('customerName').value ? this.proFormInvoiceFilterForm.get('customerName').value : '' },
      { customerAddress: this.proFormInvoiceFilterForm.get('customerAddress').value ? this.proFormInvoiceFilterForm.get('customerAddress').value : '' },
      { region: this.proFormInvoiceFilterForm.get('region').value ? this.proFormInvoiceFilterForm.get('region').value : '' },
      { division: this.proFormInvoiceFilterForm.get('division').value ? this.proFormInvoiceFilterForm.get('division').value : '' },
      { branch: this.proFormInvoiceFilterForm.get('branch').value ? this.proFormInvoiceFilterForm.get('branch').value : '' },
      { debtorsCode: this.proFormInvoiceFilterForm.get('debtorsCode').value ? this.proFormInvoiceFilterForm.get('debtorsCode').value : '' },
      { debtorsName: this.proFormInvoiceFilterForm.get('debtorsName').value ? this.proFormInvoiceFilterForm.get('debtorsName').value : '' },
      { quotationNumber: this.proFormInvoiceFilterForm.get('quotationNumber').value ? this.proFormInvoiceFilterForm.get('quotationNumber').value : '' },
      { proformaInvoiceNumber: this.proFormInvoiceFilterForm.get('proformaInvoiceNumber').value ? this.proFormInvoiceFilterForm.get('proformaInvoiceNumber').value : '' },
      { proformaInvoiceDate: this.proFormInvoiceFilterForm.get('proformaInvoiceDate').value ? this.proFormInvoiceFilterForm.get('proformaInvoiceDate').value : '' },
      { proformaInvoiceAmount: this.proFormInvoiceFilterForm.get('proformaInvoiceAmount').value ? this.proFormInvoiceFilterForm.get('proformaInvoiceAmount').value : '' }
    );

    Object.keys(this.filteredData).forEach(key => {
      if (this.filteredData[key] === "" || this.filteredData[key].length == 0) {
        delete this.filteredData[key]
      }
    });
    let filterdNewData = Object.entries(this.filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0;
    this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }
  resetForm() {
    this.proFormInvoiceFilterForm.reset()
    this.showFilterForm = !this.showFilterForm;
    this.getRequiredListData();
  }
  btnCloseClick() {
    this.showFilterForm = !this.showFilterForm;
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
