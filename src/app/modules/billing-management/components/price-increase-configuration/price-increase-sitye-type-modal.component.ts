import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, removeFormControlError, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData, PriceIncreaseConfigSiteTypeAddEditModel } from '@modules/others';
import { Store } from '@ngrx/store';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'price-increase-sitye-type-modal',
  templateUrl: './price-increase-sitye-type-modal.component.html',
})
export class PriceIncreaseModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  leadId: string;
  priceIncreaseConfigAddEditForm: FormGroup;
  siteTypeList = [];
  adminFeeId: any;
  selectedOptions = [];
  loggedInUserData: LoggedInUserModel;
  priceDetails: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isEdit = false;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService,
    private snackbarService : SnackbarService,
    private dialog: MatDialog, private store: Store<AppState>) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createPriceIncreaseAddEditForm();
    this.getSityeTypes();
    if (this.data.pricingConfigSiteTypeId) {
      this.getDetails();
    }
  }

  getSityeTypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SITE_TYPES, null, false,
      prepareGetRequestHttpParams(undefined, null, null))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.siteTypeList = getPDropdownData(response.resources);
          // for (let i = 0; i < TmpArray.length; i++) {
          //   let temp = {};
          //   temp['display'] = TmpArray[i].displayName;
          //   temp['value'] = TmpArray[i].id;
          //   this.siteTypeList.push(temp);
          // }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createPriceIncreaseAddEditForm(): void {
    let priceModel = new PriceIncreaseConfigSiteTypeAddEditModel();
    this.priceIncreaseConfigAddEditForm = this.formBuilder.group({
      priceIncreasePercentage: [null, [Validators.required, Validators.max(100)]]
    });
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.priceIncreaseConfigAddEditForm = setRequiredValidator(this.priceIncreaseConfigAddEditForm, ["priceLimitDescription", "lowerLimit", "upperLimit"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_CONFIG_SITE_TYPES, this.data.pricingConfigSiteTypeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceDetails = response.resources;
          this.priceIncreaseConfigAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  editPage() {
    if(!this.data?.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isEdit = true;
  }

  onPercentageChanges(i?) {
    this.priceIncreaseConfigAddEditForm.get('priceIncreasePercentage').valueChanges.subscribe((priceIncreasePercentage: string) => {
      if (priceIncreasePercentage == '0' || priceIncreasePercentage == '00' || priceIncreasePercentage == '0.0' ||
        priceIncreasePercentage == '0.00' || priceIncreasePercentage == '00.0' || priceIncreasePercentage == '00.00' ||
        priceIncreasePercentage == '00.' || priceIncreasePercentage == '0.' || priceIncreasePercentage.includes('99.')) {
        this.priceIncreaseConfigAddEditForm.get('priceIncreasePercentage').setErrors({ 'invalid': true });
      }
      else {
        this.priceIncreaseConfigAddEditForm = removeFormControlError(this.priceIncreaseConfigAddEditForm as FormGroup, 'invalid');
      }
    });
  }

  onSubmit() {
    if (this.priceIncreaseConfigAddEditForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.data.pricingConfigSiteTypeId) {
      this.priceIncreaseConfigAddEditForm.get('modifiedUserId').setValue(this.loggedInUserData.userId);
      this.priceIncreaseConfigAddEditForm.get('branchIds').setValue(this.priceDetails.branchIds);
      this.priceIncreaseConfigAddEditForm.get('mainAreaIds').setValue(this.priceDetails.mainAreaIds);
      this.priceIncreaseConfigAddEditForm.value.siteTypeId = this.priceDetails.siteTypeId;
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_CONFIG_SITE_TYPES, this.priceIncreaseConfigAddEditForm.value).subscribe((response) => {
        this.rxjsService.setDialogOpenProperty(false);
        if (response.isSuccess && response.statusCode == 200) {
          this.outputData.emit(true);
          this.dialog.closeAll();
        }
      })
    } else {
      this.priceIncreaseConfigAddEditForm.get('createdUserId').setValue(this.data.createdUserId);
      this.priceIncreaseConfigAddEditForm.get('branchIds').setValue(this.data.branchIds);
      this.priceIncreaseConfigAddEditForm.get('mainAreaIds').setValue(this.data.mainAreaIds);
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_CONFIG_SITE_TYPES, this.priceIncreaseConfigAddEditForm.value).subscribe((response) => {
        this.rxjsService.setDialogOpenProperty(false);
        if (response.isSuccess && response.statusCode == 200) {
          this.outputData.emit(true);
          this.dialog.closeAll();
        }
      })
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
