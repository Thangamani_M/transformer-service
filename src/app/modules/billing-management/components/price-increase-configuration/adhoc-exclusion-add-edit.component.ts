

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, getPDropdownData, HttpCancelService, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'adhoc-exclusion-add-edit',
  templateUrl: './adhoc-exclusion-add-edit.component.html',
  styleUrls: ['./monthly-price-add-edit.component.scss']
})
export class AddhocExclusionAddEditComponent implements OnInit {
  id: any;
  @Output() outputData = new EventEmitter<any>();
  defaultLocationList: any;
  contactTypeList: any;
  debtorList: any;
  startTodayDate = new Date();

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }
  adhocExclusionAddEditForm: FormGroup;
  priceIncreaseConfigAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  divisionDropDown = [];
  regionList = [];
  districtList = [];
  branchList = [];
  siteTypeList = [];
  priceIncDailog = false;
  selectedOptions = [];
  adminFeeId: any;
  pricingIncreaseRunDateConfigId: any;
  monthsList = [{ id: 1, displayName: 'Jan' }, { id: 2, displayName: 'Feb' }, { id: 3, displayName: 'March' }, { id: 4, displayName: 'April' }, { id: 5, displayName: 'May' }, { id: 6, displayName: 'June' }, { id: 7, displayName: 'July' }, { id: 8, displayName: 'Aug' }, { id: 9, displayName: 'Sep' }, { id: 10, displayName: 'Oct' }, { id: 11, displayName: 'Nov' }, { id: 12, displayName: 'Dec' }]
  defaultLocationListAll = []
  debtorListAll = []
  stockIdDetails: any;
  priceIncreaseList = [];
  isFileList = true;
  primengTableConfigProperties: any = {
    tableCaption: "Adhoc price increase 2 of 2 - Exclusions",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration' },
    {displayName: 'Billing'},
    {displayName: 'Price Increase Configuration',relativeRouterUrl: '/billing/price-increase-configuration',queryParams:{tab:4}},
    {displayName: 'Adhoc Last Increase Dateprice exclusion'}
  ],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Type Mapping',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
        }]
    }
  }
  ngOnInit() {
    this.createPriceIncreaseForm();
    this.bindingdeDebtorDropdown();
    this.getListData()
    this.checkFileDetails()
    this.getAllDebtors();
    this.getAllSuburbs()
  }

  getSuburb(id) {
    if (this.defaultLocationListAll && this.defaultLocationListAll.length > 1) {
      let data = this.defaultLocationListAll.find(e => e.value == id);
      if (!data) {
        return '--';
      }
      return data.label;
    }
  }

  getListData() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ADHOC_EXCLUSION_POST, undefined, false, prepareRequiredHttpParams({ pricingFileNameId: this.id })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.priceIncreaseList = response.resources;
      }
    })
  }
  checkFileDetails() {
    let obj = {
      "pricingFileNameId": this.id,
      "createdUserId": this.loggedInUserData.userId,
    }
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ADHOC_EXCLUSION__NAME_DETAILS,obj).subscribe((response) => {
        this.isFileList = response.isSuccess;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  submitData() {
    let submitObj = {
      "pricingFileNameId": this.id,
      "createdUserId": this.loggedInUserData.userId,
      "adhocExclusions": this.priceIncreaseList
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ADHOC_EXCLUSION_POST, submitObj).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.next()
      }
    })
  }

  next() {
    if(this.isFileList){
      this.router.navigate(['/billing/price-increase-configuration'], { queryParams: { tab: 4 } })
    }else{
      this.router.navigate(['/billing/price-increase-configuration/monthly-config/add-edit'], { queryParams: { id: this.id, configId: 2 } })
    }
  }
  back() {
    this.router.navigate(['/billing/price-increase-configuration/adhoc-config/add-edit'], { queryParams: { id: this.id } });
  }

  getDebtor(id) {
    if (this.debtorListAll && this.debtorListAll.length > 1) {
      let data = this.debtorListAll.find(e => e.value == id);
      if (!data) {
        return '--';
      }
      return data.label;
    }
  }

  getMonth(id) {
    if (this.monthsList.length > 1) {
      let data = this.monthsList.find(e => e.id == id);
      if (!data) {
        return '--';
      }
      return data.displayName;
    }
  }

  addDetails() {
    let obj = this.adhocExclusionAddEditForm.value;
    let finalArray = []
    obj.suburbId.forEach(suburb => {
      obj.debtorId.forEach(debtor => {
        finalArray.push({
          ...obj,
          suburbId: suburb,
          debtorId: debtor
        })
      });
    });
    this.priceIncreaseList.push(...finalArray)
  }

  bindingdefaultLocationDropdown() {
    let obj: any = {}
    if (this.adhocExclusionAddEditForm.value.contractTypeId) {
      obj.contractTypeId = this.adhocExclusionAddEditForm.value.contractTypeId
    }
    this.crudService
      .get(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.UX_CONTRACT_TYPE_SUBURBS,
        undefined,
        false,
        prepareRequiredHttpParams(obj)
      )
      .subscribe({
        next: (response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.defaultLocationList = getPDropdownData(response.resources);
        },
      });
  }

  bindingdeDebtorDropdown() {
    let obj: any = {}
    if (this.adhocExclusionAddEditForm.value.suburbId.length !=0) {
      obj.suburbIds = this.adhocExclusionAddEditForm.value.suburbId.join(',')
    }
    this.crudService
      .get(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.UX_CONTRACT_SUBURBS_DEBTORS,
        undefined,
        false,
        prepareRequiredHttpParams(obj)
      )
      .subscribe({
        next: (response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.debtorList = getPDropdownData(response.resources);
        },
      });
  }

  getAllSuburbs() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_SUBURBS
      )
      .subscribe({
        next: (response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.defaultLocationListAll = getPDropdownData(response.resources);
        },
      });
  }

  getAllDebtors() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        SalesModuleApiSuffixModels.UX_DEBTORS
      )
      .subscribe({
        next: (response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.debtorListAll = getPDropdownData(response.resources);
        },
      });
  }

  createPriceIncreaseForm(): void {
    let priceModel = {
      "contractTypeId": "",
      "suburbId": "",
      "debtorId": "",
      "monthId": "",
      "contractStartDate": "",
      "lastPriceIncreasetDate": ""
    }
    this.adhocExclusionAddEditForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.adhocExclusionAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
}
