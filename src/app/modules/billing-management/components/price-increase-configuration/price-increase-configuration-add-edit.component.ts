

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData, PriceIncreaseConfigModel, PriceIncreaseConfigSiteTypeAddEditModel } from '@modules/others';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { PriceIncreaseModalComponent } from './price-increase-sitye-type-modal.component';
@Component({
  selector: 'price-increase-configuration-add-edit',
  templateUrl: './price-increase-configuration-add-edit.component.html',
  styleUrls: ['./price-increase-configuration.component.scss']
})
export class PriceIncreaseConfigurationAddEditComponent implements OnInit {

  constructor(private crudService: CrudService, private dialog: MatDialog, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }
  priceIncreaseConfigForm: FormGroup;
  priceIncreaseConfigAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  divisionDropDown = [];
  regionList = [];
  districtList = [];
  branchList = [];
  siteTypeList = [];
  priceIncDailog = false;
  selectedOptions = [];
  businessAreaList = []
  stockIdDetails: any;
  adminFeeId: any;
  pricingIncreaseRunDateConfigId: any;
  priceIncreaseList = [];
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });

  ngOnInit() {
    this.createPriceIncreaseForm();
    this.createPriceIncreaseAddEditForm();
    this.getRegions();
    this.onFormControlChange();
  }

  getPriceIncreaseList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_CONFIG_SITE_TYPES, undefined, false,
      prepareRequiredHttpParams({ branchId: this.priceIncreaseConfigForm.value.branchIds }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceIncreaseList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createPriceIncreaseForm(): void {
    let priceModel = new PriceIncreaseConfigModel();
    this.priceIncreaseConfigForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.priceIncreaseConfigForm = setRequiredValidator(this.priceIncreaseConfigForm, ["regionId", "divisionIds", "districtIds", "branchIds","mainAreaIds"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createPriceIncreaseAddEditForm(): void {
    let priceModel = new PriceIncreaseConfigSiteTypeAddEditModel();
    this.priceIncreaseConfigAddEditForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess && response.statusCode == 200) {
            this.regionList = response.resources;
          }
        });
  }

  onFormControlChange() {
    this.priceIncreaseConfigForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          this.divisionDropDown = getPDropdownData(response.resources)
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseConfigForm.get('divisionIds').valueChanges.subscribe((divisionIds: string) => {
      if (!divisionIds) return;

      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionIds }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.districtList = getPDropdownData(response.resources)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseConfigForm.get('districtIds').valueChanges.subscribe((districtIds: string) => {
      if (!districtIds) return;

      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId: districtIds }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.branchList = getPDropdownData(response.resources)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })

      this.priceIncreaseConfigForm.get('branchIds').valueChanges.subscribe((branchIds: string) => {
        if (!branchIds) return;
        if (branchIds.length != 0) {
          this.getBusinessAreaList(branchIds)
        }
      });
    });
  }
  getBusinessAreaList(branchIds) {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_MAIN_AREA_BY_BRANCH,
      prepareGetRequestHttpParams(null, null,
        {
          branchIds: branchIds
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.businessAreaList = getPDropdownData(response.resources)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
  }

  getSelectedValue(e) {
    if (e.length > 0) {
      this.getPriceIncreaseList();
    }
  }

  removeRecord(data) {
    const message = `Do you want to delete?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.crudService.delete(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.PRICING_CONFIG_SITE_TYPES,
          '',
          prepareRequiredHttpParams({
            ids: data.pricingConfigSiteTypeId,
            modifiedUserId: this.loggedInUserData.userId,
            isDeleted: true
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.getPriceIncreaseList();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }

    });
  }

  addPriceIncrease() {
    if (this.priceIncreaseConfigForm.invalid) {
      return
    }
    let data = {
      branchIds: this.priceIncreaseConfigForm.value.branchIds,
      mainAreaIds: this.priceIncreaseConfigForm.value.mainAreaIds,
      createdUserId: this.loggedInUserData.userId
    }
    const dialogReff = this.dialog.open(PriceIncreaseModalComponent, { width: '680px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getPriceIncreaseList();
      }
    });
  }
}
