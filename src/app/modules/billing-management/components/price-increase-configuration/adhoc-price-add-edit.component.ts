

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { AdhocAddEditModalComponent } from './adhoc-edit-modal.component';

@Component({
  selector: 'adhoc-price-add-edit',
  templateUrl: './adhoc-price-add-edit.component.html'
})
export class AddhocPriceAddEditComponent implements OnInit {
  id: any;
  @Output() outputData = new EventEmitter<any>();
  details: any;
  adhocPriceAddEditForm: FormGroup;
  priceIncreaseConfigAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  divisionDropDown = [];
  regionList = [];
  districtList = [];
  branchList = [];
  siteTypeList = [];
  priceIncDailog = false;
  selectedOptions = [];
  adminFeeId: any;
  pricingIncreaseRunDateConfigId: any;
  stockIdDetails: any;
  priceIncreaseList = [];
  primengTableConfigProperties: any = {
    tableCaption: "Adhoc price increase 1 of 2",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration' },
    {displayName: 'Billing'},
    {displayName: 'Price Increase Configuration',relativeRouterUrl: '/billing/price-increase-configuration',queryParams:{tab:4}},
    {displayName: 'Adhoc price increase'}
  ],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Type Mapping',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
        }]
    }
  }
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog, private router: Router, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    this.createPriceIncreaseForm();
    this.getRegions();
    this.onFormControlChange();
    this.getPriceIncreaseList();
    this.rxjsService.setGlobalLoaderProperty(true);
  }

  getPriceIncreaseList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ADHOC_LIST, undefined, false,
      prepareRequiredHttpParams({ pricingFileNameId: this.id }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.details = response.resources
          this.adhocPriceAddEditForm.patchValue({
            regionId: response.resources.regionId
          });
          this.priceIncreaseList = response.resources.pricingIncreaseStockIdList;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createPriceIncreaseForm(): void {
    let priceModel = {
      'divisionIds': '',
      'regionId': '',
      'districtIds': ''
    }
    this.adhocPriceAddEditForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.adhocPriceAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.adhocPriceAddEditForm = setRequiredValidator(this.adhocPriceAddEditForm, ["regionId", "divisionIds"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.regionList = response.resources;
          }
        });
  }

  onFormControlChange() {
    this.adhocPriceAddEditForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources.length > 0 && response.isSuccess) {
            this.divisionDropDown = getPDropdownData(response.resources);
            if (this.details && this.details.divisionIds) {
              this.adhocPriceAddEditForm.patchValue({
                divisionIds: this.details.divisionIds
              });
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.adhocPriceAddEditForm.get('divisionIds').valueChanges.subscribe((divisionIds: string) => {
      if (!divisionIds) return;
      if (divisionIds.length == 0) {
        this.division = false;
        return;
      }
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionIds }))
        .subscribe((response: IApplicationResponse) => {
          this.districtList = [];
          if (response && response.resources && response.isSuccess) {
            let TmpArray = response.resources;
            for (let i = 0; i < TmpArray.length; i++) {
              let temp = {};
              temp['display'] = TmpArray[i].displayName;
              temp['value'] = TmpArray[i].id;
              this.districtList.push(temp);
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
  }

  cancel() {
  }

  removeRecord(data) {
    const message = `Do you want to delete?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.crudService.delete(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.ADHOC_LIST,
          '',
          prepareRequiredHttpParams({
            ids: data.pricingIncreaseStockId,
            modifiedUserId: this.loggedInUserData.userId,
            isDeleted: true
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.getPriceIncreaseList();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
  }

  openDialog(dt?: any) {
    if (!dt) {
      if (this.adhocPriceAddEditForm.value.divisionIds.length < 1) {
        this.division = true;
      }
      if (this.adhocPriceAddEditForm.invalid) {
        return
      }
    }
    let data = {
      details: dt ? dt : null,
      pricingFileNameId: this.id,
      districtList: this.districtList
    }
    const dialogReff = this.dialog.open(AdhocAddEditModalComponent, { width: '800px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getPriceIncreaseList();
      }
    });
  }

  getSelectedValue(e) {
    if (e.length > 0) {
      this.getPriceIncreaseList();
    }
  }

  district = false;
  division = false;

  next() {
    this.router.navigate(['/billing/price-increase-configuration/adhoc-exclusion/add-edit'], { queryParams: { id: this.id } });
  }

  addPriceIncrease() {
  }
}
