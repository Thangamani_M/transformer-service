import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ResponseMessageTypes } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControls, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'adhoc-edit-modal',
  templateUrl: './adhoc-edit-modal.component.html',
})
export class AdhocAddEditModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  leadId: string;
  adhocPriceAddEditModalForm: FormGroup;
  siteTypeList = [];
  adminFeeId: any;
  pricingIncreaseRunDateConfigId: any;
  loggedInUserData: LoggedInUserModel;
  priceDetails: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  districtList: any;
  selectedOptions: any = [];
  serviceList: any = [];
  stokIdDropdown: any = [];
  services = false
  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService,
    private dialog: MatDialog, private snackbarService: SnackbarService, private store: Store<AppState>) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createadhocPriceAddEditModalForm();
    if (!this.data.details) {
      this.getStockIdDropdown();
      this.getService();
    }
    this.districtList = getPDropdownData(this.data.districtList,"display","value");
    if (this.data.details) {
      this.adhocPriceAddEditModalForm.patchValue(this.data.details);
    }
  }


  getStockIdDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_STOCK_ID, null, false, prepareRequiredHttpParams({approved:true}))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stokIdDropdown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getService() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICES, null, false,
      prepareGetRequestHttpParams(undefined, null, null))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          if (this.data.pricingConfigServiceId) {
            this.serviceList = response.resources
          } else {
            this.serviceList = getPDropdownData(response.resources)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createadhocPriceAddEditModalForm(): void {
    let priceModel = {
      "pricingFileNameId": this.data.pricingFileNameId,
      "createdUserId": this.loggedInUserData.userId,
      "modifiedUserId": this.loggedInUserData.userId,
      "districtIds": "",
      "stockId": "",
      "serviceIds": "",
      "priceIncreasePercentage": "",
      "priceIncreaseAmount": "",
      "pricingIncreaseStockId": ""
    };
    this.adhocPriceAddEditModalForm = this.formBuilder.group({
    });
    Object.keys(priceModel).forEach((key) => {
      this.adhocPriceAddEditModalForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.adhocPriceAddEditModalForm = setRequiredValidator(this.adhocPriceAddEditModalForm, ["districtIds", "stockId","serviceIds"]);
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.data.details) {
      this.adhocPriceAddEditModalForm = removeFormControls(this.adhocPriceAddEditModalForm, ["serviceIds", "districtIds", "stockId"]);
    }
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_FILES_NAMES, this.data.pricingFileNameId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceDetails = response.resources;
          this.adhocPriceAddEditModalForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }


  onSubmit() {

    if (!this.adhocPriceAddEditModalForm.value.priceIncreasePercentage && !this.adhocPriceAddEditModalForm.value.priceIncreaseAmount) {
      this.snackbarService.openSnackbar("Select Either one among percentage or amount", ResponseMessageTypes.WARNING);
      return;
    } else if (this.adhocPriceAddEditModalForm.value.priceIncreasePercentage && this.adhocPriceAddEditModalForm.value.priceIncreaseAmount) {
      this.snackbarService.openSnackbar("Select Only one among percentage or amount", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.adhocPriceAddEditModalForm.invalid) {
      return
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.data.details) {
      this.adhocPriceAddEditModalForm.get("pricingIncreaseStockId").setValue(this.data.details.pricingIncreaseStockId);
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ADHOC_PRICE_INCREASE, this.adhocPriceAddEditModalForm.value).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.outputData.emit(true);
          this.dialog.closeAll();
          this.rxjsService.setDialogOpenProperty(false);
        }
      })
    } else {
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ADHOC_PRICE_INCREASE, this.adhocPriceAddEditModalForm.value).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.outputData.emit(true);
          this.dialog.closeAll();
          this.rxjsService.setDialogOpenProperty(false);
        }
      })
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
