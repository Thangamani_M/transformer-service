import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BillingModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { FileListAddEditModalComponent } from './file-list-modal.component';
import { PriceIncreaseExclusionModalComponent } from './price-increase-config-exclusions/price-increase-config-exclusions-edit-modal.component';
import { PriceIncreaseServiceCategoryModalComponent } from './price-increase-config-service-category';
import { PriceIncreaseModalComponent } from './price-increase-sitye-type-modal.component';
import { PriceIncreaseDateModalComponent } from './price-increse-date-config/price-increase-date-modal.component';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'price-increase-configuration',
  templateUrl: './price-increase-configuration.component.html',
  styleUrls: ['./price-increase-configuration.component.scss']
})

export class PriceIncreaseConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {
  listSubscription: any;
  primengTableConfigProperties: any = {
    tableCaption: "Price Increase Configuration",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Price Increase Configuration', relativeRouterUrl: '' }, { displayName: 'billing/price-increase-configuration' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Site Type Range',
          dataKey: 'pricingConfigSiteTypeId',
          enableBreadCrumb: true,
          enableAction: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          enableAddActionBtn: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'regionName', header: 'Region', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'districtName', header: 'District', width: '200px' },
            { field: 'branchName', header: 'Branch', width: '150px' },
            { field: 'siteTypeName', header: 'Site Type', width: '150px' },
            { field: 'priceLimitDescription', header: 'Limit Desc', width: '150px' },
            { field: 'lowerLimit', header: 'Lower Value', width: '150px' },
            { field: 'upperLimit', header: 'Higher Value', width: '150px' },
            { field: 'priceIncreasePercentage', header: 'Price Increase %', width: '150px' },
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.PRICING_CONFIG_SITE_TYPES,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true,
        },
        {
          caption: 'Service Category',
          dataKey: 'pricingConfigServiceId',
          enableBreadCrumb: true,
          enableAction: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'regionName', header: 'Region', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'districtName', header: 'District', width: '200px' },
            { field: 'branchName', header: 'Branch', width: '150px' },
            { field: 'siteTypeName', header: 'Site Type', width: '150px' },
            { field: 'serviceName', header: 'Service Category', width: '150px' },
            { field: 'priceIncreasePercentage', header: 'Price Increase %', width: '150px' }
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.PRICING_CONFIG_SERVICE_CATEGORY,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true,

        },
        {
          caption: 'Price Increase Exclusions',
          dataKey: 'pricingIncreaseExclusionId',
          enableBreadCrumb: true,
          enableAction: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'regionName', header: 'Region', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'districtName', header: 'District', width: '200px' },
            { field: 'branchName', header: 'Branch', width: '200px' },
            { field: 'siteTypeName', header: 'Site Type', width: '200px' },
            { field: 'categoryName', header: 'Service Category', width: '200px' },
            { field: 'priceIncreasePercentage', header: 'Price Increase %', width: '200px' },
            { field: 'branchManagerName', header: 'Branch Manager Name', width: '200px' },
            { field: 'businessAreaName', header: 'Business Area Name', width: '200px' },
            { field: 'contractTypeName', header: 'Contract Type Name', width: '200px' },
            { field: 'contractStartDate', header: 'Contract Start Date', width: '200px' , isDate:true},
            { field: 'debtorRefNo', header: 'Debtor Ref No.', width: '200px' },
            { field: 'lastPriceIncreasetDate', header: 'Last Price Increase Date', width: '200px' ,isDate:true},
            { field: 'monthName', header: 'Month Name', width: '200px' },
            { field: 'priceLowerLimit', header: 'Price Lower Limit', width: '200px' },
            { field: 'priceUpperLimit', header: 'Price Upper Limit', width: '200px' },
            { field: 'stockIdName', header: 'StockId Name', width: '200px' },
            { field: 'subAreaName', header: 'SubArea Name', width: '200px' },
            { field: 'suburbName', header: 'Suburb Name', width: '200px' }
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.PRICING_INCREASE_EXCLUSIONS,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true,

        },
        {
          caption: 'Price Increase Date',
          dataKey: 'pricingIncreaseMonthId',
          enableBreadCrumb: true,
          enableAction: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'regionName', header: 'Region', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'districtName', header: 'District', width: '200px' },
            { field: 'branchName', header: 'Branch', width: '150px' },
            { field: 'monthName', header: 'Month Name', width: '150px' },
            { field: 'increasePercentage', header: 'Increase Amount', width: '150px' }
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.PRICING_INCREASE_MONTHS,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true,
        },
        {
          caption: 'File List',
          dataKey: 'pricingFileNameId',
          enableBreadCrumb: true,
          enableAction: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableStatusActiveAction: false,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'pricingFileName', header: 'File Name', width: '200px' },
            { field: 'pricingFileCategoryName', header: 'File Category', width: '200px' },
            { field: 'createdBy', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '150px', isDate:true },
            { field: 'modifiedBy', header: 'Actioned By', width: '150px' },
            { field: 'modifiedDate', header: 'Actioned On', width: '150px',isDate:true },
            { field: 'pricingFileStatusName', header: 'File Status', width: '150px', isStatus: true, statusKey: '' }//status key is the class name for css
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.PRICING_FILES_NAMES,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true,
        },
      ]
    }
  }

  constructor(private crudService: CrudService, private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService, private activatedRoute: ActivatedRoute,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private datePipe: DatePipe, private router: Router, private dialog: MatDialog,
    private snackbarService: SnackbarService
  ) {
    super();
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Price Increase Configuration', relativeRouterUrl: '/billing/price-increase-configuration' },
      { displayName: this.selectedTabIndex == 0 ? 'Site Type Range' : this.selectedTabIndex == 1 ? 'Category' : this.selectedTabIndex == 2 ? 'Price Increase Exclusions' : 'Price Increase Dates', relativeRouterUrl: '/billing/price-increase-configuration' }]
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.PRICE_INCREASE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscription && !this.listSubscription?.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onTabChange(e) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    this.router.navigate(['/billing/price-increase-configuration'], { queryParams: { tab: this.selectedTabIndex } });
    this.getRequiredListData();
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Price Increase Configuration', relativeRouterUrl: '/billing/price-increase-configuration' },
    { displayName: this.selectedTabIndex == 0 ? 'Site Type Range' : this.selectedTabIndex == 1 ? 'Service Category' : this.selectedTabIndex == 2 ? 'Price Increase Exclusions' : 'Price Increase Dates', relativeRouterUrl: '/billing/price-increase-configuration' }]
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          this.router.navigate(['/billing/price-increase-configuration/add-edit']);
        } else if (this.selectedTabIndex == 1) {
          this.router.navigate(['/billing/price-increase-configuration/service-category/add-edit']);
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(['/billing/price-increase-configuration/exclusions/add-edit']);
        }
        else if (this.selectedTabIndex == 3) {
          this.router.navigate(['/billing/price-increase-configuration/date-config/add-edit']);
        }
        else if (this.selectedTabIndex == 4) {
          const dialogReff = this.dialog.open(FileListAddEditModalComponent, { width: '500px', disableClose: true, });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {
            if (ele) {
              this.getRequiredListData();
            }
          });
        }
        else {
        }
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        let isEdit = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit
        if (this.selectedTabIndex == 0) {
          let data = {
            pricingConfigSiteTypeId: row['pricingConfigSiteTypeId'],
            isEdit:isEdit
          }
          const dialogReff = this.dialog.open(PriceIncreaseModalComponent, { width: '680px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {
            if (ele) {
              this.getRequiredListData();
            }
          });
        } else if (this.selectedTabIndex == 1) {
          let data = {
            pricingConfigServiceId: row['pricingConfigServiceId'],
            isEdit:isEdit
          }
          const dialogReff = this.dialog.open(PriceIncreaseServiceCategoryModalComponent, { width: '680px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {
            if (ele) {
              this.getRequiredListData();
            }
          });
        } else if (this.selectedTabIndex == 2) {
          let data = {
            pricingIncreaseExclusionId: row['pricingIncreaseExclusionId'],
            isEdit:isEdit
          }
          const dialogReff = this.dialog.open(PriceIncreaseExclusionModalComponent, { width: '720px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {
            if (ele) {
              this.getRequiredListData();
            }
          });
        }
        else if (this.selectedTabIndex == 4) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          if (row['pricingFileCategoryId'] == 1) {
            this.router.navigate(['/billing/price-increase-configuration/monthly-config/add-edit'], { queryParams: { id: row['pricingFileNameId'], configId: row['pricingFileCategoryId'], status: row['pricingFileStatusName'] } });
          } else {
            this.router.navigate(['/billing/price-increase-configuration/adhoc-config/add-edit'], { queryParams: { id: row['pricingFileNameId'] } });
          }
        }
        else {
          let data = {
            pricingIncreaseMonthId: row['pricingIncreaseMonthId'],
            isEdit:isEdit
          }
          const dialogReff = this.dialog.open(PriceIncreaseDateModalComponent, { width: '680px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {
            if (ele) {
              this.getRequiredListData();
            }
          });
        }
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, [row],
          this.primengTableConfigProperties)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
    }
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }
}
