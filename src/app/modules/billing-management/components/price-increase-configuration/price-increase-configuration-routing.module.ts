import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddhocExclusionAddEditComponent } from './adhoc-exclusion-add-edit.component';
import { AddhocPriceAddEditComponent } from './adhoc-price-add-edit.component';
import { MonthlyPriceAddEditComponent } from './monthly-price-add-edit.component';
import { PriceIncreaseConfigExclusionsAddEditComponent } from './price-increase-config-exclusions';
import { PriceIncreaseConfigServiceCategoryAddEditComponent } from './price-increase-config-service-category';
import { PriceIncreaseConfigurationAddEditComponent } from './price-increase-configuration-add-edit.component';
import { PriceIncreaseConfigurationListComponent } from './price-increase-configuration.component';
import { PriceIncreaseDateConfigAddEditComponent } from './price-increse-date-config/price-increase-date-config-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [

  { path: '', component: PriceIncreaseConfigurationListComponent, canActivate:[AuthGuard],data: { title: 'Price Increase Configuration List' } },
  { path: 'add-edit', component: PriceIncreaseConfigurationAddEditComponent,canActivate:[AuthGuard], data: { title: 'Price Increase Configuration Add Edit' } },
  { path: 'service-category/add-edit', component: PriceIncreaseConfigServiceCategoryAddEditComponent, canActivate:[AuthGuard],data: { title: 'Price Increase Configuration Service category Add Edit' } },
  { path: 'date-config/add-edit', component: PriceIncreaseDateConfigAddEditComponent, canActivate:[AuthGuard],data: { title: 'Price Increase Date Configuration Add Edit' } },
  { path: 'monthly-config/add-edit', component: MonthlyPriceAddEditComponent,canActivate:[AuthGuard], data: { title: 'Price Increase Date Configuration Add Edit' } },
  { path: 'exclusions/add-edit', component: PriceIncreaseConfigExclusionsAddEditComponent, canActivate:[AuthGuard],data: { title: 'Price Increase Exclusions Configuration Add Edit' } },
  { path: 'adhoc-exclusion/add-edit', component: AddhocExclusionAddEditComponent,canActivate:[AuthGuard], data: { title: 'Price Increase Date Configuration Add Edit' } },
  { path: 'adhoc-config/add-edit', component: AddhocPriceAddEditComponent,canActivate:[AuthGuard], data: { title: 'Price Increase Date Configuration Add Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class PriceIncreaseConfigurationRoutingModule { }
