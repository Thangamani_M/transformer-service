import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AdhocAddEditModalComponent } from './adhoc-edit-modal.component';
import { AddhocExclusionAddEditComponent } from './adhoc-exclusion-add-edit.component';
import { AddhocPriceAddEditComponent } from './adhoc-price-add-edit.component';
import { FileListAddEditModalComponent } from './file-list-modal.component';
import { MonthlyPriceAddEditComponent } from './monthly-price-add-edit.component';
import { PriceIncreaseConfigExclusionsAddEditComponent } from './price-increase-config-exclusions';
import { PriceIncreaseExclusionModalComponent } from './price-increase-config-exclusions/price-increase-config-exclusions-edit-modal.component';
import { PriceIncreaseConfigServiceCategoryAddEditComponent, PriceIncreaseServiceCategoryModalComponent } from './price-increase-config-service-category';
import { PriceIncreaseConfigurationAddEditComponent } from './price-increase-configuration-add-edit.component';
import { PriceIncreaseConfigurationRoutingModule } from './price-increase-configuration-routing.module';
import { PriceIncreaseConfigurationListComponent } from './price-increase-configuration.component';
import { PriceIncreaseModalComponent } from './price-increase-sitye-type-modal.component';
import { PriceIncreaseDateConfigAddEditComponent } from './price-increse-date-config/price-increase-date-config-add-edit.component';
import { PriceIncreaseDateModalComponent } from './price-increse-date-config/price-increase-date-modal.component';
@NgModule({
  declarations: [PriceIncreaseConfigurationListComponent,PriceIncreaseModalComponent, PriceIncreaseConfigurationAddEditComponent,
    PriceIncreaseConfigServiceCategoryAddEditComponent, PriceIncreaseServiceCategoryModalComponent, PriceIncreaseDateConfigAddEditComponent, PriceIncreaseDateModalComponent,FileListAddEditModalComponent,AdhocAddEditModalComponent,
    MonthlyPriceAddEditComponent,AddhocPriceAddEditComponent,AddhocExclusionAddEditComponent, PriceIncreaseConfigExclusionsAddEditComponent, PriceIncreaseExclusionModalComponent ],
  imports: [
    CommonModule,
    PriceIncreaseConfigurationRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ],
  entryComponents:[PriceIncreaseModalComponent, PriceIncreaseServiceCategoryModalComponent, PriceIncreaseDateModalComponent,FileListAddEditModalComponent,
    AdhocAddEditModalComponent, PriceIncreaseExclusionModalComponent]
})
export class PriceIncreaseConfigurationModule { }
