

import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AdhocPriceIncreaseModel, PriceIncreseModel } from '@modules/others/configuration/models/same-day-test-run.model';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'monthly-price-add-edit',
  templateUrl: './monthly-price-add-edit.component.html',
  styleUrls: ['./monthly-price-add-edit.component.scss']
})
export class MonthlyPriceAddEditComponent implements OnInit {
  id: any;
  @Output() outputData = new EventEmitter<any>();
  details: any;
  configId: any;
  exclusionTypes: any;
  pricingFileNameDetails: FormArray;
  isDisabled = true;
  @ViewChild('ttDD',{static:false}) table: any;
  totalRecords = 0
  pageLimit: any = [50,100, 75, 100];
  isProcessed = false
  primengTableConfigProperties:any
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private httpCancelService: HttpCancelService, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private router : Router) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.configId = this.activatedRoute.snapshot.queryParams.configId;
    let processed = this.activatedRoute.snapshot.queryParams.status;
    if(processed =="Processed"){
      this.isProcessed = true;
    }
    this.primengTableConfigProperties = {
      tableCaption: `${this.configId ==1?'Monthly':'Adhoc'} price increase`,
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration' },
      {displayName: 'Billing'},
      {displayName: 'Price Increase Configuration',relativeRouterUrl: '/billing/price-increase-configuration',queryParams:{tab:4}},
      {displayName: `${this.configId ==1?'Monthly':'Adhoc'} price increase`}
    ],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Credit Controller Type Mapping',
            dataKey: 'creditControllerTypeId',
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn:true
          }]
      }
    }
  }
  monthlyPriceAddEditForm: FormGroup;
  priceIncreaseConfigAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  divisionDropDown = [];
  regionList = [];
  districtList = [];
  branchList = [];
  siteTypeList = [];
  priceIncDailog = false;
  selectedOptions = [];
  adminFeeId: any;
  pricingIncreaseRunDateConfigId: any;
  stockIdDetails: any;
  priceIncreaseList = [];
  loading = false;
  loadingIndex = 0
  priceDetails = []
  ngOnInit() {
    this.createPriceIncreaseForm();
    this.getExclusionType();
    this.getPriceIncreaseList();
    this.rxjsService.setGlobalLoaderProperty(true);
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    if(!this.isProcessed){
      this.next('draft');
    }
    this.getPriceIncreaseList(row['pageIndex'],row["pageSize"])
  }

  getPriceIncreaseList(pageIndex?:string, pageSize?:string) {
    pageSize = pageSize?pageSize:'50'
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_FILE_NAME_DETAILS, undefined, false,
      prepareGetRequestHttpParams(pageIndex,pageSize,{
        pricingFileNameId: this.id,
        pricingFileCategoryId: this.configId,
        createdUserId: this.loggedInUserData.userId,
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.details = response.resources
          this.totalRecords =response.totalCount
          this.pricingFileNameDetails = this.getPricingFileNameDetails;
          if (response.resources.length > 0) {
            let data = response.resources
            this.priceDetails = data;
            data.map(item => {
              this.pricingFileNameDetails.push(this.createServiceItemList(item));
            })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }else{
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }
  isEdit = true;
   onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
          this.edit();
          break;
      }
  }

  edit() {
    this.isEdit = false;
  }

  createServiceItemList(serviceListModel?: PriceIncreseModel): FormGroup {
    let servicesModel = new PriceIncreseModel(serviceListModel);
    let formControls = {};
    Object.keys(servicesModel).forEach((key) => {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  createPriceIncreaseForm(): void {
    let priceModel = new AdhocPriceIncreaseModel()
    this.monthlyPriceAddEditForm = this.formBuilder.group({
      pricingFileNameDetails: this.formBuilder.array([])
    });
    Object.keys(priceModel).forEach((key) => {
      this.monthlyPriceAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.monthlyPriceAddEditForm.get("pricingFileNameId").setValue(this.id);
    this.monthlyPriceAddEditForm.get("modifiedUserId").setValue(this.loggedInUserData.userId);
  }

  getExclusionType() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_EXCLUSION_TYPES, undefined, true,
      null).subscribe(res => {
        this.exclusionTypes = res.resources;
      })
  }

  get getPricingFileNameDetails(): FormArray {
    if (!this.monthlyPriceAddEditForm) return;
    return this.monthlyPriceAddEditForm.get("pricingFileNameDetails") as FormArray;
  }

  cancel() {}

  removeRecord(data) {
    const message = `Do you want to delete?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.crudService.delete(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.ADHOC_LIST,
          '',
          prepareRequiredHttpParams({
            ids: data.pricingIncreaseStockId,
            modifiedUserId: this.loggedInUserData.userId,
            isDeleted: true
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.getPriceIncreaseList();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
  }

  openDialog(dt?: any) {}

  getSelectedValue(e) {
    if (e.length > 0) {
      this.getPriceIncreaseList();
    }
  }

  district = false;
  division = false;

  next(str) {
    if (this.monthlyPriceAddEditForm.invalid) {
      return;
    }

    if (str == 'draft') {
      this.monthlyPriceAddEditForm.get('isDrafted').setValue(true)
    } else {
      this.monthlyPriceAddEditForm.get('isDrafted').setValue(false)
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_FILE_NAME_DETAILS, this.monthlyPriceAddEditForm.value).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if(str=="save"){
          this.router.navigate(['/billing/price-increase-configuration'], {queryParams:{tab:4}})
        }
      }
    })
  }

  addPriceIncrease() { }
  ngAfterViewInit() { }
}
