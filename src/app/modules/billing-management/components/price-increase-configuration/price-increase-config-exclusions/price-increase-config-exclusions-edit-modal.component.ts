import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams } from '@app/shared/utils';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData, PriceIncreaseConfigExclusionsAddEditModel } from '@modules/others';
import { Store } from '@ngrx/store';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@sales/shared';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'price-increase-config-exclusions-edit-modal',
  templateUrl: './price-increase-config-exclusions-edit-modal.component.html',
})
export class PriceIncreaseExclusionModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  priceIncreaseExclusionForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  priceDetails: any;
  debtorCodeId: any;
  businessAreaList = [];
  bussinessAreaId: any;
  subAreaList = [];
  subAreaId: any;
  categoryList = [];
  originList = [];
  subCategoryList = [];
  installOriginList = [];
  stockIdList = [];
  debtorList = [];
  debtorKeyUp = false;
  businessAreaKeyUp = false;
  subAreaKeyUp = false;
  contractTypes = [{ value: 1, display: 'Service Agreement' }, { value: 2, display: 'Installation Agreement' }]
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isDisabled = false;
  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService,
    private snackbarService : SnackbarService,
    private dialog: MatDialog, private store: Store<AppState>) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createPriceIncreaseConfigExclutionForm();
    this.onFormControlChanges();
    this.getDetails();
    this.getDebtorList();
    this.getSubAreaList();
    this.getCategoryList();
    this.getOriginList();
    this.getSubCategoryList();
    this.getStockIdList();
    this.getInstallOriginList();
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_INCREASE_EXCLUSIONS, this.data.pricingIncreaseExclusionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceDetails = response.resources;
          this.priceIncreaseExclusionForm.patchValue(response.resources);
          this.priceIncreaseExclusionForm.controls['debtorId'].setValue(response.resources?.debtorRefNo);
          this.priceIncreaseExclusionForm.controls['businessAreaId'].setValue(response.resources.businessAreaName);
          this.priceIncreaseExclusionForm.controls['subAreaId'].setValue(response.resources.subAreaName);
          this.isDisabled = true;
        }
        this.rxjsService.setDialogOpenProperty(false);
      })
  }

  createPriceIncreaseConfigExclutionForm() {
    let stageOneModel = new PriceIncreaseConfigExclusionsAddEditModel();
    this.priceIncreaseExclusionForm = this.formBuilder.group({});
    Object.keys(stageOneModel).forEach((key) => {
      this.priceIncreaseExclusionForm.addControl(key, new FormControl(stageOneModel[key]));
    });
    this.priceIncreaseExclusionForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.priceIncreaseExclusionForm.get('pricingIncreaseExclusionId').setValue(this.data.pricingIncreaseExclusionId);
  }

  getDebtorList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_DEBTORS,
      prepareGetRequestHttpParams(null, null,
        {
          divisionId: this.priceIncreaseExclusionForm.value.divisionId,
          districtId: this.priceIncreaseExclusionForm.value.districtId,
          branchId: this.priceIncreaseExclusionForm.value.branchId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.debtorList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
  }

  getBusinessAreaList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_BUSINESS_AREA,
      prepareGetRequestHttpParams(null, null,
        {
          divisionId: this.priceIncreaseExclusionForm.value.divisionId,
          districtId: this.priceIncreaseExclusionForm.value.districtId,
          branchId: this.priceIncreaseExclusionForm.value.branchId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.businessAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
  }


  getSubAreaList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA_SEARCH,
      prepareGetRequestHttpParams(null, null,
        {
          divisionId: this.priceIncreaseExclusionForm.value.divisionId,
          districtId: this.priceIncreaseExclusionForm.value.districtId,
          branchId: this.priceIncreaseExclusionForm.value.branchId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.subAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
  }

  getCategoryList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CATEGORY,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.categoryList = response.resources;
        }
      });
  }

  getOriginList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_ORIGINS,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.originList = response.resources;
        }
      });
  }

  getInstallOriginList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.installOriginList = response.resources;
        }
      });
  }

  getSubCategoryList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_SUBCATEGORY,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.subCategoryList = response.resources;
        }
      });
  }

  getStockIdList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_STOCK_ID,
      prepareGetRequestHttpParams(null, null, {
        approved: true
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.stockIdList = response.resources;
        }
      });
  }

  onFormControlChanges() {
    this.priceIncreaseExclusionForm.get('debtorId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val) {
          this.debtorList = [];
          if (val.length >= 1) {
            if (!this.priceIncreaseExclusionForm.value.debtorId) {
              return
            } else {
              if (!this.debtorKeyUp) {
                return
              } else {
                return this.filterDebtorDetailsBySearchOptions(val);
              }
            }
          }
        } else {
          this.debtorList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources.length === 0) {
          }
          if (response.resources != null) {
            this.debtorList = response.resources;
            if (this.debtorList.length == 0) {
              this.debtorList = [];
            }
          } else {
            this.debtorList = [];
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.priceIncreaseExclusionForm.get('businessAreaId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val) {
          this.businessAreaList = [];
          if (val.length >= 1) {
            if (!this.priceIncreaseExclusionForm.value.businessAreaId) {
              return
            } else {
              if (!this.businessAreaKeyUp) {
                return
              } else {
                return this.filterBusinessAreaDetailsBySearchOptions(val, this.priceIncreaseExclusionForm.value.branchId);
              }
            }
          } else {
          }

        } else {
          this.businessAreaList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources.length === 0) {
          }
          if (response.resources != null) {
            this.businessAreaList = response.resources;
            if (this.businessAreaList.length == 0) {
              this.businessAreaList = [];
            }
          } else {
            this.businessAreaList = [];
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.priceIncreaseExclusionForm.get('subAreaId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val) {
          this.subAreaList = [];
          if (val.length >= 1) {
            if (!this.priceIncreaseExclusionForm.value.subAreaId) {
              return
            } else {
              if (!this.subAreaKeyUp) {
                return
              } else {
                return this.filterSubAreaDetailsBySearchOptions(val);
              }
            }
          }
        } else {
          this.subAreaList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources.length === 0) {
          }
          if (response.resources != null) {
            this.subAreaList = response.resources;
            if (this.subAreaList.length == 0) {
              this.subAreaList = [];
            }
          } else {
            this.subAreaList = [];
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.priceIncreaseExclusionForm.get('priceLowerLimit').valueChanges.subscribe((priceLowerLimit: string) => {
      if (!priceLowerLimit) return;
      this.priceIncreaseExclusionForm.get('priceUpperLimit').setValue(9999999);
    });
  }

  filterDebtorDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_DEBTORS, null, true, prepareGetRequestHttpParams(null, null, {
        search: serachValue
      }))
  }

  filterBusinessAreaDetailsBySearchOptions(serachValue?: string, branchId?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_BUSINESS_AREA, null, true, prepareGetRequestHttpParams(null, null, {
        search: serachValue,
        branchId: branchId
      }))
  }

  filterSubAreaDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
        search: serachValue
      }))
  }

  onSelectedDebtorCode(value, cdm, id) {
    this.priceIncreaseExclusionForm.get('debtorId').setValue(cdm.displayName);
    this.debtorCodeId = id;
  }

  onSelectedBusinessArea(value, cdm, id) {
    this.priceIncreaseExclusionForm.get('businessAreaId').setValue(cdm.displayName);
    this.bussinessAreaId = id;
  }

  onSelectedSubArea(value, cdm, id) {
    this.priceIncreaseExclusionForm.get('subAreaId').setValue(cdm.displayName);
    this.subAreaId = id;
  }

  onSubmit() {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.priceIncreaseExclusionForm.invalid) {
      return;
    }
    this.priceIncreaseExclusionForm.get('debtorId').setValue(this.debtorCodeId);
    this.priceIncreaseExclusionForm.get('businessAreaId').setValue(this.bussinessAreaId);
    this.priceIncreaseExclusionForm.get('subAreaId').setValue(this.subAreaId);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_INCREASE_EXCLUSIONS, this.priceIncreaseExclusionForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.outputData.emit(true);
        this.dialog.closeAll();
        this.rxjsService.setDialogOpenProperty(false);
      }
    })
  }

  onDebtorKeyUp(e) {
    if (e.isTrusted) {
      this.debtorKeyUp = true;
    }
  }

  onBusinessAreaKeyUp(e) {
    if (e.isTrusted) {
      this.businessAreaKeyUp = true;
    }
  }

  editPage() {
    if(!this.data?.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isDisabled = false
  }

  onSubAreaKeyUp(e) {
    if (e.isTrusted) {
      this.subAreaKeyUp = true;
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
