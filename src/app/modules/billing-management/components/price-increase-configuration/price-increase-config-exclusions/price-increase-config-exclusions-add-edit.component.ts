import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { PriceIncreaseConfigExclusionsAddEditModel } from '@modules/others/configuration';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'price-increase-config-exclusions-add-edit',
  templateUrl: './price-increase-config-exclusions-add-edit.component.html',
  styleUrls: ['./price-increase-config-exclusions-add-edit.component.scss']
})

export class PriceIncreaseConfigExclusionsAddEditComponent {
  customerStatusTypes = [];
  @Input() fromUrl = '';
  priceIncreaseExclusionForm: FormGroup;
  pricingIncreaseExclusionId: any;
  regions = [];
  divisions = [];
  districts = [];
  branches = [];
  debtorList = [];
  debtorCodeId: any;
  businessAreaList = [];
  bussinessAreaId: any;
  subAreaList = [];
  subAreaId: any;
  categoryList = [];
  originList = [];
  subCategoryList = [];
  installOriginList = [];
  stockIdList = [];
  selectedIndex = 0;
  loggedUser: any;
  primengTableConfigProperties:any
  contractTypes = [{ value: 1, display: 'Service Agreement' }, { value: 2, display: 'Installation Agreement' }]
  isSearchBtnDisabled: boolean;
  countryCodeList = [{ displayName: '+27' }, { displayName: '+91' }, { displayName: '+45' }];
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  validateInputConfig = new CustomDirectiveConfig();
  alphanumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private store: Store<AppState>, private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.activatedRoute.queryParams.subscribe(params => {
      this.pricingIncreaseExclusionId = params.pricingIncreaseExclusionId;
    });
    this.primengTableConfigProperties= {
      tableCaption: "Price Increase Config - Exclusions",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' },
      { displayName: 'Billing ', relativeRouterUrl: '' },
        { displayName: 'Price Increase Configuration ', relativeRouterUrl: '/billing/price-increase-configuration', queryParams:{tab:2} },{
        displayName: 'Exclusions'
      }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Price Increase Config - Exclusions',
            dataKey: 'creditControllerTypeId',
            enableBreadCrumb: true,
            enableAction: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.createPriceIncreaseConfigExclutionForm();
    this.getRegions();
    this.getDebtorList();
    this.getSubAreaList();
    this.getCategoryList();
    this.getOriginList();
    this.getSubCategoryList();
    this.getStockIdList();
    this.onFormControlChanges();
    this.getInstallOriginList();
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { countryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.regions = response.resources;
          }
        })
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createPriceIncreaseConfigExclutionForm() {
    let stageOneModel = new PriceIncreaseConfigExclusionsAddEditModel();
    this.priceIncreaseExclusionForm = this.formBuilder.group({});
    Object.keys(stageOneModel).forEach((key) => {
      this.priceIncreaseExclusionForm.addControl(key, new FormControl(stageOneModel[key]));
    });
    this.priceIncreaseExclusionForm.get('createdUserId').setValue(this.loggedUser.userId)
  }

  getDebtorList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_DEBTORS,
      prepareGetRequestHttpParams(null, null,
        {
          divisionId: this.priceIncreaseExclusionForm.value.divisionId,
          districtId: this.priceIncreaseExclusionForm.value.districtId,
          branchId: this.priceIncreaseExclusionForm.value.branchId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.debtorList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
  }

  getBusinessAreaList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_BUSINESS_AREA,
      prepareGetRequestHttpParams(null, null,
        {
          divisionId: this.priceIncreaseExclusionForm.value.divisionId,
          districtId: this.priceIncreaseExclusionForm.value.districtId,
          branchId: this.priceIncreaseExclusionForm.value.branchId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.businessAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
  }

  getSubAreaList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA_SEARCH,
      prepareGetRequestHttpParams(null, null,
        {
          divisionId: this.priceIncreaseExclusionForm.value.divisionId,
          districtId: this.priceIncreaseExclusionForm.value.districtId,
          branchId: this.priceIncreaseExclusionForm.value.branchId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.subAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
  }


  getCategoryList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CATEGORY,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.categoryList = response.resources;
        }
      });
  }

  getOriginList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_ORIGINS,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.originList = response.resources;
        }
      });
  }

  getInstallOriginList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.installOriginList = response.resources;
        }
      });
  }

  getSubCategoryList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_SUBCATEGORY,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.subCategoryList = response.resources;
        }
      });
  }

  getStockIdList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_STOCK_ID,
      prepareGetRequestHttpParams(null, null, {
        approved: true
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.stockIdList = response.resources;
        }
      });
  }


  filterDebtorDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_DEBTORS, null, true, prepareGetRequestHttpParams(null, null, {
        debtorRefNo: serachValue
      }))
  }

  filterBusinessAreaDetailsBySearchOptions(serachValue?: string, branchId?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_BUSINESS_AREA, null, true, prepareGetRequestHttpParams(null, null, {
        search: serachValue,
        branchId: branchId
      }))
  }

  filterSubAreaDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
        search: serachValue
      }))
  }


  onSelectedDebtorCode(value, cdm, id) {
    this.priceIncreaseExclusionForm.get('debtorId').setValue(cdm.id);
    this.debtorCodeId = id;
  }

  onSelectedBusinessArea(value, cdm, id) {
    this.priceIncreaseExclusionForm.get('businessAreaId').setValue(cdm.id);
    this.bussinessAreaId = id;
  }

  onSelectedSubArea(value, cdm, id) {
    this.priceIncreaseExclusionForm.get('subAreaId').setValue(cdm.id);
    this.subAreaId = id;
  }

  changeDebtorCode() {
    this.priceIncreaseExclusionForm.get('debtorName').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val) {
          this.debtorList = [];
          if (val.length >= 1) {
            return this.filterDebtorDetailsBySearchOptions(val);
          }
        } else {
          this.debtorList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources.length === 0) {
          }
          if (response.resources != null) {
            this.debtorList = response.resources;
            if (this.debtorList.length == 0) {
              this.debtorList = [];
            }
          } else {
            this.debtorList = [];
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onFormControlChanges() {
    this.priceIncreaseExclusionForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.divisions = [];
      this.districts = [];
      this.branches = [];
      this.priceIncreaseExclusionForm.get('districtId').setValue('');
      this.priceIncreaseExclusionForm.get('divisionId').setValue('');
      this.priceIncreaseExclusionForm.get('branchId').setValue('');
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.divisions = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseExclusionForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.districts = [];
      this.branches = [];
      this.priceIncreaseExclusionForm.get('districtId').setValue('');
      this.priceIncreaseExclusionForm.get('branchId').setValue('');
      this.getDebtorList();
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.districts = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseExclusionForm.get('districtId').valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      this.branches = [];
      this.priceIncreaseExclusionForm.get('branchId').setValue('');
      this.getDebtorList();
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.branches = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });

    this.priceIncreaseExclusionForm.get('branchId').valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      setTimeout(() => {
        this.getDebtorList();
        this.getBusinessAreaList();
      }, 2000);
    });

    this.priceIncreaseExclusionForm.get('businessAreaName').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val) {
          this.businessAreaList = [];
          if (val.length >= 1) {
            return this.filterBusinessAreaDetailsBySearchOptions(val, this.priceIncreaseExclusionForm.value.branchId);
          }
        } else {
          this.businessAreaList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources.length === 0) {
          }
          if (response.resources != null) {
            this.businessAreaList = response.resources;
            if (this.businessAreaList.length == 0) {
              this.businessAreaList = [];
            }
          } else {
            this.businessAreaList = [];
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.priceIncreaseExclusionForm.get('subAreaName').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val) {
          this.subAreaList = [];
          if (val.length >= 1) {
            return this.filterSubAreaDetailsBySearchOptions(val);
          }
        } else {
          this.subAreaList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources.length === 0) {
          }
          if (response.resources != null) {
            this.subAreaList = response.resources;
            if (this.subAreaList.length == 0) {
              this.subAreaList = [];
            }
          } else {
            this.subAreaList = [];
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.priceIncreaseExclusionForm.get('priceLowerLimit').valueChanges.subscribe((priceLowerLimit: string) => {
      if (!priceLowerLimit) return;
      this.priceIncreaseExclusionForm.get('priceUpperLimit').setValue(9999999);
    });
  }

  onTabChanged(index: number) {
    this.selectedIndex = index;
    if (this.selectedIndex == 0) {
      this.priceIncreaseExclusionForm.get('categoryId').setValue('');
      this.priceIncreaseExclusionForm.get('originId').setValue('');
      this.priceIncreaseExclusionForm.get('installOriginId').setValue('');
      this.priceIncreaseExclusionForm.get('subCategoryId').setValue('');
      this.priceIncreaseExclusionForm.get('stockId').setValue('');
      this.priceIncreaseExclusionForm.get('contractTypeId').setValue('');
      this.priceIncreaseExclusionForm.get('isArea').setValue(true);
      this.priceIncreaseExclusionForm.get('isBusinessClassification').setValue(false);
      this.priceIncreaseExclusionForm.get('isServices').setValue(false);
      if (this.priceIncreaseExclusionForm.value.businessAreaId) {
        this.priceIncreaseExclusionForm.get('businessAreaId').setValue(this.priceIncreaseExclusionForm.value.businessAreaId)
      }
    } else if (this.selectedIndex == 1) {
      this.priceIncreaseExclusionForm.get('debtorId').setValue('');
      this.priceIncreaseExclusionForm.get('debtorName').setValue('');
      this.priceIncreaseExclusionForm.get('subAreaId').setValue('');
      this.priceIncreaseExclusionForm.get('subAreaName').setValue('');
      this.priceIncreaseExclusionForm.get('businessAreaName').setValue('');
      this.priceIncreaseExclusionForm.get('businessAreaId').setValue('');
      this.priceIncreaseExclusionForm.get('isSaveOfferProcessed').setValue(false);
      this.priceIncreaseExclusionForm.get('priceLowerLimit').setValue(null);
      this.priceIncreaseExclusionForm.get('priceUpperLimit').setValue(null);
      this.priceIncreaseExclusionForm.get('stockId').setValue('');
      this.priceIncreaseExclusionForm.get('contractTypeId').setValue('');
      this.priceIncreaseExclusionForm.get('businessAreaName').setValue('');
      this.priceIncreaseExclusionForm.get('businessAreaId').setValue('');
      this.priceIncreaseExclusionForm.get('isArea').setValue(false);
      this.priceIncreaseExclusionForm.get('isBusinessClassification').setValue(true);
      this.priceIncreaseExclusionForm.get('isServices').setValue(false);
      if (this.priceIncreaseExclusionForm.value.businessAreaId1) {
        this.priceIncreaseExclusionForm.get('businessAreaId').setValue(this.priceIncreaseExclusionForm.value.businessAreaId1)
      }
    }
    else if (this.selectedIndex == 2) {
      this.priceIncreaseExclusionForm.get('categoryId').setValue('');
      this.priceIncreaseExclusionForm.get('originId').setValue('');
      this.priceIncreaseExclusionForm.get('installOriginId').setValue('');
      this.priceIncreaseExclusionForm.get('subCategoryId').setValue('');
      this.priceIncreaseExclusionForm.get('debtorName').setValue('');
      this.priceIncreaseExclusionForm.get('debtorId').setValue('');
      this.priceIncreaseExclusionForm.get('subAreaId').setValue('');
      this.priceIncreaseExclusionForm.get('subAreaName').setValue('');
      this.priceIncreaseExclusionForm.get('isSaveOfferProcessed').setValue(false);
      this.priceIncreaseExclusionForm.get('priceLowerLimit').setValue(null);
      this.priceIncreaseExclusionForm.get('priceUpperLimit').setValue(null);
      this.priceIncreaseExclusionForm.get('businessAreaName').setValue('');
      this.priceIncreaseExclusionForm.get('businessAreaId').setValue('');
      this.priceIncreaseExclusionForm.get('isArea').setValue(false);
      this.priceIncreaseExclusionForm.get('isBusinessClassification').setValue(false);
      this.priceIncreaseExclusionForm.get('isServices').setValue(true);
    }
  }

  onSubmit() {
    this.onTabChanged(this.selectedIndex);
    if (this.priceIncreaseExclusionForm.invalid) {
      return;
    }

    if (this.selectedIndex === 0) {
      this.priceIncreaseExclusionForm.get('debtorId').setValue(this.debtorCodeId);
      this.priceIncreaseExclusionForm.get('businessAreaId').setValue(this.bussinessAreaId);
      this.priceIncreaseExclusionForm.get('subAreaId').setValue(this.subAreaId);
    } else if (this.selectedIndex === 1) {
      this.priceIncreaseExclusionForm.get('businessAreaId').setValue(this.bussinessAreaId);
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_INCREASE_EXCLUSIONS, this.priceIncreaseExclusionForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(['billing/price-increase-configuration'], { queryParams: { tab: 2 } });
      } else {
        this.priceIncreaseExclusionForm.get('debtorName').setValue('');
        this.priceIncreaseExclusionForm.get('debtorId').setValue('');
        this.priceIncreaseExclusionForm.get('businessAreaId').setValue('');
        this.priceIncreaseExclusionForm.get('businessAreaName').setValue('');
        this.priceIncreaseExclusionForm.get('subAreaName').setValue('');
        this.priceIncreaseExclusionForm.get('subAreaId').setValue('');
      }
    })
  }
}
