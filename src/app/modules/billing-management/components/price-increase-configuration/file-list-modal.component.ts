import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'file-list-type-modal',
  templateUrl: './file-list-modal.component.html',
})
export class FileListAddEditModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  leadId: string;
  fileAddEditForm: FormGroup;
  siteTypeList = [];
  adminFeeId: any;
  selectedOptions = [];
  loggedInUserData: LoggedInUserModel;
  priceDetails: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService, private router: Router,
    private dialog: MatDialog, private store: Store<AppState>) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createFileAddEditForm();
    this.getSityeTypes();
    if (this.data && this.data.pricingFileNameId) {
      this.getDetails();
    }
  }

  getSityeTypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SITE_TYPES, null, false,
      prepareGetRequestHttpParams(undefined, null, null))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          let TmpArray = response.resources;
          for (let i = 0; i < TmpArray.length; i++) {
            let temp = {};
            temp['display'] = TmpArray[i].displayName;
            temp['value'] = TmpArray[i].id;
            this.siteTypeList.push(temp);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createFileAddEditForm(): void {
    let priceModel = {
      "pricingFileCategoryId": "",
      "createdUserId": this.loggedInUserData.userId,
      "pricingFileName": ""
    };
    this.fileAddEditForm = this.formBuilder.group({
    });
    Object.keys(priceModel).forEach((key) => {
      this.fileAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.fileAddEditForm = setRequiredValidator(this.fileAddEditForm, ["pricingFileCategoryId", "pricingFileName"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_FILES_NAMES, this.data.pricingFileNameId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceDetails = response.resources;
          this.fileAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSubmit() {
    if (this.fileAddEditForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_FILES_NAMES, this.fileAddEditForm.value).subscribe((response) => {
      if (response.isSuccess && response.resources && response.statusCode == 200) {
        this.outputData.emit(true);
        this.dialog.closeAll();
        if (this.fileAddEditForm.value.pricingFileCategoryId == 1) {
        } else {
          this.router.navigate(['/billing/price-increase-configuration/adhoc-config/add-edit'], { queryParams: { id: response.resources } });
        }
        this.rxjsService.setDialogOpenProperty(false);
      }
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
