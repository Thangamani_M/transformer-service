import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData, PriceIncreaseDateConfigAddEditModel } from '@modules/others';
import { Store } from '@ngrx/store';
import { BillingModuleApiSuffixModels } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'price-increase-date-modal',
  templateUrl: './price-increase-date-modal.component.html',
})
export class PriceIncreaseDateModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  leadId: string;
  priceIncreaseConfigAddEditForm: FormGroup;
  siteTypeList = [];
  serviceList = [];
  loggedInUserData: LoggedInUserModel;
  priceDetails: any;
  pricingIncreaseMonthId: any;
  selectedOptions = [];
  selectedOptions1 = [];
  categoryList = [];
  priceIncreaseDateDetails: any;
  isEdit = false;
  monthsList = [{ id: 1, displayName: 'Jan' }, { id: 2, displayName: 'Feb' }, { id: 3, displayName: 'March' }, { id: 4, displayName: 'April' }, { id: 5, displayName: 'May' }, { id: 6, displayName: 'June' }, { id: 7, displayName: 'Jully' }, { id: 8, displayName: 'Aug' }, { id: 9, displayName: 'Sep' }, { id: 10, displayName: 'Oct' }, { id: 11, displayName: 'Nov' }, { id: 12, displayName: 'Dec' }]
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService,
    private dialog: MatDialog, private store: Store<AppState>, private snackbarService: SnackbarService) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createPriceIncreaseAddEditForm();
    this.getCategory();
    if (this.data.pricingIncreaseMonthId) {
      this.pricingIncreaseMonthId = this.data.pricingIncreaseMonthId;
      this.getDetails();
    }
  }

  editPage() {
    if (!this.data?.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isEdit = true;
  }

  createPriceIncreaseAddEditForm(): void {
    let priceModel = new PriceIncreaseDateConfigAddEditModel();
    this.priceIncreaseConfigAddEditForm = this.formBuilder.group({
    });
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.priceIncreaseConfigAddEditForm = setRequiredValidator(this.priceIncreaseConfigAddEditForm, ["categoryId", "monthId", "increasePercentage"]);
    this.priceIncreaseConfigAddEditForm.get("increasePercentage").setValidators([Validators.max(100)])
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_INCREASE_MONTHS, this.data.pricingIncreaseMonthId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceIncreaseDateDetails = response.resources;
          this.priceIncreaseConfigAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getCategory() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CATEGORY, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.categoryList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSubmit() {
    if (this.priceIncreaseConfigAddEditForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.data.pricingIncreaseMonthId) {
      this.priceIncreaseConfigAddEditForm.get('modifiedUserId').setValue(this.loggedInUserData.userId);
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_INCREASE_MONTHS, this.priceIncreaseConfigAddEditForm.value).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.outputData.emit(true);
          this.dialog.closeAll();
          this.rxjsService.setDialogOpenProperty(false);
        }
      })
    } else {
      this.priceIncreaseConfigAddEditForm.get('branchIds').setValue(this.data.branchIds);
      this.priceIncreaseConfigAddEditForm.get('createdUserId').setValue(this.data.createdUserId);
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_INCREASE_MONTHS, this.priceIncreaseConfigAddEditForm.value).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.outputData.emit(true);
          this.dialog.closeAll();
          this.rxjsService.setDialogOpenProperty(false);
        }
      })
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
