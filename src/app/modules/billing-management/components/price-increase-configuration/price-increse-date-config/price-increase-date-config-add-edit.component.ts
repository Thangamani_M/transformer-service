

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData, PriceIncreaseConfigModel, PriceIncreaseConfigSiteTypeAddEditModel } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { PriceIncreaseDateModalComponent } from './price-increase-date-modal.component';

@Component({
  selector: 'price-increase-date-config-add-edit',
  templateUrl: './price-increase-date-config-add-edit.component.html'
})
export class PriceIncreaseDateConfigAddEditComponent implements OnInit {

  constructor(private crudService: CrudService, private dialog: MatDialog, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }
  priceIncreaseConfigForm: FormGroup;
  priceIncreaseConfigAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  divisionDropDown = [];
  regionList = [];
  districtList = [];
  branchList = [];
  siteTypeList = [];
  priceIncDailog = false;
  selectedOptions = [];
  stockIdDetails: any;
  adminFeeId: any;
  pricingIncreaseRunDateConfigId: any;
  priceIncreaseList = [];
  primengTableConfigProperties :any=  {
    tableCaption: "Price Increase Config - Date Config",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' },
    { displayName: 'Billing ', relativeRouterUrl: '' },
      { displayName: 'Price Increase Configuration ', relativeRouterUrl: '/billing/price-increase-configuration', queryParams:{tab:3} },{
      displayName: 'Date Config'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Price Increase Config - Date Config',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
        }]
    }
  }
  ngOnInit() {
    this.createPriceIncreaseForm();
    this.createPriceIncreaseAddEditForm();
    this.getRegions();
    this.onFormControlChange();
  }

  getPriceIncreaseList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_INCREASE_MONTHS, undefined, false,
      prepareRequiredHttpParams({ branchId: this.priceIncreaseConfigForm.value.branchIds }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceIncreaseList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createPriceIncreaseForm(): void {
    let priceModel = new PriceIncreaseConfigModel();
    this.priceIncreaseConfigForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.priceIncreaseConfigForm = setRequiredValidator(this.priceIncreaseConfigForm, ["regionId", "divisionIds", "districtIds","branchIds"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createPriceIncreaseAddEditForm(): void {
    let priceModel = new PriceIncreaseConfigSiteTypeAddEditModel();
    this.priceIncreaseConfigAddEditForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess && response.statusCode == 200) {
            this.regionList = response.resources;
          }
        });
  }

  onFormControlChange() {
    this.priceIncreaseConfigForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.divisionDropDown = getPDropdownData(response.resources)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseConfigForm.get('divisionIds').valueChanges.subscribe((divisionIds: string) => {
      if (!divisionIds) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionIds }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.districtList = getPDropdownData(response.resources)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseConfigForm.get('districtIds').valueChanges.subscribe((districtIds: any) => {
      if (!districtIds) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId: districtIds }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.branchList = getPDropdownData(response.resources)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });

  }

  getSelectedValue(e) {
    if (e.length > 0) {
      this.getPriceIncreaseList();
    }
  }

  removeRecord(data) {
    const message = `Do you want to delete?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.crudService.delete(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.PRICING_INCREASE_MONTHS,
          '',
          prepareRequiredHttpParams({
            ids: data.pricingIncreaseMonthId,
            modifiedUserId: this.loggedInUserData.userId,
            isDeleted: true
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.getPriceIncreaseList();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
  }


  addPriceIncrease() {
    if (this.priceIncreaseConfigForm.invalid) {
      return
    }
    let data = {
      branchIds: this.priceIncreaseConfigForm.value.branchIds,
      createdUserId: this.loggedInUserData.userId
    }
    const dialogReff = this.dialog.open(PriceIncreaseDateModalComponent, { width: '680px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getPriceIncreaseList();
      }
    });
  }
}
