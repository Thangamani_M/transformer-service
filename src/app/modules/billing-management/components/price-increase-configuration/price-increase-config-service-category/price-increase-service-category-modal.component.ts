import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData, PriceIncreaseConfigServiceCategoryAddEditModel } from '@modules/others';
import { Store } from '@ngrx/store';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'price-increase-service-category-modal',
  templateUrl: './price-increase-service-category-modal.component.html',
})
export class PriceIncreaseServiceCategoryModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  leadId: string;
  priceIncreaseConfigAddEditForm: FormGroup;
  siteTypeList = [];
  serviceList = [];
  loggedInUserData: LoggedInUserModel;
  priceDetails: any;
  adminFeeId: any;
  pricingConfigServiceId: any;
  selectedOptions = [];
  selectedOptions1 = [];
  isEdit = false;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService,
    private snackbarService : SnackbarService,
    private dialog: MatDialog, private store: Store<AppState>) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createPriceIncreaseAddEditForm();
    this.getSityeTypes();
    this.getService();
    if (this.data.pricingConfigServiceId) {
      this.getDetails();
    }
  }

  getSityeTypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SITE_TYPES, null, false,
      prepareGetRequestHttpParams(undefined, null, null))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          if (this.data.pricingConfigServiceId) {
            this.siteTypeList = response.resources
          } else {
            this.siteTypeList = getPDropdownData(response.resources)

          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  editPage() {
    if(!this.data?.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isEdit = true;
  }

  getService() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICES, null, false,
      prepareGetRequestHttpParams(undefined, null, null))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          if (this.data.pricingConfigServiceId) {
            this.serviceList = response.resources
          } else {
            this.serviceList = getPDropdownData(response.resources)

          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }


  createPriceIncreaseAddEditForm(): void {
    let priceModel = new PriceIncreaseConfigServiceCategoryAddEditModel();
    this.priceIncreaseConfigAddEditForm = this.formBuilder.group({
      priceIncreasePercentage: [null, [Validators.required, Validators.max(100)]]
    });
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigAddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.priceIncreaseConfigAddEditForm = setRequiredValidator(this.priceIncreaseConfigAddEditForm, ["siteTypeIds", "serviceIds"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_CONFIG_SERVICE_CATEGORY, this.data.pricingConfigServiceId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceDetails = response.resources;
          this.priceIncreaseConfigAddEditForm.patchValue(response.resources);
          this.priceIncreaseConfigAddEditForm.get('serviceIds').setValue(response.resources.serviceId);
          this.priceIncreaseConfigAddEditForm.get('siteTypeIds').setValue(response.resources.siteTypeId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSubmit() {
    if (this.priceIncreaseConfigAddEditForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.data.pricingConfigServiceId) {
      this.priceIncreaseConfigAddEditForm.get('modifiedUserId').setValue(this.loggedInUserData.userId);
      this.priceIncreaseConfigAddEditForm.get('branchIds').setValue(this.priceDetails.branchIds);
      this.priceIncreaseConfigAddEditForm.get('mainAreaIds').setValue(this.priceDetails.mainAreaIds);
      this.priceIncreaseConfigAddEditForm.value.siteTypeId = this.priceIncreaseConfigAddEditForm.value.siteTypeIds;
      this.priceIncreaseConfigAddEditForm.value.serviceId = this.priceIncreaseConfigAddEditForm.value.serviceIds;
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_CONFIG_SERVICE_CATEGORY, this.priceIncreaseConfigAddEditForm.value).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.outputData.emit(true);
          this.dialog.closeAll();
          this.rxjsService.setDialogOpenProperty(false);
        }
      })
    } else {
      this.priceIncreaseConfigAddEditForm.get('createdUserId').setValue(this.data.createdUserId);
      this.priceIncreaseConfigAddEditForm.get('branchIds').setValue(this.data.branchIds);
      this.priceIncreaseConfigAddEditForm.get('mainAreaIds').setValue(this.data.mainAreaIds);
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_CONFIG_SERVICE_CATEGORY, this.priceIncreaseConfigAddEditForm.value).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.outputData.emit(true);
          this.dialog.closeAll();
          this.rxjsService.setDialogOpenProperty(false);
        }
      })
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
