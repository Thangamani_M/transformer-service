

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData, PriceIncreaseConfigModel, PriceIncreaseConfigSiteTypeAddEditModel } from '@modules/others';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { PriceIncreaseServiceCategoryModalComponent } from '.';

@Component({
  selector: 'price-increase-config-service-category-add-edit',
  templateUrl: './price-increase-config-service-category-add-edit.component.html'
})
export class PriceIncreaseConfigServiceCategoryAddEditComponent implements OnInit {
  constructor(private crudService: CrudService, private dialog: MatDialog, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }
  priceIncreaseConfigForm: FormGroup;
  priceIncreaseConfigAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  divisionDropDown = [];
  regionList = [];
  districtList = [];
  branchList = [];
  businessAreaList = [];
  siteTypeList = [];
  priceIncDailog = false;
  selectedOptions = [];
  adminFeeId: any;
  pricingIncreaseRunDateConfigId: any;
  stockIdDetails: any;
  priceIncreaseList = [];
  primengTableConfigProperties :any=  {
    tableCaption: "Price Increase Config - Service Category",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' },
    { displayName: 'Billing ', relativeRouterUrl: '' },
      { displayName: 'Price Increase Configuration ', relativeRouterUrl: '/billing/price-increase-configuration', queryParams:{tab:1} },{
      displayName: 'Service Category'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Price Increase Config - Service Category',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
        }]
    }
  }
  ngOnInit() {
    this.createPriceIncreaseForm();
    this.createPriceIncreaseAddEditForm();
    this.getRegions();
    this.onFormControlChange();
  }

  getPriceIncreaseList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICING_CONFIG_SERVICE_CATEGORY, undefined, false,
      prepareRequiredHttpParams({ branchId: this.priceIncreaseConfigForm.value.branchIds }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.priceIncreaseList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createPriceIncreaseForm(): void {
    let priceModel = new PriceIncreaseConfigModel();
    this.priceIncreaseConfigForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.priceIncreaseConfigForm = setRequiredValidator(this.priceIncreaseConfigForm, ["regionId", "divisionIds", "districtIds","branchIds","mainAreaIds"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createPriceIncreaseAddEditForm(): void {
    let priceModel = new PriceIncreaseConfigSiteTypeAddEditModel();
    this.priceIncreaseConfigAddEditForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.priceIncreaseConfigAddEditForm.addControl(key, new FormControl(priceModel[key]));

    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  removeRecord(data) {
    const message = `Do you want to delete?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.crudService.delete(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.PRICING_CONFIG_SERVICE_CATEGORY,
          '',
          prepareRequiredHttpParams({
            ids: data.pricingConfigServiceId,
            modifiedUserId: this.loggedInUserData.userId,
            isDeleted: true
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.getPriceIncreaseList();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess && response.statusCode == 200 && response.resources) {
            this.regionList = response.resources;
          }
        });
  }

  getBusinessAreaList(branchIds) {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_MAIN_AREA_BY_BRANCH,
      prepareGetRequestHttpParams(null, null,
        {
          divisionId: this.priceIncreaseConfigForm.value.divisionIds,
          districtId: this.priceIncreaseConfigForm.value.districtIds,
          branchId: branchIds
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.businessAreaList = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
  }

  onFormControlChange() {
    this.priceIncreaseConfigForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.divisionDropDown =  getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseConfigForm.get('divisionIds').valueChanges.subscribe((divisionIds: string) => {
      if (!divisionIds) return;

      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionIds }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.districtList =  getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseConfigForm.get('districtIds').valueChanges.subscribe((districtIds: any) => {
      if (!districtIds) return;

      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId: districtIds }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.branchList = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.priceIncreaseConfigForm.get('branchIds').valueChanges.subscribe((branchIds: any) => {
      if (!branchIds) return;
      if (branchIds.length != 0) {
        this.getBusinessAreaList(branchIds)
      }
    });
  }

  getSelectedValue(e) {
    if (e.length > 0) {
      this.getPriceIncreaseList();
    }
  }

  addPriceIncrease() {

    if (this.priceIncreaseConfigForm.invalid) {
      return
    }

    let data = {
      branchIds: this.priceIncreaseConfigForm.value.branchIds,
      mainAreaIds: this.priceIncreaseConfigForm.value.mainAreaIds,
      createdUserId: this.loggedInUserData.userId
    }
    const dialogReff = this.dialog.open(PriceIncreaseServiceCategoryModalComponent, { width: '680px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getPriceIncreaseList();
      }
    });
  }
}
