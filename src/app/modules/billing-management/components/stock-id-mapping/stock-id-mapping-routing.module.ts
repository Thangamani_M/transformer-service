import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockIdMappingAddEditComponent } from './stock-id-mapping-add-edit.component';
import { StockIdMappingListComponent } from './stock-id-mapping-list.component';
import { StockIdMappingViewComponent } from './stock-id-mapping-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    // { path: '', redirectTo:'list',canActivate:[AuthGuard],pathMatch:'full'},
    { path: '', component: StockIdMappingListComponent, canActivate:[AuthGuard],data: { title: 'Stock ID Mapping list' }},
    { path: 'add-edit', component: StockIdMappingAddEditComponent,canActivate:[AuthGuard], data: { title: 'Stock ID Mapping Add/Edit' }},
    { path: 'view', component: StockIdMappingViewComponent,canActivate:[AuthGuard], data: { title: 'Stock ID Mapping View' }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class StockIdMappingRoutingModule { }
