import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-stock-id-mapping-view',
  templateUrl: './stock-id-mapping-view.component.html'
})
export class StockIdMappingViewComponent implements OnInit {
  stockId: string;
  stockIdDetails: any;
  divisionList: any;
  districtList: any;
  districtId: any
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.stockId = this.activatedRoute.snapshot.queryParams.id
    this.districtId = this.activatedRoute.snapshot.queryParams.districtId
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getstockIdDetailsById(this.stockId, this.districtId);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.STOCK_ID_MAPPING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getstockIdDetailsById(stockId: string, districtId: string) {
    let otherParams = {}
    otherParams['divisionId'] = stockId
    otherParams['districtId'] = districtId
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_MAPPING_DETAILS, null, false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stockIdDetails = response.resources;
          let districListData = [];
          let divisionListData = [];
          if (this.stockIdDetails.districts.length) {
            for (let i = 0; i < this.stockIdDetails.districts.length; i++) {
              districListData.push(this.stockIdDetails.districts[i].districtName)
            }
            this.districtList = districListData.join();
          }
          if (this.stockIdDetails.divisions.length) {
            for (let i = 0; i < this.stockIdDetails.divisions.length; i++) {
              divisionListData.push(this.stockIdDetails.divisions[i].divisionName)
            }

            this.divisionList = divisionListData.join();
          }

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.stockId) {
      this.router.navigate(['billing/stock-id-mapping/add-edit'], { queryParams: { id: this.stockId, districtId: this.districtId } });
    }
  }
}
