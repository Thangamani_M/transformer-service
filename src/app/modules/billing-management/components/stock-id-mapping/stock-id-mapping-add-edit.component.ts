import { HttpParams } from '@angular/common/http';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { ServiceCategoryListModel, StockDescriptionListModel, StockIdMappingAddEditModel } from '@modules/others/configuration/models/stock-id-mapping.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-stock-id-mapping-add-edit',
  templateUrl: './stock-id-mapping-add-edit.component.html'
})
export class StockIdMappingAddEditComponent implements OnInit, AfterViewChecked {
  divisionList: any = [];
  districtList: any = [];
  serviceCategoryList: any = [];
  serviceList: any = [];
  stockIdList: any = [];
  descriptionList: any = [];
  isStockCreateMap: boolean = false;
  stockIdMappingForm: FormGroup;
  stockAddedList: any = [];
  dropdownsAndData = [];
  loggedUser: any
  stockMappingId: any
  serviceCategories: FormArray
  stockDescriptions: FormArray
  stockIdDetails: any
  districtId: any
  stockIdMappingDetails = {};
  @ViewChild(SelectAutocompleteComponent, { static: false }) multiSelect: SelectAutocompleteComponent;

  constructor(private crudService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private dialog: MatDialog, private httpCancelService: HttpCancelService,
    private rxjsService: RxjsService, private formbuilder: FormBuilder, private changeDetectorRef: ChangeDetectorRef,) {
    this.stockMappingId = this.activatedRoute.snapshot.queryParams.id;
    this.districtId = this.activatedRoute.snapshot.queryParams.districtId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    let otherParams = {}
    otherParams['approved'] = true
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_DIVISIONS),
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_UX_SERVICE_CATEGORY),
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.UX_STOCK_ID, null, false, prepareGetRequestHttpParams(null, null, otherParams)),

    ];
    this.loadDropdownData(this.dropdownsAndData);
    this.createstockIdMappingForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.stockMappingId) {
      this.getStockIdDetailsById().subscribe((response: IApplicationResponse) => {
        this.stockIdMappingDetails = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        let stockIdMappingAddEditModel = new StockIdMappingAddEditModel(response.resources);
        this.stockIdDetails = response.resources;
        this.stockIdMappingForm.patchValue(stockIdMappingAddEditModel);
        let divisions = []
        response.resources.divisions.forEach(element => {
          divisions.push(element.divisionId)
        });
        let districts = []
        response.resources.districts.forEach(element => {
          districts.push(element.districtId)
        });
        this.stockIdMappingForm.get('divisionIds').setValue(divisions)
        this.stockIdMappingForm.get('districtIds').setValue(districts)
        this.stockIdMappingForm.get('divisionId').setValue(divisions ? divisions[0] : null)
        this.stockIdMappingForm.get('districtId').setValue(districts ? districts[0] : null)
        this.stockIdMappingForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.stockIdMappingForm.get('modifiedUserId').setValue(this.loggedUser.userId)
        this.serviceCategories = this.getServiceCategoryListArray
        response.resources.serviceCategoryDetails.forEach((serviceCategoryListModel: any, index) => {
          this.serviceCategories.push(this.createServiceCategoryListModel(serviceCategoryListModel));
          this.onCategoryChange(index)
          serviceCategoryListModel.stockDescriptionDetails.forEach((stockDescriptionListModel: StockDescriptionListModel, j) => {
            this.stockDescriptions = this.getstockIdDescriptionListArray(index)
            this.stockDescriptions.push(this.createStockDescriptionListModel(stockDescriptionListModel));
            this.onDescriptionChange(index, j)
          });


        });
      })
    } else {
      this.serviceCategories = this.getServiceCategoryListArray
      this.serviceCategories.push(this.createServiceCategoryListModel());
      this.stockDescriptions = this.getstockIdDescriptionListArray(0)
      this.stockDescriptions.push(this.createStockDescriptionListModel());
    }

    this.stockIdMappingForm.get('divisionIds').valueChanges.subscribe(data => {
      if (data != '') {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
          prepareGetRequestHttpParams(null, null,
            { divisionId: data })).subscribe(resp => {
              this.districtList = getPDropdownData(resp.resources);
                if (this.stockMappingId) {
                  this.stockIdMappingForm.get('districtIds').setValue([this.stockIdMappingDetails["districts"][0].districtId]);
                }
              this.rxjsService.setGlobalLoaderProperty(false);
            })
      } else {
        this.districtList = [];
      }
    })
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  createMapping() {
    this.isStockCreateMap = true;
  }

  //Get Details
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    let otherParams = {}
    otherParams['divisionId'] = this.stockMappingId
    otherParams['districtId'] = this.districtId
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_MAPPING_DETAILS, null, false, prepareGetRequestHttpParams(null, null, otherParams))

    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_MAPPING_DETAILS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams)
    );
  }

  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = getPDropdownData(resp.resources);
              break;
            case 1:
              let serviceCategories = resp.resources;
              if (serviceCategories.length) {
                for (let i = 0; i < serviceCategories.length; i++) {
                  let temp = {};
                  temp['display'] = serviceCategories[i].displayName;
                  temp['value'] = serviceCategories[i].id;
                  this.serviceCategoryList.push(temp);
                }
              }
              break;
            case 2:
              let stockIds = resp.resources;
              if (stockIds.length) {
                for (let i = 0; i < stockIds.length; i++) {
                  let temp = {};
                  temp['display'] = stockIds[i].displayName;
                  temp['value'] = stockIds[i].id;
                  this.stockIdList.push(temp);
                }
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createstockIdMappingForm(): void {
    let stockIdMappingAddEditModel = new StockIdMappingAddEditModel();
    this.stockIdMappingForm = this.formbuilder.group({
      serviceCategories: this.formbuilder.array([])
    });
    Object.keys(stockIdMappingAddEditModel).forEach((key) => {
      this.stockIdMappingForm.addControl(key, new FormControl(stockIdMappingAddEditModel[key]));
    });
    this.stockIdMappingForm = setRequiredValidator(this.stockIdMappingForm, ["divisionIds", "districtIds"]);
    this.stockIdMappingForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.stockIdMappingForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  //Create FormArray controls
  createServiceCategoryListModel(serviceCategoryListModel?: ServiceCategoryListModel): FormGroup {
    let serviceCategoryListModelControl = new ServiceCategoryListModel(serviceCategoryListModel);
    let formControls = {};
    Object.keys(serviceCategoryListModelControl).forEach((key) => {
      if (key === 'stockDescriptions') {
        formControls[key] = this.formbuilder.array([])
      } else if (key === 'serviceList') {
        formControls[key] = [{ value: serviceCategoryListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: serviceCategoryListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formbuilder.group(formControls);
  }

  //Create FormArray controls
  createStockDescriptionListModel(stockDescriptionListModel?: StockDescriptionListModel): FormGroup {
    let stockDescriptionListModelControl = new StockDescriptionListModel(stockDescriptionListModel);
    let formControls = {};
    Object.keys(stockDescriptionListModelControl).forEach((key) => {
      if (key === 'descriptionList') {
        formControls[key] = [{ value: stockDescriptionListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: stockDescriptionListModelControl[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formbuilder.group(formControls);
  }

  //Create FormArray
  get getServiceCategoryListArray(): FormArray {
    if (!this.stockIdMappingForm) return;
    return this.stockIdMappingForm.get("serviceCategories") as FormArray;
  }
  //Create FormArray
  getstockIdDescriptionListArray(index: number): FormArray {
    if (!this.stockIdMappingForm) return;
    return this.serviceCategories.at(index).get("stockDescriptions") as FormArray

  }

  //Add Employee Details
  addServiceCategroy(): void {
    if (this.getServiceCategoryListArray.invalid) {
      return;
    };

    this.serviceCategories = this.getServiceCategoryListArray;
    let serviceCategoryListModel = new ServiceCategoryListModel();
    this.serviceCategories.insert(0, this.createServiceCategoryListModel(serviceCategoryListModel));

    this.stockDescriptions = this.getstockIdDescriptionListArray(0);
    let stockDescriptionListModel = new StockDescriptionListModel();
    this.stockDescriptions.insert(0, this.createStockDescriptionListModel(stockDescriptionListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeServiceCategroy(i: number): void {
    if (!this.getServiceCategoryListArray.controls[i].value.stockDescriptionId) {
      this.getServiceCategoryListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getServiceCategoryListArray.controls[i].value.stockDescriptionNameId && this.getServiceCategoryListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SMS_GROUP_EMPLOYEE,
          this.getServiceCategoryListArray.controls[i].value.stockDescriptionNameId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getServiceCategoryListArray.removeAt(i);
            }
            if (this.getServiceCategoryListArray.length === 0) {
              this.addServiceCategroy();
            };
          });
      }
      else {
        this.getServiceCategoryListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  //Add Employee Details
  addDescription(i, j): void {
    if (this.getstockIdDescriptionListArray(i).invalid) {
      return;
    };
    this.stockDescriptions = this.getstockIdDescriptionListArray(i);
    let stockDescriptionListModel = new StockDescriptionListModel();
    this.stockDescriptions.insert(0, this.createStockDescriptionListModel(stockDescriptionListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeDescription(i, j): void {
    if (!this.getstockIdDescriptionListArray(i).controls[j].value.stockDescriptionId) {
      this.getstockIdDescriptionListArray(i).removeAt(j);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getServiceCategoryListArray.controls[i].value.stockDescriptionId && this.getServiceCategoryListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SMS_GROUP_EMPLOYEE,
          this.getServiceCategoryListArray.controls[i].value.stockDescriptionId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getstockIdDescriptionListArray(i).removeAt(j)
            }
            if (this.getServiceCategoryListArray.length === 0) {
              this.addDescription(0, 0)
            };
          });
      }
      else {
        this.getstockIdDescriptionListArray(i).removeAt(j)
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onCategoryChange(index) {
    let serviceCategoryId = this.getServiceCategoryListArray.controls[index].value.serviceCategoryId
    let params = new HttpParams().set('ServiceCategoryId', serviceCategoryId);
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICES, null, null, params).subscribe((response: IApplicationResponse) => {
      this.getServiceCategoryListArray.controls[index].get('serviceList').setValue(response.resources)
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }


  onDescriptionChange(i, j) {
    let stockId = this.getstockIdDescriptionListArray(i).controls[j].value.stockId
    let params = new HttpParams().set('StockId', stockId);
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_DESCRIPTION, null, null, params).subscribe((response: IApplicationResponse) => {
      this.getstockIdDescriptionListArray(i).controls[j].get('descriptionList').setValue(response.resources)
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  onSubmit() {

    if (this.stockIdMappingForm.invalid) {
      return;
    }
    let formValue = this.stockIdMappingForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.stockMappingId) ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_MAPPING, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_MAPPING, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/billing/stock-id-mapping?tab=0');
      }
    });
  }
}
