import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxPrintModule } from 'ngx-print';
import { StockIdMappingAddEditComponent } from './stock-id-mapping-add-edit.component';
import { StockIdMappingListComponent } from './stock-id-mapping-list.component';
import { StockIdMappingRoutingModule } from './stock-id-mapping-routing.module';
import { StockIdMappingViewComponent } from './stock-id-mapping-view.component';


@NgModule({
  declarations: [StockIdMappingListComponent, StockIdMappingAddEditComponent, StockIdMappingViewComponent],
  imports: [
    CommonModule,
    StockIdMappingRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    NgxPrintModule
  ]
})
export class StockIdMappingModule { }
