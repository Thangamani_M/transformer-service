
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales';

@Component({
  selector: 'app-type-id-view',
  templateUrl: './transaction-type-view.component.html',
  styleUrls: ['./transaction-configuration-list.component.scss']
})
export class TransactionTypeViewComponent implements OnInit {
  stockId: string;
  stockIdDetails: any;
  id: any;
  details: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.id = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getDetailsById(this.id);
  }

  getDetailsById(stockId: string) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES, this.id, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.details = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  edit() {
    if (this.stockId) {
      this.router.navigate(['billing/stock-id/add-edit'], { queryParams: { id: this.stockId } });
    }
  }
}
