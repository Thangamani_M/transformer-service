import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TransctionDescriptionRoutingModule } from './transaction-configuration-list-routing.module';
import { TransactionConfigurationListComponent } from './transaction-configuration-list.component';
import { TransactionDescriptionEditComponent } from './transaction-description-add-edit.component';
import { TransactionDescriptionSubTypeViewComponent } from './transaction-description-subtype-view.component';
import { TransactionDescriptionViewComponent } from './transaction-description-view.component';
import { TransactionSubTypeEditComponent } from './transaction-subtype-add-edit.component';
import { TransactionTypeAddEditComponent } from './transaction-type-add-edit.component';
import { TransactionTypeViewComponent } from './transaction-type-view.component';


@NgModule({
  declarations: [TransactionConfigurationListComponent,TransactionDescriptionViewComponent,TransactionTypeViewComponent,TransactionTypeAddEditComponent,TransactionDescriptionEditComponent,TransactionSubTypeEditComponent,TransactionDescriptionSubTypeViewComponent],
  imports: [
    CommonModule,
    TransctionDescriptionRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ],
  entryComponents:[TransactionTypeAddEditComponent]
})
export class TransctionDescriptionModule { }
