
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { Documents, SubTypes, TransactionDescriptionSubTypeAddEditModel } from '@modules/others/configuration/models/stock-id.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-transaction-subtype-add-edit',
  templateUrl: './transaction-subtype-add-edit.component.html',
  styleUrls: ['./transaction-configuration-list.component.scss']
})
export class TransactionSubTypeEditComponent implements OnInit {
  id: any
  divisionDropDown: any = [];
  transactionSubtypeAddEditForm: FormGroup;
  subTypes: FormArray;
  documents: FormArray;
  rOCommunityPatrolTypesList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  stockIdDetails: any;
  isDuplicate: boolean;
  isDuplicateNumber: boolean;

  @ViewChildren('input') rows: QueryList<any>;

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  transationTypes: any;
  details: any;
  loggedInUserData: LoggedInUserModel;
  transationDescriptions: any;
  isEdit: boolean = false;

  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}]
    }
  }

  constructor(private activatedRoute: ActivatedRoute, private dialogService: DialogService,
    private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService,
     private snackbarService: SnackbarService) {
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createtransactionSubTypeAddEditForm();
    this.getUxTransactionType();
    if (this.id) {
      this.getDetailsById();
    } else {
      this.subTypes = this.getSubtypesListArray;
      this.subTypes.push(this.createDescriptionListModel());
      this.documents = this.getDescriptionDocumentsModelListArray(0)
      this.documents.push(this.createDescriptionDocumentsModel());
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BANK_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  changeTransactionTYpe() {
    let id = this.transactionSubtypeAddEditForm.value.invoiceTransactionTypeId;
    this.transactionSubtypeAddEditForm.get('invoiceTransactionDescriptionId').setValue('');
    if (this.subTypes) {
      this.subTypes.clear();
    }
    if (this.documents) {
      this.documents.clear();
    }
    this.getUxTransactionDescriptions(id);
  }

  changeTransactionDescription() {
    let id = this.transactionSubtypeAddEditForm.value.invoiceTransactionDescriptionId;
    this.id = id;
    this.getDetailsById();
  }

  //Create FormArray controls
  createDescriptionListModel(subTypes?: SubTypes): FormGroup {
    let descriptionsListModelControl = new SubTypes(subTypes);
    let formControls = {};
    Object.keys(descriptionsListModelControl).forEach((key) => {
      if (key === 'documents') {
        formControls[key] = this.formBuilder.array([])
      } else if (key === 'subTypeDescription') {
        formControls[key] = [{ value: descriptionsListModelControl[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: descriptionsListModelControl[key], disabled: false }]

      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createDescriptionDocumentsModel(descriptionDocuments?: Documents): FormGroup {
    let descriptionDocumentsModel = new Documents(descriptionDocuments);
    let formControls = {};
    Object.keys(descriptionDocumentsModel).forEach((key) => {
      //  if(key === 'documentName'){
      //     formControls[key] = [{ value: descriptionDocumentsModel[key], disabled: false }, [Validators.required]]
      //   }else{
      formControls[key] = [{ value: descriptionDocumentsModel[key], disabled: false }]

      // }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getSubtypesListArray(): FormArray {
    if (!this.transactionSubtypeAddEditForm) return;
    return this.transactionSubtypeAddEditForm.get("subTypes") as FormArray;
  }

  //Create FormArray
  getDescriptionDocumentsModelListArray(index: number): FormArray {
    if (!this.transactionSubtypeAddEditForm) return;
    return this.subTypes.at(index).get("documents") as FormArray

  }

  getUxTransactionType() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.transationTypes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getUxTransactionDescriptions(id) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_DESCRIPTION, null, false, prepareRequiredHttpParams({
      InvoiceTransactionTypeId: id
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.transationDescriptions = [];
          this.transationDescriptions = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_SUBTYPES, this.id, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.details = response.resources;
          if (this.subTypes) {
            this.subTypes.clear();
          }
          if (this.documents) {
            this.documents.clear();
          }
          this.transactionSubtypeAddEditForm.patchValue(this.details);
          this.subTypes = this.getSubtypesListArray;
          let id = this.transactionSubtypeAddEditForm.value.invoiceTransactionTypeId;
          this.getUxTransactionDescriptions(id);
          response.resources.subTypes.forEach((serviceCategoryListModel: any, index) => {
            this.subTypes.push(this.createDescriptionListModel(serviceCategoryListModel));

            serviceCategoryListModel.documents.forEach((descriptionDocuments: Documents, j) => {
              let descriptionDocumentsModel = new Documents(descriptionDocuments);

              this.documents = this.getDescriptionDocumentsModelListArray(index)
              this.documents.push(this.createDescriptionDocumentsModel(descriptionDocumentsModel));


            });


          });
          if (response.resources.subTypes.length == 0) {
            this.subTypes = this.getSubtypesListArray;
            this.subTypes.push(this.createDescriptionListModel());
            this.documents = this.getDescriptionDocumentsModelListArray(0)
            this.documents.push(this.createDescriptionDocumentsModel());
          }
          this.transactionSubtypeAddEditForm.disable();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  addDescription() {
    if (this.getSubtypesListArray.invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };

    this.subTypes = this.getSubtypesListArray;
    let descriptionsListModelControl = new SubTypes();

    this.subTypes.insert(0, this.createDescriptionListModel(descriptionsListModelControl));

    this.documents = this.getDescriptionDocumentsModelListArray(0);
    let descriptionDocumentsModel = new Documents();

    this.documents.insert(0, this.createDescriptionDocumentsModel(descriptionDocumentsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  //Add Employee Details
  addDocument(i, j): void {
    if (this.getSubtypesListArray.invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.getDescriptionDocumentsModelListArray(i).invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };
    this.documents = this.getDescriptionDocumentsModelListArray(i);
    let descriptionDocumentsModel = new Documents();

    this.documents.insert(0, this.createDescriptionDocumentsModel(descriptionDocumentsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  createtransactionSubTypeAddEditForm(): void {
    let transactionDescriptionSubTypeAddEditModel = new TransactionDescriptionSubTypeAddEditModel();
    this.transactionSubtypeAddEditForm = this.formBuilder.group({
      subTypes: this.formBuilder.array([])
    });
    Object.keys(transactionDescriptionSubTypeAddEditModel).forEach((key) => {
      this.transactionSubtypeAddEditForm.addControl(key, new FormControl(transactionDescriptionSubTypeAddEditModel[key]));
    });
    this.transactionSubtypeAddEditForm = setRequiredValidator(this.transactionSubtypeAddEditForm, ["invoiceTransactionDescriptionId", "invoiceTransactionTypeId"]);
    this.transactionSubtypeAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
  }
  //Get Details
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID,
      this.id
    );
  }


  changeEnable(e, obj) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        ids: obj.value.invoiceTransactionDescriptionSubTypeDocumentId,
        isActive: e.value,
        modifiedUserId: this.loggedUser.userId,
        moduleName: ModulesBasedApiSuffix.BILLING,
        apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_DESCRIPTION_DOCUMENT_SUBTYPE_DISABLE
      },
    });
    ref.onClose.subscribe((result) => {
      this.subTypes.clear();
      this.getDetailsById();

      if (!result) {
        // this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }
  edit() {

    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isEdit = true;
    this.transactionSubtypeAddEditForm.enable();
  }
  onSubmit(): void {

    if (this.transactionSubtypeAddEditForm.invalid) {
      return;
    }



    let formValue = this.transactionSubtypeAddEditForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_SUBTYPES, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/billing/transaction-configuration/list?tab=2');
      }
    });
  }
}
