

import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {  CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,  ResponseMessageTypes,  RxjsService, SnackbarService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { TransactionTypeAddEditModel } from '@modules/others/configuration/models/stock-id.model';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { TransactionTypeAddEditComponent } from './transaction-type-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';

@Component({
  selector: 'app-transaction-description-list',
  templateUrl: './transaction-configuration-list.component.html',
  styleUrls: ['./transaction-configuration-list.component.scss']
})
export class TransactionConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {

  @ViewChildren(Table) tables: QueryList<Table>;
  group: any;
  activeStatus: any = []
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  today: any = new Date()
  searchColumns: any
  pageSize: number = 10;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private tableFilterFormService: TableFilterFormService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private snackbarService: SnackbarService,
    private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder,) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Transaction types and description config",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Transaction', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Transaction types',
            dataKey: 'invoiceTransactionTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'transactionType', header: 'Transaction Type', width: '200px' },
              { field: 'transactionTypeDescription', header: 'Transaction Type Description', width: '200px' },
              { field: 'createdBy', header: 'Created By', width: '200px' },
              { field: 'createdDate', header: 'Created Date', width: '200px' },
              { field: 'isActive', header: 'Status', width: '150px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enbableAddActionBtn: true,
            disabled:true
          },
          {
            caption: 'Description',
            dataKey: 'invoiceTransactionTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: true,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'transactionType', header: 'Transaction Type', width: '200px' },

            ],
            apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_DESCRIPTION,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enbableAddActionBtn: true,
            disabled:true
          },
          {
            caption: 'Types and Description Mapping',
            dataKey: 'invoiceTransactionTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: true,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'transactionType', header: 'Transaction Type', width: '200px' },

            ],
            apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_SUBTYPES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enbableAddActionBtn: true,
            disabled:true
          },
          {
            caption: 'Process Type Transaction Description Mapping',
            dataKey: 'invoiceTransactionTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'transactionType', header: 'Transaction Type', width: '200px' },
              { field: 'transactionTypeDescription', header: 'Transaction Type Description', width: '200px' },
              { field: 'createdBy', header: 'Created By', width: '200px' },
              { field: 'createdDate', header: 'Created Date', width: '200px' },
              { field: 'isActive', header: 'Status', width: '150px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enbableAddActionBtn: true,
            disabled:true
          },
        ]

      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
    this.activeStatus = [
      { label: 'Enabled', value: 'Enabled' },
      { label: 'Disabled', value: 'Disabled' },
    ]
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.TRANSACTIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  expandRow(rowIndex, i, obj) {
    obj.isExpand = !obj.isExpand;
    this.dataList[rowIndex].descriptions[i].isExpand = obj.isExpand;
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode === 200) {
        this.dataList = data.resources;

        if (this.selectedTabIndex == 2) {
          this.dataList.forEach(element => {
            element.descriptions.forEach(dt => {
              dt.isExpand = false;
            });
          });
        }


        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }



  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }

        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (row['searchColumns']) {
            Object.keys(row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
              }
              else {
                otherParams[key] = row['searchColumns'][key];
              }
            });
          }

          if (row['sortOrderColumn']) {
            otherParams['sortOrder'] = row['sortOrder'];
            otherParams['sortOrderColumn'] = row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      default:
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            editableObject = new TransactionTypeAddEditModel();

            const dialogReff = this.dialog.open(TransactionTypeAddEditComponent, { width: '700px', data: editableObject, disableClose: true });
            dialogReff.afterClosed().subscribe(result => {
              if (result) {
                this.getRequiredListData();
              }
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            })
            break;
          case 1:
            this.router.navigate(["billing/transaction-configuration/list/transaction-description/add-edit"]);
            break;
          case 2:
            this.router.navigate(["billing/transaction-configuration/list/transaction-subtype/add-edit"]);
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            const dialogReff = this.dialog.open(TransactionTypeAddEditComponent, { width: '700px', data: editableObject, disableClose: true });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {

              if (ele) {
                this.getRequiredListData();
              }
            })
            break;
          case 1:
            this.router.navigate(["billing/transaction-configuration/list/view/transaction-description"], { queryParams: { id: editableObject['invoiceTransactionTypeId'] } });
            break;
          case 2:
            this.router.navigate(["billing/transaction-configuration/list/transaction-subtype/add-edit"], { queryParams: { id: editableObject['invoiceTransactionDescriptionId'] } });
            break;
        }
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["billing/transaction-configuration/transaction-type/list/add-edit"], { queryParams: { id: editableObject['invoiceTransactionTypeId'] } });
            break;
          case 1:
            this.router.navigate(["billing/transaction-configuration/transaction-type/list/add-edit"], { queryParams: { id: editableObject['invoiceTransactionTypeId'] } });
            break;
        }
    }
  }

  onTabChange(event) {

    if (event.index == 3) {
      this.router.navigate(['/billing/process-type-transaction-description-mapping'], { queryParams: { tab: this.selectedTabIndex } })
      return
    }
    this.tables.forEach(table => { //to set default row count list
      table.rows = 10
    })

    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.getRequiredListData()

  }

  onChangeStatus(rowData, index,subIndex?: string | number | undefined | null,subSubIndex?: string | number | undefined | null,) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: this.selectedTabIndex == 0 ? rowData.invoiceTransactionTypeId : this.selectedTabIndex == 2 ? rowData.invoiceTransactionDescriptionSubTypeId : rowData.invoiceTransactionDescriptionId,
        isActive: rowData.isActive == true ? true : false,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        if(this.selectedTabIndex == 0){
          this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
        }else if(this.selectedTabIndex == 1){
          this.dataList[index].descriptions[subIndex].isActive = this.dataList[index].descriptions[subIndex].isActive ? false : true;
        }else if(this.selectedTabIndex == 2){
          this.dataList[index].descriptions[subIndex].subTypes[subSubIndex].isActive = this.dataList[index].descriptions[subIndex].subTypes[subSubIndex].isActive ? false : true;
        }else{
          this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
        }
      }
    });
  }

  contactNumber(rowData) { }
}

















// import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
// import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
// import { MatDialog } from '@angular/material';
// import { ActivatedRoute, Router } from '@angular/router';
// import { AppState } from '@app/reducers';
// import { CommonPaginationConfig, createOrUpdateFilteredFieldKeyValues, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, filterFormControlNamesWhichHasValues, findPropsDifference, HttpCancelService, IPatchTableFilteredFields, LoggedInUserModel, ModulesBasedApiSuffix, OtherService, patchPersistedNgrxFieldValuesToForm, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, preparePTableLoadDataConfiguration, ResponseMessageTypes, RxjsService, SnackbarService, SplitSearchColumnFieldsFromObject, tableFilteredDataSelector$ } from '@app/shared';
// import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
// import { CrudService } from '@app/shared/services';
// import { TableFilterFormService } from '@app/shared/services/create-form.services';
// import { MomentService } from '@app/shared/services/moment.service';
// import { loggedInUserData } from '@modules/others';
// import { TransactionTypeAddEditModel } from '@modules/others/configuration/models/stock-id.model';
// import { BillingModuleApiSuffixModels } from '@modules/sales';
// import { Store } from '@ngrx/store';
// import { DialogService } from 'primeng/api';
// import { Table } from 'primeng/table';
// import { combineLatest, of } from 'rxjs';
// import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
// import { TransactionTypeAddEditComponent } from './transaction-type-add-edit.component';
// import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
// import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
// @Component({
//   selector: 'app-transaction-description-list',
//   templateUrl: './transaction-configuration-list.component.html',
//   styleUrls: ['./transaction-configuration-list.component.scss']
// })
// export class TransactionConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {
//   @ViewChildren(Table) tables: QueryList<Table>;
//   group: any;
//   activeStatus: any = []
//   searchKeyword: FormControl;
//   searchForm: FormGroup
//   columnFilterForm: FormGroup;
//   today: any = new Date()
//   searchColumns: any
//   pageSize: number = 10;

//   constructor(private crudService: CrudService,
//     private activatedRoute: ActivatedRoute,
//     private tableFilterFormService: TableFilterFormService,
//     private dialog: MatDialog, private httpCancelService: HttpCancelService,
//     public dialogService: DialogService,
//     private snackbarService: SnackbarService,
//     private router: Router, private otherService: OtherService,
//     private store: Store<AppState>, private momentService: MomentService,
//     private rxjsService: RxjsService, private _fb: FormBuilder,) {
//     super();
//     this.primengTableConfigProperties = {
//       tableCaption: "Transaction types and description config",
//       selectedTabIndex: 0,
//       breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Transaction', relativeRouterUrl: '' }],
//       tableComponentConfigs: {
//         tabsList: [
//           {
//             caption: 'Transaction types',
//             dataKey: 'invoiceTransactionTypeId',
//             enableBreadCrumb: true,
//             enableExportCSV: false,
//             enableExportExcel: false,
//             enableExportCSVSelected: false,
//             enableReset: false,
//             enableGlobalSearch: false,
//             reorderableColumns: false,
//             resizableColumns: false,
//             enableScrollable: true,
//             checkBox: false,
//             enableRowDelete: false,
//             enableStatusActiveAction: false,
//             enableFieldsSearch: true,
//             rowExpantable: false,
//             rowExpantableIndex: 0,
//             enableHyperLink: true,
//             cursorLinkIndex: 0,
//             enableSecondHyperLink: false,
//             cursorSecondLinkIndex: 1,
//             columns: [
//               { field: 'transactionType', header: 'Transaction Type', width: '200px' },
//               { field: 'transactionTypeDescription', header: 'Transaction Type Description', width: '200px' },
//               { field: 'createdBy', header: 'Created By', width: '200px' },
//               { field: 'createdDate', header: 'Created Date', width: '200px' },
//               { field: 'isActive', header: 'Status', width: '150px' },
//             ],
//             apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES,
//             moduleName: ModulesBasedApiSuffix.BILLING,
//             enableMultiDeleteActionBtn: false,
//             enbableAddActionBtn: true,
//             disabled: true
//           },
//           {
//             caption: 'Description',
//             dataKey: 'invoiceTransactionTypeId',
//             enableBreadCrumb: true,
//             enableExportCSV: false,
//             enableExportExcel: false,
//             enableExportCSVSelected: false,
//             enableReset: false,
//             enableGlobalSearch: false,
//             reorderableColumns: false,
//             resizableColumns: false,
//             enableScrollable: true,
//             checkBox: false,
//             enableRowDelete: false,
//             enableStatusActiveAction: false,
//             enableFieldsSearch: true,
//             rowExpantable: true,
//             rowExpantableIndex: 0,
//             enableHyperLink: true,
//             cursorLinkIndex: 0,
//             enableSecondHyperLink: false,
//             cursorSecondLinkIndex: 1,
//             columns: [
//               { field: 'transactionType', header: 'Transaction Type', width: '200px' },
//             ],
//             apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_DESCRIPTION,
//             moduleName: ModulesBasedApiSuffix.BILLING,
//             enableMultiDeleteActionBtn: false,
//             enbableAddActionBtn: true,
//             disabled: true
//           },
//           {
//             caption: 'Types and Description Mapping',
//             dataKey: 'invoiceTransactionTypeId',
//             enableBreadCrumb: true,
//             enableExportCSV: false,
//             enableExportExcel: false,
//             enableExportCSVSelected: false,
//             enableReset: false,
//             enableGlobalSearch: false,
//             reorderableColumns: false,
//             resizableColumns: false,
//             enableScrollable: true,
//             checkBox: false,
//             enableRowDelete: false,
//             enableStatusActiveAction: false,
//             enableFieldsSearch: true,
//             rowExpantable: true,
//             rowExpantableIndex: 0,
//             enableHyperLink: false,
//             cursorLinkIndex: 0,
//             enableSecondHyperLink: false,
//             cursorSecondLinkIndex: 1,
//             columns: [
//               { field: 'transactionType', header: 'Transaction Type', width: '200px' },
//             ],
//             apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_SUBTYPES,
//             moduleName: ModulesBasedApiSuffix.BILLING,
//             enableMultiDeleteActionBtn: false,
//             enbableAddActionBtn: true,
//             disabled: true
//           },
//           {
//             caption: 'Process Type Transaction Description Mapping',
//             dataKey: 'invoiceTransactionTypeId',
//             enableBreadCrumb: true,
//             enableExportCSV: false,
//             enableExportExcel: false,
//             enableExportCSVSelected: false,
//             enableReset: false,
//             enableGlobalSearch: false,
//             reorderableColumns: false,
//             resizableColumns: false,
//             enableScrollable: true,
//             checkBox: false,
//             enableRowDelete: false,
//             enableStatusActiveAction: false,
//             enableFieldsSearch: true,
//             rowExpantable: false,
//             rowExpantableIndex: 0,
//             enableHyperLink: true,
//             cursorLinkIndex: 0,
//             enableSecondHyperLink: false,
//             cursorSecondLinkIndex: 1,
//             columns: [
//               { field: 'transactionType', header: 'Transaction Type', width: '200px' },
//               { field: 'transactionTypeDescription', header: 'Transaction Type Description', width: '200px' },
//               { field: 'createdBy', header: 'Created By', width: '200px' },
//               { field: 'createdDate', header: 'Created Date', width: '200px' },
//               { field: 'isActive', header: 'Status', width: '150px' },
//             ],
//             apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES,
//             moduleName: ModulesBasedApiSuffix.BILLING,
//             enableMultiDeleteActionBtn: false,
//             enbableAddActionBtn: true,
//             disabled: true
//           },
//         ]
//       }
//     }
//     this.searchForm = this._fb.group({ searchKeyword: "" });
//     this.columnFilterForm = this._fb.group({});
//     this.activatedRoute.queryParamMap.subscribe((params) => {
//       this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
//       this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
//       this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
//     });
//     this.activeStatus = [
//       { label: 'Enabled', value: 'Enabled' },
//       { label: 'Disabled', value: 'Disabled' },
//     ]
//   }

//   ngOnInit(): void {
//     this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
//     this.searchKeywordRequest();
//     this.columnFilterRequest()
//     this.combineLatestNgrxStoreData()
//     this.getRequiredListData();
//     // start logic table fields search persistant
//     combineLatest([
//       // Little trick is added to validate and retrieve the field searches across the multiple tab based tables
//       this.rxjsService.getAnyPropertyValue(),
//       this.store.select(tableFilteredDataSelector$)
//     ]).subscribe((observables) => {
//       if (observables[0]) {
//         this.areColumnFiltersEdited = false;
//       }
//       if (!observables[1]) return;
//       this.tableFilteredFields = observables[1];
//       setTimeout(() => {
//         // Give some milliseconds to render with proper history of fields search values and tabl results
//         if (Object.keys(this.tableFilteredFields?.[this.otherService.currentUrl] ?
//           this.tableFilteredFields[this.otherService.currentUrl] : {}).length > 2) {
//           this.organizeRowVariableAsPerTabIndex();
//           let { columnFilterForm, row }: IPatchTableFilteredFields = patchPersistedNgrxFieldValuesToForm(this.columnFilterForm, this.tableFilteredFields, this.returnRowObjectWithIndexKey(), this.otherService,
//             this.httpCancelService, this.areColumnFiltersEdited);
//           this.columnFilterForm = columnFilterForm;
//           this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = row;
//           let keyValuesObj = filterFormControlNamesWhichHasValues({ ...this.columnFilterForm.value });
//           if (Object.keys(keyValuesObj).length > 0) {
//             this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey(), null, false);
//           }
//         }
//       });
//     });
//     // end logic table fields search persistant
//   }

//   // start logic table fields search persistant
//   organizeRowVariableAsPerTabIndex() {
//     this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = {};
//   }

//   returnRowObjectWithIndexKey() {
//     return this.row[this.selectedTabIndex ? this.selectedTabIndex : 0]
//   }
//   // end logic table fields search persistant

//   combineLatestNgrxStoreData() {
//     combineLatest([
//       this.store.select(loggedInUserData),
//       this.store.select(currentComponentPageBasedPermissionsSelector$)]
//     ).subscribe((response) => {
//       this.loggedInUserData = new LoggedInUserModel(response[0]);
//       let permission = response[1][CONFIGURATION_COMPONENT.TRANSACTIONS]
//       if (permission) {
//         let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
//         this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
//       }
//     });
//   }

//   searchKeywordRequest() {
//     this.searchForm.valueChanges
//       .pipe(
//         debounceTime(debounceTimeForSearchkeyword),
//         distinctUntilChanged(),
//         switchMap(val => {
//           return of(this.onCRUDRequested(CrudType.GET, {}));
//         })
//       )
//       .subscribe();
//   }

//   columnFilterRequest() {
//     this.columnFilterForm.valueChanges
//       .pipe(
//         debounceTime(debounceTimeForSearchkeyword),
//         distinctUntilChanged(),
//         switchMap(obj => {
//           Object.keys(obj).forEach(key => {
//             if (obj[key] === "") {
//               delete obj[key]
//             }
//           });
//           this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
//           // start logic table fields search persistant
//           this.organizeRowVariableAsPerTabIndex();
//           if (this.tableFilteredFields[this.otherService.currentUrl]) {
//             this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = { ...this.tableFilteredFields[this.otherService.currentUrl] };
//             let sourceObj = SplitSearchColumnFieldsFromObject(this.tableFilteredFields[this.otherService.currentUrl]);
//             // Delete properties from the source object with comparing the destination object and return the source object
//             let filteredSearchColumns = findPropsDifference(sourceObj, this.searchColumns);
//             this.returnRowObjectWithIndexKey()['searchColumns'] = { ...filteredSearchColumns, ...this.searchColumns };
//             // this.returnRowObjectWithIndexKey()['pageIndex'] = this.searchFirst ? 0 : this.returnRowObjectWithIndexKey()['pageIndex'];
//             this.returnRowObjectWithIndexKey()['pageIndex'] = this.returnRowObjectWithIndexKey()['pageIndex'];
//           }
//           else {
//             this.organizeRowVariableAsPerTabIndex();
//             this.returnRowObjectWithIndexKey()['searchColumns'] = this.searchColumns;
//             this.returnRowObjectWithIndexKey()['pageIndex'] = +CommonPaginationConfig.defaultPageIndex;
//             this.returnRowObjectWithIndexKey()['pageSize'] = +CommonPaginationConfig.defaultPageSize;
//           }
//           // set to true to make sure the column filters are changed by the user
//           this.areColumnFiltersEdited = true;
//           // end logic table fields search persistant
//           return of(this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey()));
//         })
//       )
//       .subscribe();
//   }

//   expandRow(rowIndex, i, obj) {
//     obj.isExpand = !obj.isExpand;
//     this.dataList[rowIndex].descriptions[i].isExpand = obj.isExpand;
//   }

//   getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
//     this.loading = true;
//     let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
//     billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

//     this.crudService.get(
//       ModulesBasedApiSuffix.BILLING,
//       billingModuleApiSuffixModels,
//       undefined,
//       false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
//     ).subscribe(data => {
//       this.loading = false;
//       this.rxjsService.setGlobalLoaderProperty(false);
//       if (data.isSuccess && data.statusCode === 200) {
//         this.dataList = data.resources;

//         if (this.selectedTabIndex == 2) {
//           this.dataList.forEach(element => {
//             element.descriptions.forEach(dt => {
//               dt.isExpand = false;
//             });
//           });
//         }


//         this.totalRecords = data.totalCount;
//       } else {
//         this.dataList = null;
//         this.totalRecords = 0;

//       }
//     })
//   }

//   loadPaginationLazy(event) {
//     // start logic table fields search persistant
//     this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = preparePTableLoadDataConfiguration(event, this.searchColumns, this.tableFilteredFields, this.otherService, this.returnRowObjectWithIndexKey());
//     this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey());
//     // end logic table fields search persistant
//   }

//   onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string, shouldDispatchNgrxAction = true): void {
//     switch (type) {
//       case CrudType.CREATE:
//         if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
//           return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//         }
//         this.openAddEditPage(CrudType.CREATE, row);
//         break;
//       case CrudType.GET:
//         let otherParams = {};
//         if (this.searchForm.value.searchKeyword) {
//           otherParams["search"] = this.searchForm.value.searchKeyword;
//         }
//         if (Object.keys(row ? row : {}).length > 0) {
//           // logic for split columns and its values to key value pair
//           if (row['searchColumns']) {
//             Object.keys(row['searchColumns']).forEach((key) => {
//               if (key.toLowerCase().includes('period') || key.toLowerCase().includes('date') && row['searchColumns'][key] instanceof Date) {
//                 otherParams[key] = this.momentService.localToUTCDateTime(row['searchColumns'][key]);
//               } else {
//                 otherParams[key] = row['searchColumns'][key];
//               }
//             });
//           }
//           if (row['sortOrderColumn'] && row['sortOrder']) {
//             otherParams['sortOrder'] = row['sortOrder'];
//             otherParams['sortOrderColumn'] = row['sortOrderColumn'];
//           }
//         }
//         // start logic table fields search persistant
//         if ((row['pageIndex'] || row['pageIndex'] == 0) && row['pageSize']) {
//           otherParams['pageIndex'] = row['pageIndex'];
//           otherParams['pageSize'] = row['pageSize'];
//         }
//         createOrUpdateFilteredFieldKeyValues(this.tableFilteredFields, this.primengTableConfigProperties.tableComponentConfigs.tabsList,
//           otherParams, this.areColumnFiltersEdited, this.otherService, this.store, shouldDispatchNgrxAction);
//         this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = otherParams;
//         if ((row['pageIndex'] || row['pageIndex'] == 0) && row['pageSize']) {
//           delete otherParams['pageIndex'];
//           delete otherParams['pageSize'];
//         }
//         else if ((otherParams['pageIndex'] || otherParams['pageIndex'] == 0) && otherParams['pageSize']) {
//           row['pageIndex'] = otherParams['pageIndex'];
//           row['pageSize'] = otherParams['pageSize'];
//           delete otherParams['pageIndex'];
//           delete otherParams['pageSize'];
//         }
//         setTimeout(() => {
//           this.first = parseInt(row['pageIndex']) * parseInt(row['pageSize']);
//           this.pageSize = parseInt(row['pageSize']);
//           this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
//         });
//         // end logic table fields search persistant
//         break;
//       case CrudType.VIEW:
//         this.openAddEditPage(CrudType.VIEW, row);
//         break;
//       case CrudType.EDIT:
//         this.openAddEditPage(CrudType.EDIT, row);
//         break;
//       default:
//     }
//   }

//   openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
//     switch (type) {
//       case CrudType.CREATE:
//         switch (this.selectedTabIndex) {
//           case 0:
//             editableObject = new TransactionTypeAddEditModel();
//             const dialogReff = this.dialog.open(TransactionTypeAddEditComponent, { width: '700px', data: editableObject, disableClose: true });
//             dialogReff.afterClosed().subscribe(result => {
//               if (result) {
//                 this.getRequiredListData();
//               }
//             });
//             dialogReff.componentInstance.outputData.subscribe(ele => {
//               if (ele) {
//                 this.getRequiredListData();
//               }
//             })
//             break;
//           case 1:
//             this.router.navigate(["billing/transaction-configuration/list/transaction-description/add-edit"]);
//             break;
//           case 2:
//             this.router.navigate(["billing/transaction-configuration/list/transaction-subtype/add-edit"]);
//             break;
//         }
//         break;
//       case CrudType.VIEW:
//         switch (this.selectedTabIndex) {
//           case 0:
//             if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//               return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//             }
//             const dialogReff = this.dialog.open(TransactionTypeAddEditComponent, { width: '700px', data: editableObject, disableClose: true });
//             dialogReff.afterClosed().subscribe(result => {
//             });
//             dialogReff.componentInstance.outputData.subscribe(ele => {
//               if (ele) {
//                 this.getRequiredListData();
//               }
//             })
//             break;
//           case 1:
//             this.router.navigate(["billing/transaction-configuration/list/view/transaction-description"], { queryParams: { id: editableObject['invoiceTransactionTypeId'] } });
//             break;
//           case 2:
//             this.router.navigate(["billing/transaction-configuration/list/transaction-subtype/add-edit"], { queryParams: { id: editableObject['invoiceTransactionDescriptionId'] } });
//             break;
//         }
//         break;
//       case CrudType.EDIT:
//         switch (this.selectedTabIndex) {
//           case 0:
//             this.router.navigate(["billing/transaction-configuration/transaction-type/list/add-edit"], { queryParams: { id: editableObject['invoiceTransactionTypeId'] } });
//             break;
//           case 1:
//             this.router.navigate(["billing/transaction-configuration/transaction-type/list/add-edit"], { queryParams: { id: editableObject['invoiceTransactionTypeId'] } });
//             break;
//         }
//     }
//   }

//   onTabChange(event) {
//     if (event.index == 3) {
//       this.router.navigate(['/billing/process-type-transaction-description-mapping'], { queryParams: { tab: this.selectedTabIndex } })
//       return;
//     }
//     this.row = {}
//     this.columnFilterForm = this._fb.group({})
//     this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
//     this.columnFilterRequest();
//     this.dataList = [];
//     this.totalRecords = null;
//     this.selectedTabIndex = event.index;
//     // Little trick is added to validate and retrieve the field searches across the multiple tab based tables
//     this.rxjsService.setAnyPropertyValue({ tabChanged: true });
//   }

//   onChangeStatus(rowData, index, subIndex?: string | number | undefined | null, subSubIndex?: string | number | undefined | null,) {
//     const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
//       showHeader: false,
//       baseZIndex: 10000,
//       width: '400px',
//       data: {
//         index: index,
//         ids: this.selectedTabIndex == 0 ? rowData.invoiceTransactionTypeId : this.selectedTabIndex == 2 ? rowData.invoiceTransactionDescriptionSubTypeId : rowData.invoiceTransactionDescriptionId,
//         isActive: rowData.isActive == true ? true : false,
//         modifiedUserId: this.loggedInUserData.userId,
//         moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
//         apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
//       },
//     });
//     ref.onClose.subscribe((result) => {
//       if (!result) {
//         if (this.selectedTabIndex == 0) {
//           this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
//         } else if (this.selectedTabIndex == 1) {
//           this.dataList[index].descriptions[subIndex].isActive = this.dataList[index].descriptions[subIndex].isActive ? false : true;
//         } else if (this.selectedTabIndex == 2) {
//           this.dataList[index].descriptions[subIndex].subTypes[subSubIndex].isActive = this.dataList[index].descriptions[subIndex].subTypes[subSubIndex].isActive ? false : true;
//         } else {
//           this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
//         }
//       }
//     });
//   }

//   contactNumber(rowData) { }
// }
