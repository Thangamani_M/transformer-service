
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales';

@Component({
  selector: 'app-description-subtype-view',
  templateUrl: './transaction-description-subtype-view.component.html',
  styleUrls: ['./transaction-configuration-list.component.scss']
})
export class TransactionDescriptionSubTypeViewComponent implements OnInit {
  stockId: string;
  stockIdDetails: any;
  id: any;
  details: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.id = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getDetailsById();
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_SUBTYPES, this.id, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.details = response.resources;


        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  edit() {
    alert("SA")
    if (this.id) {
      this.router.navigate(["billing/transaction-configuration/transaction-subtype/add-edit"], { queryParams: { id: this.id } });

    }
  }
}
