
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { DescriptionDocuments, Descriptions, TransactionDescriptionAddEditModel } from '@modules/others/configuration/models/stock-id.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-transaction-description-add-edit',
  templateUrl: './transaction-description-add-edit.component.html',
  styleUrls: ['./transaction-configuration-list.component.scss']
})
export class TransactionDescriptionEditComponent implements OnInit {
  id: any
  divisionDropDown: any = [];
  transactionDescriptionAddEditForm: FormGroup;
  descriptions: FormArray;
  descriptionDocuments: FormArray;
  rOCommunityPatrolTypesList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  stockIdDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });

  @ViewChildren('input') rows: QueryList<any>;

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  transationTypes: any;
  details: any;
  loggedInUserData: LoggedInUserModel;

  constructor(private activatedRoute: ActivatedRoute, private dialogService: DialogService,
    private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createtransactionDescriptionAddEditForm();
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getUxTransactionType();
    if (this.id) {
      this.getDetailsById();
    } else {
      this.descriptions = this.getDescriptionsListArray
      this.descriptions.push(this.createDescriptionListModel());

      this.descriptionDocuments = this.getDescriptionDocumentsModelListArray(0)
      this.descriptionDocuments.push(this.createDescriptionDocumentsModel());
    }



  }

  changeTransactionType() {
    this.id = this.transactionDescriptionAddEditForm.get('invoiceTransactionTypeId').value;
    this.getDetailsById();
  }

  //Create FormArray controls
  createDescriptionListModel(descriptions?: Descriptions): FormGroup {
    let descriptionsListModelControl = new Descriptions(descriptions);
    let formControls = {};
    Object.keys(descriptionsListModelControl).forEach((key) => {
      if (key === 'descriptionDocuments') {
        formControls[key] = this.formBuilder.array([])
      } else if (key === 'transactionDescription') {
        formControls[key] = [{ value: descriptionsListModelControl[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: descriptionsListModelControl[key], disabled: false }]

      }
    });
    return this.formBuilder.group(formControls);
  }



  //Create FormArray controls
  createDescriptionDocumentsModel(descriptionDocuments?: DescriptionDocuments): FormGroup {
    let descriptionDocumentsModel = new DescriptionDocuments(descriptionDocuments);
    let formControls = {};
    Object.keys(descriptionDocumentsModel).forEach((key) => {

      formControls[key] = [{ value: descriptionDocumentsModel[key], disabled: false }]

    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getDescriptionsListArray(): FormArray {
    if (!this.transactionDescriptionAddEditForm) return;
    return this.transactionDescriptionAddEditForm.get("descriptions") as FormArray;
  }

  //Create FormArray
  getDescriptionDocumentsModelListArray(index: number): FormArray {
    if (!this.transactionDescriptionAddEditForm) return;
    return this.descriptions.at(index).get("descriptionDocuments") as FormArray

  }

  getUxTransactionType() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.transationTypes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_DESCRIPTION, this.id, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.details = response.resources;

          if (this.descriptions) {
            this.descriptions.clear();
          }
          if (this.descriptionDocuments) {
            this.descriptionDocuments.clear();
          }

          this.transactionDescriptionAddEditForm.patchValue(this.details);
          this.descriptions = this.getDescriptionsListArray;

          response.resources.descriptions.forEach((serviceCategoryListModel: any, index) => {
            this.descriptions.push(this.createDescriptionListModel(serviceCategoryListModel));

            serviceCategoryListModel.descriptionDocuments.forEach((descriptionDocuments: DescriptionDocuments, j) => {
              let descriptionDocumentsModel = new DescriptionDocuments(descriptionDocuments);

              this.descriptionDocuments = this.getDescriptionDocumentsModelListArray(index)
              this.descriptionDocuments.push(this.createDescriptionDocumentsModel(descriptionDocumentsModel));


            });


          });

          if (response.resources.descriptions.length == 0) {
            this.descriptions = this.getDescriptionsListArray
            this.descriptions.push(this.createDescriptionListModel());

            this.descriptionDocuments = this.getDescriptionDocumentsModelListArray(0)
            this.descriptionDocuments.push(this.createDescriptionDocumentsModel());

          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  addDescription() {
    if (this.getDescriptionsListArray.invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };

    this.descriptions = this.getDescriptionsListArray;
    let descriptionsListModelControl = new Descriptions();

    this.descriptions.insert(0, this.createDescriptionListModel(descriptionsListModelControl));

    this.descriptionDocuments = this.getDescriptionDocumentsModelListArray(0);
    let descriptionDocumentsModel = new DescriptionDocuments();

    this.descriptionDocuments.insert(0, this.createDescriptionDocumentsModel(descriptionDocumentsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  //Add Employee Details
  addDocument(i, j): void {
    if (this.getDescriptionsListArray.invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.getDescriptionDocumentsModelListArray(i).invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };
    this.descriptionDocuments = this.getDescriptionDocumentsModelListArray(i);
    let descriptionDocumentsModel = new DescriptionDocuments();

    this.descriptionDocuments.insert(0, this.createDescriptionDocumentsModel(descriptionDocumentsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  createtransactionDescriptionAddEditForm(): void {
    let transactionDescriptionAddEditModel = new TransactionDescriptionAddEditModel();
    this.transactionDescriptionAddEditForm = this.formBuilder.group({
      descriptions: this.formBuilder.array([])
    });
    Object.keys(transactionDescriptionAddEditModel).forEach((key) => {
      this.transactionDescriptionAddEditForm.addControl(key, new FormControl(transactionDescriptionAddEditModel[key]));
    });
    this.transactionDescriptionAddEditForm = setRequiredValidator(this.transactionDescriptionAddEditForm, ["invoiceTransactionTypeId"]);
    this.transactionDescriptionAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
  }
  //Get Details
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID,
      this.id
    );
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  changeEnable(e, obj, index, jindex) {
    // if (obj?.value?.invoiceTransactionDescriptionDocumentId) {
    //   const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
    //     // header: 'Choose a Car',
    //     showHeader: false,
    //     baseZIndex: 10000,
    //     width: '400px',
    //     data: {
    //       ids: obj.value.invoiceTransactionDescriptionDocumentId,
    //       isActive: e.value,
    //       modifiedUserId: this.loggedInUserData.userId,
    //       moduleName: ModulesBasedApiSuffix.BILLING,
    //       apiSuffixModel: BillingModuleApiSuffixModels.INVOICE_TRANSACTION_DESCRIPTION_DOCUMENT_DISABLE
    //     },
    //   });
    //   ref.onClose.subscribe((result) => {
    //     this.descriptions.clear();
    //     this.getDetailsById();

    //     if (!result) {
    //       // this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
    //     }
    //   });
    // }
    //this.getDescriptionDocumentsModelListArray(index)?.controls[jindex].get('isActive').setValue(e.value);
    // this.descriptionDocuments.controls[index].get('isActive').setValue(e.value);

    // //this.descriptions?.controls[index].get('isActive').setValue(e.value);
    // obj.get('isActive').setValue(e.value)
  }

  onSubmit(): void {

    if (this.transactionDescriptionAddEditForm.invalid) {
      return;
    }



    let formValue = this.transactionDescriptionAddEditForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_DESCRIPTION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/billing/transaction-configuration/list?tab=1');
      }
    });
  }
}
