import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionConfigurationListComponent } from './transaction-configuration-list.component';
import { TransactionDescriptionEditComponent } from './transaction-description-add-edit.component';
import { TransactionDescriptionSubTypeViewComponent } from './transaction-description-subtype-view.component';
import { TransactionDescriptionViewComponent } from './transaction-description-view.component';
import { TransactionSubTypeEditComponent } from './transaction-subtype-add-edit.component';
import { TransactionTypeAddEditComponent } from './transaction-type-add-edit.component';
import { TransactionTypeViewComponent } from './transaction-type-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: TransactionConfigurationListComponent, canActivate: [AuthGuard], data: { title: 'Transaction Configuration List' } },
  { path: 'list/view/transaction-type', component: TransactionTypeViewComponent, canActivate: [AuthGuard], data: { title: 'Transaction Type View' } },
  { path: 'list/view/transaction-description', component: TransactionDescriptionViewComponent, canActivate: [AuthGuard], data: { title: 'Transaction Description View' } },
  { path: 'list/transaction-type/add-edit', component: TransactionTypeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Transaction Type Add Edit' } },
  { path: 'list/transaction-description/add-edit', component: TransactionDescriptionEditComponent, canActivate: [AuthGuard], data: { title: 'Transaction Description Add Edit' } },
  { path: 'list/transaction-subtype/add-edit', component: TransactionSubTypeEditComponent, canActivate: [AuthGuard], data: { title: 'Transaction Sub Type Add Edit' } },
  { path: 'list/view/transaction-subtype', component: TransactionDescriptionSubTypeViewComponent, canActivate: [AuthGuard], data: { title: 'Transaction Description SubType View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class TransctionDescriptionRoutingModule { }
