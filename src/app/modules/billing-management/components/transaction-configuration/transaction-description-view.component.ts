
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-description-id-view',
  templateUrl: './transaction-description-view.component.html',
  styleUrls: ['./transaction-configuration-list.component.scss']
})
export class TransactionDescriptionViewComponent implements OnInit {


  stockId: string;
  stockIdDetails: any;
  id: any;
  details: any;
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.id = this.activatedRoute.snapshot.queryParams.id
    this.primengTableConfigProperties = {
      tableCaption: "View Transaction Description",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing', relativeRouterUrl: '' },
      { displayName: 'Transactions', relativeRouterUrl: '/billing/transaction-configuration', queryParams: { tab: 1 } }, { displayName: 'View Transaction Description' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getDetailsById(this.id);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.TRANSACTIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDetailsById(stockId: string) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_DESCRIPTION, this.id, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          let des = [];
          response.resources?.descriptions.forEach(item=>{
            des.push(item.transactionDescription)
          })
          this.viewData = [
            { name: "Transaction Type", value: response.resources?.transactionType, order: 1 },
            { name: "Created By", value: response.resources?.createdBy, order: 2 },
            { name: "Created Date", value: response.resources?.createdDate, order: 3 },
            { name: 'Status', order:4, value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
            { name: "Description", value: des.join(","), order: 5 },
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.edit()
        break;
    }
  }


  edit() {
    if (this.id) {
      this.router.navigate(["/billing/transaction-configuration/list/transaction-description/add-edit"], { queryParams: { id: this.id } });

    }
  }
}
