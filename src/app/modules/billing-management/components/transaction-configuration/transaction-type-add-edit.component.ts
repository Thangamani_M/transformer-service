
import { Component, EventEmitter, Inject, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatOption, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { TransactionTypeAddEditModel } from '@modules/others/configuration/models/stock-id.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-transaction-type-add-edit',
  templateUrl: './transaction-type-add-edit.component.html',
  styleUrls: ['./transaction-configuration-list.component.scss']
})
export class TransactionTypeAddEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  stockId: any
  divisionDropDown: any = [];
  transactionTypeAddEditForm: FormGroup;
  rOCommunityPatrolTypesList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  stockIdDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  showEdit: boolean = true;
  action;
  @ViewChildren('input') rows: QueryList<any>;

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  id: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialog: MatDialog, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.id = this.data.invoiceTransactionTypeId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.action = this.id ? this.showEdit ? 'View' : 'Update' : 'Create';
  }

  ngOnInit() {
    this.createtransactionTypeAddEditForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.id) {
      this.getStockIdDetailsById().subscribe((response: IApplicationResponse) => {

        this.rxjsService.setGlobalLoaderProperty(false);

        this.stockIdDetails = response.resources;
        this.transactionTypeAddEditForm.get('transactionType').disable();
        this.transactionTypeAddEditForm.get('transactionTypeDescription').disable();
        this.transactionTypeAddEditForm.patchValue(this.stockIdDetails);
      })
    }

  }

  createtransactionTypeAddEditForm(): void {
    let transactionTypeAddEditModel = new TransactionTypeAddEditModel();
    this.transactionTypeAddEditForm = this.formBuilder.group({
    });
    Object.keys(transactionTypeAddEditModel).forEach((key) => {
      this.transactionTypeAddEditForm.addControl(key, new FormControl(transactionTypeAddEditModel[key]));
    });
    this.transactionTypeAddEditForm = setRequiredValidator(this.transactionTypeAddEditForm, ["transactionTypeDescription", "transactionType"]);
    this.transactionTypeAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
  }
  //Get Details 
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES,
      this.id
    );
  }
  edit() {
    this.transactionTypeAddEditForm.get('transactionType').enable();
    this.transactionTypeAddEditForm.get('transactionTypeDescription').enable();
    this.showEdit = !this.showEdit;
    this.action = this.id ? this.showEdit ? 'View' : 'Update' : 'Create';
  }
  onSubmit(): void {

    if (this.transactionTypeAddEditForm.invalid) {
      return;
    }



    let formValue = this.transactionTypeAddEditForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.stockId) ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.outputData.emit(true);
        this.dialog.closeAll();
      }
    });
  }
}