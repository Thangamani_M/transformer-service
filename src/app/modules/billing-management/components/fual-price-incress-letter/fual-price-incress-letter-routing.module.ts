import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FualPriceIncressLetterComponent } from './fual-price-incress-letter.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: FualPriceIncressLetterComponent,canActivate:[AuthGuard],  data: { title: 'Fuel price increase letter list' }  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FualPriceIncressLetterRoutingModule { }
