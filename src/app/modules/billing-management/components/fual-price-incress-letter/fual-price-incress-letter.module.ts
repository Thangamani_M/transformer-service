import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { FualPriceIncressLetterAddEditComponent } from './fual-price-incress-letter-add-edit/fual-price-incress-letter-add-edit.component';
import { FualPriceIncressLetterRoutingModule } from './fual-price-incress-letter-routing.module';
import { FualPriceIncressLetterComponent } from './fual-price-incress-letter.component';

@NgModule({
  declarations: [FualPriceIncressLetterComponent, FualPriceIncressLetterAddEditComponent],
  imports: [
    CommonModule,
    FualPriceIncressLetterRoutingModule,
    ReactiveFormsModule, FormsModule,
    SharedModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule
  ],
  entryComponents:[FualPriceIncressLetterAddEditComponent]
})
export class FualPriceIncressLetterModule { }
