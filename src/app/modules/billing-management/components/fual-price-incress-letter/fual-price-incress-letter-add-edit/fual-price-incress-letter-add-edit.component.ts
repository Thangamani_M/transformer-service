import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-fual-price-incress-letter-add-edit',
  templateUrl: './fual-price-incress-letter-add-edit.component.html'
})
export class FualPriceIncressLetterAddEditComponent implements OnInit {

  formConfigs = formConfigs;
  alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
  fualPriceAddEditForm: FormGroup;
  loggedUser: UserLogin;
  divisionList: any;
  divisionListWithName: any;
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createbdiCodeForm();

    if (this.config.data?.fuelIncreaseLetterConfigId) {
      this.getDetails()
    }
  }


  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.FUEL_INCRESS_LETTER_CONFIG, this.config.data?.fuelIncreaseLetterConfigId).subscribe((response) => {
      if (response.statusCode == 200 && response.resources && response.isSuccess) {
        this.fualPriceAddEditForm.patchValue(response.resources);
      }
    })
  }

  createbdiCodeForm(): void {
    this.fualPriceAddEditForm = this.formBuilder.group({
      fuelIncreaseLetterConfigId: [''],
      letterContent: ['', [Validators.compose([Validators.required,Validators.minLength(2),Validators.maxLength(5000)])]],
      createdUserId: [this.loggedUser.userId]
    });
    this.fualPriceAddEditForm = setRequiredValidator(this.fualPriceAddEditForm, ["letterContent"]);
  }

  onSubmit(): void {
    if (this.fualPriceAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.FUEL_INCRESS_LETTER_CONFIG, this.fualPriceAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true)
        this.fualPriceAddEditForm.reset();

      }
    })
  }

  dialogClose(): void {
    this.ref.close(false)
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}

