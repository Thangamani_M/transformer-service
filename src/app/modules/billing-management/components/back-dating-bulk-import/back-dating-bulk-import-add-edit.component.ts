import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from 'primeng/api';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, debounceTimeForSearchkeyword, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { StateModel, UserModuleApiSuffixModels } from '@modules/user';
import { ItManagementApiSuffixModels, loggedInUserData } from "@modules/others";
import { BackDatingBulkImportaddModel } from '@modules/others/configuration/models/back-dating-bulk-import.model';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { BulkDetailsApprovalRequestComponent } from './bulk-details-approval-request.component';
import { SelectionModel } from '@angular/cdk/collections';
@Component({
  selector: 'app-back-dating-bulk-list-import-add-edit',
  templateUrl: './back-dating-bulk-import-add-edit.component.html',
  styleUrls: ['./back-dating-bulk-import-add.component.scss']
})
export class BackDatingBulkImportAddEditComponent implements OnInit {
  public formData = new FormData();
  transationTypes: any;
  selection = new SelectionModel<StateModel>(true, []);
  assignedToList: [];
  selectedFile: any = [];
  TransactionTypedropdown: any;
  observableResponse;
  TransactionType: any = [];
  Transactiontypedescription: any;
  preParams = {};
  dataList: any;
  first: any = 0;
  divisionList = [];
  loading: boolean;
  searchColumns: any
  transationDescriptions: any;
  backdatingbulkimport: FormGroup;
  selectedTabIndex: any = 0;
  DescriptionsType: any;
  fileList: File[] = [];
  row: any = {}
  maxFilesUpload: Number = 1;
  listOfFiles: any[] = [];
  backDatingFileName = '';
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  status: any = [];
  pageLimit: any = [10, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  activeStatus: any = [];
  backlist: boolean = false;
  Detailslist: boolean = false;
  backDatingImportId: any;
  backdatingDoaApprovalList: any;
  isPassed: boolean = true;
  primengTableConfigProperties: any;
  loggedInUserData: LoggedInUserModel;
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private tableFilterFormService: TableFilterFormService, private dialogService: DialogService,
    private router: Router, private momentService: MomentService, private snackbarService: SnackbarService, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
      this.backDatingImportId = this.activatedRoute.snapshot.queryParams.id;

      this.primengTableConfigProperties = {
      tableCaption: `${this.backDatingImportId?"Edit":"Create"} Bulk Upload`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing Management', relativeRouterUrl: '' }, { displayName: 'Back Dating' },{ displayName:  `${this.backDatingImportId?"Edit":"Create"} Bulk Upload` }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Create Bulk Upload',
            dataKey: 'bdi',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,

            columns: [
              { field: 'bdi', header: 'BDI', width: '200px' },
              { field: 'debtorCode', header: 'Debtor Code', width: '200px' },
              { field: 'referenceNumber', header: 'Reference No', width: '200px' },
              { field: 'description', header: 'Description ', width: '200px' },
              { field: 'glCategory', header: 'GL Category', width: '200px' },
              { field: 'totalIncl', header: 'Total Incl', width: '200px' },
              { field: 'vat', header: 'VAT', width: '200px' },
              { field: 'vatAmount', header: 'VAT Amount', width: '200px' },
              { field: 'totalIncl', header: 'Total Excl', width: '200px' },
              { field: 'isPassed', header: 'Passed', isCheckbox: true, isDisabled: true, width: '200px' },
              { field: 'errorMessage', header: 'Error Msg', width: '200px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.BACK_DATING_IMPORT_DETAILS,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },
        ]

      }
    }
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.getUxTransactionType();
    this.getUsers();
    this.getDivisions();
    //this.getDetails();
    this.getVatTaxCode()
    this.combineLatestNgrxStoreData();
    this.createBackDatingBulkAddEditModeForm();
  }
  VatCode = []


  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }
  loadPaginationLazy(event) {
    // this.otherParams = {};
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:

        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
          }
        } else {
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }
  //Get List
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.preParams['backDatingImportId'] = this.backDatingImportId;
    otherParams = { ...otherParams, ...this.preParams };
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BACK_DATING_IMPORT_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        {
          ...otherParams
        }
      )
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources.backDactingImportDetails;
        this.dataList = this.observableResponse;
        this.selectedRows = []
        this.totalRecords = data.resources.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    })
  }

  //Create Form
  createBackDatingBulkAddEditModeForm(): void {
    let BackDatingMandateModeModel = new BackDatingBulkImportaddModel();
    this.backdatingbulkimport = this.formBuilder.group({});
    Object.keys(BackDatingMandateModeModel).forEach((key) => {
      this.backdatingbulkimport.addControl(key, new FormControl(BackDatingMandateModeModel[key])
      );
    });
    this.backdatingbulkimport.get('transactionTypeId').valueChanges.subscribe((val) => {
      if (!val) {
        return;
      }
      this.changeTransactionType(val);
    })
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getVatTaxCode() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_BACK_DATING_VAT_CODE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.VatCode = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getUxTransactionType() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.transationTypes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BACK_DATING_IMPORT_DETAILS, null, false,
      prepareGetRequestHttpParams(null, null,
        { backDatingImportId: this.backDatingImportId }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          if (response.resources.backDactingImportFile?.postDate) {
            response.resources.backDactingImportFile.postDate = new Date(response.resources.backDactingImportFile.postDate)
          }
          let backDatingBulkImportModel = new BackDatingBulkImportaddModel(response.resources.backDactingImportFile);
          this.backdatingbulkimport.patchValue(backDatingBulkImportModel);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getUsers() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS_ALL).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.assignedToList = response.resources
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getDivisions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_DIVISIONS).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.divisionList = response.resources
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  changeTransactionType(val) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_DESCRIPTION, null, false, prepareRequiredHttpParams({
      InvoiceTransactionTypeId: val
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.transationDescriptions = [];
          this.transationDescriptions = response.resources;
        }
        let prCallData = this.transationTypes.find(el => el?.id == val)
        if (prCallData) {
          this.Transactiontypedescription = prCallData.displayName.split('-')[1];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  changeDescriptionType() {
    let invoiceTransactionDescriptionId = this.backdatingbulkimport.get('transactionDescriptionId').value;
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_INVOICE_TRANSACTION_DESCRIPTION_SUBTYPES, null, false, prepareRequiredHttpParams({
      invoiceTransactionDescriptionId
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.DescriptionsType = [];
          this.DescriptionsType = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  uploadFiles(file) {
    if (file && file.length == 0)
      return;

    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.listOfFiles = [];
    }
    const supportedExtensions = ['xlsx'];
    for (let i = 0; i < file.length; i++) {
      this.selectedFile = file[i];
      const path = this.selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension.toLowerCase())) {
        let filename = this.fileList.find(x => x.name === this.selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(this.selectedFile);
          this.listOfFiles.push(this.selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }
      }
      else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - xlsx', ResponseMessageTypes.WARNING);
      }
    }
  }

  uploadImportExcelFile() {
    if (this.backdatingbulkimport.invalid) {
      return;
    }
    let formValue = this.backdatingbulkimport.value;
    formValue.CreatedUserId = this.loggedInUserData.userId;
    formValue.postDate = new Date(),
      formValue.backDatingFileNumber = this.selectedFile.length;
    let formData = new FormData();
    formData.append('json', JSON.stringify(formValue));
    formData.append("FileName", this.selectedFile.name);
    formData.append("file", this.selectedFile);
    formData.append("CreatedUserId", this.loggedInUserData.userId);
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BACK_DATING, formData).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode == 200) {
        this.backDatingImportId = response.resources;
        this.Detailslist = true;
        this.getDetails();
      }
    })
  }

  uploadBackdating(): void {
    if (this.backdatingbulkimport.invalid) {
      return;
    }
    let formValue = this.backdatingbulkimport.value;
    formValue.backDatingFileNumber = this.selectedFile.length;
    formValue.backDatingImportId = this.backDatingImportId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BACK_DATING, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.backDatingImportId = response.resources;
        this.backlist = true
        this.getRequiredListData();
      }
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  onCancel() {
    this.router.navigate(["/billing/back-dating"])
  }

  downloadMyFile() {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', '../../../../../assets/files/Back Dating Functionatility-2.xlsx');
    link.setAttribute('download', `back-dating.xlsx`);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  onSubmitforApproval() {
    if (!this.isPassed) {
      const message = `There were errors updating the bulk details. The bulk update cannot submitted for approval`;
      const dialogData = new ConfirmDialogModel("Error", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        width: "60vw",
        data: { ...dialogData, isConfirm: false, isClose: true },
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (!result) {
          return;
        }
      });
    } else {
      const message = `Bulk Details have been updated succesfully and ready to be submitted for DOA Approval`;
      const dialogData = new ConfirmDialogModel("Success", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        width: "30vw",
        data: { ...dialogData, isConfirm: false, isClose: true },
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BACK_DATING_DOA_APPROVAL_LIST, null, false, null)
            .subscribe((response: IApplicationResponse) => {
              if (response.resources) {
                this.backdatingDoaApprovalList = response.resources;
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            })
          let data = {
            ApprovalList: this.backdatingDoaApprovalList,
            BackDatingImportId: this.backDatingImportId,
          }
          this.dialog.closeAll()
          const dialogReff = this.dialog.open(BulkDetailsApprovalRequestComponent, { disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
        }
      });
    }
  }

  onActionSubmited($event) { }
}
