import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackDatingBulkImportAddEditComponent } from './back-dating-bulk-import-add-edit.component';
import { BackDatingBulkImportAddComponent } from './back-dating-bulk-import-add.component';
import { BackDatingBulkImportListComponent } from './back-dating-bulk-import-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: BackDatingBulkImportListComponent, canActivate: [AuthGuard], data: { title: 'Back Dating' } },
  { path: 'add-edit', component: BackDatingBulkImportAddComponent, canActivate: [AuthGuard], data: { title: 'Back Dating Add' } },
  { path: 'add', component: BackDatingBulkImportAddEditComponent, canActivate: [AuthGuard], data: { title: 'Back Dating Add Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})

export class BackDatingBulkImportRoutingModule { }
