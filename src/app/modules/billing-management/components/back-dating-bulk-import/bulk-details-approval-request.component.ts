import { Component, Inject, OnInit } from "@angular/core";
import { MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
@Component({
  selector: 'app-bulk-details-approval-request',
  templateUrl: './bulk-details-approval-request.component.html',
})

export class BulkDetailsApprovalRequestComponent implements OnInit {
  loading: boolean = false
  roleId: any;
  userId: any;
  backdatingDoaApprovalList = '';
  BackDatingImportId = '';
  ApprovalRequest: boolean = false
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private httpCancelService: HttpCancelService, private router: Router, public dialog: MatDialog, private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService) {
    this.backdatingDoaApprovalList = data.backdatingDoaApprovalList,
      this.store
        .pipe(select(loggedInUserData))
        .subscribe((userData: UserLogin) => {

          if (!userData) {
            return;
          }
          this.roleId = userData["roleId"];
          this.userId = userData["userId"];
        });
  }

  ngOnInit() {
    this.ApprovalRequest = true;
  }
  closemodel() {
    this.dialog.closeAll();
  }
  onSubmitforApproval() {
    let otherParams = {
      roleId: this.roleId,
      createdUserId: this.userId,
      backDatingImportId: this.data.BackDatingImportId,
      createdDate: this.data.createdDate,
      IsApproved: false,
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BACK_DATING_DOA_APPROVAL_INSERT, otherParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialog.closeAll();
        const message = `Request has been submitted for DOA Approval`;
        const dialogData = new ConfirmDialogModel("Approval Request", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
          width: "30vw",
          data: { ...dialogData, isConfirm: false, isClose: true },
          disableClose: true
        });
        dialogRef.afterClosed().subscribe(result => {
          if (!result) {
            this.dialog.closeAll();
            return;
          }
        });
      } else {
        this.dialog.closeAll();
        this.router.navigateByUrl('/billing/back-dating');
      }
    })
  }
}
