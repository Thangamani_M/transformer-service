import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BackDatingBulkImportAddEditComponent } from './back-dating-bulk-import-add-edit.component';
import { BackDatingBulkImportAddComponent } from './back-dating-bulk-import-add.component';
import { BackDatingBulkImportListComponent } from './back-dating-bulk-import-list.component';
import { BackDatingBulkImportRoutingModule } from './back-dating-routing.module';
import { BulkDetailsApprovalRequestComponent } from './bulk-details-approval-request.component';
@NgModule({
  declarations: [BackDatingBulkImportListComponent,BackDatingBulkImportAddEditComponent,BulkDetailsApprovalRequestComponent,BackDatingBulkImportAddComponent],
  imports: [
    CommonModule,
    BackDatingBulkImportRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  entryComponents: [ BulkDetailsApprovalRequestComponent ]
})
export class BackDatingBulkImportModule { }
