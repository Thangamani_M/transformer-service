import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,  ResponseMessageTypes,  RxjsService, SnackbarService, } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-back-dating-bulk-list-import',
  templateUrl: './back-dating-bulk-import-list.component.html'
})
export class BackDatingBulkImportListComponent extends PrimeNgTableVariablesModel implements OnInit {
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  row: any = {};
  constructor(private crudService: CrudService,private activatedRoute: ActivatedRoute,public dialogService: DialogService, private router: Router,
    private store: Store<AppState>,private rxjsService: RxjsService, private snackbarService : SnackbarService) {
      super();
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedInUserData = userData;
      });
      this.primengTableConfigProperties = {
        tableCaption: "Bulk Import",
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Bulk Import List' }],
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'Bulk Import',
              dataKey: 'backDatingImportId',
              enableBreadCrumb: true,
              enableExportCSV: false,
              enableExportExcel: false,
              enableExportCSVSelected: false,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableRowDelete: false,
              enableStatusActiveAction: false,
              enableFieldsSearch: true,
              rowExpantable: false,
              rowExpantableIndex: 0,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              enableSecondHyperLink: false,
              cursorSecondLinkIndex: 1,
              enableAction: true,
              enableAddActionBtn: true,
              columns: [
                { field: 'fileNumber', header: 'File Number', width: '200px' },
                { field: 'backDatingFileName', header: 'File Name', width: '200px' },
                { field: 'division', header: 'Division', width: '200px' },
                { field: 'transactionType', header: 'Transaction Type', width: '200px' },
                { field: 'transactionDescription', header: 'Description Type', width: '200px' },
                { field: 'batchName', header: 'Batch Name', width: '200px' },
                { field: 'postDate', header: 'Post Type', width: '200px' },
                { field: 'createdDate', header: 'Created Date', width: '200px' },
                { field: 'createdBy', header: 'Created By', width: '200px' },
              ],
              apiSuffixModel: BillingModuleApiSuffixModels.BACK_DATING_IMPORT_LIST,
              moduleName: ModulesBasedApiSuffix.BILLING,
              enableMultiDeleteActionBtn: false,
              ebableAddActionBtn: true,
            },
          ]
        }
      }
      this.activatedRoute.queryParamMap.subscribe((params) => {
        this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
        this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
      });
     }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][BILLING_MODULE_COMPONENT.BACK_DATING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BACK_DATING_IMPORT_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize ,otherParams
      )
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.selectedRows = []
        data.resources.forEach(element => {
          if (element.isActive) {
            this.selectedRows.push(element)
          }
        });
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("billing/back-dating/add");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["billing/back-dating/add-edit"], { queryParams: { id: editableObject['backDatingImportId'] } });
            break;
        }
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }
}
