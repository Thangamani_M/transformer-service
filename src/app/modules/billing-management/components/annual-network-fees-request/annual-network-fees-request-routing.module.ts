import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnnualNetworkFeesAddEditComponent } from './annual-network-fees-add-edit.component';
import {  AnnualNetworkFeesListComponent } from './annual-network-fees-list.component';
import { AnnualNetworkFeesRequestComponentList } from './annual-network-fees-request-list.component';
const routes: Routes = [
    { path: '', component: AnnualNetworkFeesRequestComponentList, data: { title: 'Annual Network Fees Request' } },
    { path: 'list', component: AnnualNetworkFeesListComponent, data: { title: 'Annual Network Fees Request' } },
    { path: 'add-edit', component: AnnualNetworkFeesAddEditComponent, data: { title: 'Annual Network Fees Request' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AnnualFeeRequestRoutingModule { }
