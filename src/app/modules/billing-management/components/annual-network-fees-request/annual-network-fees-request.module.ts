import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AnnualNetworkFeesAddEditComponent } from './annual-network-fees-add-edit.component';
import { AnnualNetworkFeesListComponent } from './annual-network-fees-list.component';
import { AnnualNetworkFeesRequestComponentList } from './annual-network-fees-request-list.component';
import { AnnualFeeRequestRoutingModule } from './annual-network-fees-request-routing.module';
@NgModule({
  declarations: [AnnualNetworkFeesRequestComponentList,AnnualNetworkFeesListComponent,AnnualNetworkFeesAddEditComponent],
  imports: [
    CommonModule,
    AnnualFeeRequestRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ]
})
export class AnnualFeeRequestModule { }
