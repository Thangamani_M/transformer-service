import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, formConfigs, HttpCancelService, IApplicationResponse, monthsByNumber, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { loggedInUserData } from '@modules/others';
import { AppState } from '@app/reducers';
import { AnnualNetworkFeesRequestsModel, AnnualNetworkModel } from '@modules/others/configuration/models/annual-network-fees-request.model';
@Component({
  selector: 'app-annual-network-fees-add-edit',
  templateUrl: './annual-network-fees-add-edit.component.html',
  styleUrls: ['./annual-network-fees-add-edit.component.scss']
})
export class AnnualNetworkFeesAddEditComponent implements OnInit {
  loading: boolean;
  annualNetworkFeesddEditForm: FormGroup;
  annualNetworkFeeBatchHeaderId: any;
  SubmitForDORes: boolean;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  ApplyExclution: boolean;
  monthArray = monthsByNumber;
  loggedInUserData: LoggedInUserModel;
  anfBatchUpdate: FormArray;
  SubmitDoUSerListBtn: boolean;
  SubmitDoUSerList: any;
  details: any;
  AcceptList: any[];
  isPermanent: boolean = false;
  Consecutive: boolean = false;
  primengTableConfigProperties: any = {
    tableCaption: "Edit Annual Network Fees Request",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Billing Management ', relativeRouterUrl: '' },
    { displayName: 'Annual Network Fees Request', relativeRouterUrl: '/billing/annual-network-fees-request/' },
    { displayName: 'Edit Annual Network Fees Request' },
    ],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Type Mapping',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
        }]
    }
  }
  constructor(
    private rxjsService: RxjsService, private httpCancelService: HttpCancelService, private store: Store<AppState>, private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
      this.annualNetworkFeeBatchHeaderId = this.activatedRoute.snapshot.queryParams.id;
    });
  }

  ngOnInit() {
    this.getannualnetworkList();
    this.createannualnetworkForm();
    this.rxjsService.setGlobalLoaderProperty(true);
  }

  //Create Form
  createannualnetworkForm(): void {
    let priceModel = new AnnualNetworkModel()
    // create form controls dynamically from model class
    this.annualNetworkFeesddEditForm = this.formBuilder.group({
      anfBatchUpdate: this.formBuilder.array([]),
    });
    Object.keys(priceModel).forEach((key) => {
      this.annualNetworkFeesddEditForm.addControl(key, new FormControl(priceModel[key]));
    });
    this.annualNetworkFeesddEditForm.get("annualNetworkFeeBatchHeaderId").setValue(this.annualNetworkFeeBatchHeaderId);
    this.annualNetworkFeesddEditForm.get("modifiedUserId").setValue(this.loggedInUserData.userId);
    this.annualNetworkFeesddEditForm.get("modifiedDate").setValue(new Date);
    //this.annualNetworkFeesddEditForm.controls['anfBatchUpdate'] = setRequiredValidator(this.annualNetworkFeesddEditForm.controls['anfBatchUpdate'].value as FormGroup, ["excludeReason"]);
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  //Get Annual network
  getannualnetworkList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ANNUAL_NETWORK_FEE_BATCH_DETAIL_LIST, undefined, false,
      prepareRequiredHttpParams({
        AnnualNetworkFeeBatchHeaderId: this.annualNetworkFeeBatchHeaderId
      }))
      .subscribe((response) => {
        if (response && response.resources && response.isSuccess) {
          this.details = response.resources
          this.anfBatchUpdate = this.getannualnetworkFileNameDetails;
          if (response.resources.length > 0) {
            response.resources.forEach((serviceListModel: AnnualNetworkFeesRequestsModel) => {
              this.anfBatchUpdate.push(this.createServiceItemList(serviceListModel));
            })
          }
          this.details.forEach(element => {
            if (element['isPermanent'] == true)
              this.isPermanent = true;
            if (element['conscutive'] != '' || element['conscutive'] != '0')
              this.Consecutive = true;
          });
          this.getSubmitDoUSerList();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }

  //Formarray
  get getannualnetworkFileNameDetails(): FormArray {
    if (!this.annualNetworkFeesddEditForm) return;
    return this.annualNetworkFeesddEditForm.get("anfBatchUpdate") as FormArray;
  }

  openDialog(dt?: any) {
  }

  //Onsubmit
  applyExclution() {
    if (this.annualNetworkFeesddEditForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let FeesForm = this.annualNetworkFeesddEditForm.value;
    this.AcceptList = ['annualNetworkFeeBatchDetailId', 'conscutive', 'isPermanent', 'excludeReason', 'isOnceOff'];
    Object.keys(FeesForm.anfBatchUpdate).forEach(key => {
      Object.keys(FeesForm.anfBatchUpdate[key]).forEach(key1 => {
        if (!this.AcceptList.includes(key1)) {
          delete FeesForm.anfBatchUpdate[key][key1];
        }
      });
    });
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ANF_BATCH_APPLY_EXCLUSION, FeesForm).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        // this.router.navigate(['/billing/annual-network-fees-request'], { queryParams: { id: this.annualNetworkFeeBatchHeaderId } });
        this.router.navigate(['/billing/annual-network-fees-request']);
      }
    })
    // }
  }
  //Submit For Do
  SubmitForDO() {
    if (this.annualNetworkFeesddEditForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let FeesForm = this.annualNetworkFeesddEditForm.value;
    this.AcceptList = ['annualNetworkFeeBatchDetailId', 'conscutive', 'isPermanent', 'excludeReason', 'isOnceOff'];
    Object.keys(FeesForm.anfBatchUpdate).forEach(key => {
      Object.keys(FeesForm.anfBatchUpdate[key]).forEach(key1 => {
        if (!this.AcceptList.includes(key1)) {
          delete FeesForm.anfBatchUpdate[key][key1];
        }
      });
    });
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ANF_BATCH_SUBMIT_DO, FeesForm).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(['/billing/annual-network-fees-request']);
      }
    })
  }

  //Create Form
  createServiceItemList(serviceListModel?: AnnualNetworkFeesRequestsModel): FormGroup {
    let servicesModel = new AnnualNetworkFeesRequestsModel(serviceListModel);
    let formControls = {};
    Object.keys(servicesModel).forEach((key) => {
      if (key == 'excludeReason') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
      }
    });
    let form = this.formBuilder.group(formControls);
    form.get('isPermanent').valueChanges.subscribe(value => {
      if (value) {
        form.get('excludeReason').setValidators([Validators.required]);
        form.get('excludeReason').updateValueAndValidity();
      }
      else {
        form.get('excludeReason').setValidators([]);
        form.get('excludeReason').updateValueAndValidity();
      }
    })
    form.get('isOnceOff').valueChanges.subscribe(value => {
      if (value) {
        form.get('excludeReason').setValidators([Validators.required]);
        form.get('excludeReason').updateValueAndValidity();
      }
      else {
        form.get('excludeReason').setValidators([]);
        form.get('excludeReason').updateValueAndValidity();
      }
    })
    form.get('conscutive').valueChanges.subscribe(value => {
      if (value)
      // (value != '' || value != '0'
      {
        form.controls['excludeReason'].enable()
        form.get('excludeReason').setValidators([Validators.required])
        form.controls['excludeReason'].updateValueAndValidity();
      }
      else {
        form.get('excludeReason').setValidators([]);
        form.get('excludeReason').updateValueAndValidity();
      }
    })
    return form;
  }

  //SubmitDoUSerList
  getSubmitDoUSerList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ANNUAL_NETWORK_FEE_DO_USER, undefined, false,
      prepareRequiredHttpParams({
        UserId: this.loggedInUserData.userId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.SubmitDoUSerList = response.resources;
          this.SubmitForDORes = this.SubmitDoUSerList.isSubmitDOUser;
          if (this.SubmitDoUSerList.isHighLevelUser && this.SubmitForDORes) {
            this.SubmitDoUSerListBtn = false;
            this.ApplyExclution = true;
          }
          else if (!this.SubmitDoUSerList.isHighLevelUser && !this.SubmitForDORes) {
            this.SubmitDoUSerListBtn = true;
            this.ApplyExclution = false;
          }
          else if (!this.SubmitDoUSerList.isHighLevelUser && this.SubmitForDORes) {
            if (this.isPermanent || this.Consecutive) {
              this.SubmitDoUSerListBtn = true;
              this.ApplyExclution = false;
            }
            if (!this.isPermanent && !this.Consecutive) {
              this.SubmitDoUSerListBtn = false;
              this.ApplyExclution = false;
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }
  //onchange
  changeisOnceOff(index) {
    let changeisOnceOff = this.annualNetworkFeesddEditForm.controls.anfBatchUpdate.value[index].isOnceOff
    if (changeisOnceOff) {
      this.anfBatchUpdate.controls[index].get('isPermanent').setValue(false);
      this.anfBatchUpdate.controls[index].get('conscutive').setValue('0');
    }
    let isOnceOff = this.anfBatchUpdate.controls[index].get('isOnceOff').value
    if ((this.SubmitForDORes && this.SubmitDoUSerList.isHighLevelUser)) {
      this.SubmitDoUSerListBtn = false;
      this.ApplyExclution = true;
    }
    else if ((!this.SubmitForDORes && !this.SubmitDoUSerList.isHighLevelUser)) {
      this.SubmitDoUSerListBtn = true;
      this.ApplyExclution = false;
    }
  }
  //onchange
  changeisPermanent(index) {
    let changePermanent = this.annualNetworkFeesddEditForm.controls.anfBatchUpdate.value[index].isPermanent
    let conscutive = this.annualNetworkFeesddEditForm.controls.anfBatchUpdate.value[index].conscutive;
    if (changePermanent) {
      this.anfBatchUpdate.controls[index].get('isOnceOff').setValue(false);
      this.anfBatchUpdate.controls[index].get('conscutive').setValue('0');
    }
    if ((this.SubmitForDORes && this.SubmitDoUSerList.isHighLevelUser)) {
      this.SubmitDoUSerListBtn = false;
      this.ApplyExclution = true;
    }
    else if ((!this.SubmitForDORes && !this.SubmitDoUSerList.isHighLevelUser)) {
      this.SubmitDoUSerListBtn = true;
      this.ApplyExclution = false;
    }
    else if ((this.SubmitForDORes && !this.SubmitDoUSerList.isHighLevelUser && changePermanent)) {
      this.SubmitDoUSerListBtn = true;
      this.ApplyExclution = false;
    }
    else if ((this.SubmitForDORes && !this.SubmitDoUSerList.isHighLevelUser &&
      !changePermanent && (conscutive == '' || conscutive == '0'))) {
      this.SubmitDoUSerListBtn = false;
      this.ApplyExclution = false;
    }
  }
  //onchange
  OnChange(index) {
    let conscutive = this.annualNetworkFeesddEditForm.controls.anfBatchUpdate.value[index].conscutive
    let changePermanent = this.annualNetworkFeesddEditForm.controls.anfBatchUpdate.value[index].isPermanent
    if (conscutive == '') {
      this.anfBatchUpdate.controls[index].get('isOnceOff').setValue(false);
      this.anfBatchUpdate.controls[index].get('isPermanent').setValue(false);
      this.anfBatchUpdate.controls[index].get('excludeReason').setValidators([Validators.required])
      this.anfBatchUpdate.controls[index].get('excludeReason').updateValueAndValidity();


    }
    if (conscutive != '0') {
      this.anfBatchUpdate.controls[index].get('isOnceOff').setValue(false);
      this.anfBatchUpdate.controls[index].get('isPermanent').setValue(false);

    }

    if ((this.SubmitForDORes && this.SubmitDoUSerList.isHighLevelUser)) {
      this.SubmitDoUSerListBtn = false;
      this.ApplyExclution = true;
    }
    else if ((!this.SubmitForDORes && !this.SubmitDoUSerList.isHighLevelUser)) {
      this.SubmitDoUSerListBtn = true;
      this.ApplyExclution = false;
    }
    else if ((this.SubmitForDORes && !this.SubmitDoUSerList.isHighLevelUser && changePermanent && (conscutive != '0' || conscutive == ''))) {
      this.SubmitDoUSerListBtn = true;
      this.ApplyExclution = false;
    }
    else if ((this.SubmitForDORes && !this.SubmitDoUSerList.isHighLevelUser &&
      !changePermanent && (conscutive == '' || conscutive == '0'))) {
      this.SubmitDoUSerListBtn = false;
      this.ApplyExclution = false;
    }
  }
}
