import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType,LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,ResponseMessageTypes,RxjsService, SnackbarService} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest} from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-annual-network-fees-list',
  templateUrl: './annual-network-fees-list.component.html',
})
export class AnnualNetworkFeesListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};
  preParams = {};
  isDisabled: false;
  annualNetworkFeeBatchHeaderId: any;
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,public dialogService: DialogService, private router: Router,private store: Store<AppState>,
    private rxjsService: RxjsService, private snackbarService: SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
      this.annualNetworkFeeBatchHeaderId = this.activatedRoute.snapshot.queryParams.id;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Annual Network Fees Request",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '/billing/annual-network-fees-request' }, { displayName: 'Annual Network Fees Request' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Annual Network Fees Request',
            dataKey: '',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            enableSecondHyperLink: false,
            enableAction: true,
            enableEditActionBtn: true,
            cursorSecondLinkIndex: 1,
            cursorLinkIndex: 0,
            columns: [
              { field: 'customerRefNo', header: 'Customer Code', width: '200px' },
              { field: 'customerName', header: 'Customer Description', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '200px' },
              { field: 'billName', header: 'Bill Name', width: '200px' },
              { field: 'contractRefNo', header: 'Contract ID', width: '200px' },
              { field: 'division', header: 'Division', width: '200px' },
              { field: 'subArea', header: 'Sub Area', width: '200px' },
              { field: 'mainArea', header: 'Main Area', width: '200px' },
              { field: 'createdDate', header: 'Created Date', width: '200px' },
              { field: 'doRunCode', header: 'Do Run Code', width: '200px' },
              { field: 'paymentMethodName', header: 'Payment Method Name', width: '200px' },
              { field: 'smsDebtor', header: 'Sms Debtor', width: '200px' },
              { field: 'email', header: 'Email', width: '200px' },
              { field: 'buildingNo', header: 'Building No', width: '200px' },
              { field: 'buildingName', header: 'Building Name', width: '200px' },
              { field: 'streetNo', header: 'Street No', width: '200px' },
              { field: 'streetName', header: 'Street Name', width: '200px' },
              { field: 'suburbName', header: 'Suburb Name', width: '200px' },
              { field: 'cityName', header: 'City Name', width: '200px' },
              { field: 'originName', header: 'Origin Name', width: '200px' },
              { field: 'debtorGroupName', header: 'Debtor Group Name', width: '200px' },
              { field: 'installOriginName', header: 'Install Origin Name', width: '200px' },
              { field: 'categoryName', header: 'Category Name', width: '200px' },
              { field: 'dealType', header: 'Deal Type', width: '200px' },
              { field: 'salesRep', header: 'Sales Rep', width: '200px' },
              { field: 'salesChannel', header: 'Sales Channel', width: '200px' },
              { field: 'businessArea', header: 'Business Area', width: '200px' },
              { field: 'anfWaive', header: 'Anf Waive', width: '200px' },
              { field: 'branchName', header: 'Branch Name', width: '200px' },
              { field: 'districtName', header: 'District Name', width: '200px' },
              { field: 'regionName', header: 'Region Name', width: '200px' },
              { field: 'acquired', header: 'Acquired', width: '200px' },
              { field: 'bilable', header: 'Bilable', width: '200px' },
              { field: 'licenceCount', header: 'LicenceCount', width: '200px' },
              { field: 'isBilled', header: 'IsBilled', width: '200px' },
              { field: 'techArea', header: 'TechArea', width: '200px' },
              { field: 'salesArea', header: 'Sales Area', width: '200px' },
              { field: 'stockAmountExc', header: 'Stock Amount Exc', width: '200px' },
              { field: 'stockAmountInc', header: 'Stock Amount Inc', width: '200px' },
              { field: 'isOnceOff', header: 'Is Once Off', width: '200px', isCheckbox: true, isDisabled: true },
              { field: 'isPermanent', header: 'Is Permanent', width: '200px', isCheckbox: true, isDisabled: true },
              { field: 'conscutive', header: 'Consecutive Waivers', width: '200px', },
              { field: 'excludeReason', header: 'Exclude Reason', width: '200px', }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.ANNUAL_NETWORK_FEE_BATCH_DETAIL_LIST,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.preParams['AnnualNetworkFeeBatchHeaderId'] = this.annualNetworkFeeBatchHeaderId
    otherParams = { ...otherParams, ...this.preParams };
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.ANNUAL_NETWORK_FEE_BATCH_DETAIL_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode == 200) {
        this.dataList = data.resources;
        this.selectedRows = []
        data.resources.forEach(element => {
          if (element.isActive) {
            this.selectedRows.push(element)
          }
        });
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
          if (this.row['sortOrderColumn']) {
            unknownVar['sortOrder'] = this.row['sortOrder'];
            unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("billing/annual-network-fees-request/list");
            break;
        }
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            if(this.dataList.length === 0){
              this.snackbarService.openSnackbar("No Data Found", ResponseMessageTypes.WARNING);
              return;
            }
            this.redirectToAddEditPage();
            break;
        }
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  redirectToAddEditPage(): void {
    this.router.navigate(['billing/annual-network-fees-request/add-edit'], { queryParams: { id: this.annualNetworkFeeBatchHeaderId } });
  }
}
