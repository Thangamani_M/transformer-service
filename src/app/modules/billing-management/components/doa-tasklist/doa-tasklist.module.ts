import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DoaTasklistDetailsRoutingModule } from './doa-tasklist-routing.module';
import { DoaTasklistComponent } from './doa-tasklist.component';
@NgModule({
  declarations: [DoaTasklistComponent],
  imports: [
    CommonModule,
    DoaTasklistDetailsRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
})
export class DoaTasklistDetailsModule { }
