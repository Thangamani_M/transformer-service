import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoaTasklistComponent } from './doa-tasklist.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: DoaTasklistComponent, canActivate:[AuthGuard],data: { title: 'DOA Tasklist' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})

export class DoaTasklistDetailsRoutingModule { }
