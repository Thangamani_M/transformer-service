import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {  CrudService, CrudType,currentComponentPageBasedPermissionsSelector$,LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-doa-tasklist',
  templateUrl: './doa-tasklist.component.html'
})
export class DoaTasklistComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  first: any = 0;
  row: any = {}
  activeStatus: any = []

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    public dialogService: DialogService, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "DOA Task List",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing Mangement', relativeRouterUrl: '' }, { displayName: 'DOA Task List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Doa Task List',
            dataKey: 'backDatingImportId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'requestNumber', header: 'Request Number', width: '200px' },
              { field: 'createdUserName', header: 'Creator User Name', width: '200px' },
              { field: 'lastUserName', header: 'Last User Name', width: '200px' },
              { field: 'currentUserName', header: 'Current User Name', width: '200px' },
              { field: 'escalationAmount', header: 'Escalation Amount', width: '200px' },
              { field: 'requestDate', header: 'Request Date', width: '200px' },
              { field: 'actionDate', header: 'Action Date/Time', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'processName', header: 'Process Name', width: '200px' },
              { field: 'pathDescription', header: 'Path Description', width: '200px' },
              { field: 'onLevel', header: 'On Level', width: '200px' },
              { field: 'levelName', header: 'Level Name', width: '200px' },
              { field: 'escalationType', header: 'Escalation Type', width: '200px' },
              { field: 'department', header: 'Department', width: '200px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.BACK_DATING_DOA_APPROVAL_LIST,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
          },
        ]

      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.activeStatus = [
      { label: 'Enabled', value: 'Enabled' },
      { label: 'Disabled', value: 'Disabled' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][BILLING_MODULE_COMPONENT.BACK_DATING_DOA]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canApprove) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        let url = '/billing/back-dating/add-edit/';
        this.router.navigate([url], { queryParams: { id: editableObject['backDatingImportId'], declinetransaction: false, BackDatingImportApprovalId: editableObject['backDatingImportApprovalId'] } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BACK_DATING_DOA_APPROVAL_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams
      )
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.selectedRows = []
        data.resources.forEach(element => {
          if (element.isActive) {
            this.selectedRows.push(element)
          }
        });
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }
}
