import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { ShiftCancelReasonAddEditModel } from '@modules/others/configuration/models/risk-watch-guard-service.model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-shift-cancel-reason-add-edit',
  templateUrl: './shift-cancel-reason-add-edit.component.html',
  styleUrls: ['./shift-cancel-reason-add-edit.component.scss'],
})

export class ShiftCancelReasonAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  shiftCancelReasonAddEditForm: FormGroup;
  loggedUser: UserLogin;
  shiftCancelReasonTypeId: any
  isDuplicate = false;
  addArray: any
  primengTableConfigProperties:any
  constructor(
    private crudService: CrudService,
    private router: Router,
    private dialogService: DialogService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.shiftCancelReasonTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: `${this.shiftCancelReasonTypeId ? 'Update' : 'Add'} Shift Cancel Reason`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },{ displayName: 'Risk Watch Configuration', relativeRouterUrl: '' }, { displayName: 'Shift Cancel Reason' ,relativeRouterUrl: '/billing/risk-watch-configuration',queryParams:{tab:2}},{ displayName: `${this.shiftCancelReasonTypeId ? 'Update' : 'Add'} Shift Cancel Reason`,}],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Shift Cancel Reason',
            dataKey: 'riskWatchConfigId',
            enableBreadCrumb: true,
          }]
        }
      }
  }

  ngOnInit(): void {
    this.createShiftCancelReasonForm();
    if (this.shiftCancelReasonTypeId) {
      this.getDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          const addObj = {
            shiftCancelReasonTypeId: response.resources?.shiftCancelReasonTypeId,
            createdUserId: response.resources?.createdUserId,
            shiftCancelReasonNameDescription: response.resources?.shiftCancelReasonNameDescription,
            shiftCancelReasonTypeNames: response.resources?.shiftCancelReasonTypeNames,
            isActive: response.resources?.isActive,
          }
          this.createshiftCancelReasonArray(addObj);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createShiftCancelReasonForm(): void {
    let shiftCancelReasonFormModel = new ShiftCancelReasonAddEditModel();
    this.shiftCancelReasonAddEditForm = this.formBuilder.group({});
    Object.keys(shiftCancelReasonFormModel).forEach((key) => {
      this.shiftCancelReasonAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(shiftCancelReasonFormModel[key]));
    });
    this.shiftCancelReasonAddEditForm = setRequiredValidator(this.shiftCancelReasonAddEditForm, ["shiftCancelReasonTypeNames", "shiftCancelReasonNameDescription"]);
    this.shiftCancelReasonAddEditForm.addControl('shiftCancelReasonArray', this.formBuilder.array([]));
  }

  get getShiftCancelReasonArray(): FormArray {
    return this.shiftCancelReasonAddEditForm.get('shiftCancelReasonArray') as FormArray;
  }


  // Create FormArray controls
  createshiftCancelReasonArray(data?: ShiftCancelReasonAddEditModel) {
    let commercialPorudctData = new ShiftCancelReasonAddEditModel(data ? data : undefined);
    let shiftCancelReasonTypeArray = this.formBuilder.group({});
    Object.keys(commercialPorudctData).forEach((key) => {
      shiftCancelReasonTypeArray.addControl(key, new FormControl({ value: commercialPorudctData[key], disabled: true }));
    });
    if (this.shiftCancelReasonTypeId) {
      shiftCancelReasonTypeArray.get('shiftCancelReasonTypeNames').enable();
      shiftCancelReasonTypeArray.get('shiftCancelReasonNameDescription').enable();
    }
    this.shiftCancelReasonAddEditForm.get('shiftCancelReasonTypeNames').reset();
    this.shiftCancelReasonAddEditForm.get('shiftCancelReasonNameDescription').reset();
    this.shiftCancelReasonAddEditForm.get('shiftCancelReasonTypeNames').setErrors(null);
    this.shiftCancelReasonAddEditForm.get('shiftCancelReasonTypeNames').markAsUntouched();
    this.shiftCancelReasonAddEditForm.get('isActive').setValue(true);
    this.getShiftCancelReasonArray.push(shiftCancelReasonTypeArray);
  }


  addReasonType() {
    this.isDuplicate = false;
    if (!this.shiftCancelReasonAddEditForm.get('shiftCancelReasonTypeNames')?.valid) {
      this.shiftCancelReasonAddEditForm.markAllAsTouched();
      return;
    }
    this.getShiftCancelReasonArray.value.forEach(el => {
      if (el.shiftCancelReasonTypeNames === this.shiftCancelReasonAddEditForm.value.shiftCancelReasonTypeNames) {
        this.isDuplicate = true;
      }
    });
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Shift Cancel Reason Type Name already exist", ResponseMessageTypes.WARNING);
      return;
    }
    const addObj: any = {
      shiftCancelReasonTypeId: null,
      createdUserId: this.loggedUser?.userId,
      modifiedUserId: this.loggedUser?.userId,
      shiftCancelReasonTypeNames: this.shiftCancelReasonAddEditForm.value.shiftCancelReasonTypeNames,
      shiftCancelReasonNameDescription: this.shiftCancelReasonAddEditForm.value.shiftCancelReasonNameDescription,
      isActive: true,
    }
    this.createshiftCancelReasonArray(addObj);
  }

  //remove index
  removeReasonType(i: number): void {
    if (this.getShiftCancelReasonArray.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {

      let customText = "Are you sure you want to delete this?"
      const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
        header: 'Confirmation',
        showHeader: false,
        closable: true,
        baseZIndex: 10000,
        width: '400px',
        data: { customText: customText },
      });
      confirm.onClose.subscribe((resp) => {

        if (resp === false) {
          this.isDuplicate = false;
          this.getShiftCancelReasonArray.removeAt(i);
        }
      });
    }
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.SHIFT_CANCEL_REASON,
      this.shiftCancelReasonTypeId, true, null, 1
    );
  }


  onSubmit() {
    if (!this.getShiftCancelReasonArray?.value?.length) {
      return;
    }
    this.addArray = [...this.getShiftCancelReasonArray.value];
    if (!this.shiftCancelReasonTypeId) {
      this.addArray.forEach((key) => {
        key["createdUserId"] = this.loggedUser.userId;
      })
    }
    if (this.shiftCancelReasonTypeId) {
      this.shiftCancelReasonAddEditForm.value["shiftCancelReasonTypeNames"] = this.shiftCancelReasonAddEditForm.controls.shiftCancelReasonArray.get('0').value.shiftCancelReasonTypeNames;
      this.shiftCancelReasonAddEditForm.value["shiftCancelReasonNameDescription"] = this.shiftCancelReasonAddEditForm.controls.shiftCancelReasonArray.get('0').value.shiftCancelReasonNameDescription;
      this.shiftCancelReasonAddEditForm.value["isActive"] = this.shiftCancelReasonAddEditForm.controls.shiftCancelReasonArray.get('0').value.isActive;
      this.shiftCancelReasonAddEditForm.value["createdUserId"] = this.loggedUser.userId;
      this.shiftCancelReasonAddEditForm.value["modifiedUserId"] = this.loggedUser.userId;
      this.shiftCancelReasonAddEditForm.value["shiftCancelReasonTypeId"] = this.shiftCancelReasonTypeId;
      delete this.shiftCancelReasonAddEditForm.value["shiftCancelReasonArray"];
    }
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent

    this.shiftCancelReasonAddEditForm.get('shiftCancelReasonTypeNames').setErrors(null);
    this.shiftCancelReasonAddEditForm.get('shiftCancelReasonNameDescription').setErrors(null);
    let crudService: Observable<IApplicationResponse> = !this.shiftCancelReasonTypeId
      ? this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SHIFT_CANCEL_REASON, this.addArray, 1)
      : this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SHIFT_CANCEL_REASON, this.shiftCancelReasonAddEditForm.value, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/billing/risk-watch-configuration'],{queryParams:{tab:2}});
      }
    })
  }
  goBack(){
    this.router.navigate(['/billing/risk-watch-configuration'],{queryParams:{tab:2}});
  }


}
