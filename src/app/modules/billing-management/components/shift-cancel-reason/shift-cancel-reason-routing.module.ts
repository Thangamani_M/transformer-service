import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShiftCancelReasonAddEditComponent } from './shift-cancel-reason-add-edit/shift-cancel-reason-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: 'add-edit', component: ShiftCancelReasonAddEditComponent, canActivate:[AuthGuard],data: { title: 'Shift Cancel Reason Add/Edit' }},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ShiftCancelReasonRoutingModule { }
