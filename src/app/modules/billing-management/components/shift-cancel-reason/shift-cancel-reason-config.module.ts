import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ShiftCancelReasonAddEditComponent } from './shift-cancel-reason-add-edit/shift-cancel-reason-add-edit.component';
import { ShiftCancelReasonRoutingModule } from './shift-cancel-reason-routing.module';

@NgModule({
  declarations: [ShiftCancelReasonAddEditComponent],
  imports: [
    CommonModule,
    ShiftCancelReasonRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
  ]
})
export class ShiftCancelReasonModule { }
