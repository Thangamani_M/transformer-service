
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { addFormControls, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, removeFormControls, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { StockIdAddEditModel } from '@modules/others/configuration/models/stock-id.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-stock-id-add-edit',
  templateUrl: './stock-id-add-edit.component.html',
  styleUrls: ['./stock-id-add-edit.component.scss']
})
export class StockIdAddEditComponent implements OnInit {

  stockId: any
  divisionDropDown: any = [];
  stockIdForm: FormGroup;
  rOCommunityPatrolTypesList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  stockIdDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  stockIdTypes = []
  stockCategory = []
  @ViewChildren('input') rows: QueryList<any>;

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.stockId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createstockIdForm();
    this.getStockIdType()

    if (this.stockId) {
      this.getStockIdDetailsById().subscribe((response: IApplicationResponse) => {

        this.rxjsService.setGlobalLoaderProperty(false);
        let stockIdAddEditModel = new StockIdAddEditModel(response.resources);
        this.stockIdDetails = response.resources;
        this.stockIdForm.patchValue(stockIdAddEditModel);
      })
    }
    this.stockIdForm.get('isFixedFee').valueChanges.subscribe((isFixedFee: boolean) => {
      if (isFixedFee) {
        this.stockIdForm = addFormControls(this.stockIdForm, [{ controlName: "isTierBasedPrice" }]);
        this.stockIdForm = setRequiredValidator(this.stockIdForm, ['isTierBasedPrice']);
        this.stockIdForm.get('isTierBasedPrice').setValue(true);
      }
      else {
        this.stockIdForm = removeFormControls(this.stockIdForm, ["isTierBasedPrice"]);
      }
    });
    this.onChangeStockIdType();
  }

  createstockIdForm(): void {
    let stockIdAddEditModel = new StockIdAddEditModel();
    this.stockIdForm = this.formBuilder.group({
    });
    Object.keys(stockIdAddEditModel).forEach((key) => {
      this.stockIdForm.addControl(key, new FormControl(stockIdAddEditModel[key]));
    });
    this.stockIdForm = setRequiredValidator(this.stockIdForm, ["stockIdName", "stockIdNotes", "isFixedFee", "isTierBasedPrice","stockIDTypeId","stockCategoryId"]);
    this.stockIdForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.stockIdForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }
  //Get Details
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID,
      this.stockId
    );
  }

  getStockIdType (){
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_TYPES, undefined).subscribe(response=>{
        if(response.isSuccess && response.statusCode ==200){
          this.stockIdTypes =  response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onChangeStockIdType (){
    this.stockIdForm.get('stockIDTypeId').valueChanges.subscribe(stockTypeId=>{
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_TYPE_CATEGORY_DROPDOWN, null, false, prepareRequiredHttpParams({StockIdTypeId : stockTypeId})).subscribe(response=>{
        if(response.isSuccess && response.statusCode ==200){
          this.stockCategory =  response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    })
  }

  onSubmit(): void {
    if (this.stockIdForm.pristine) {
      this.router.navigate(['billing/stock-id-request/add-edit'], { queryParams: { id: this.stockId } });
    } else {
      if (this.stockIdForm.invalid) {
        return;
      }

      let formValue = this.stockIdForm.value;
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = (!this.stockId) ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID, formValue) :
        this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          // this.router.navigateByUrl('/billing/stock-id/list?tab=0');
          if (response.resources) {
            this.router.navigate(['billing/stock-id-request/add-edit'], { queryParams: { id: response.resources } });
          } else {
            this.router.navigateByUrl("billing/stock-id-request/add-edit");
          }
        }
      })
    }

  }

}
