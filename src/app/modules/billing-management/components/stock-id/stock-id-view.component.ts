import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-stock-id-view',
  templateUrl: './stock-id-view.component.html'
})
export class StockIdViewComponent implements OnInit {


  stockId: string;
  stockIdDetails: any;
  primengTableConfigProperties: any= {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store : Store<AppState>,
    private snackbarService: SnackbarService) {
    this.stockId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getstockIdDetailsById(this.stockId);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.STOCK_ID_CREATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getstockIdDetailsById(stockId: string) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID, stockId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stockIdDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.stockId) {
      this.router.navigate(['billing/stock-id/add-edit'], { queryParams: { id: this.stockId } });
    }
  }
}
