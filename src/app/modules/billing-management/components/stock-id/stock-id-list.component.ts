

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-id-list',
  templateUrl: './stock-id-list.component.html'
})
export class StockIdListComponent extends PrimeNgTableVariablesModel  implements OnInit {
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  activeStatus: any = []
  row: any = {}
  constructor(private crudService: CrudService,private activatedRoute: ActivatedRoute,public dialogService: DialogService, private router: Router,
    private store: Store<AppState>,private rxjsService: RxjsService, private snackbarService: SnackbarService) {
      super();
    this.primengTableConfigProperties = {
      tableCaption: "Stock ID",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Stock ID List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock ID List',
            dataKey: 'stockId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableAction:true,
            enableExportBtn: false,
            enableAddActionBtn: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stockIdName', header: 'Stock ID', width: '200px' },
            { field: 'stockIdNotes', header: 'Notes', width: '200px' },
            { field: 'isDay', header: 'Day', width: '150px', type:"dropdown",options: [
              { label: 'Enabled', value: 'Enabled' },
              { label: 'Disabled', value: 'Disabled' },
            ] },
            { field: 'isHours', header: 'Hour', width: '150px' , type:"dropdown",options: [
              { label: 'Enabled', value: 'Enabled' },
              { label: 'Disabled', value: 'Disabled' },
            ]},
            { field: 'isShift', header: 'Shift', width: '150px' , type:"dropdown",options: [
              { label: 'Enabled', value: 'Enabled' },
              { label: 'Disabled', value: 'Disabled' },
            ]},
            { field: 'isGrade', header: 'Grade', width: '150px', type:"dropdown",options: [
              { label: 'Enabled', value: 'Enabled' },
              { label: 'Disabled', value: 'Disabled' },
            ] },
            { field: 'isFixedFee', header: 'Type Of Fee', width: '200px' },
            { field: 'isActive', header: 'Status', width: '150px' }],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_ID,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
}
onChangeSelecedRows(e) {
  this.selectedRows = e;
}

combineLatestNgrxStoreData() {
  combineLatest([
    this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
  ).subscribe((response) => {
    this.loggedInUserData= new LoggedInUserModel(response[0]);
    let permission = response[1][BILLING_MODULE_COMPONENT.STOCK_ID_CREATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
      this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
          if (this.row['sortOrderColumn']) {
            unknownVar['sortOrder'] = this.row['sortOrder'];
            unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
        case CrudType.VIEW:
          this.openAddEditPage(CrudType.VIEW, row);
          break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("billing/stock-id/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["billing/stock-id/view"], { queryParams: { id: editableObject['stockId'] } });
            break;
        }
    }
  }
}
