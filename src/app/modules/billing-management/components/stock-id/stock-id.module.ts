import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockIdAddEditComponent } from './stock-id-add-edit.component';
import { StockIdListComponent } from './stock-id-list.component';
import { StockIdRoutingModule } from './stock-id-routing.module';
import { StockIdViewComponent } from './stock-id-view.component';



@NgModule({
  declarations: [StockIdListComponent, StockIdAddEditComponent, StockIdViewComponent],
  imports: [
    CommonModule,
    StockIdRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ]
})
export class StockIdModule { }
