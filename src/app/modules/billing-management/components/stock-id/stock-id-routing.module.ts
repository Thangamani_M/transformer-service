import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockIdAddEditComponent } from './stock-id-add-edit.component';
import { StockIdListComponent } from './stock-id-list.component';
import { StockIdViewComponent } from './stock-id-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    // { path: '', redirectTo:'list',canActivate:[AuthGuard],pathMatch:'full'},
    { path: '', component: StockIdListComponent, canActivate:[AuthGuard],data: { title: 'Stock ID list' }},
    { path: 'add-edit', component: StockIdAddEditComponent, canActivate:[AuthGuard],data: { title: 'Stock ID Add/Edit' }},
    { path: 'view', component: StockIdViewComponent, canActivate:[AuthGuard],data: { title: 'Stock ID View' }},

];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class StockIdRoutingModule { }
