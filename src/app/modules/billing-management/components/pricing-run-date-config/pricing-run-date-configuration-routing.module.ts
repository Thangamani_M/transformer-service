import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PriceRunDateAddEditConfigurationComponent } from './pricing-run-date-configuration-add-edit.component';
import { PriceRunDateConfigurationComponent } from './pricing-run-date-configuration-list.component';
import { PriceRunDateViewConfigurationComponent } from './pricing-run-date-configuration-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: PriceRunDateConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Price Increase Run Date List' } },
  { path: 'view', component: PriceRunDateViewConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Price Increase Date View' } },
  { path: 'add-edit', component: PriceRunDateAddEditConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Price Increase Date Add-Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class PricingRunDateConfigurationRoutingModule { }
