import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'pricing-run-date-configuration',
  templateUrl: './pricing-run-date-configuration-list.component.html'
})
export class PriceRunDateConfigurationComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Price Increase Run Date Config",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Price Increase Run Date Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Price Increase Run Date Config',
            dataKey: 'pricingIncreaseRunDateConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'divisionName', header: 'Division Name', width: '200px' },
            { field: 'financialYear', header: 'Financial Year', width: '200px' },
            { field: 'financialPeriod', header: 'Financial Period', width: '200px' }],
            apiSuffixModel: BillingModuleApiSuffixModels.PRICE_INCREASE_RUN_DATE_CONFIG,
            moduleName: ModulesBasedApiSuffix.BILLING,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.PRICE_INCREASE_RUN_DATE_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.router.navigate(['billing/pricing-run-date-configuration/add-edit'], { skipLocationChange: true });
        break;
      case CrudType.GET:
        this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.router.navigate(['billing/pricing-run-date-configuration/view'], { queryParams: { id: row['pricingIncreaseRunDateConfigId'], divisionId: row['divisionId'], financialYearId: row['financialYearId'] }, skipLocationChange: true });
        break;
      default:
        break;
    }
  }
}
