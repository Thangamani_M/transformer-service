import { Component, OnInit } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  CrudType,
  currentComponentPageBasedPermissionsSelector$,
  getPDropdownData,
  IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  setRequiredValidator,
  SnackbarService,
} from "@app/shared";
import { CrudService } from "@app/shared/services";
import { loggedInUserData } from "@modules/others";
import { PriceIncreaseRunDateConfigAddEditModel } from "@modules/others/configuration/models/price-increase-run-date";
import { CONFIGURATION_COMPONENT } from "@modules/others/configuration/utils/configuration-component.enum";
import {
  BillingModuleApiSuffixModels,
  SalesModuleApiSuffixModels,
} from "@modules/sales";
import { Store } from "@ngrx/store";
import { combineLatest, forkJoin, Observable } from "rxjs";
import { take } from "rxjs/operators";
@Component({
  selector: "pricing-run-date-configuration-view",
  templateUrl: "./pricing-run-date-configuration-view.component.html",
  styleUrls: ["./pricing-run-date-configuration-add-edit.component.scss"],
})
export class PriceRunDateViewConfigurationComponent implements OnInit {
  financialYearId: any;
  divisionId: any;
  priceIncreaseRunDateConfigForm: FormGroup;
  pricingIncreaseRunDateConfigDetails: FormArray;
  divisionList: any[] = [];
  financialPeriodList = [];
  financialYearList: any[];
  selectedOptions = [];
  divisionIds: any;
  timeLimitList = [];
  billRunDateConfigdetails: {};
  loggedInUserData: LoggedInUserModel;
  pricingIncreaseRunDateConfigId: any;
  statusList = [
    { value: true, displayName: "Automatic" },
    { value: false, displayName: "Manual" },
  ];
  startTodayDate = new Date();
  primengTableConfigProperties: any = {
    tableCaption: "View Price Increase Run Date Configuration",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [
      { displayName: "Configuration" },
      { displayName: "Billing" },
      {
        displayName: "Price Increase Run Date Configuration",
        relativeRouterUrl: "/billing/pricing-run-date-configuration",
      },
      { displayName: "View Price Increase Run Date Configuration" },
    ],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Price Increase Run Date Configuration",
          dataKey: "creditControllerTypeId",
          enableBreadCrumb: true,
          enableAction: true,
          enableViewBtn: true,
        },
      ],
    },
  };
  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rjxService: RxjsService,
    private snackbarService: SnackbarService
  ) {
    this.pricingIncreaseRunDateConfigId =
      this.activatedRoute.snapshot.queryParams.id;
    this.divisionIds = this.activatedRoute.snapshot.queryParams.divisionId;
    this.financialYearId =
      this.activatedRoute.snapshot.queryParams.financialYearId;
    combineLatest([this.store.select(loggedInUserData)])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createPriceIncreaseRunDateConfigAddEditForm();
    this.LoadDropdowns();
    this.approvingTimeLimit();
    if (this.pricingIncreaseRunDateConfigId) {
      this.getpriceIncreaseRunDateConfig().subscribe(
        (response: IApplicationResponse) => {
          if (response.statusCode == 200) {
            this.crudService
              .get(
                ModulesBasedApiSuffix.BILLING,
                BillingModuleApiSuffixModels.UX_PRICE_INCREASE_RUN_DATE_CONFIG_DIVISION,
                null,
                false,
                prepareGetRequestHttpParams(null, null, {
                  financialYearId: this.financialYearId,
                  IsCreateMode: false,
                })
              )
              .subscribe((response: IApplicationResponse) => {
                if (response && response.resources && response.isSuccess) {
                  this.divisionList =  getPDropdownData(response.resources)
                }
                this.rjxService.setGlobalLoaderProperty(false);
              });
            this.priceIncreaseRunDateConfigForm
              .get("financialYearId")
              .setValue(this.financialYearId);
            let options = this.divisionIds.split(",");
            this.selectedOptions = options;
            this.priceIncreaseRunDateConfigForm
              .get("createdUserId")
              .setValue(this.loggedInUserData.userId);
            this.pricingIncreaseRunDateConfigDetails =
              this.getBillRunDateConfig;
            this.pricingIncreaseRunDateConfigDetails.clear();
            if (
              response.resources.pricingIncreaseRunDateConfigDetails.length > 0
            ) {
              let binManagementMaster =
                new PriceIncreaseRunDateConfigAddEditModel(response.resources);
              this.billRunDateConfigdetails = binManagementMaster;
              response.resources.pricingIncreaseRunDateConfigDetails.forEach(
                (billRunDateConfig: PriceIncreaseRunDateConfigAddEditModel) => {
                  billRunDateConfig.notificationDate =
                    billRunDateConfig?.notificationDate
                      ? new Date(billRunDateConfig?.notificationDate)
                      : null;
                  billRunDateConfig.postDate = billRunDateConfig?.postDate
                    ? new Date(billRunDateConfig?.postDate)
                    : null;
                  this.pricingIncreaseRunDateConfigDetails.push(
                    this.createpriceIncreaseRunDateConfigFormGroup(
                      billRunDateConfig
                    )
                  );
                }
              );
            }
          }
          this.rjxService.setGlobalLoaderProperty(false);
        }
      );
      }
      this.priceIncreaseRunDateConfigForm.get("divisionIds").setValue([this.divisionIds]);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      let permission =
        response[0][CONFIGURATION_COMPONENT.PRICE_INCREASE_RUN_DATE_CONFIG];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(
            this.primengTableConfigProperties,
            permission
          );
        this.primengTableConfigProperties =
          prepareDynamicTableTabsFromPermissionsObj[
            "primengTableConfigProperties"
          ];
      }
    });
  }

  onFormControlChanges(): void {
    this.priceIncreaseRunDateConfigForm
      .get("financialYearId")
      .valueChanges.subscribe((financialYearId: string) => {
        if (!financialYearId) return;
        this.crudService
          .get(
            ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.UX_PRICE_INCREASE_RUN_DATE_CONFIG_DIVISION,
            null,
            false,
            prepareGetRequestHttpParams(null, null, {
              financialYearId: financialYearId,
              IsCreateMode: true,
            })
          )
          .subscribe((response: IApplicationResponse) => {
            if (response && response.resources && response.isSuccess) {
              this.divisionList =  getPDropdownData(response.resources)
            }
            this.rjxService.setGlobalLoaderProperty(false);
          });
      });

    this.priceIncreaseRunDateConfigForm
      .get("divisionIds")
      .valueChanges.subscribe((divisionIds: any) => {
        if (!divisionIds) return;
        this.divisionIds = divisionIds.join(",");
        this.getpriceIncreaseRunDateConfig().subscribe(
          (response: IApplicationResponse) => {
            if (response.statusCode == 200) {
              this.pricingIncreaseRunDateConfigDetails =
                this.getBillRunDateConfig;
              this.pricingIncreaseRunDateConfigDetails.clear();
              if (
                response.resources.pricingIncreaseRunDateConfigDetails.length >
                0
              ) {
                let binManagementMaster =
                  new PriceIncreaseRunDateConfigAddEditModel(
                    response.resources
                  );
                this.billRunDateConfigdetails = binManagementMaster;
                response.resources.pricingIncreaseRunDateConfigDetails.forEach(
                  (
                    billRunDateConfig: PriceIncreaseRunDateConfigAddEditModel
                  ) => {
                    billRunDateConfig.notificationDate =
                      billRunDateConfig?.notificationDate
                        ? new Date(billRunDateConfig?.notificationDate)
                        : null;
                    billRunDateConfig.postDate = billRunDateConfig?.postDate
                      ? new Date(billRunDateConfig?.postDate)
                      : null;
                    this.pricingIncreaseRunDateConfigDetails.push(
                      this.createpriceIncreaseRunDateConfigFormGroup(
                        billRunDateConfig
                      )
                    );
                  }
                );
              }
            }
            this.rjxService.setGlobalLoaderProperty(false);
          }
        );
      });
  }

  //Load Dropdowns
  LoadDropdowns() {
    this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.financialYearList = response[0].resources;
      this.financialYearList.forEach((item) => {
        item["displayName"] = item["displayName"].split("- ")[1];
        item["id"] = item["id"];
      });
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  //Get Details
  getpriceIncreaseRunDateConfig() {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PRICE_INCREASE_RUN_DATE_CONFIG_DETAILS,
      undefined,
      true,
      prepareGetRequestHttpParams(null, null, {
        financialYearId: this.financialYearId,
        divisionIds: this.divisionIds,
      })
    );
  }

  getAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(
        ModulesBasedApiSuffix.BILLING,
        SalesModuleApiSuffixModels.UX_BILLING_FINANCIAL_YEARS,
        undefined,
        true
      )
    );
  }

  approvingTimeLimit() {
    this.crudService
      .dropdown(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.UX_PRICE_INCREASE_APPROVING_TIME_LIMIT,
        prepareGetRequestHttpParams(null, null, {})
      )
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.timeLimitList = response.resources;
        }
      });
  }

  createPriceIncreaseRunDateConfigAddEditForm(): void {
    let priceIncreaseRunDateConfigAddEditModel =
      new PriceIncreaseRunDateConfigAddEditModel();
    this.priceIncreaseRunDateConfigForm = this.formBuilder.group({
      financialYearId: "",
      financialYear: "",
      divisionIds: "",
      pricingIncreaseRunDateConfigDetails: this.formBuilder.array([]),
    });
    Object.keys(priceIncreaseRunDateConfigAddEditModel).forEach((key) => {
      this.priceIncreaseRunDateConfigForm.addControl(
        key,
        new FormControl(priceIncreaseRunDateConfigAddEditModel[key])
      );
    });
    this.priceIncreaseRunDateConfigForm
      .get("createdUserId")
      .setValue(this.loggedInUserData.userId);
    this.priceIncreaseRunDateConfigForm = setRequiredValidator(
      this.priceIncreaseRunDateConfigForm,
      ["divisionIds"]
    );
  }
  //Create FormArray
  get getBillRunDateConfig(): FormArray {
    if (!this.priceIncreaseRunDateConfigForm) return;
    return this.priceIncreaseRunDateConfigForm.get(
      "pricingIncreaseRunDateConfigDetails"
    ) as FormArray;
  }
  //Create FormArray controls
  createpriceIncreaseRunDateConfigFormGroup(
    billRunDateConfig?: PriceIncreaseRunDateConfigAddEditModel
  ): FormGroup {
    let priceIncreaseRunDateConfigAddEditModel =
      new PriceIncreaseRunDateConfigAddEditModel(
        billRunDateConfig ? billRunDateConfig : undefined
      );
    let formControls = {};
    Object.keys(priceIncreaseRunDateConfigAddEditModel).forEach((key) => {
      formControls[key] = [
        {
          value: priceIncreaseRunDateConfigAddEditModel[key],
          disabled:
            billRunDateConfig &&
            key === "divisionIds" &&
            priceIncreaseRunDateConfigAddEditModel[key] !== ""
              ? true
              : false,
        },
        key === "financialYearDetailId" ? [Validators.required] : "",
      ];
    });
    return this.formBuilder.group(formControls);
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit();
        break;
        }
        }

  Edit() {
    if (
      !this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]
        .canEdit
    ) {
      return this.snackbarService.openSnackbar(
        PERMISSION_RESTRICTION_ERROR,
        ResponseMessageTypes.WARNING
      );
    }
    if (this.financialYearId && this.divisionIds) {
      this.router.navigate(
        ["billing/pricing-run-date-configuration/add-edit"],
        {
          queryParams: {
            id: this.pricingIncreaseRunDateConfigId,
            financialYearId: this.financialYearId,
            divisionIds: this.divisionIds,
          },
          skipLocationChange: true,
        }
      );
    }
  }

  onSubmit() {}
}
