import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { PriceIncreaseRunDateConfigAddEditModel } from '@modules/others/configuration/models/price-increase-run-date';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'pricing-run-date-configuration-add-edit',
  templateUrl: './pricing-run-date-configuration-add-edit.component.html',
  styleUrls: ['./pricing-run-date-configuration-add-edit.component.scss']
})
export class PriceRunDateAddEditConfigurationComponent implements OnInit {
  financialYearId: any;
  divisionId: any;
  priceIncreaseRunDateConfigForm: FormGroup;
  pricingIncreaseRunDateConfigDetails: FormArray;
  divisionList: any[] = [];
  adminFeeId: any;
  financialPeriodList = [];
  financialYearList: any[];
  billRunDateList = [];
  isButtondisabled = false;
  errorMessage: string;
  params: any = { billingFinancialYearId: '', };
  billingDate: any;
  todayDate: Date = new Date();
  isDuplicate = false;
  isUpdate = false;
  minBillRunDate = [];
  maxBillRunDate = [];
  tmpfinancialPeriod: any;
  selectedOptions = [];
  tmpBillingFinancialYearId: any;
  tmpindex: number
  tmpbillRunDateConfig: FormArray;
  fperiod: string;
  header: string;
  btnName: string;
  divisionIds: any;
  timeLimitList = [];
  billRunDateConfigdetails: {};
  FinancialYearValue: any;
  divisionName: any;
  loggedInUserData: LoggedInUserModel;
  pricingIncreaseRunDateConfigId: any;
  currentYear: any;
  lastYear: any;
  minDate: any;
  maxDate: any;
  statusList = [
    { value: true, displayName: 'Automatic' },
    { value: false, displayName: 'Manual' }
  ];
  startTodayDate = new Date();
  primengTableConfigProperties:any
  constructor(private formBuilder: FormBuilder, private store: Store<AppState>, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService, private router: Router, private rjxService: RxjsService) {
    this.pricingIncreaseRunDateConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.divisionIds = this.activatedRoute.snapshot.queryParams.divisionIds;
    this.financialYearId = this.activatedRoute.snapshot.queryParams.financialYearId;
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
      this.primengTableConfigProperties=  {
        tableCaption: `${this.pricingIncreaseRunDateConfigId?"Update":"Add"} Price Increase Run Date Configuration`,
        shouldShowBreadCrumb: false,
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' },
        { displayName: 'Billing ', relativeRouterUrl: '' },
          { displayName: 'Price Increase Run Date Configuration', relativeRouterUrl: '/billing/pricing-run-date-configuration'},
          { displayName: `${this.pricingIncreaseRunDateConfigId?"Update":"Add"} Price Increase Run Date Configuration`}],
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'Price Increase Config - Service Category',
              dataKey: 'creditControllerTypeId',
              enableBreadCrumb: true,
              enableAction: true,
            }]
        }
      }
      if(this.divisionIds){
          this.primengTableConfigProperties.breadCrumbItems[3] = {displayName:"View Price Increase Run Date Configuration", relativeRouterUrl:"/billing/pricing-run-date-configuration/view", queryParams:{id:this.pricingIncreaseRunDateConfigId, divisionId:this.divisionIds, financialYearId: this.financialYearId}}
      }
      this.primengTableConfigProperties.breadCrumbItems[this.divisionIds?4:3]= { displayName: `${this.pricingIncreaseRunDateConfigId?"Update":"Add"} Price Increase Run Date Configuration`};
  }

  ngOnInit() {
    this.createPriceIncreaseRunDateConfigAddEditForm();
    this.LoadDropdowns();
    this.onFormControlChanges();
    this.approvingTimeLimit();

    if (this.pricingIncreaseRunDateConfigId) {
      this.priceIncreaseRunDateConfigForm.get('financialYearId').setValue(this.financialYearId);
      this.getpriceIncreaseRunDateConfig().subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200) {
          let value = response.resources.financialYearId;
          let found = this.financialYearList.find(e => e.id == value);
          this.currentYear = found.displayName;
          this.lastYear = Number(this.currentYear) - 1;
          this.minDate = new Date(this.lastYear, 0, 1);
          this.maxDate = new Date(this.currentYear, 11, 31)
          this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PRICE_INCREASE_RUN_DATE_CONFIG_DIVISION, null, false,
            prepareGetRequestHttpParams(null, null,
              { financialYearId: this.financialYearId, IsCreateMode: false }))
            .subscribe((response: IApplicationResponse) => {
              if (response && response.resources && response.isSuccess) {
                this.divisionList = getPDropdownData(response.resources);
              }
              this.rjxService.setGlobalLoaderProperty(false);
            })
          this.priceIncreaseRunDateConfigForm.get('financialYearId').setValue(this.financialYearId);
          this.priceIncreaseRunDateConfigForm.get('divisionIds').setValue([this.divisionIds]);
          let options = this.divisionIds.split(',');
          this.selectedOptions = options;
          this.pricingIncreaseRunDateConfigDetails = this.getBillRunDateConfig;
          this.pricingIncreaseRunDateConfigDetails.clear();
          if (response.resources.pricingIncreaseRunDateConfigDetails.length > 0) {
            let binManagementMaster = new PriceIncreaseRunDateConfigAddEditModel(response.resources);
            this.billRunDateConfigdetails = binManagementMaster;
            response.resources.pricingIncreaseRunDateConfigDetails.forEach((billRunDateConfig: PriceIncreaseRunDateConfigAddEditModel) => {
              billRunDateConfig.notificationDate = billRunDateConfig?.notificationDate ? new Date(billRunDateConfig?.notificationDate) : null
              billRunDateConfig.postDate = billRunDateConfig?.postDate ? new Date(billRunDateConfig?.postDate) : null
              this.pricingIncreaseRunDateConfigDetails.push(this.createpriceIncreaseRunDateConfigFormGroup(billRunDateConfig));
            });
          }
        }
        this.rjxService.setGlobalLoaderProperty(false);
      });
    }
  }

  onChange(e) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PRICE_INCREASE_RUN_DATE_CONFIG_DIVISION, null, false,
      prepareGetRequestHttpParams(null, null,
        { financialYearId: e.target.value, IsCreateMode: true }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.divisionList = getPDropdownData(response.resources)
        }
        this.rjxService.setGlobalLoaderProperty(false);
      })
  }

  onFormControlChanges() {
    this.priceIncreaseRunDateConfigForm.get('divisionIds').valueChanges.subscribe((divisionIds: any) => {
      if (divisionIds.length > 0) {
        this.divisionIds = divisionIds.join(',')
        this.getpriceIncreaseRunDateConfig().subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200) {
            this.priceIncreaseRunDateConfigForm.get('pricingIncreaseRunDateConfigId').setValue(this.pricingIncreaseRunDateConfigId);
            this.pricingIncreaseRunDateConfigDetails = this.getBillRunDateConfig;
            this.pricingIncreaseRunDateConfigDetails.clear();
            if (response.resources.pricingIncreaseRunDateConfigDetails.length > 0) {
              let binManagementMaster = new PriceIncreaseRunDateConfigAddEditModel(response.resources);
              this.billRunDateConfigdetails = binManagementMaster;
              response.resources.pricingIncreaseRunDateConfigDetails.forEach((billRunDateConfig: PriceIncreaseRunDateConfigAddEditModel) => {
                billRunDateConfig.notificationDate = billRunDateConfig?.notificationDate ? new Date(billRunDateConfig?.notificationDate) : null
                billRunDateConfig.postDate = billRunDateConfig?.postDate ? new Date(billRunDateConfig?.postDate) : null
                this.pricingIncreaseRunDateConfigDetails.push(this.createpriceIncreaseRunDateConfigFormGroup(billRunDateConfig));
              });
            }
          }
          this.rjxService.setGlobalLoaderProperty(false);
        });
      }
    })
  }

  financialYear(e) {
    let value = this.priceIncreaseRunDateConfigForm.value.financialYearId;
    let found = this.financialYearList.find(e => e.id == value);
    this.currentYear = found.displayName;
    this.lastYear = Number(this.currentYear) - 1;
    this.minDate = new Date(this.lastYear, 0, 1);
    this.maxDate = new Date(this.currentYear, 11, 31)
  }
  //Load Dropdowns
  LoadDropdowns() {
    this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.financialYearList = response[0].resources;
      this.financialYearList.forEach(item => {
        item["displayName"] = item["displayName"].split("- ")[1];
        item["id"] = item["id"];
      });
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }
  //Get Details
  getpriceIncreaseRunDateConfig() {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICE_INCREASE_RUN_DATE_CONFIG_DETAILS, undefined, true, prepareGetRequestHttpParams(null, null, {
      financialYearId: this.priceIncreaseRunDateConfigForm.controls.financialYearId.value,
      divisionIds: this.divisionIds
    }));
  }

  getAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BILLING_FINANCIAL_YEARS, undefined, true),
    )
  }

  approvingTimeLimit() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PRICE_INCREASE_APPROVING_TIME_LIMIT,
      prepareGetRequestHttpParams(null, null, {})).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.timeLimitList = response.resources;
        }
      });
  }

  createPriceIncreaseRunDateConfigAddEditForm(): void {
    let priceIncreaseRunDateConfigAddEditModel = new PriceIncreaseRunDateConfigAddEditModel()
    this.priceIncreaseRunDateConfigForm = this.formBuilder.group({
      financialYearId: '',
      financialYear: '',
      divisionIds: '',
      pricingIncreaseRunDateConfigDetails: this.formBuilder.array([])
    });
    Object.keys(priceIncreaseRunDateConfigAddEditModel).forEach((key) => {
      this.priceIncreaseRunDateConfigForm.addControl(key, new FormControl(priceIncreaseRunDateConfigAddEditModel[key]));
    });
    this.priceIncreaseRunDateConfigForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.priceIncreaseRunDateConfigForm = setRequiredValidator(this.priceIncreaseRunDateConfigForm, ["divisionIds"]);
  }
  //Create FormArray
  get getBillRunDateConfig(): FormArray {
    if (!this.priceIncreaseRunDateConfigForm) return;
    return this.priceIncreaseRunDateConfigForm.get("pricingIncreaseRunDateConfigDetails") as FormArray;
  }
  //Create FormArray controls
  createpriceIncreaseRunDateConfigFormGroup(billRunDateConfig?: PriceIncreaseRunDateConfigAddEditModel): FormGroup {
    let priceIncreaseRunDateConfigAddEditModel = new PriceIncreaseRunDateConfigAddEditModel(billRunDateConfig ? billRunDateConfig : undefined);
    let formControls = {};
    Object.keys(priceIncreaseRunDateConfigAddEditModel).forEach((key) => {
      if (key === 'approvingTimeLimit' || key === 'notificationDate' || key === 'postDate') {
        formControls[key] = [{ value: priceIncreaseRunDateConfigAddEditModel[key], disabled: false }, [Validators.required]]
      }
      formControls[key] = [{
        value: priceIncreaseRunDateConfigAddEditModel[key], disabled: billRunDateConfig
          && (key === 'divisionIds') && priceIncreaseRunDateConfigAddEditModel[key] !== '' ? true : false
      },
      (key === 'financialYearDetailId' ? [Validators.required] : '')
      ]
    });
    let xyz = this.formBuilder.group(formControls);
    xyz = setRequiredValidator(xyz, ["approvingTimeLimit", "notificationDate", "postDate"]);
    return xyz;
  }

  onBillDateChange(event) {
    this.isDuplicate = false;
    if (this.getBillRunDateConfig.length > 1) {
      var lengthNew = this.getBillRunDateConfig.length - 1;
      var findIndex = this.getBillRunDateConfig.getRawValue().findIndex(x => x.billingDate == event && x.divisionId == this.getBillRunDateConfig.getRawValue()[lengthNew].divisionId && x.financialYearId == this.getBillRunDateConfig.getRawValue()[lengthNew].financialYearId);
      if (lengthNew != findIndex) {
        this.snackbarService.openSnackbar("Bill run date already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }
    }
  }

  onSubmit() {
    if (this.priceIncreaseRunDateConfigForm.invalid) {
      return
    }
    this.isButtondisabled = true;
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PRICE_INCREASE_RUN_DATE_CONFIG
      , this.priceIncreaseRunDateConfigForm.value)
      .subscribe({
        next: response => {
          if (response.isSuccess) {
            this.router.navigate(['billing/pricing-run-date-configuration']);
          } else {
            this.isButtondisabled = false;
          }
        },
        error: err => this.errorMessage = err
      });
    this.isButtondisabled = false;
  }
}
