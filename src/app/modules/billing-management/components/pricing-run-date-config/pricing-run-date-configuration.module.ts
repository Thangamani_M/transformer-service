import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PriceRunDateAddEditConfigurationComponent } from './pricing-run-date-configuration-add-edit.component';
import { PriceRunDateConfigurationComponent } from './pricing-run-date-configuration-list.component';
import { PricingRunDateConfigurationRoutingModule } from './pricing-run-date-configuration-routing.module';
import { PriceRunDateViewConfigurationComponent } from './pricing-run-date-configuration-view.component';
@NgModule({
  declarations: [PriceRunDateConfigurationComponent, PriceRunDateAddEditConfigurationComponent, PriceRunDateViewConfigurationComponent],
  imports: [
    CommonModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    PricingRunDateConfigurationRoutingModule,
    FormsModule,
    MaterialModule
  ],
  providers: [
    DatePipe
  ],
  entryComponents: []
})
export class PricingRunDateConfigurationModule { }
