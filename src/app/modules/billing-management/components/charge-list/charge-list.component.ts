import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-charge-list',
  templateUrl: './charge-list.component.html'
})
export class BillingChargeListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {}
  loggedUser: any;
  constructor(private router: Router, private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Charge List",
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '/billing' }, { displayName: 'Charge List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Charge List',
            dataKey: 'proformaInvoiceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'chargeListNumber', header: 'Charge List Number', width: '170px' },
              { field: 'chargeListTypeName', header: 'Charge List Type', width: '150px' },
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'startDate', header: 'Start Date', width: '150px' },
              { field: 'endDate', header: 'End Date', width: '150px' },
              { field: 'runDate', header: 'Run Date', width: '150px' },
              { field: 'exclVAT', header: 'Excl VAT', width: '150px' },
              { field: 'inclVAT', header: 'Incl VAT', width: '150px' },
              { field: 'discountExclVAT', header: 'Discount Excl VAT', width: '150px' },
              { field: 'discountTotal', header: 'Discount Total', width: '150px' },
              { field: 'totalExclVAT', header: 'Total Excl VAT', width: '150px' },
              { field: 'totalInclVAT', header: 'Total Incl VAT', width: '150px' },
              { field: 'debitOrderRunCodeName', header: 'Debit Order Run Code Name', width: '220px' },
              { field: 'originName', header: 'Origin Name', width: '150px' },
              { field: 'createdBy', header: 'Created By', width: '150px' },
              { field: 'createdDate', header: 'Created Date', width: '150px' },
              { field: 'crmStatus', header: 'Status', width: '150px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableExportBtn: false,
            enablePrintBtn: false,
            printTitle: 'Pro Forma Invoice',
            printSection: 'print-section0',
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.CHARGE_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let apiVersion = 1;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.CHARGE_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams),
      apiVersion
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['billing/charge-list/add-edit']);
        break;
      case CrudType.GET:
        this.getRequiredListData(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.VIEW:
        this.router.navigate(['billing/charge-list/view'], { queryParams: { id: row["chargeListId"] } });
        break;
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
