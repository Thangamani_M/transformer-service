import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { ItManagementApiSuffixModels, loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { BillingChargeListModel } from "../../models/billing-charge-list.models";

@Component({
    selector: 'app-charge-add-edit',
    templateUrl: './charge-add-edit.component.html'
  })
  export class BillingChargeAddEditComponent implements OnInit {
    primengTableConfigProperties: any;
    userData:any;
    startTodayDate=new Date();
    divisionList=[];
    runCodeList =[];
    originList=[];
    chargeTypeList=[];
    selectedTabIndex=0;
    chargeListForm:FormGroup;
    constructor(private momentService: MomentService, private router: Router,private store: Store<AppState>,
        private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
        this.primengTableConfigProperties = {
          tableCaption: 'Create Charge List',
          selectedTabIndex: 0,
          breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '/billing' },{ displayName: 'Charge List', relativeRouterUrl: '/billing/charge-list' }, { displayName: 'Create Charge List', relativeRouterUrl: '', }],
          tableComponentConfigs: {
            tabsList: [
              {
                caption: 'Dealer Branch',
                dataKey: 'branchId',
                enableBreadCrumb: true,
                enableAction: false,
                url: '',
              },
            ]
          }
        }
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
          if (!userData) return;
          this.userData = userData;
        })

      }

    ngOnInit(): void {
       this.createForm();
       this.getOrigins();
       this.getDivisions();
       this.getDebitorCodeDropdown();
       this.getChargeTypeListDropdown();
    }

    getDebitorCodeDropdown() {
      this.crudService.dropdown(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.UX_DEBIT_ORDER_RUN_CODES,
        undefined
      ).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
           this.runCodeList = data.resources;
        }

      });
    }

    getChargeTypeListDropdown() {
      this.crudService.dropdown(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.CHARGE_LIST_TYPES,
        undefined
      ).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
           this.chargeTypeList = data.resources;
        }
      });
    }

    createForm(billingChargeListModel?: BillingChargeListModel) {
      let dealerTypeModel = new BillingChargeListModel(billingChargeListModel);
      this.chargeListForm = this.formBuilder.group({});
      Object.keys(dealerTypeModel).forEach((key) => {
        if (dealerTypeModel[key] === 'billingChargeList') {
          this.chargeListForm.addControl(key, new FormArray(dealerTypeModel[key]));
        } else {
          this.chargeListForm.addControl(key, new FormControl());

        }
      });
      this.chargeListForm = setRequiredValidator(this.chargeListForm, ["startDate","endDate","divisionIds","chargeListTypeId"]);
    }

    getOrigins(){
     this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_ORIGINS).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
             this.originList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }

    getDivisions(){
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_DIVISIONS).subscribe((response) => {
         if (response.isSuccess && response.statusCode == 200) {
              this.divisionList = response.resources
         }
         this.rxjsService.setGlobalLoaderProperty(false);
       });
     }

     onChargeOptionsSelected(event){
       if(event.target.value == 1) {
            this.startTodayDate = new Date()
            this.chargeListForm.get('startDate').setValue(new Date());
            this.chargeListForm.get('endDate').setValue(new Date());
            this.chargeListForm.get('startDate').enable();
            this.chargeListForm.get('endDate').enable();
       }
       if(event.target.value == 2) {
        this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ACTIVE_FINANCIAL_PERIODS).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200 && response.resources) {
            this.startTodayDate = new Date(response.resources.startDate)
              this.chargeListForm.get('startDate').setValue(new Date(response.resources.startDate));
              this.chargeListForm.get('endDate').setValue(new Date(response.resources.endDate));
              this.chargeListForm.get('startDate').enable();
              this.chargeListForm.get('endDate').enable();
            }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
       }
     }

    onSubmit() {
      if (this.chargeListForm.invalid) {
        this.chargeListForm.markAllAsTouched();
        return;
      }
      let formValue = this.chargeListForm.value;
      formValue.startDate = formValue.startDate ? this.momentService.toMoment(formValue.startDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
      formValue.endDate = formValue.endDate ? this.momentService.toMoment(formValue.endDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
      let originId = [];let divisionId = [];    let debitOrderRunCodeId = [];
       if(formValue.originIds && formValue.originIds.length) {
        formValue.originIds.forEach(element => {
          originId.push(element.id);
         });
       }
       if(formValue.divisionIds &&  formValue.divisionIds.length) {
        formValue.divisionIds.forEach(element => {
          divisionId.push(element.id);
        });
       }
       if(formValue.debitOrderRunCodeIds &&  formValue.debitOrderRunCodeIds.length) {
        formValue.debitOrderRunCodeIds.forEach(element => {
          debitOrderRunCodeId.push(element.id);
         });
       }
     let finalObject = {
      startDate: formValue.startDate,
      endDate: formValue.endDate,
      divisionIds: divisionId,
      originIds: originId,
      debitOrderRunCodeIds: debitOrderRunCodeId,
      isManuallyCreated: true,
      chargeListTypeId : Number(formValue.chargeListTypeId),//
      createdUserId: this.userData.userId
    }
      let api = null;
      if(formValue.chargeListTypeId == 1 || formValue.chargeListTypeId.toString() == '1' )
         api =  this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_SAME_DAY_RUN, finalObject, 1);
      else if(formValue.chargeListTypeId == 2 || formValue.chargeListTypeId.toString() == '2' )
         api =  this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_MONTHLY_RUN, finalObject, 1);
      if(api){
        api.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
             this.router.navigate(['billing/charge-list']);
          }
          this.rxjsService.setDialogOpenProperty(false);
        });
      }

    }
  }
