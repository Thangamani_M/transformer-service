import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingChargeAddEditComponent } from './charge-add-edit.component';
import { BillingChargeListViewComponent } from './charge-list-view.component';
import { BillingChargeListComponent } from './charge-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: BillingChargeListComponent, canActivate: [AuthGuard], data: { title: 'Charge List' } },
  { path: 'add-edit', component: BillingChargeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Add/Edit Charge List' } },
  { path: 'view', component: BillingChargeListViewComponent, canActivate: [AuthGuard], data: { title: 'View Charge List' } }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class BillingChargeListRoutingModule { }
