import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-charge-list-view',
  templateUrl: './charge-list-view.component.html'
})
export class BillingChargeListViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};
  ChargeListViewDetail: any;
  loggedUser: any;
  chargeListId: any;
  chargeListDetails: any;
  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Charge List",
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '/billing' }, { displayName: 'Charge List', relativeRouterUrl: '/billing/charge-list' }, { displayName: 'View Charge List', relativeRouterUrl: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Charge List',
            dataKey: 'proformaInvoiceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'customerRefNo', header: 'Customer RefNo', width: '200px' },
              { field: 'branchName', header: 'Branch Name', width: '200px' },
              { field: 'categoryName', header: 'Category Name', width: '200px' },
              { field: 'contractRefNo', header: 'Contract RefNo', width: '200px' },
              { field: 'dealType', header: 'Deal Type', width: '200px' },
              { field: 'districtName', header: 'District Name', width: '200px' },
              { field: 'installOriginName', header: 'Install Origin Name', width: '200px' },
              { field: 'originName', header: 'Origin Name', width: '200px' },
              { field: 'serviceName', header: 'Service Name', width: '200px' },
              { field: 'paymentMethodName', header: 'Payment Method Name', width: '200px' },
              { field: 'invTotalExc', header: 'InvTotalExc', width: '200px' },
              { field: 'invTotalIncl', header: 'InvTotalIncl', width: '200px' },
              { field: 'totalExcl', header: 'TotalExcl', width: '200px' },
              { field: 'totalIncl', header: 'TotalIncl', width: '200px' },
              { field: 'billingDate', header: 'BillingDate', width: '200px',isDate:true },
              { field: 'nextBillDate', header: 'NextBillDate', width: '200px',isDate:true },
              { field: 'contractStartDate', header: 'Contract Start Date', width: '200px',isDate:true },
              { field: 'customerDescription', header: 'Customer Description', width: '200px' },
              { field: 'debtorCode', header: 'Debtor Code', width: '200px' },
              { field: 'billStartDate', header: 'Bill Start Date', width: '200px', isDate: true },
              { field: 'paymentFrequency', header: 'Payment Frequency', width: '200px' },
              { field: 'debitOrderRunCodeName', header: 'DORun Code', width: '200px' },
              { field: 'annualDiscountTotalExc', header: 'Annual Discount Total Exc', width: '200px' },
              { field: 'annualDiscountTotal', header: 'Annual Discount Total', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enablePrintBtn: false,
            printTitle: 'Pro Forma Invoice',
            printSection: 'print-section0',
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          }
        ]
      }
    }
    this.ChargeListViewDetail = [
      { name: 'Charge List Number', value: '' },
      { name: 'Debit Order Run CodeName', value: '' },
      { name: 'Origin Name', value: '' },
      { name: 'Is Active', value: '' },
      { name: 'Excl VAT', value: '' },
      { name: 'Incl VAT', value: '' },
      { name: 'Discount Excl VAT', value: '' },
      { name: 'Discount Total', value: '' },
      { name: 'Total Excl VAT', value: '' },
      { name: 'Total Incl VAT', value: '' },
      { name: 'Start Date', value: '' },
      { name: 'Run Date', value: '' },
      { name: 'Charge List Type', value: '' },
      { name: 'Created By', value: '' },
    ]
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.chargeListId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';

    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.getRequiredDetailData();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.CHARGE_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  paramss: any;
  getRequiredDetailData() {
    let params = { chargeListId: this.chargeListId };
    this.paramss = { ...params }
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CHARGE_LIST, this.chargeListId, false, this.paramss)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode = 200 && response.resources) {
          this.chargeListDetails = response.resources;
          this.ChargeListViewDetail = [
            { name: 'Charge List Number', value: response.resources?.chargeListNumber },
            { name: 'Debit Order Run CodeName', value: response.resources?.debitOrderRunCodeName },
            { name: 'Origin Name', value: response.resources?.originName },
            { name: 'Is Active', value: response.resources?.isActive },
            { name: 'Excl VAT', value: response.resources?.exclVAT ? 'R ' + parseFloat(response.resources?.exclVAT).toFixed(2) : 'R 0.00' },
            { name: 'Incl VAT', value: response.resources?.inclVAT ? 'R ' + parseFloat(response.resources?.inclVAT).toFixed(2) : 'R 0.00' },
            { name: 'Discount Excl VAT', value: response.resources?.discountExclVAT ? 'R ' + parseFloat(response.resources?.discountExclVAT).toFixed(2) : 'R 0.00' },
            { name: 'Discount Total', value: response.resources?.discountTotal ? 'R ' + parseFloat(response.resources?.discountTotal).toFixed(2) : 'R 0.00' },
            { name: 'Total Excl VAT', value: response.resources?.totalExclVAT ? 'R ' + parseFloat(response.resources?.totalExclVAT).toFixed(2) : 'R 0.00' },
            { name: 'Total Incl VAT', value: response.resources?.totalInclVAT ? 'R ' + parseFloat(response.resources?.totalInclVAT).toFixed(2) : 'R 0.00' },
            { name: 'Start Date', value: response.resources?.startDate },
            { name: 'Run Date', value: response.resources?.runDate },
            { name: 'Charge List Type', value: response.resources?.chargeListTypeName },
            { name: 'Created By', value: response.resources?.createdBy },
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    let chargeListObj = { chargeListId: this.chargeListId };
    otherParams = { ...otherParams, ...chargeListObj }
    this.loading = true;
    let apiVersion = 1;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.CHARGE_LIST_DETAIL,
      null,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams),
      apiVersion
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {

          val.invTotalExc = val.invTotalExc ? 'R ' + parseFloat(val.invTotalExc).toFixed(2) : 'R 0.00';
          val.invTotalIncl = val.invTotalIncl ? 'R ' + parseFloat(val.invTotalIncl).toFixed(2) : 'R 0.00';
          val.totalExcl = val.totalExcl ? 'R ' + parseFloat(val.totalExcl).toFixed(2) : 'R 0.00';
          val.totalIncl = val.totalIncl ? 'R ' + parseFloat(val.totalIncl).toFixed(2) : 'R 0.00';

          val.annualDiscountTotalExc = val.annualDiscountTotalExc ? 'R ' + parseFloat(val.annualDiscountTotalExc).toFixed(2) : 'R 0.00';
          val.annualDiscountTotal = val.annualDiscountTotal ? 'R ' + parseFloat(val.annualDiscountTotal).toFixed(2) : 'R 0.00';
          return val;
        })
      }
      return res;
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
