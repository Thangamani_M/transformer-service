import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BillingChargeAddEditComponent } from './charge-add-edit.component';
import { BillingChargeListViewComponent } from './charge-list-view.component';
import { BillingChargeListComponent } from './charge-list.component';
import { BillingChargeListRoutingModule } from './charge-list.component-routing.module';
@NgModule({
  declarations: [BillingChargeListComponent,BillingChargeAddEditComponent ,BillingChargeListViewComponent],
  imports: [
    CommonModule,
    BillingChargeListRoutingModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ],
  entryComponents:[BillingChargeAddEditComponent ],
})
export class BillingChargeListModule { }
