enum ReportsModuleApiSuffixModels {
  AGING_REPORT_LIST = 'aging-report-list',
  AGING_REPORT_EXPORT = 'aging-report/export',
  AGING_REPORT_DETAILED_LIST = 'aging-reportdetailed-list',
  AGING_REPORT_DETAILED_EXPORT = 'aging-report/detailed-export',
  INCENTIVE_GENERATE_REPORT = 'incentive-generate-report',
  INCENTIVE_REDUCTION_PERCENTAGE = 'incentive-reduction-percentage',
  INCENTIVE_REDUCTION_PERCENTAGE_AVERAGE_EXCL = 'incentive-reduction-percentage-average/current-excl',
  INCENTIVE_REDUCTION_QUALIFIED_LEVEL1 = 'incentive-reduction-qualified-level1',
  INCENTIVE_REDUCTION_QUALIFIED_LEVEL2 = 'incentive-reduction-qualified-level2',
  INCENTIVE_REDUCTION_MONTH_TO_DATE_PRODUCTIVITY = 'incentive-reduction-month-to-date-productivity',
  INCENTIVE_REDUCTION_OPENING = 'incentive-calculation-reduction-opening/detail',
  INCENTIVE_REDUCTION_CLOSING = 'incentive-calculation-reduction-closing',
  INCENTIVE_REDUCTION_BUCKETS = 'incentive-buckets',
  INCENTIVE_CALCULATION_AVERAGE_REDUCTION_ = 'incentive-calculation-average-reduction/detail',
  INCENTIVE_CALCULATION_EXPORT = 'export-incentive-reports',
  INCENTIVE_CALCULATION_INCENTIVES = 'incentive-calculation-reduction-incentives/detail',

}

enum REPORT_COMPONENTS {
  AGE_ANALYSIS_REPORTS = 'Age Analysis Reports',
  CLOSINGS = 'Closings',
  INCENTIVE_CALCULATION = 'Incentives Calculation',
  OPENINGS = 'Openings',
}
export { ReportsModuleApiSuffixModels, REPORT_COMPONENTS };

