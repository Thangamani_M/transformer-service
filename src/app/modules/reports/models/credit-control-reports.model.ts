class AgeAnalysisModel {
  divisionId?: string;
  bdiNumber?: string;
  branchId?: string;
  debtorRefNo?: string;
  controller?: string;
  billName?: string;

  constructor(ageAnalysisModel?: AgeAnalysisModel) {
    this.divisionId = ageAnalysisModel ? ageAnalysisModel.divisionId == undefined ? '' : ageAnalysisModel.divisionId : '';
    this.bdiNumber = ageAnalysisModel ? ageAnalysisModel.bdiNumber == undefined ? '' : ageAnalysisModel.bdiNumber : '';
    this.debtorRefNo = ageAnalysisModel ? ageAnalysisModel.debtorRefNo == undefined ? '' : ageAnalysisModel.debtorRefNo : '';
    this.controller = ageAnalysisModel ? ageAnalysisModel.controller == undefined ? '' : ageAnalysisModel.controller : '';
    this.billName = ageAnalysisModel ? ageAnalysisModel.billName == undefined ? '' : ageAnalysisModel.billName : '';
  }
}

class AgeAnalysisDetailedModel {
  divisionId?: string;
  bdiNumber?: string;
  branchId?: string;
  debtorRefNo?: string;
  controller?: string;
  billName?: string;
  latestPaidDate?: string;
  latestUnPaidDate?: string;
  risk?: string;
  latestDealer?: string;
  dealerName?: string;
  latestDealerWarrantyEndDate?: string;
  controllerNo?: string;
  smsDebtor?: string;
  cellNo?: string;
  phoneNo1?: string;
  phoneNo2?: string;
  faxNo?: string;
  email?: string;
  branch?: string;
  activeStatus?: string;
  bucketStatus?: string;
  bucketCount?: string;
  paymentMethod?: string;
  arrearStatus?: string;
  debitDate?:string
  offset?:string


  constructor(ageAnalysisDetailedModel?: AgeAnalysisDetailedModel) {
    this.divisionId = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.divisionId == undefined ? '' : ageAnalysisDetailedModel.divisionId : '';
    this.bdiNumber = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.bdiNumber == undefined ? '' : ageAnalysisDetailedModel.bdiNumber : '';
    this.debtorRefNo = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.debtorRefNo == undefined ? '' : ageAnalysisDetailedModel.debtorRefNo : '';
    this.controller = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.controller == undefined ? '' : ageAnalysisDetailedModel.controller : '';
    this.billName = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.billName == undefined ? '' : ageAnalysisDetailedModel.billName : '';
    this.latestPaidDate = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.latestPaidDate == undefined ? '' : ageAnalysisDetailedModel.latestPaidDate : '';
    this.latestUnPaidDate = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.latestUnPaidDate == undefined ? '' : ageAnalysisDetailedModel.latestUnPaidDate : '';
    this.risk = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.risk == undefined ? '' : ageAnalysisDetailedModel.risk : '';
    this.latestDealer = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.latestDealer == undefined ? '' : ageAnalysisDetailedModel.latestDealer : '';
    this.dealerName = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.dealerName == undefined ? '' : ageAnalysisDetailedModel.dealerName : '';
    this.latestDealerWarrantyEndDate = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.latestDealerWarrantyEndDate == undefined ? '' : ageAnalysisDetailedModel.latestDealerWarrantyEndDate : '';
    this.controllerNo = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.controllerNo == undefined ? '' : ageAnalysisDetailedModel.controllerNo : '';
    this.smsDebtor = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.smsDebtor == undefined ? '' : ageAnalysisDetailedModel.smsDebtor : '';
    this.cellNo = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.cellNo == undefined ? '' : ageAnalysisDetailedModel.cellNo : '';
    this.phoneNo1 = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.phoneNo1 == undefined ? '' : ageAnalysisDetailedModel.phoneNo1 : '';
    this.phoneNo2 = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.phoneNo2 == undefined ? '' : ageAnalysisDetailedModel.phoneNo2 : '';
    this.faxNo = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.faxNo == undefined ? '' : ageAnalysisDetailedModel.faxNo : '';
    this.email = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.email == undefined ? '' : ageAnalysisDetailedModel.email : '';
    this.branch = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.branch == undefined ? '' : ageAnalysisDetailedModel.branch : '';
    this.activeStatus = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.activeStatus == undefined ? '' : ageAnalysisDetailedModel.activeStatus : '';
    this.bucketStatus = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.bucketStatus == undefined ? '' : ageAnalysisDetailedModel.bucketStatus : '';
    this.bucketCount = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.bucketCount == undefined ? '' : ageAnalysisDetailedModel.bucketCount : '';
    this.paymentMethod = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.paymentMethod == undefined ? '' : ageAnalysisDetailedModel.paymentMethod : '';
    this.arrearStatus = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.arrearStatus == undefined ? '' : ageAnalysisDetailedModel.arrearStatus : '';
    this.debitDate = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.debitDate == undefined ? '' : ageAnalysisDetailedModel.debitDate : '';
    this.offset = ageAnalysisDetailedModel ? ageAnalysisDetailedModel.offset == undefined ? '' : ageAnalysisDetailedModel.offset : '';
  }
}
export { AgeAnalysisModel, AgeAnalysisDetailedModel };
