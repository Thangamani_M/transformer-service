import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AdvancedAgeAnalysisComponent } from './advanced-age-analysis/advanced-age-analysis.component';
import { CreditControlReportsRoutingModule } from './credit-control-reports-routing.module';
import { GenerateIncentiveCalculationComponent } from './incentives/generate-incentive-calculation/generate-incentive-calculation.component';
import { IncentivesCalculationReportsComponent } from './incentives/incentives-calculation-reports/incentives-calculation-reports.component';

@NgModule({
  declarations: [AdvancedAgeAnalysisComponent, IncentivesCalculationReportsComponent, GenerateIncentiveCalculationComponent],
  imports: [
    CommonModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    CreditControlReportsRoutingModule
  ]
})
export class CreditControlReportsModule { }
