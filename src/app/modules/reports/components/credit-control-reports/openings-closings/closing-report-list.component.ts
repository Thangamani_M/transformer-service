import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DailyStagingPayListFilter } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  prepareRequiredHttpParams,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

enum ReportType {
  OPENINGS = 'Openings',
  CLOSINGS = 'Closings',
}

@Component({
  selector: 'app-closing-report-list',
  templateUrl: './closing-report-list.component.html',
  styleUrls: ['./closing-report-list.component.scss']
})
export class ClosingReportListComponent implements OnInit {




      userData: UserLogin;
      stagingReportFilterForm: FormGroup;
      observableResponse: any;
      dataList: any
      deletConfirm: boolean = false;
      addConfirm: boolean = false;
      statusConfirm: boolean = false;
      showDialogSpinner: boolean = false;
      showFilterForm = false;
      public bradCrum: MatMenuItem[];
      status: any = [];
      selectedRows: string[] = [];
      selectedRow: any;
      selectedTabIndex: any = 0;
      totalRecords: any;
      pageLimit: number[] = [10, 25, 50, 75, 100];
      loggedInUserData: LoggedInUserModel;
      pageSize: number = 10;
      warehouseList = [];
      referenceNumberList = [];
      receivingBarCodeList = [];
      stockCodeList = [];
      dateFormat = 'MMM dd, yyyy';
      primengTableConfigProperties: any;

      columnFilterForm: FormGroup;
      searchForm: FormGroup
      searchColumns: any
      row: any = {}
      loading: boolean;
      startTodayDate = 0;
      reportTtype:any = ReportType.OPENINGS

      constructor(
        private commonService: CrudService,
        private snackbarService: SnackbarService, private tableFilterFormService: TableFilterFormService,
        private router: Router, private formBuilder: FormBuilder,
        private _fb: FormBuilder, private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService, private datePipe: DatePipe,
        private store: Store<AppState>, private momentService: MomentService,
      ) {

        this.primengTableConfigProperties = {
          tableCaption: "Closings",
          breadCrumbItems: [{ displayName: 'Reports', relativeRouterUrl: '' }, { displayName: 'Credit Control Reports', relativeRouterUrl: '' },
          { displayName: '', relativeRouterUrl: '' }
          ],
          selectedTabIndex: 0,
          tableComponentConfigs: {
            tabsList: [
              {
                caption: 'Automated',
                dataKey: 'incentiveAutomatedClosingId',
                enableBreadCrumb: true,
                enableReset: false,
                enableGlobalSearch: false,
                reorderableColumns: false,
                resizableColumns: false,
                enableScrollable: true,
                checkBox: false,
                enableRowDelete: false,
                enableFieldsSearch: true,
                enableHyperLink: true,
                cursorLinkIndex: 0,
                columns: [{ field: 'reportName', header: 'Report Name' }, { field: 'reportRunDate', header: 'Report Run Date' }],
                shouldShowDeleteActionBtn: false,
                shouldShowCreateActionBtn: false,
                shouldShowFilterActionBtn: false,
                areCheckboxesRequired: false,
                isDateWithTimeRequired: true,
                enableExportCSV: false,
                apiSuffixModel: CollectionModuleApiSuffixModels.INCENTIVE_AUTOMATED_CLOSING,
                moduleName: ModulesBasedApiSuffix.COLLECTIONS,
              },
              {
                caption: 'Manual',
                dataKey: 'incentiveManualClosingId',
                enableBreadCrumb: true,
                enableReset: false,
                enableGlobalSearch: false,
                reorderableColumns: false,
                resizableColumns: false,
                enableScrollable: true,
                checkBox: false,
                enableRowDelete: false,
                enableFieldsSearch: true,
                enableHyperLink: true,
                cursorLinkIndex: 0,
                shouldShowDeleteActionBtn: false,
                shouldShowCreateActionBtn: false,
                shouldShowFilterActionBtn: true,
                areCheckboxesRequired: false,
                isDateWithTimeRequired: true,
                enableExportCSV: false,
              }
            ]
          }
        }

        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
          if (!userData) return;
          this.userData = userData;
        });

        this.searchForm = this._fb.group({ searchKeyword: "" });
        this.columnFilterForm = this._fb.group({});

        this.activatedRoute.queryParamMap.subscribe((params) => {
          this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
          this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'Closings - ' + this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
        });

      }

      ngOnInit(): void {
        this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
        this.searchKeywordRequest();
        this.columnFilterRequest();
        this.combineLatestNgrxStoreData()
        this.getRequiredListData();
      }

      combineLatestNgrxStoreData() {
        combineLatest(
          this.store.select(loggedInUserData)
        ).subscribe((response) => {
          this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
      }

      searchKeywordRequest() {
        this.searchForm.valueChanges
          .pipe(
            debounceTime(debounceTimeForSearchkeyword),
            distinctUntilChanged(),
            switchMap(val => {
              return of(this.onCRUDRequested(CrudType.GET, {}));
            })
          )
          .subscribe();
      }

      columnFilterRequest() {
        this.columnFilterForm.valueChanges
          .pipe(
            debounceTime(debounceTimeForSearchkeyword),
            distinctUntilChanged(),
            switchMap(obj => {
              Object.keys(obj).forEach(key => {
                if (obj[key] === "") {
                  delete obj[key]
                }
              });
              this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
              this.row['searchColumns'] = this.searchColumns
              return of(this.onCRUDRequested(CrudType.GET, this.row));
            })
          )
          .subscribe();
      }

      getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
        InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
        this.commonService.get(
          ModulesBasedApiSuffix.COLLECTIONS,
          InventoryModuleApiSuffixModels,
          undefined,
          false, prepareGetRequestHttpParams(pageIndex, pageSize,
            otherParams ? otherParams : {
              UserId: this.userData.userId
            })
        ).subscribe(data => {
          this.loading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          if (data.isSuccess) {
            this.observableResponse = data.resources;
            this.dataList = this.observableResponse;
            this.totalRecords = data.totalCount;
          } else {
            this.observableResponse = null;
            this.dataList = this.observableResponse
            this.totalRecords = 0;

          }
        })
      }

      loadPaginationLazy(event) {
        let row = {}
        row['pageIndex'] = event.first / event.rows;
        row["pageSize"] = event.rows;
        row["sortOrderColumn"] = event.sortField;
        row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
        this.row = row;
        this.onCRUDRequested(CrudType.GET, this.row);
      }

      onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
        switch (type) {
          case CrudType.CREATE:
            this.openAddEditPage(CrudType.CREATE, row);
            break;
          case CrudType.GET:
            let otherParams = {};
            if (this.searchForm.value.searchKeyword) {
              otherParams["search"] = this.searchForm.value.searchKeyword;
            }
            if (Object.keys(this.row).length > 0) {
              // logic for split columns and its values to key value pair

              if (this.row['searchColumns']) {
                Object.keys(this.row['searchColumns']).forEach((key) => {
                  if (key.toLowerCase().includes('date')) {
                    otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
                  } else {
                    otherParams[key] = this.row['searchColumns'][key];
                  }
                });
              }
              if (this.row['sortOrderColumn']) {
                otherParams['sortOrder'] = this.row['sortOrder'];
                otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
              }
            }
            otherParams['UserId'] = this.userData.userId;
            this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
            break;
          case CrudType.EDIT:
            this.openAddEditPage(CrudType.VIEW, row);
            break;
          case CrudType.FILTER:
            this.displayAndLoadFilterData();
            break;
          default:
        }
      }

      onTabChange(event) {
        if (event.index == 1) {
          this.router.navigate(['/reports', 'credit-control-reports','openings-closings', 'add-edit'], {
            queryParams: {
              reportTtype: ReportType.CLOSINGS
            }
          });
          return false;
        }
        this.row = {}
        this.columnFilterForm = this._fb.group({})
        this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
        this.columnFilterRequest();
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = event.index
        this.router.navigate(['/reports/credit-control-reports/openings-closings/closings'], { queryParams: { tab: this.selectedTabIndex,reportTtype: ReportType.CLOSINGS } })
        this.getRequiredListData()
      }

      openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
        switch (type) {
          case CrudType.CREATE:
            break;
          case CrudType.VIEW:
            this.router.navigate(['/reports', 'credit-control-reports','openings-closings', 'view'], {
              queryParams: {
                id: editableObject['incentiveAutomatedClosingId'],
                reportTtype: ReportType.CLOSINGS,
                tab:0
              }
            });
            break;
        }
      }

      displayAndLoadFilterData() {
        this.createstagingReportFilterForm();
        this.showFilterForm = !this.showFilterForm;
        this.warehouseList = [];
        this.referenceNumberList = [];
        forkJoin([
          this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
          this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SEARCH),
        ]).subscribe((response: IApplicationResponse[]) => {
          response.forEach((resp: IApplicationResponse, ix: number) => {
            if (resp.isSuccess && resp.statusCode === 200) {
              switch (ix) {
                case 0:
                  let warehouseList = resp.resources;
                  for (var i = 0; i < warehouseList.length; i++) {
                    let tmp = {};
                    tmp['value'] = warehouseList[i].id;
                    tmp['display'] = warehouseList[i].displayName;
                    this.warehouseList.push(tmp)
                  }
                  break;
                case 1:
                  let referenceNumberList = resp.resources;
                  for (var i = 0; i < referenceNumberList.length; i++) {
                    let tmp = {};
                    tmp['value'] = referenceNumberList[i].id;
                    tmp['display'] = referenceNumberList[i].displayName;
                    this.referenceNumberList.push(tmp)
                  }
                  break;
              }
            }
          })
          this.rxjsService.setGlobalLoaderProperty(false);
        })

      }

      createstagingReportFilterForm(stagingPayListModel?: DailyStagingPayListFilter) {
        let DailyStagingPayListModel = new DailyStagingPayListFilter(stagingPayListModel);
        this.stagingReportFilterForm = this.formBuilder.group({});
        Object.keys(DailyStagingPayListModel).forEach((key) => {
          if (typeof DailyStagingPayListModel[key] === 'string') {
            this.stagingReportFilterForm.addControl(key, new FormControl(DailyStagingPayListModel[key]));
          }
        });
      }

      getSelectedReferenceNumber(reference): void {
        this.receivingBarCodeList = [];
        if (reference != '') {
          let params = new HttpParams().set('PurchaseOrderIds', reference);
          this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIVING_BARCODE_LIST, undefined, true, params).subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
              let receivingBarCodeList = response.resources;
              for (var i = 0; i < receivingBarCodeList.length; i++) {
                let tmp = {};
                tmp['value'] = receivingBarCodeList[i].id;
                tmp['display'] = receivingBarCodeList[i].displayName;
                this.receivingBarCodeList.push(tmp)
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
        }
      }

      getSelectedRecevingBarcode(stockCode): void {
        this.stockCodeList = [];
        if (stockCode != '') {
          let params = new HttpParams().set('OrderReceiptBatchIds', stockCode);
          this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STAGING_STOCK_CODE_LIST, undefined, true, params).subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
              let stockCodeList = response.resources;
              for (var i = 0; i < stockCodeList.length; i++) {
                let tmp = {};
                tmp['value'] = stockCodeList[i].id;
                tmp['display'] = stockCodeList[i].displayName;
                this.stockCodeList.push(tmp)
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
        }
      }

      submitFilter() {
        let data = Object.assign({},
          this.stagingReportFilterForm.get('warehouseId').value == '' ? null : { WarehouseIds: this.stagingReportFilterForm.get('warehouseId').value },
          this.stagingReportFilterForm.get('referenceNumberId').value == '' ? null : { PurchaseOrderIds: this.stagingReportFilterForm.get('referenceNumberId').value },
          this.stagingReportFilterForm.get('receivingBarcodeId').value == '' ? null : { OrderReceiptBatchIds: this.stagingReportFilterForm.get('receivingBarcodeId').value },
          this.stagingReportFilterForm.get('fromDate').value == '' ? null : { FromDate: this.datePipe.transform(this.stagingReportFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
          this.stagingReportFilterForm.get('toDate').value == '' ? null : { ToDate: this.datePipe.transform(this.stagingReportFilterForm.get('toDate').value, 'yyyy-MM-dd') },
          this.stagingReportFilterForm.get('stockCodeId').value == '' ? null : { ItemIds: this.stagingReportFilterForm.get('stockCodeId').value }, { UserId: this.userData.userId }
        );
        this.observableResponse = this.getRequiredListData('', '', data);
        this.showFilterForm = !this.showFilterForm;
      }

      resetForm() {

        this.stagingReportFilterForm.reset();
        this.observableResponse = this.getRequiredListData('', '', null);
        this.showFilterForm = !this.showFilterForm;
      }

      exportList() {
        if (this.dataList.length != 0) {
          let fileName = 'Closing Report' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
          let columnNames = ["Report Name", "Report Run Date"];
          let header = columnNames.join(',');
          let csv = header;
          csv += '\r\n';
          this.dataList.map(c => {
            csv += [c['reportName'], c['reportRunDate']].join(',');
            csv += '\r\n';
          })
          var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
          var link = document.createElement("a");
          if (link.download !== undefined) {
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", fileName);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }
        }
        else {
          this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
        }
      }
    }



