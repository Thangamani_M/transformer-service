import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpeningsClosingsAddEditComponent } from './openings-closings-add-edit.component';
import { OpeningsClosingsViewComponent } from './openings-closings-view.component';
import { OpeningReportListComponent } from './opening-report-list.component';
import { ClosingReportListComponent } from './closing-report-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', redirectTo: 'openings', canActivate: [AuthGuard], pathMatch: 'full' },
  { path: 'openings', component: OpeningReportListComponent, canActivate: [AuthGuard], data: { title: 'Openings List' } },
  { path: 'closings', component: ClosingReportListComponent, canActivate: [AuthGuard], data: { title: 'Closings List' } },
  { path: 'view', component: OpeningsClosingsViewComponent, canActivate: [AuthGuard], data: { title: 'Openings Closings view' } },
  { path: 'add-edit', component: OpeningsClosingsAddEditComponent, canActivate: [AuthGuard], data: { title: 'Openings Closings' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class OpeningsClosingsRoutingModule { }
