import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { GenerateReportCreationFilter } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { REPORT_COMPONENTS } from '@modules/reports/shared';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';

enum ReportType {
  OPENINGS = 'Openings',
  CLOSINGS = 'Closings',
}

@Component({
  selector: 'app-openings-closings-add-edit',
  templateUrl: './openings-closings-add-edit.component.html',
  styleUrls: ['./openings-closings-add-edit.component.scss']
})
export class OpeningsClosingsAddEditComponent implements OnInit {

  userData: UserLogin;
  stagingReportFilterForm: FormGroup;
  observableResponse: any;
  dataList: any
  deletConfirm: boolean = false;
  addConfirm: boolean = false;
  statusConfirm: boolean = false;
  showDialogSpinner: boolean = false;
  showFilterForm = false;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedRows: string[] = [];
  selectedRow: any;
  selectedTabIndex: any = 0;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  pageSize: number = 10;
  stagingBayReportId: string;
  stagingPayReportDetails: any = null;
  generateReportForm: FormGroup;
  warehouseList = [];
  referenceNumberList = [];
  receivingBarCodeList = [];
  stockCodeList = [];
  stockInfoDetails = [];
  stockFullInfoDetails = [];
  systemTypePostDTOs: FormArray;
  dateFormat = 'MMM dd, yyyy';
  tabIndex: any;
  todayDate = new Date();
  startTodayDate = new Date();
  reportTtype: any
  primengTableConfigProperties: any
  columnFilterForm: FormGroup;
  searchForm: FormGroup
  searchColumns: any
  row: any = {}
  loading: boolean;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{disabled:true},{disabled:true}]
    }
  }
  constructor(
    private httpService: CrudService, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    private formBuilder: FormBuilder, private datePipe: DatePipe, private store: Store<AppState>, private momentService: MomentService
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })

    this.activatedRoute.queryParamMap.subscribe((params) => {
      // this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.selectedTabIndex = 0;
      this.reportTtype = params['params']['reportTtype'];
    });

    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});



    this.primengTableConfigProperties = {
      tableCaption: "Openings",
      breadCrumbItems: [{ displayName: 'Reports', relativeRouterUrl: '' }, { displayName: 'Credit Control Reports', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Automated',
            dataKey: '',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'reportName', header: 'Report Name' }, { field: 'reportRunDate', header: 'Report Run Date' }],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: false,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_OPENING,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          },
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.getRequiredListData();
    this.getDropdownList()
    this.createstagingReportFilterForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][this.reportTtype == "Closings" ? REPORT_COMPONENTS.CLOSINGS : REPORT_COMPONENTS.OPENINGS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    this.httpService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      (this.reportTtype == ReportType.OPENINGS) ? CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_OPENING : CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_CLOSING,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          UserId: this.userData.userId
        })
    ).pipe(map(res => {
      if (res.resources.length > 0) {
        res.resources.forEach(element => {
        });
      }
      return res
    }))
      .subscribe(data => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        } else {
          this.observableResponse = null;
          this.dataList = this.observableResponse
          this.totalRecords = 0;
        }
      })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        otherParams['UserId'] = this.userData.userId;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.VIEW:
        this.router.navigate(['/reports', 'credit-control-reports', 'openings-closings', 'view'], {
          queryParams: {
            id: (this.reportTtype == ReportType.OPENINGS) ? editableObject['incentiveManualOpeningId'] : editableObject['incentiveManualClosingId'],
            reportTtype: this.reportTtype,
            tab: 1
          }
        });
        break;
    }
  }

  getDropdownList() {
    this.warehouseList = [];
    this.referenceNumberList = [];
    forkJoin([
      this.httpService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS),
      this.httpService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CREDIT_CONTROLLER),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let warehouseList = resp.resources;
              for (var i = 0; i < warehouseList.length; i++) {
                let tmp = {};
                tmp['value'] = warehouseList[i].id;
                tmp['display'] = warehouseList[i].displayName;
                this.warehouseList.push(tmp)
              }
              break;
            case 1:
              let referenceNumberList = resp.resources;
              for (var i = 0; i < referenceNumberList.length; i++) {
                let tmp = {};
                tmp['value'] = referenceNumberList[i].id;
                tmp['display'] = referenceNumberList[i].displayName;
                this.referenceNumberList.push(tmp)
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  createstagingReportFilterForm(stagingPayListModel?: GenerateReportCreationFilter) {
    this.generateReportForm = this.formBuilder.group({
      divisionIds: [''],
      creditControllerIds: [''],
      reportRunDate: [''],
      createdUserId: [this.userData.userId]
    });
  }

  generateReport() {

    let data = Object.assign({},
      this.generateReportForm.get('divisionIds').value == '' || this.generateReportForm.get('divisionIds').value == null ? null :
        (this.reportTtype == ReportType.OPENINGS) ? { divisionIds: this.generateReportForm.get('divisionIds').value.toString() } : { divisionIds: this.generateReportForm.get('divisionIds').value },
      this.generateReportForm.get('creditControllerIds').value == '' || this.generateReportForm.get('creditControllerIds').value == null ? null :
        (this.reportTtype == ReportType.OPENINGS) ? { creditControllerIds: this.generateReportForm.get('creditControllerIds').value.toString() } : { creditControllerIds: this.generateReportForm.get('creditControllerIds').value },
       this.generateReportForm.get('reportRunDate').value == '' || this.generateReportForm.get('reportRunDate').value == null ? '' :
        { reportRunDate: this.datePipe.transform(this.generateReportForm.get('reportRunDate').value, 'yyyy-MM-dd') },
      { createdUserId: this.userData.userId }

    );

    let crudService = (this.reportTtype == ReportType.OPENINGS) ? this.httpService.create(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_OPENING, data) : this.httpService.create(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_CLOSING, data)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.getRequiredListData();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  submitFilter() {

    this.stockInfoDetails.forEach(value => {
      value.createdDate = new Date().toISOString();
      value.createdUserId = this.userData.userId;
    });

    let crudService: Observable<IApplicationResponse> = this.httpService.create(
      ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DAILY_STAGING_PAY_REPORT, this.stockInfoDetails)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.navigateToList();
      }
      else {
        this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
      }
    });
  }

  onTabClicked(tabIndex) {
    this.tabIndex = tabIndex.index;
  }

  exportList() {
    if (this.dataList.length != 0) {
      let fileName = this.reportTtype + ' Report' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["Report Name", "Report Run Date"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList.map(c => {
        csv += [c['reportName'], c['reportRunDate']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
    else {
      this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
    }
  }

  onTabChange(event) {

  }

  navigateToList() {
    if (this.reportTtype == ReportType.OPENINGS) {
      this.router.navigate(['/reports', 'credit-control-reports', 'openings-closings', 'openings']);
    } else {
      this.router.navigate(['/reports', 'credit-control-reports', 'openings-closings', 'closings']);
    }

  }
}

