import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ClosingReportListComponent } from './closing-report-list.component';
import { OpeningReportListComponent } from './opening-report-list.component';
import { OpeningsClosingsAddEditComponent } from './openings-closings-add-edit.component';
import { OpeningsClosingsRoutingModule } from './openings-closings-routing.module';
import { OpeningsClosingsViewComponent } from './openings-closings-view.component';



@NgModule({
  declarations: [OpeningsClosingsAddEditComponent, OpeningsClosingsViewComponent, OpeningReportListComponent, ClosingReportListComponent],
  imports: [
    CommonModule,
    OpeningsClosingsRoutingModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
   
  ]
})
export class OpeningsClosingsModule { }
