import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DailyStagingPayListFilter } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  prepareRequiredHttpParams,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

enum ReportType {
  OPENINGS = 'Openings',
  CLOSINGS = 'Closings',
}

@Component({
  selector: 'app-openings-closings-view',
  templateUrl: './openings-closings-view.component.html'
})
export class OpeningsClosingsViewComponent implements OnInit {


  userData: UserLogin;
  stagingReportFilterForm: FormGroup;
  observableResponse: any;
  dataList: any
  deletConfirm: boolean = false;
  addConfirm: boolean = false;
  statusConfirm: boolean = false;
  showDialogSpinner: boolean = false;
  showFilterForm = false;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedRows: string[] = [];
  selectedRow: any;
  selectedTabIndex: any = 0;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;

  warehouseList = [];
  referenceNumberList = [];
  receivingBarCodeList = [];
  stockCodeList = [];
  dateFormat = 'MMM dd, yyyy';
  primengTableConfigProperties: any;
  pageSize: number = 10;
  columnFilterForm: FormGroup;
  searchForm: FormGroup
  searchColumns: any
  row: any = {}
  loading: boolean;
  startTodayDate = 0;
  reportTtype: any
  id: any
  tab: Number = 0
  constructor(
    private commonService: CrudService,
    private snackbarService: SnackbarService, private tableFilterFormService: TableFilterFormService,
    private router: Router, private formBuilder: FormBuilder,
    private _fb: FormBuilder, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private datePipe: DatePipe,
    private store: Store<AppState>, private momentService: MomentService,
  ) {

    this.primengTableConfigProperties = {
      tableCaption: this.reportTtype,
      breadCrumbItems: [
        { displayName: 'Reports', relativeRouterUrl: '' },
        { displayName: 'Credit Control Reports', relativeRouterUrl: '/reports/credit-control-reports' },
        { displayName: this.reportTtype, relativeRouterUrl: '/reports/credit-control-reports/openings-closings/' + this.reportTtype?.toLowerCase() },
        { displayName: '', relativeRouterUrl: '' }
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: (this.reportTtype == ReportType.OPENINGS) ? 'incentiveManualOpeningId' : 'incentiveManualClosingDetailId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' },
            // { field: 'creditControllerCode', header: 'Credit Controller Code',width:'200px'  },
            { field: 'creditControllerName', header: 'Credit Controller Name', width: '200px' },
            // { field: 'debtorCode', header: 'Debtor Code',width:'150px'  },
            { field: 'bdiNumber', header: 'BDI', width: '150px' },
            { field: 'billName', header: 'Bill Name', width: '150px' },
            { field: 'totalDueAmount', header: 'Total Due', width: '150px' },
            { field: 'currentPercentage', header: 'Current', width: '150px' },
            { field: 'amount30Days', header: '30 Days', width: '150px' },
            { field: 'amount60Days', header: '60 Days', width: '150px' },
            { field: 'amount90Days', header: '90 Days', width: '150px' },
            { field: 'amount120Days', header: '120 Days', width: '150px' },
            { field: 'amount120PlusDays', header: '120+ Days', width: '150px' },
            { field: 'notes', header: 'Notes', width: '200px' },
            { field: 'adjustment', header: 'Adjustments', width: '150px' }],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: (this.reportTtype == ReportType.OPENINGS) ? CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_OPENING_DETAILS : CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_CLOSING_DETAILS,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          },
        ]
      }
    }

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      // this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.selectedTabIndex = 0;
      this.id = params['params']['id'];
      this.reportTtype = params['params']['reportTtype'];
      this.tab = Number(params['params']['tab']);
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      // this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.reportTtype;
      // if (this.tab == 0) {
      //   this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = '/reports/credit-control-reports/openings-closings/' + this.reportTtype?.toLowerCase()
      // } else {
      //   this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = '/reports/credit-control-reports/openings-closings/add-edit?reportType=' + this.reportTtype?.toLowerCase()
      // }
      // this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.reportTtype + ' - ' + (this.tab == 0) ? 'Automated' : 'Manual';
      // this.primengTableConfigProperties.tableCaption = this.reportTtype
    });

  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (!otherParams) {
      otherParams = {}
    }
    if (this.reportTtype == ReportType.OPENINGS) {
      if(this.tab == 0){
        otherParams['incentiveAutomatedOpeningId'] = this.id
      }else{
        otherParams['incentiveManualOpeningId'] = this.id
      }
    } else {
      if(this.tab == 0){
        otherParams['incentiveAutomatedClosingId'] = this.id
      }else{
        otherParams['incentiveManualClosingId'] = this.id
      }
    }
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      (this.reportTtype == ReportType.OPENINGS) ? (this.tab == 0) ? CollectionModuleApiSuffixModels.INCENTIVE_AUTOMATED_OPENING_DETAILS :  CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_OPENING_DETAILS : (this.tab == 0) ? CollectionModuleApiSuffixModels.INCENTIVE_AUTOMATED_CLOSING_DETAILS : CollectionModuleApiSuffixModels.INCENTIVE_MANUAL_CLOSING_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          // UserId: this.userData.userId
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        otherParams['UserId'] = this.userData.userId;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      default:
    }
  }

  onTabChange(event) {
    if (event.index == 1) {
      this.router.navigate(['/reports', 'credit-control-reports', 'openings-closings', 'add-edit'], {
        queryParams: {
          reportTtype: this.reportTtype
        }
      });
      return false;
    }
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['//reports/credit-control-reports/openings-closings'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.VIEW:
        this.router.navigate(['/reports', 'credit-control-reports', 'openings-closings', 'view'], {
          queryParams: {
            id: editableObject['incentiveManualClosingId'],
            reportTtype: this.reportTtype
          }
        });
        break;
    }
  }

  displayAndLoadFilterData() {
    this.createstagingReportFilterForm();
    this.showFilterForm = !this.showFilterForm;
    this.warehouseList = [];
    this.referenceNumberList = [];
    forkJoin([
      this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
      this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SEARCH),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let warehouseList = resp.resources;
              for (var i = 0; i < warehouseList.length; i++) {
                let tmp = {};
                tmp['value'] = warehouseList[i].id;
                tmp['display'] = warehouseList[i].displayName;
                this.warehouseList.push(tmp)
              }
              break;
            case 1:
              let referenceNumberList = resp.resources;
              for (var i = 0; i < referenceNumberList.length; i++) {
                let tmp = {};
                tmp['value'] = referenceNumberList[i].id;
                tmp['display'] = referenceNumberList[i].displayName;
                this.referenceNumberList.push(tmp)
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })

  }

  createstagingReportFilterForm(stagingPayListModel?: DailyStagingPayListFilter) {
    let DailyStagingPayListModel = new DailyStagingPayListFilter(stagingPayListModel);
    this.stagingReportFilterForm = this.formBuilder.group({});
    Object.keys(DailyStagingPayListModel).forEach((key) => {
      if (typeof DailyStagingPayListModel[key] === 'string') {
        this.stagingReportFilterForm.addControl(key, new FormControl(DailyStagingPayListModel[key]));
      }
    });
  }

  getSelectedReferenceNumber(reference): void {
    this.receivingBarCodeList = [];
    if (reference != '') {
      let params = new HttpParams().set('PurchaseOrderIds', reference);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIVING_BARCODE_LIST, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let receivingBarCodeList = response.resources;
          for (var i = 0; i < receivingBarCodeList.length; i++) {
            let tmp = {};
            tmp['value'] = receivingBarCodeList[i].id;
            tmp['display'] = receivingBarCodeList[i].displayName;
            this.receivingBarCodeList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getSelectedRecevingBarcode(stockCode): void {
    this.stockCodeList = [];
    if (stockCode != '') {
      let params = new HttpParams().set('OrderReceiptBatchIds', stockCode);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STAGING_STOCK_CODE_LIST, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let stockCodeList = response.resources;
          for (var i = 0; i < stockCodeList.length; i++) {
            let tmp = {};
            tmp['value'] = stockCodeList[i].id;
            tmp['display'] = stockCodeList[i].displayName;
            this.stockCodeList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  submitFilter() {
    let data = Object.assign({},
      this.stagingReportFilterForm.get('warehouseId').value == '' ? null : { WarehouseIds: this.stagingReportFilterForm.get('warehouseId').value },
      this.stagingReportFilterForm.get('referenceNumberId').value == '' ? null : { PurchaseOrderIds: this.stagingReportFilterForm.get('referenceNumberId').value },
      this.stagingReportFilterForm.get('receivingBarcodeId').value == '' ? null : { OrderReceiptBatchIds: this.stagingReportFilterForm.get('receivingBarcodeId').value },
      this.stagingReportFilterForm.get('fromDate').value == '' ? null : { FromDate: this.datePipe.transform(this.stagingReportFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.stagingReportFilterForm.get('toDate').value == '' ? null : { ToDate: this.datePipe.transform(this.stagingReportFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      this.stagingReportFilterForm.get('stockCodeId').value == '' ? null : { ItemIds: this.stagingReportFilterForm.get('stockCodeId').value }, { UserId: this.userData.userId }
    );
    this.observableResponse = this.getRequiredListData('', '', data);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {

    this.stagingReportFilterForm.reset();
    this.observableResponse = this.getRequiredListData('', '', null);
    this.showFilterForm = !this.showFilterForm;
  }

  exportList() {
    if (this.dataList.length != 0) {
      let fileName = this.reportTtype + ' Report' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["Division", "Credit Controller Name", "BDI", "Bill Name", "Total Due", "Current", "30 Days", "60 Days", "90 Days", "120 Days", "120+ Days", "Notes", "Adjustments"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList.map(c => {
        csv += [c['divisionName'], c['creditControllerName'], c['bdiNumber'], c['billName'], c['totalDueAmount'], c['currentPercentage'],
        c['amount30Days'], c['amount60Days'], c['amount90Days'], c['amount120Days'], c['amount120PlusDays'], c['notes'], c['adjustment']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
    else {
      this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
    }
  }





}

