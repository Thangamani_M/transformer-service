import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvancedAgeAnalysisComponent } from './advanced-age-analysis/advanced-age-analysis.component';
import { GenerateIncentiveCalculationComponent } from './incentives/generate-incentive-calculation/generate-incentive-calculation.component';
import { IncentivesCalculationReportsComponent } from './incentives/incentives-calculation-reports/incentives-calculation-reports.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: 'advanced-age-analysis', component: AdvancedAgeAnalysisComponent, canActivate: [AuthGuard], data: { title: 'Advanced Age Analysis' } },
  { path: 'openings-closings', loadChildren: () => import('./openings-closings/openings-closings.module').then(m => m.OpeningsClosingsModule) },
  { path: 'incentives-calculation-reports', component: IncentivesCalculationReportsComponent, canActivate: [AuthGuard], data: { title: 'Incentives Calculation Reports' } },
  { path: 'generate-incentive-calculation', component: GenerateIncentiveCalculationComponent, canActivate: [AuthGuard], data: { title: 'Incentives Calculation Reports' } },
  { path: 'incentives-reports', loadChildren: () => import('./incentives/incentive-reports/incentive-reports.module').then(m => m.IncentiveReportsModule) },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class CreditControlReportsRoutingModule { }
