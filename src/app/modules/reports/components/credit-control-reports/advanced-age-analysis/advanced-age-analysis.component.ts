import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SharedModuleApiSuffixModels, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AgeAnalysisDetailedModel, AgeAnalysisModel } from '@modules/reports/models/credit-control-reports.model';
import { ReportsModuleApiSuffixModels, REPORT_COMPONENTS } from '@modules/reports/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-advanced-age-analysis',
  templateUrl: './advanced-age-analysis.component.html'
})
export class AdvancedAgeAnalysisComponent implements OnInit {

  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  tabListColumns: any;
  dataList: any;
  listapiSubscription: any;
  loggedInUserData: LoggedInUserModel;
  observableResponse: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  pageSize: any = 10;
  row: any = {}
  loading: boolean;
  status: any = [];
  selectedRows = [];
  divisionList = []
  paymentMethod = []
  branchDropdown = []
  otherParams :any
  filterData :any = {}
  ageAnalysisForm: FormGroup
  constructor(private crudService: CrudService, private rxjsService: RxjsService, private router: Router, public dialogService: DialogService,
    private activateRoute: ActivatedRoute, private formBuilder: FormBuilder, private snackbarService: SnackbarService, private store: Store<AppState>,) {
    this.primengTableConfigProperties = {
      tableCaption: 'Advanced Age Analysis',
      breadCrumbItems: [{ displayName: 'Reports', relativeRouterUrl: '' }, { displayName: 'Credit Control Reports', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Age Analysis",
            dataKey: 'faQuestionAnswerConfigId',
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableExportBtn: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableBreadCrumb: true,
            columns: [
              { field: 'division', header: 'Division', width: '120px' },
              { field: 'bdiNumber', header: 'BDI Number', width: '150px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '150px' },
              { field: 'controller', header: 'Controller', width: '150px' },
              { field: 'billName', header: 'Bill Name', width: '150px' },
              { field: 'totalOutstanding', header: 'Total Due', width: '120px' },
              { field: 'currentOutstanding', header: 'Current', width: '120px' },
              { field: 'd30OutStanding', header: '30 Days', width: '120px' },
              { field: 'd60OutStanding', header: '60 Days', width: '120px' },
              { field: 'd90OutStanding', header: '90 Days', width: '120px' },
              { field: 'd120OutStanding', header: '120 Days ', width: '120px' },
              { field: 'd120OverOutStanding', header: 'Over 120 Days', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: ReportsModuleApiSuffixModels.AGING_REPORT_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true
          },
          {
            caption: "Detailed Age Analysis",
            dataKey: 'faQuestionAnswerConfigId',
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableExportBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            enableBreadCrumb: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'division', header: 'Division', width: '120px' },
              { field: 'bdiNumber', header: 'BDI Number', width: '150px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '150px' },
              { field: 'controller', header: 'Controller', width: '150px' },
              { field: 'billName', header: 'Bill Name', width: '150px' },
              { field: 'totalOutstanding', header: 'Total Due', width: '120px' },
              { field: 'currentOutstanding', header: 'Current', width: '120px' },
              { field: 'd30OutStanding', header: '30 Days', width: '120px' },
              { field: 'd60OutStanding', header: '60 Days', width: '120px' },
              { field: 'd90OutStanding', header: '90 Days', width: '120px' },
              { field: 'd120OutStanding', header: '120 Days ', width: '120px' },
              { field: 'd120OverOutStanding', header: 'Over 120 Days', width: '150px' },
              { field: 'activeStatus', header: 'Active Status', width: '150px' },
              { field: 'bucketStatus', header: 'Bucket Status', width: '150px' },
              { field: 'bucketCount', header: 'Bucket Count', width: '150px' },
              { field: 'paymentMethod', header: 'Payment Method', width: '150px' },
              { field: 'debitDate', header: 'Debit Date', width: '150px' },
              { field: 'offSet', header: 'Offset ', width: '150px' },
              { field: 'udTotalOutstanding', header: '(UD) Total', width: '150px' },
              { field: 'udD30OutStanding', header: '(UD) 30 Days', width: '120px' },
              { field: 'udD60OutStanding', header: '(UD) 60 Days', width: '120px' },
              { field: 'udD90OutStanding', header: '(UD) 90 Days', width: '120px' },
              { field: 'udD120OutStanding', header: '(UD) 120 Days ', width: '120px' },
              { field: 'udD120OverOutStanding', header: '(UD) Over 120 Days', width: '150px' },
              { field: 'udTotalOutstandingCount', header: '(UD) Total Count', width: '150px' },
              { field: 'udCurrentOutstandingCount', header: '(UD) Current Count', width: '150px' },
              { field: 'udD30OutStandingCount', header: '(UD) 30 Days Count', width: '150px' },
              { field: 'udD60OutStandingCount', header: '(UD) 60 Days Count', width: '150px' },
              { field: 'udD90OutStandingCount', header: '(UD) 90 Days Count', width: '150px' },
              { field: 'udD120OutStandingCount', header: '(UD) 120 Days Count', width: '150px' },
              { field: 'udD120OverOutStandingCount', header: '(UD) Over 120 Days Count', width: '150px' },
              { field: 'arrearStatus', header: 'Arrear Status', width: '150px' },
              { field: 'consecutive90Days', header: 'Consecutive (Current to 90 Days)', width: '150px' },
              { field: 'utTotalOutstanding', header: '(UT) Total Due', width: '150px' },
              { field: 'utCurrentOutstanding', header: '(UT) Current', width: '150px' },
              { field: 'utD30OutStanding', header: '(UT) 30 Days', width: '150px' },
              { field: 'utD60OutStanding', header: '(UT) 60 Days', width: '150px' },
              { field: 'utD90OutStanding', header: '(UT) 90 Days', width: '150px' },
              { field: 'utD120OutStanding', header: '(UT) 120 Days', width: '150px' },
              { field: 'utD120OverOutStanding', header: '(UT) Over 120 Days', width: '150px' },
              { field: 'latestPaidDate', header: 'Latest Paid Date', width: '150px' },
              { field: 'latestUnPaidDate', header: 'Latest Unpaid Date', width: '150px' },
              { field: 'risk', header: 'Risk', width: '150px' },
              { field: 'latestDealer', header: 'Latest Dealer', width: '150px' },
              { field: 'dealerName', header: 'Dealer Name', width: '150px' },
              { field: 'latestDealerWarrantyEndDate', header: 'Latest Dealer Warrantry End Date', width: '150px' },
              { field: 'controller', header: 'Controller Name', width: '150px' },
              { field: 'controllerNo', header: 'Controller No', width: '150px' },
              { field: 'email', header: 'Controller Email', width: '150px' },
              { field: 'smsDebtor', header: 'SMS Debtor', width: '150px' },
              { field: 'cellNo', header: 'Cell No', width: '150px' },
              { field: 'phoneNo1', header: 'Phone No 1', width: '150px' },
              { field: 'phoneNo2', header: 'Phone No 2', width: '150px' },
              { field: 'faxNo', header: 'Fax No', width: '150px' },
              { field: 'branch', header: 'Branch', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: ReportsModuleApiSuffixModels.AGING_REPORT_DETAILED_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true
          }
        ]
      }
    };
    this.selectedTabIndex = this.activateRoute.snapshot?.queryParams?.tab ? this.activateRoute.snapshot?.queryParams?.tab : 0;
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit(): void {

    this.rxjsService.setGlobalLoaderProperty(false);
    this.combineLatestNgrxStoreData();
    this.getAgeAdvancedListData();
    this.getDivisions()
    this.getBranchDropdown()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.selectedTabIndex == 0) {
      this.createAgeAnalysisFilterForm()
    } else {
      this.createDetailedAgeAnalysisFilterForm()
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][REPORT_COMPONENTS.AGE_ANALYSIS_REPORTS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.selectedTabIndex =  +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex']
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDivisions() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.divisionList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    })

  }
  getBranchDropdown(): void {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SharedModuleApiSuffixModels.UX_BRANCHES)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.branchDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  tabClick(e) {
    if (e) {
      this.router.navigate(['./'], { relativeTo: this.activateRoute, queryParams: { tab: e?.index } });
      this.selectedTabIndex = e?.index;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
      this.getAgeAdvancedListData();
      if (e.index == 0) {
        this.createAgeAnalysisFilterForm()
      }
      if (e.index == 1) {
        this.createDetailedAgeAnalysisFilterForm()
      }
    }
  }
  createAgeAnalysisFilterForm(data?: AgeAnalysisModel) {
    let _model = new AgeAnalysisModel(data);
    this.ageAnalysisForm = this.formBuilder.group({});
    Object.keys(_model).forEach((key) => {
      this.ageAnalysisForm.addControl(key, new FormControl(_model[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createDetailedAgeAnalysisFilterForm(data?: AgeAnalysisDetailedModel) {
    let _model = new AgeAnalysisDetailedModel(data);
    this.ageAnalysisForm = this.formBuilder.group({});
    Object.keys(_model).forEach((key) => {
      this.ageAnalysisForm.addControl(key, new FormControl(_model[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);

  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        unknownVar = {...this.filterData, ...unknownVar}
        this.getAgeAdvancedListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.FILTER:
        break;

      case CrudType.EXPORT:
        this.exportAPI()
        break;

    }
  }


  getAgeAdvancedListData(pageIndex?: string | any, pageSize?: string | any, otherParams?: object) {
    this.loading = true;
    let reportModuleApiSuffixModels: ReportsModuleApiSuffixModels;
    reportModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    // if (this.listapiSubscription && !this.listapiSubscription?.closed) {
    //   this.listapiSubscription?.unsubscribe();
    // }
    pageIndex = pageIndex ? pageIndex : 0;
    pageSize = pageSize ? pageSize : this.pageSize;


    this.otherParams = otherParams
    this.listapiSubscription = this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      reportModuleApiSuffixModels,
      null,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe(data => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data?.isSuccess && data?.statusCode == 200) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        } else {
          this.observableResponse = null;
          this.dataList = this.observableResponse
          this.totalRecords = 0;
        }

      })

  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onSubmitAgeAnalysis() {
    let _value = this.ageAnalysisForm.value;
    let req = {};
    for (const key in _value) {
      if (Object.prototype.hasOwnProperty.call(_value, key)) {
        if (_value[key]) {
          req[key] = _value[key];
        }
      }
    }
    this.filterData = req;
    this.getAgeAdvancedListData(0, 10, req);

  }


  ngOnDestroy() {
    if (this.listapiSubscription) {
      this.listapiSubscription.unsubscribe();
    }
  }

  exportAPI() {
    let api: ReportsModuleApiSuffixModels = this.selectedTabIndex == 0 ? ReportsModuleApiSuffixModels.AGING_REPORT_EXPORT : ReportsModuleApiSuffixModels.AGING_REPORT_DETAILED_EXPORT;
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, api, null,
      false, prepareRequiredHttpParams(this.otherParams)).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.exportExcel(response.resources);
        this.rxjsService.setGlobalLoaderProperty(false);

      }
    })

  }


  exportExcel(data) {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(data);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

}
