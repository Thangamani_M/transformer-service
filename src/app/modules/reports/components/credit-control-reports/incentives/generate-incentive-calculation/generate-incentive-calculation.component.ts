import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-generate-incentive-calculation',
  templateUrl: './generate-incentive-calculation.component.html',
})
export class GenerateIncentiveCalculationComponent {
  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  loggedInUserData: LoggedInUserModel;
  row: any = {}
  loading: boolean;
  status: any = [];
  divisionList = [];
  regionList = [];
  incentiveGenearteForm: FormGroup;
  regionIds = [];
  startTodayDate = new Date();
  loggedUser;
  constructor(private momentService: MomentService, private crudService: CrudService, private rxjsService: RxjsService, private router: Router,
    private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: 'Generate Incentive Calculation',
      breadCrumbItems: [{ displayName: 'Reports', relativeRouterUrl: '' }, { displayName: 'Credit Control Reports', relativeRouterUrl: '' },
      { displayName: 'Incentive Calculation Reports', relativeRouterUrl: '/reports/credit-control-reports/incentives-calculation-reports' }, { displayName: 'Generate Incentive Calculation', relativeRouterUrl: '' }
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Generate Incentive Calculation",
            dataKey: 'faQuestionAnswerConfigId',
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableExportBtn: false,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableBreadCrumb: true,
            columns: [],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    };
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }
  ngOnInit() {
    this.createForm();
    this.getRegions();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createForm() {
    this.incentiveGenearteForm = new FormGroup({
      'reportDate': new FormControl(null),
      'regionId': new FormControl(null),
      'divisionId': new FormControl(null)
    });
    this.onValueChanges();
    this.incentiveGenearteForm = setRequiredValidator(this.incentiveGenearteForm, ['reportDate', 'regionId', 'divisionId']);
  }
  onValueChanges() {
    this.incentiveGenearteForm.get("regionId").valueChanges.subscribe((value: any) => {
      this.regionIds = [];
      value.forEach(element => {
        this.regionIds.push(element.id);
      });
      if (this.regionIds && this.regionIds.length > 0)
        this.getDivisionList();
    });
  }
  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.regionList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }
  getDivisionList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_GENERATE_REPORT_UX_REGION_DEIVISON,
      prepareGetRequestHttpParams(null, null,
        { RegionIds: this.regionIds })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.divisionList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }
  onSubmit() {
    if (this.incentiveGenearteForm.invalid) {
      this.incentiveGenearteForm.markAllAsTouched();
      return;
    }
    let formValue = this.incentiveGenearteForm.getRawValue();
    formValue.reportDate = (formValue.reportDate && formValue.reportDate != null) ? this.momentService.toMoment(formValue.reportDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;

    let incentiveCalculationRegionDivisions = [];
    formValue.regionId.forEach(element => {
      let filter = formValue.divisionId.filter(x => x.regionId == element.id);
      let divisionIds = [];
      filter.forEach(element => {
        divisionIds.push({ divisionId: element.divisionId });
      });
      let param = { regionId: element.id, divisionIds: divisionIds };
      incentiveCalculationRegionDivisions.push(param);
    });
    let finaObj = {
      runDate: formValue.reportDate,
      incentiveCalculationRegionDivisions: incentiveCalculationRegionDivisions,
      createdUserId: this.loggedUser.userId
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_GENERATE_REPORT, finaObj, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(['/reports/credit-control-reports/incentives-calculation-reports']);
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
}