import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { ReportsModuleApiSuffixModels, REPORT_COMPONENTS } from '@modules/reports/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-incentive-reports',
  templateUrl: './incentive-reports.component.html',
  styleUrls: ['./incentive-reports.component.scss']
})
export class IncentiveReportsComponent implements OnInit {


  constructor(private crudService : CrudService, private activatedRoute : ActivatedRoute,
    private rxjsService : RxjsService,  private store: Store<AppState>, ) {
    this.activatedRoute.queryParams.subscribe(response=>{
      this.incentiveCalculationReportId = response['id'];
    })
  }
  incentiveCalculationReportId = ""
  selectedTabIndex = 0;
  isReductionDisabled = true;
  isAvgReductionDisabled = true;
  primengTableConfigProperties: any = {
    tableCaption: "Incentives Calculation",
    breadCrumbItems: [{ displayName: 'Reports', relativeRouterUrl: '' }, { displayName: 'Credit Control Reports', relativeRouterUrl: '' },
    { displayName: 'Incentives', relativeRouterUrl: '/reports/credit-control-reports/incentives-calculation-reports' },{ displayName: 'Incentives Calculation', relativeRouterUrl: '' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Incentives Calculation',
          dataKey: 'bulkSMSConfigId',
          captionFontSize: '21px',
          enableBreadCrumb: true,
          enableAction:true,
          enableExportBtn:true,
        }
      ]
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][REPORT_COMPONENTS.INCENTIVE_CALCULATION]
      let report = permission.find(item => item.menuName == "Reports");
      let find = report['subMenu'].find(item => item.menuName == "REDUCTION")
      let find1 = report['subMenu'].find(item => item.menuName == "AVERAGE REDUCTION")
      if(find){
        this.isReductionDisabled = false
      }
     if(find1){false
      this.isAvgReductionDisabled = false
     };
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.EXPORT:
      this.downloadReport()
        break;


      default:
    }
  }


  downloadReport(){
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, ReportsModuleApiSuffixModels.INCENTIVE_CALCULATION_EXPORT, undefined, false, prepareRequiredHttpParams({incentiveCalculationReportId:this.incentiveCalculationReportId})).subscribe(response=>{
        this.rxjsService.setGlobalLoaderProperty(false);
      if(response.isSuccess && response.statusCode ==200 && response.resources){
        window.open(response.resources);
      }
    })
  }

  // ?IncentiveCalculationReportId=031797a0-2d5a-4406-8a47-afe76cbfabbe

}
