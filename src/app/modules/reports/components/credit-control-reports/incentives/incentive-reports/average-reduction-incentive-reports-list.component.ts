import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { ReportsModuleApiSuffixModels } from '@modules/reports/shared';

@Component({
  selector: 'app-average-reduction-incentive-reports-list',
  templateUrl: './average-reduction-incentive-reports-list.component.html',
  styleUrls: ['./incentive-reports.component.scss']
})
export class IncentiveAverageReductionReportsListComponent implements OnInit {

  pageSize: number = 10;
  constructor(private crudService: CrudService, private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute, private router: Router) {

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.incentiveCalculationReportId = params['params']['id']
    });

  }

  incentiveCalculationReportId = '' //"031797a0-2d5a-4406-8a47-afe76cbfabbe"; // due to api issue
  dataList: any = [];
  listapiSubscription: any;
  observableResponse: any;
  loading = false;
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  selectedTabIndex = 0;
  primengTableConfigProperties: any = {
    tableCaption: "Opening",
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Average Reduction',
          dataKey: 'incentiveCalculationReductionOpeningId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'incentiveRank', header: 'Rank', width: '150px' },
            { field: 'reportingEmployeeName', header: 'Credit Controller', width: '150px' },
            { field: 'regionName', header: 'Region', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'averageReduction', header: 'Average Reduction', width: '200px' },
          ],
          parentKey: 'incentiveCalculationAverageReductionDTO',
          childKey: 'incentiveCalculationAverageReductionDetailDTO',
          childColumns: [
            { field: 'averageReductionRank', header: 'Rank', width: '150px' },
            { field: 'creditControllerName', header: 'Credit Controller', width: '150px' },
            { field: 'regionName', header: 'Region', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'averageReduction', header: 'Average Reduction', width: '150px' },
            { field: 'reInstated', header: 'Reinstated', width: '150px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_CALCULATION_AVERAGE_REDUCTION_,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }
      ]
    }
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    let reportsModuleApiSuffixModels: ReportsModuleApiSuffixModels;
    reportsModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    let parentKey = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].parentKey;

    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      reportsModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          incentiveCalculationReportId: this.incentiveCalculationReportId
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        if (parentKey) {
          this.observableResponse = data.resources[parentKey];
        } else {
          this.observableResponse = data.resources;
        }
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount?data.totalCount:this.dataList.length;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.getRequiredListData(row["pageIndex"], row["pageSize"])

  }

  onTabChange(event) {
    this.selectedTabIndex = event.index
    this.router.navigate(['/reports/credit-control-reports/incentives-reports'], { queryParams: { id: this.incentiveCalculationReportId, tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

}
