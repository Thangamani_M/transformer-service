import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { ReportsModuleApiSuffixModels, REPORT_COMPONENTS } from '@modules/reports/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-incentive-reports-list',
  templateUrl: './reduction-incentive-reports-list.component.html',
})
export class IncentiveReportsListComponent implements OnInit {

  pageSize: number = 10;
  constructor(private crudService: CrudService, private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,) {

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.incentiveCalculationReportId = params['params']['id']
      this.getRequiredListData()
    });

  }

  incentiveCalculationReportId = '' //"031797a0-2d5a-4406-8a47-afe76cbfabbe"; // due to api issue
  dataList: any = [];
  listapiSubscription: any;
  observableResponse: any;
  loading = false;
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  selectedTabIndex = 0;
  primengTableConfigProperties: any = {
    tableCaption: "Opening",
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Opening',
          dataKey: 'incentiveCalculationReductionOpeningId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'reportingEmployeeName', header: 'Credit Controller', width: '150px' },
            { field: 'creditControllerCount', header: 'Count', width: '150px' },
            { field: 'currentPercentage', header: 'Current', width: '150px' },
            { field: 'currentExclPercentage', header: 'Current(Excl)', width: '200px' },
            { field: 'amount30Days', header: '30 Days', width: '200px' },
            { field: 'amount60Days', header: '60 Days', width: '200px' },
            { field: 'amount90Days', header: '90 Days', width: '200px' },
            { field: 'amount120Days', header: '120 Days', width: '200px' },
            { field: 'amount120PlusDays', header: '120+ Days', width: '200px' },
          ],
          parentKey: 'incentiveCalculationReductionOpeningDTO',
          childKey: 'incentiveCalculationReductionOpeningDetailDTO',
          childColumns: [
            { field: 'creditControllerName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'openingCurrent', header: 'Percentage on Current', width: '150px' },
            { field: 'percentageOnCurrentExcl', header: 'Percentage on Current(Excl)', width: '150px' },
            { field: 'opening30Days', header: 'Percentage on 30 Days', width: '150px' },
            { field: 'opening60Days', header: 'Percentage on 60 Days', width: '150px' },
            { field: 'opening90Days', header: 'Percentage on 90 Days', width: '150px' },
            { field: 'opening120Days', header: 'Percentage on 120 Days', width: '150px' },
            { field: 'openingOver120Days', header: 'Percentage on 120+ Days', width: '150px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_REDUCTION_OPENING,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        },
        {
          caption: 'Closing',
          dataKey: 'incentiveCalculationReductionClosingId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'creditControllerName', header: 'Credit Controller', width: '150px' },
            { field: 'creditControllerCount', header: 'Count', width: '150px' },
            { field: 'currentPercentage', header: 'Current', width: '150px' },
            { field: 'currentExclPercentage', header: 'Current(Excl)', width: '200px' },
            { field: 'amount30Days', header: '30 Days', width: '200px' },
            { field: 'amount60Days', header: '60 Days', width: '200px' },
            { field: 'amount90Days', header: '90 Days', width: '200px' },
            { field: 'amount120Days', header: '120 Days', width: '200px' },
            { field: 'amount120PlusDays', header: '120+ Days', width: '200px' },
          ],
          parentKey: '',
          childKey: 'detail',
          childColumns: [
            { field: 'creditControllerName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'percentageOnCurrent', header: 'Percentage on Current', width: '150px' },
            { field: 'percentageOnCurrentExcl', header: 'Percentage on Current(Excl)', width: '150px' },
            { field: 'amount30Days', header: 'Percentage on 30 Days', width: '150px' },
            { field: 'amount60Days', header: 'Percentage on 60 Days', width: '150px' },
            { field: 'amount90Days', header: 'Percentage on 90 Days', width: '150px' },
            { field: 'amount120Days', header: 'Percentage on 120 Days', width: '150px' },
            { field: 'amount120PlusDays', header: 'Percentage on 120+ Days', width: '150px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_REDUCTION_CLOSING,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        },
        {
          caption: 'Reduction Percentage',
          dataKey: 'creditControllerId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'creditControllerName', header: 'Credit Controller', width: '150px' },
            { field: 'creditControllerCount', header: 'Count', width: '150px' },
            { field: 'currentPercentage', header: 'Current', width: '150px' },
            { field: 'currentExclPercentage', header: 'Current(Excl)', width: '200px' },
            { field: 'amount30Days', header: '30 Days', width: '200px' },
            { field: 'amount60Days', header: '60 Days', width: '200px' },
            { field: 'amount90Days', header: '90 Days', width: '200px' },
            { field: 'amount120Days', header: '120 Days', width: '200px' },
            { field: 'amount120PlusDays', header: '120+ Days', width: '200px' },
          ],
          parentKey: 'incentiveCalculationReductionPercentage',
          childKey: 'incentiveCalculationReductionPercentageDetails',
          childColumns: [
            { field: 'creditControllerName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'percentageOnCurrent', header: 'Percentage on Current', width: '150px' },
            { field: 'percentageOnCurrentExcl', header: 'Percentage on Current(Excl)', width: '150px' },
            { field: 'percentage30Days', header: 'Percentage on 30 Days', width: '150px' },
            { field: 'percentage60Days', header: 'Percentage on 60 Days', width: '150px' },
            { field: 'percentage90Days', header: 'Percentage on 90 Days', width: '150px' },
            { field: 'percentage120Days', header: 'Percentage on 120 Days', width: '150px' },
            { field: 'percentageOver120Days', header: 'Percentage on 120+ Days', width: '150px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_REDUCTION_PERCENTAGE,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        },
        {
          caption: 'Qualified Level 1 Target Percentage',
          dataKey: 'incentiveCalculationReductionQualifiedLevel1PercentageId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          columns: [
            { field: 'creditControllerName', header: 'Credit Controller', width: '150px' },
            { field: 'creditControllerCount', header: 'Count', width: '150px' },
            { field: 'percentage15On30Days', header: '15% on 30 Days', width: '200px' },
            { field: 'percentage17On60Days', header: '17% on 60 Days', width: '200px' },
            { field: 'percentage20On90Days', header: '20% on 90 Days', width: '200px' },
            { field: 'percentage25On120Days', header: '25% on 120 Days', width: '200px' },
            { field: 'percentage30On120PlusDays', header: '30% on 120+ Days', width: '200px' },
          ],
          parentKey: 'incentiveReductionQualifiedLevel1Percentages',
          childKey: 'incentiveReductionQualifiedLevel1PercentageDetails',
          childColumns: [
            { field: 'creditControllerName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'level1Target15PercentageOn30Days', header: 'Level 1 Target(15%) on 30 Days', width: '150px' },
            { field: 'level1Target17PercentageOn60Days', header: 'Level 1 Target(17%) on 60 Days', width: '150px' },
            { field: 'level1Target20PercentageOn90Days', header: 'Level 1 Target(20%) on 90 Days', width: '150px' },
            { field: 'level1Target25PercentageOn120Days', header: 'Level 1 Target(25%) on 120 Days', width: '150px' },
            { field: 'level1Target30PercentageOnOver120Days', header: 'Level 1 Target(30%) on 120+ Days', width: '150px' },
          ],
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_REDUCTION_QUALIFIED_LEVEL1,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        },
        {
          caption: 'Qualified Level 2 Target Percentage',
          dataKey: 'incentiveCalculationReductionQualifiedLevel2PercentageId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          columns: [
            { field: 'creditControllerName', header: 'Credit Controller', width: '150px' },
            { field: 'creditControllerCount', header: 'Count', width: '150px' },
            { field: 'percentage70OnCurrent', header: '70% on Current', width: '200px' },
            { field: 'percentage20On30Days', header: '20% on 30 Days', width: '200px' },
            { field: 'percentage22On60Days', header: '22% on 60 Days', width: '200px' },
            { field: 'percentage25On90Days', header: '25% on 90 Days', width: '200px' },
            { field: 'percentage30On120Days', header: '30% on 120 Days', width: '200px' },
            { field: 'percentage35On120PlusDays', header: '35% on 120+ Days', width: '200px' },
          ],
          parentKey: 'incentiveReductionQualifiedLevel2Percentages',
          childKey: 'incentiveReductionQualifiedLevel2PercentageDetails',
          childColumns: [
            { field: 'creditControllerName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'level2Target70PercentageOnCurrent', header: 'Level 2 Target(70%) on Current', width: '150px' },
            { field: 'level2Target20PercentageOn30Days', header: 'Level 2 Target(20%) on 30 Days', width: '150px' },
            { field: 'level2Target22PercentageOn60Days', header: 'Level 2 Target(22%) on 60 Days', width: '150px' },
            { field: 'level2Target25PercentageOn90Days', header: 'Level 2 Target(25%) on 90 Days', width: '150px' },
            { field: 'level2Target30PercentageOn120Days', header: 'Level 2 Target(30%) on 120 Days', width: '150px' },
            { field: 'level2Target35PercentageOnOver120Days', header: 'Level 2 Target(35%) on 120+ Days', width: '150px' },
          ],
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_REDUCTION_QUALIFIED_LEVEL2,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        },
        {
          caption: 'Buckets',
          dataKey: 'incentiveCalculationReductionQualifiedLevel2PercentageId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          columns: [
            { field: 'displayName', header: 'Credit Controller', width: '150px' },
            { field: 'currentCount', header: 'Count', width: '100px' },
            { field: 'reinstament', header: 'Reinstatements', width: '200px' },
            { field: 'bucketCount', header: 'Buckets Achieved', width: '200px' },
            { field: 'bucketTarget', header: 'Buckets Target', width: '200px' },
          ],
          parentKey: '',
          childKey: 'childDTO',
          childColumns: [
            { field: 'displayName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'reinstament', header: 'Reinstatements', width: '150px' },
            { field: 'bucketCount', header: 'Buckets Achieved', width: '200px' },
            { field: 'bucketTarget', header: 'Buckets Target', width: '200px' },

          ],
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_REDUCTION_BUCKETS,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        },
        {
          caption: 'Average Reduction Excl Current',
          dataKey: 'incentiveCalculationReductionAverageExclCurrentId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          columns: [
            { field: 'creditControllerName', header: 'Credit Controller', width: '150px' },
            { field: 'creditControllerCount', header: 'Count', width: '100px' },
            { field: 'achieved', header: 'Average Reduction Excl Current Achieved', width: '200px' },
            { field: 'taget', header: 'Average Reduction Excl Current Target', width: '200px' },
          ],
          parentKey: 'incentiveCalculationReductionAverageExclCurrent',
          childKey: 'incentiveCalculationReductionAverageExclCurrentDetails',
          childColumns: [
            { field: 'creditControllerName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'averageReductionExclCurrentAchieved', header: 'Average Reduction Excl Current Achieved', width: '200px' },
            { field: 'averageReductionExclCurrentTarget', header: 'Average Reduction Excl Current Target', width: '200px' },

          ],
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_REDUCTION_PERCENTAGE_AVERAGE_EXCL,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        },
        {
          caption: 'Month to Date Productivity',
          dataKey: 'incentiveCalculationReductionMonthToDateProductivityId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          columns: [
            { field: 'creditControllerName', header: 'Credit Controller', width: '150px' },
            { field: 'creditControllerCount', header: 'Count', width: '100px' },
            { field: 'achieved', header: 'Month to Date Productivity Achieved', width: '200px' },
            { field: 'taget', header: 'Month to Date Productivity Target', width: '200px' },
          ],
          parentKey: 'incentiveReductionMonthToDateProductivities',
          childKey: 'incentiveReductionMonthToDateProductivityDetails',
          childColumns: [
            { field: 'creditControllerName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'monthToDateProductivityAchieved', header: 'Month to Date Productivity Achieved', width: '200px' },
            { field: 'monthToDateProductivityMinTarget', header: 'Month to Date Productivity Minimum Target', width: '200px' },

          ],
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_REDUCTION_MONTH_TO_DATE_PRODUCTIVITY,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        },
        {
          caption: 'Incentives',
          dataKey: 'incentiveCalculationReductionIncentiveId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          shouldShowFilterActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false,
          columns: [
            { field: 'creditControllerName', header: 'Credit Controller', width: '150px' },
            { field: 'creditControllerCount', header: 'Count', width: '100px' },
            { field: 'initialIncentive', header: 'Initial Incentive', width: '100px' },
            { field: 'lessIncentiveOnAvgReduction', header: 'Less Incentive on Avg Reduction', width: '150px' },
            { field: 'currentTopupIncentive', header: 'Current top up Incentive', width: '150px' },
            { field: 'lessIncentiveOnProductivity', header: 'Less Incentive on Productivity', width: '150px' },
            { field: 'addTopupIncentive', header: 'Add to top up Incentives', width: '150px' },
            { field: 'finalIncentive', header: 'Final Incentive', width: '100px' },
            { field: 'incentiveRank', header: 'Rank', width: '100px' },
          ],
          parentKey: 'incentiveCalculationReductionIncentivesDTO',
          childKey: 'incentiveCalculationReductionIncentivesDetailDTO',
          childColumns: [
            { field: 'creditControllerName', header: 'Controller Name', width: '150px' },
            { field: 'incentiveReductionTypeName', header: 'Type', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level', width: '150px' },
            { field: 'initialIncentive', header: 'Initial Incentive', width: '100px' },
            { field: 'lessIncentiveOnAvgReduction', header: 'Less Incentive on Avg Reduction', width: '150px' },
            { field: 'currentTopupIncentive', header: 'Current top up Incentive', width: '150px' },
            { field: 'lessIncentiveOnProductivity', header: 'Less Incentive on Productivity', width: '150px' },
            { field: 'addTopupIncentive', header: 'Add to top up Incentives', width: '150px' },
            { field: 'finalIncentive', header: 'Final Incentive', width: '100px' },
            { field: 'incentiveRank', header: 'Rank', width: '100px' },
          ],
          apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_CALCULATION_INCENTIVES,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        }
      ]
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    // this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][REPORT_COMPONENTS.INCENTIVE_CALCULATION]
      let report = permission.find(item => item.menuName == "Reports");
      let rReport = report['subMenu'].find(item => item.menuName == "REDUCTION");
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, rReport['subMenu']);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.loading = true;
    let reportsModuleApiSuffixModels: ReportsModuleApiSuffixModels;
    reportsModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    let parentKey = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].parentKey;

    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      reportsModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          incentiveCalculationReportId: this.incentiveCalculationReportId
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        if (parentKey) {
          this.observableResponse = data.resources[parentKey];
          this.totalRecords = data.totalCount ? data.totalCount : this.observableResponse.length;

        } else {
          this.observableResponse = data.resources;
          this.totalRecords = data.totalCount ? data.totalCount : this.observableResponse.length;

        }
        this.dataList = this.observableResponse;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.getRequiredListData(row["pageIndex"], row["pageSize"])

  }

  onTabChange(event) {
    this.selectedTabIndex = event.index
    this.router.navigate(['/reports/credit-control-reports/incentives-reports'], { queryParams: { id: this.incentiveCalculationReportId, tab: this.selectedTabIndex } })
    // this.getRequiredListData()
  }

}
