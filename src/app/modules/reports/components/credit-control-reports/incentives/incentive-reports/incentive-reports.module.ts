import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { IncentiveAverageReductionReportsListComponent } from './average-reduction-incentive-reports-list.component';
import { IncentiveReportsRoutingModule } from './incentive-reports-routing.module';
import { IncentiveReportsComponent } from './incentive-reports.component';
import { IncentiveReportsListComponent } from './reduction-incentive-reports-list.component';


@NgModule({
  declarations: [IncentiveReportsComponent, IncentiveReportsListComponent,IncentiveAverageReductionReportsListComponent],
  imports: [
    CommonModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    IncentiveReportsRoutingModule
  ],
  exports: [IncentiveReportsListComponent,IncentiveAverageReductionReportsListComponent]
})
export class IncentiveReportsModule { }
