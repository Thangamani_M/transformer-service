import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IncentiveReportsComponent } from './incentive-reports.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: IncentiveReportsComponent, canActivate: [AuthGuard], data: { title: 'Incentive Reports' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class IncentiveReportsRoutingModule { }
