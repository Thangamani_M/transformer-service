import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { loggedInUserData } from "@modules/others";
import { ReportsModuleApiSuffixModels, REPORT_COMPONENTS } from "@modules/reports/shared";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: 'app-incentives-calculation-reports',
  templateUrl: './incentives-calculation-reports.component.html',
})
export class IncentivesCalculationReportsComponent implements OnInit {
  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  tabListColumns: any;
  dataList: any;
  listapiSubscription: any;
  loggedInUserData: LoggedInUserModel;
  observableResponse: any;
  startTodayDate = new Date();
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  first: any = 0;
  pageSize: any = 10;
  row: any = {}
  loading: boolean;
  status: any = [];
  selectedRows = [];
  divisionList = []
  paymentMethod = []
  branchDropdown = []
  incentiveForm: FormGroup
  constructor(private momentService: MomentService, private crudService: CrudService, private rxjsService: RxjsService, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.primengTableConfigProperties = {
      tableCaption: 'Incentives Calculation Reports',
      breadCrumbItems: [{ displayName: 'Reports', relativeRouterUrl: '' }, { displayName: 'Credit Control Reports', relativeRouterUrl: '' },
      { displayName: 'Incentives Calculation Reports', relativeRouterUrl: '' }
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Incentives Calculation Reports",
            dataKey: 'faQuestionAnswerConfigId',
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableExportBtn: false,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableBreadCrumb: true,
            columns: [
              { field: 'reportName', header: 'Report Name', width: '120px' },
              { field: 'reportDate', header: 'Date & Time', width: '150px' },
              { field: 'status', header: 'Status', width: '150px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: ReportsModuleApiSuffixModels.INCENTIVE_GENERATE_REPORT,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    };

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.createForm();
    this.search(0, 10, null);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][REPORT_COMPONENTS.INCENTIVE_CALCULATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createForm() {
    this.incentiveForm = new FormGroup({
      'reportDate': new FormControl(null),
    });
  }
  search(pageIndex?: any, pageSize?: any, val?: any) {
    let formValue = this.incentiveForm.getRawValue();
    formValue.reportDate = (formValue.reportDate && formValue.reportDate != null) ? this.momentService.toMoment(formValue.reportDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    let params = null; let api;
    params = { ...val }
    if (formValue.reportDate) {
      params = { ...val, ...{ reportDate: formValue.reportDate } }
      api = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, ReportsModuleApiSuffixModels.INCENTIVE_GENERATE_REPORT, null, null, prepareGetRequestHttpParams(pageIndex, pageSize, params));
    }
    else {
      api = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, ReportsModuleApiSuffixModels.INCENTIVE_GENERATE_REPORT, null, null, prepareGetRequestHttpParams(pageIndex, pageSize, params));
    }
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.dataList = res.resources;
        this.totalRecords = res.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/reports/credit-control-reports/generate-incentive-calculation']);
        break;

      case CrudType.VIEW:
        this.router.navigate(['/reports/credit-control-reports/incentives-reports'], { queryParams: { id: row['incentiveCalculationReportId'] } });
        break;
      case CrudType.GET:
        this.search(row["pageIndex"], row["pageSize"], unknownVar);
        break;
    }
  }

}
