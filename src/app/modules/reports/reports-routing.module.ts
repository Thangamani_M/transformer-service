import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'credit-control-reports', loadChildren: () => import('./components/credit-control-reports/credit-control-reports.module').then(m => m.CreditControlReportsModule) },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ReportsRoutingModule { }
