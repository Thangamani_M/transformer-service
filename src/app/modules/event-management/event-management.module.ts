import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { GMapModule } from 'primeng/gmap';
import { LogawayConfirmPopupComponent } from './components/configuration/stack-winelands/popup-models/logaway-confirm-popup/logaway-confirm-popup.component';
import { DispatcherMapReusablePopupComponent } from './components/dispatcher-map-view/dispatcher-map-view-reusable-popup.component';
import { DispatcherMapViewComponent } from './components/dispatcher-map-view/dispatcher-map-view.component';
import { EventManagementRoutingModule } from './event-management-routing.module';
import { EventsMgmtSharedModule } from './shared/events-mgmt-shared.module';
@NgModule({
  declarations: [ LogawayConfirmPopupComponent,DispatcherMapViewComponent,DispatcherMapReusablePopupComponent],
  imports: [
    CommonModule,
    EventManagementRoutingModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    EventsMgmtSharedModule,
    GMapModule
  ],
  entryComponents: [LogawayConfirmPopupComponent,DispatcherMapReusablePopupComponent]
})
export class EventManagementModule { }
