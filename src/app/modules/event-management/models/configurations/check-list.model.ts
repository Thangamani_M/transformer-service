class CheckListModel {
    constructor(checkListModel?: CheckListModel) {
        this.vehicleCheckListId = checkListModel ? checkListModel.vehicleCheckListId == undefined ? '' : checkListModel.vehicleCheckListId : '';
        this.checkAfterReloggingPeriod = checkListModel ? checkListModel.checkAfterReloggingPeriod == undefined ? null : checkListModel.checkAfterReloggingPeriod : null;
        this.modifiedUserId = checkListModel ? checkListModel.modifiedUserId == undefined ? "" : checkListModel.modifiedUserId : "";
        this.checkListItemsList = checkListModel ? checkListModel.checkListItemsList == undefined ? [] : checkListModel.checkListItemsList : [];
        this.createdUserId = checkListModel ? checkListModel.createdUserId == undefined ? null : checkListModel.createdUserId : null;
        this.isActive = checkListModel ? checkListModel.isActive == undefined ? true : checkListModel.isActive : true;
        this.vehicleCheckListName = checkListModel ? checkListModel.vehicleCheckListName == undefined ? "" : checkListModel.vehicleCheckListName : "";
        this.description = checkListModel ? checkListModel.description == undefined ? "" : checkListModel.description : "";
        this.checkListDivisionsList = checkListModel ? checkListModel.checkListDivisionsList == undefined ? null : checkListModel.checkListDivisionsList : null;

    }
    vehicleCheckListId?: string;
    checkAfterReloggingPeriod:number;
    modifiedUserId: string;
    checkListItemsList: CheckListItemsListModel[]; 
    createdUserId: string;
    isActive: boolean;
    vehicleCheckListName:string;
    description:string;
    checkListDivisionsList:CheckListDivisionModel[];
    isEnabled:boolean;
}

  

class CheckListItemsListModel {
    constructor(checkListItemListModel?: CheckListItemsListModel) {
        this.checlListItemId = checkListItemListModel ? checkListItemListModel.checlListItemId == undefined ? '' : checkListItemListModel.checlListItemId : '';
        this.vehicleCheckListItemName = checkListItemListModel ? checkListItemListModel.vehicleCheckListItemName == undefined ? null: checkListItemListModel.vehicleCheckListItemName : null;
        this.checkListOptionId = checkListItemListModel ? checkListItemListModel.checkListOptionId == undefined ? null : checkListItemListModel.checkListOptionId : null;
        this.isAlertSupervisor = checkListItemListModel ? checkListItemListModel.isAlertSupervisor == undefined ? true : checkListItemListModel.isAlertSupervisor : true;
    }
    checlListItemId?:string;
    vehicleCheckListItemName:string;
    checkListOptionId: number;
    isAlertSupervisor: boolean

}


class CheckListDivisionModel {
     divisionId:string
}

export { CheckListModel,CheckListItemsListModel}
