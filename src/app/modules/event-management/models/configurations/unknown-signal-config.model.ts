class UnknownSignalConfigModel {
    constructor(unknownSignalConfigModel?: UnknownSignalConfigModel) {
        this.alarm = unknownSignalConfigModel ? unknownSignalConfigModel.alarm == undefined ? '' : unknownSignalConfigModel.alarm : '';
        this.alarmDescription = unknownSignalConfigModel ? unknownSignalConfigModel.alarmDescription == undefined ? '' : unknownSignalConfigModel.alarmDescription : '';
        this.unknownSignalConfigId = unknownSignalConfigModel ? unknownSignalConfigModel.unknownSignalConfigId == undefined ? null : unknownSignalConfigModel.unknownSignalConfigId : null;
        this.frequency = unknownSignalConfigModel ? unknownSignalConfigModel.frequency == undefined ? null : unknownSignalConfigModel.frequency : null;
        this.minimumSignalCountinFrequency = unknownSignalConfigModel ? unknownSignalConfigModel.minimumSignalCountinFrequency == undefined ? null : unknownSignalConfigModel.minimumSignalCountinFrequency : null;
        this.duration = unknownSignalConfigModel ? unknownSignalConfigModel.duration == undefined ? null : unknownSignalConfigModel.duration : null;
        this.ModifiedUserId = unknownSignalConfigModel ? unknownSignalConfigModel.ModifiedUserId == undefined ? '' : unknownSignalConfigModel.ModifiedUserId : '';
        this.alarmId = unknownSignalConfigModel ? unknownSignalConfigModel.alarmId == undefined ? null : unknownSignalConfigModel.alarmId : null;
    }
    alarm:string;
    alarmDescription:string;
    unknownSignalConfigId:number;
    frequency:number;
    minimumSignalCountinFrequency:number;
    duration:number;
    ModifiedUserId:string
    alarmId:number
    
}
export { UnknownSignalConfigModel };

