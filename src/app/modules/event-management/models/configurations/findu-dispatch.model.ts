class FindUDispatchModel {
    constructor(communityPatrolModel?: FindUDispatchModel) {
        this.findUDispatchId = communityPatrolModel ? communityPatrolModel.findUDispatchId == undefined ? '' : communityPatrolModel.findUDispatchId : '';
        this.findUDispatchName = communityPatrolModel ? communityPatrolModel.findUDispatchName == undefined ? '' : communityPatrolModel.findUDispatchName : '';
        this.description = communityPatrolModel ? communityPatrolModel.description == undefined ? '' : communityPatrolModel.description : '';
        this.deviceMovedDistance = communityPatrolModel ? communityPatrolModel.deviceMovedDistance == undefined ? '' : communityPatrolModel.deviceMovedDistance : '';
        this.findUDispatchDivisionList = communityPatrolModel ? communityPatrolModel.findUDispatchDivisionList == undefined ? null : communityPatrolModel.findUDispatchDivisionList : null;
        this.createdUserId = communityPatrolModel ? communityPatrolModel.createdUserId == undefined ? null : communityPatrolModel.createdUserId : null;
        this.modifiedUserId = communityPatrolModel ? communityPatrolModel.modifiedUserId == undefined ? null : communityPatrolModel.modifiedUserId : null;
        this.isActive = communityPatrolModel ? communityPatrolModel.isActive == undefined ? true : communityPatrolModel.isActive : true;
    }
    findUDispatchId?: string;
    findUDispatchName: string;
    description: string;
    deviceMovedDistance: string;
    findUDispatchDivisionList: DivisionsModel[];
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}

class DivisionsModel {
    divisionId: string;
}

export { FindUDispatchModel }