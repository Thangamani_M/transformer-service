class OpenAreasModel {
    constructor(openAreasModel?: OpenAreasModel) {
        this.openAreasList = openAreasModel ? openAreasModel.openAreasList == undefined ? null : openAreasModel.openAreasList : null;
    }
    openAreasList: OpenAreasListModel[];
}

class OpenAreasListModel {
    constructor(openAreasListModel?: OpenAreasListModel) {
        this.openAreaId = openAreasListModel ? openAreasListModel.openAreaId == undefined ? null : openAreasListModel.openAreaId : null;
        this.mainAreaId = openAreasListModel ? openAreasListModel.mainAreaId == undefined ? null : openAreasListModel.mainAreaId : null;
        this.modifiedUserId = openAreasListModel ? openAreasListModel.modifiedUserId == undefined ? null : openAreasListModel.modifiedUserId : null;
        this.subAreaId = openAreasListModel ? openAreasListModel.subAreaId == undefined ? null : openAreasListModel.subAreaId : null;
        this.responseOfficerId = openAreasListModel ? openAreasListModel.responseOfficerId == undefined ? null : openAreasListModel.responseOfficerId : null;
        this.isOnAir = openAreasListModel ? openAreasListModel.isOnAir == undefined ? false : openAreasListModel.isOnAir : false;
        this.openAreaReasonId = openAreasListModel ? openAreasListModel.openAreaReasonId == undefined ? null : openAreasListModel.openAreaReasonId : null;
        this.subArea = openAreasListModel ? openAreasListModel.subArea == undefined ? null : openAreasListModel.subArea : null;

    }
    openAreaId: any;
    mainAreaId:string;
    modifiedUserId: string;
    subAreaId: string
    responseOfficerId: any;
    isOnAir: boolean;
    openAreaReasonId: any;
    subArea:string;
}

class ResetVehicleModel {
    constructor(resetVehicleModel?: ResetVehicleModel) {
        this.vehicleRegistrationNumber = resetVehicleModel ? resetVehicleModel.vehicleRegistrationNumber == undefined ? '' : resetVehicleModel.vehicleRegistrationNumber : '';
        this.responseOfficerId = resetVehicleModel ? resetVehicleModel.responseOfficerId == undefined ? '' : resetVehicleModel.responseOfficerId : '';
        this.subAreaId = resetVehicleModel ? resetVehicleModel.subAreaId == undefined ? '' : resetVehicleModel.subAreaId : '';
        this.createdUserId = resetVehicleModel ? resetVehicleModel.createdUserId == undefined ? '' : resetVehicleModel.createdUserId : '';
    }
    vehicleRegistrationNumber:string;
    responseOfficerId: string;
    subAreaId: string;
    createdUserId: string;
}

export { OpenAreasModel, OpenAreasListModel, ResetVehicleModel }