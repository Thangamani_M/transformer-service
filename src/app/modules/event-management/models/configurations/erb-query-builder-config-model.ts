class ERBQueryBuilderModel {
    constructor(eRBQueryBuilderModel?: ERBQueryBuilderModel) {
        this.erbQueryBuilderId = eRBQueryBuilderModel ? eRBQueryBuilderModel.erbQueryBuilderId == undefined ? '' : eRBQueryBuilderModel.erbQueryBuilderId : '';
        this.modifiedUserId = eRBQueryBuilderModel ? eRBQueryBuilderModel.modifiedUserId == undefined ? "" : eRBQueryBuilderModel.modifiedUserId : "";
        this.createdUserId = eRBQueryBuilderModel ? eRBQueryBuilderModel.createdUserId == undefined ? null : eRBQueryBuilderModel.createdUserId : null;
        this.isActive = eRBQueryBuilderModel ? eRBQueryBuilderModel.isActive == undefined ? true : eRBQueryBuilderModel.isActive : true;
        this.templateName = eRBQueryBuilderModel ? eRBQueryBuilderModel.templateName == undefined ? "" : eRBQueryBuilderModel.templateName : "";
        this.erbTemplatetypeId = eRBQueryBuilderModel ? eRBQueryBuilderModel.erbTemplatetypeId == undefined ? "" : eRBQueryBuilderModel.erbTemplatetypeId : "";
        this.erbQueryBuilderColumnsList = eRBQueryBuilderModel ? eRBQueryBuilderModel.erbQueryBuilderColumnsList == undefined ? [] : eRBQueryBuilderModel.erbQueryBuilderColumnsList : [];

    }
    erbQueryBuilderId?: string;
    modifiedUserId: string; 
    createdUserId: string;
    isActive: boolean;
    templateName:string;
    archiveAlias:string;
    erbTemplatetypeId:string;
    erbQueryBuilderColumnsList: ERBQueryBuilderColumnsListModel[];
}

  

class ERBQueryBuilderColumnsListModel {
    constructor(eRBQueryBuilderColumnsListModel?: ERBQueryBuilderColumnsListModel) {
        this.customModuleId = eRBQueryBuilderColumnsListModel ? eRBQueryBuilderColumnsListModel.customModuleId == undefined ? null : eRBQueryBuilderColumnsListModel.customModuleId : null;
        this.schemaName = eRBQueryBuilderColumnsListModel ? eRBQueryBuilderColumnsListModel.schemaName == undefined ? '' : eRBQueryBuilderColumnsListModel.schemaName : '';
        this.tableName = eRBQueryBuilderColumnsListModel ? eRBQueryBuilderColumnsListModel.tableName == undefined ? '' : eRBQueryBuilderColumnsListModel.tableName : '';
        this.columnName = eRBQueryBuilderColumnsListModel ? eRBQueryBuilderColumnsListModel.columnName == undefined ? '' : eRBQueryBuilderColumnsListModel.columnName : '';
        this.tableNameList = eRBQueryBuilderColumnsListModel ? eRBQueryBuilderColumnsListModel.tableNameList == undefined ? [] : eRBQueryBuilderColumnsListModel.tableNameList : [];
        this.customFieldList = eRBQueryBuilderColumnsListModel ? eRBQueryBuilderColumnsListModel.customFieldList == undefined ? [] : eRBQueryBuilderColumnsListModel.customFieldList : [];

    }
    customModuleId:null;
    schemaName:string;
    tableName:string;
    columnName: string;
    tableNameList:any;
    customFieldList:any;
}



class ERBQueryBuilderDetails {
    erbQueryBuilderId:string;
    templateName: string;
    erbTemplatetypeName: string;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    createdUserName:string;
    modifiedUserName:string;
    erbQueryBuilderColumnsList:string[];
    erbQueryBuilderColumns:string;
    
}

export { ERBQueryBuilderDetails, ERBQueryBuilderModel,  ERBQueryBuilderColumnsListModel}
