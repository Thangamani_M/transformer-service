export class StackAreaConfigAddEditModel {
    constructor(stackAreaConfigAddEditModel?: StackAreaConfigAddEditModel) {
        this.createdUserId = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.createdUserId == undefined ? '' : stackAreaConfigAddEditModel.createdUserId : '';
        this.areaName = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.areaName == undefined ? '' : stackAreaConfigAddEditModel.areaName : '';
        this.divisionId = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.divisionId == undefined ? '' : stackAreaConfigAddEditModel.divisionId : '';
        this.mainAreaId = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.mainAreaId == undefined ? '' : stackAreaConfigAddEditModel.mainAreaId : '';
        this.isActive = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.isActive == undefined ? true : stackAreaConfigAddEditModel.isActive : true;
        this.modifiedUserId = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.modifiedUserId == undefined ? '' : stackAreaConfigAddEditModel.modifiedUserId : '';
        this.stackAreaConfigId = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.stackAreaConfigId == undefined ? '' : stackAreaConfigAddEditModel.stackAreaConfigId : '';

        this.stackAreaConfigDivisionList = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.stackAreaConfigDivisionList == undefined ? null : stackAreaConfigAddEditModel.stackAreaConfigDivisionList : null;
        this.stackAreaConfigMainAreaList = stackAreaConfigAddEditModel ? stackAreaConfigAddEditModel.stackAreaConfigMainAreaList == undefined ? null : stackAreaConfigAddEditModel.stackAreaConfigMainAreaList : null;
    }
    createdUserId?: string;
    areaName?: string;
    divisionId?: string;
    mainAreaId?: string;
    isActive?:boolean;
    modifiedUserId?:string;
    stackAreaConfigId?:any;
    stackAreaConfigDivisionList:DivisionsModel[];
    stackAreaConfigMainAreaList?:MainAreaModel[];
}

class MainAreaModel {
    mainAreaId: string;
}

class DivisionsModel {
    divisionId: string;
}

export class StackAreaConfigView {
    areaName:string;
    mainAreaName:string;
    divisionName: Date;
    divisionId: string;
    mainAreaId: any;
    stackAreaConfigId: string;
    stackAreaConfigDivisions :string;
    stackAreaConfigMainAreas :string;
    isActive : boolean;
    createdDate : Date;
    modifiedDate : Date;
    createdUserName :string;
    modifiedUserName :string;
}

