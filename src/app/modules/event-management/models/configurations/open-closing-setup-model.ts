class OpenClosingSetupModel {
    constructor(openClosingSetupModel?: OpenClosingSetupModel) {
        this.openCloseSetupId = openClosingSetupModel ? openClosingSetupModel.openCloseSetupId == undefined ? '' : openClosingSetupModel.openCloseSetupId : '';
        this.modifiedUserId = openClosingSetupModel ? openClosingSetupModel.modifiedUserId == undefined ? "" : openClosingSetupModel.modifiedUserId : "";
        this.createdUserId = openClosingSetupModel ? openClosingSetupModel.createdUserId == undefined ? null : openClosingSetupModel.createdUserId : null;
        this.isActive = openClosingSetupModel ? openClosingSetupModel.isActive == undefined ? true : openClosingSetupModel.isActive : true;
        this.openCloseSetupName = openClosingSetupModel ? openClosingSetupModel.openCloseSetupName == undefined ? "" : openClosingSetupModel.openCloseSetupName : "";
        this.description = openClosingSetupModel ? openClosingSetupModel.description == undefined ? "" : openClosingSetupModel.description : "";
        this.openCloseSetupDivisionList = openClosingSetupModel ? openClosingSetupModel.openCloseSetupDivisionList == undefined ? null : openClosingSetupModel.openCloseSetupDivisionList : null;
        this.openCloseSetupArmList = openClosingSetupModel ? openClosingSetupModel.openCloseSetupArmList == undefined ? null : openClosingSetupModel.openCloseSetupArmList : null;
        this.openCloseSetupDisarmList = openClosingSetupModel ? openClosingSetupModel.openCloseSetupDisarmList == undefined ? null : openClosingSetupModel.openCloseSetupDisarmList : null;
        this.disArmingWindow = openClosingSetupModel ? openClosingSetupModel.disArmingWindow == undefined ? "" : openClosingSetupModel.disArmingWindow : "";
        this.armingWindow = openClosingSetupModel ? openClosingSetupModel.armingWindow == undefined ? "" : openClosingSetupModel.armingWindow : "";
        this.failToOpenAlarmTypeId = openClosingSetupModel ? openClosingSetupModel.failToOpenAlarmTypeId == undefined ? "" : openClosingSetupModel.failToOpenAlarmTypeId : "";
        this.failToCloseAlarmTypeId = openClosingSetupModel ? openClosingSetupModel.failToCloseAlarmTypeId == undefined ? "" : openClosingSetupModel.failToCloseAlarmTypeId : "";

    }
    openCloseSetupId?: string;
    modifiedUserId?: string;
    openCloseSetupName?: string;
    description:string;
    isActive: boolean;
    openCloseSetupDivisionList?:DivisionsModel[];
    openCloseSetupArmList?: OpenCloseSetupArmListModel[]; 
    openCloseSetupDisarmList?: OpenCloseSetupDisarmListModel[];
    createdUserId: string;
    disArmingWindow: string;
    armingWindow: string;
    failToOpenAlarmTypeId: string;
    failToCloseAlarmTypeId: string;
}

class DivisionsModel {
    divisionId: string;
}

class OpenCloseSetupArmListModel {
    constructor(openCloseSetupArmListModel?: OpenCloseSetupArmListModel) {
        this.openCloseSetupArmId = openCloseSetupArmListModel ? openCloseSetupArmListModel.openCloseSetupArmId == undefined ? '' : openCloseSetupArmListModel.openCloseSetupArmId : '';
        this.signalReceivedAlarmId = openCloseSetupArmListModel ? openCloseSetupArmListModel.signalReceivedAlarmId == undefined ? '' : openCloseSetupArmListModel.signalReceivedAlarmId : '';
        this.unscheduledCloseAlarmId = openCloseSetupArmListModel ? openCloseSetupArmListModel.unscheduledCloseAlarmId == undefined ? '' : openCloseSetupArmListModel.unscheduledCloseAlarmId : '';
        this.closeEarlyAlarmId = openCloseSetupArmListModel ? openCloseSetupArmListModel.closeEarlyAlarmId == undefined ? '' : openCloseSetupArmListModel.closeEarlyAlarmId : '';
        this.closeLateAlarmId = openCloseSetupArmListModel ? openCloseSetupArmListModel.closeLateAlarmId == undefined ? '' : openCloseSetupArmListModel.closeLateAlarmId : '';
        

    }
    openCloseSetupArmId:string;
    signalReceivedAlarmId:string;
    unscheduledCloseAlarmId:string;
    closeEarlyAlarmId:string;
    closeLateAlarmId:string;
}

class OpenCloseSetupDisarmListModel {
    constructor(openCloseSetupDisarmListModel?: OpenCloseSetupDisarmListModel) {
        this.openCloseSetupDisarmId = openCloseSetupDisarmListModel ? openCloseSetupDisarmListModel.openCloseSetupDisarmId == undefined ? '' : openCloseSetupDisarmListModel.openCloseSetupDisarmId : '';
        this.signalReceivedAlarmId = openCloseSetupDisarmListModel ? openCloseSetupDisarmListModel.signalReceivedAlarmId == undefined ? '' : openCloseSetupDisarmListModel.signalReceivedAlarmId : '';
        this.openEarlyAlarmId = openCloseSetupDisarmListModel ? openCloseSetupDisarmListModel.openEarlyAlarmId == undefined ? '' : openCloseSetupDisarmListModel.openEarlyAlarmId : '';
        this.openLateAlarmId = openCloseSetupDisarmListModel ? openCloseSetupDisarmListModel.openLateAlarmId == undefined ? '' : openCloseSetupDisarmListModel.openLateAlarmId : '';
        this.afterHoursAlarmId = openCloseSetupDisarmListModel ? openCloseSetupDisarmListModel.afterHoursAlarmId == undefined ? '' : openCloseSetupDisarmListModel.afterHoursAlarmId : '';
      

    }
    openCloseSetupDisarmId:string;
    signalReceivedAlarmId:string;
    openEarlyAlarmId:string;
    openLateAlarmId:string;
    afterHoursAlarmId:string;
}

class openCloseSetupDetailsModel {
    openCloseSetupId?: string;
    openCloseSetupName?: string;
    description?: string;
    isActive?: boolean;
    createdDate?: string;
    createdUserId?: string;
    modifiedDate?: string;
    modifiedUserId?: string;
    openCloseSetupArmList?: string[];
    openCloseSetupDisarmList?: string[];
    openCloseSetupDivisions?: string;
    openCloseSetupArms?: string;
    openCloseSetupDisarms?: string;
}

export { OpenClosingSetupModel, OpenCloseSetupArmListModel, OpenCloseSetupDisarmListModel, openCloseSetupDetailsModel };


