class PostIncidentPatrolModel {
    constructor(postIncidentPatrolModel?: PostIncidentPatrolModel) {
        this.postIncidentPatrolId = postIncidentPatrolModel ? postIncidentPatrolModel.postIncidentPatrolId == undefined ? '' : postIncidentPatrolModel.postIncidentPatrolId : '';
        this.divisionId = postIncidentPatrolModel ? postIncidentPatrolModel.divisionId == undefined ? "" : postIncidentPatrolModel.divisionId : "";
        this.postIncidentPatrolMainAreasList = postIncidentPatrolModel ? postIncidentPatrolModel.postIncidentPatrolMainAreasList == undefined ? [] : postIncidentPatrolModel.postIncidentPatrolMainAreasList : [];
        this.createdUserId = postIncidentPatrolModel ? postIncidentPatrolModel.createdUserId == undefined ? null : postIncidentPatrolModel.createdUserId : null;
        this.modifiedUserId = postIncidentPatrolModel ? postIncidentPatrolModel.modifiedUserId == undefined ? null : postIncidentPatrolModel.modifiedUserId : "";
        this.isActive = postIncidentPatrolModel ? postIncidentPatrolModel.isActive == undefined ? true : postIncidentPatrolModel.isActive : true;
        this.duration = postIncidentPatrolModel ? postIncidentPatrolModel.duration == undefined ? "" : postIncidentPatrolModel.duration : "";
        this.patrollingLimit = postIncidentPatrolModel ? postIncidentPatrolModel.patrollingLimit == undefined ? "" : postIncidentPatrolModel.patrollingLimit : "";
        this.tempPostIncidentPatrolMainAreasList = postIncidentPatrolModel ? postIncidentPatrolModel.tempPostIncidentPatrolMainAreasList == undefined ? "" : postIncidentPatrolModel.tempPostIncidentPatrolMainAreasList : "";
        this.isEnabled = postIncidentPatrolModel ? postIncidentPatrolModel.isEnabled == undefined ? true : postIncidentPatrolModel.isEnabled : true;
        this.postIncidentPatrolName = postIncidentPatrolModel ? postIncidentPatrolModel.postIncidentPatrolName == undefined ? '' : postIncidentPatrolModel.postIncidentPatrolName : '';

    }
    postIncidentPatrolId?: string;
    divisionId: string;
    postIncidentPatrolMainAreasList: PostIncidentPatrolMainAreasListModel[]; 
    createdUserId: string
    modifiedUserId: string
    isActive: boolean;
    duration:string;
    patrollingLimit:string;
    tempPostIncidentPatrolMainAreasList:string;
    isEnabled:boolean;
    postIncidentPatrolName?:string;
}

class PostIncidentPatrolMainAreasListModel {
    constructor(postIncidentPatrolMainAreasListModel?: PostIncidentPatrolMainAreasListModel) {
        this.mainAreaId = postIncidentPatrolMainAreasListModel ? postIncidentPatrolMainAreasListModel.mainAreaId == undefined ? '' : postIncidentPatrolMainAreasListModel.mainAreaId : '';

    }
    mainAreaId:string;
}




class PostIncidentPatrolDetails {
    postIncidentPatrolName:string;
    postIncidentPatrolId:string;
    divisionId:string;
    duration: string;
    patrollingLimit: string;
    postIncidentPatrolMainAreasList: string[];
    divisionName:string;
    createdUserId:string;
    modifiedDate:string;
    createdDate:string;
    modifiedUserId:string;
    postIncidentPatrolMainAreas:string;
    isEnabled:boolean;
    isActive:boolean;

}

export { PostIncidentPatrolDetails, PostIncidentPatrolModel }
