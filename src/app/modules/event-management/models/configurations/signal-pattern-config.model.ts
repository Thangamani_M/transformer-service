class signalpatternPatrolConfigModel {
    constructor(patrolConfigModel?: signalpatternPatrolConfigModel) {
        this.generatedAlarmTypeId = patrolConfigModel ? patrolConfigModel.generatedAlarmTypeId == undefined ? '' : patrolConfigModel.generatedAlarmTypeId : '';
        this.signalDurationMinutes = patrolConfigModel ? patrolConfigModel.signalDurationMinutes == undefined ? '' : patrolConfigModel.signalDurationMinutes : '';
        this.descriptions = patrolConfigModel ? patrolConfigModel.descriptions == undefined ? '' : patrolConfigModel.descriptions : '';
        this.isTechnicianEscalationRequired = patrolConfigModel ? patrolConfigModel.isTechnicianEscalationRequired == undefined ? false : patrolConfigModel.isTechnicianEscalationRequired : false;
        this.createdUserId = patrolConfigModel ? patrolConfigModel.createdUserId == undefined ? '' : patrolConfigModel.createdUserId : '';
        this.signalPatternAlarmTypesList = patrolConfigModel ? patrolConfigModel.signalPatternAlarmTypesList == undefined ? null : patrolConfigModel.signalPatternAlarmTypesList : null;
        this.isActive = patrolConfigModel ? patrolConfigModel.isActive == undefined ? false : patrolConfigModel.isActive : false;
        this.signalPatternId = patrolConfigModel ? patrolConfigModel.signalPatternId == undefined ? '' : patrolConfigModel.signalPatternId : '';
        this.modifiedUserId = patrolConfigModel ? patrolConfigModel.modifiedUserId == undefined ? '' : patrolConfigModel.modifiedUserId : '';
    }
    
    generatedAlarmTypeId?:number|string;
    signalDurationMinutes?:string;
    descriptions?:string;
    isTechnicianEscalationRequired?:boolean;
    createdUserId?:string;
    isActive?:boolean;
    signalPatternId?:string;
    modifiedUserId?:string;
    signalPatternAlarmTypesList?:signalpatternPatrolModel[];
}
class signalpatternPatrolModel {
    constructor(patrolConfigModel?: signalpatternPatrolModel) {
        this.alarmTypeId = patrolConfigModel ? patrolConfigModel.alarmTypeId == undefined ? '' : patrolConfigModel.alarmTypeId : '';
        this.signalIntervalSeconds = patrolConfigModel ? patrolConfigModel.signalIntervalSeconds == undefined ? '' : patrolConfigModel.signalIntervalSeconds : '';
        this.sortOrder = patrolConfigModel ? patrolConfigModel.sortOrder == undefined ? '' : patrolConfigModel.sortOrder : '';
    }
    
    sortOrder?:string;
    signalIntervalSeconds?:string;
    alarmTypeId?:string;
}
    export { signalpatternPatrolConfigModel, signalpatternPatrolModel };

