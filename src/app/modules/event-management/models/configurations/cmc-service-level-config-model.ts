class CMCServiceLevelConfigModel {
    constructor(cMCServiceLevelConfigModel?: CMCServiceLevelConfigModel) {
        this.cmcServiceLevelConfigId = cMCServiceLevelConfigModel ? cMCServiceLevelConfigModel.cmcServiceLevelConfigId == undefined ? '' : cMCServiceLevelConfigModel.cmcServiceLevelConfigId : '';
        this.createdUserId = cMCServiceLevelConfigModel ? cMCServiceLevelConfigModel.createdUserId == undefined ? '' : cMCServiceLevelConfigModel.createdUserId : '';
        this.modifiedUserId = cMCServiceLevelConfigModel ? cMCServiceLevelConfigModel.modifiedUserId == undefined ? '' : cMCServiceLevelConfigModel.modifiedUserId : '';
        this.phonebackServiceLevel = cMCServiceLevelConfigModel ? cMCServiceLevelConfigModel.phonebackServiceLevel == undefined ? '' : cMCServiceLevelConfigModel.phonebackServiceLevel : '';
        this.dispatchTimeServiceLevel = cMCServiceLevelConfigModel ? cMCServiceLevelConfigModel.dispatchTimeServiceLevel == undefined ? '' : cMCServiceLevelConfigModel.dispatchTimeServiceLevel : '';
        this.finishTimeServiceLevel = cMCServiceLevelConfigModel ? cMCServiceLevelConfigModel.finishTimeServiceLevel == undefined ? '' : cMCServiceLevelConfigModel.finishTimeServiceLevel : '';
        this.isActive = cMCServiceLevelConfigModel ? cMCServiceLevelConfigModel.isActive == undefined ? true : cMCServiceLevelConfigModel.isActive : true;

    }
    cmcServiceLevelConfigId?: string;
    createdUserId?:string;
    modifiedUserId?:string;
    phonebackServiceLevel?:string;
    dispatchTimeServiceLevel?:string;
    finishTimeServiceLevel?:string
    isActive?: boolean;
}  

class CMCServiceLevelConfigDetails {
    cmcServiceLevelConfigId?:string;
    phonebackServiceLevel?:string;
    dispatchTimeServiceLevel?: string;
    finishTimeServiceLevel?: string;
    isActive?: boolean;
    createdDate?:string;
    createdUserId?:string;
    modifiedDate?:string;
    modifiedUserId?:string;
}

export { CMCServiceLevelConfigModel, CMCServiceLevelConfigDetails }
