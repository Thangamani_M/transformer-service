class SMTechIncentiveModel {
    constructor(smTechIncentiveModel?: SMTechIncentiveModel) {
        this.teamLeaders = smTechIncentiveModel ? smTechIncentiveModel.teamLeaders == undefined ? [] : smTechIncentiveModel.teamLeaders : [];
        this.technicians = smTechIncentiveModel ? smTechIncentiveModel.technicians == undefined ? [] : smTechIncentiveModel.technicians : [];
    }

    teamLeaders: TeamLeadersListModel[];
    technicians: TechniciansListModel[];
}

class TeamLeadersListModel {
    constructor(teamLeadersListModel?: TeamLeadersListModel) {
        this.signalManagementTeamLeaderId = teamLeadersListModel ? teamLeadersListModel.signalManagementTeamLeaderId == undefined ? '' : teamLeadersListModel.signalManagementTeamLeaderId : '';
        this.targetAmount = teamLeadersListModel ? teamLeadersListModel.targetAmount == undefined ? "" : teamLeadersListModel.targetAmount : "";
        this.incentiveAmount = teamLeadersListModel ? teamLeadersListModel.incentiveAmount == undefined ? "" : teamLeadersListModel.incentiveAmount : "";
        this.employeeName = teamLeadersListModel ? teamLeadersListModel.employeeName == undefined ? "" : teamLeadersListModel.employeeName : "";
        this.techAreas = teamLeadersListModel ? teamLeadersListModel.techAreas == undefined ? []: teamLeadersListModel.techAreas : [];
        this.signalManagementTeamLeaderIncentiveId = teamLeadersListModel ? teamLeadersListModel.signalManagementTeamLeaderIncentiveId == undefined ? null : teamLeadersListModel.signalManagementTeamLeaderIncentiveId : null;

    }
    signalManagementTeamLeaderId: string;
    signalManagementTeamLeaderIncentiveId:string;
    targetAmount:string;
    incentiveAmount: string;
    techAreas: TechAreaListModel[];
    employeeName: string;
}

class TechniciansListModel {
    constructor(teamLeadersListModel?: TechniciansListModel) {
        this.signalManagementTechnicianId = teamLeadersListModel ? teamLeadersListModel.signalManagementTechnicianId == undefined ? '' : teamLeadersListModel.signalManagementTechnicianId : '';
        this.incentives = teamLeadersListModel ? teamLeadersListModel.incentives == undefined ? "" : teamLeadersListModel.incentives : "";
        this.techAreaName = teamLeadersListModel ? teamLeadersListModel.techAreaName == undefined ? "" : teamLeadersListModel.techAreaName : "";
        this.employeeName = teamLeadersListModel ? teamLeadersListModel.employeeName == undefined ? "" : teamLeadersListModel.employeeName : "";
        
    }
    signalManagementTechnicianId: string;
    techAreaName:string;
    incentives: string;
    employeeName:string;
}

class TechAreaListModel {
    constructor(techAreaListModel?: TechAreaListModel) {
        this.techAreaId = techAreaListModel ? techAreaListModel.techAreaId == undefined ? '' : techAreaListModel.techAreaId : '';

    }
    techAreaId: string;
}

export { SMTechIncentiveModel,TeamLeadersListModel,TechniciansListModel,TechAreaListModel }
