class AlarmTypeConfigModel {
    constructor(alarmTypeConfigModel?: AlarmTypeConfigModel) {
        this.alarmTypeConfigId = alarmTypeConfigModel ? alarmTypeConfigModel.alarmTypeConfigId == undefined ? '' : alarmTypeConfigModel.alarmTypeConfigId : '';
        this.alarmTypeConfigName = alarmTypeConfigModel ? alarmTypeConfigModel.alarmTypeConfigName == undefined ? "" : alarmTypeConfigModel.alarmTypeConfigName : "";
        this.alarmTypeConfigDivisionList = alarmTypeConfigModel ? alarmTypeConfigModel.alarmTypeConfigDivisionList == undefined ? [] : alarmTypeConfigModel.alarmTypeConfigDivisionList : [];
        this.createdUserId = alarmTypeConfigModel ? alarmTypeConfigModel.createdUserId == undefined ? null : alarmTypeConfigModel.createdUserId : null;
        this.modifiedUserId = alarmTypeConfigModel ? alarmTypeConfigModel.modifiedUserId == undefined ? null : alarmTypeConfigModel.modifiedUserId : "";
        this.isActive = alarmTypeConfigModel ? alarmTypeConfigModel.isActive == undefined ? true : alarmTypeConfigModel.isActive : true;
        this.description = alarmTypeConfigModel ? alarmTypeConfigModel.description == undefined ? "" : alarmTypeConfigModel.description : "";
        this.tempAlarmTypeConfigDivisionList = alarmTypeConfigModel ? alarmTypeConfigModel.tempAlarmTypeConfigDivisionList == undefined ? "" : alarmTypeConfigModel.tempAlarmTypeConfigDivisionList : "";
        this.alarmTypeConfigListList = alarmTypeConfigModel ? alarmTypeConfigModel.alarmTypeConfigListList == undefined ? [] : alarmTypeConfigModel.alarmTypeConfigListList : [];

    }
    alarmTypeConfigId?: string;
    alarmTypeConfigName?: string;
    alarmTypeConfigDivisionList?: AlarmTypeConfigDivisionListModel[]; 
    createdUserId?: string
    modifiedUserId?: string
    isActive?: boolean;
    description?:string;
    tempAlarmTypeConfigDivisionList?:string;
    alarmTypeConfigListList?:AlarmTypeConfigListListModel[];
}

class AlarmTypeConfigDivisionListModel {
    constructor(alarmTypeConfigDivisionListModel?: AlarmTypeConfigDivisionListModel) {
        this.divisionId = alarmTypeConfigDivisionListModel ? alarmTypeConfigDivisionListModel.divisionId == undefined ? '' : alarmTypeConfigDivisionListModel.divisionId : '';

    }
    divisionId:string;
}

class AlarmTypeConfigListListModel {
    constructor(alarmTypeConfigListListModel?: AlarmTypeConfigListListModel) {
      this.alarmTypeId = alarmTypeConfigListListModel ? alarmTypeConfigListListModel.alarmTypeId == undefined ? '' : alarmTypeConfigListListModel.alarmTypeId : '';
      this.isAutoDispatchMode = alarmTypeConfigListListModel ? alarmTypeConfigListListModel.isAutoDispatchMode == undefined ? true : alarmTypeConfigListListModel.isAutoDispatchMode : true;
      this.isNormalModeRequest = alarmTypeConfigListListModel ? alarmTypeConfigListListModel.isNormalModeRequest == undefined ? true : alarmTypeConfigListListModel.isNormalModeRequest : true;
      this.isStormModeRequest = alarmTypeConfigListListModel ? alarmTypeConfigListListModel.isStormModeRequest == undefined ? true : alarmTypeConfigListListModel.isStormModeRequest : true;
      this.alarmTypeDescription = alarmTypeConfigListListModel ? alarmTypeConfigListListModel.alarmTypeDescription == undefined ? '' : alarmTypeConfigListListModel.alarmTypeDescription : '';
      this.alarm = alarmTypeConfigListListModel ? alarmTypeConfigListListModel.alarm == undefined ? '' : alarmTypeConfigListListModel.alarm : '';

    }
    alarmTypeId?:string;
    alarm?:string;
    alarmTypeDescription?:string;
    isAutoDispatchMode?:boolean;
    isNormalModeRequest?:boolean;
    isStormModeRequest?:boolean;
}

class AlarmTypeConfigDetails {
    alarmTypeConfigId:string;
    alarmTypeConfigName:string;
    description: string;
    isEnabled: boolean;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    alarmTypeConfigDivisionList:string[];
    alarmTypeConfigListList:string[];
    alarmTypeConfigDivisions:string;
    alarmTypeConfigList:string;
    createdUserName: string;
    modifiedUserName:string;

}

export {AlarmTypeConfigDetails, AlarmTypeConfigModel, AlarmTypeConfigListListModel}