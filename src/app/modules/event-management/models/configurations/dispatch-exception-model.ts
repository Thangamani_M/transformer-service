class DispatchExceptionModel {
    constructor(dispatchExceptionModel?: DispatchExceptionModel) {
        this.dispatchExceptionId = dispatchExceptionModel ? dispatchExceptionModel.dispatchExceptionId == undefined ? '' : dispatchExceptionModel.dispatchExceptionId : '';
        this.dispatchExceptionName = dispatchExceptionModel ? dispatchExceptionModel.dispatchExceptionName == undefined ? "" : dispatchExceptionModel.dispatchExceptionName : "";
        this.dispatchExceptionDivisionsList = dispatchExceptionModel ? dispatchExceptionModel.dispatchExceptionDivisionsList == undefined ? [] : dispatchExceptionModel.dispatchExceptionDivisionsList : [];
        this.createdUserId = dispatchExceptionModel ? dispatchExceptionModel.createdUserId == undefined ? null : dispatchExceptionModel.createdUserId : null;
        this.modifiedUserId = dispatchExceptionModel ? dispatchExceptionModel.modifiedUserId == undefined ? null : dispatchExceptionModel.modifiedUserId : "";
        this.isActive = dispatchExceptionModel ? dispatchExceptionModel.isActive == undefined ? true : dispatchExceptionModel.isActive : true;
        this.description = dispatchExceptionModel ? dispatchExceptionModel.description == undefined ? "" : dispatchExceptionModel.description : "";
        this.tempDispatchExceptionDivisionsList = dispatchExceptionModel ? dispatchExceptionModel.tempDispatchExceptionDivisionsList == undefined ? "" : dispatchExceptionModel.tempDispatchExceptionDivisionsList : "";
        this.dispatchTime = dispatchExceptionModel ? dispatchExceptionModel.dispatchTime == undefined ? '' : dispatchExceptionModel.dispatchTime : '';
        this.distance = dispatchExceptionModel ? dispatchExceptionModel.distance == undefined ? '' : dispatchExceptionModel.distance : '';
    }
    dispatchExceptionId?: string;
    dispatchExceptionName: string;
    dispatchExceptionDivisionsList: DispatchExceptionDivisionsListModel[]; 
    createdUserId: string
    modifiedUserId: string
    isActive: boolean;
    description:string;
    tempDispatchExceptionDivisionsList:string;
    dispatchTime?:string;
    distance?:string;
}

class DispatchExceptionDivisionsListModel {
    constructor(dispatchExceptionDivisionsListModel?: DispatchExceptionDivisionsListModel) {
        this.divisionId = dispatchExceptionDivisionsListModel ? dispatchExceptionDivisionsListModel.divisionId == undefined ? '' : dispatchExceptionDivisionsListModel.divisionId : '';

    }
    divisionId:string;
}

class DispatchExceptionDetails {
    divisionName?:string;
    dispatchExceptionId?:string;
    dispatchExceptionName?:string;
    description?:string;
    dispatchExceptionDivisionsList?: string[];
    dispatchExceptionDivisions?:string;
    distance?:string;
    dispatchTime?:string;
    isActive?:boolean;
    createdDate?:string;
    modifiedDate?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    createdUserName?:string;
    modifiedUserName?:string;

}

export { DispatchExceptionModel, DispatchExceptionDetails}
