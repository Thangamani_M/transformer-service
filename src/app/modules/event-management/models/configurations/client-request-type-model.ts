class ClientRequestTypeModel {
    constructor(clientRequestTypeModel?: ClientRequestTypeModel) {
        this.clientRequestTypeId = clientRequestTypeModel ? clientRequestTypeModel.clientRequestTypeId == undefined ? '' : clientRequestTypeModel.clientRequestTypeId : '';
        this.createdUserId = clientRequestTypeModel ? clientRequestTypeModel.createdUserId == undefined ? '' : clientRequestTypeModel.createdUserId : '';
        this.modifiedUserId = clientRequestTypeModel ? clientRequestTypeModel.modifiedUserId == undefined ? '' : clientRequestTypeModel.modifiedUserId : '';
        this.clientRequestTypeName = clientRequestTypeModel ? clientRequestTypeModel.clientRequestTypeName == undefined ? '' : clientRequestTypeModel.clientRequestTypeName : '';
        this.description = clientRequestTypeModel ? clientRequestTypeModel.description == undefined ? '' : clientRequestTypeModel.description : '';
        this.isActive = clientRequestTypeModel ? clientRequestTypeModel.isActive == undefined ? true : clientRequestTypeModel.isActive : true;
        this.isSystem = clientRequestTypeModel ? clientRequestTypeModel.isSystem == undefined ? true : clientRequestTypeModel.isSystem : true;

    }
    clientRequestTypeId?: string;
    createdUserId?:string;
    modifiedUserId?:string;
    clientRequestTypeName?:string;
    description?:string;
    isActive?: boolean;
    isSystem?: boolean;
}  

class ClientRequestTypeDetails {
    clientRequestTypeId:string;
    clientRequestTypeName:string;
    description: string;
    isActive: boolean;
    isSystem:boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
}

export { ClientRequestTypeModel, ClientRequestTypeDetails }
