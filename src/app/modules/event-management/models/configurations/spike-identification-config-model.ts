class SpikeIdentificationConfigModel {
    constructor(spikeIdentificationConfigModel?: SpikeIdentificationConfigModel) {
        this.spikeIdentificationConfigId = spikeIdentificationConfigModel ? spikeIdentificationConfigModel.spikeIdentificationConfigId == undefined ? '' : spikeIdentificationConfigModel.spikeIdentificationConfigId : '';
        this.windowPeriods = spikeIdentificationConfigModel ? spikeIdentificationConfigModel.windowPeriods == undefined ? null : spikeIdentificationConfigModel.windowPeriods : null;
        this.weekReview = spikeIdentificationConfigModel ? spikeIdentificationConfigModel.weekReview == undefined ? null : spikeIdentificationConfigModel.weekReview : null;
        this.deviationValue = spikeIdentificationConfigModel ? spikeIdentificationConfigModel.deviationValue == undefined ? null : spikeIdentificationConfigModel.deviationValue : null;
        this.createdDate = spikeIdentificationConfigModel ? spikeIdentificationConfigModel.createdDate == undefined ? '' : spikeIdentificationConfigModel.createdDate : '';
        this.createdUserId = spikeIdentificationConfigModel ? spikeIdentificationConfigModel.createdUserId == undefined ? '' : spikeIdentificationConfigModel.createdUserId : '';
        this.modifiedDate = spikeIdentificationConfigModel ? spikeIdentificationConfigModel.modifiedDate == undefined ? '' : spikeIdentificationConfigModel.modifiedDate : '';
        this.modifiedUserId = spikeIdentificationConfigModel ? spikeIdentificationConfigModel.modifiedUserId == undefined ? '' : spikeIdentificationConfigModel.modifiedUserId : '';
    }
    spikeIdentificationConfigId:string;
    windowPeriods:number;
    weekReview:number;
    deviationValue:number;
    createdDate:string;
    createdUserId:string;
    modifiedUserId:string;
    modifiedDate:string;

    
}
export { SpikeIdentificationConfigModel };
