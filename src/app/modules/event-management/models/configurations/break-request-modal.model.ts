class BreakRequestModel {
    constructor(breakRequestModel?: BreakRequestModel) {
        this.responseOfficerBreakId = breakRequestModel ? breakRequestModel.responseOfficerBreakId == undefined ? '' : breakRequestModel.responseOfficerBreakId : '';
        this.reason = breakRequestModel ? breakRequestModel.reason == undefined ? "" : breakRequestModel.reason : "";
        this.repliedBy = breakRequestModel ? breakRequestModel.repliedBy == undefined ? null : breakRequestModel.repliedBy : null;
        this.breakDuration = breakRequestModel ? breakRequestModel.breakDuration == undefined ? "" : breakRequestModel.breakDuration : "";
        this.comments = breakRequestModel ? breakRequestModel.comments == undefined ? "" : breakRequestModel.comments : "";
        this.isRadioButton = breakRequestModel ? breakRequestModel.isRadioButton == undefined ? true : breakRequestModel.isRadioButton : true;
    }
    responseOfficerBreakId?: string;
    reason?: string;
    repliedBy?: string;
    breakDuration?:string;  
    comments?:string;
    isRadioButton?: boolean;
}

class ResponseOfficerDetails {
    subAreaName?: string;
    displayName?:string;
    vehicleStatus?:string;
}

class VehicleShiftChangeRequestModel {
    constructor(vehicleShiftChangeRequestModel ?: VehicleShiftChangeRequestModel) {
        this.shiftChangingId = vehicleShiftChangeRequestModel ? vehicleShiftChangeRequestModel.shiftChangingId == undefined ? '' : vehicleShiftChangeRequestModel.shiftChangingId : '';
        this.approvedBy = vehicleShiftChangeRequestModel ? vehicleShiftChangeRequestModel.approvedBy == undefined ? "" : vehicleShiftChangeRequestModel.approvedBy : "";
        this.comments = vehicleShiftChangeRequestModel ? vehicleShiftChangeRequestModel.comments == undefined ? "" : vehicleShiftChangeRequestModel.comments : "";
        this.isApproved = vehicleShiftChangeRequestModel ? vehicleShiftChangeRequestModel.isApproved == undefined ? false : vehicleShiftChangeRequestModel.isApproved : false;
        this.responsedUserId = vehicleShiftChangeRequestModel ? vehicleShiftChangeRequestModel.responsedUserId == undefined ? '' : vehicleShiftChangeRequestModel.responsedUserId : '';
    }
    shiftChangingId?: string;
    approvedBy?: string;  
    comments?:string;
    isApproved?: boolean;
    responsedUserId?:string

}


export { BreakRequestModel, ResponseOfficerDetails, VehicleShiftChangeRequestModel}
