class RoAcceptTimeModel {
    constructor(roAcceptTimeModel?: RoAcceptTimeModel) {
        this.ROAcceptTimeId = roAcceptTimeModel ? roAcceptTimeModel.ROAcceptTimeId == undefined ? '' : roAcceptTimeModel.ROAcceptTimeId : '';
        this.divisionId = roAcceptTimeModel ? roAcceptTimeModel.divisionId == undefined ? "" : roAcceptTimeModel.divisionId : "";
        
        this.createdUserId = roAcceptTimeModel ? roAcceptTimeModel.createdUserId == undefined ? null : roAcceptTimeModel.createdUserId : null;
        this.modifiedUserId = roAcceptTimeModel ? roAcceptTimeModel.modifiedUserId == undefined ? null : roAcceptTimeModel.modifiedUserId : "";
        this.isActive = roAcceptTimeModel ? roAcceptTimeModel.isActive == undefined ? true : roAcceptTimeModel.isActive : true;
        this.time = roAcceptTimeModel ? roAcceptTimeModel.time == undefined ? "" : roAcceptTimeModel.time : "";
       
        this.mainAreaId = roAcceptTimeModel ? roAcceptTimeModel.mainAreaId == undefined ? "" : roAcceptTimeModel.mainAreaId : "";
    }
    ROAcceptTimeId?: string;
    divisionId: string;
    createdUserId: string
    modifiedUserId: string
    isActive: boolean;
    time:string;
    mainAreaId?:string;
}

  


class RoAcceptTimeDetails {
    ROAcceptTimeId:string;
    divisionName:string;
    time:string;
    ROAcceptTimeMainAreaList: string[];
    ROAcceptTimeMainAreas:string;
    divisionId:string;
    createdDate:string;
    modifiedDate:string;
    createdUserId:string;
    modifiedUserId:string;
    createdUserName:string;
    modifiedUserName:string;
    isActive:boolean;

}
export { RoAcceptTimeModel, RoAcceptTimeDetails}
