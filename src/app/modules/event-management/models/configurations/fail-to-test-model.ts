class FailToTestModel {
    constructor(failToTestModel?: FailToTestModel) {
        this.failToTestId = failToTestModel ? failToTestModel.failToTestId == undefined ? '' : failToTestModel.failToTestId : '';
        this.newDecoderOnSiteDelay = failToTestModel ? failToTestModel.newDecoderOnSiteDelay == undefined ? "" : failToTestModel.newDecoderOnSiteDelay : "";
        this.createdUserId = failToTestModel ? failToTestModel.createdUserId == undefined ? null : failToTestModel.createdUserId : null;
        this.modifiedUserId = failToTestModel ? failToTestModel.modifiedUserId == undefined ? null : failToTestModel.modifiedUserId : "";
        this.isActive = failToTestModel ? failToTestModel.isActive == undefined ? true : failToTestModel.isActive : true;
        this.alarmTypId = failToTestModel ? failToTestModel.alarmTypId == undefined ? "" : failToTestModel.alarmTypId : "";
        this.startTime = failToTestModel ? failToTestModel.startTime == undefined ? '' : failToTestModel.startTime : '';
        this.endTime = failToTestModel ? failToTestModel.endTime == undefined ? '' : failToTestModel.endTime : '';
        this.noSignalHours = failToTestModel ? failToTestModel.noSignalHours == undefined ? '' : failToTestModel.noSignalHours : '';

    }
    failToTestId?: string;
    newDecoderOnSiteDelay: string;
    createdUserId: string
    modifiedUserId: string
    isActive: boolean;
    alarmTypId:string;
    startTime?:string;
    endTime?:string;
    noSignalHours?: string;
}

class PatrolConfigModel {
    constructor(patrolConfigModel?: PatrolConfigModel) {
        this.createdUserId = patrolConfigModel ? patrolConfigModel.createdUserId == undefined ? '' : patrolConfigModel.createdUserId : '';
        this.dailyLimit = patrolConfigModel ? patrolConfigModel.dailyLimit == undefined ? null : patrolConfigModel.dailyLimit : null;
        this.dailyVisibilityPatrolLimit = patrolConfigModel ? patrolConfigModel.dailyVisibilityPatrolLimit == undefined ? null : patrolConfigModel.dailyVisibilityPatrolLimit : null;
        this.divisionId = patrolConfigModel ? patrolConfigModel.divisionId == undefined ? '' : patrolConfigModel.divisionId : '';
        this.generalPatrolCommunicationWindowEnd = patrolConfigModel ? patrolConfigModel.generalPatrolCommunicationWindowEnd == undefined ? '' : patrolConfigModel.generalPatrolCommunicationWindowEnd : '';
        this.generalPatrolCommunicationWindowStart = patrolConfigModel ? patrolConfigModel.generalPatrolCommunicationWindowStart == undefined ? '' : patrolConfigModel.generalPatrolCommunicationWindowStart : '';
        this.generalPatrolWindowEnd = patrolConfigModel ? patrolConfigModel.generalPatrolWindowEnd == undefined ? '' : patrolConfigModel.generalPatrolWindowEnd : '';
        this.generalPatrolWindowStart = patrolConfigModel ? patrolConfigModel.generalPatrolWindowStart == undefined ? '' : patrolConfigModel.generalPatrolWindowStart : '';
        this.holidayPatrolDailyLimit = patrolConfigModel ? patrolConfigModel.holidayPatrolDailyLimit == undefined ? null : patrolConfigModel.holidayPatrolDailyLimit : null;
        this.maxAllowedPaidPatrol = patrolConfigModel ? patrolConfigModel.maxAllowedPaidPatrol == undefined ? null : patrolConfigModel.maxAllowedPaidPatrol : null;
        this.maxHolidayCount = patrolConfigModel ? patrolConfigModel.maxHolidayCount == undefined ? null : patrolConfigModel.maxHolidayCount : null;
        this.maxHolidayPaidPatrolAllowed = patrolConfigModel ? patrolConfigModel.maxHolidayPaidPatrolAllowed == undefined ? null : patrolConfigModel.maxHolidayPaidPatrolAllowed : null;
        this.patrolConfigDescription = patrolConfigModel ? patrolConfigModel.patrolConfigDescription == undefined ? '' : patrolConfigModel.patrolConfigDescription : '';
        this.patrolConfigId = patrolConfigModel ? patrolConfigModel.patrolConfigId == undefined ? '' : patrolConfigModel.patrolConfigId : '';
        this.patrolConfigName = patrolConfigModel ? patrolConfigModel.patrolConfigName == undefined ? '' : patrolConfigModel.patrolConfigName : '';
        this.postIncidentDailyPatrolLimit = patrolConfigModel ? patrolConfigModel.postIncidentDailyPatrolLimit == undefined ? null : patrolConfigModel.postIncidentDailyPatrolLimit : null;
        this.postIncidentPatrolDays = patrolConfigModel ? patrolConfigModel.postIncidentPatrolDays == undefined ? null : patrolConfigModel.postIncidentPatrolDays : null;
        this.patrolConfigMainAreaDetails = patrolConfigModel ? patrolConfigModel.patrolConfigMainAreaDetails == undefined ? '' : patrolConfigModel.patrolConfigMainAreaDetails : '';
        
        this.visibilityPatrolMaxDays = patrolConfigModel ? patrolConfigModel.visibilityPatrolMaxDays == undefined ? null : patrolConfigModel.visibilityPatrolMaxDays : null;
        this.withoutArrivalDays = patrolConfigModel ? patrolConfigModel.withoutArrivalDays == undefined ? null : patrolConfigModel.withoutArrivalDays : null;
        this.isAutoPatrolLimitedToPatrolWindow = patrolConfigModel ? patrolConfigModel.isAutoPatrolLimitedToPatrolWindow == undefined ? false : patrolConfigModel.isAutoPatrolLimitedToPatrolWindow : false;
        this.isAutoPatrolRequireCommunication = patrolConfigModel ? patrolConfigModel.isAutoPatrolRequireCommunication == undefined ? false : patrolConfigModel.isAutoPatrolRequireCommunication : false;
        this.isHolidayPaidPatrolLimitedToPatrolWindow = patrolConfigModel ? patrolConfigModel.isHolidayPaidPatrolLimitedToPatrolWindow == undefined ? false : patrolConfigModel.isHolidayPaidPatrolLimitedToPatrolWindow : false;
        this.isHolidayPaidPatrolRequireCommunication = patrolConfigModel ? patrolConfigModel.isHolidayPaidPatrolRequireCommunication == undefined ? false : patrolConfigModel.isHolidayPaidPatrolRequireCommunication : false;
        this.isHolidayPatrolLimitedToPatrolWindow = patrolConfigModel ? patrolConfigModel.isHolidayPatrolLimitedToPatrolWindow == undefined ? false : patrolConfigModel.isHolidayPatrolLimitedToPatrolWindow : false;
        this.isHolidayPatrolRequireCommunication = patrolConfigModel ? patrolConfigModel.isHolidayPatrolRequireCommunication == undefined ? false : patrolConfigModel.isHolidayPatrolRequireCommunication : false;
        this.isPaidPatrolLimitedToPatrolWindow = patrolConfigModel ? patrolConfigModel.isPaidPatrolLimitedToPatrolWindow == undefined ? false : patrolConfigModel.isPaidPatrolLimitedToPatrolWindow : false;
        this.isPaidPatrolRequireCommunication = patrolConfigModel ? patrolConfigModel.isPaidPatrolRequireCommunication == undefined ? false : patrolConfigModel.isPaidPatrolRequireCommunication : false;
        this.isPostIncidentPatrolRequireCommunication = patrolConfigModel ? patrolConfigModel.isPostIncidentPatrolRequireCommunication == undefined ? false : patrolConfigModel.isPostIncidentPatrolRequireCommunication : false;
        this.isPostIncidentLimitedToPatrolWindow = patrolConfigModel ? patrolConfigModel.isPostIncidentLimitedToPatrolWindow == undefined ? false : patrolConfigModel.isPostIncidentLimitedToPatrolWindow : false;
       
        this.isStatus = patrolConfigModel ? patrolConfigModel.isStatus == undefined ? false : patrolConfigModel.isStatus : false;
        this.isVisibilityPatrolLimitedToPatrolWindow = patrolConfigModel ? patrolConfigModel.isVisibilityPatrolLimitedToPatrolWindow == undefined ? false : patrolConfigModel.isVisibilityPatrolLimitedToPatrolWindow : false;
        this.isVisibilityPatrolRequireCommunication = patrolConfigModel ? patrolConfigModel.isVisibilityPatrolRequireCommunication == undefined ? false : patrolConfigModel.isWelcomePatrolLimitedToPatrolWindow : false;
        this.isWelcomePatrolLimitedToPatrolWindow = patrolConfigModel ? patrolConfigModel.isWelcomePatrolLimitedToPatrolWindow == undefined ? false : patrolConfigModel.isStatus : false;
        this.isWelcomePatrolRequireCommunication = patrolConfigModel ? patrolConfigModel.isWelcomePatrolRequireCommunication == undefined ? false : patrolConfigModel.isWelcomePatrolRequireCommunication : false;       
    }
    createdUserId?: string;
    dailyLimit?:Number;
    patrolConfigMainAreaDetails?:string;
    dailyVisibilityPatrolLimit?:Number;
    divisionId?:string;
    generalPatrolCommunicationWindowEnd?:string;
    generalPatrolCommunicationWindowStart?:string;
    generalPatrolWindowEnd?:string;
    generalPatrolWindowStart?:string;
    holidayPatrolDailyLimit?:Number;
    maxAllowedPaidPatrol?:Number;
    maxHolidayCount?:Number;
    maxHolidayPaidPatrolAllowed?:Number;
    patrolConfigDescription?:string;
    patrolConfigId?:string;
    patrolConfigName?:string;
    postIncidentDailyPatrolLimit?:Number;
    postIncidentPatrolDays?:Number;
    visibilityPatrolMaxDays?:Number;
    withoutArrivalDays?:Number;
    isAutoPatrolLimitedToPatrolWindow?:boolean;
    isAutoPatrolRequireCommunication?:boolean;
    isHolidayPaidPatrolLimitedToPatrolWindow?:boolean;
    isHolidayPaidPatrolRequireCommunication?:boolean;
    isHolidayPatrolLimitedToPatrolWindow?:boolean;
    isHolidayPatrolRequireCommunication?:boolean;
    isPaidPatrolLimitedToPatrolWindow?:boolean;
    isPaidPatrolRequireCommunication?:boolean;
    isPostIncidentLimitedToPatrolWindow?:boolean;
    isPostIncidentPatrolRequireCommunication?:boolean;
    isStatus?:boolean;
    isVisibilityPatrolLimitedToPatrolWindow?:boolean;
    isVisibilityPatrolRequireCommunication?:boolean;
    isWelcomePatrolLimitedToPatrolWindow?:boolean;
    isWelcomePatrolRequireCommunication?:boolean;


}

class FailToTestDetails {
    newDecoderOnSiteDelay?:string;
    failToTestId?:string;
    alarmTypId?:string;
    startTime?:string;
    endTime?:string;
    isActive?:boolean;
    createdDate?:string;
    modifiedDate?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    createdUserName?:string;
    modifiedUserName?:string;

}

export { FailToTestModel,PatrolConfigModel, FailToTestDetails}
