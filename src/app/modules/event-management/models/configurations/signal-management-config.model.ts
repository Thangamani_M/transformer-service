class SignalManagementConfigModel {
    constructor(signalManagementConfigModel?: SignalManagementConfigModel) {
        this.createdUserId = signalManagementConfigModel ? signalManagementConfigModel.createdUserId == undefined ? '' : signalManagementConfigModel.createdUserId : '';
        this.arrivalThresholdCountIn28Day = signalManagementConfigModel ? signalManagementConfigModel.arrivalThresholdCountIn28Day == undefined ? null: signalManagementConfigModel.arrivalThresholdCountIn28Day : null;
        this.newCustomerDelayDays = signalManagementConfigModel ? signalManagementConfigModel.newCustomerDelayDays == undefined ? null: signalManagementConfigModel.newCustomerDelayDays : null;
        this.signalManagementConfigId = signalManagementConfigModel ? signalManagementConfigModel.signalManagementConfigId == undefined ? '' : signalManagementConfigModel.signalManagementConfigId : '';

        this.divisionId = signalManagementConfigModel ? signalManagementConfigModel.divisionId == undefined ? '' : signalManagementConfigModel.divisionId : '';

        this.individualWithArrivalDays = signalManagementConfigModel ? signalManagementConfigModel.individualWithArrivalDays == undefined ? null: signalManagementConfigModel.individualWithArrivalDays : null;
        this.installationIncubationDays = signalManagementConfigModel ? signalManagementConfigModel.installationIncubationDays == undefined ? null: signalManagementConfigModel.installationIncubationDays : null;
        this.isInstallationIncubation = signalManagementConfigModel ? signalManagementConfigModel.isInstallationIncubation == undefined ? true : signalManagementConfigModel.isInstallationIncubation : true;
        this.installationEscalationAmount = signalManagementConfigModel ? signalManagementConfigModel.installationEscalationAmount == undefined ? null: signalManagementConfigModel.installationEscalationAmount : null;
        this.serviceArrivalCount = signalManagementConfigModel ? signalManagementConfigModel.serviceArrivalCount == undefined ? null: signalManagementConfigModel.serviceArrivalCount : null;
        this.installationSuspendPeriodStartDate = signalManagementConfigModel ? signalManagementConfigModel.installationSuspendPeriodStartDate == undefined ? '' : signalManagementConfigModel.installationSuspendPeriodStartDate : '';
        this.installationSuspendPeriodEndDate = signalManagementConfigModel ? signalManagementConfigModel.installationSuspendPeriodEndDate == undefined ? '' : signalManagementConfigModel.installationSuspendPeriodEndDate : '';
        this.serviceIncubationDays = signalManagementConfigModel ? signalManagementConfigModel.serviceIncubationDays == undefined ? null: signalManagementConfigModel.serviceIncubationDays : null;
        this.isServiceIncubation = signalManagementConfigModel ? signalManagementConfigModel.isServiceIncubation == undefined ? false : signalManagementConfigModel.isServiceIncubation : false;
        this.serviceSuspendPeriodStartDate = signalManagementConfigModel ? signalManagementConfigModel.serviceSuspendPeriodStartDate == undefined ? '' : signalManagementConfigModel.serviceSuspendPeriodStartDate : '';
        this.serviceSuspendPeriodEndDate = signalManagementConfigModel ? signalManagementConfigModel.serviceSuspendPeriodEndDate == undefined ? '' : signalManagementConfigModel.serviceSuspendPeriodEndDate : '';
        this.isActive = signalManagementConfigModel ? signalManagementConfigModel.isActive == undefined ? true : signalManagementConfigModel.isActive : true;
        this.signalManagementResolutionConfigsList = signalManagementConfigModel ? signalManagementConfigModel.signalManagementResolutionConfigsList == undefined ? null : signalManagementConfigModel.signalManagementResolutionConfigsList : null;
        this.signalManagementERBThresholdConfigsList = signalManagementConfigModel ? signalManagementConfigModel.signalManagementERBThresholdConfigsList == undefined ? null : signalManagementConfigModel.signalManagementERBThresholdConfigsList : null;

        this.modifiedUserId = signalManagementConfigModel ? signalManagementConfigModel.modifiedUserId == undefined ? '' : signalManagementConfigModel.modifiedUserId : '';

    }
    signalManagementConfigId?:string;
    modifiedUserId?:string;
    createdUserId?: string;
    arrivalThresholdCountIn28Day?:Number;
    newCustomerDelayDays?:Number;
    divisionId?: string;
    individualWithArrivalDays?: Number;
    installationIncubationDays?: Number;
    isInstallationIncubation?:boolean;
    installationEscalationAmount?:Number;
    serviceArrivalCount: Number;
    installationSuspendPeriodStartDate?:string;
    installationSuspendPeriodEndDate?:string;
    serviceIncubationDays?:Number;
    isServiceIncubation?:boolean;
    serviceSuspendPeriodStartDate?:string;
    serviceSuspendPeriodEndDate?:string;
    isActive?: boolean;
    signalManagementResolutionConfigsList:SignalManagementResolutionConfigsListModel[];
    signalManagementERBThresholdConfigsList:SignalManagementERBThresholdConfigsListModel[];
}

class SignalManagementResolutionConfigsListModel {
    constructor(signalManagementResolutionConfigsListModel?: SignalManagementResolutionConfigsListModel) {
        this.categoryName = signalManagementResolutionConfigsListModel ? signalManagementResolutionConfigsListModel.categoryName == undefined ? '' : signalManagementResolutionConfigsListModel.categoryName : '';
        this.categoryDescription = signalManagementResolutionConfigsListModel ? signalManagementResolutionConfigsListModel.categoryDescription == undefined ? '' : signalManagementResolutionConfigsListModel.categoryDescription : '';
        this.signalManagementResolutionConfigId = signalManagementResolutionConfigsListModel ? signalManagementResolutionConfigsListModel.signalManagementResolutionConfigId == undefined ? '' : signalManagementResolutionConfigsListModel.signalManagementResolutionConfigId : '';
    }
    categoryName?:string;
    categoryDescription?:string;
    signalManagementResolutionConfigId?:string;
}

class SignalManagementERBThresholdConfigsListModel {
    constructor(signalManagementERBThresholdConfigsListModel?: SignalManagementERBThresholdConfigsListModel) {
        this.arrivalThresholdFrom = signalManagementERBThresholdConfigsListModel ? signalManagementERBThresholdConfigsListModel.arrivalThresholdFrom == undefined ? null: signalManagementERBThresholdConfigsListModel.arrivalThresholdFrom : null;
        this.arrivalThresholdTo = signalManagementERBThresholdConfigsListModel ? signalManagementERBThresholdConfigsListModel.arrivalThresholdTo == undefined ? null: signalManagementERBThresholdConfigsListModel.arrivalThresholdTo : null;
        this.thresholdIncludeVATAmount = signalManagementERBThresholdConfigsListModel ? signalManagementERBThresholdConfigsListModel.thresholdIncludeVATAmount == undefined ? null: signalManagementERBThresholdConfigsListModel.thresholdIncludeVATAmount : null;
        this.thresholdExcludeVATAmount = signalManagementERBThresholdConfigsListModel ? signalManagementERBThresholdConfigsListModel.thresholdExcludeVATAmount == undefined ? null : signalManagementERBThresholdConfigsListModel.thresholdExcludeVATAmount : null;
        this.isNotifyClient = signalManagementERBThresholdConfigsListModel ? signalManagementERBThresholdConfigsListModel.isNotifyClient == undefined ? true : signalManagementERBThresholdConfigsListModel.isNotifyClient : true;
        this.isRequireNotification = signalManagementERBThresholdConfigsListModel ? signalManagementERBThresholdConfigsListModel.isRequireNotification == undefined ? true : signalManagementERBThresholdConfigsListModel.isRequireNotification : true;
        this.signalManagementERBThresholdConfigId = signalManagementERBThresholdConfigsListModel ? signalManagementERBThresholdConfigsListModel.signalManagementERBThresholdConfigId == undefined ? '': signalManagementERBThresholdConfigsListModel.signalManagementERBThresholdConfigId : '';
        this.taxId = signalManagementERBThresholdConfigsListModel ? signalManagementERBThresholdConfigsListModel.taxId == undefined ? 49: signalManagementERBThresholdConfigsListModel.taxId : 49;

    }
    arrivalThresholdFrom?:Number;
    arrivalThresholdTo?:Number;
    thresholdIncludeVATAmount?:Number;
    thresholdExcludeVATAmount?:Number;
    isNotifyClient?: boolean;
    isRequireNotification?:boolean;
    taxId?:Number;
    signalManagementERBThresholdConfigId?:string;
}

class SignalManagementConfigDetails {
  arrivalThresholdCountIn28Day?:string;
  createdDate?:string;
  createdUserId?:string;
  createdUserName?:string;
  divisionId?:string;
  divisionName?:string;
  individualWithArrivalDays?:string;
 installationEscalationAmount?:string;
 installationIncubationDays?:string;
 installationSuspendPeriodEndDate?:string;
 installationSuspendPeriodStartDate?:string;
 isActive?:boolean;
 isInstallationIncubation?:boolean;
 isServiceIncubation?:boolean;
 modifiedDate?:string;
 modifiedUserId?:string;
 modifiedUserName?:string;
 newCustomerDelayDays?:string;
 serviceIncubationDays?:string;
 serviceSuspendPeriodEndDate?:string;
 serviceSuspendPeriodStartDate?:string;
 signalManagementConfigId?:string;
 signalManagementERBThresholdConfigs?:string;
 signalManagementERBThresholdConfigsList?:string[];
 signalManagementResolutionConfigs?:string;
 signalManagementResolutionConfigsList?:string[];
}
export { SignalManagementConfigModel,SignalManagementConfigDetails, SignalManagementResolutionConfigsListModel, SignalManagementERBThresholdConfigsListModel }
