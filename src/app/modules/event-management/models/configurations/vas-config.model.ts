class VASConfigModel {
    constructor(vASConfigModel?: VASConfigModel) {
        this.vasConfigId = vASConfigModel ? vASConfigModel.vasConfigId == undefined ? '' : vASConfigModel.vasConfigId : '';
        this.vASConfigAlarmTypesList = vASConfigModel ? vASConfigModel.vASConfigAlarmTypesList == undefined ? [] : vASConfigModel.vASConfigAlarmTypesList : [];
        this.groupName = vASConfigModel ? vASConfigModel.groupName == undefined ? "" : vASConfigModel.groupName : "";
    
        this.feedbackDelay = vASConfigModel ? vASConfigModel.feedbackDelay == undefined ? null : vASConfigModel.feedbackDelay : null;
        this.vASConfigDivisionsList = vASConfigModel ? vASConfigModel.vASConfigDivisionsList == undefined ? null : vASConfigModel.vASConfigDivisionsList : null;
        this.vASConfigTimeDelaysList = vASConfigModel ? vASConfigModel.vASConfigTimeDelaysList == undefined ? null : vASConfigModel.vASConfigTimeDelaysList : null;
        this.modifiedUserId = vASConfigModel ? vASConfigModel.modifiedUserId == undefined ? "" : vASConfigModel.modifiedUserId : "";
        this.createdUserId = vASConfigModel ? vASConfigModel.createdUserId == undefined ? null : vASConfigModel.createdUserId : null;
        this.isActive = vASConfigModel ? vASConfigModel.isActive == undefined ? true : vASConfigModel.isActive : true;
    }
    vasConfigId?: string;
    modifiedUserId: string;
    vASConfigAlarmTypesList: VASConfigAlarmTypesListModel[];
    createdUserId: string;
    isActive: boolean;
    groupName: string;
    description: string;
    feedbackDelay: number;
    vASConfigDivisionsList: VASConfigDivisionModel[];
    vASConfigTimeDelaysList: VASConfigTimeDelaysListModel[];
    isEnabled: boolean;
}



class VASConfigAlarmTypesListModel {
    constructor(vASConfigAlarmTypesListModel?: VASConfigAlarmTypesListModel) {
        this.vasConfigAlarmTypeIdDummy = vASConfigAlarmTypesListModel ? vASConfigAlarmTypesListModel.vasConfigAlarmTypeIdDummy == undefined ? '' : vASConfigAlarmTypesListModel.vasConfigAlarmTypeIdDummy : '';
        this.alarmTypeId = vASConfigAlarmTypesListModel ? vASConfigAlarmTypesListModel.alarmTypeId == undefined ? '' : vASConfigAlarmTypesListModel.alarmTypeId : '';
        this.alarmDescription = vASConfigAlarmTypesListModel ? vASConfigAlarmTypesListModel.alarmDescription == undefined ? null : vASConfigAlarmTypesListModel.alarmDescription : null;
    }
    vasConfigAlarmTypeIdDummy?: string;
    alarmTypeId: string;
    alarmDescription: string;

}

class VASConfigTimeDelaysListModel {
    constructor(vASConfigTimeDelaysListModel?: VASConfigTimeDelaysListModel) {
        this.vasConfigTimeDelayIdDummy = vASConfigTimeDelaysListModel ? vASConfigTimeDelaysListModel.vasConfigTimeDelayIdDummy == undefined ? '' : vASConfigTimeDelaysListModel.vasConfigTimeDelayIdDummy : '';
        this.vasConfigDayId = vASConfigTimeDelaysListModel ? vASConfigTimeDelaysListModel.vasConfigDayId == undefined ? '' : vASConfigTimeDelaysListModel.vasConfigDayId : '';
        this.startTime = vASConfigTimeDelaysListModel ? vASConfigTimeDelaysListModel.startTime == undefined ? null : vASConfigTimeDelaysListModel.startTime : null;
        this.endTime = vASConfigTimeDelaysListModel ? vASConfigTimeDelaysListModel.endTime == undefined ? null : vASConfigTimeDelaysListModel.endTime : null;
        this.delayTime = vASConfigTimeDelaysListModel ? vASConfigTimeDelaysListModel.delayTime == undefined ? null : vASConfigTimeDelaysListModel.delayTime : null;
    }
    vasConfigTimeDelayIdDummy?: string;
    vasConfigDayId: string;
    startTime: string;
    endTime: string;
    delayTime: string;

}


class VASConfigDivisionModel {constructor(vASConfigDivisionModel?: VASConfigDivisionModel) {
    this.divisionId = vASConfigDivisionModel ? vASConfigDivisionModel.divisionId == undefined ? '' : vASConfigDivisionModel.divisionId : '';
    }
    divisionId: string
}

export { VASConfigModel, VASConfigAlarmTypesListModel,VASConfigTimeDelaysListModel }
