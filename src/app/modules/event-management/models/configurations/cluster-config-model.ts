class ClusterConfigModel {
    constructor(clusterConfigModel?: ClusterConfigModel) {
        this.clusterConfigId = clusterConfigModel ? clusterConfigModel.clusterConfigId == undefined ? '' : clusterConfigModel.clusterConfigId : '';
        this.modifiedUserId = clusterConfigModel ? clusterConfigModel.modifiedUserId == undefined ? "" : clusterConfigModel.modifiedUserId : "";
        this.clusterConfigSubAreaList = clusterConfigModel ? clusterConfigModel.clusterConfigSubAreaList == undefined ? [] : clusterConfigModel.clusterConfigSubAreaList : [];
        this.createdUserId = clusterConfigModel ? clusterConfigModel.createdUserId == undefined ? null : clusterConfigModel.createdUserId : null;
        this.isActive = clusterConfigModel ? clusterConfigModel.isActive == undefined ? true : clusterConfigModel.isActive : true;
        this.clusterName = clusterConfigModel ? clusterConfigModel.clusterName == undefined ? "" : clusterConfigModel.clusterName : "";
        this.mainAreaId = clusterConfigModel ? clusterConfigModel.mainAreaId == undefined ? "" : clusterConfigModel.mainAreaId : "";
        this.divisionId = clusterConfigModel ? clusterConfigModel.divisionId == undefined ? "" : clusterConfigModel.divisionId : "";
        this.isEnabled = clusterConfigModel ? clusterConfigModel.isEnabled == undefined ? true : clusterConfigModel.isEnabled : true;

    }
    clusterConfigId?: string;
    modifiedUserId: string;
    clusterConfigSubAreaList: ClusterConfigSubAreaListModel[]; 
    createdUserId: string;
    isActive: boolean;
    clusterName:string;
    mainAreaId:string;
    divisionId:string;
    isEnabled:boolean;
}

  

class ClusterConfigSubAreaListModel {
    constructor(clusterConfigSubAreaListModel?: ClusterConfigSubAreaListModel) {
        this.subAreaId = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.subAreaId == undefined ? '' : clusterConfigSubAreaListModel.subAreaId : '';
        this.vehicleCategoryId = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.vehicleCategoryId == undefined ? '' : clusterConfigSubAreaListModel.vehicleCategoryId : '';
        this.clusterConfigSubAreaId = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.clusterConfigSubAreaId == undefined ? null : clusterConfigSubAreaListModel.clusterConfigSubAreaId : null;
        this.subArea = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.subArea == undefined ? '' : clusterConfigSubAreaListModel.subArea : '';
        this.vehicleCategoryName = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.vehicleCategoryName == undefined ? '' : clusterConfigSubAreaListModel.vehicleCategoryName : '';
        this.dispatchModeId = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.dispatchModeId == undefined ? '' : clusterConfigSubAreaListModel.dispatchModeId : '';

    }
    subAreaId:string;
    vehicleCategoryId:string;
    clusterConfigSubAreaId:null;
    subArea:string;
    vehicleCategoryName:string;
    dispatchModeId?:string;
}


class ClusterConfigDetails {
    clusterConfigId:string;
    clusterName:string;
    divisionId: string;
    mainAreaId: string;
    isEnabled: boolean;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    clusterConfigSubAreaList:string[];
    clusterConfigSubAreas:string;
    divisionName:string;
}

export { ClusterConfigDetails, ClusterConfigModel,  ClusterConfigSubAreaListModel}
