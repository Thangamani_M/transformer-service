class DelayToDurationModel {
    constructor(delayToDurationModel?: DelayToDurationModel) {
        this.delayDurationId = delayToDurationModel ? delayToDurationModel.delayDurationId == undefined ? '' : delayToDurationModel.delayDurationId : '';
        this.divisionId = delayToDurationModel ? delayToDurationModel.divisionId == undefined ? '' : delayToDurationModel.divisionId : '';
        this.maxDuration = delayToDurationModel ? delayToDurationModel.maxDuration == undefined ? '' : delayToDurationModel.maxDuration : '';
        this.createdUserId = delayToDurationModel ? delayToDurationModel.createdUserId == undefined ? null : delayToDurationModel.createdUserId : null;
        this.modifiedUserId = delayToDurationModel ? delayToDurationModel.modifiedUserId == undefined ? null : delayToDurationModel.modifiedUserId : null;
        this.isActive = delayToDurationModel ? delayToDurationModel.isActive == undefined ? true : delayToDurationModel.isActive : true;
    }
    delayDurationId?: string;
    divisionId: string;
    maxDuration: string;
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}
export { DelayToDurationModel }