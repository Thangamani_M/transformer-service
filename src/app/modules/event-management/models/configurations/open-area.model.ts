class OpenAreaConfigModel {
    constructor(openAreaConfigModel?: OpenAreaConfigModel) {
        this.openAreaReasonId = openAreaConfigModel ? openAreaConfigModel.openAreaReasonId == undefined ? null : openAreaConfigModel.openAreaReasonId : null;
        this.createdUserId = openAreaConfigModel ? openAreaConfigModel.createdUserId == undefined ? '' : openAreaConfigModel.createdUserId : '';
        this.openAreaReason = openAreaConfigModel ? openAreaConfigModel.openAreaReason == undefined ? '' : openAreaConfigModel.openAreaReason : '';
        this.isActive = openAreaConfigModel ? openAreaConfigModel.isActive == undefined ? true : openAreaConfigModel.isActive : true;
        this.description = openAreaConfigModel ? openAreaConfigModel.description == undefined ? '' : openAreaConfigModel.description : '';
        this.modifiedUserId = openAreaConfigModel ? openAreaConfigModel.modifiedUserId == undefined ? '' : openAreaConfigModel.modifiedUserId : '';
    }
    openAreaReasonId: number;
    createdUserId?: string;
    openAreaReason: string;
    description: string;
    isActive:boolean;
    modifiedUserId:string;
}
export { OpenAreaConfigModel }