class DeclineSetupModel {
    constructor(declineSetupModel?: DeclineSetupModel) {
        this.declineSetupId = declineSetupModel ? declineSetupModel.declineSetupId == undefined ? '' : declineSetupModel.declineSetupId : '';
        this.modifiedUserId = declineSetupModel ? declineSetupModel.modifiedUserId == undefined ? "" : declineSetupModel.modifiedUserId : "";
        this.declineSetupReasonList = declineSetupModel ? declineSetupModel.declineSetupReasonList == undefined ? [] : declineSetupModel.declineSetupReasonList : [];
        this.createdUserId = declineSetupModel ? declineSetupModel.createdUserId == undefined ? null : declineSetupModel.createdUserId : null;
        this.isActive = declineSetupModel ? declineSetupModel.isActive == undefined ? true : declineSetupModel.isActive : true;
        this.declineSetupName = declineSetupModel ? declineSetupModel.declineSetupName == undefined ? "" : declineSetupModel.declineSetupName : "";
        this.description = declineSetupModel ? declineSetupModel.description == undefined ? "" : declineSetupModel.description : "";
        this.declineSetupDivisionsList = declineSetupModel ? declineSetupModel.declineSetupDivisionsList == undefined ? null : declineSetupModel.declineSetupDivisionsList : null;
        this.delayTime = declineSetupModel ? declineSetupModel.delayTime == undefined ? "" : declineSetupModel.delayTime : "";
        this.desription = declineSetupModel ? declineSetupModel.desription == undefined ? "" : declineSetupModel.desription : "";

    }
    declineSetupId?: string;
    modifiedUserId: string;
    declineSetupReasonList: DeclineSetupReasonListModel[]; 
    createdUserId: string;
    isActive: boolean;
    declineSetupName:string;
    description:string;
    declineSetupDivisionsList:DeclineSetupDivisionModel[];
    isEnabled:boolean;
    delayTime:string;
    desription:string;
}

  

class DeclineSetupReasonListModel {
    constructor(declineSetupReasonListModel?: DeclineSetupReasonListModel) {
        this.declineSetupReasonListName = declineSetupReasonListModel ? declineSetupReasonListModel.declineSetupReasonListName == undefined ? '' : declineSetupReasonListModel.declineSetupReasonListName : '';
        this.DeclineSetupReasonListId = declineSetupReasonListModel ? declineSetupReasonListModel.DeclineSetupReasonListId == undefined ? 0 : declineSetupReasonListModel.DeclineSetupReasonListId : 0;
        this.colorCode = declineSetupReasonListModel ? declineSetupReasonListModel.colorCode == undefined ? '' : declineSetupReasonListModel.colorCode : '';

    }
    declineSetupReasonListName:string;
    DeclineSetupReasonListId:Number;
    colorCode:string;
}


class DeclineSetupDivisionModel {
     divisionId:string
}

export { DeclineSetupModel,DeclineSetupReasonListModel}
