class IncidentModel {
    constructor(incidentModel?: IncidentModel) {
        this.incidentId = incidentModel ? incidentModel.incidentId == undefined ? '' : incidentModel.incidentId : '';
        this.modifiedUserId = incidentModel ? incidentModel.modifiedUserId == undefined ? "" : incidentModel.modifiedUserId : "";
        this.incidentTypesList = incidentModel ? incidentModel.incidentTypesList == undefined ? [] : incidentModel.incidentTypesList : [];
        this.createdUserId = incidentModel ? incidentModel.createdUserId == undefined ? null : incidentModel.createdUserId : null;
        this.isActive = incidentModel ? incidentModel.isActive == undefined ? true : incidentModel.isActive : true;
        this.incidentName = incidentModel ? incidentModel.incidentName == undefined ? "" : incidentModel.incidentName : "";
        this.description = incidentModel ? incidentModel.description == undefined ? "" : incidentModel.description : "";
        this.incidentDivisionsList = incidentModel ? incidentModel.incidentDivisionsList == undefined ? null : incidentModel.incidentDivisionsList : null;

    }
    incidentId?: string;
    modifiedUserId: string;
    incidentTypesList: IncidentTypesListModel[]; 
    createdUserId: string;
    isActive: boolean;
    incidentName:string;
    description:string;
    incidentDivisionsList:IncidentDivisionModel[];
    isEnabled:boolean;
}

  

class IncidentTypesListModel {
    constructor(incidentTypesListModel?: IncidentTypesListModel) {
        this.incidentTypeDummyId = incidentTypesListModel ? incidentTypesListModel.incidentTypeDummyId == undefined ? '' : incidentTypesListModel.incidentTypeDummyId : '';
        this.eventTypeId = incidentTypesListModel ? incidentTypesListModel.eventTypeId == undefined ? '' : incidentTypesListModel.eventTypeId : '';
        this.incidentTypeId = incidentTypesListModel ? incidentTypesListModel.incidentTypeId == undefined ? 0 : incidentTypesListModel.incidentTypeId : 0;
        this.colorCode = incidentTypesListModel ? incidentTypesListModel.colorCode == undefined ? '' : incidentTypesListModel.colorCode : '';

    }
    incidentTypeDummyId?:string;
    eventTypeId:string;
    incidentTypeId:Number;
    colorCode:string;
}


class IncidentDivisionModel {
     divisionId:string
}

export { IncidentModel,IncidentTypesListModel}
