class TempSuspendSignalModel {
    constructor(tempSuspendSignalModel?: TempSuspendSignalModel) {
        this.createdUserId = tempSuspendSignalModel ? tempSuspendSignalModel.createdUserId == undefined ? '' : tempSuspendSignalModel.createdUserId : '';
        this.customerId = tempSuspendSignalModel ? tempSuspendSignalModel.customerId == undefined ? '' : tempSuspendSignalModel.customerId : '';
        this.customerAddressId = tempSuspendSignalModel ? tempSuspendSignalModel.customerAddressId == undefined ? '' : tempSuspendSignalModel.customerAddressId : '';
        this.toDate = tempSuspendSignalModel ? tempSuspendSignalModel.toDate == undefined ? '' : tempSuspendSignalModel.toDate : '';
        this.occurrenceBookSignals = tempSuspendSignalModel ? tempSuspendSignalModel.occurrenceBookSignals == undefined ? [] : tempSuspendSignalModel.occurrenceBookSignals : [];
    }
    createdUserId?:string;
    customerId:string;
    customerAddressId:string;
    toDate:string;
    occurrenceBookSignals:any[];
  
}

export { TempSuspendSignalModel}
