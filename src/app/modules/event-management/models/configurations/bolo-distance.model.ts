class BoloDistanceModel {
    constructor(boloDistanceModel?: BoloDistanceModel) {
        this.boloDistanceId = boloDistanceModel ? boloDistanceModel.boloDistanceId == undefined ? '' : boloDistanceModel.boloDistanceId : '';
        this.boloDistanceName = boloDistanceModel ? boloDistanceModel.boloDistanceName == undefined ? '' : boloDistanceModel.boloDistanceName : '';
        this.boloDistanceDivisionList = boloDistanceModel ? boloDistanceModel.boloDistanceDivisionList == undefined ? null : boloDistanceModel.boloDistanceDivisionList : null;
        this.branchId = boloDistanceModel ? boloDistanceModel.branchId == undefined ? '' : boloDistanceModel.branchId : '';
        this.distance = boloDistanceModel ? boloDistanceModel.distance == undefined ? null : boloDistanceModel.distance : null;
        this.createdUserId = boloDistanceModel ? boloDistanceModel.createdUserId == undefined ? null : boloDistanceModel.createdUserId : null;
        this.modifiedUserId = boloDistanceModel ? boloDistanceModel.modifiedUserId == undefined ? null : boloDistanceModel.modifiedUserId : null;
        this.isActive = boloDistanceModel ? boloDistanceModel.isActive == undefined ? true : boloDistanceModel.isActive : true;
    }
    boloDistanceId?: string;
    boloDistanceName:string;
    boloDistanceDivisionList: DivisionsModel[];
    branchId: string;
    distance: string;
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}

class DivisionsModel {
    divisionId: string;
}


export { BoloDistanceModel }