class StatsConfigModel {
    constructor(declineSetupModel?: StatsConfigModel) {
        this.statsConfigId = declineSetupModel ? declineSetupModel.statsConfigId == undefined ? '' : declineSetupModel.statsConfigId : '';
        this.modifiedUserId = declineSetupModel ? declineSetupModel.modifiedUserId == undefined ? "" : declineSetupModel.modifiedUserId : "";
        this.createdUserId = declineSetupModel ? declineSetupModel.createdUserId == undefined ? null : declineSetupModel.createdUserId : null;
        this.isActive = declineSetupModel ? declineSetupModel.isActive == undefined ? true : declineSetupModel.isActive : true;
        this.groupName = declineSetupModel ? declineSetupModel.groupName == undefined ? "" : declineSetupModel.groupName : "";
        this.description = declineSetupModel ? declineSetupModel.description == undefined ? "" : declineSetupModel.description : "";
        this.statsConfigAlarmTypesList = declineSetupModel ? declineSetupModel.statsConfigAlarmTypesList == undefined ? null : declineSetupModel.statsConfigAlarmTypesList : null;
        this.statsConfigEventTypesList = declineSetupModel ? declineSetupModel.statsConfigEventTypesList == undefined ? null : declineSetupModel.statsConfigEventTypesList : null;

    }
    statsConfigId?: string;
    modifiedUserId: string;
    createdUserId: string;
    isActive: boolean;
    groupName:string;
    description:string;
    statsConfigAlarmTypesList:StatsConfigAlarmTypeModel[];
    statsConfigEventTypesList:StatsConfigEventTypeModel[];
}

class StatsConfigAlarmTypeModel {
    alarmTypeId:string
}

class StatsConfigEventTypeModel {
    eventTypeId:string
}

class StatsConfigDetails {
    statsConfigId?: string;
    modifiedUserId?: string;
    createdUserId?: string;
    modifiedUserName?: string;
    createdUserName?: string;
    isActive?: boolean;
    groupName?:string;
    description?:string;
    statsConfigAlarmTypes?: string;
    statsConfigEventTypes?:string;
}

export { StatsConfigModel, StatsConfigDetails}
