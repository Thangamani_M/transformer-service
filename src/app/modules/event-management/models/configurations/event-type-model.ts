class EventTypeModel {
    constructor(eventTypeModel?: EventTypeModel) {
        this.eventTypeId = eventTypeModel ? eventTypeModel.eventTypeId == undefined ? '' : eventTypeModel.eventTypeId : '';
        this.createdUserId = eventTypeModel ? eventTypeModel.createdUserId == undefined ? '' : eventTypeModel.createdUserId : '';
        this.modifiedUserId = eventTypeModel ? eventTypeModel.modifiedUserId == undefined ? '' : eventTypeModel.modifiedUserId : '';
        this.incidentName = eventTypeModel ? eventTypeModel.incidentName == undefined ? '' : eventTypeModel.incidentName : '';
        this.description = eventTypeModel ? eventTypeModel.description == undefined ? '' : eventTypeModel.description : '';

        this.isActive = eventTypeModel ? eventTypeModel.isActive == undefined ? true : eventTypeModel.isActive : true;
        this.isEnabledFlag = eventTypeModel ? eventTypeModel.isEnabledFlag == undefined ? true : eventTypeModel.isEnabledFlag : true;
        this.isHoldForFinish = eventTypeModel ? eventTypeModel.isHoldForFinish == undefined ? false : eventTypeModel.isHoldForFinish : false;
        this.isRequiresFeedback = eventTypeModel ? eventTypeModel.isRequiresFeedback == undefined ? false : eventTypeModel.isRequiresFeedback : false;

    }
    eventTypeId?: string;
    isActive?: boolean;
    modifiedUserId?:string;
    createdUserId?:string;
    incidentName?:string;
    description?:string;
    isEnabledFlag?:boolean;
    isHoldForFinish?:boolean;
    isRequiresFeedback?:boolean;
}  

class EventTypeDetails {
    eventTypeId:string;
    incidentName:string;
    description: string;
    isActive: boolean;
    isEnabledFlag:boolean;
    isHoldForFinish:boolean;
    isRequiresFeedback:boolean;
}

export { EventTypeModel, EventTypeDetails }
