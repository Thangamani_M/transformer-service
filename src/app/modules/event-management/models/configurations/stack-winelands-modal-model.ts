class StackWinelandsModal{
    constructor(stackWinelandsModal?:StackWinelandsModal){
        this.stackAreaConfigId=stackWinelandsModal?stackWinelandsModal.stackAreaConfigId==undefined?'':stackWinelandsModal.stackAreaConfigId:'';
        this.stackConfigId=stackWinelandsModal?stackWinelandsModal.stackConfigId==undefined?'':stackWinelandsModal.stackConfigId:'';
         this.onHoldStack=stackWinelandsModal?stackWinelandsModal.onHoldStack==undefined? false:stackWinelandsModal.onHoldStack:false;
    }
    stackAreaConfigId: any;
    stackConfigId: string;
    onHoldStack: boolean;
}
export { StackWinelandsModal };

