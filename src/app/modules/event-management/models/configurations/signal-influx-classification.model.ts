class SignalInfluxClassificationAddEditModel {
    constructor(signalInfluxClassificationAddEditModel?: SignalInfluxClassificationAddEditModel) {
        this.signalInfluxClassificationTypeName = signalInfluxClassificationAddEditModel ? signalInfluxClassificationAddEditModel.signalInfluxClassificationTypeName == undefined ? '' : signalInfluxClassificationAddEditModel.signalInfluxClassificationTypeName : '';
        this.description = signalInfluxClassificationAddEditModel ? signalInfluxClassificationAddEditModel.description == undefined ? '' : signalInfluxClassificationAddEditModel.description : '';
        this.isActive = signalInfluxClassificationAddEditModel ? signalInfluxClassificationAddEditModel.isActive == undefined ? true : signalInfluxClassificationAddEditModel.isActive : true;
    }
    signalInfluxClassificationTypeName:string;
    description:string;
    isActive:boolean;
}

export {SignalInfluxClassificationAddEditModel};