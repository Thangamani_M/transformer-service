class ERBNotificationModel {
    constructor(erbNotificationModel?: ERBNotificationModel) {
        this.divisionId = erbNotificationModel ? erbNotificationModel.divisionId == undefined ? '' : erbNotificationModel.divisionId : '';
        this.erbQueryBuilderId = erbNotificationModel ? erbNotificationModel.erbQueryBuilderId == undefined ? '' : erbNotificationModel.erbQueryBuilderId : '';
        this.isCreated = erbNotificationModel ? erbNotificationModel.isCreated == undefined ? true : erbNotificationModel.isCreated : true;
        this.status = erbNotificationModel ? erbNotificationModel.status == undefined ? 'Not Created' : erbNotificationModel.status : 'Not Created';
        this.year = erbNotificationModel ? erbNotificationModel.year == undefined ? '' : erbNotificationModel.year : '';
        this.notificationMonthNumber = erbNotificationModel ? erbNotificationModel.notificationMonthNumber == undefined ? '' : erbNotificationModel.notificationMonthNumber : '';
        this.erbClientNotifyStatusId = erbNotificationModel ? erbNotificationModel.erbClientNotifyStatusId == undefined ? '' : erbNotificationModel.erbClientNotifyStatusId : '';
        this.createdUserId = erbNotificationModel ? erbNotificationModel.createdUserId == undefined ? '' : erbNotificationModel.createdUserId : '';
    }
    divisionId:string;
    erbQueryBuilderId:string;
    isCreated:boolean;
    year:string;
    status:string;
    notificationMonthNumber:string;
    erbClientNotifyStatusId:string;
    createdUserId:string;
}

export {ERBNotificationModel};