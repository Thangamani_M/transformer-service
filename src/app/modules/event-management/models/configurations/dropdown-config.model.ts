class DropDownConfigModel {
    constructor(dropDownConfigModel?: DropDownConfigModel) {
        this.callCategories = dropDownConfigModel ? dropDownConfigModel.callCategories == undefined ? [] : dropDownConfigModel.callCategories : [];
        this.resolutionCategories = dropDownConfigModel ? dropDownConfigModel.resolutionCategories == undefined ? [] : dropDownConfigModel.resolutionCategories : [];
        this.followUpReasons = dropDownConfigModel ? dropDownConfigModel.followUpReasons == undefined ? [] : dropDownConfigModel.followUpReasons : [];
    }

    callCategories: CallCategoryListModel[];
    resolutionCategories: ResolutionCategoryListModel[];
    followUpReasons: FollowUpCategoryListModel[];
}

class CallCategoryListModel {
    constructor(callCategoryListModel?: CallCategoryListModel) {
        this.callCategoryId = callCategoryListModel ? callCategoryListModel.callCategoryId == undefined ? '' : callCategoryListModel.callCategoryId : '';
        this.callCategoryName = callCategoryListModel ? callCategoryListModel.callCategoryName == undefined ? "" : callCategoryListModel.callCategoryName : "";
        this.description = callCategoryListModel ? callCategoryListModel.description == undefined ? "" : callCategoryListModel.description : "";
       
        this.isActive = callCategoryListModel ? callCategoryListModel.isActive == undefined ? true : callCategoryListModel.isActive : true;

    }
    callCategoryId: string;
    callCategoryName:string;
    description: string;
    isActive:boolean
}

class ResolutionCategoryListModel {
    constructor(callCategoryListModel?: ResolutionCategoryListModel) {
        this.resolutionCategoryId = callCategoryListModel ? callCategoryListModel.resolutionCategoryId == undefined ? '' : callCategoryListModel.resolutionCategoryId : '';
        this.resolutionCategoryName = callCategoryListModel ? callCategoryListModel.resolutionCategoryName == undefined ? "" : callCategoryListModel.resolutionCategoryName : "";
        this.description = callCategoryListModel ? callCategoryListModel.description == undefined ? "" : callCategoryListModel.description : "";
        this.isActive = callCategoryListModel ? callCategoryListModel.isActive == undefined ? true : callCategoryListModel.isActive : true;

    }
    resolutionCategoryId: string;
    resolutionCategoryName:string;
    description: string;
    isActive:boolean
}
class FollowUpCategoryListModel {
    constructor(callCategoryListModel?: FollowUpCategoryListModel) {
        this.followUpReasonId = callCategoryListModel ? callCategoryListModel.followUpReasonId == undefined ? '' : callCategoryListModel.followUpReasonId : '';
        this.followUpReasonName = callCategoryListModel ? callCategoryListModel.followUpReasonName == undefined ? "" : callCategoryListModel.followUpReasonName : "";
        this.description = callCategoryListModel ? callCategoryListModel.description == undefined ? "" : callCategoryListModel.description : "";
        this.isActive = callCategoryListModel ? callCategoryListModel.isActive == undefined ? true : callCategoryListModel.isActive : true;

    }
    followUpReasonId: string;
    followUpReasonName:string;
    description: string;
    isActive:boolean
}

class ProcessTypeDesMappingModel {
    constructor(processTypeDesMappingModel?: ProcessTypeDesMappingModel) {
        this.callCategories = processTypeDesMappingModel ? processTypeDesMappingModel.callCategories == undefined ? [] : processTypeDesMappingModel.callCategories : [];
  }

    callCategories: ProcessTypeDesMappingListModel[];
}
class ProcessTypeDesMappingListModel {
    constructor(processTypeDesMappingListModel?: ProcessTypeDesMappingListModel) {
        this.invoiceTransactionTypeDescriptionMappingId = processTypeDesMappingListModel ? processTypeDesMappingListModel.invoiceTransactionTypeDescriptionMappingId == undefined ? '' : processTypeDesMappingListModel.invoiceTransactionTypeDescriptionMappingId : '';
        this.processTypeId = processTypeDesMappingListModel ? processTypeDesMappingListModel.processTypeId == undefined ? '' : processTypeDesMappingListModel.processTypeId : '';
        this.invoiceTransactionTypeId = processTypeDesMappingListModel ? processTypeDesMappingListModel.invoiceTransactionTypeId == undefined ? "" : processTypeDesMappingListModel.invoiceTransactionTypeId : "";
        this.invoiceTransactionDescriptionIds = processTypeDesMappingListModel ? processTypeDesMappingListModel.invoiceTransactionDescriptionIds == undefined ? [] : processTypeDesMappingListModel.invoiceTransactionDescriptionIds : [];
        this.invoiceTransactionDescriptionSubTypeIds = processTypeDesMappingListModel ? processTypeDesMappingListModel.invoiceTransactionDescriptionSubTypeIds == undefined ? [] : processTypeDesMappingListModel.invoiceTransactionDescriptionSubTypeIds : [];
        
        this.transationDescriptionsDropdown = processTypeDesMappingListModel ? processTypeDesMappingListModel.transationDescriptionsDropdown == undefined ? [] : processTypeDesMappingListModel.transationDescriptionsDropdown : [];
        this.subTypeDescriptionsDropdown = processTypeDesMappingListModel ? processTypeDesMappingListModel.subTypeDescriptionsDropdown == undefined ? [] : processTypeDesMappingListModel.subTypeDescriptionsDropdown : [];
        this.modifiedUserId = processTypeDesMappingListModel ? processTypeDesMappingListModel.modifiedUserId == undefined ? "" : processTypeDesMappingListModel.modifiedUserId : "";
        this.createdUserId = processTypeDesMappingListModel ? processTypeDesMappingListModel.createdUserId == undefined ? null : processTypeDesMappingListModel.createdUserId : null;
        
    }
    invoiceTransactionTypeDescriptionMappingId?:string
    processTypeId: string;
    invoiceTransactionTypeId:string;
    invoiceTransactionDescriptionIds:any;
    invoiceTransactionDescriptionSubTypeIds:any;
    modifiedUserId: string;
    transationDescriptionsDropdown: any;
    subTypeDescriptionsDropdown:any
    createdUserId: string
}

export { DropDownConfigModel,CallCategoryListModel,ResolutionCategoryListModel,FollowUpCategoryListModel,ProcessTypeDesMappingModel,ProcessTypeDesMappingListModel }
