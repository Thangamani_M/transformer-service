
class ActivePatrolMessageModel {
    constructor(ActivePatrolMessageModel?: ActivePatrolMessageModel) {
        this.message = ActivePatrolMessageModel ? ActivePatrolMessageModel.message == undefined ? '' : ActivePatrolMessageModel.message : '';
        this.description = ActivePatrolMessageModel ? ActivePatrolMessageModel.description == undefined ? "" : ActivePatrolMessageModel.description : "";
        this.createdUserId = ActivePatrolMessageModel ? ActivePatrolMessageModel.createdUserId == undefined ? '' : ActivePatrolMessageModel.createdUserId : '';
        this.isActive = ActivePatrolMessageModel ? ActivePatrolMessageModel.isActive == undefined ? true : ActivePatrolMessageModel.isActive : true;
    }
    message: string;
    description: string;
    createdUserId: string;
    isActive: boolean;
}

export {  ActivePatrolMessageModel }