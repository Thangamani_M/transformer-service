class AlarmGroupModel {
    constructor(alarmGroupModel?: AlarmGroupModel) {
        this.alarmGroupId = alarmGroupModel ? alarmGroupModel.alarmGroupId == undefined ? '' : alarmGroupModel.alarmGroupId : '';
        this.alarmGroupName = alarmGroupModel ? alarmGroupModel.alarmGroupName == undefined ? '' : alarmGroupModel.alarmGroupName : '';
        this.description = alarmGroupModel ? alarmGroupModel.description == undefined ? '' : alarmGroupModel.description : '';
        this.alarmTypeList = alarmGroupModel ? alarmGroupModel.alarmTypeList == undefined ? null : alarmGroupModel.alarmTypeList : null;
        this.createdUserId = alarmGroupModel ? alarmGroupModel.createdUserId == undefined ? null : alarmGroupModel.createdUserId : null;
        this.modifiedUserId = alarmGroupModel ? alarmGroupModel.modifiedUserId == undefined ? null : alarmGroupModel.modifiedUserId : null;
        this.isActive = alarmGroupModel ? alarmGroupModel.isActive == undefined ? true : alarmGroupModel.isActive : true;
    }
    alarmGroupId?: string;
    alarmGroupName: string;
    description: string;
    alarmTypeList: AlarTypeListModel[];
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}

class AlarTypeListModel {
    alarmTypeId: string;
    alarmGroupId: string;
    createdUserId: string;
    modifiedUserId: string;
}

export { AlarmGroupModel,AlarTypeListModel }