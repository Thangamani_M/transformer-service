export class SafeEntryLimitAddEditModel {
    constructor(safeEntryLimitAddEditModel?: SafeEntryLimitAddEditModel) {
        this.safeEntryLimitId = safeEntryLimitAddEditModel ? safeEntryLimitAddEditModel.safeEntryLimitId == undefined ? '' : safeEntryLimitAddEditModel.safeEntryLimitId : '';
        this.safeEntryLimitCustomersList = safeEntryLimitAddEditModel ? safeEntryLimitAddEditModel.safeEntryLimitCustomersList == undefined ? null : safeEntryLimitAddEditModel.safeEntryLimitCustomersList : null;
        this.safeEntryLimitDivisionsList = safeEntryLimitAddEditModel ? safeEntryLimitAddEditModel.safeEntryLimitDivisionsList == undefined ? null : safeEntryLimitAddEditModel.safeEntryLimitDivisionsList : null;
        this.maxCount = safeEntryLimitAddEditModel ? safeEntryLimitAddEditModel.maxCount == undefined ? '' : safeEntryLimitAddEditModel.maxCount : '';
        this.modifiedUserId = safeEntryLimitAddEditModel ? safeEntryLimitAddEditModel.modifiedUserId == undefined ? '' : safeEntryLimitAddEditModel.modifiedUserId : '';
        this.createdUserId = safeEntryLimitAddEditModel ? safeEntryLimitAddEditModel.createdUserId == undefined ? '' : safeEntryLimitAddEditModel.createdUserId : '';
        this.isActive = safeEntryLimitAddEditModel ? safeEntryLimitAddEditModel.isActive == undefined ? true : safeEntryLimitAddEditModel.isActive : true;
        this.safeEntryLimitName = safeEntryLimitAddEditModel ? safeEntryLimitAddEditModel.safeEntryLimitName == undefined ? '' : safeEntryLimitAddEditModel.safeEntryLimitName : '';

    }
    safeEntryLimitId?: string;
    divisionId?: string;
    maxCount?: string;
    modifiedUserId?: string;  
    createdUserId?:string;
    isActive?: boolean;
    divisionName?: string;
    safeEntryLimitCustomersList: CustomersModel[];
    safeEntryLimitDivisionsList: DivisionsModel[];
    safeEntryLimitName?:string;
}

class CustomersModel {
    customerId: string;
    addressId: string;
}

class DivisionsModel {
    divisionId: string;
}

export class SafeEntryLimitView {
    maxCount?: string;
    divisionName?: string;
    safeEntryLimitId?: string;
    divisionId?: string;
    modifiedUserId?: string;
    isActive?: boolean;
    createdDate?: Date;
    modifiedDate?:Date;
}

