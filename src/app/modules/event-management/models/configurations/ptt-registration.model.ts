class PttRegistrarionModel {
    constructor(pttRegistrarionModel?: PttRegistrarionModel) {
        this.createdUserId = pttRegistrarionModel ? pttRegistrarionModel.createdUserId == undefined ? '' : pttRegistrarionModel.createdUserId : '';
        this.pttid = pttRegistrarionModel ? pttRegistrarionModel.pttid == undefined ? '' : pttRegistrarionModel.pttid : '';
        this.deviceName = pttRegistrarionModel ? pttRegistrarionModel.deviceName == undefined ? "" : pttRegistrarionModel.deviceName : "";
        this.contractDate = pttRegistrarionModel ? pttRegistrarionModel.contractDate == undefined ? "" : pttRegistrarionModel.contractDate : "";
        this.modifiedUserId = pttRegistrarionModel ? pttRegistrarionModel.modifiedUserId == undefined ? "" : pttRegistrarionModel.modifiedUserId : "";
        this.imeiNumber = pttRegistrarionModel ? pttRegistrarionModel.imeiNumber == undefined ? "" : pttRegistrarionModel.imeiNumber : "";
        this.pttRegistrationId = pttRegistrarionModel ? pttRegistrarionModel.pttRegistrationId == undefined ? "" : pttRegistrarionModel.pttRegistrationId : "";
        this.isActive = pttRegistrarionModel ? pttRegistrarionModel.isActive == undefined ? true : pttRegistrarionModel.isActive : true;

    }
    createdUserId?: string;
    pttid: string;
    deviceName: string;
    contractDate: string; 
    modifiedUserId: string; 
    imeiNumber:string;   
    pttRegistrationId:string;
    isActive: boolean
}

class pttRegistrationViewDto {
    pttRegistrationId:string;
    pttid:string;
    imeiNumber: string;
    deviceName: string;
    contractDate: any;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    createdUserName :string;
    modifiedUserName : string;
    channelName: string;   
     areaName: string;
     description:string;
     username: string;
}

export { PttRegistrarionModel, pttRegistrationViewDto };

