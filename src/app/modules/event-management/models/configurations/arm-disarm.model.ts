class ArmDisarmModel {
    constructor(armDisarmModel?: ArmDisarmModel) {
        this.armDisarmList = armDisarmModel ? armDisarmModel.armDisarmList == undefined ? null : armDisarmModel.armDisarmList : null;
    }
    armDisarmList: ArmDisarmListModel[];
}

class ArmDisarmListModel {
    constructor(armDisarmListModel?: ArmDisarmListModel) {
        this.customerAddressId = armDisarmListModel ? armDisarmListModel.customerAddressId == undefined ? null : armDisarmListModel.customerAddressId : null;
        this.dayId = armDisarmListModel ? armDisarmListModel.dayId == undefined ? null : armDisarmListModel.dayId : null;
        this.dayName = armDisarmListModel ? armDisarmListModel.dayName == undefined ? null : armDisarmListModel.dayName : null;
        this.disarmStartTime = armDisarmListModel ? armDisarmListModel.disarmStartTime == undefined ? null : armDisarmListModel.disarmStartTime : null;
        this.disarmEndTime = armDisarmListModel ? armDisarmListModel.disarmEndTime == undefined ? null : armDisarmListModel.disarmEndTime : null;
        this.armStartTime = armDisarmListModel ? armDisarmListModel.armStartTime == undefined ? null : armDisarmListModel.armStartTime : null;
        this.armEndTime = armDisarmListModel ? armDisarmListModel.armEndTime == undefined ? null : armDisarmListModel.armEndTime : null;
        this.modifiedUserId = armDisarmListModel ? armDisarmListModel.modifiedUserId == undefined ? null : armDisarmListModel.modifiedUserId : null;
    }
    customerAddressId: string;
    dayId: string;
    dayName: string
    disarmStartTime: any;
    disarmEndTime: any;
    armStartTime: any;
    armEndTime: any;
    modifiedUserId: string;
}

export { ArmDisarmModel, ArmDisarmListModel }