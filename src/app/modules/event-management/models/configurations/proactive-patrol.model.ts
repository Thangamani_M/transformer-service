class ProactivePatrolModel {
    constructor(proactivePatrolModel?: ProactivePatrolModel) {
        this.proactivePatrolId = proactivePatrolModel ? proactivePatrolModel.proactivePatrolId == undefined ? '' : proactivePatrolModel.proactivePatrolId : '';
        this.modifiedUserId = proactivePatrolModel ? proactivePatrolModel.modifiedUserId == undefined ? "" : proactivePatrolModel.modifiedUserId : "";
        this.proactivePatrolTypeList = proactivePatrolModel ? proactivePatrolModel.proactivePatrolTypeList == undefined ? [] : proactivePatrolModel.proactivePatrolTypeList : [];
        this.createdUserId = proactivePatrolModel ? proactivePatrolModel.createdUserId == undefined ? null : proactivePatrolModel.createdUserId : null;
        this.isActive = proactivePatrolModel ? proactivePatrolModel.isActive == undefined ? true : proactivePatrolModel.isActive : true;
        this.proactivePatrolName = proactivePatrolModel ? proactivePatrolModel.proactivePatrolName == undefined ? "" : proactivePatrolModel.proactivePatrolName : "";
        this.description = proactivePatrolModel ? proactivePatrolModel.description == undefined ? "" : proactivePatrolModel.description : "";
        this.proactivePatrolDivisionList = proactivePatrolModel ? proactivePatrolModel.proactivePatrolDivisionList == undefined ? null : proactivePatrolModel.proactivePatrolDivisionList : null;

    }
    proactivePatrolId?: string;
    modifiedUserId: string;
    proactivePatrolTypeList: ProactivePatrolTypeListModel[]; 
    createdUserId: string;
    isActive: boolean;
    proactivePatrolName:string;
    description:string;
    proactivePatrolDivisionList:proactivePatrolDivisionModel[];
    isEnabled:boolean;
}

  

class ProactivePatrolTypeListModel {
    constructor(proactivePatrolTypeListModel?: ProactivePatrolTypeListModel) {
        this.proactivePatrolTypeDummyId = proactivePatrolTypeListModel ? proactivePatrolTypeListModel.proactivePatrolTypeDummyId == undefined ? '' : proactivePatrolTypeListModel.proactivePatrolTypeDummyId : '';
        this.proactivePatrolTypeName = proactivePatrolTypeListModel ? proactivePatrolTypeListModel.proactivePatrolTypeName == undefined ? '' : proactivePatrolTypeListModel.proactivePatrolTypeName : '';
        this.proactivePatrolTypeId = proactivePatrolTypeListModel ? proactivePatrolTypeListModel.proactivePatrolTypeId == undefined ? 0 : proactivePatrolTypeListModel.proactivePatrolTypeId : 0;
        this.colorCode = proactivePatrolTypeListModel ? proactivePatrolTypeListModel.colorCode == undefined ? '' : proactivePatrolTypeListModel.colorCode : '';

    }
    proactivePatrolTypeDummyId?:string;
    proactivePatrolTypeName:string;
    proactivePatrolTypeId:Number;
    colorCode:string;
}


class proactivePatrolDivisionModel {
     divisionId:string
}

export { ProactivePatrolModel,ProactivePatrolTypeListModel}
