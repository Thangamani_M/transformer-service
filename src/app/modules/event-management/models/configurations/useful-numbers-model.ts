
class UsefulNumbersModel {
    constructor(usefulNumbersModel?: UsefulNumbersModel) {
        this.usefulNumberId = usefulNumbersModel ? usefulNumbersModel.usefulNumberId == undefined ? '' : usefulNumbersModel.usefulNumberId : '';
        this.modifiedUserId = usefulNumbersModel ? usefulNumbersModel.modifiedUserId == undefined ? "" : usefulNumbersModel.modifiedUserId : "";
        this.usefulNumberContactList = usefulNumbersModel ? usefulNumbersModel.usefulNumberContactList == undefined ? [] : usefulNumbersModel.usefulNumberContactList : [];
        this.createdUserId = usefulNumbersModel ? usefulNumbersModel.createdUserId == undefined ? null : usefulNumbersModel.createdUserId : null;
        this.isActive = usefulNumbersModel ? usefulNumbersModel.isActive == undefined ? true : usefulNumbersModel.isActive : true;
        this.groupName = usefulNumbersModel ? usefulNumbersModel.groupName == undefined ? "" : usefulNumbersModel.groupName : "";
        this.description = usefulNumbersModel ? usefulNumbersModel.description == undefined ? "" : usefulNumbersModel.description : "";
        this.usefulNumberDivisionList = usefulNumbersModel ? usefulNumbersModel.usefulNumberDivisionList == undefined ? null : usefulNumbersModel.usefulNumberDivisionList : null;
        this.usefulNumberMainAreaList = usefulNumbersModel ? usefulNumbersModel.usefulNumberMainAreaList == undefined ? null : usefulNumbersModel.usefulNumberMainAreaList : null;
    }
    usefulNumberId?: string;
    modifiedUserId: string;
    usefulNumberContactList: UsefulNumberContactListModel[]; 
    createdUserId: string;
    isActive: boolean;
    groupName:string;
    description:string;
    usefulNumberDivisionList?:DivisionsModel[];
    usefulNumberMainAreaList?:MainAreaModel[];
}

class MainAreaModel {
    mainAreaId: string;
}

class DivisionsModel {
    divisionId: string;
}

  

class UsefulNumberContactListModel {
    constructor(usefulNumberContactListModel?: UsefulNumberContactListModel) {
        this.contactName = usefulNumberContactListModel ? usefulNumberContactListModel.contactName == undefined ? '' : usefulNumberContactListModel.contactName : '';
        this.contactNumber = usefulNumberContactListModel ? usefulNumberContactListModel.contactNumber == undefined ? '' : usefulNumberContactListModel.contactNumber : '';
        this.alternateContactNumber = usefulNumberContactListModel ? usefulNumberContactListModel.alternateContactNumber == undefined ? '' : usefulNumberContactListModel.alternateContactNumber : '';
        this.contactNumberCountryCode = usefulNumberContactListModel ? usefulNumberContactListModel.contactNumberCountryCode == undefined ? "+27" : usefulNumberContactListModel.contactNumberCountryCode : "+27";
        this.alternateContactNumberCountryCode = usefulNumberContactListModel ? usefulNumberContactListModel.alternateContactNumberCountryCode == undefined ? "+27" : usefulNumberContactListModel.alternateContactNumberCountryCode : "+27";
        this.usefulNumberContactId = usefulNumberContactListModel ? usefulNumberContactListModel.usefulNumberContactId == undefined ? null : usefulNumberContactListModel.usefulNumberContactId : null;
    }
    contactName:string;
    contactNumber:string;
    alternateContactNumber:string;
    usefulNumberContactId:null;
    contactNumberCountryCode: string;
    alternateContactNumberCountryCode: string;

}



class UsefulNumberDetailsModel {
    usefulNumberId:string;
    groupName: string;
    description: string;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    usefulNumberDivisionList:string[];
    usefulNumberMainAreaList:string[];
    usefulNumberDivisions:string;
    usefulNumberMainAreas:string;
    usefulNumberContactList:string[];
}

export { UsefulNumberDetailsModel, UsefulNumbersModel,  UsefulNumberContactListModel}
