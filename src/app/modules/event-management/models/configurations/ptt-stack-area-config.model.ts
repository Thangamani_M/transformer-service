class PTTstackareaConfigModel {
    constructor(pttstackareaConfigModel?: PTTstackareaConfigModel) {
        this.pttStackAreaConfigId = pttstackareaConfigModel ? pttstackareaConfigModel.pttStackAreaConfigId == undefined ? '' : pttstackareaConfigModel.pttStackAreaConfigId : '';
        this.channelName = pttstackareaConfigModel ? pttstackareaConfigModel.channelName == undefined ? '' : pttstackareaConfigModel.channelName : '';
        this.description = pttstackareaConfigModel ? pttstackareaConfigModel.description == undefined ? '' : pttstackareaConfigModel.description : '';
        this.username = pttstackareaConfigModel ? pttstackareaConfigModel.username == undefined ? '' : pttstackareaConfigModel.username : '';
        this.stackAreaConfigId = pttstackareaConfigModel ? pttstackareaConfigModel.stackAreaConfigId == undefined ? '' : pttstackareaConfigModel.stackAreaConfigId : '';
        
    }
    
    channelName?:string;
    description?:string;
    pttStackAreaConfigId?:string;
    username?:string;
    stackAreaConfigId?:string;
}
export { PTTstackareaConfigModel };

