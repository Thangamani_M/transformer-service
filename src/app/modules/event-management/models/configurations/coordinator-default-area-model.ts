class CoordinatorDefaultAreaModel {
    constructor(coordinatorDefaultAreaModel?: CoordinatorDefaultAreaModel) {
        this.coordinatorList = coordinatorDefaultAreaModel ? coordinatorDefaultAreaModel.coordinatorList == undefined ? [] : coordinatorDefaultAreaModel.coordinatorList : [];
    }

    coordinatorList: CoordinatorsListModel[];
}

class CoordinatorsListModel {
    constructor(coordinatorsListModel?: CoordinatorsListModel) {
        this.coordinatorId = coordinatorsListModel ? coordinatorsListModel.coordinatorId == undefined ? '' : coordinatorsListModel.coordinatorId : '';
        this.employeeId = coordinatorsListModel ? coordinatorsListModel.employeeId == undefined ? "" : coordinatorsListModel.employeeId : "";
        this.employeeName = coordinatorsListModel ? coordinatorsListModel.employeeName == undefined ? "" : coordinatorsListModel.employeeName : "";
        this.coordinatorMainAreaList = coordinatorsListModel ? coordinatorsListModel.coordinatorMainAreaList == undefined ? [] : coordinatorsListModel.coordinatorMainAreaList : [];
        this.coordinatorTechAreaList = coordinatorsListModel ? coordinatorsListModel.coordinatorTechAreaList == undefined ? [] : coordinatorsListModel.coordinatorTechAreaList : [];
        this.modifiedUserId = coordinatorsListModel ? coordinatorsListModel.modifiedUserId == undefined ? "" : coordinatorsListModel.modifiedUserId : "";
        this.createdUserId = coordinatorsListModel ? coordinatorsListModel.createdUserId == undefined ? null : coordinatorsListModel.createdUserId : null;
    }
    coordinatorId: string;
    employeeId: string;
    employeeName: string;
    coordinatorMainAreaList: MainAreaListModel[]
    coordinatorTechAreaList: TechAreaListModel[]
    modifiedUserId: string;
    createdUserId: string

}

class MainAreaListModel {
    constructor(mainAreaListModel?: MainAreaListModel) {
        this.mainAreaId = mainAreaListModel ? mainAreaListModel.mainAreaId == undefined ? '' : mainAreaListModel.mainAreaId : '';

    }
    mainAreaId: string;
}


class TechAreaListModel {
    constructor(techAreaListModel?: TechAreaListModel) {
        this.techAreaId = techAreaListModel ? techAreaListModel.techAreaId == undefined ? '' : techAreaListModel.techAreaId : '';

    }
    techAreaId: string;
}




export { CoordinatorDefaultAreaModel, CoordinatorsListModel, MainAreaListModel, TechAreaListModel }
