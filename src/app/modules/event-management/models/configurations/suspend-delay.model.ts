class SuspendDelayModel {
    constructor(suspendDelayModel?: SuspendDelayModel) {
        this.maxSuspendDelayId = suspendDelayModel ? suspendDelayModel.maxSuspendDelayId == undefined ? '' : suspendDelayModel.maxSuspendDelayId : '';
        this.maxSuspendDelayName = suspendDelayModel ? suspendDelayModel.maxSuspendDelayName == undefined ? '' : suspendDelayModel.maxSuspendDelayName : '';
        this.maxSuspendDelayDivisionList = suspendDelayModel ? suspendDelayModel.maxSuspendDelayDivisionList == undefined ? null : suspendDelayModel.maxSuspendDelayDivisionList : null;
        this.timeDelay = suspendDelayModel ? suspendDelayModel.timeDelay == undefined ? null : suspendDelayModel.timeDelay : null;
        this.createdUserId = suspendDelayModel ? suspendDelayModel.createdUserId == undefined ? null : suspendDelayModel.createdUserId : null;
        this.modifiedUserId = suspendDelayModel ? suspendDelayModel.modifiedUserId == undefined ? null : suspendDelayModel.modifiedUserId : null;
        this.isActive = suspendDelayModel ? suspendDelayModel.isActive == undefined ? true : suspendDelayModel.isActive : true;
    }
    maxSuspendDelayId?: string;
    maxSuspendDelayName:string;
    maxSuspendDelayDivisionList: string[];
    timeDelay: string;
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}
export { SuspendDelayModel }