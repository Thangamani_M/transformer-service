class TimeOnSiteModel {
    constructor(timeOnSiteModel?: TimeOnSiteModel) {
        this.timeOnSiteId = timeOnSiteModel ? timeOnSiteModel.timeOnSiteId == undefined ? '' : timeOnSiteModel.timeOnSiteId : '';
        this.modifiedUserId = timeOnSiteModel ? timeOnSiteModel.modifiedUserId == undefined ? "" : timeOnSiteModel.modifiedUserId : "";
        this.timeOnSiteMainAreaList = timeOnSiteModel ? timeOnSiteModel.timeOnSiteMainAreaList == undefined ? [] : timeOnSiteModel.timeOnSiteMainAreaList : [];
        this.createdUserId = timeOnSiteModel ? timeOnSiteModel.createdUserId == undefined ? null : timeOnSiteModel.createdUserId : null;
        this.isActive = timeOnSiteModel ? timeOnSiteModel.isActive == undefined ? true : timeOnSiteModel.isActive : true;
        this.divisionId = timeOnSiteModel ? timeOnSiteModel.divisionId == undefined ? "" : timeOnSiteModel.divisionId : "";

    }
    timeOnSiteId?: string;
    modifiedUserId: string;
    timeOnSiteMainAreaList: TimeOnSiteMainAreaListModel[]; 
    createdUserId: string;
    isActive: boolean;
    divisionId:string;  
}

  

class TimeOnSiteMainAreaListModel {
    constructor(timeOnSiteMainAreaListModel?: TimeOnSiteMainAreaListModel) {
        this.mainAreaId = timeOnSiteMainAreaListModel ? timeOnSiteMainAreaListModel.mainAreaId == undefined ? '' : timeOnSiteMainAreaListModel.mainAreaId : '';
        this.timeOnSite = timeOnSiteMainAreaListModel ? timeOnSiteMainAreaListModel.timeOnSite == undefined ? null: timeOnSiteMainAreaListModel.timeOnSite : null;
        this.isSiteAverage = timeOnSiteMainAreaListModel ? timeOnSiteMainAreaListModel.isSiteAverage == undefined ? true : timeOnSiteMainAreaListModel.isSiteAverage : true;
        this.timeOnSiteMainAreaId = timeOnSiteMainAreaListModel ? timeOnSiteMainAreaListModel.timeOnSiteMainAreaId == undefined ? '' : timeOnSiteMainAreaListModel.timeOnSiteMainAreaId : '';

    }
    timeOnSiteMainAreaId:any;
    mainAreaId?:string;
    timeOnSite:number;
    isSiteAverage: boolean

}

export { TimeOnSiteModel,TimeOnSiteMainAreaListModel}
