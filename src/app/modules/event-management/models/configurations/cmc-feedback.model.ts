class CMCFeedbackModel {
    constructor(cmcFeedbackModel?: CMCFeedbackModel) {
        this.cmcFeedbackId = cmcFeedbackModel ? cmcFeedbackModel.cmcFeedbackId == undefined ? '' : cmcFeedbackModel.cmcFeedbackId : '';
        this.modifiedUserId = cmcFeedbackModel ? cmcFeedbackModel.modifiedUserId == undefined ? "" : cmcFeedbackModel.modifiedUserId : "";
        this.cmcFeedbackStandByTimeLimitsList = cmcFeedbackModel ? cmcFeedbackModel.cmcFeedbackStandByTimeLimitsList == undefined ? [] : cmcFeedbackModel.cmcFeedbackStandByTimeLimitsList : [];
        this.createdUserId = cmcFeedbackModel ? cmcFeedbackModel.createdUserId == undefined ? null : cmcFeedbackModel.createdUserId : null;
        this.isActive = cmcFeedbackModel ? cmcFeedbackModel.isActive == undefined ? true : cmcFeedbackModel.isActive : true;
        this.cmcFeedbackName = cmcFeedbackModel ? cmcFeedbackModel.cmcFeedbackName == undefined ? "" : cmcFeedbackModel.cmcFeedbackName : "";
        this.description = cmcFeedbackModel ? cmcFeedbackModel.description == undefined ? "" : cmcFeedbackModel.description : "";
        this.cmcFeedbackDivisionsList = cmcFeedbackModel ? cmcFeedbackModel.cmcFeedbackDivisionsList == undefined ? null : cmcFeedbackModel.cmcFeedbackDivisionsList : null;

    }
    cmcFeedbackId?: string;
    modifiedUserId: string;
    cmcFeedbackStandByTimeLimitsList: CMCFeedbackStandByTimeLimitsListModel[]; 
    createdUserId: string;
    isActive: boolean;
    cmcFeedbackName:string;
    description:string;
    cmcFeedbackDivisionsList:PropertyDivisionModel[];
    isEnabled:boolean;
}

  

class CMCFeedbackStandByTimeLimitsListModel {
    constructor(cmcFeedbackStandByTimeLimitsListModel?: CMCFeedbackStandByTimeLimitsListModel) {
        this.cmcFeedbackStandByItemId = cmcFeedbackStandByTimeLimitsListModel ? cmcFeedbackStandByTimeLimitsListModel.cmcFeedbackStandByItemId == undefined ? '' : cmcFeedbackStandByTimeLimitsListModel.cmcFeedbackStandByItemId : '';
        this.standOffSeconds = cmcFeedbackStandByTimeLimitsListModel ? cmcFeedbackStandByTimeLimitsListModel.standOffSeconds == undefined ? null: cmcFeedbackStandByTimeLimitsListModel.standOffSeconds : null;
        this.cmcEscalationSeconds = cmcFeedbackStandByTimeLimitsListModel ? cmcFeedbackStandByTimeLimitsListModel.cmcEscalationSeconds == undefined ? null : cmcFeedbackStandByTimeLimitsListModel.cmcEscalationSeconds : null;
    }
    cmcFeedbackStandByItemId:string;
    standOffSeconds: number;
    cmcEscalationSeconds: number

}


class PropertyDivisionModel {
     divisionId:string
}

export { CMCFeedbackModel,CMCFeedbackStandByTimeLimitsListModel}
