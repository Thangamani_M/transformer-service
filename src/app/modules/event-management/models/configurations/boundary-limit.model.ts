class BoundaryLimitModel {
    constructor(boundaryLimitModel?: BoundaryLimitModel, editMode?: boolean) {
        this.boundaryLimitId = boundaryLimitModel ? boundaryLimitModel.boundaryLimitId == undefined ? '' : boundaryLimitModel.boundaryLimitId : '';
        this.divisionId = boundaryLimitModel ? boundaryLimitModel.divisionId == undefined ? "" : boundaryLimitModel.divisionId : "";
       
        this.createdUserId = boundaryLimitModel ? boundaryLimitModel.createdUserId == undefined ? null : boundaryLimitModel.createdUserId : null;
        this.modifiedUserId = boundaryLimitModel ? boundaryLimitModel.modifiedUserId == undefined ? null : boundaryLimitModel.modifiedUserId : "";
        this.isActive = boundaryLimitModel ? boundaryLimitModel.isActive == undefined ? true : boundaryLimitModel.isActive : true;
        this.maxDistance = boundaryLimitModel ? boundaryLimitModel.maxDistance == undefined ? "" : boundaryLimitModel.maxDistance : "";
      
        this.mainAreaId = boundaryLimitModel ? boundaryLimitModel.mainAreaId == undefined ? '' : boundaryLimitModel.mainAreaId : '';
    }
    boundaryLimitId?: string;
    divisionId: string;
    createdUserId: string
    modifiedUserId: string
    isActive: boolean;
    maxDistance:string;
    mainAreaId?:string;
}


class BoundaryLimitDetails {
    divisionName:string;
    maxDistance:string;
    boundaryLimitMainAreaList: string[];
    boundaryLimitMainAreas:string;
}
export { BoundaryLimitModel, BoundaryLimitDetails}
