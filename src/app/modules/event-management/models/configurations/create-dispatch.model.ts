class QuickAddServiceCallInitiationModel {

    constructor(quickAddServiceCallModel?: QuickAddServiceCallInitiationModel) {
        this.callInitiationId = quickAddServiceCallModel ? quickAddServiceCallModel.callInitiationId == undefined ? '' : quickAddServiceCallModel.callInitiationId : '';
        this.partitionId = quickAddServiceCallModel ? quickAddServiceCallModel.partitionId == undefined ? '' : quickAddServiceCallModel.partitionId : '';
        this.customerId = quickAddServiceCallModel ? quickAddServiceCallModel.customerId == undefined ? '' : quickAddServiceCallModel.customerId : '';
        this.customerName = quickAddServiceCallModel ? quickAddServiceCallModel.customerName == undefined ? '' : quickAddServiceCallModel.customerName : '';
        this.customerAddress = quickAddServiceCallModel ? quickAddServiceCallModel.customerAddress == undefined ? '' : quickAddServiceCallModel.customerAddress : '';
        this.customerRefNo = quickAddServiceCallModel ? quickAddServiceCallModel.customerRefNo == undefined ? '' : quickAddServiceCallModel.customerRefNo : '';
        this.keyholderId = quickAddServiceCallModel ? quickAddServiceCallModel.keyholderId == undefined ? '' : quickAddServiceCallModel.keyholderId : '';
        this.contactName = quickAddServiceCallModel ? quickAddServiceCallModel.contactName == undefined ? '' : quickAddServiceCallModel.contactName : '';
        this.contactNo = quickAddServiceCallModel ? quickAddServiceCallModel.contactNo == undefined ? '' : quickAddServiceCallModel.contactNo : '';
        this.contactNoCountryCode = quickAddServiceCallModel ? quickAddServiceCallModel.contactNoCountryCode == undefined ? '' : quickAddServiceCallModel.contactNoCountryCode : '';
        this.faultDescriptionId = quickAddServiceCallModel ? quickAddServiceCallModel.faultDescriptionId == undefined ? '' : quickAddServiceCallModel.faultDescriptionId : '';
        this.isCode50 = quickAddServiceCallModel ? quickAddServiceCallModel.isCode50 == undefined ? false : quickAddServiceCallModel.isCode50 : false;
        this.addressId = quickAddServiceCallModel ? quickAddServiceCallModel.addressId == undefined ? '' : quickAddServiceCallModel.addressId : '';
        this.createdUserId = quickAddServiceCallModel ? quickAddServiceCallModel.createdUserId == undefined ? '' : quickAddServiceCallModel.createdUserId : '';
        this.modifiedUserId = quickAddServiceCallModel ? quickAddServiceCallModel.modifiedUserId == undefined ? '' : quickAddServiceCallModel.modifiedUserId : '';
        this.isNewContact = quickAddServiceCallModel ? quickAddServiceCallModel.isNewContact == undefined ? false : quickAddServiceCallModel.isNewContact : false;
        this.comments = quickAddServiceCallModel ? quickAddServiceCallModel.comments == undefined ? [] : quickAddServiceCallModel.comments : [];
        this.addressId = quickAddServiceCallModel ? quickAddServiceCallModel.addressId == undefined ? '' : quickAddServiceCallModel.addressId : '';
        this.callinitiationContactId = quickAddServiceCallModel ? quickAddServiceCallModel.callinitiationContactId == undefined ? '' : quickAddServiceCallModel.callinitiationContactId : '';
        this.priority = quickAddServiceCallModel ? quickAddServiceCallModel.priority == undefined ? '' : quickAddServiceCallModel.priority : '';
        this.alarmTypeId = quickAddServiceCallModel ? quickAddServiceCallModel.alarmTypeId == undefined ? '' : quickAddServiceCallModel.alarmTypeId : '';
        this.customerAddressId = quickAddServiceCallModel ? quickAddServiceCallModel.customerAddressId == undefined ? '' : quickAddServiceCallModel.customerAddressId : '';
        this.notepad = quickAddServiceCallModel ? quickAddServiceCallModel.notepad == undefined ? '' : quickAddServiceCallModel.notepad : '';
        this.isBookTechnician = quickAddServiceCallModel ? quickAddServiceCallModel.isBookTechnician == undefined ? false : quickAddServiceCallModel.isBookTechnician : false;
        this.occurrenceBookPhoneInId = quickAddServiceCallModel ? quickAddServiceCallModel.occurrenceBookPhoneInId == undefined ? '' : quickAddServiceCallModel.occurrenceBookPhoneInId : '';
    }

    callInitiationId: string;
    occurrenceBookPhoneInId: string;
    partitionId: string;
    customerId:string;
    customerName:string;
    customerAddress:string;
    customerRefNo:string;
    callinitiationContactId:string;
    keyholderId:string;
    contactName:string;
    contactNo:string;
    contactNoCountryCode:string;
    faultDescriptionId:string;
    isCode50:boolean;
    addressId:string;
    createdUserId:string;
    modifiedUserId:string;
    isNewContact:boolean;
    priority:string;
    alarmTypeId:string;
    customerAddressId:string;
    notepad:string;
    isBookTechnician:boolean;

    comments: QuickCommentsListModel[];
}
class QuickCommentsListModel {

    constructor(commentsListModel?: QuickCommentsListModel) {
        this.callInitiationCommentId = commentsListModel == undefined ? undefined : commentsListModel.callInitiationCommentId == undefined ? undefined : commentsListModel.callInitiationCommentId;
        this.comments = commentsListModel ? commentsListModel.comments == undefined ? '' : commentsListModel.comments : '';
    }

    callInitiationCommentId?: string;
    comments: string;
}

export { QuickAddServiceCallInitiationModel,QuickCommentsListModel }