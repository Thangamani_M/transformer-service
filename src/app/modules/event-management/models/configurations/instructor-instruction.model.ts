class InstructorInstructionAddEditModel {
    constructor(instructorInstructionAddEditModel?: InstructorInstructionAddEditModel) {
        this.instructorInstructionTypeId = instructorInstructionAddEditModel ? instructorInstructionAddEditModel.instructorInstructionTypeId == undefined ? '' : instructorInstructionAddEditModel.instructorInstructionTypeId : '';
        this.comments = instructorInstructionAddEditModel ? instructorInstructionAddEditModel.comments == undefined ? '' : instructorInstructionAddEditModel.comments : '';
        this.isAcknowledgeRequired = instructorInstructionAddEditModel ? instructorInstructionAddEditModel.isAcknowledgeRequired == undefined ? false : instructorInstructionAddEditModel.isAcknowledgeRequired : false;
        this.isLinkToPage = instructorInstructionAddEditModel ? instructorInstructionAddEditModel.isLinkToPage == undefined ? false : instructorInstructionAddEditModel.isLinkToPage : false;
        this.urlLink = instructorInstructionAddEditModel ? instructorInstructionAddEditModel.urlLink == undefined ? '' : instructorInstructionAddEditModel.urlLink : '';
        this.instructorInstructionTriggerTypesList = instructorInstructionAddEditModel ? instructorInstructionAddEditModel.instructorInstructionTriggerTypesList == undefined ? [] : instructorInstructionAddEditModel.instructorInstructionTriggerTypesList : [];
        
    }
    instructorInstructionTypeId:string;
    comments:string;
    isAcknowledgeRequired:Boolean;
    isLinkToPage:Boolean;
    urlLink:string;
    instructorInstructionTriggerTypesList : any[];
}

export {InstructorInstructionAddEditModel};