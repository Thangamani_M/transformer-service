class SupervisorMessageModel {
    constructor(supervisorMessageModel?: SupervisorMessageModel) {
        this.createdUserId = supervisorMessageModel ? supervisorMessageModel.createdUserId == undefined ? '' : supervisorMessageModel.createdUserId : '';
        this.occurrenceBookId = supervisorMessageModel ? supervisorMessageModel.occurrenceBookId == undefined ? '' : supervisorMessageModel.occurrenceBookId : '';
        this.operatorId = supervisorMessageModel ? supervisorMessageModel.operatorId == undefined ? '' : supervisorMessageModel.operatorId : '';
        this.supervisorMainAreaId = supervisorMessageModel ? supervisorMessageModel.supervisorMainAreaId == undefined ? null : supervisorMessageModel.supervisorMainAreaId : null
        this.supervisorSubAreaId = supervisorMessageModel ? supervisorMessageModel.supervisorSubAreaId == undefined ? null : supervisorMessageModel.supervisorSubAreaId : null;
     
        this.isSiteMessage = supervisorMessageModel ? supervisorMessageModel.isSiteMessage == undefined ? false : supervisorMessageModel.isSiteMessage : false;
        this.message = supervisorMessageModel ? supervisorMessageModel.message == undefined ? '' : supervisorMessageModel.message : '';
    }
    createdUserId?:string;
    occurrenceBookId:string;
    operatorId:string;
    supervisorMainAreaId:string;
    supervisorSubAreaId:string;
    isSiteMessage:Boolean;
    message:string;
}


export { SupervisorMessageModel}
