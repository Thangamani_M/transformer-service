class ROCommunityPatrolModel {
    constructor(rOCommunityPatrolModel?: ROCommunityPatrolModel) {
        this.roCommunityPatrolId = rOCommunityPatrolModel ? rOCommunityPatrolModel.roCommunityPatrolId == undefined ? '' : rOCommunityPatrolModel.roCommunityPatrolId : '';
        this.modifiedUserId = rOCommunityPatrolModel ? rOCommunityPatrolModel.modifiedUserId == undefined ? "" : rOCommunityPatrolModel.modifiedUserId : "";
        this.rOCommunityPatrolTypesList = rOCommunityPatrolModel ? rOCommunityPatrolModel.rOCommunityPatrolTypesList == undefined ? [] : rOCommunityPatrolModel.rOCommunityPatrolTypesList : [];
        this.createdUserId = rOCommunityPatrolModel ? rOCommunityPatrolModel.createdUserId == undefined ? null : rOCommunityPatrolModel.createdUserId : null;
        this.isActive = rOCommunityPatrolModel ? rOCommunityPatrolModel.isActive == undefined ? true : rOCommunityPatrolModel.isActive : true;
        this.roCommunityPatrolName = rOCommunityPatrolModel ? rOCommunityPatrolModel.roCommunityPatrolName == undefined ? "" : rOCommunityPatrolModel.roCommunityPatrolName : "";
        this.description = rOCommunityPatrolModel ? rOCommunityPatrolModel.description == undefined ? "" : rOCommunityPatrolModel.description : "";
        this.rOCommunityPatrolDivisionsList = rOCommunityPatrolModel ? rOCommunityPatrolModel.rOCommunityPatrolDivisionsList == undefined ? null : rOCommunityPatrolModel.rOCommunityPatrolDivisionsList : null;

    }
    roCommunityPatrolId?: string;
    modifiedUserId: string;
    rOCommunityPatrolTypesList: ROCommunityPatrolTypesListModel[];
    createdUserId: string;
    isActive: boolean;
    roCommunityPatrolName: string;
    description: string;
    rOCommunityPatrolDivisionsList: ROCommunityPatrolDivisionsModel[];
    isEnabled: boolean;
}



class ROCommunityPatrolTypesListModel {
    constructor(rOCommunityPatrolTypesListModel?: ROCommunityPatrolTypesListModel) {
        this.roCommunityPatrolTypeId = rOCommunityPatrolTypesListModel ? rOCommunityPatrolTypesListModel.roCommunityPatrolTypeId == undefined ? 0 : rOCommunityPatrolTypesListModel.roCommunityPatrolTypeId : 0;
        this.roCommunityPatrolTypeName = rOCommunityPatrolTypesListModel ? rOCommunityPatrolTypesListModel.roCommunityPatrolTypeName == undefined ? '' : rOCommunityPatrolTypesListModel.roCommunityPatrolTypeName : '';
        this.colorCode = rOCommunityPatrolTypesListModel ? rOCommunityPatrolTypesListModel.colorCode == undefined ? '' : rOCommunityPatrolTypesListModel.colorCode : '';

    }
    roCommunityPatrolTypeId?: Number;
    roCommunityPatrolTypeName: string;
    colorCode: string;

}


class ROCommunityPatrolDivisionsModel {
    divisionId: string
}

export { ROCommunityPatrolModel, ROCommunityPatrolTypesListModel };

