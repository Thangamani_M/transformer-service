class NkaListModel {
    constructor(nkaListModel?: NkaListModel) {
        this.name = nkaListModel ? nkaListModel.name == undefined ? '' : nkaListModel.name : '';
       
        this.emailAddress = nkaListModel ? nkaListModel.emailAddress == undefined ? null : nkaListModel.emailAddress : null;
        this.description = nkaListModel ? nkaListModel.description == undefined ? '' : nkaListModel.description : '';
        this.isActive = nkaListModel ? nkaListModel.isActive == undefined ? true : nkaListModel.isActive : true;
        
    }
    name?: string;
    signalManagementThirdPartyNKAId: string;
    emailAddress: string;
    description: string;
    isActive:boolean;
    guardingDivisionId: string;
    isDelear:boolean;
    isNKA:boolean;
}

class GUArdingModel{
    constructor(gUardingModel?:GUArdingModel){
        this.name = gUardingModel ? gUardingModel.name == undefined ? '' : gUardingModel.name : '';
        this.emailAddress = gUardingModel ? gUardingModel.emailAddress == undefined ? null : gUardingModel.emailAddress : null;
        this.description = gUardingModel ? gUardingModel.description == undefined ? '' : gUardingModel.description : '';
        this.isActive = gUardingModel ? gUardingModel.isActive == undefined ? true : gUardingModel.isActive : true;
       
    }
    name?: string;
    emailAddress: string;
    description: string;
    divisionId:string; guardingDivisionId:string;
    isActive:boolean;
}
class branchModel{
    constructor(BranchModel?:branchModel){
       
        this.divisionId = BranchModel ? BranchModel.divisionId == undefined ? '' : BranchModel.divisionId : '';
        this.branchId = BranchModel ? BranchModel.branchId == undefined ? null : BranchModel.branchId : null;
        this.isActive = BranchModel ? BranchModel.isActive == undefined ? true : BranchModel.isActive : true;
        this.isStatus = BranchModel ? BranchModel.isStatus == undefined ? true : BranchModel.isStatus : true;
    }
    divisionId: string;
    branchId: string;
    isStatus: boolean;
    isActive:boolean;
    
}
class branchRecipientModel{
    constructor(BranchRecipientModel?:branchRecipientModel){
        this.signalManagementThirdPartyBranchId = BranchRecipientModel ? BranchRecipientModel.signalManagementThirdPartyBranchId == undefined ? 0 : BranchRecipientModel.signalManagementThirdPartyBranchId : 0;
        this.name = BranchRecipientModel ? BranchRecipientModel.name == undefined ? '' : BranchRecipientModel.name : '';
        this.emailAddress = BranchRecipientModel ? BranchRecipientModel.emailAddress == undefined ? null : BranchRecipientModel.emailAddress : null;
        this.description = BranchRecipientModel ? BranchRecipientModel.description == undefined ? '' : BranchRecipientModel.description : '';
        this.divisionId = BranchRecipientModel ? BranchRecipientModel.divisionId == undefined ? '' : BranchRecipientModel.divisionId : '';
        this.branchId = BranchRecipientModel ? BranchRecipientModel.branchId == undefined ? '' : BranchRecipientModel.branchId : '';
        this.branchList = BranchRecipientModel ? BranchRecipientModel.branchList == undefined ? [] : BranchRecipientModel.branchList : [];
    }
    signalManagementThirdPartyBranchId:number;
    name?: string;
    emailAddress: string;
    description: string;
    divisionId: string;
    branchId: string;
    branchList: any
}

class miscRecipientModel{
    constructor(miscRecipientModelModel?:miscRecipientModel){
        this.thirdPartyMiscellaneousOriginId = miscRecipientModelModel ? miscRecipientModelModel.thirdPartyMiscellaneousOriginId == undefined ? '' : miscRecipientModelModel.thirdPartyMiscellaneousOriginId : '';
        this.name = miscRecipientModelModel ? miscRecipientModelModel.name == undefined ? '' : miscRecipientModelModel.name : '';
        this.emailAddress = miscRecipientModelModel ? miscRecipientModelModel.emailAddress == undefined ? null : miscRecipientModelModel.emailAddress : null;
        this.description = miscRecipientModelModel ? miscRecipientModelModel.description == undefined ? '' : miscRecipientModelModel.description : '';
        
    }
    thirdPartyMiscellaneousOriginId:string;
    name?: string;
    emailAddress: string;
    description: string;
}
class miscOriginModel{
    constructor(MiscOriginModel?:miscOriginModel){
        this.description = MiscOriginModel ? MiscOriginModel.description == undefined ? '' : MiscOriginModel.description : '';
        this.isActive = MiscOriginModel ? MiscOriginModel.isActive == undefined ? true : MiscOriginModel.isActive : true;
        this.thirdPartyMiscellaneousOriginOriginList = MiscOriginModel?MiscOriginModel.thirdPartyMiscellaneousOriginOriginList == undefined ? [] : MiscOriginModel.thirdPartyMiscellaneousOriginOriginList : [];
        this.thirdPartyMiscellaneousOriginCategoryList = MiscOriginModel?MiscOriginModel.thirdPartyMiscellaneousOriginCategoryList == undefined ? [] : MiscOriginModel.thirdPartyMiscellaneousOriginCategoryList : [];
        this.thirdPartyMiscellaneousOriginMainAreaList = MiscOriginModel?MiscOriginModel.thirdPartyMiscellaneousOriginMainAreaList == undefined ? [] : MiscOriginModel.thirdPartyMiscellaneousOriginMainAreaList : [];
    }
    description: string;
    isActive:boolean;
    thirdPartyMiscellaneousOriginOriginList: thirdPartyMiscellaneousOriginListModel[];
    thirdPartyMiscellaneousOriginCategoryList:thirdPartyMiscellaneousOriginCategoryListModel[];
    thirdPartyMiscellaneousOriginMainAreaList:thirdPartyMiscellaneousOriginMainAreaListModel[];

}
class thirdPartyMiscellaneousOriginListModel{
    constructor(thirdPartyMiscellaneousModel?:thirdPartyMiscellaneousOriginListModel){
        this.originId = thirdPartyMiscellaneousModel ? thirdPartyMiscellaneousModel.originId == undefined ? 0 : thirdPartyMiscellaneousModel.originId : 0;
    }
    originId: number;  
}

class thirdPartyMiscellaneousOriginCategoryListModel{
    constructor(thirdPartyMiscellaneousCategoryModel?:thirdPartyMiscellaneousOriginCategoryListModel){
        this.categoryId = thirdPartyMiscellaneousCategoryModel ? thirdPartyMiscellaneousCategoryModel.categoryId ==undefined ? 0 : thirdPartyMiscellaneousCategoryModel.categoryId : 0;
    }
    categoryId: number;
}
class thirdPartyMiscellaneousOriginMainAreaListModel{
    constructor(thirdPartyMainAreaModel?:thirdPartyMiscellaneousOriginMainAreaListModel){
        this.mainAreaId = thirdPartyMainAreaModel ? thirdPartyMainAreaModel.mainAreaId == undefined ? "" : thirdPartyMainAreaModel.mainAreaId : "";
    }
    mainAreaId: string;
}


export { NkaListModel,GUArdingModel,branchModel,branchRecipientModel,miscRecipientModel,
    miscOriginModel,thirdPartyMiscellaneousOriginListModel,
thirdPartyMiscellaneousOriginCategoryListModel,thirdPartyMiscellaneousOriginMainAreaListModel }
