class DelayDispatchModel {
    constructor(delayDispatchModel?: DelayDispatchModel) {
        this.delayDispatchId = delayDispatchModel ? delayDispatchModel.delayDispatchId == undefined ? '' : delayDispatchModel.delayDispatchId : '';
        this.delayDispatchName = delayDispatchModel ? delayDispatchModel.delayDispatchName == undefined ? '' : delayDispatchModel.delayDispatchName : '';
        this.delayDispatchDescription = delayDispatchModel ? delayDispatchModel.delayDispatchDescription == undefined ? '' : delayDispatchModel.delayDispatchDescription : '';
        this.divisionId = delayDispatchModel ? delayDispatchModel.divisionId == undefined ? '' : delayDispatchModel.divisionId : '';
        this.delayDispatchMainAreaList = delayDispatchModel ? delayDispatchModel.delayDispatchMainAreaList == undefined ? '' : delayDispatchModel.delayDispatchMainAreaList : '';
        this.delayDispatchTimingList = delayDispatchModel ? delayDispatchModel.delayDispatchTimingList == undefined ? [] : delayDispatchModel.delayDispatchTimingList : [];
        this.createdUserId = delayDispatchModel ? delayDispatchModel.createdUserId == undefined ? null : delayDispatchModel.createdUserId : null;
        this.modifiedUserId = delayDispatchModel ? delayDispatchModel.modifiedUserId == undefined ? null : delayDispatchModel.modifiedUserId : null;
        this.isActive = delayDispatchModel ? delayDispatchModel.isActive == undefined ? true : delayDispatchModel.isActive : true;
    }
    delayDispatchId?: string;
    delayDispatchName: string;
    delayDispatchDescription:string
    divisionId: string;
    delayDispatchMainAreaList: any;
    delayDispatchTimingList:PropertydelayDispatchTimingListModel[];
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}
class PropertydelayDispatchTimingListModel {
    constructor(PropertydelayDispatchTimingListModel?: PropertydelayDispatchTimingListModel) {
        this.delayDispatchTimingId = PropertydelayDispatchTimingListModel ? PropertydelayDispatchTimingListModel.delayDispatchTimingId == undefined ? null : PropertydelayDispatchTimingListModel.delayDispatchTimingId : null;
        this.timeRangeFrom = PropertydelayDispatchTimingListModel ? PropertydelayDispatchTimingListModel.timeRangeFrom == undefined ? null : PropertydelayDispatchTimingListModel.timeRangeFrom : null;
        this.timeRangeTo = PropertydelayDispatchTimingListModel ? PropertydelayDispatchTimingListModel.timeRangeTo == undefined ? null : PropertydelayDispatchTimingListModel.timeRangeTo : null;
        this.isSiteAverage = PropertydelayDispatchTimingListModel ? PropertydelayDispatchTimingListModel.isSiteAverage == undefined ? false : PropertydelayDispatchTimingListModel.isSiteAverage : false;
        this.delayTime = PropertydelayDispatchTimingListModel ? PropertydelayDispatchTimingListModel.delayTime == undefined ? null : PropertydelayDispatchTimingListModel.delayTime : null;
    }
    timeRangeFrom: string;
    timeRangeTo: string;
    isSiteAverage: boolean;
    delayTime: number;
    delayDispatchTimingId:string;
}
export { DelayDispatchModel, PropertydelayDispatchTimingListModel };

