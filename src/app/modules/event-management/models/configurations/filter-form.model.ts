class FilterFormModel {
    constructor(filterFormModel?: FilterFormModel) {
        this.stackAreaConfigId = filterFormModel ? filterFormModel.stackAreaConfigId == undefined ? '' : filterFormModel.stackAreaConfigId : '';
        this.stackConfigId = filterFormModel ? filterFormModel.stackConfigId == undefined ? '' : filterFormModel.stackConfigId : '';
        this.onHoldStack = filterFormModel ? filterFormModel.onHoldStack == undefined ? false : filterFormModel.onHoldStack : false;
    }
    stackAreaConfigId?:string;
    stackConfigId:string;
    onHoldStack:boolean;
}


export { FilterFormModel };

