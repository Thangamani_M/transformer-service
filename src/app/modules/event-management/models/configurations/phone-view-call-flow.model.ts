class CallworkFlowListModel {
    constructor(callworkflowlistModel?: CallworkFlowListModel) {
        this.priority = callworkflowlistModel ? callworkflowlistModel.priority == undefined ? '' : callworkflowlistModel.priority : '';
        this.preference = callworkflowlistModel ? callworkflowlistModel.preference == undefined ? '' : callworkflowlistModel.preference : '';
        this.keyHolderId = callworkflowlistModel ? callworkflowlistModel.keyHolderId == undefined ? '' : callworkflowlistModel.keyHolderId : '';
        this.mobileNumberCountryCode = callworkflowlistModel ? callworkflowlistModel.mobileNumberCountryCode == undefined ? '' : callworkflowlistModel.mobileNumberCountryCode : '';
        this.mobileNumber = callworkflowlistModel ? callworkflowlistModel.mobileNumber == undefined ? '' : callworkflowlistModel.mobileNumber : '';
        this.callStatus = callworkflowlistModel ? callworkflowlistModel.callStatus == undefined ? '' : callworkflowlistModel.callStatus : '';
        this.callEndTime = callworkflowlistModel ? callworkflowlistModel.callEndTime == undefined ? '' : callworkflowlistModel.callEndTime : '';
        this.callFeedback = callworkflowlistModel ? callworkflowlistModel.callFeedback == undefined ? '' : callworkflowlistModel.callFeedback : '';
        this.phonerId = callworkflowlistModel ? callworkflowlistModel.phonerId == undefined ? '' : callworkflowlistModel.phonerId : '';
        this.callDuration = callworkflowlistModel ? callworkflowlistModel.callDuration == undefined ? '' : callworkflowlistModel.callDuration : '';
        this.callReceiverName = callworkflowlistModel ? callworkflowlistModel.callReceiverName == undefined ? '' : callworkflowlistModel.callReceiverName : '';
        this.occurrenceBookCallFlowId = callworkflowlistModel ? callworkflowlistModel.occurrenceBookCallFlowId == undefined ? '' : callworkflowlistModel.occurrenceBookCallFlowId : '';
    }
    priority:string;
    callDuration :string;
    phonerId :string;
    mobileNumber :string;
    callStatus :string;
    callEndTime :string;
    preference :string;
    keyHolderId :string;
    mobileNumberCountryCode :string;
    callFeedback :string;
    callReceiverName:string;
    occurrenceBookCallFlowId:string;
}

export { CallworkFlowListModel}
