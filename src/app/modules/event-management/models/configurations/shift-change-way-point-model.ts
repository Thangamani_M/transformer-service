export class ShiftChangeWayPointWayModel {
    constructor(shiftChangeWayPointWayModel?: ShiftChangeWayPointWayModel) {
        this.shiftChangeWayPointId = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.shiftChangeWayPointId == undefined ? '' : shiftChangeWayPointWayModel.shiftChangeWayPointId : '';
        this.divisionId = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.divisionId == undefined ? '' : shiftChangeWayPointWayModel.divisionId : '';
        this.shiftChangeWayPointName = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.shiftChangeWayPointName == undefined ? '' : shiftChangeWayPointWayModel.shiftChangeWayPointName : '';

        this.mainAreaId = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.mainAreaId == undefined ? '' : shiftChangeWayPointWayModel.mainAreaId : '';
        this.description = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.description == undefined ? '' : shiftChangeWayPointWayModel.description : '';
        this.latitude = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.latitude == undefined ? '' : shiftChangeWayPointWayModel.latitude : '';
        this.longitude = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.longitude == undefined ? '' : shiftChangeWayPointWayModel.longitude : '';
        this.radius = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.radius == undefined ? '' : shiftChangeWayPointWayModel.radius : '';



        this.modifiedUserId = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.modifiedUserId == undefined ? '' : shiftChangeWayPointWayModel.modifiedUserId : '';
        this.createdUserId = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.createdUserId == undefined ? '' : shiftChangeWayPointWayModel.createdUserId : '';
        this.isActive = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.isActive == undefined ? true : shiftChangeWayPointWayModel.isActive : true;
        this.isEnabled = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.isEnabled == undefined ? true : shiftChangeWayPointWayModel.isEnabled : true;

        this.shiftChangeWayPoint = shiftChangeWayPointWayModel ? shiftChangeWayPointWayModel.shiftChangeWayPoint == undefined ? '' : shiftChangeWayPointWayModel.shiftChangeWayPoint : '';
    }
    shiftChangeWayPointId?: string;
    divisionId?: string;
    shiftChangeWayPointName?: string;
    mainAreaId?: string;
    description?: string;
    latitude?: string;
    longitude?: string;
    radius?: string;
    modifiedUserId?: string;
    createdUserId?: string;
    isActive?: boolean;
    isEnabled?: boolean
    shiftChangeWayPoint?: string;
}

export class ShiftChangeWayPointDetails {
    shiftChangeWayPointId?: string;
    divisionId?: string;
    mainAreaId?: string;
    description?: string;
    latitude?: string;
    shiftChangeWayPointName?: string;
    longitude?: string;
    radius?: string;
    divisionName?: string;
    mainArea?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    createdUserName?: string;
    modifiedUserName?: string;
    isActive?: boolean;
    isEnabled?: boolean;
    createdDate?: Date;
    modifiedDate?: Date;
}
