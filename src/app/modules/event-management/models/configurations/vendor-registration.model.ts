class VendorRegistrationModel {
    constructor(vendorRegistration?:VendorRegistrationModel){
        this.documentName = vendorRegistration?vendorRegistration.documentName == undefined?'':vendorRegistration.documentName:'';
        this.description = vendorRegistration?vendorRegistration.description == undefined?'':vendorRegistration.description:'';
        this.vendorDocumentId = vendorRegistration?vendorRegistration.vendorDocumentId == undefined?'':vendorRegistration.vendorDocumentId:'';
        this.vendorDocumentTypeId = vendorRegistration?vendorRegistration.vendorDocumentTypeId == undefined?'':vendorRegistration.vendorDocumentTypeId:'';
        this.createdUserId = vendorRegistration?vendorRegistration.createdUserId == undefined?'':vendorRegistration.createdUserId:'';
        this.fileFormat = vendorRegistration?vendorRegistration.fileFormat == undefined?'':vendorRegistration.fileFormat:'';
        this.status = vendorRegistration?vendorRegistration.status == undefined?true:vendorRegistration.status:true;
        this.modifiedUserId = vendorRegistration?vendorRegistration.modifiedUserId == undefined?'':vendorRegistration.modifiedUserId:'';
    }
    documentName?:string;
    description?:string;
    vendorDocumentId?:string;
    vendorDocumentTypeId?:string;
    createdUserId?:string;
    fileFormat?:string;
    status?:boolean;
    modifiedUserId?:string;
}
export { VendorRegistrationModel};