class LongStandoffModel {
    constructor(longStandoffModel?: LongStandoffModel) {
        this.longStandOffId = longStandoffModel ? longStandoffModel.longStandOffId == undefined ? '' : longStandoffModel.longStandOffId : '';
        this.modifiedUserId = longStandoffModel ? longStandoffModel.modifiedUserId == undefined ? "" : longStandoffModel.modifiedUserId : "";
        this.longStandOffMainAreaList = longStandoffModel ? longStandoffModel.longStandOffMainAreaList == undefined ? [] : longStandoffModel.longStandOffMainAreaList : [];
        this.createdUserId = longStandoffModel ? longStandoffModel.createdUserId == undefined ? null : longStandoffModel.createdUserId : null;
        this.isActive = longStandoffModel ? longStandoffModel.isActive == undefined ? true : longStandoffModel.isActive : true;
        this.divisionId = longStandoffModel ? longStandoffModel.divisionId == undefined ? "" : longStandoffModel.divisionId : "";
        this.description = longStandoffModel ? longStandoffModel.description == undefined ? "" : longStandoffModel.description : "";

    }
    longStandOffId?: string;
    modifiedUserId: string;
    longStandOffMainAreaList: LongStandoffMainAreaListModel[]; 
    createdUserId: string;
    isActive: boolean;
    divisionId:string;
    description:string;
}

  

class LongStandoffMainAreaListModel {
    constructor(clusterConfigSubAreaListModel?: LongStandoffMainAreaListModel) {
        this.standOffEscalation = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.standOffEscalation == undefined ? '' : clusterConfigSubAreaListModel.standOffEscalation : '';
        this.mainAreaId = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.mainAreaId == undefined ? '' : clusterConfigSubAreaListModel.mainAreaId : '';
        this.longStandOffMainAreaId = clusterConfigSubAreaListModel ? clusterConfigSubAreaListModel.longStandOffMainAreaId == undefined ? null : clusterConfigSubAreaListModel.longStandOffMainAreaId : null;

    }
    standOffEscalation:string;
    mainAreaId:string;
    longStandOffMainAreaId:null;
}


class LongStandoffDetailsModel {
    longStandOffId:string;
    divisionId: string;
    description: string;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    longStandOffMainAreaList:string[];
    createdUserName:string;
    modifiedUserName:string;
    divisionName:string;
    longStandOffMainAreas:string;
}

export { LongStandoffDetailsModel, LongStandoffModel,  LongStandoffMainAreaListModel}
