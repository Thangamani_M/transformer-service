class PropertyAccessModel {
    constructor(propertyAccessModel?: PropertyAccessModel) {
        this.propertyAccessId = propertyAccessModel ? propertyAccessModel.propertyAccessId == undefined ? '' : propertyAccessModel.propertyAccessId : '';
        this.modifiedUserId = propertyAccessModel ? propertyAccessModel.modifiedUserId == undefined ? "" : propertyAccessModel.modifiedUserId : "";
        this.propertyAccessReasonList = propertyAccessModel ? propertyAccessModel.propertyAccessReasonList == undefined ? [] : propertyAccessModel.propertyAccessReasonList : [];
        this.createdUserId = propertyAccessModel ? propertyAccessModel.createdUserId == undefined ? null : propertyAccessModel.createdUserId : null;
        this.isActive = propertyAccessModel ? propertyAccessModel.isActive == undefined ? true : propertyAccessModel.isActive : true;
        this.propertyAccessName = propertyAccessModel ? propertyAccessModel.propertyAccessName == undefined ? "" : propertyAccessModel.propertyAccessName : "";
        this.description = propertyAccessModel ? propertyAccessModel.description == undefined ? "" : propertyAccessModel.description : "";
        this.propertyAccessDivisionsList = propertyAccessModel ? propertyAccessModel.propertyAccessDivisionsList == undefined ? null : propertyAccessModel.propertyAccessDivisionsList : null;

    }
    propertyAccessId?: string;
    modifiedUserId: string;
    propertyAccessReasonList: PropertyAccessReasonListModel[]; 
    createdUserId: string;
    isActive: boolean;
    propertyAccessName:string;
    description:string;
    propertyAccessDivisionsList:PropertyDivisionModel[];
    isEnabled:boolean;
}

  

class PropertyAccessReasonListModel {
    constructor(propertyAccessReasonListModel?: PropertyAccessReasonListModel) {
        this.propertyAccessReasonListDummyId = propertyAccessReasonListModel ? propertyAccessReasonListModel.propertyAccessReasonListDummyId == undefined ? '' : propertyAccessReasonListModel.propertyAccessReasonListDummyId : '';
        this.propertyAccessReasonListName = propertyAccessReasonListModel ? propertyAccessReasonListModel.propertyAccessReasonListName == undefined ? '' : propertyAccessReasonListModel.propertyAccessReasonListName : '';
        this.isDuplicate = propertyAccessReasonListModel ? propertyAccessReasonListModel.isDuplicate == undefined ? false : propertyAccessReasonListModel.isDuplicate : false;
        this.propertyAccessReasonListId = propertyAccessReasonListModel ? propertyAccessReasonListModel.propertyAccessReasonListId == undefined ? 0 : propertyAccessReasonListModel.propertyAccessReasonListId : 0;
        this.colorCode = propertyAccessReasonListModel ? propertyAccessReasonListModel.colorCode == undefined ? '' : propertyAccessReasonListModel.colorCode : '';

    }
    propertyAccessReasonListDummyId:string;
    propertyAccessReasonListName:string;
    isDuplicate:boolean;
    propertyAccessReasonListId:Number;
    colorCode: string;
}


class PropertyDivisionModel {
     divisionId:string
}

export { PropertyAccessModel,PropertyAccessReasonListModel}
