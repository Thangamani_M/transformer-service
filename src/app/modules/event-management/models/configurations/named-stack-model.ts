
class NamedStackModel {
    constructor(NamedStackModel?: NamedStackModel) {
        this.namedStackConfigName = NamedStackModel ? NamedStackModel.namedStackConfigName == undefined ? '' : NamedStackModel.namedStackConfigName : '';
        this.description = NamedStackModel ? NamedStackModel.description == undefined ? "" : NamedStackModel.description : "";
        this.createdUserId = NamedStackModel ? NamedStackModel.createdUserId == undefined ? '' : NamedStackModel.createdUserId : '';
        this.isActive = NamedStackModel ? NamedStackModel.isActive == undefined ? true : NamedStackModel.isActive : true;
    }
    namedStackConfigName: string;
    description: string;
    createdUserId: string;
    isActive: boolean;
}

export {  NamedStackModel }