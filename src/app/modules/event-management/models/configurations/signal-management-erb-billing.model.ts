class ERBBillingModel{
    constructor(erbbillingModel?: ERBBillingModel ){
        this.divisionId = erbbillingModel ? erbbillingModel.divisionId == undefined ? '' : erbbillingModel.divisionId : '';
        this.erbQueryBuilderId = erbbillingModel ? erbbillingModel.erbQueryBuilderId == undefined ? '' : erbbillingModel.erbQueryBuilderId : '';
        this.isCreated = erbbillingModel ? erbbillingModel.isCreated == undefined ? true : erbbillingModel.isCreated : true;
        this.status = erbbillingModel ? erbbillingModel.status == undefined ? 'Not Created' : erbbillingModel.status : 'Not Created';
        this.year = erbbillingModel ? erbbillingModel.year == undefined ? '' : erbbillingModel.year : '';
        this.notificationMonthNumber = erbbillingModel ? erbbillingModel.notificationMonthNumber == undefined ? '' : erbbillingModel.notificationMonthNumber : '';
        this.erbClientNotifyStatusId = erbbillingModel ? erbbillingModel.erbClientNotifyStatusId == undefined ? '' : erbbillingModel.erbClientNotifyStatusId : '';
        this.createdUserId = erbbillingModel ? erbbillingModel.createdUserId == undefined ? '' : erbbillingModel.createdUserId : '';
        this.runCount = erbbillingModel ? erbbillingModel.runCount == undefined ? '' : erbbillingModel.runCount : '';
        this.erbBillingDocumentDate = erbbillingModel ? erbbillingModel.erbBillingDocumentDate == undefined ? '' : erbbillingModel.erbBillingDocumentDate : '';
        this.debitOrderPaymentDate = erbbillingModel ? erbbillingModel.debitOrderPaymentDate == undefined ? '' : erbbillingModel.debitOrderPaymentDate : '';
        this.erbBillingBatchAmount = erbbillingModel ? erbbillingModel.erbBillingBatchAmount == undefined ? '' : erbbillingModel.erbBillingBatchAmount : '0';
        this.billingMonthNumber =  erbbillingModel ? erbbillingModel.notificationMonthNumber == undefined ? '' : erbbillingModel.notificationMonthNumber : '';
        this.erbBillingNotifyStatusId = '1';
        this.isPost = false;
        this.isSendSMS = false;
    }
    divisionId:string;
    erbQueryBuilderId:string;
    isCreated:boolean;
    status:string;
    year:string;
    notificationMonthNumber:string;
    erbClientNotifyStatusId:string;
    createdUserId:string;
    runCount:string;
    erbBillingDocumentDate:string;
    debitOrderPaymentDate:string;
    erbBillingBatchAmount:string;
    erbBillingNotifyStatusId:string;
    billingMonthNumber: string;
    isPost:boolean;
    isSendSMS:boolean;
}


export{ERBBillingModel};