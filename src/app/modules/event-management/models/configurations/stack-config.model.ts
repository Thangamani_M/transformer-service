class StackConfigModel {
    constructor(stackConfigModel?: StackConfigModel) {
        this.stackConfigId = stackConfigModel ? stackConfigModel.stackConfigId == undefined ? '' : stackConfigModel.stackConfigId : '';
        this.stackName = stackConfigModel ? stackConfigModel.stackName == undefined ? '' : stackConfigModel.stackName : '';
        this.stackConfigDivisions = stackConfigModel ? stackConfigModel.stackConfigDivisions == undefined ? null : stackConfigModel.stackConfigDivisions : null;
        this.stackConfigAlarmGroups = stackConfigModel ? stackConfigModel.stackConfigAlarmGroups == undefined ? null : stackConfigModel.stackConfigAlarmGroups : null;
        this.namedStackConfigId = stackConfigModel ? stackConfigModel.namedStackConfigId == undefined ? '' : stackConfigModel.namedStackConfigId : '';
        this.stackConfigCustomerProfileTypes = stackConfigModel ? stackConfigModel.stackConfigCustomerProfileTypes == undefined ? null : stackConfigModel.stackConfigCustomerProfileTypes : null;
        this.createdUserId = stackConfigModel ? stackConfigModel.createdUserId == undefined ? null : stackConfigModel.createdUserId : null;
        this.modifiedUserId = stackConfigModel ? stackConfigModel.modifiedUserId == undefined ? null : stackConfigModel.modifiedUserId : null;
        this.isActive = stackConfigModel ? stackConfigModel.isActive == undefined ? true : stackConfigModel.isActive : true;
    }
    stackConfigId?: string;
    stackName: string;
    stackConfigDivisions: StackConfigDivisionModel[];
    stackConfigAlarmGroups: StackAlaramGroupModel[];  
    namedStackConfigId: string; 
    stackConfigCustomerProfileTypes:StackConfigCustomerModel[];   
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}
class StackConfigDivisionModel {
    divisionId:string
}
class StackAlaramGroupModel {
    alarmGroupId:string
}
class StackConfigCustomerModel {
    customerProfileTypeId:string
}

export { StackConfigModel, StackConfigDivisionModel, StackAlaramGroupModel, StackConfigCustomerModel };

