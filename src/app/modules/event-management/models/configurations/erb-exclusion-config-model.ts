class ERBExclusionConfigModel {
    constructor(eRBExclusionConfigModel?: ERBExclusionConfigModel) {
        this.erbExclusionConfigId = eRBExclusionConfigModel ? eRBExclusionConfigModel.erbExclusionConfigId == undefined ? '' : eRBExclusionConfigModel.erbExclusionConfigId : '';
        this.divisionId = eRBExclusionConfigModel ? eRBExclusionConfigModel.divisionId == undefined ? "" : eRBExclusionConfigModel.divisionId : "";
        this.techAttendedInLastHours = eRBExclusionConfigModel ? eRBExclusionConfigModel.techAttendedInLastHours == undefined ? "" : eRBExclusionConfigModel.techAttendedInLastHours : "";
        this.techAttendedInLastDays = eRBExclusionConfigModel ? eRBExclusionConfigModel.techAttendedInLastDays == undefined ? "" : eRBExclusionConfigModel.techAttendedInLastDays : "";

        this.createdUserId = eRBExclusionConfigModel ? eRBExclusionConfigModel.createdUserId == undefined ? null : eRBExclusionConfigModel.createdUserId : null;
        this.modifiedUserId = eRBExclusionConfigModel ? eRBExclusionConfigModel.modifiedUserId == undefined ? null : eRBExclusionConfigModel.modifiedUserId : "";
        this.isSiteTypeName = eRBExclusionConfigModel ? eRBExclusionConfigModel.isSiteTypeName == undefined ? true : eRBExclusionConfigModel.isSiteTypeName : true;
        this.isNKAExclusion = eRBExclusionConfigModel ? eRBExclusionConfigModel.isNKAExclusion == undefined ? true : eRBExclusionConfigModel.isNKAExclusion : true;

        this.erbExcludedAlarmTypeList = eRBExclusionConfigModel ? eRBExclusionConfigModel.erbExcludedAlarmTypeList == undefined ? [] : eRBExclusionConfigModel.erbExcludedAlarmTypeList : [];
        this.erbExcludedOriginList = eRBExclusionConfigModel ? eRBExclusionConfigModel.erbExcludedOriginList == undefined ? [] : eRBExclusionConfigModel.erbExcludedOriginList : [];
        this.erbExcludedSiteTypeList = eRBExclusionConfigModel ? eRBExclusionConfigModel.erbExcludedSiteTypeList == undefined ? [] : eRBExclusionConfigModel.erbExcludedSiteTypeList : [];

        this.isActive = eRBExclusionConfigModel ? eRBExclusionConfigModel.isActive == undefined ? true : eRBExclusionConfigModel.isActive : true;
    }
    erbExclusionConfigId?: string;
    divisionId: string;
    techAttendedInLastHours? : string;
    techAttendedInLastDays? : string;
    createdUserId: string
    modifiedUserId: string
    isActive: boolean;
    isSiteTypeName: boolean;
    isNKAExclusion: boolean;
    erbExcludedAlarmTypeList: AlarmTypeListModel[]; 
    erbExcludedOriginList: OriginListModel[];
    erbExcludedSiteTypeList: SiteTypeListModel[];
}

class AlarmTypeListModel {
    constructor(alarmTypeListModel?: AlarmTypeListModel) {
        this.alarmTypeId = alarmTypeListModel ? alarmTypeListModel.alarmTypeId == undefined ? '' : alarmTypeListModel.alarmTypeId : '';

    }
    alarmTypeId:string;
}

class OriginListModel {
    constructor(originListModel?: OriginListModel) {
        this.originId = originListModel ? originListModel.originId == undefined ? '' : originListModel.originId : '';

    }
    originId:string;
}

class SiteTypeListModel {
    constructor(siteTypeListModel?: SiteTypeListModel) {
        this.siteTypeId = siteTypeListModel ? siteTypeListModel.siteTypeId == undefined ? '' : siteTypeListModel.siteTypeId : '';

    }
    siteTypeId:string;
}

export { ERBExclusionConfigModel}
