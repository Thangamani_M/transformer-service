class DecoderAccountConfigModel {
    constructor(eventTypeModel?: DecoderAccountConfigModel) {
        this.decoderAccountConfigId = eventTypeModel ? eventTypeModel.decoderAccountConfigId == undefined ? '' : eventTypeModel.decoderAccountConfigId : '';
        this.createdUserId = eventTypeModel ? eventTypeModel.createdUserId == undefined ? '' : eventTypeModel.createdUserId : '';
        this.modifiedUserId = eventTypeModel ? eventTypeModel.modifiedUserId == undefined ? '' : eventTypeModel.modifiedUserId : '';
        this.decoderId = eventTypeModel ? eventTypeModel.decoderId == undefined ? '' : eventTypeModel.decoderId : '';
        this.commsTypeName = eventTypeModel ? eventTypeModel.commsTypeName == undefined ? '' : eventTypeModel.commsTypeName : '';
        this.length = eventTypeModel ? eventTypeModel.length == undefined ? '' : eventTypeModel.length : '';

       
        this.isManualAccountCodeGeneration = eventTypeModel ? eventTypeModel.isManualAccountCodeGeneration == undefined ? true : eventTypeModel.isManualAccountCodeGeneration : true;
        this.isHexadecimal = eventTypeModel ? eventTypeModel.isHexadecimal == undefined ? false : eventTypeModel.isHexadecimal : false;
        this.isPartitionConsidered = eventTypeModel ? eventTypeModel.isPartitionConsidered == undefined ? false : eventTypeModel.isPartitionConsidered : false;
        this.isBOMMandatory = eventTypeModel ? eventTypeModel.isBOMMandatory == undefined ? false : eventTypeModel.isBOMMandatory : false;
        this.setNamesList = eventTypeModel ? eventTypeModel.setNamesList == undefined ? [] : eventTypeModel.setNamesList : [];

    }
    decoderAccountConfigId?: string;
    modifiedUserId?:string;
    createdUserId?:string;
    decoderId?:string;
    commsTypeName?:string;
    length: string
    isManualAccountCodeGeneration?:boolean;
    isHexadecimal?:boolean;
    isPartitionConsidered?:boolean;
    isBOMMandatory?:boolean
    setNamesList: SetNamesListModel[]
}  

class SetNamesListModel {
    constructor(eventTypeModel?: SetNamesListModel) {
        this.setNameId = eventTypeModel ? eventTypeModel.setNameId == undefined ? '' : eventTypeModel.setNameId : '';
      
    }
    setNameId: string
}  


export { DecoderAccountConfigModel}
