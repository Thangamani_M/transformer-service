class ThirdPartyDetailsModel {
    constructor(thirdPartyDetailsModel?: ThirdPartyDetailsModel) {
        this.thirdPartyId = thirdPartyDetailsModel ? thirdPartyDetailsModel.thirdPartyId == undefined ? '' : thirdPartyDetailsModel.thirdPartyId : '';
        this.createdUserId = thirdPartyDetailsModel ? thirdPartyDetailsModel.createdUserId == undefined ? '' : thirdPartyDetailsModel.createdUserId : '';
        this.modifiedUserId = thirdPartyDetailsModel ? thirdPartyDetailsModel.modifiedUserId == undefined ? '' : thirdPartyDetailsModel.modifiedUserId : '';
        this.companyName = thirdPartyDetailsModel ? thirdPartyDetailsModel.companyName == undefined ? '' : thirdPartyDetailsModel.companyName : '';
        this.monitoringCenterNumber = thirdPartyDetailsModel ? thirdPartyDetailsModel.monitoringCenterNumber == undefined ? '' : thirdPartyDetailsModel.monitoringCenterNumber : '';
        this.divisionId = thirdPartyDetailsModel ? thirdPartyDetailsModel.divisionId == undefined ? '' : thirdPartyDetailsModel.divisionId : '';
        this.contactPersonName = thirdPartyDetailsModel ? thirdPartyDetailsModel.contactPersonName == undefined ? '' : thirdPartyDetailsModel.contactPersonName : '';
        this.officeContactNumber = thirdPartyDetailsModel ? thirdPartyDetailsModel.officeContactNumber == undefined ? '' : thirdPartyDetailsModel.officeContactNumber : '';
        this.buildingName = thirdPartyDetailsModel ? thirdPartyDetailsModel.buildingName == undefined ? '' : thirdPartyDetailsModel.buildingName : '';
        this.buildingNumber = thirdPartyDetailsModel ? thirdPartyDetailsModel.buildingNumber == undefined ? '' : thirdPartyDetailsModel.buildingNumber : '';
        this.streetNumber = thirdPartyDetailsModel ? thirdPartyDetailsModel.streetNumber == undefined ? '' : thirdPartyDetailsModel.streetNumber : '';
        this.streetAddress = thirdPartyDetailsModel ? thirdPartyDetailsModel.streetAddress == undefined ? '' : thirdPartyDetailsModel.streetAddress : '';
        this.suburb = thirdPartyDetailsModel ? thirdPartyDetailsModel.suburb == undefined ? '' : thirdPartyDetailsModel.suburb : '';
        this.cityName = thirdPartyDetailsModel ? thirdPartyDetailsModel.cityName == undefined ? '' : thirdPartyDetailsModel.cityName : '';
        this.province = thirdPartyDetailsModel ? thirdPartyDetailsModel.province == undefined ? '' : thirdPartyDetailsModel.province : '';
        this.postalCode = thirdPartyDetailsModel ? thirdPartyDetailsModel.postalCode == undefined ? '' : thirdPartyDetailsModel.postalCode : '';
        this.latitude = thirdPartyDetailsModel ? thirdPartyDetailsModel.latitude == undefined ? '' : thirdPartyDetailsModel.latitude : '';
        this.longitude = thirdPartyDetailsModel ? thirdPartyDetailsModel.longitude == undefined ? '' : thirdPartyDetailsModel.longitude : '';
        this.findAddress = thirdPartyDetailsModel ? thirdPartyDetailsModel.findAddress == undefined ? '' : thirdPartyDetailsModel.findAddress : '';
        this.isActive = thirdPartyDetailsModel ? thirdPartyDetailsModel.isActive == undefined ? true : thirdPartyDetailsModel.isActive : true;
        this.locationPin = thirdPartyDetailsModel ? thirdPartyDetailsModel.locationPin == undefined ? '' : thirdPartyDetailsModel.locationPin : '';

        this.fullAddress = thirdPartyDetailsModel ? thirdPartyDetailsModel.fullAddress == undefined ? '' : thirdPartyDetailsModel.fullAddress : '';
        this.jsonAddress = thirdPartyDetailsModel ? thirdPartyDetailsModel.jsonAddress == undefined ? '' : thirdPartyDetailsModel.jsonAddress : '';
        this.suburbName = thirdPartyDetailsModel ? thirdPartyDetailsModel.suburbName == undefined ? '' : thirdPartyDetailsModel.suburbName : '';
        this.provinceName = thirdPartyDetailsModel ? thirdPartyDetailsModel.provinceName == undefined ? '' : thirdPartyDetailsModel.provinceName : '';
    
        this.suburbId = thirdPartyDetailsModel ? thirdPartyDetailsModel.suburbId == undefined ? '' : thirdPartyDetailsModel.suburbId : '';
        this.cityId = thirdPartyDetailsModel ? thirdPartyDetailsModel.cityId == undefined ? '' : thirdPartyDetailsModel.cityId : '';
        this.provinceId = thirdPartyDetailsModel ? thirdPartyDetailsModel.provinceId == undefined ? '' : thirdPartyDetailsModel.provinceId : '';
        this.thirdPartyTypeId = thirdPartyDetailsModel ? thirdPartyDetailsModel.thirdPartyTypeId == undefined ? '' : thirdPartyDetailsModel.thirdPartyTypeId : '';
        this.monitoringCenterNumberCountryCode = thirdPartyDetailsModel ? thirdPartyDetailsModel.monitoringCenterNumberCountryCode == undefined ? "+27" : thirdPartyDetailsModel.monitoringCenterNumberCountryCode : "+27";
        this.officeContactNumberCountryCode = thirdPartyDetailsModel ? thirdPartyDetailsModel.officeContactNumberCountryCode == undefined ? "+27" : thirdPartyDetailsModel.officeContactNumberCountryCode : "+27";

    }
    thirdPartyId?: string;
    locationPin?:string;
    createdUserId?: string;
    modifiedUserId?: string;
    companyName?: string;
    monitoringCenterNumber?: string;
    divisionId?: string;
    contactPersonName?: string;
    officeContactNumber?: string;
    buildingName?: string;
    buildingNumber?: string;
    streetNumber?: string;
    streetAddress?: string;
    suburb?: string;
    cityName?: string;
    province?: string;
    postalCode?: string;
    latitude?: string;
    longitude?: string;
    findAddress?:string;
    isActive?: boolean;
    suburbId?:string;
    cityId?:string;
    provinceId?:string;
    fullAddress:string;
    jsonAddress:string;
    suburbName:string;
    provinceName:string;
    thirdPartyTypeId: string;
    monitoringCenterNumberCountryCode:string
    officeContactNumberCountryCode:  string
}

class ThirdPartyDetails {
    thirdPartyId?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    companyName?: string;
    suburbName?: string;
    cityName?: string;
    provinceName?: string;
    monitoringCenterNumber?: string;
    divisionId?: string;
    contactPersonName?: string;
    officeContactNumber?: string;
    buildingName?: string;
    buildingNumber?: string;
    streetNumber?: string;
    streetAddress?: string;
    suburbId?: string;
    city?: string;
    provinceId?: string;
    postalCode?: string;
    latitude?: string;
    longitude?: string;
    isActive?: boolean;
    createdDate?: string;
    modifiedDate?: string;
    divisionName?: string;
    createdUserName?: string;
    modifiedUserName?: string;
    findAddress?:string;
    thirdPartyTypeName?:string;
}





export { ThirdPartyDetailsModel, ThirdPartyDetails }



