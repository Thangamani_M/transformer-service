class PrintStackModel {
  constructor(printStackModel?: PrintStackModel) {
    this.occurrenceBookListReportTemplateName = printStackModel ? printStackModel.occurrenceBookListReportTemplateName == undefined ? null : printStackModel.occurrenceBookListReportTemplateName : null;
    this.occurrenceBookListReportTemplateColumnsList = printStackModel ? printStackModel.occurrenceBookListReportTemplateColumnsList == undefined ? [] : printStackModel.occurrenceBookListReportTemplateColumnsList : [];
    this.createdUserId = printStackModel ? printStackModel.createdUserId == undefined ? null : printStackModel.createdUserId : null;
    this.occurrenceBookListReportTemplateId = printStackModel ? printStackModel.occurrenceBookListReportTemplateId == undefined ? null : printStackModel.occurrenceBookListReportTemplateId : null;
  }
  occurrenceBookListReportTemplateName?: string;
  occurrenceBookListReportTemplateColumnsList?: any[];
  createdUserId?: string;
  occurrenceBookListReportTemplateId?: string;
}
export { PrintStackModel }
