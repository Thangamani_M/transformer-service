class DistributionGroupModel {
    constructor(distributionGroupModel?: DistributionGroupModel) {
        this.distributionGroupId = distributionGroupModel ? distributionGroupModel.distributionGroupId == undefined ? '' : distributionGroupModel.distributionGroupId : '';
        this.modifiedUserId = distributionGroupModel ? distributionGroupModel.modifiedUserId == undefined ? "" : distributionGroupModel.modifiedUserId : "";
        this.distributionGroupEmployeeList = distributionGroupModel ? distributionGroupModel.distributionGroupEmployeeList == undefined ? [] : distributionGroupModel.distributionGroupEmployeeList : [];
        this.createdUserId = distributionGroupModel ? distributionGroupModel.createdUserId == undefined ? null : distributionGroupModel.createdUserId : null;
        this.isActive = distributionGroupModel ? distributionGroupModel.isActive == undefined ? true : distributionGroupModel.isActive : true;
        this.distributionGroupName = distributionGroupModel ? distributionGroupModel.distributionGroupName == undefined ? "" : distributionGroupModel.distributionGroupName : "";
        this.description = distributionGroupModel ? distributionGroupModel.description == undefined ? "" : distributionGroupModel.description : "";
        this.divisionId = distributionGroupModel ? distributionGroupModel.divisionId == undefined ? "" : distributionGroupModel.divisionId : "";
        this.isEnabled = distributionGroupModel ? distributionGroupModel.isEnabled == undefined ? true : distributionGroupModel.isEnabled : true;
        this.distributionGroupMainAreaList = distributionGroupModel ? distributionGroupModel.distributionGroupMainAreaList == undefined ? [] : distributionGroupModel.distributionGroupMainAreaList : [];
        this.tempDistributionGroupMainAreaList = distributionGroupModel ? distributionGroupModel.tempDistributionGroupMainAreaList == undefined ? "" : distributionGroupModel.tempDistributionGroupMainAreaList : "";

    }
    distributionGroupId?: string;
    modifiedUserId: string;
    distributionGroupEmployeeList: DistributionGroupEmployeeListModel[]; 
    distributionGroupMainAreaList:DistributionGroupMainAreaListModel[];
    createdUserId: string;
    isActive: boolean;
    distributionGroupName:string;
    description:string;
    divisionId:string;
    isEnabled:boolean;
    tempDistributionGroupMainAreaList?:string;
}

  

class DistributionGroupEmployeeListModel {
    constructor(distributionGroupEmployeeListModel?: DistributionGroupEmployeeListModel) {
        this.employeeName = distributionGroupEmployeeListModel ? distributionGroupEmployeeListModel.employeeName == undefined ? '' : distributionGroupEmployeeListModel.employeeName : '';
        this.mobileNumberCountryCode = distributionGroupEmployeeListModel ? distributionGroupEmployeeListModel.mobileNumberCountryCode == undefined ? '+27' : distributionGroupEmployeeListModel.mobileNumberCountryCode : '+27';
        this.mobileNumber = distributionGroupEmployeeListModel ? distributionGroupEmployeeListModel.mobileNumber == undefined ? '' : distributionGroupEmployeeListModel.mobileNumber : '';
        this.distributionGroupEmployeeId = distributionGroupEmployeeListModel ? distributionGroupEmployeeListModel.distributionGroupEmployeeId == undefined ? 0 : distributionGroupEmployeeListModel.distributionGroupEmployeeId : 0;
    }
    employeeName:string;
    mobileNumber:string;
    mobileNumberCountryCode:string;
    distributionGroupEmployeeId:number;

}

class DistributionGroupMainAreaListModel {
    constructor(distributionGroupMainAreaListModel?: DistributionGroupMainAreaListModel) {
        this.mainAreaId = distributionGroupMainAreaListModel ? distributionGroupMainAreaListModel.mainAreaId == undefined ? '' : distributionGroupMainAreaListModel.mainAreaId : '';

    }
    mainAreaId:string;
}



class DistributionGroupDetails {
    distributionGroupId:string;
    divisionId:string;
    groupName: string;
    description: string;
    isEnabled: boolean;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    distributionGroupMainAreaList:string[];
    distributionGroupEmployeeList:string[];
    distributionGroupEmployees:string;
    distributionGroupMainAreas:string;
    divisionName:string;
    createdUserName:string;
    modifiedUserName:string;

}



export { DistributionGroupDetails, DistributionGroupModel, DistributionGroupEmployeeListModel, DistributionGroupMainAreaListModel };

