class CmcSmsGroupModel {
    constructor(cmcSmsGroupModel?: CmcSmsGroupModel) {
        this.cmcsmsGroupId = cmcSmsGroupModel ? cmcSmsGroupModel.cmcsmsGroupId == undefined ? '' : cmcSmsGroupModel.cmcsmsGroupId : '';
        this.modifiedUserId = cmcSmsGroupModel ? cmcSmsGroupModel.modifiedUserId == undefined ? "" : cmcSmsGroupModel.modifiedUserId : "";
        this.cmcsmsGroupEmployeeList = cmcSmsGroupModel ? cmcSmsGroupModel.cmcsmsGroupEmployeeList == undefined ? [] : cmcSmsGroupModel.cmcsmsGroupEmployeeList : [];
        this.createdUserId = cmcSmsGroupModel ? cmcSmsGroupModel.createdUserId == undefined ? null : cmcSmsGroupModel.createdUserId : null;
        this.isActive = cmcSmsGroupModel ? cmcSmsGroupModel.isActive == undefined ? true : cmcSmsGroupModel.isActive : true;
        this.groupName = cmcSmsGroupModel ? cmcSmsGroupModel.groupName == undefined ? "" : cmcSmsGroupModel.groupName : "";
        this.description = cmcSmsGroupModel ? cmcSmsGroupModel.description == undefined ? "" : cmcSmsGroupModel.description : "";
        this.divisionId = cmcSmsGroupModel ? cmcSmsGroupModel.divisionId == undefined ? "" : cmcSmsGroupModel.divisionId : "";
        this.isEnabled = cmcSmsGroupModel ? cmcSmsGroupModel.isEnabled == undefined ? true : cmcSmsGroupModel.isEnabled : true;

    }
    cmcsmsGroupId?: string;
    modifiedUserId: string;
    cmcsmsGroupEmployeeList: CmcSmsGroupEmployeeListModel[]; 
    createdUserId: string;
    isActive: boolean;
    groupName:string;
    description:string;
    divisionId:string;
    isEnabled:boolean;
}

  

class CmcSmsGroupEmployeeListModel {
    constructor(cmcSmsGroupEmployeeListModel?: CmcSmsGroupEmployeeListModel) {
        this.employeeName = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.employeeName == undefined ? '' : cmcSmsGroupEmployeeListModel.employeeName : '';
        this.mobileNumber = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.mobileNumber == undefined ? '' : cmcSmsGroupEmployeeListModel.mobileNumber : '';
        this.mobileNumberCountryCode = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.mobileNumberCountryCode == undefined ? '+27' : cmcSmsGroupEmployeeListModel.mobileNumberCountryCode : '+27';
        this.cmcsmsGroupEmployeeId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.cmcsmsGroupEmployeeId == undefined ? null : cmcSmsGroupEmployeeListModel.cmcsmsGroupEmployeeId : null;
    }
    employeeName:string;
    mobileNumber:string;
    mobileNumberCountryCode:string;
    cmcsmsGroupEmployeeId:null;

}



class CmcSmsGroupDetails {
    cmcsmsGroupId:string;
    divisionId:string;
    groupName: string;
    description: string;
    isEnabled: boolean;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    cmcsmsGroupEmployeeList:string[];
    cmcsmsEmployees:string;
    divisionName:string;
}

export { CmcSmsGroupDetails, CmcSmsGroupModel, CmcSmsGroupEmployeeListModel };

