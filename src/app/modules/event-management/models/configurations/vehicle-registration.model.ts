class VehicleRegistrarionModel {
    constructor(vehicleRegistrarionModel?: VehicleRegistrarionModel) {
        this.vehicleRegistrationId = vehicleRegistrarionModel ? vehicleRegistrarionModel.vehicleRegistrationId == undefined ? '' : vehicleRegistrarionModel.vehicleRegistrationId : '';
        this.vehicleRegistrationNumber = vehicleRegistrarionModel ? vehicleRegistrarionModel.vehicleRegistrationNumber == undefined ? '' : vehicleRegistrarionModel.vehicleRegistrationNumber : '';
        this.assetNumber = vehicleRegistrarionModel ? vehicleRegistrarionModel.assetNumber == undefined ? "" : vehicleRegistrarionModel.assetNumber : "";
        this.vehicleMake = vehicleRegistrarionModel ? vehicleRegistrarionModel.vehicleMake == undefined ? "" : vehicleRegistrarionModel.vehicleMake : "";
        this.vehicleModel = vehicleRegistrarionModel ? vehicleRegistrarionModel.vehicleModel == undefined ? "" : vehicleRegistrarionModel.vehicleModel : "";
        this.fleetNumber = vehicleRegistrarionModel ? vehicleRegistrarionModel.fleetNumber == undefined ? "" : vehicleRegistrarionModel.fleetNumber : "";
        this.rNumber = vehicleRegistrarionModel ? vehicleRegistrarionModel.rNumber == undefined ? "" : vehicleRegistrarionModel.rNumber : "";
        this.tracking = vehicleRegistrarionModel ? vehicleRegistrarionModel.tracking == undefined ? "" : vehicleRegistrarionModel.tracking : "";
        this.pdaRegistrationId = vehicleRegistrarionModel ? vehicleRegistrarionModel.pdaRegistrationId == undefined ? "" : vehicleRegistrarionModel.pdaRegistrationId : "";
        this.pttRegistrationId = vehicleRegistrarionModel ? vehicleRegistrarionModel.pttRegistrationId == undefined ? "" : vehicleRegistrarionModel.pttRegistrationId : "";
        this.createdUserId = vehicleRegistrarionModel ? vehicleRegistrarionModel.createdUserId == undefined ? null : vehicleRegistrarionModel.createdUserId : null;
        this.modifiedUserId = vehicleRegistrarionModel ? vehicleRegistrarionModel.modifiedUserId == undefined ? null : vehicleRegistrarionModel.modifiedUserId : null;
        this.isActive = vehicleRegistrarionModel ? vehicleRegistrarionModel.isActive == undefined ? true : vehicleRegistrarionModel.isActive : true;
        this.vehicleTypeId = vehicleRegistrarionModel ? vehicleRegistrarionModel.vehicleTypeId == undefined ? '' : vehicleRegistrarionModel.vehicleTypeId : '';
    }
    vehicleRegistrationId?: string;
    vehicleRegistrationNumber: string;
    assetNumber: string;
    vehicleMake: string; 
    vehicleModel: string; 
    fleetNumber:string;   
    rNumber: string;
    tracking:string;
    pdaRegistrationId: string;
    pttRegistrationId:string;
    createdUserId: string
    modifiedUserId: string
    vehicleTypeId:string;
    isActive: boolean
}
export { VehicleRegistrarionModel };
