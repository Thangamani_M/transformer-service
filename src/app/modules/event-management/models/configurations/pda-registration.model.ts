class PdaRegistrationModel {
    constructor(pdaRegistrationModel?: PdaRegistrationModel) {
        this.pdaRegistrationId = pdaRegistrationModel ? pdaRegistrationModel.pdaRegistrationId == undefined ? '' : pdaRegistrationModel.pdaRegistrationId : '';
        this.pdaid = pdaRegistrationModel ? pdaRegistrationModel.pdaid == undefined ? '' : pdaRegistrationModel.pdaid : '';
        this.imeiNumber = pdaRegistrationModel ? pdaRegistrationModel.imeiNumber == undefined ? '' : pdaRegistrationModel.imeiNumber : '';
        this.simCardNumber = pdaRegistrationModel ? pdaRegistrationModel.simCardNumber == undefined ? "" : pdaRegistrationModel.simCardNumber : "";
        this.contractDate = pdaRegistrationModel ? pdaRegistrationModel.contractDate == undefined ? null : pdaRegistrationModel.contractDate : null;
        this.createdUserId = pdaRegistrationModel ? pdaRegistrationModel.createdUserId == undefined ? null : pdaRegistrationModel.createdUserId : null;
        this.modifiedUserId = pdaRegistrationModel ? pdaRegistrationModel.modifiedUserId == undefined ? null : pdaRegistrationModel.modifiedUserId : null;
        this.isActive = pdaRegistrationModel ? pdaRegistrationModel.isActive == undefined ? true : pdaRegistrationModel.isActive : true;
    }
    pdaRegistrationId?: string;
    pdaid: string;
    imeiNumber: string;
    simCardNumber: string;
    contractDate: Date;
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}
export { PdaRegistrationModel }