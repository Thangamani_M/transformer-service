class ERBExReasonConfigAddEditModel {
    constructor(erbExReasonConfigAddEditModel?: ERBExReasonConfigAddEditModel) {
        this.reasonName = erbExReasonConfigAddEditModel ? erbExReasonConfigAddEditModel.reasonName == undefined ? '' : erbExReasonConfigAddEditModel.reasonName : '';
        this.description = erbExReasonConfigAddEditModel ? erbExReasonConfigAddEditModel.description == undefined ? '' : erbExReasonConfigAddEditModel.description : '';
    }
    reasonName:string;
    description:string;
}

export {ERBExReasonConfigAddEditModel};