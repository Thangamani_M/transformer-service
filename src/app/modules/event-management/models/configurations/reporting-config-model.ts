class ReportingConfigModel {
    constructor(reportingConfigModel?: ReportingConfigModel) {
        this.reportingConfigId = reportingConfigModel ? reportingConfigModel.reportingConfigId == undefined ? '' : reportingConfigModel.reportingConfigId : '';
        this.reportingConfigName = reportingConfigModel ? reportingConfigModel.reportingConfigName == undefined ? "" : reportingConfigModel.reportingConfigName : "";
        this.reportingConfigDivisionList = reportingConfigModel ? reportingConfigModel.reportingConfigDivisionList == undefined ? [] : reportingConfigModel.reportingConfigDivisionList : [];
        this.createdUserId = reportingConfigModel ? reportingConfigModel.createdUserId == undefined ? null : reportingConfigModel.createdUserId : null;
        this.modifiedUserId = reportingConfigModel ? reportingConfigModel.modifiedUserId == undefined ? null : reportingConfigModel.modifiedUserId : "";
        this.isActive = reportingConfigModel ? reportingConfigModel.isActive == undefined ? true : reportingConfigModel.isActive : true;
        this.description = reportingConfigModel ? reportingConfigModel.description == undefined ? "" : reportingConfigModel.description : "";
        this.tempReportingConfigDivisionList = reportingConfigModel ? reportingConfigModel.tempReportingConfigDivisionList == undefined ? "" : reportingConfigModel.tempReportingConfigDivisionList : "";
        this.measure1 = reportingConfigModel ? reportingConfigModel.measure1 == undefined ? '' : reportingConfigModel.measure1 : '';
        this.measure2 = reportingConfigModel ? reportingConfigModel.measure2 == undefined ? '' : reportingConfigModel.measure2 : '';
        this.measure3 = reportingConfigModel ? reportingConfigModel.measure3 == undefined ? '' : reportingConfigModel.measure3 : '';
    }
    reportingConfigId?: string;
    reportingConfigName: string;
    reportingConfigDivisionList: ReportingConfigDivisionListModel[]; 
    createdUserId: string
    modifiedUserId: string
    isActive: boolean;
    description:string;
    tempReportingConfigDivisionList:string;
    measure1?:string;
    measure2?:string;
    measure3?:string;
}

class ReportingConfigDivisionListModel {
    constructor(reportingConfigDivisionListModel?: ReportingConfigDivisionListModel) {
        this.divisionId = reportingConfigDivisionListModel ? reportingConfigDivisionListModel.divisionId == undefined ? '' : reportingConfigDivisionListModel.divisionId : '';

    }
    divisionId:string;
}

class ReportingConfigDetails {
    divisionName?:string;
    reportingConfigId?:string;
    reportingConfigName?:string;
    description?:string;
    reportingConfigDivisionList?: string[];
    reportingConfigDivisions?:string;
    measure1?:string;
    measure2?:string;
    measure3?:string;
    isActive?:boolean;
    createdDate?:string;
    modifiedDate?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    createdUserName?:string;
    modifiedUserName?:string;

}

export { ReportingConfigModel, ReportingConfigDetails}
