class DispatcherListModal{
    constructor(dispatcherListModal?:DispatcherListModal){
        this.stackAreaConfigId=dispatcherListModal?dispatcherListModal.stackAreaConfigId==undefined?'':dispatcherListModal.stackAreaConfigId:'';
        this.stackConfigId=dispatcherListModal?dispatcherListModal.stackConfigId==undefined?'':dispatcherListModal.stackConfigId:'';
         this.onHoldStack=dispatcherListModal?dispatcherListModal.onHoldStack==undefined? false:dispatcherListModal.onHoldStack:false;
    }
    stackAreaConfigId: any;
    stackConfigId: string;
    onHoldStack: boolean;
}
export { DispatcherListModal };
