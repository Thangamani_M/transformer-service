class CommunityPatrolModel {
    constructor(communityPatrolModel?: CommunityPatrolModel) {
        this.communityPatrolId = communityPatrolModel ? communityPatrolModel.communityPatrolId == undefined ? '' : communityPatrolModel.communityPatrolId : '';
        this.communityName = communityPatrolModel ? communityPatrolModel.communityName == undefined ? '' : communityPatrolModel.communityName : '';
        this.patrolTime = communityPatrolModel ? communityPatrolModel.patrolTime == undefined ? '' : communityPatrolModel.patrolTime : '';
        this.restTime = communityPatrolModel ? communityPatrolModel.restTime == undefined ? '' : communityPatrolModel.restTime : '';
        this.communityPatrolCustomersList = communityPatrolModel ? communityPatrolModel.communityPatrolCustomersList == undefined ? null : communityPatrolModel.communityPatrolCustomersList : null;
        this.communityPatrolDivisionList = communityPatrolModel ? communityPatrolModel.communityPatrolDivisionList == undefined ? null : communityPatrolModel.communityPatrolDivisionList : null;
        this.createdUserId = communityPatrolModel ? communityPatrolModel.createdUserId == undefined ? null : communityPatrolModel.createdUserId : null;
        this.modifiedUserId = communityPatrolModel ? communityPatrolModel.modifiedUserId == undefined ? null : communityPatrolModel.modifiedUserId : null;
        this.lssId = communityPatrolModel ? communityPatrolModel.lssId == undefined ? null : communityPatrolModel.lssId : null;
        this.isActive = communityPatrolModel ? communityPatrolModel.isActive == undefined ? true : communityPatrolModel.isActive : true;
    }
    communityPatrolId?: string;
    communityName: string;
    patrolTime: string;
    restTime: string;
    communityPatrolCustomersList: CustomersModel[];
    communityPatrolDivisionList: DivisionsModel[];
    createdUserId: string
    modifiedUserId: string
    lssId:string;
    isActive: boolean
}

class CustomersModel {
    customerId: string;
    addressId: string;
    isActive: string;
}

class DivisionsModel {
    divisionId: string;
}

export { CommunityPatrolModel };
