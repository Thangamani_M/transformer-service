class QueryBuilderConfigModel {
    constructor(queryBuilderConfigModel?: QueryBuilderConfigModel) {
        this.archiveAlias = queryBuilderConfigModel ? queryBuilderConfigModel.archiveAlias == undefined ? "" : queryBuilderConfigModel.archiveAlias : "";
        this.queryBuilderConfigId = queryBuilderConfigModel ? queryBuilderConfigModel.queryBuilderConfigId == undefined ? '' : queryBuilderConfigModel.queryBuilderConfigId : '';
        this.queryBuilderTypeId = queryBuilderConfigModel ? queryBuilderConfigModel.queryBuilderTypeId == undefined ? "" : queryBuilderConfigModel.queryBuilderTypeId : "";
        this.customModuleId = queryBuilderConfigModel ? queryBuilderConfigModel.customModuleId == undefined ? '' : queryBuilderConfigModel.customModuleId : '';
        this.tableName = queryBuilderConfigModel ? queryBuilderConfigModel.tableName == undefined ? '' : queryBuilderConfigModel.tableName : '';
        this.modifiedUserId = queryBuilderConfigModel ? queryBuilderConfigModel.modifiedUserId == undefined ? "" : queryBuilderConfigModel.modifiedUserId : "";
        this.createdUserId = queryBuilderConfigModel ? queryBuilderConfigModel.createdUserId == undefined ? null : queryBuilderConfigModel.createdUserId : null;  
        this.schemaName = queryBuilderConfigModel ? queryBuilderConfigModel.schemaName == undefined ? '' : queryBuilderConfigModel.schemaName : '';
        this.maxRecord = queryBuilderConfigModel ? queryBuilderConfigModel.maxRecord == undefined ? '' : queryBuilderConfigModel.maxRecord : '';
        this.isActive = queryBuilderConfigModel ? queryBuilderConfigModel.isActive == undefined ? true : queryBuilderConfigModel.isActive : true;
        this.queryBuilderFields = queryBuilderConfigModel ? queryBuilderConfigModel.queryBuilderFields == undefined ? [] : queryBuilderConfigModel.queryBuilderFields : [];

    }
    archiveAlias:string;
    queryBuilderConfigId?: string;
    queryBuilderTypeId:string;
    customModuleId: string;
    tableName: string;
    modifiedUserId: string; 
    createdUserId: string;
    schemaName:string;
    maxRecord:string;
    isActive: boolean;
    
    
    isEnabled:boolean;
    queryBuilderFields: QueryBuilderTablesListModel[];
}

  

class QueryBuilderTablesListModel {
    constructor(queryBuilderTablesListModel?: QueryBuilderTablesListModel) {
        this.fieldName = queryBuilderTablesListModel ? queryBuilderTablesListModel.fieldName == undefined ? '' : queryBuilderTablesListModel.fieldName : '';
        this.queryBuilderTableId = queryBuilderTablesListModel ? queryBuilderTablesListModel.queryBuilderTableId == undefined ? '' : queryBuilderTablesListModel.queryBuilderTableId : '';
        this.isTimeLimit = queryBuilderTablesListModel ? queryBuilderTablesListModel.isTimeLimit == undefined ? false : queryBuilderTablesListModel.isTimeLimit : false;
        this.days = queryBuilderTablesListModel ? queryBuilderTablesListModel.days == undefined ? '' : queryBuilderTablesListModel.days : '';

    }
    fieldName:any;
    queryBuilderTableId:string;
    isTimeLimit: boolean;
    days: string;

}



class QueryBuilderConfigDetails {
    queryBuilderConfigId:string;
    archiveAlias:string;
    connectionString: string;
    queryBuilderTypeName: string;
    maxRecord: string;
    isActive: boolean;
    createdDate:string;
    createdUserId:string;
    modifiedDate:string;
    modifiedUserId:string;
    queryBuilderTablesList:string[];
    queryBuilderTables:string;
    queryBuilderFieldList:string[];
    queryBuilderFields:string;
    
}

export { QueryBuilderConfigDetails, QueryBuilderConfigModel,  QueryBuilderTablesListModel}
