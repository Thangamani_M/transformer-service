class DefaultMessageConfigModel {
    constructor(defaultMessageConfigModel?: DefaultMessageConfigModel) {
        this.createdUserId = defaultMessageConfigModel ? defaultMessageConfigModel.createdUserId == undefined ? '' : defaultMessageConfigModel.createdUserId : '';
        this.defaultMessage = defaultMessageConfigModel ? defaultMessageConfigModel.defaultMessage == undefined ? '' : defaultMessageConfigModel.defaultMessage : '';
        this.isActive = defaultMessageConfigModel ? defaultMessageConfigModel.isActive == undefined ? true : defaultMessageConfigModel.isActive : true;
        this.modifiedUserId = defaultMessageConfigModel ? defaultMessageConfigModel.modifiedUserId == undefined ? '' : defaultMessageConfigModel.modifiedUserId : '';
        this.defaultMessageId = defaultMessageConfigModel ? defaultMessageConfigModel.defaultMessageId == undefined ? '' : defaultMessageConfigModel.defaultMessageId : '';
        this.defaultMessageTypeMappings = defaultMessageConfigModel ? defaultMessageConfigModel.defaultMessageTypeMappings == undefined ? [] : defaultMessageConfigModel.defaultMessageTypeMappings : [];

    }
    createdUserId?: string;
    defaultMessage?: string;
    isActive?: boolean;
    modifiedUserId?: string;
    defaultMessageId?: string;
    defaultMessageTypeMappings?: DefaultMessageTypeMappingsModel[]; 
}

class DefaultMessageTypeMappingsModel {
    constructor(defaultMessageTypeMappingsModel?: DefaultMessageTypeMappingsModel) {
        this.defaultMessageTypeId = defaultMessageTypeMappingsModel ? defaultMessageTypeMappingsModel.defaultMessageTypeId == undefined ? '' : defaultMessageTypeMappingsModel.defaultMessageTypeId : '';
       

    }
    defaultMessageTypeId:string;
}

export { DefaultMessageConfigModel }