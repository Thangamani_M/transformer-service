class SignalAutomationModel {
    constructor(signalAutomationModel?: SignalAutomationModel) {
        this.createdUserId = signalAutomationModel ? signalAutomationModel.createdUserId == undefined ? '' : signalAutomationModel.createdUserId : '';
        this.waitForRestore = signalAutomationModel ? signalAutomationModel.waitForRestore == undefined ? "" : signalAutomationModel.waitForRestore : "";
        this.modifiedUserId = signalAutomationModel ? signalAutomationModel.modifiedUserId == undefined ? "" : signalAutomationModel.modifiedUserId : "";
        this.waitForDeliveryConfirmation = signalAutomationModel ? signalAutomationModel.waitForDeliveryConfirmation == undefined ? "" : signalAutomationModel.waitForDeliveryConfirmation : "";
        this.signalAutomationId = signalAutomationModel ? signalAutomationModel.signalAutomationId == undefined ? "" : signalAutomationModel.signalAutomationId : "";
        this.isActive = signalAutomationModel ? signalAutomationModel.isActive == undefined ? true : signalAutomationModel.isActive : true;
        this.divisions = signalAutomationModel ? signalAutomationModel.divisions == undefined ? null : signalAutomationModel.divisions : null;
        this.alarmTypes = signalAutomationModel ? signalAutomationModel.alarmTypes == undefined ? null : signalAutomationModel.alarmTypes : null;
        this.signalAutomationName = signalAutomationModel ? signalAutomationModel.signalAutomationName == undefined ? "" : signalAutomationModel.signalAutomationName : "";

    }
    createdUserId?: string;
    waitForRestore: string; 
    modifiedUserId: string; 
    waitForDeliveryConfirmation:string;   
    signalAutomationId:string;
    isActive:boolean;
    signalAutomationName:string;
    divisions:SignalAutomationDivisionListModel[];
    alarmTypes:SignalAutomationSignalTypeListModel[];
}  

class SignalAutomationDivisionListModel {
    divisionId:string
}

class SignalAutomationSignalTypeListModel {
    alarmTypeId:string
}


class signalAutomationDetails {
    signalAutomationId:string;
    signalTypeId: string;
    waitForRestore: string;
    waitForDeliveryConfirmation: string;
    isActive: boolean;
    signalAutomationDivisions:string;
    createdUserId:string;
    modifiedDate:string;
    signalAutomationAlarmTypes:string;

}

export { SignalAutomationModel, signalAutomationDetails }
