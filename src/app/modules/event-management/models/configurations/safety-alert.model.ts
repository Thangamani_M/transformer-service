class SafetyAlertModel {
    constructor(safetyAlertModel?: SafetyAlertModel) {
        this.safetyAlertConfigId = safetyAlertModel ? safetyAlertModel.safetyAlertConfigId == undefined ? '' : safetyAlertModel.safetyAlertConfigId : '';
        this.modifiedUserId = safetyAlertModel ? safetyAlertModel.modifiedUserId == undefined ? "" : safetyAlertModel.modifiedUserId : "";
        this.safetyAlertConfigMainAreasList = safetyAlertModel ? safetyAlertModel.safetyAlertConfigMainAreasList == undefined ? [] : safetyAlertModel.safetyAlertConfigMainAreasList : [];
        this.createdUserId = safetyAlertModel ? safetyAlertModel.createdUserId == undefined ? null : safetyAlertModel.createdUserId : null;
        this.isActive = safetyAlertModel ? safetyAlertModel.isActive == undefined ? true : safetyAlertModel.isActive : true;
        this.divisionId = safetyAlertModel ? safetyAlertModel.divisionId == undefined ? "" : safetyAlertModel.divisionId : "";

    }
    safetyAlertConfigId?: string;
    modifiedUserId: string;
    safetyAlertConfigMainAreasList: SafetyAlertConfigMainAreasListModel[]; 
    createdUserId: string;
    isActive: boolean;
    divisionId:string;  
}

  

class SafetyAlertConfigMainAreasListModel {
    constructor(safetyAlertConfigMainAreasListModel?: SafetyAlertConfigMainAreasListModel) {
        this.mainAreaId = safetyAlertConfigMainAreasListModel ? safetyAlertConfigMainAreasListModel.mainAreaId == undefined ? '' : safetyAlertConfigMainAreasListModel.mainAreaId : '';
        this.safetyAlertTimer = safetyAlertConfigMainAreasListModel ? safetyAlertConfigMainAreasListModel.safetyAlertTimer == undefined ? null: safetyAlertConfigMainAreasListModel.safetyAlertTimer : null;
        this.passwordWait = safetyAlertConfigMainAreasListModel ? safetyAlertConfigMainAreasListModel.passwordWait == undefined ? null : safetyAlertConfigMainAreasListModel.passwordWait : null;
        this.isActive = safetyAlertConfigMainAreasListModel ? safetyAlertConfigMainAreasListModel.isActive == undefined ? true : safetyAlertConfigMainAreasListModel.isActive : true;
    }
    mainAreaId?:string;
    safetyAlertTimer:number;
    passwordWait: number;
    isActive: boolean

}

export { SafetyAlertModel,SafetyAlertConfigMainAreasListModel}
