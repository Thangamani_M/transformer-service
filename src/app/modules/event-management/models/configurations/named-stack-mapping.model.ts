class NamedStackMappingModel {
    constructor(namedStackMappingModel?: NamedStackMappingModel) {
        this.namedStackId = namedStackMappingModel ? namedStackMappingModel.namedStackId == undefined ? '' : namedStackMappingModel.namedStackId : '';
        this.namedStackConfigId = namedStackMappingModel ? namedStackMappingModel.namedStackConfigId == undefined ? '' : namedStackMappingModel.namedStackConfigId : '';
        this.description = namedStackMappingModel ? namedStackMappingModel.description == undefined ? '' : namedStackMappingModel.description : '';
        this.namedStackCustomerList = namedStackMappingModel ? namedStackMappingModel.namedStackCustomerList == undefined ? null : namedStackMappingModel.namedStackCustomerList : null;
        this.createdUserId = namedStackMappingModel ? namedStackMappingModel.createdUserId == undefined ? null : namedStackMappingModel.createdUserId : null;
        this.modifiedUserId = namedStackMappingModel ? namedStackMappingModel.modifiedUserId == undefined ? null : namedStackMappingModel.modifiedUserId : null;
        this.isActive = namedStackMappingModel ? namedStackMappingModel.isActive == undefined ? true : namedStackMappingModel.isActive : true;
        this.isEnabled = namedStackMappingModel ? namedStackMappingModel.isEnabled == undefined ? true : namedStackMappingModel.isEnabled : true;
    }
    namedStackId?: string;
    namedStackConfigId: string;
    description: string;
    namedStackCustomerList: NamedStackMappingsModel[];
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
    isEnabled:boolean
}

class NamedStackMappingsModel {
    customerId: string;
    addressId: string;
    isActive: string;
}

export { NamedStackMappingModel,NamedStackMappingsModel }