class FeedbackOffsiteModel {
    constructor(feedbackOffsiteModel?: FeedbackOffsiteModel) {
        this.feedbackOffsiteId = feedbackOffsiteModel ? feedbackOffsiteModel.feedbackOffsiteId == undefined ? '' : feedbackOffsiteModel.feedbackOffsiteId : '';
        this.feedbackOffsiteName = feedbackOffsiteModel ? feedbackOffsiteModel.feedbackOffsiteName == undefined ? "" : feedbackOffsiteModel.feedbackOffsiteName : "";
        this.feedbackOffsiteDivisionList = feedbackOffsiteModel ? feedbackOffsiteModel.feedbackOffsiteDivisionList == undefined ? [] : feedbackOffsiteModel.feedbackOffsiteDivisionList : [];
        this.createdUserId = feedbackOffsiteModel ? feedbackOffsiteModel.createdUserId == undefined ? null : feedbackOffsiteModel.createdUserId : null;
        this.modifiedUserId = feedbackOffsiteModel ? feedbackOffsiteModel.modifiedUserId == undefined ? null : feedbackOffsiteModel.modifiedUserId : "";
        this.isActive = feedbackOffsiteModel ? feedbackOffsiteModel.isActive == undefined ? true : feedbackOffsiteModel.isActive : true;
        this.description = feedbackOffsiteModel ? feedbackOffsiteModel.description == undefined ? "" : feedbackOffsiteModel.description : "";
        this.tempFeedbackOffsiteDivisionList = feedbackOffsiteModel ? feedbackOffsiteModel.tempFeedbackOffsiteDivisionList == undefined ? "" : feedbackOffsiteModel.tempFeedbackOffsiteDivisionList : "";
        this.distance = feedbackOffsiteModel ? feedbackOffsiteModel.distance == undefined ? '' : feedbackOffsiteModel.distance : '';
    }
    feedbackOffsiteId?: string;
    feedbackOffsiteName: string;
    feedbackOffsiteDivisionList: FeedbackOffsiteDivisionListModel[]; 
    createdUserId: string
    modifiedUserId: string
    isActive: boolean;
    description:string;
    tempFeedbackOffsiteDivisionList:string;
    distance?:string;
}

class FeedbackOffsiteDivisionListModel {
    constructor(feedbackOffsiteDivisionsListModel?: FeedbackOffsiteDivisionListModel) {
        this.divisionId = feedbackOffsiteDivisionsListModel ? feedbackOffsiteDivisionsListModel.divisionId == undefined ? '' : feedbackOffsiteDivisionsListModel.divisionId : '';

    }
    divisionId:string;
}

class FeedbackOffsiteDetails {
    feedbackOffsiteName?:string;
    feedbackOffsiteId?:string;
    description?:string;
    feedbackOffsiteDivisionList?: string[];
    feedbackOffsiteDivisions?:string;
    distance?:string;
    isActive?:boolean;
    createdDate?:string;
    modifiedDate?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    createdUserName?:string;
    modifiedUserName?:string;

}

export { FeedbackOffsiteModel, FeedbackOffsiteDetails}
