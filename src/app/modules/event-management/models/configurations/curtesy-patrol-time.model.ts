class CurtesyPatrolTimeModel {
    constructor(curtesyPatrolTimeModel?: CurtesyPatrolTimeModel) {
        this.courtesyPatrolTimingId = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.courtesyPatrolTimingId == undefined ? '' : curtesyPatrolTimeModel.courtesyPatrolTimingId : '';
        this.courtesyPatrolTimingName = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.courtesyPatrolTimingName == undefined ? '' : curtesyPatrolTimeModel.courtesyPatrolTimingName : '';
        this.divisionId = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.divisionId == undefined ? '' : curtesyPatrolTimeModel.divisionId : '';
        this.courtesyPatrolTimingMainAreaList = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.courtesyPatrolTimingMainAreaList == undefined ? null : curtesyPatrolTimeModel.courtesyPatrolTimingMainAreaList : null;
        this.fromTime = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.fromTime == undefined ? null : curtesyPatrolTimeModel.fromTime : null;
        this.toTime = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.toTime == undefined ? null : curtesyPatrolTimeModel.toTime : null;
        this.createdUserId = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.createdUserId == undefined ? null : curtesyPatrolTimeModel.createdUserId : null;
        this.modifiedUserId = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.modifiedUserId == undefined ? null : curtesyPatrolTimeModel.modifiedUserId : null;
        this.isActive = curtesyPatrolTimeModel ? curtesyPatrolTimeModel.isActive == undefined ? true : curtesyPatrolTimeModel.isActive : true;
    }
    courtesyPatrolTimingId?: string;
    courtesyPatrolTimingName:string;
    divisionId: string;
    courtesyPatrolTimingMainAreaList: string;
    fromTime: string;
    toTime: string;
    isSiteAverage: boolean;
    delayTime: number
    createdUserId: string
    modifiedUserId: string
    isActive: boolean
}
export { CurtesyPatrolTimeModel }