class VisibilityLoggingCofigModel {
    constructor(VisibilityLoggingCofigModel?: VisibilityLoggingCofigModel) {
        this.visibilityLoggingconfigId = VisibilityLoggingCofigModel ? VisibilityLoggingCofigModel.visibilityLoggingconfigId == undefined ? '' : VisibilityLoggingCofigModel.visibilityLoggingconfigId : '';
        this.createdUserId = VisibilityLoggingCofigModel ? VisibilityLoggingCofigModel.createdUserId == undefined ? '' : VisibilityLoggingCofigModel.createdUserId : '';
        this.modifiedUserId = VisibilityLoggingCofigModel ? VisibilityLoggingCofigModel.modifiedUserId == undefined ? '' : VisibilityLoggingCofigModel.modifiedUserId : '';
        this.divisionId = VisibilityLoggingCofigModel ? VisibilityLoggingCofigModel.divisionId == undefined ? '' : VisibilityLoggingCofigModel.divisionId: '';
        this.description = VisibilityLoggingCofigModel ? VisibilityLoggingCofigModel.description == undefined ? '' : VisibilityLoggingCofigModel.description : '';
        this.intervalInDays = VisibilityLoggingCofigModel ? VisibilityLoggingCofigModel.intervalInDays == undefined ? '' : VisibilityLoggingCofigModel.intervalInDays : '';
        this.numberOfPasses =  VisibilityLoggingCofigModel ? VisibilityLoggingCofigModel.intervalInDays == undefined ? '' : VisibilityLoggingCofigModel.intervalInDays : '';
        this.distanceToCapture = VisibilityLoggingCofigModel ? VisibilityLoggingCofigModel.intervalInDays == undefined ? '' : VisibilityLoggingCofigModel.intervalInDays : '';
    }
    visibilityLoggingconfigId?: string;
    createdUserId?:string;
    modifiedUserId?:string;
    divisionId?:string;
    description?:string;
    intervalInDays?:string
    numberOfPasses?: string;
    distanceToCapture?: string;
}  

class VisibilityConfigDetails {
    visibilityLoggingconfigId?:string;
    divisionId?:string;
    description?: string;
    intervalInDays?: string;
    numberOfPasses?: string;
    createdDate?:string;
    createdUserId?:string;
    modifiedDate?:string;
    modifiedUserId?:string;
}

export { VisibilityLoggingCofigModel, VisibilityConfigDetails }
