import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventSupervisorDashboardComponent } from './event-supervisor-dashboard.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  {
    path: '', component: EventSupervisorDashboardComponent, data: { title: 'Event Supervisor Dashboard' },canActivate: [AuthGuard] 
  },
  { path: 'staff-monitoring', loadChildren: () => import('../event-supervisor-dashboard/staff-monitoring/staff-monitoring.module').then(m => m.StaffMonitoringModule) },
  { path: 'keyholder-password', loadChildren: () => import('../event-supervisor-dashboard/keyholder-password/keyholder-password.module').then(m => m.KeyholderPasswordModule) },
  { path: 'operator-logged-in', loadChildren: () => import('../event-supervisor-dashboard/operator-loged-in/operator-loged-in.module').then(m => m.OperatorLogedInModule) },
  { path: 'operator-interaction', loadChildren: () => import('../event-supervisor-dashboard/operator-interaction/operator-interaction.module').then(m => m.OperatorInteractionModule) },
  { path: 'bolo', loadChildren: () => import('../event-supervisor-dashboard/bolo/bolo.module').then(m => m.BoloModule) },
  { path: 'crime-docket', loadChildren: () => import('../event-supervisor-dashboard/crime-docket/crime-docket.module').then(m => m.CrimeDocketModule) },
  { path: 'open-area', loadChildren: () => import('../event-supervisor-dashboard/open-area/open-area.module').then(m => m.OpenAreaListModule) },
  { path: 'long-standoff', loadChildren: () => import('../event-supervisor-dashboard/supervisor-long-standoff/supervisor-long-standoff.module').then(m => m.SupervisorLongStandoffModule) },
  { path: 'login-non-ar', loadChildren: () => import('../event-supervisor-dashboard/Login-Non-AR/login-non-ar.module').then(m => m.LoginNonArModule) },
  { path: 'auto-dispatch-usage', loadChildren: () => import('../event-supervisor-dashboard/auto-dispatch-usage/auto-dispatch-usage.module').then(m => m.AutoDispatchUsageModule) },
  { path: 'data-integrity-failure', loadChildren: () => import('../event-supervisor-dashboard/data-integrity-process-failure/data-integrity-process-failure.module').then(m => m.DataIntegrityProcessFailureModule) },
  { path: 'long-phone', loadChildren: () => import('../event-supervisor-dashboard/long-phone/long-phone.module').then(m => m.LongPhoneModule) },
  { path: 'long-arrival-response-th-one', loadChildren: () => import('../event-supervisor-dashboard/long-arrival-response-th-one/long-arrival-response-th-one.module').then(m => m.LongArrivalResponseThOneModule) },
  { path: 'long-arrival-response-th-two', loadChildren: () => import('../event-supervisor-dashboard/long-arrival-response-th-two/long-arrival-response-th-two.module').then(m => m.LongArrivalResponseThTwoModule) },
  { path: 'long-arrival-response-th-three', loadChildren: () => import('../event-supervisor-dashboard/long-arrival-response-th-three/long-arrival-response-th-three.module').then(m => m.LongArrivalResponseThThreeModule) },
  { path: 'user-ranking', loadChildren: () => import('../event-supervisor-dashboard/user-ranking/user-ranking.module').then(m => m.UserRankingModule) },
  { path: 'service-median', loadChildren: () => import('../event-supervisor-dashboard/service-median/service-median.module').then(m => m.ServiceMedianModule) },
  { path: 'shift-stats', loadChildren: () => import('../event-supervisor-dashboard/shift-stats/shift-stats.module').then(m => m.ShiftStatsModule) },
  { path: 'service-level', loadChildren: () => import('../event-supervisor-dashboard/service-level/service-level.module').then(m => m.ServiceLevelModule) },
  { path: 'volume', loadChildren: () => import('../event-supervisor-dashboard/volume/volume.module').then(m => m.VolumeModule) },
  { path: 'create-message', loadChildren: () => import('../event-supervisor-dashboard/create-message/create-message.module').then(m => m.CreateMessageModule) },
  { path: 'operator', loadChildren: () => import('../event-supervisor-dashboard/operator/operator.module').then(m => m.OperatorModule) },
  { path: 'service-iqr', loadChildren: () => import('../event-supervisor-dashboard/service-iqr/service-iqr.module').then(m => m.ServiceIqrModule) },
  { path: 'group-messages', loadChildren: () => import('../event-supervisor-dashboard/group-messages/group-messages.module').then(m => m.GroupMessagesModule) },
  { path: 'vas-dashboard-view', loadChildren: () => import('../event-supervisor-dashboard/vas-dashboard-view/vas-dashboard-view.module').then(m => m.VasDashboardViewModule) },
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class EventSupervisorDashboardRoutingModule { }
