import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OperatorLogedInViewComponent } from './operator-loged-in-view.component';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'view', component: OperatorLogedInViewComponent, data: { title: 'Logged In View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class OperatorLogedInRoutingModule { }
