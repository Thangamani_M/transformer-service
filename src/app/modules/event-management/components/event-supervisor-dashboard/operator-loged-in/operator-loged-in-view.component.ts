import { Component, EventEmitter, Input, OnInit, Output, SimpleChange } from '@angular/core';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { OperatorInteractionAreaDistrubutioncomponentComponent } from '../operator-interaction-area-distrubutioncomponent/operator-interaction-area-distrubutioncomponent.component';

@Component({
  selector: 'app-operator-loged-in-view',
  templateUrl: './operator-loged-in-view.component.html',
})
export class OperatorLogedInViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() breadcrumFalse: boolean = true;
  @Input() refresh: any;
  @Output() onReallocationSuccessful = new EventEmitter<any>();
  changes:any;

  constructor(
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private momentService: MomentService,
    public dialogService: DialogService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Logged In View",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' },{ displayName: 'Staff Monitoring', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 1} }, { displayName: 'Logged In view' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Operator Logged-in View',
            dataKey: 'userId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'operatorName', header: 'Operator' },
            { field: 'roleName', header: 'Role' },
            { field: 'stackName', header: 'Stack' },
            { field: 'areaName', header: 'Area' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.STAFF_MANITORING_LOGGED_IN_VIEW,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: false,
            enableReloadBtn: false,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    if(!this.changes){
      this.getOperatorLoggerViewDetails();
    }
  }

  ngOnChanges(changes: SimpleChange): void {
    this.changes = changes
    if (changes) {
      if (changes['refresh']) {
        this.getOperatorLoggerViewDetails()
      }
    }
  }

  getOperatorLoggerViewDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getOperatorLoggerViewDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(row, unknownVar)
        break;
    }
  }


  openAddEditPage(row?: any, editableObject?: any | string): void {
    const ref = this.dialogService.open(OperatorInteractionAreaDistrubutioncomponentComponent, {
      showHeader: false,
      baseZIndex: 1000,
      width: '850px',
      data: { ...editableObject, row: row, header: 'Area Distrubution Dialog', },
    });
    ref.onClose.subscribe((result) => {
      if (result == true) {
        this.onReallocationSuccessful.emit(result);
        this.getOperatorLoggerViewDetails()
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

}
