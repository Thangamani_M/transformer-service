import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OperatorLogedInRoutingModule } from './operator-loged-in-routing.module';
import { OperatorLogedInViewComponent } from './operator-loged-in-view.component';


@NgModule({
  declarations: [OperatorLogedInViewComponent],
  imports: [
    CommonModule,
    OperatorLogedInRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  exports:[OperatorLogedInViewComponent]
})
export class OperatorLogedInModule { }
