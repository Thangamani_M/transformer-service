import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-operator-interaction-view',
  templateUrl: './operator-interaction-view.component.html',
  styleUrls: ['./operator-interaction-view.component.scss']
})
export class OperatorInteractionViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {}
  filterData: any;
  dateFilterForm: FormGroup;
  operatorId: any;
  operatorDetails: any;
  dataList: any;

  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private momentService: MomentService, private _fb: FormBuilder, private datePipe:DatePipe) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: 'Operator Interactions',
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Staff Monitoring', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 1} }, { displayName: 'Operator Interactions' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal Management Stack Operator',
            dataKey: 'occurrenceBookPhonerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'customerName', header: 'Customer' },
            { field: 'fullAddress', header: 'Address' },
            { field: 'occurrenceBookNumber', header: 'OBNumber' },
            { field: 'occurrenceBookPhonerTime', header: 'DateTime', isDate:true },
            { field: 'interactionTypeName', header: 'Interaction' },],
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            apiSuffixModel: EventMgntModuleApiSuffixModels.OPERATOR_INTERACTION_SUPERVISOR_VIEW,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: false,
            enableReloadBtn: false,
            enableExportCSV: false,
          }
        ]
      }
    }
    this.operatorId = this.activatedRoute.snapshot.queryParams.id

  }

  ngOnInit(): void {
    this.getLongResponceDetails();
    this.GetOperatorDetailsById(this.operatorId);
    this.dateFilterForm = this._fb.group({
      dateStart: ['', Validators.required],
      dateEnd: ['', Validators.required]
    })
  }

  getLongResponceDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
      OperatorId: this.operatorId,
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        // data.resources.forEach(element => {
        //   element.occurrenceBookPhonerTime =  this.datePipe.transform(element?.occurrenceBookPhonerTime, 'dd-MM-yyyy, HH:mm:ss') 
        // });
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
    })
  }

  GetOperatorDetailsById(operatorId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPERATOR, operatorId, false, null).subscribe((response: IApplicationResponse) => {
      this.operatorDetails = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};

        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getLongResponceDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
  }
  }

  onSubmitFilter() {
    if(this.dateFilterForm.invalid){
      return
    }
    let filteredData = Object.assign({},     
      { dateStart: this.dateFilterForm.get('dateStart').value ? this.momentService.toConvertDate(this.dateFilterForm.get('dateStart').value) : 'dd-MM-yyyy, h:mm:ss a' },
      { dateEnd: this.dateFilterForm.get('dateEnd').value ? this.momentService.toConvertDate(this.dateFilterForm.get('dateEnd').value) : 'dd-MM-yyyy, h:mm:ss a' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0
    this.dataList = this.getLongResponceDetails(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
  }


  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

}