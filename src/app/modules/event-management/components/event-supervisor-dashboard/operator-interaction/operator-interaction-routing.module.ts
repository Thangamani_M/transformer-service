import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OperatorInteractionViewComponent } from './operator-interaction-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'view', component: OperatorInteractionViewComponent, data: { title: 'Operator Interaction View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class OperatorInteractionRoutingModule { }
