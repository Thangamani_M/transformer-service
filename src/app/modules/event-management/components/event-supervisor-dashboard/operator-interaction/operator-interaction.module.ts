import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OperatorInteractionRoutingModule } from './operator-interaction-routing.module';
import { OperatorInteractionViewComponent } from './operator-interaction-view.component';



@NgModule({
  declarations: [OperatorInteractionViewComponent],
  imports: [
    CommonModule,
    OperatorInteractionRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ]
})
export class OperatorInteractionModule { }
