import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { AutoDispatchUsageListComponent } from './auto-dispatch-usage-list.component';
import { AutoDispatchUsageRoutingModule } from './auto-dispatch-usage-routing.module';

@NgModule({
  declarations: [AutoDispatchUsageListComponent],
  imports: [
    CommonModule,
    AutoDispatchUsageRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AutoDispatchUsageModule { }
