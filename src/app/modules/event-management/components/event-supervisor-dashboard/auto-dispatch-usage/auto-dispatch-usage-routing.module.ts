import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoDispatchUsageListComponent } from './auto-dispatch-usage-list.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: AutoDispatchUsageListComponent, data: { title: 'Auto Dispatch Usage List' } },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class AutoDispatchUsageRoutingModule { }
