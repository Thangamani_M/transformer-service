import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-service-median',
  templateUrl: './service-median.component.html',
})
export class ServiceMedianComponent implements OnInit {

  filterData: any;
  refresh: number = 0;
  export: number = 0;
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;
    });
  }
  ngOnInit() {
  };
  exportRequest() {
    this.export++;
  };
  refreshRequest() {
    this.refresh++;
  };

  navigateHourly(){
    this.router.navigate(["/event-management/supervisor-dashboard"], { queryParams: { tab: 6,hourlyChildTab:9}, state: { example: this.filterData} });
  }
}