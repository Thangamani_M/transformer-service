import { Component, Input, OnInit, SimpleChange } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { EventMgntModuleApiSuffixModels } from "@modules/event-management";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-service-median-list',
  templateUrl: './service-median-list.component.html',
})
export class ServiceMedianListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() filterData: any;
  @Input() refresh: any;
  @Input() export: any;

  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Volume",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Hourly Stats', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 6, hourlyChildTab: 7 } }, { displayName: 'Long Phoneback' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Volume',
            dataKey: 'divisionId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'divisionName', header: 'Division' },
            { field: 'mainArea', header: 'Main Area' },
            { field: 'phoneMedian', header: 'PhoneMedian' },
            { field: 'dispatchMedian', header: 'DispatchMedian' },
            { field: 'responseMedian', header: 'ResponseMedian' },
            { field: 'finishMedian', header: 'FinishMedian' },
            { field: 'feedbackMedian', header: 'FeedbackMedian' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SERVICE_MEDIAN,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            enableReloadBtn: true,
            enableExportCSV: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;
    });
  }

  ngOnInit(): void {
    this.getServiceMedianList();
  }

  getServiceMedianList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.rxjsService.setFormChangeDetectionProperty(true);
    let obj1 = {
      IsAll: true,
      DivisionId: this.filterData.divisionId,
      FromDate: this.filterData.fromDate,
      ToDate: this.filterData.toDate,
      SiteTypeIds: this.filterData.siteTypeIds
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;;
        this.totalRecords = data.totalCount;
      }
      else{
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getServiceMedianList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.RELOAD:
        this.getServiceMedianList();
        break
      case CrudType.EXPORT:
        this.exportExcel();
        break
      default:
    }
  }

  ngOnChanges(changes: SimpleChange): void {
    if (changes) {
      if (changes['refresh'] && changes['refresh'].currentValue > 0) {
        this.getServiceMedianList()
      }
      if (changes['export'] && changes['export'].currentValue > 0) {
        this.exportExcel()
      }
    }

  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

}