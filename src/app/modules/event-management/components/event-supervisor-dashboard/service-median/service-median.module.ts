import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceMedianRoutingModule } from './service-median-routing.module';
import { ServiceMedianComponent } from './service-median.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { ServiceMedianTotalListComponent } from './service-median-total-list.component';
import { ServiceMedianListComponent } from './service-median-list.component';


@NgModule({
  declarations: [ServiceMedianComponent, ServiceMedianTotalListComponent, ServiceMedianListComponent],
  imports: [
    CommonModule,
    ServiceMedianRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ServiceMedianModule { }
