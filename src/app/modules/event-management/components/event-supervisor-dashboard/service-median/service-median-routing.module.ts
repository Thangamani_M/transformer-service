import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceMedianComponent } from './service-median.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ServiceMedianComponent, data: { title: 'Service Median' } },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ServiceMedianRoutingModule { }
