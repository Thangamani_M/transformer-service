import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-group-messages-add-edit',
  templateUrl: './group-messages-add-edit.component.html',
  styleUrls: ['./group-messages-add-edit.component.scss']
})
export class GroupMessagesAddEditComponent implements OnInit {

  creatId: any;
  createDetails: any;
  mainAreaDropDown: any = [];

  loggedUser: any;
  createForm: FormGroup;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  public isInvalid: boolean = false;

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  dropDownData: any;
  dropDownDataValue: any;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.creatId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.creatId ? 'Update Group Message':'Add Group Message';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: ' Supervisor Dashboard', relativeRouterUrl: '' },
      { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } },{ displayName: 'Group Message', relativeRouterUrl: '/event-management/supervisor-dashboard/group-messages' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.creatId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/supervisor-dashboard/group-messages/view' , queryParams: {id: this.creatId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createcreateForm();
    this.getMainAreaDropdown();
    if (this.creatId) {
      this.getcreatDetailsById(this.creatId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createcreateForm(): void {
    // create form controls dynamically from model class
    this.createForm = this.formBuilder.group({
      createdUserId: [this.loggedUser.userId, Validators.required],
      supervisorMessageGroupTypeId: [Validators.required],
      messages: ['', Validators.required],
      supervisorId: [this.loggedUser.userId, Validators.required],
      messagingendTime: [new Date()]
    });

  }


  getMainAreaDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_GROUP_MESSAGES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropDown = response.resources;

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onchangeMessageGroup(event) {

    this.dropDownData = event.value
    this.dropDownDataValue = event;
  }

  getcreatDetailsById(creatId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GROUP_MESSAGES, creatId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.createDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.createDetails?.divisionName ? this.title +' - '+ this.createDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
          this.createForm.patchValue(this.createDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onSubmit(): void {
    if (this.createForm.invalid) {
      return;
    }
    let formValue = this.createForm.value;

    formValue.supervisorMessageGroupTypeId = this.dropDownData
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GROUP_MESSAGES_POST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/supervisor-dashboard/group-messages');
      }
    })
  }

  onCRUDRequested(type){}

}