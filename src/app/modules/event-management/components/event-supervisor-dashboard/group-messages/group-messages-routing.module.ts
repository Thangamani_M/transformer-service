import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupMessagesAddEditComponent } from './group-messages-add-edit.component';
import { GroupMessagesListComponent } from './group-messages-list.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: GroupMessagesListComponent, data: { title: 'Group-message List' } },
  { path: 'add', component: GroupMessagesAddEditComponent, data: { title: 'Group-message Add' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class GroupMessagesRoutingModule { }
