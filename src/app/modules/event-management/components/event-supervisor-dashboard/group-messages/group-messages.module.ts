import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatTabsModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { GroupMessagesAddEditComponent } from './group-messages-add-edit.component';
import { GroupMessagesListComponent } from './group-messages-list.component';
import { GroupMessagesRoutingModule } from './group-messages-routing.module';



@NgModule({
  declarations: [GroupMessagesAddEditComponent, GroupMessagesListComponent],
  imports: [
    CommonModule,
    GroupMessagesRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatTabsModule
  ]
})
export class GroupMessagesModule { }
