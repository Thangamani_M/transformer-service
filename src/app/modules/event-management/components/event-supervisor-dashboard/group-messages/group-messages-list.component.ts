import { DatePipe } from '@angular/common';
import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-group-messages-list',
  templateUrl: './group-messages-list.component.html',
})
export class GroupMessagesListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};

  constructor(private router: Router, private rxjsService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private momentService: MomentService,private store: Store<AppState>,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Group Message",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Group Message' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Group Message',
            dataKey: 'supervisorId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'messagingendTime', header: 'Date Time',isDate:true  },
            { field: 'sentby', header: 'Sent By' },
            { field: 'groupName', header: 'Group Name' },
            { field: 'messages', header: 'Messages' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.GROUP_MESSAGES,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getGroupMessageList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getGroupMessageList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
            SupervisorId: this.loggedInUserData.userId,
          };
          if (otherParams) {
            otherParams = { ...otherParams, ...obj1 };
          } else {
            otherParams = obj1;
          }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
        }
        return res;
      })).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getGroupMessageList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl("event-management/supervisor-dashboard/group-messages/add");
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}

