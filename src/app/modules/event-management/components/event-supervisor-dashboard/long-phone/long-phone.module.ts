import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LongPhoneRoutingModule } from './long-phone-routing.module';
import { LongPhoneComponent } from './long-phone.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';


@NgModule({
  declarations: [LongPhoneComponent],
  imports: [
    CommonModule,
    LongPhoneRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LongPhoneModule { }
