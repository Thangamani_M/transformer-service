import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LongPhoneComponent } from './long-phone.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: LongPhoneComponent, data: { title: 'Long phone List' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class LongPhoneRoutingModule { }
