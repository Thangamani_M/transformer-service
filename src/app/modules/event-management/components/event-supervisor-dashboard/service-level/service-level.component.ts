import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-service-level',
  templateUrl: './service-level.component.html',
})
export class ServiceLevelComponent implements OnInit {
  filterData: any;
  refresh: number = 0;
  export: number = 0;
  constructor(private activatedRoute: ActivatedRoute, private router:Router) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;
    });
  }

  ngOnInit() {
  }
  exportRequest() {
    this.export++;
  }

  refreshRequest() {
    this.refresh++;
  }

  navigateHourly(){
    this.router.navigate(["/event-management/supervisor-dashboard"], { queryParams: { tab: 6,hourlyChildTab:8}, state: { example: this.filterData} });
  }
}
