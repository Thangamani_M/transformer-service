import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceLevelRoutingModule } from './service-level-routing.module';
import { ServiceLevelComponent } from './service-level.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { ServiceLevelTotalListComponent } from './service-level-total-list.component';
import { ServiceLevelListComponent } from './service-level-list.component';


@NgModule({
  declarations: [ServiceLevelComponent, ServiceLevelTotalListComponent, ServiceLevelListComponent],
  imports: [
    CommonModule,
    ServiceLevelRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ServiceLevelModule { }
