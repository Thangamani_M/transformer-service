import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceLevelComponent } from './service-level.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ServiceLevelComponent, data: { title: 'Service Level' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ServiceLevelRoutingModule { }
