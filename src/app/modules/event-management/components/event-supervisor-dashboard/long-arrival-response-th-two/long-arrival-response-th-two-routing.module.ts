import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LongArrivalResponseThTwoComponent } from './long-arrival-response-th-two.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: LongArrivalResponseThTwoComponent, data: { title: 'Long Arrival Response Th two' } }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class LongArrivalResponseThTwoRoutingModule { }
