import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LongArrivalResponseThTwoRoutingModule } from './long-arrival-response-th-two-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { LongArrivalResponseThTwoComponent } from './long-arrival-response-th-two.component';


@NgModule({
  declarations: [LongArrivalResponseThTwoComponent],
  imports: [
    CommonModule,
    LongArrivalResponseThTwoRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LongArrivalResponseThTwoModule { }
