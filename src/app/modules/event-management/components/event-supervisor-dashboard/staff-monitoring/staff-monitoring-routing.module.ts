import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffMonitoringViewComponent } from './staff-monitoring-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: StaffMonitoringViewComponent, data: { title: 'Staff Monitoring Add/Edit' } },
  { path: 'view', component: StaffMonitoringViewComponent, data: { title: 'Staff Monitoring View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StaffMonitoringRoutingModule { }
