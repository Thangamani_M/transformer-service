import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { AreaDistributionModel, ExcludeAlarmListModel, IncludeAlarmListModel } from '../models/ara-distribution-details.model';
import { OperatorInteractionAreaDistrubutioncomponentComponent } from '../operator-interaction-area-distrubutioncomponent/operator-interaction-area-distrubutioncomponent.component';

@Component({
  selector: 'app-staff-monitoring-view',
  templateUrl: './staff-monitoring-view.component.html'
})
export class StaffMonitoringViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  stackConfigId: string;
  stackAreaConfigId: string;
  stackMonitoringDetails: any;
  distributionDialog: boolean = false;
  distributionForm: FormGroup;
  loggedUser: any;
  distributionDetails: any;
  stackAreaConfigList: any = [];
  stackConfigList: any = [];
  alarmTypeList: any = [];
  staffTypeList: any = [];
  today: any = new Date();
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private momentService: MomentService,
    private httpCancelService: HttpCancelService,
    private _fb: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    public dialogService: DialogService) {
    super();
    this.stackConfigId = this.activatedRoute.snapshot.queryParams.stackConfigId
    this.stackAreaConfigId = this.activatedRoute.snapshot.queryParams.stackAreaConfigId

    this.primengTableConfigProperties = {
      tableCaption: 'View Staff Monitoring',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Staff Monitoring',relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 1 }}, { displayName: 'Operator',relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 1 }},{displayName:'',}],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal Management Stack Operator',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'operatorName', header: 'Operator' }, { field: 'dispatcherName', header: 'Dispatcher' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.STACK_MONITORING_SUPPERVISOR_VIEW_DETAILS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            ebableFilterActionBtn: false
          },


        ]

      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {

    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_STACK_AREA_CONFIG),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_STACK_CONFIG),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_ALARM_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_STAFF_TYPE),];
    this.loadActionTypes(dropdownsAndData);
    this.createDistributionForm()
    this.getStaffMonitoringById();
  }


  ngAfterViewInit(): void {
  }

  exportExcel() { }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.stackAreaConfigList = resp.resources;
              break;
            case 1:
              this.stackConfigList = resp.resources;
              break;
            case 2:
              resp.resources.forEach(element => {
                let data = { label: element.displayName, value: { alarmTypeId: element.id } }
                this.alarmTypeList.push(data)
              });
              break;
            case 3:
              this.staffTypeList = resp.resources;
              break;
          }


        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createDistributionForm() {
    let cmcSmsGroupModel = new AreaDistributionModel();
    this.distributionForm = this._fb.group({
    });
    Object.keys(cmcSmsGroupModel).forEach((key) => {
      this.distributionForm.addControl(key, new FormControl(cmcSmsGroupModel[key]));
    });
    this.distributionForm = setRequiredValidator(this.distributionForm, ["staffTypeId", "stackConfigId", "stackAreaConfigId"]);
    this.distributionForm.get('userId').setValue(this.loggedUser.userId)
    this.distributionForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.distributionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  //Create FormArray
  get getstackDistributionIncludeAlarmTypeListArray(): FormArray {
    if (!this.distributionForm) return;
    return this.distributionForm.get("stackDistributionIncludeAlarmTypeList") as FormArray;
  }

  get getstackDistributionExcludeAlarmTypeListArray(): FormArray {
    if (!this.distributionForm) return;
    return this.distributionForm.get("stackDistributionExcludeAlarmTypeList") as FormArray;
  }


  //Create FormArray controls
  createstackDistributionIncludeAlarmTypeListModel(cmcSmsGroupEmployeeListModel?: ExcludeAlarmListModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new ExcludeAlarmListModel(cmcSmsGroupEmployeeListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }, [Validators.required]]
    });
    return this._fb.group(formControls);
  }
  //Create FormArray controls
  createstackDistributionExcludeAlarmTypeListModel(cmcSmsGroupEmployeeListModel?: IncludeAlarmListModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new IncludeAlarmListModel(cmcSmsGroupEmployeeListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }, [Validators.required]]
      
    });
    return this._fb.group(formControls);
  }




  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getStaffMonitoringById(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(row, unknownVar)
        break;
    }
  }


  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  openDistributionDialog(row, unknownVar) {
    this.distributionDialog = true
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_DISTRIBUTION_OPERATOR, unknownVar == '0' ? row.operatorUserId : row.dispatcherUserId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          let cmcSmsGroup = new AreaDistributionModel(response.resources);

          this.distributionDetails = response.resources;
          this.distributionForm.patchValue(cmcSmsGroup);
          this.distributionForm.get('userId').setValue(unknownVar == '0' ? row.operatorUserId : row.dispatcherUserId)
          this.distributionForm.get('createdUserId').setValue(this.loggedUser.userId)
          this.distributionForm.get('modifiedUserId').setValue(this.loggedUser.userId)

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getStaffMonitoringById(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    const params = { stackConfigId: this.stackConfigId, stackAreaConfigId: this.stackAreaConfigId }
    otherParams = { ...otherParams, ...params };

    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.STACK_MONITORING_SUPPERVISOR_VIEW_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.stackMonitoringDetails = data.resources;
        this.primengTableConfigProperties.breadCrumbItems[3].displayName = "View --" + this.stackMonitoringDetails?.areaName;
        this.dataList = data.resources.stackMonitoringSupervisorViewUserList;
        this.totalRecords = this.dataList.length;
      } else {

        this.dataList = null
        this.totalRecords = 0;

      }


    });
  }

  openAddEditPage(row?: any, editableObject?: any | string): void {
    const ref = this.dialogService.open(OperatorInteractionAreaDistrubutioncomponentComponent, {
      showHeader: false,
      baseZIndex: 1000,
      width: '850px',
      data: { ...editableObject, row: row, header: 'Area Distrubution Dialog', },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
      }
    });
  }

  onSubmitDistribution() {
    if (this.distributionForm.invalid) {
      return;
    }

    let formValue = this.distributionForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_DISTRIBUTION, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.distributionDialog = false
      }
    })
  }


}
