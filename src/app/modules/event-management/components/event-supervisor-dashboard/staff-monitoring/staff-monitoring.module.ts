import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StaffMonitoringRoutingModule } from './staff-monitoring-routing.module';
import { StaffMonitoringViewComponent } from './staff-monitoring-view.component';



@NgModule({
  declarations: [StaffMonitoringViewComponent],
  imports: [
    CommonModule,
    StaffMonitoringRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ]
})
export class StaffMonitoringModule { }
