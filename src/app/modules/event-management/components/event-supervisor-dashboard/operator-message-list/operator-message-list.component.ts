import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { Router } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-operator-message-list',
  templateUrl: './operator-message-list.component.html',
})
export class OperatorMessageListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() refresh:any;

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService,
    private router: Router) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Long Standoff",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Long Standoff' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Long Standoff',
            dataKey: 'supervisorMessageId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'operatorName', header: 'Operator' }, { field: 'message', header: 'Message' },
            { field: 'messageDateTime', header: 'Received At', isDate:true },
            { field: 'isActive', header: 'Status' },
            { field: 'readDateTime', header: 'Acknowledged At',isDate:true }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.SUPERVISOR_MESSAGE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
   // this.getSiterelatedMessage();
  }

  ngOnChanges(changes: SimpleChange): void {
    if (changes) {
      if (changes['refresh']) {
        this.getSiterelatedMessage()
      }
    }
  }

  getSiterelatedMessage(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (otherParams) {
      otherParams['isSiteMessage'] = false
    } else {
      otherParams = {}
      otherParams['isSiteMessage'] = false
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          // res?.resources?.forEach(val => {
          //   val.messageDateTime = val.messageDateTime ? this.datePipe.transform(val.messageDateTime, 'dd-MM-yyyy h:mm a') : '';
          //   val.readDateTime = val.readDateTime ? this.datePipe.transform(val.readDateTime, 'dd-MM-yyyy h:mm a') : '';
          //   return val;
          // })
        }
        return res;
      })).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSiterelatedMessage(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
      case CrudType.VIEW:
        this.router.navigate(['customer/manage-customers/view/' + row['customerId']], { queryParams: { addressId: row['customerAddressId'] } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}