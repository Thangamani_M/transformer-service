import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { LoginNonArRoutingModule } from './login-non-ar-routing.module';
import { LoginNonArComponent } from './login-non-ar.component';




@NgModule({
  declarations: [LoginNonArComponent],
  imports: [
    CommonModule,
    LoginNonArRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class LoginNonArModule { }
