import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginNonArComponent } from './login-non-ar.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: LoginNonArComponent, data: { title: 'Login Non-AR' } },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class LoginNonArRoutingModule { }
