import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-data-integrity-process-failure',
  templateUrl: './data-integrity-process-failure.component.html',
})
export class DataIntegrityProcessFailureComponent {

  filterData: any;
  refresh: number = 0
  export: number = 0
  
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;
    });
  }

  exportRequest() {
    this.export++
  }

  refreshRequest() {
    this.refresh++
  }
  navigateHourly(){
    this.router.navigate(["/event-management/supervisor-dashboard"], { queryParams: { tab: 6,hourlyChildTab:11}, state: { example: this.filterData} });
  }

}






