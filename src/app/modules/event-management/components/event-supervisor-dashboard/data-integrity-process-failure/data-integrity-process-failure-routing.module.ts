import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataIntegrityProcessFailureDetailsComponent } from './data-integrity-process-failure-details/data-integrity-process-failure-details.component';
import { DataIntegrityProcessFailureComponent } from './data-integrity-process-failure/data-integrity-process-failure.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: DataIntegrityProcessFailureComponent, data: { title: 'Data Integrity Process Failure List' } },
  { path: 'view', component: DataIntegrityProcessFailureDetailsComponent, data: { title: 'Data Integrity Process Failure Details' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DataIntegrityProcessFailureRoutingModule { }
