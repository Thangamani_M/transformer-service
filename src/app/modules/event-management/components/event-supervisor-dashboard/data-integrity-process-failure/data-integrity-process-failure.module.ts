import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataIntegrityProcessFailureRoutingModule } from './data-integrity-process-failure-routing.module';
import { DataIntegrityProcessFailureDetailsComponent } from './data-integrity-process-failure-details/data-integrity-process-failure-details.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { DataIntegrityProcessFailureComponent } from './data-integrity-process-failure/data-integrity-process-failure.component';
import { DataIntegrityProcessFailureListComponent } from './data-integrity-process-failure/data-integrity-process-failure-list.component';
import { DataIntegrityProcessFailureTotalListComponent } from './data-integrity-process-failure/data-integrity-process-failure-total-list.component';
import { NoPasswordNoResponseListComponent } from './data-integrity-process-failure-details/no-password-no-response-list.component';
import { FinisheInFifteenNoPasswordListComponent } from './data-integrity-process-failure-details/finishe-in-fifteen-no-password-list.component';
import { ResponseLoggedAfterCanceledListComponent } from './data-integrity-process-failure-details/response-logged-after-canceled-list.component';
import { FinishedInFourMinutesListComponent } from './data-integrity-process-failure-details/finished-in-four-minutes-list.component';

@NgModule({
  declarations: [DataIntegrityProcessFailureListComponent, DataIntegrityProcessFailureDetailsComponent, DataIntegrityProcessFailureTotalListComponent, DataIntegrityProcessFailureComponent, NoPasswordNoResponseListComponent, FinisheInFifteenNoPasswordListComponent, ResponseLoggedAfterCanceledListComponent, FinishedInFourMinutesListComponent],
  imports: [
    CommonModule,
    DataIntegrityProcessFailureRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DataIntegrityProcessFailureModule { }
