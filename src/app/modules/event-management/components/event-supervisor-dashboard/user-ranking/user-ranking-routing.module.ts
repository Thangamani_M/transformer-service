import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRankingComponent } from './user-ranking.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: UserRankingComponent, data: { title: 'User Ranking' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class UserRankingRoutingModule { }
