import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRankingRoutingModule } from './user-ranking-routing.module';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRankingComponent } from './user-ranking.component';


@NgModule({
  declarations: [UserRankingComponent],
  imports: [
    CommonModule,
    UserRankingRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class UserRankingModule { }
