import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { VasDashboardViewComponent } from './vas-dashboard-view.component';


@NgModule({
  declarations: [VasDashboardViewComponent],
  imports: [
    CommonModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
        { path: '', component: VasDashboardViewComponent, data: { title: 'VAS Dashboard Manager View' } }
    ])
  ]
})
export class VasDashboardViewModule { }
