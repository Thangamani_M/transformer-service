import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CrudService, CrudType, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared';
import { forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-vas-dashboard-view',
  templateUrl: './vas-dashboard-view.component.html',
  // styleUrls: ['./vas-dashboard-view.component.scss']
})
export class VasDashboardViewComponent extends PrimeNgTableVariablesModel {

  minuteList: any = [];
  listSubscribtion: any;
  vasViewDetail: any = [];
  showFilterForm: boolean;
  vasViewFilterForm: FormGroup;

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "VAS Manager Dashboard View",
      breadCrumbItems: [{ displayName: 'Dashboard', relativeRouterUrl: '' }, { displayName: 'Supervisor Dashboard: Activity', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 2 } }, { displayName: 'VAS Dashboard Manager View' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'VAS Dashboard View',
            dataKey: 'rowNumber',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'rowNumber', header: 'ID', width: '100px' },
              { field: 'dialDateTime', header: 'Entry Date Time', width: '140px', isDateTime: true },
              { field: 'occurrenceBookNumber', header: 'Event ID', width: '140px' },
              { field: 'customerNumber', header: 'Customer ID', width: '140px' },
              { field: 'notes', header: 'Notes', width: '150px' },
              { field: 'callOutCome', header: 'Call Outcome', width: '110px', },
              { field: 'jsonResponse', header: 'JSON Result', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
            enableRefreshBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.VAS_MANAGER_DASHBOARD_DIAL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            disabled: true
          },
        ]
      }
    };
    this.onShowValue();
  }

  ngOnInit(): void {
    this.createFilterForm();
    this.onLoadDropdown();
  }

  createFilterForm() {
    this.vasViewFilterForm = new FormGroup({
      minute: new FormControl(''),
    })
    this.vasViewFilterForm?.get('minute').valueChanges.subscribe((res: any) => {
      if (res) {
        this.submitFilter();
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.REFRESH:
        this.showFilterForm = false;
        this.submitFilter();
        break;
      default:
    }
  }

  onLoadDropdown() {
    this.loading = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_MINUTE, '5/5/12')
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.minuteList = getPDropdownData(response?.resources);
          this.vasViewFilterForm?.get('minute').setValue(this.minuteList[0]?.value);
          this.submitFilter();
        } else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }

  submitFilter() {
    this.rxjsService.setGlobalLoaderProperty(true);
    const api = [this.getTableAPI('0', '10', { minutes: this.vasViewFilterForm?.get('minute').value }),
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VAS_MANAGER_DASHBOARD_SUMMARY,
      this.vasViewFilterForm?.get('minute').value).pipe(map(result => result), catchError(error => of(error)))];
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.onBindTableData(resp);
              break;
            case 1:
              this.onShowValue(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getTableAPI(pageIndex?: string, pageSize?: string, otherParams?: object) {
    const module = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName
    const eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    return this.crudService.get(module, eventMgntModuleApiSuffixModels, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).pipe(map(result => result), catchError(error => of(error)))
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = {...otherParams, ...this.vasViewFilterForm?.value};
    this.loading = true;
    if (this.listSubscribtion && !this.listSubscribtion?.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.getTableAPI(pageIndex, pageSize, otherParams).subscribe((response: IApplicationResponse) => {
      this.onBindTableData(response);
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onBindTableData(response: IApplicationResponse) {
    if (response?.isSuccess) {
      this.dataList = response?.resources;
      this.totalRecords = response?.totalCount;
    } else {
      this.dataList = null;
      this.totalRecords = 0;
    }
    this.reset = false;
    this.loading = false;
  }

  onShowValue(response?: IApplicationResponse) {
    const labelValueWidth = { labelWidth: 'calc(100% - 100px)', valueWidth: '100px' };
    const greenclassName = 'status-label-green text-center p-2';
    const yellowclassName = 'status-label-yellow text-center p-2';
    const redclassName = 'status-label-light-red text-center p-2';
    this.vasViewDetail = [
      { name: 'Total Lines Available', value: response?.resources ? response?.resources?.totalLinesAvailable : '', ...labelValueWidth, statusClass: greenclassName },
      { name: 'Number of Calls', value: response?.resources ? response?.resources?.numberOfCalls : '', ...labelValueWidth, statusClass: yellowclassName },
      { name: 'Client Request Arrival', value: response?.resources ? response?.resources?.clientRequestArrivalCount : '', ...labelValueWidth, statusClass: redclassName },
      { name: 'Timeout Reached', value: response?.resources ? response?.resources?.timeoutReached : '', ...labelValueWidth, statusClass: redclassName },
      { name: 'Incorrect Password', value: response?.resources ? response?.resources?.incorrectPassword : '', ...labelValueWidth, statusClass: redclassName },
      { name: 'Client Cancelled', value: response?.resources ? response?.resources?.cancellationSuccessfull : '', ...labelValueWidth, statusClass: redclassName },
      { name: 'No Contact Made', value: response?.resources ? response?.resources?.noContactMade : '', ...labelValueWidth, statusClass: redclassName },
      { name: 'No Line Available', value: response?.resources ? response?.resources?.noAvailableAgent : '', ...labelValueWidth, statusClass: redclassName },
      { name: 'Comms Error to ADS', value: response?.resources ? response?.resources?.commsErrorToADS : '', ...labelValueWidth, statusClass: redclassName },
    ];
  }

  ngOnDestory() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
