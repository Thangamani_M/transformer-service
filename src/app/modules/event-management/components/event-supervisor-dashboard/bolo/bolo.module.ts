import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoloRoutingModule } from './bolo-routing.module';
import { BoloAddEditComponent } from './bolo-add-edit.component';
import { BoloViewComponent } from './bolo-view.component';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatTabsModule } from '@angular/material';
import { BoloListComponent } from './bolo-list.component';
import { GMapModule } from 'primeng/gmap';


@NgModule({
  declarations: [BoloAddEditComponent, BoloViewComponent, BoloListComponent],
  imports: [
    CommonModule,
    BoloRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatTabsModule,
    GMapModule,
  ]
})
export class BoloModule { }
