import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoloAddEditComponent } from './bolo-add-edit.component';
import { BoloListComponent } from './bolo-list.component';
import { BoloViewComponent } from './bolo-view.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: BoloListComponent, data: { title: 'BOLO List' } },
  { path: 'add-edit', component: BoloAddEditComponent, data: { title: 'BOLO Add/Edit' } },
  { path: 'view', component: BoloViewComponent, data: { title: 'BOLO View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class BoloRoutingModule { }
