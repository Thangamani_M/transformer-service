import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

declare var google: any;
@Component({
  selector: 'app-bolo-add-edit',
  templateUrl: './bolo-add-edit.component.html',
  styleUrls: ['./bolo-add-edit.component.scss']
})
export class BoloAddEditComponent implements OnInit {

  map: any;
  overlays
  options: any
  openMapDialog: boolean = false;
  boloId: any;
  boloDetails: any;
  mainAreaDropDown: any = [];
  subAreaDropdown: any = [];
  loggedUser: any;
  boloForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  lat: any;
  lang: any;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.boloId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.map = google.maps.Map;
    this.title =this.boloId ? 'Update BOLO':'Add BOLO';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' },
      { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } },{ displayName: 'Create BOLO', relativeRouterUrl: '/event-management/supervisor-dashboard/bolo' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.boloId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/supervisor-dashboard/bolo/view' , queryParams: {id: this.boloId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createboloForm();
    this.getMainAreaDropdown();
    this.options = {
      center: {
        lat: -25.746020,
        lng: 28.187120
      },
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    if (this.boloId) {
      this.getboloDetailsById(this.boloId);
      return
    }
    this.omit_number(event);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  omit_number(event) {
    var key;
    key = event.charCode;  //         key = event.keyCode;  (Both can be used)
    return ((key > 47 && key < 58) || key == 45 || key == 46);
  }

  createboloForm(): void {
    // create form controls dynamically from model class
    this.boloForm = this.formBuilder.group({
      boloId: [''],
      mainAreaId: ['', Validators.required],
      subAreaId: ['', Validators.required],
      latitude: ['', Validators.required],
      longitude: ['', Validators.required],
      message: ['', Validators.required],
      submittedById: ['', Validators.required],
      createdUserId: ['', Validators.required],
      modifiedUserId: ['', Validators.required],
      submittedDateTime: [new Date().toISOString()],
      isActive: [true],
    });
    this.boloForm = setRequiredValidator(this.boloForm, ["mainAreaId", "subAreaId", "latitude", "longitude"]);
    this.boloForm.get('submittedById').setValue(this.loggedUser.userId)
    this.boloForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.boloForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.boloForm.get('mainAreaId').valueChanges.subscribe(val => {
      if (val) {
       this.getLotitudeAndLongitude(val)
      }
    })
    this.boloForm.get('subAreaId').valueChanges.subscribe(val => {
      if (val) {
        let stackNameData = this.subAreaDropdown.find(e => e.id == val);
        let coordinate = stackNameData?.coordinates
        let coordinates = coordinate.split(',')
          this.boloForm.get('longitude').setValue(coordinates[0])
          this.boloForm.get('latitude').setValue(coordinates[1])
      }
    })
    if (!this.boloId) {
      this.boloForm.removeControl('boloId');
      this.boloForm.removeControl('modifiedUserId');
    } else {
      this.boloForm.removeControl('createdUserId');
    }
  }

  openMap(event,event1) {
    this.options = {};
    this.overlays = {};
    let location = [event1,event]
    this.openMapDialog = true
    if (location) {
    } else {
      location = []
      location[0] = -25.746020 // fidelity south afcia coordinates by default
      location[1] = 28.187120 // fidelity south afcia coordinates by default
    }
    this.options = {
      center: { lat: Number(location[0]), lng: Number(location[1]) },
      zoom: 12
    };

    setTimeout(() => {
      this.map.setCenter({
        lat: Number(this.lat? this.lat:location[0]),
        lng: Number( this.lang? this.lang:location[1])
      });
    }, 500);

    this.overlays = [new google.maps.Marker({ position: { lat: Number(this.lat? this.lat:location[0]), lng: Number(this.lang?this.lang:location[1]) }, icon: "assets/img/map-icon.png" })];
  }


  handleMapClick(event) {
    this.boloForm.get('latitude').setValue(event.latLng.lat())
    this.boloForm.get('longitude').setValue(event.latLng.lng())
    this.lat = event.latLng.lat()
    this.lang = event.latLng.lng()
    this.openMapDialog = false
    this.rxjsService.setFormChangeDetectionProperty(true)

  }

  setMap(event) {
    this.map = event.map;
  }

  getMainAreaDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_MAIN_AREA_ALL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getLotitudeAndLongitude(mainAreaId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SUB_SRES_COORDINATE, mainAreaId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subAreaDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getboloDetailsById(boloId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOLO, boloId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.boloDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.boloDetails?.mainArea ? this.title +' - '+ this.boloDetails?.mainArea : this.title + ' --', relativeRouterUrl: '' });
          this.boloForm.patchValue(this.boloDetails);
          this.boloForm.get('subAreaId').setValue(this.boloDetails.subAreaId)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.boloForm.invalid) {
      return;
    }
    let formValue = this.boloForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.boloId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOLO, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOLO, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/supervisor-dashboard/bolo');
      }
    })
  }

  onCRUDRequested(type){}

}
