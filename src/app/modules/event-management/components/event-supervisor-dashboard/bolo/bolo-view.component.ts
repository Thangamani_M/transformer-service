import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-bolo-view',
  templateUrl: './bolo-view.component.html',
})
export class BoloViewComponent implements OnInit {

  boloId: any
  boloViewDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.boloId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View BOLO',
      selectedTabIndex: 0,
      breadCrumbItems: [
        { displayName: 'Supervisor Dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } },
        { displayName: 'BOLO', relativeRouterUrl: '/event-management/supervisor-dashboard/bolo/list' }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.boloId) {
      this.getboloViewDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.boloViewDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems[3].displayName = "View --" + this.boloViewDetails.mainArea;
        this.boloViewDetails = [
          { name: 'Main Area', value: response.resources.mainArea },
          { name: 'Sub Area', value: response.resources.subArea },
          { name: 'Latitude', value: response.resources.latitude },
          { name: 'Longitude', value: response.resources.longitude },
          { name: 'Message', value: response.resources.message },
          { name: 'Created On', value: response.resources?.createdDate, isDateTime: true },
          { name: 'Modified On', value: response.resources?.modifiedDate, isDateTime: true },
          { name: 'Created By', value: response.resources?.createdUserName },
          { name: 'Modified By', value: response.resources?.modifiedUserName },
          { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
        ];
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['event-management/supervisor-dashboard/bolo/add-edit'], { queryParams: { id: this.boloId } });
  }

  getboloViewDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.BOLO,
      this.boloId
    );
  }

}

