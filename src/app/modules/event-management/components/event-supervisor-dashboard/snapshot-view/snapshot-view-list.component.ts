import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-snapshot-view-list',
  templateUrl: './snapshot-view-list.component.html'
})
export class SnapshotViewListComponent extends PrimeNgTableVariablesModel implements OnInit {

primengTableConfigProperties: any;
row: any = {};
@Input() refresh:any;

constructor(
  private rxjsService: RxjsService, private crudService: CrudService,
  private momentService: MomentService) {
  super();
  this.primengTableConfigProperties = {
    tableCaption: "Volume",
    breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Hourly Stats', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 6, hourlyChildTab: 7 } }, { displayName: 'Long Phoneback' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Volume',
          dataKey: 'divisionId',
          captionFontSize: '21px',
          enableBreadCrumb: true,
          enableAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [{ field: 'mainArea', header: 'Main Area' },
                      { field: 'actionCount', header: 'Actions' },
                      { field: 'dispatchCount', header: 'Dispatches' },
                      { field: 'pendingCount', header: 'Pending Events' },
                      { field: 'pendingDispatchCount', header: 'Pending Dispatches' },
                      { field: 'finishedCount', header: 'Finished' },
                      { field: 'vehicleLoggedInCount', header: 'Vehicles Logged-In' }],
          apiSuffixModel: EventMgntModuleApiSuffixModels.SNAPSHOT_VIEW,
          moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          enableMultiDeleteActionBtn: false,
          enableAddActionBtn: false,
          shouldShowFilterActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportBtn: true,
          enableReloadBtn: true,
          enableExportCSV: false,
        }
      ]
    }
  }
}

ngOnInit(): void {
}

ngOnChanges(changes: SimpleChange): void {
  if (changes) {
    if (changes['refresh']) {
      this.getServiceMedianList()
    }
  }
}

getServiceMedianList(pageIndex?: string, pageSize?: string, otherParams?: object) {
  this.loading = true;
  let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
  eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
  this.crudService.get(
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
    eventMgntModuleApiSuffixModels,
    undefined,
    false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
  ).subscribe((data: IApplicationResponse) => {
    this.loading = false;
    this.rxjsService.setGlobalLoaderProperty(false);
    if (data.isSuccess) {
      this.dataList = data.resources;;
      this.totalRecords = data.totalCount;
    }
    else{
      this.dataList = null;
      this.totalRecords = 0;
    }
  })
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
  switch (type) {
    case CrudType.CREATE:
      break;
    case CrudType.GET:
      let otherParams = {};
      if (Object.keys(this.row).length > 0) {
        if (this.row['searchColumns']) {
          Object.keys(this.row['searchColumns']).forEach((key) => {
            if (key.toLowerCase().includes('date')) {
              otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
            } else {
              otherParams[key] = this.row['searchColumns'][key];
            }
          });
        }
        if (this.row['sortOrderColumn']) {
          otherParams['sortOrder'] = this.row['sortOrder'];
          otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
        }
      }
      this.getServiceMedianList(row["pageIndex"], row["pageSize"], unknownVar)
      break;
  }
}

onActionSubmited(e: any) {
  if (e.data && !e.search) {
    this.onCRUDRequested(e.type, e.data)
  } else if (e.data && e.search) {
    this.onCRUDRequested(e.type, e.data, e.search);
  } else if (e.type && !e.data) {
    this.onCRUDRequested(e.type, {})
  }
}

onChangeSelecedRows(e) {
  this.selectedRows = e;
}

}
