import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatTabsModule } from '@angular/material';
import { SupervisorLongStandoffComponent } from './supervisor-long-standoff.component';
import { SupervisorLongStandoffRoutingModule } from './supervisor-long-standoff-routing.module';


@NgModule({
  declarations: [SupervisorLongStandoffComponent],
  imports: [
    CommonModule,
    SupervisorLongStandoffRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatTabsModule
  ]
})
export class SupervisorLongStandoffModule { }
