import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupervisorLongStandoffComponent } from './supervisor-long-standoff.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: SupervisorLongStandoffComponent, data: { title: 'Long Standoff' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SupervisorLongStandoffRoutingModule { }
