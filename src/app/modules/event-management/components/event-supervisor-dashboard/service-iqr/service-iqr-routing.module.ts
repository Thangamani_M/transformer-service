import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServiceIqrComponent } from './service-iqr.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ServiceIqrComponent, data: { title: 'Service Median' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class ServiceIqrRoutingModule { }
