import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { ServiceIqrListComponent } from './service-iqr-list.component';
import { ServiceIqrRoutingModule } from './service-iqr-routing.module';
import { ServiceIqrTotalListComponent } from './service-iqr-total-list.component';
import { ServiceIqrComponent } from './service-iqr.component';



@NgModule({
  declarations: [ServiceIqrComponent, ServiceIqrListComponent, ServiceIqrTotalListComponent],
  imports: [
    CommonModule,
    ServiceIqrRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ServiceIqrModule { }
