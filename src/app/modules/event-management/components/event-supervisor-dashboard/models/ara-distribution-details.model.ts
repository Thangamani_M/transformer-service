class AreaDistributionModel {
    constructor(cmcSmsGroupModel?: AreaDistributionModel) {
        this.shiftPatternId = cmcSmsGroupModel ? cmcSmsGroupModel.shiftPatternId == undefined ? '' : cmcSmsGroupModel.shiftPatternId : '';
        this.stackDistributionIncludeAlarmTypeList = cmcSmsGroupModel ? cmcSmsGroupModel.stackDistributionIncludeAlarmTypeList == undefined ? [] : cmcSmsGroupModel.stackDistributionIncludeAlarmTypeList : [];
        this.stackDistributionExcludeAlarmTypeList = cmcSmsGroupModel ? cmcSmsGroupModel.stackDistributionExcludeAlarmTypeList == undefined ? [] : cmcSmsGroupModel.stackDistributionExcludeAlarmTypeList : [];
        this.staffTypeId = cmcSmsGroupModel ? cmcSmsGroupModel.staffTypeId == undefined ? "" : cmcSmsGroupModel.staffTypeId : "";
        this.stackConfigId = cmcSmsGroupModel ? cmcSmsGroupModel.stackConfigId == undefined ? "" : cmcSmsGroupModel.stackConfigId : "";
        this.stackAreaConfigId = cmcSmsGroupModel ? cmcSmsGroupModel.stackAreaConfigId == undefined ? "" : cmcSmsGroupModel.stackAreaConfigId : "";
        this.comments = cmcSmsGroupModel ? cmcSmsGroupModel.comments == undefined ? "" : cmcSmsGroupModel.comments : "";
        this.isActive = cmcSmsGroupModel ? cmcSmsGroupModel.isActive == undefined ? true : cmcSmsGroupModel.isActive : true;
        this.userId = cmcSmsGroupModel ? cmcSmsGroupModel.userId == undefined ? null : cmcSmsGroupModel.userId : null;
        this.createdUserId = cmcSmsGroupModel ? cmcSmsGroupModel.createdUserId == undefined ? null : cmcSmsGroupModel.createdUserId : null;
        this.modifiedUserId = cmcSmsGroupModel ? cmcSmsGroupModel.modifiedUserId == undefined ? "" : cmcSmsGroupModel.modifiedUserId : "";

    }
    shiftPatternId?: string;
    stackDistributionIncludeAlarmTypeList:  IncludeAlarmListModel[]; 
    stackDistributionExcludeAlarmTypeList:  ExcludeAlarmListModel[];
    isActive: boolean;
    staffTypeId:string;
    stackConfigId:string;
    stackAreaConfigId:string;
    comments:string; 
    userId: string
    createdUserId: string;
    modifiedUserId: string;
}

  

class  IncludeAlarmListModel {
    constructor(cmcSmsGroupEmployeeListModel?:  IncludeAlarmListModel) {
        this.alarmTypeId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.alarmTypeId == undefined ? '' : cmcSmsGroupEmployeeListModel.alarmTypeId : '';
     }
    alarmTypeId:string;

}
class  ExcludeAlarmListModel {
    constructor(cmcSmsGroupEmployeeListModel?:  ExcludeAlarmListModel) {
        this.alarmTypeId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.alarmTypeId == undefined ? '' : cmcSmsGroupEmployeeListModel.alarmTypeId : '';
     }
    alarmTypeId:string;

}


export { AreaDistributionModel , IncludeAlarmListModel, ExcludeAlarmListModel}
