class CrimeDocketModel {
    constructor(crimeDocketModel?: CrimeDocketModel) {
        this.crimeDocketId = crimeDocketModel ? crimeDocketModel.crimeDocketId == undefined ? null : crimeDocketModel.crimeDocketId :null;
        this.occurrenceBookId = crimeDocketModel ? crimeDocketModel.occurrenceBookId == undefined ? '' : crimeDocketModel.occurrenceBookId : '';
        this.incidentypeId = crimeDocketModel ? crimeDocketModel.incidentypeId == undefined ? '' : crimeDocketModel.incidentypeId : '';
        this.signalHappensOnId = crimeDocketModel ? crimeDocketModel.signalHappensOnId == undefined ? '' : crimeDocketModel.signalHappensOnId : '';
        this.perimeterProtectionName = crimeDocketModel ? crimeDocketModel.perimeterProtectionName == undefined ? '' : crimeDocketModel.perimeterProtectionName : '';
        this.accessedOntoProperty = crimeDocketModel ? crimeDocketModel.accessedOntoProperty == undefined ? '' : crimeDocketModel.accessedOntoProperty : '';
        this.accessedIntoProperty = crimeDocketModel ? crimeDocketModel.accessedIntoProperty == undefined ? '' : crimeDocketModel.accessedIntoProperty : '';
        this.stationHappensOnId = crimeDocketModel ? crimeDocketModel.stationHappensOnId == undefined ? '' : crimeDocketModel.stationHappensOnId : '';
        this.sapsOfficerName = crimeDocketModel ? crimeDocketModel.sapsOfficerName == undefined ? '' : crimeDocketModel.sapsOfficerName : '';
        this.sapsOfficerRankName = crimeDocketModel ? crimeDocketModel.sapsOfficerRankName == undefined ? '' : crimeDocketModel.sapsOfficerRankName : '';
        this.caseNumber = crimeDocketModel ? crimeDocketModel.caseNumber == undefined ? '' : crimeDocketModel.caseNumber : '';
        this.lcrc = crimeDocketModel ? crimeDocketModel.lcrc == undefined ? '' : crimeDocketModel.lcrc : '';
        this.shotsFiredByFidelityADTCount = crimeDocketModel ? crimeDocketModel.shotsFiredByFidelityADTCount == undefined ? '' : crimeDocketModel.shotsFiredByFidelityADTCount : '';
        this.shotsFiredByClientCount = crimeDocketModel ? crimeDocketModel.shotsFiredByClientCount == undefined ? '' : crimeDocketModel.shotsFiredByClientCount : '';
        this.shotsFiredBySAPCCount = crimeDocketModel ? crimeDocketModel.shotsFiredBySAPCCount == undefined ? '' : crimeDocketModel.shotsFiredBySAPCCount : '';
        this.shotsFiredBySuspectsCount = crimeDocketModel ? crimeDocketModel.shotsFiredBySuspectsCount == undefined ? '' : crimeDocketModel.shotsFiredBySuspectsCount : '';
        this.suspectsCount = crimeDocketModel ? crimeDocketModel.suspectsCount == undefined ? '' : crimeDocketModel.suspectsCount : '';
        this.victimCount = crimeDocketModel ? crimeDocketModel.victimCount == undefined ? '' : crimeDocketModel.victimCount : '';
        this.restraintsDetails = crimeDocketModel ? crimeDocketModel.restraintsDetails == undefined ? '' : crimeDocketModel.restraintsDetails : '';
        this.injuryDetails = crimeDocketModel ? crimeDocketModel.injuryDetails == undefined ? '' : crimeDocketModel.injuryDetails : '';
        this.estimatedValue = crimeDocketModel ? crimeDocketModel.estimatedValue == undefined ? '' : crimeDocketModel.estimatedValue : '';
        this.vehicleCount = crimeDocketModel ? crimeDocketModel.vehicleCount == undefined ? '' : crimeDocketModel.vehicleCount : '';
        this.dogCount = crimeDocketModel ? crimeDocketModel.dogCount == undefined ? '' : crimeDocketModel.dogCount : '';
        this.takenVehicleCount = crimeDocketModel ? crimeDocketModel.takenVehicleCount == undefined ? '' : crimeDocketModel.takenVehicleCount : '';
        this.followedFrom = crimeDocketModel ? crimeDocketModel.followedFrom == undefined ? '' : crimeDocketModel.followedFrom : '';
        this.followedIntoDriveway = crimeDocketModel ? crimeDocketModel.followedIntoDriveway == undefined ? '' : crimeDocketModel.followedIntoDriveway : '';

        this.activationTime = crimeDocketModel ? crimeDocketModel.activationTime == undefined ? '' : crimeDocketModel.activationTime : '';
        this.gprsArrivalTime = crimeDocketModel ? crimeDocketModel.gprsArrivalTime == undefined ? '' : crimeDocketModel.gprsArrivalTime : '';
        this.responseTime = crimeDocketModel ? crimeDocketModel.responseTime == undefined ? '' : crimeDocketModel.responseTime : '';
        this.systemType = crimeDocketModel ? crimeDocketModel.systemType == undefined ? '' : crimeDocketModel.systemType : '';
        this.officerName = crimeDocketModel ? crimeDocketModel.officerName == undefined ? '' : crimeDocketModel.officerName : '';
        this.responseOfficerVehicleCallSign = crimeDocketModel ? crimeDocketModel.responseOfficerVehicleCallSign == undefined ? '' : crimeDocketModel.responseOfficerVehicleCallSign : '';
        this.vehicleRegistrationNumber = crimeDocketModel ? crimeDocketModel.vehicleRegistrationNumber == undefined ? '' : crimeDocketModel.vehicleRegistrationNumber : '';
        this.seniorName = crimeDocketModel ? crimeDocketModel.seniorName == undefined ? '' : crimeDocketModel.seniorName : '';
        
        
        this.isCCTVFootage = crimeDocketModel ? crimeDocketModel.isCCTVFootage == undefined ? false : crimeDocketModel.isCCTVFootage : false;
        this.isSystemActivatedBySenior = crimeDocketModel ? crimeDocketModel.isSystemActivatedBySenior == undefined ? false : crimeDocketModel.isSystemActivatedBySenior : false;
        this.isSystemArmedAtIncident = crimeDocketModel ? crimeDocketModel.isSystemArmedAtIncident == undefined ? false : crimeDocketModel.isSystemArmedAtIncident : false;
        this.isLoadShedding = crimeDocketModel ? crimeDocketModel.isLoadShedding == undefined ? false : crimeDocketModel.isLoadShedding : false;
        this.isSAPS = crimeDocketModel ? crimeDocketModel.isSAPS == undefined ? false : crimeDocketModel.isSAPS : false;
        this.isShotsFired = crimeDocketModel ? crimeDocketModel.isShotsFired == undefined ? false : crimeDocketModel.isShotsFired : false;
        this.isShotsFiredByFidelityADT = crimeDocketModel ? crimeDocketModel.isShotsFiredByFidelityADT == undefined ? false : crimeDocketModel.isShotsFiredByFidelityADT : false;
        this.isShotsFiredByClient = crimeDocketModel ? crimeDocketModel.isShotsFiredByClient == undefined ? false : crimeDocketModel.isShotsFiredByClient : false;
        this.isShotsFiredBySAPC = crimeDocketModel ? crimeDocketModel.isShotsFiredBySAPC == undefined ? false : crimeDocketModel.isShotsFiredBySAPC : false;
        this.isShotsFiredBySuspects = crimeDocketModel ? crimeDocketModel.isShotsFiredBySuspects == undefined ? false : crimeDocketModel.isShotsFiredBySuspects : false;
        this.isSuspects = crimeDocketModel ? crimeDocketModel.isSuspects == undefined ? false : crimeDocketModel.isSuspects : false;
        this.isVictimInvolved = crimeDocketModel ? crimeDocketModel.isVictimInvolved == undefined ? false : crimeDocketModel.isVictimInvolved : false;
        this.isRestraintsUsed = crimeDocketModel ? crimeDocketModel.isRestraintsUsed == undefined ? false : crimeDocketModel.isRestraintsUsed : false;
        this.isInjuries = crimeDocketModel ? crimeDocketModel.isInjuries == undefined ? false : crimeDocketModel.isInjuries : false;
        this.isItemsStolen = crimeDocketModel ? crimeDocketModel.isItemsStolen == undefined ? false : crimeDocketModel.isItemsStolen : false;
        this.isVehicle = crimeDocketModel ? crimeDocketModel.isVehicle == undefined ? false : crimeDocketModel.isVehicle : false;
        this.isDogsOnPremises = crimeDocketModel ? crimeDocketModel.isDogsOnPremises == undefined ? false : crimeDocketModel.isDogsOnPremises : false;
        this.isGardenerOnSite = crimeDocketModel ? crimeDocketModel.isGardenerOnSite == undefined ? false : crimeDocketModel.isGardenerOnSite : false;
        this.isDomesticStaffHeldUp = crimeDocketModel ? crimeDocketModel.isDomesticStaffHeldUp == undefined ? false : crimeDocketModel.isDomesticStaffHeldUp : false;
        this.isVehicleTakenFromPremises = crimeDocketModel ? crimeDocketModel.isVehicleTakenFromPremises == undefined ? false : crimeDocketModel.isVehicleTakenFromPremises : false;
        this.isClientFollowedHome = crimeDocketModel ? crimeDocketModel.isClientFollowedHome == undefined ? false : crimeDocketModel.isClientFollowedHome : false;


        this.crimeDocketSuspects = crimeDocketModel ? crimeDocketModel.crimeDocketSuspects == undefined ? null : crimeDocketModel.crimeDocketSuspects : null;
        this.createdUserId = crimeDocketModel ? crimeDocketModel.createdUserId == undefined ? null : crimeDocketModel.createdUserId : null;
        this.modifiedUserId = crimeDocketModel ? crimeDocketModel.modifiedUserId == undefined ? null : crimeDocketModel.modifiedUserId : null;
    }
    crimeDocketId?: string;
    occurrenceBookId: string;
    incidentypeId: string;
    signalHappensOnId: string;
    perimeterProtectionName: string;

    accessedOntoProperty: string;
    accessedIntoProperty: string;
    isCCTVFootage: boolean;
    isSystemActivatedBySenior: boolean;
    isSystemArmedAtIncident: boolean;
    isLoadShedding: boolean;
    isSAPS: boolean;
    stationHappensOnId: string;
    sapsOfficerName: string;
    sapsOfficerRankName: string;
    caseNumber: string;
    lcrc: string;
    isShotsFired: boolean;
    isShotsFiredByFidelityADT: boolean;
    shotsFiredByFidelityADTCount: string;
    isShotsFiredByClient: boolean;
    shotsFiredByClientCount: string;
    isShotsFiredBySAPC: boolean;
    shotsFiredBySAPCCount: string;
    isShotsFiredBySuspects: boolean;
    shotsFiredBySuspectsCount: string;
    isSuspects: boolean;
    suspectsCount: string;
    isVictimInvolved: boolean;
    victimCount: string;
    isRestraintsUsed: boolean;
    restraintsDetails: string;
    isInjuries: boolean;
    injuryDetails: string
    isItemsStolen: boolean;
    estimatedValue: string
    isVehicle: boolean;
    vehicleCount: string
    isDogsOnPremises: boolean;
    dogCount: string
    isGardenerOnSite: boolean;
    isDomesticStaffHeldUp: boolean;
    isVehicleTakenFromPremises: boolean;
    takenVehicleCount: string
    isClientFollowedHome: boolean;
    followedFrom: string
    followedIntoDriveway: string

    activationTime: string
    gprsArrivalTime:string
    responseTime:string
    systemType:string
    vehicleRegistrationNumber:string
    responseOfficerVehicleCallSign:string
    officerName:string
    seniorName:string

    crimeDocketSuspects: CrimeDocketSuspectsModel[];
    crimeDocketVictims: CrimeDocketVictimsModel[];
    crimeDocketStolenItems: CrimeDocketStolenItemsModel[];
    crimeDocketVehicles: CrimeDocketVehiclesModel[];
    crimeDocketDogs: CrimeDocketDogsModel[];
    crimeDocketTakenVehicles: CrimeDocketTakenVehiclesModel[];

    createdUserId: string
    modifiedUserId: string
}

class CrimeDocketSuspectsModel {
    constructor(crimeDocketModel?: CrimeDocketSuspectsModel) {
        this.isHeightUnknown = crimeDocketModel ? crimeDocketModel.isHeightUnknown == undefined ? false : crimeDocketModel.isHeightUnknown : false;
        this.isBuildUnknown = crimeDocketModel ? crimeDocketModel.isBuildUnknown == undefined ? false : crimeDocketModel.isBuildUnknown : false;
        this.isRaceUnknown = crimeDocketModel ? crimeDocketModel.isRaceUnknown == undefined ? false : crimeDocketModel.isRaceUnknown : false;
        this.isComplexionUnknown = crimeDocketModel ? crimeDocketModel.isComplexionUnknown == undefined ? false : crimeDocketModel.isComplexionUnknown : false;
        this.isGenderUnknown = crimeDocketModel ? crimeDocketModel.isGenderUnknown == undefined ? false : crimeDocketModel.isGenderUnknown : false;
        this.isAgeUnknown = crimeDocketModel ? crimeDocketModel.isAgeUnknown == undefined ? false : crimeDocketModel.isAgeUnknown : false;
        this.isFacialDescriptionUnknown = crimeDocketModel ? crimeDocketModel.isFacialDescriptionUnknown == undefined ? false : crimeDocketModel.isFacialDescriptionUnknown : false;
        this.isHeadGearUnknown = crimeDocketModel ? crimeDocketModel.isHeadGearUnknown == undefined ? false : crimeDocketModel.isHeadGearUnknown : false;
        this.isHairColorUnknown = crimeDocketModel ? crimeDocketModel.isHairColorUnknown == undefined ? false : crimeDocketModel.isHairColorUnknown : false;
        this.isHairLengthUnknown = crimeDocketModel ? crimeDocketModel.isHairLengthUnknown == undefined ? false : crimeDocketModel.isHairLengthUnknown : false;
        this.isGlassesUnknown = crimeDocketModel ? crimeDocketModel.isGlassesUnknown == undefined ? false : crimeDocketModel.isGlassesUnknown : false;
        this.isFacialHairUnknown = crimeDocketModel ? crimeDocketModel.isFacialHairUnknown == undefined ? false : crimeDocketModel.isFacialHairUnknown : false;
        this.isScarsUnknown = crimeDocketModel ? crimeDocketModel.isScarsUnknown == undefined ? false : crimeDocketModel.isScarsUnknown : false;
        this.isClothingUnknown = crimeDocketModel ? crimeDocketModel.isClothingUnknown == undefined ? false : crimeDocketModel.isClothingUnknown : false;
        this.isShirtUnknown = crimeDocketModel ? crimeDocketModel.isShirtUnknown == undefined ? false : crimeDocketModel.isShirtUnknown : false;
        this.isJacketUnknown = crimeDocketModel ? crimeDocketModel.isJacketUnknown == undefined ? false : crimeDocketModel.isJacketUnknown : false;
        this.isPantStyleUnknown = crimeDocketModel ? crimeDocketModel.isPantStyleUnknown == undefined ? false : crimeDocketModel.isPantStyleUnknown : false;
        this.isPantTypeUnknown = crimeDocketModel ? crimeDocketModel.isPantTypeUnknown == undefined ? false : crimeDocketModel.isPantTypeUnknown : false;
        this.isShoesUnknown = crimeDocketModel ? crimeDocketModel.isShoesUnknown == undefined ? false : crimeDocketModel.isShoesUnknown : false;
        this.isJewelleryUnknown = crimeDocketModel ? crimeDocketModel.isJewelleryUnknown == undefined ? false : crimeDocketModel.isJewelleryUnknown : false;
        this.isGlovesUnknownh = crimeDocketModel ? crimeDocketModel.isGlovesUnknownh == undefined ? false : crimeDocketModel.isGlovesUnknownh : false;
        this.isGloves = crimeDocketModel ? crimeDocketModel.isGloves == undefined ? false : crimeDocketModel.isGloves : false;
        this.isWeaponUnknown = crimeDocketModel ? crimeDocketModel.isWeaponUnknown == undefined ? false : crimeDocketModel.isWeaponUnknown : false;
        this.height = crimeDocketModel ? crimeDocketModel.height == undefined ? '' : crimeDocketModel.height : '';
        this.buildId = crimeDocketModel ? crimeDocketModel.buildId == undefined ? '' : crimeDocketModel.buildId : '';
        this.raceId = crimeDocketModel ? crimeDocketModel.raceId == undefined ? '' : crimeDocketModel.raceId : '';
        this.complexionId = crimeDocketModel ? crimeDocketModel.complexionId == undefined ? '' : crimeDocketModel.complexionId : '';
        this.genderId = crimeDocketModel ? crimeDocketModel.genderId == undefined ? '' : crimeDocketModel.genderId : '';
        this.age = crimeDocketModel ? crimeDocketModel.age == undefined ? '' : crimeDocketModel.age : '';
        this.facialDescription = crimeDocketModel ? crimeDocketModel.facialDescription == undefined ? '' : crimeDocketModel.facialDescription : '';
        this.headGearId = crimeDocketModel ? crimeDocketModel.headGearId == undefined ? '' : crimeDocketModel.headGearId : '';
        this.hairColorId = crimeDocketModel ? crimeDocketModel.hairColorId == undefined ? '' : crimeDocketModel.hairColorId : '';
        this.hairLengthId = crimeDocketModel ? crimeDocketModel.hairLengthId == undefined ? '' : crimeDocketModel.hairLengthId : '';
        this.glassesId = crimeDocketModel ? crimeDocketModel.glassesId == undefined ? '' : crimeDocketModel.glassesId : '';
        this.facialHairId = crimeDocketModel ? crimeDocketModel.facialHairId == undefined ? '' : crimeDocketModel.facialHairId : '';
        this.scars = crimeDocketModel ? crimeDocketModel.scars == undefined ? '' : crimeDocketModel.scars : '';
        this.graphicsOnClothes = crimeDocketModel ? crimeDocketModel.graphicsOnClothes == undefined ? '' : crimeDocketModel.graphicsOnClothes : '';
        this.shirtTypeId = crimeDocketModel ? crimeDocketModel.shirtTypeId == undefined ? '' : crimeDocketModel.shirtTypeId : '';
        this.shirtColor = crimeDocketModel ? crimeDocketModel.shirtColor == undefined ? '' : crimeDocketModel.shirtColor : '';
        this.jacketTypeId = crimeDocketModel ? crimeDocketModel.jacketTypeId == undefined ? '' : crimeDocketModel.jacketTypeId : '';
        this.jacketColor = crimeDocketModel ? crimeDocketModel.jacketColor == undefined ? '' : crimeDocketModel.jacketColor : '';
        this.jacketColor = crimeDocketModel ? crimeDocketModel.jacketColor == undefined ? '' : crimeDocketModel.jacketColor : '';
        this.pantStyleId = crimeDocketModel ? crimeDocketModel.pantStyleId == undefined ? '' : crimeDocketModel.pantStyleId : '';
        this.pantColor = crimeDocketModel ? crimeDocketModel.pantColor == undefined ? '' : crimeDocketModel.pantColor : '';
        this.pantTypeId = crimeDocketModel ? crimeDocketModel.pantTypeId == undefined ? '' : crimeDocketModel.pantTypeId : '';
        this.shoesTypeId = crimeDocketModel ? crimeDocketModel.shoesTypeId == undefined ? '' : crimeDocketModel.shoesTypeId : '';
        this.shoesColor = crimeDocketModel ? crimeDocketModel.shoesColor == undefined ? '' : crimeDocketModel.shoesColor : '';
        this.jewelleryId = crimeDocketModel ? crimeDocketModel.jewelleryId == undefined ? '' : crimeDocketModel.jewelleryId : '';
        this.weaponId = crimeDocketModel ? crimeDocketModel.weaponId == undefined ? '' : crimeDocketModel.weaponId : '';
        this.distinguishingFeatures = crimeDocketModel ? crimeDocketModel.distinguishingFeatures == undefined ? '' : crimeDocketModel.distinguishingFeatures : '';
    }
    isHeightUnknown: boolean
    height: string;
    isBuildUnknown: boolean;
    buildId: string;
    isRaceUnknown: boolean;
    raceId: string;
    isComplexionUnknown: boolean;
    complexionId: string;
    isGenderUnknown: boolean;
    genderId: string;
    isAgeUnknown: boolean;
    age: string;
    isFacialDescriptionUnknown: boolean;
    facialDescription: string;
    isHeadGearUnknown: boolean;
    headGearId: string;
    isHairColorUnknown: boolean;
    hairColorId: string;
    isHairLengthUnknown: boolean;
    hairLengthId: string;
    isGlassesUnknown: boolean;
    glassesId: string;
    isFacialHairUnknown: boolean;
    facialHairId: string;
    isScarsUnknown: boolean;
    scars: string;
    isClothingUnknown: boolean;
    graphicsOnClothes: string;
    isShirtUnknown: boolean;
    shirtTypeId: string;
    shirtColor: string;
    isJacketUnknown: boolean;
    jacketTypeId: string;
    jacketColor: string;
    isPantStyleUnknown: boolean;
    pantStyleId: string;
    pantColor: string;
    isPantTypeUnknown: boolean;
    pantTypeId: string;
    isShoesUnknown: boolean;
    shoesTypeId: string;
    shoesColor: string;
    isJewelleryUnknown: boolean;
    jewelleryId: string;
    isGlovesUnknownh: boolean;
    isGloves: boolean;
    isWeaponUnknown: boolean;
    weaponId: string;
    distinguishingFeatures: string;
}

class CrimeDocketVictimsModel {
    constructor(crimeDocketModel?: CrimeDocketVictimsModel) {
        this.isGenderUnknown = crimeDocketModel ? crimeDocketModel.isGenderUnknown == undefined ? false : crimeDocketModel.isGenderUnknown : false;
        this.isAgeUnknown = crimeDocketModel ? crimeDocketModel.isAgeUnknown == undefined ? false : crimeDocketModel.isAgeUnknown : false;
        this.genderId = crimeDocketModel ? crimeDocketModel.genderId == undefined ? '' : crimeDocketModel.genderId : '';
        this.age = crimeDocketModel ? crimeDocketModel.age == undefined ? '' : crimeDocketModel.age : '';
        this.distinguishingFeatures = crimeDocketModel ? crimeDocketModel.distinguishingFeatures == undefined ? '' : crimeDocketModel.distinguishingFeatures : '';
    }
    isGenderUnknown: boolean
    genderId: string
    isAgeUnknown: boolean
    age: string
    distinguishingFeatures: string
}

class CrimeDocketStolenItemsModel {
    constructor(crimeDocketModel?: CrimeDocketStolenItemsModel) {
        this.crimeDocketStolenItemName = crimeDocketModel ? crimeDocketModel.crimeDocketStolenItemName == undefined ? '' : crimeDocketModel.crimeDocketStolenItemName : '';
        this.crimeDocketStolenItemDescription = crimeDocketModel ? crimeDocketModel.crimeDocketStolenItemDescription == undefined ? '' : crimeDocketModel.crimeDocketStolenItemDescription : '';
    }
    crimeDocketStolenItemName: string;
    crimeDocketStolenItemDescription: string
}


class CrimeDocketVehiclesModel {
    constructor(crimeDocketModel?: CrimeDocketVehiclesModel) {
        this.occupantCount = crimeDocketModel ? crimeDocketModel.occupantCount == undefined ? '' : crimeDocketModel.occupantCount : '';
        this.vehicleMakeName = crimeDocketModel ? crimeDocketModel.vehicleMakeName == undefined ? '' : crimeDocketModel.vehicleMakeName : '';
        this.vehicleModelName = crimeDocketModel ? crimeDocketModel.vehicleModelName == undefined ? '' : crimeDocketModel.vehicleModelName : '';
        this.vehicleColorName = crimeDocketModel ? crimeDocketModel.vehicleColorName == undefined ? '' : crimeDocketModel.vehicleColorName : '';
        this.vehicleShapeName = crimeDocketModel ? crimeDocketModel.vehicleShapeName == undefined ? '' : crimeDocketModel.vehicleShapeName : '';
        this.vehicleRegistrationNumber = crimeDocketModel ? crimeDocketModel.vehicleRegistrationNumber == undefined ? '' : crimeDocketModel.vehicleRegistrationNumber : '';
        this.vehicleDamage = crimeDocketModel ? crimeDocketModel.vehicleDamage == undefined ? '' : crimeDocketModel.vehicleDamage : '';
        this.distinguishableMarkings = crimeDocketModel ? crimeDocketModel.distinguishableMarkings == undefined ? '' : crimeDocketModel.distinguishableMarkings : '';

    }
    occupantCount: string
    vehicleMakeName: string
    vehicleModelName: string
    vehicleColorName: string
    vehicleShapeName: string
    vehicleRegistrationNumber: string
    vehicleDamage: string
    distinguishableMarkings: string
}

class CrimeDocketDogsModel {
    constructor(crimeDocketModel?: CrimeDocketDogsModel) {
        this.dogSizeId = crimeDocketModel ? crimeDocketModel.dogSizeId == undefined ? '' : crimeDocketModel.dogSizeId : '';
        this.dogTypeName = crimeDocketModel ? crimeDocketModel.dogTypeName == undefined ? '' : crimeDocketModel.dogTypeName : '';
        this.isDogVicious = crimeDocketModel ? crimeDocketModel.isDogVicious == undefined ? false : crimeDocketModel.isDogVicious : false;

    }
    dogSizeId: string
    isDogVicious: boolean
    dogTypeName: string
}

class CrimeDocketTakenVehiclesModel {
    constructor(crimeDocketModel?: CrimeDocketTakenVehiclesModel) {
        this.occupantCount = crimeDocketModel ? crimeDocketModel.occupantCount == undefined ? '' : crimeDocketModel.occupantCount : '';
        this.vehicleMakeName = crimeDocketModel ? crimeDocketModel.vehicleMakeName == undefined ? '' : crimeDocketModel.vehicleMakeName : '';
        this.vehicleModelName = crimeDocketModel ? crimeDocketModel.vehicleModelName == undefined ? '' : crimeDocketModel.vehicleModelName : '';
        this.vehicleColorName = crimeDocketModel ? crimeDocketModel.vehicleColorName == undefined ? '' : crimeDocketModel.vehicleColorName : '';
        this.vehicleShapeName = crimeDocketModel ? crimeDocketModel.vehicleShapeName == undefined ? '' : crimeDocketModel.vehicleShapeName : '';
        this.vehicleRegistrationNumber = crimeDocketModel ? crimeDocketModel.vehicleRegistrationNumber == undefined ? '' : crimeDocketModel.vehicleRegistrationNumber : '';
        this.vehicleDamage = crimeDocketModel ? crimeDocketModel.vehicleDamage == undefined ? '' : crimeDocketModel.vehicleDamage : '';
        this.distinguishableMarkings = crimeDocketModel ? crimeDocketModel.distinguishableMarkings == undefined ? '' : crimeDocketModel.distinguishableMarkings : '';
    }
    occupantCount: string
    vehicleMakeName: string
    vehicleModelName: string
    vehicleColorName: string
    vehicleShapeName: string
    vehicleRegistrationNumber: string
    vehicleDamage: string
    distinguishableMarkings: string
}
export { CrimeDocketModel, CrimeDocketSuspectsModel, CrimeDocketVictimsModel, CrimeDocketStolenItemsModel, CrimeDocketVehiclesModel, CrimeDocketDogsModel, CrimeDocketTakenVehiclesModel }