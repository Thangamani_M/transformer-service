import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RxjsService, CrudService, ModulesBasedApiSuffix, IApplicationResponse, CrudType, prepareGetRequestHttpParams } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-password-view-dilog',
  templateUrl: './password-view-dilog.component.html',
})
export class PasswordViewDilogComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  PassWordViewDetails:any = [];

  constructor(private rxjsService: RxjsService,private crudService: CrudService, private momentService: MomentService,
    public ref: DynamicDialogRef,public config: DynamicDialogConfig, private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "User Ranking",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Hourly Stats', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 6, hourlyChildTab: 1 } }, { displayName: 'User Ranking' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'User Ranking',
            dataKey: 'userId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns:[{ field: 'fullAddress', header: 'Site Details' },
             { field: 'accessedDate', header: 'Date Time', isDate:true }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.STACK_MONITORING_SUPPERVISOR_VIEW_OPERATOR_KEYHOLDER_PASSWORD,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            enableReloadBtn: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getPasswordDetails();
    this.onShowValue(this.config?.data)
  }

  getPasswordDetails(pageIndex?: string, pageSize?: string, otherParams?:any) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_MONITORING_SUPPERVISOR_VIEW_OPERATOR_KEYHOLDER_PASSWORD, this.config?.data?.userId,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))

        .subscribe((data: IApplicationResponse) => {
          this.loading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          if (data.isSuccess) {
            this.dataList = data.resources.operatorAccessKeyholderPasswordSupervisorViewList;
          } else {
            this.dataList = null
            this.totalRecords = 0;

          }
        });
    }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
  
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
  
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getPasswordDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }
  
  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(false);
  }

  onShowValue(response?: any) {
    this.PassWordViewDetails = [
      { name: 'OB Number', value: this.config?.data?.operatorName  },

    ]
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  
  }