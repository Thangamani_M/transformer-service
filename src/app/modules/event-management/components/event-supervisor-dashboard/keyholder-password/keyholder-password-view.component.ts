import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { PasswordViewDilogComponent } from './password-view-dilog/password-view-dilog.component';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-keyholder-password-view',
  templateUrl: './keyholder-password-view.component.html',
})
export class KeyholderPasswordViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() breadcrumFalse: boolean = true;
  @Input() refresh:any
  changes:any;


  constructor(public dialogService: DialogService,
    private rxjsService: RxjsService, private crudService: CrudService,
    private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Password Views",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Staff Monitoring', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 1} }, { displayName: 'Password Views' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Pass Word View',
            dataKey: 'divisionId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'operatorName', header: 'Operator' },
            { field: 'accessedCount', header: 'Flagged Password Views' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SUPERVISOR_VIEW_OPERATOR_KEYHOLDER_PASSWORD,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: false,
            enableReloadBtn: false,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    if(!this.changes)
    this.getResponceLoggedAfterCanceledDetails();
  }

  ngOnChanges(changes: SimpleChange): void {
    this.changes = changes
    if (changes) {
      if (changes['refresh']) {
        this.getResponceLoggedAfterCanceledDetails()
      }
    }
  }

  getResponceLoggedAfterCanceledDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      else {
        this.dataList = null;
        this.totalRecords = 0
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getResponceLoggedAfterCanceledDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
        case CrudType.VIEW:
          this.rxjsService.setDialogOpenProperty(true);
          this.openAddEditPage(CrudType.VIEW, row);
          break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: any | string): void {
    const ref = this.dialogService.open(PasswordViewDilogComponent, { 
      showHeader: false,
      baseZIndex: 1000,
      width: '750px',
      data: {...editableObject,header: 'Password View', },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.getResponceLoggedAfterCanceledDetails()
      }
    });

  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

}