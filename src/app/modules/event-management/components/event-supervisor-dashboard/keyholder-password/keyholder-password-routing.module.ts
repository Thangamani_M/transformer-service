import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KeyholderPasswordViewComponent } from './keyholder-password-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'view', component: KeyholderPasswordViewComponent, data: { title: 'Keyholder Password View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class KeyholderPasswordRoutingModule { }
