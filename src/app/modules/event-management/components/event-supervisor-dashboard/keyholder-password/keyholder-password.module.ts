import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { KeyholderPasswordRoutingModule } from './keyholder-password-routing.module';
import { KeyholderPasswordViewComponent } from './keyholder-password-view.component';
import { PasswordViewDilogComponent } from './password-view-dilog/password-view-dilog.component';

export { KeyholderPasswordViewComponent };

@NgModule({
  declarations: [KeyholderPasswordViewComponent, PasswordViewDilogComponent],
  imports: [
    CommonModule,
    KeyholderPasswordRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  exports:[KeyholderPasswordViewComponent],
  entryComponents: [PasswordViewDilogComponent]
})
export class KeyholderPasswordModule { }
