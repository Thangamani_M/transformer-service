import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { RxjsService, CrudService, MomentService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, IApplicationResponse, CrudType } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-hourly-operator',
  templateUrl: './hourly-operator.component.html',
})
export class HourlyOperatorComponent extends PrimeNgTableVariablesModel  implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    public router: Router) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Signal History",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Signal History' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'stackConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'operatorName', header: 'Name', width: '200px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.OPERATOR,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {  
      let otherParams = {}
      this.getRequiredList(null, null, otherParams);
  }

  getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
       null,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.resources.length;
          this.isShowNoRecord = false;
        } else {
          this.dataList = null;
          this.isShowNoRecord = true;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar)
      case CrudType.VIEW:
        let merged = Object.assign({}, this.config?.data, row);
        merged.operatorId = row['userId'];
        this.router.navigate(["event-management/supervisor-dashboard/operator/view"], { queryParams: merged });
        this.ref.close();
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  btnCloseClick(){
    this.ref.close();
  }

}