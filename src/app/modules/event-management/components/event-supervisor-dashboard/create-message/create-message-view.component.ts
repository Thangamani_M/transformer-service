import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-create-message-view',
  templateUrl: './create-message-view.component.html',
})
export class CreateMessageViewComponent implements OnInit {

createId: any
createDetailes: any;
selectedTabIndex = 0;
primengTableConfigProperties: any

constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.createId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
  });
  this.primengTableConfigProperties = {
    tableCaption: 'View Messaage',
    selectedTabIndex: 0,
    breadCrumbItems: [
      { displayName: 'Supervisor Dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } },
      { displayName: 'Create Messaage', relativeRouterUrl: '/event-management/supervisor-dashboard/bolo/list' }
      , { displayName: '', }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  this.rxjsService.setGlobalLoaderProperty(false);
  if (this.createId) {
    this.getcreateDetailes().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.createDetailes = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = "View --" + this.createDetailes.mainArea;
      this.createDetailes = [
        { name: 'Main Area', value: response.resources.mainArea },
        { name: 'Sub Area', value: response.resources.subArea },
        { name: 'Message', value: response.resources.messages },
        { name: 'Modified On', value: response.resources?.modifiedDate, isDateTime: true },
        { name: 'Modified By', value: response.resources?.modifiedUserName },
        { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  this.router.navigate(['event-management/supervisor-dashboard/create-message/add-edit'], { queryParams: { id: this.createId } });
}

getcreateDetailes(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.CREATE_MESSAGE,
    this.createId
  );
}

}
