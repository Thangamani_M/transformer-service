import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateMessageAddEditComponent } from './create-message-add-edit.component';
import { CreateMessageListComponent } from './create-message-list.component';
import { CreateMessageViewComponent } from './create-message-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: CreateMessageListComponent, data: { title: 'Create-Messeage List' } },
  { path: 'add-edit', component: CreateMessageAddEditComponent, data: { title: 'Create-message Add/Edit' } },
  { path: 'view', component: CreateMessageViewComponent, data: { title: 'Create-message View' } },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CreateMessageRoutingModule { }
