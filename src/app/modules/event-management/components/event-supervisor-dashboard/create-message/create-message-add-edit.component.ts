import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, HttpCancelService, RxjsService, CrudService, setRequiredValidator, ModulesBasedApiSuffix, IApplicationResponse} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { UserLogin } from '@modules/others/models';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-message-add-edit',
  templateUrl: './create-message-add-edit.component.html',
})
export class CreateMessageAddEditComponent implements OnInit {

  creatId: any;
  createDetails: any;
  mainAreaDropDown: any = [];
  subAreaDropdown: any = [];
  loggedUser: any;
  createForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.creatId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.creatId ? 'Update Create Message':'Add Create Message';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' },
      { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } },{ displayName: 'Create Message', relativeRouterUrl: '/event-management/supervisor-dashboard/create-message' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.creatId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/supervisor-dashboard/create-message/view' , queryParams: {id: this.creatId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createcreateForm();
    this.getMainAreaDropdown();
    this.getSubAreaDropdown();
    if (this.creatId) {
      this.getcreatDetailsById(this.creatId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createcreateForm(): void {
    // create form controls dynamically from model class
    this.createForm = this.formBuilder.group({
      creatId:[''],
      mainAreaId:['',Validators.required],
      subAreaId:['',Validators.required],
      messages:['',Validators.required],
      supervisorId:['',Validators.required],
      createdUserId:['',Validators.required],
      supervisorAlertId: ['',Validators.required],
      modifiedUserId:['',Validators.required],
      isActive:[true],
    });
    this.createForm = setRequiredValidator(this.createForm, ["mainAreaId", "subAreaId", "messages"]);
    this.createForm.get('supervisorAlertId').setValue(this.loggedUser.userId)
    this.createForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.createForm.get('supervisorId').setValue(this.loggedUser.userId)
    this.createForm.get('createdUserId').setValue(this.loggedUser.userId)
    if (!this.creatId) {
      this.createForm.removeControl('creatId');
      this.createForm.removeControl('modifiedUserId');
    } else {
      this.createForm.removeControl('createdUserId');
    }
  }

  getMainAreaDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_MAIN_AREA_ALL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropDown = response.resources;
         }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSubAreaDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SUB_AREA, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subAreaDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getcreatDetailsById(creatId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CREATE_MESSAGE, creatId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.createDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.createDetails?.mainArea ? this.title +' - '+ this.createDetails?.mainArea : this.title + ' --', relativeRouterUrl: '' });
          this.createForm.patchValue(this.createDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.createForm.invalid) {
      return;
    }
    let formValue = this.createForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.creatId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CREATE_MESSAGE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CREATE_MESSAGE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/supervisor-dashboard/create-message');
      }
    })
  }

  onCRUDRequested(type){}

}

