import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatTabsModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { CreateMessageAddEditComponent } from './create-message-add-edit.component';
import { CreateMessageListComponent } from './create-message-list.component';
import { CreateMessageRoutingModule } from './create-message-routing.module';
import { CreateMessageViewComponent } from './create-message-view.component';



@NgModule({
  declarations: [CreateMessageViewComponent, CreateMessageAddEditComponent, CreateMessageListComponent],
  imports: [
    CommonModule,
    CreateMessageRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatTabsModule
  ]
})
export class CreateMessageModule { }
