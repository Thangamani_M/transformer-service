import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { LoggedInUserModel, CrudService, SnackbarService, RxjsService, ModulesBasedApiSuffix, CrudType, prepareGetRequestHttpParams, ResponseMessageTypes, IApplicationResponse, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { map, } from 'rxjs/operators';
import { DatePipe } from '@angular/common'
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-create-message-list',
  templateUrl: './create-message-list.component.html',
})
export class CreateMessageListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {}

  constructor(private router: Router, private rxjsService: RxjsService,
    private crudService: CrudService, private snackbarService: SnackbarService,
    private datePipe: DatePipe, private momentService: MomentService, private store: Store<AppState>,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Create Message",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Create Message' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Create Message',
            dataKey: 'supervisorAlertId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableCallPickBtn: true,
            enableStatusActiveAction: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'mainArea', header: 'Main Area', width: '100px' },
            { field: 'subArea', header: 'Sub Area', width: '70px' },
            { field: 'messages', header: 'Messages', width: '100px' },
            { field: 'alertSendTime', header: 'AlertSendTime', width: '100px' },
            { field: 'createdDate', header: 'CreatedDate', width: '100px' },
            { field: 'modifiedDate', header: 'ModifiedDate', width: '100px' },
            { field: 'isActive', header: 'Status', width: '80px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.CREATE_MESSAGE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
      .pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res?.resources?.forEach(val => {
            val.createdDate = val.createdDate ? this.datePipe.transform(val.createdDate, 'dd-MM-yyyy h:mm a') : '';
            val.modifiedDate = val.modifiedDate ? this.datePipe.transform(val.modifiedDate, 'dd-MM-yyyy h:mm a') : '';
            val.alertSendTime = val.alertSendTime ? this.datePipe.transform(val.alertSendTime, 'dd-MM-yyyy h:mm a') : '';
            return val;
          })
        }
        return res;
      }))
      .subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.CLICK:
        this.openAddEditPage(CrudType.CLICK, row);
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl("event-management/supervisor-dashboard/create-message/add-edit");
        break;
      case CrudType.CLICK:
        switch (this.selectedTabIndex) {
          case 0:
            this.onOneOrManyRowsEdit()
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/supervisor-dashboard/create-message/view"], { queryParams: { id: editableObject['supervisorAlertId'] } });
            break;
        }
        break;
    }
  }

  onOneOrManyRowsEdit() {
    if (this.selectedRows.length == 0) {
      this.snackbarService.openSnackbar("Please select atleast one item to Update", ResponseMessageTypes.WARNING);
    }
    if (this.selectedRows.length > 0) {
      var updateableIds = []
      this.selectedRows.forEach((element: any) => {
        updateableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
      });
      let postObj = {
        "supervisorAlertIds": updateableIds,
        "modifiedUserId": this.loggedInUserData.userId
      }
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUPERVISOR_ALERT_RECALL, postObj, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
        }
        else {
          this.snackbarService.openSnackbar("Supervisor Alerts Failed", ResponseMessageTypes.WARNING);
        }
        this.rxjsService.setDialogOpenProperty(false);
      });
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}