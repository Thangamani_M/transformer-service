import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LongArrivalResponseThOneComponent } from './long-arrival-response-th-one.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: LongArrivalResponseThOneComponent, data: { title: 'Long Arrival Response Th One' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class LongArrivalResponseThOneRoutingModule { }
