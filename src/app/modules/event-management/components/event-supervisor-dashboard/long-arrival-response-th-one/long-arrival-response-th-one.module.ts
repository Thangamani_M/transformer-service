import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LongArrivalResponseThOneRoutingModule } from './long-arrival-response-th-one-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { LongArrivalResponseThOneComponent } from './long-arrival-response-th-one.component';


@NgModule({
  declarations: [LongArrivalResponseThOneComponent],
  imports: [
    CommonModule,
    LongArrivalResponseThOneRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LongArrivalResponseThOneModule { }
