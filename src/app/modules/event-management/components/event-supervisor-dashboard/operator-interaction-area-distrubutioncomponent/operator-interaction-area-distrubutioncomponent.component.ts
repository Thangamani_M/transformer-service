import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
import { AreaDistributionModel, ExcludeAlarmListModel, IncludeAlarmListModel } from '../models/ara-distribution-details.model';
@Component({
  selector: 'app-operator-interaction-area-distrubutioncomponent',
  templateUrl: './operator-interaction-area-distrubutioncomponent.component.html',
})
export class OperatorInteractionAreaDistrubutioncomponentComponent implements OnInit {
  distributionForm: FormGroup;
  loggedUser: UserLogin;
  stackAreaConfigList;
  stackConfigList;
  alarmTypeList: any = [];
  staffTypeList;
  distributionDetails;
  detailsRow;

  constructor(public _fb: FormBuilder,
    private store: Store<AppState>,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.detailsRow = this.config?.data?.row
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_STACK_AREA_CONFIG),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_STACK_CONFIG),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_ALARM_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_STAFF_TYPE),];
    this.loadActionTypes(dropdownsAndData);
    this.createDistributionForm()
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.stackAreaConfigList = resp.resources;
              break;
            case 1:
              this.stackConfigList = resp.resources;
              break;
            case 2:
              resp.resources.forEach(element => {
                let data = { label: element.displayName, value: { alarmTypeId: element.id } }
                this.alarmTypeList.push(data)
              });
              break;
            case 3:
              this.staffTypeList = resp.resources;
              break;
          }
        }
        if (response.length == ix + 1) {
          this.DistrubutionDialogViewDetails();
        }
      });
    });
  }

  createDistributionForm() {
    let cmcSmsGroupModel = new AreaDistributionModel();
    this.distributionForm = this._fb.group({
    });
    Object.keys(cmcSmsGroupModel).forEach((key) => {
      this.distributionForm.addControl(key, new FormControl(cmcSmsGroupModel[key]));
    });
    this.distributionForm = setRequiredValidator(this.distributionForm, ["staffTypeId", "stackConfigId", "stackAreaConfigId"]);
    this.distributionForm.get('userId').setValue(this.loggedUser.userId)
    this.distributionForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.distributionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  //Create FormArray controls
  createstackDistributionIncludeAlarmTypeListModel(cmcSmsGroupEmployeeListModel?: ExcludeAlarmListModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new ExcludeAlarmListModel(cmcSmsGroupEmployeeListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }, [Validators.required]]
    });
    return this._fb.group(formControls);
  }
  //Create FormArray controls
  createstackDistributionExcludeAlarmTypeListModel(cmcSmsGroupEmployeeListModel?: IncludeAlarmListModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new IncludeAlarmListModel(cmcSmsGroupEmployeeListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }, [Validators.required]]
    });
    return this._fb.group(formControls);
  }

  DistrubutionDialogViewDetails() {
    if (this.detailsRow.operatorUserId || this.detailsRow.dispatcherUserId) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_DISTRIBUTION_OPERATOR, this.detailsRow.operatorUserId ? this.detailsRow.operatorUserId : this.detailsRow.dispatcherUserId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
            let cmcSmsGroup = new AreaDistributionModel(response.resources);
            this.distributionDetails = response.resources;
            this.distributionForm.patchValue(cmcSmsGroup);
            this.distributionForm.get('userId').setValue(this.detailsRow.operatorUserId ? this.detailsRow.operatorUserId : this.detailsRow.dispatcherUserId)
            this.distributionForm.get('createdUserId').setValue(this.loggedUser.userId)
            this.distributionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
    }
    else {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_DISTRIBUTION_OPERATOR, this.detailsRow.userId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
            let cmcSmsGroup = new AreaDistributionModel(response.resources);
            this.distributionDetails = response.resources;
            this.distributionForm.patchValue(cmcSmsGroup);
            this.distributionForm.get('userId').setValue(this.detailsRow.userId)
            this.distributionForm.get('createdUserId').setValue(this.loggedUser.userId)
            this.distributionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
    }
  }

  onSubmitDistribution() {
    if (this.distributionForm.invalid) {
      return;
    }
    let formValue = this.distributionForm.value;
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_DISTRIBUTION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.isSuccess) {
        this.ref.close(true);
      }
    });
  }

  btnCloseClick() {
    this.ref.close(false)
  }
}