import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OperatorComponent } from './operator.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'view', component: OperatorComponent, data: { title: 'Operator  View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class OperatorRoutingModule { }
