import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-operator',
  templateUrl: './operator.component.html',
  styleUrls: ['./operator.component.scss']
})
export class OperatorComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  filterData: any;
  operatorDetails: any;
  observableResponseCurrent: any;

  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private momentService: MomentService , private router:Router, private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Operator",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Hourly Stats', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 6, hourlyChildTab: 7 } }, { displayName: 'Operator' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Operator',
            dataKey: 'operatorId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'statShiftDate', header: 'Date', isDate:true },
            { field: 'statShift', header: 'Shifts' },
            { field: 'actions', header: 'Action' },
            { field: 'dispatches', header: 'Dispatches' },
            { field: 'finishes', header: 'Finishes' },
            { field: 'medianACKTime', header: 'MedianAckTime' },
            { field: 'medianPhoneTime', header: 'MedianPhnTime' },
            { field: 'medianDispatcheTime', header: 'MedianDispTime' },
            { field: 'medianFinishTime', header: 'MedianFinishTime' },
            { field: 'aveAcknowledgedTime', header: 'AveAvcTime' },
            { field: 'avePhoneTime', header: 'AvePhnTime' },
            { field: 'aveDispatcheTime', header: 'AveDispTime' },
            { field: 'aveFinishTime', header: 'AveFinihTime' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.OPERATOR_HISTORY,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            enableReloadBtn: true,
            enableExportCSV: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;
    });
  }

  ngOnInit(): void {
    this.GetOperatorDetailsById(this.filterData?.operatorId)
    this.getOperatorListDetails();
    this.operatorCurrentHourly()
  }

  GetOperatorDetailsById(operatorId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPERATOR, operatorId, false, null).subscribe((response: IApplicationResponse) => {
      this.operatorDetails = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

   getOperatorListDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
      this.loading = true
      otherParams = { ...otherParams, ...this.filterData };
      this.crudService.get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.OPERATOR_HISTORY,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe(data => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null
          this.totalRecords = 0;

        }
      });
    }

  operatorCurrentHourly(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    let otherParamsAll = { ...otherParams, ...this.filterData };
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.OPERATOR_CURRENT_LAST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsAll)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponseCurrent = data.resources;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getOperatorListDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  navigateHourly(){
    this.router.navigate(["/event-management/supervisor-dashboard"], { queryParams: { tab: 6,hourlyChildTab:13}, state: { example: this.filterData} });
  }

}
