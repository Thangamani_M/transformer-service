import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-classify-influx-list',
  templateUrl: './classify-influx-list.component.html',
  styleUrls: ['./classify-influx-list.component.scss']
})
export class ClassifyInfluxListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  singnalInfluxDialog: boolean = false;
  signalForm: FormGroup;
  signalInfluxId: any;
  httpCancelService: any;
  dropDownSignalList: any;
  @Input() refresh:any;

  constructor(private rxjsService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private momentService: MomentService,
    private _fb: FormBuilder, private store: Store<AppState>,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Crime Docket",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Crime Docket' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Crime Docket',
            dataKey: 'supervisorMessageId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'mainArea', header: 'Main Area' },
            { field: 'areaName', header: 'Stack' },
            { field: 'nonEmergencySignal', header: 'Non-EME Signals' },
            { field: 'emergencySignal', header: 'EME Signal' },
            { field: 'signalInfluxDate', header: 'Influx Start' , isDate:true},
            { field: 'signalInfluxClassificationTypeName', header: 'Classification' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_INFLUX,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createSignalForm();
    this.getSignalDropDown();
  }

  ngOnChanges(changes: SimpleChange): void {
    if (changes) {
      if (changes['refresh']) {
        this.getClassifyInfluxDetails()
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createSignalForm() {
    this.signalForm = this._fb.group({
      signalInfluxId: [''],
      signalInfluxClassficationTypeId: ['', Validators.required],
      modifiedUserId: ['']
    });
  };

  getSignalDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLASSIFICATION_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dropDownSignalList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getClassifyInfluxDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
        }
        return res;
      })).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getClassifyInfluxDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.singnalInfluxDialog = true;
        this.signalInfluxId = row['signalInfluxId']
        break;
    }
  }


  UpdateNotification() {
    let forValue = this.signalForm.value;
    forValue.signalInfluxId = this.signalInfluxId
    forValue.modifiedUserId = this.loggedInUserData.userId
    forValue.signalInfluxClassficationTypeId = forValue.signalInfluxClassficationTypeId.id
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_INFLUX, forValue, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.singnalInfluxDialog = false;
        this.getClassifyInfluxDetails();
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {});
    }
  }

}
