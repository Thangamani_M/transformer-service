import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ClassifyInfluxListComponent } from './classify-influx/classify-influx-list.component';
import { EventSupervisorDashboardRoutingModule } from './event-supervisor-dashboard-routing.module';
import { EventSupervisorDashboardComponent } from './event-supervisor-dashboard.component';
import { KeyholderPasswordViewComponent } from './keyholder-password/keyholder-password-view.component';
import { KeyholderPasswordModule } from './keyholder-password/keyholder-password.module';
import { OperatorInstructionComponent } from './operator-instruction/operator-instruction.component';
import { OperatorLogedInViewComponent } from './operator-loged-in/operator-loged-in-view.component';
import { OperatorLogedInModule } from './operator-loged-in/operator-loged-in.module';
import { OperatorMessageListComponent } from './operator-message-list/operator-message-list.component';
import { SiteRelatedMessageListComponent } from './site-related-message-list/site-related-message-list.component';
import { SnapshotViewListComponent } from './snapshot-view/snapshot-view-list.component';
import { StaffMonitoringAreaDistrubutionComponent } from './staff-monitoring-area-distrubution/staff-monitoring-area-distrubution.component';
import { HourlyOperatorComponent } from './hourly-operator/hourly-operator.component';

export { KeyholderPasswordViewComponent };
export { OperatorLogedInViewComponent };
@NgModule({
  declarations: [EventSupervisorDashboardComponent,
     SiteRelatedMessageListComponent, 
     OperatorMessageListComponent, 
     ClassifyInfluxListComponent, 
     SnapshotViewListComponent,
     StaffMonitoringAreaDistrubutionComponent,
     OperatorInstructionComponent,
     HourlyOperatorComponent,
    ],
  imports: [
    CommonModule,
    EventSupervisorDashboardRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    KeyholderPasswordModule,
    OperatorLogedInModule
  ],
  entryComponents:[OperatorInstructionComponent, HourlyOperatorComponent]
})
export class EventSupervisorDashboardModule { }
