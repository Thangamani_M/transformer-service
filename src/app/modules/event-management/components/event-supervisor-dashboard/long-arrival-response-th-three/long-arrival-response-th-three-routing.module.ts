import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LongArrivalResponseThThreeComponent } from '../long-arrival-response-th-three/long-arrival-response-th-three.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: LongArrivalResponseThThreeComponent, data: { title: 'Long Arrival Response Th three' } }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class LongArrivalResponseThThreeRoutingModule { }
