import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LongArrivalResponseThThreeRoutingModule } from './long-arrival-response-th-three-routing.module';
import { LongArrivalResponseThThreeComponent } from './long-arrival-response-th-three.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';


@NgModule({
  declarations: [LongArrivalResponseThThreeComponent],
  imports: [
    CommonModule,
    LongArrivalResponseThThreeRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LongArrivalResponseThThreeModule { }
