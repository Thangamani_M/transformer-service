import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ReusablePrimeNGTableFeatureService, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs-compat/Observable';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-site-related-message-list',
  templateUrl: './site-related-message-list.component.html'
})
export class SiteRelatedMessageListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  supervisorDialog: boolean = false;
  loggedUser: UserLogin;
  supervisorMessageId: any;
  @Input() refresh:any;


  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private store: Store<AppState>,) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Long Standoff",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Long Standoff' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Long Standoff',
            dataKey: 'supervisorMessageId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 2,
            columns: [{ field: 'customerName', header: 'Customer Name' },
            { field: 'address', header: 'Address' },
            { field: 'message', header: 'Message' },
            { field: 'messageDateTime', header: 'Received At', isDate:true },
            { field: 'operatorName', header: 'Operator' },
            { field: 'acknowledgedStatus', header: 'Status' },
            { field: 'readDateTime', header: 'Acknowledged At' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.SUPERVISOR_MESSAGE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChange): void {
    if (changes) {
      if (changes['refresh']) {
        this.getSiterelatedMessage()
      }
    }
  }

  getSiterelatedMessage(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (otherParams) {
      otherParams['isSiteMessage'] = true
    } else {
      otherParams = {}
      otherParams['isSiteMessage'] = true
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
        }
        return res;
      })).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSiterelatedMessage(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        if (CrudType.EDIT) {
          this.openAddeditPage(CrudType.EDIT, row)
        }
        break;
      case CrudType.VIEW:
        if (CrudType.VIEW) {
          this.openAddeditPageView(CrudType.VIEW, row)
        }
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }
  openAddeditPage(type: CrudType | string, editableObject?: object | string) {
    this.supervisorDialog = true;
    this.supervisorMessageId = editableObject['supervisorMessageId']
  }

  openAddeditPageView(type: CrudType | string, editableObject?: object | string) {

    this.router.navigate(['customer/manage-customers/view/' + editableObject['customerId']], { queryParams: { addressId: editableObject['customerAddressId'] } });
  }

  sendMessage() {
    this.rxjsService.setPopupLoaderProperty(true)
    if (this.supervisorMessageId == '') {
      return
    }
    let formValue = {
      supervisorMessageReadId: "",
      supervisorMessageId: this.supervisorMessageId,
      supervisorId: this.loggedUser.userId,
      createdUserId: this.loggedUser.userId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUPERVISOR_MESSAGE_READ_SITE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.supervisorDialog = false;
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

}