import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VolumeRoutingModule } from './volume-routing.module';
import { VolumeComponent } from './volume.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';


@NgModule({
  declarations: [VolumeComponent],
  imports: [
    CommonModule,
    VolumeRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class VolumeModule { }
