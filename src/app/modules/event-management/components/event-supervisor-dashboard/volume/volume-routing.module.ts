import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VolumeComponent } from './volume.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: VolumeComponent, data: { title: 'Volume' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class VolumeRoutingModule { }
