import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT, SHORTCUT_KEYS } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { RequestType } from '@modules/technical-management/components/technical-area-manager-worklist/request-type.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { HourlyOperatorComponent } from './hourly-operator/hourly-operator.component';
import { OperatorInstructionComponent } from './operator-instruction/operator-instruction.component';
@Component({
  selector: 'app-event-supervisor-dashboard',
  templateUrl: './event-supervisor-dashboard.component.html',
  styleUrls: ['./event-supervisor-dashboard.component.scss']
})
export class EventSupervisorDashboardComponent implements OnInit {

  @ViewChildren(Table) tables: QueryList<Table>;
  observableResponse;
  selectedTabIndex: any = 0;
  selectedHourlyChildIndex: any = 0
  primengTableConfigProperties: any;
  primengTableConfigPropertiesOperator: any;
  dataList: any
  dataListOperator: any
  loading: boolean;
  loadingOperator: boolean = false
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  totalRecordsOperator: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup;
  reprocessDialog: boolean = false;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any
  row: any = {}
  pageSize: number = 10;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  openQuickAction: boolean = false
  operatorIntructionsDialog: boolean = false
  hourlySearchForm: FormGroup
  divisionDropDown: any = []
  siteTypeDropDown: any = []
  todayDate: any;
  breadcrumFalse: boolean = false;
  supervisorNotificationId: any;
  isvisibilityPatrol: boolean;
  startTodayDate = 0;
  minDate: Date;
  maxDate: Date;
  customerAddressId: any;
  customerId: any;
  occurrenceBookId: any;
  monitoringUserId: any;
  hourlyPatchData: any;
  allPermissions:any = []
  loggedUser: UserLogin;

  refresh =0
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService,
    private dialog: MatDialog) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Supervisor Dashboard",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Client Assistance' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Client Assistance',
            dataKey: 'supervisorRequestId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'customerName', header: 'Client', width: '200px' }, { field: 'customerAddress', header: 'Address', width: '200px' }, { field: 'mainArea', header: 'Main Area', width: '150px' }, { field: 'operatorName', header: 'Operator', width: '150px' }, { field: 'occurrenceBookNumber', header: 'OB Number', width: '200px' }, { field: 'comments', header: 'Comments', width: '200px' }, { field: 'clientRequestTypeName', header: 'Request Type', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CLIENT_ASSISTENCE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled: true
          },
          {
            caption: 'Staff Monitoring',
            dataKey: 'stackAreaConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stackName', header: 'Stack', width: '200px' }, { field: 'areaName', header: 'Area', width: '200px' }, { field: 'operatorCount', header: 'Operators', width: '200px' }, { field: 'dispatcherCount', header: 'Dispatchers', width: '200px' }, { field: 'combinedCount', header: 'Combined', width: '200px' }],
            apiSuffixModel: null,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled: true
          },
          {
            caption: 'Activity',
            dataKey: 'namedStackConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'namedStackConfigName', header: 'Named Stack Name', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: null,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled: true
          },
          {
            caption: 'Messages',
            dataKey: 'supervisorMessageId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'customerName', header: 'Customer Name', width: '200px' }, { field: 'address', header: 'Address', width: '300px' }, { field: 'message', header: 'Message', width: '300px' }, { field: 'isSiteMessage', header: 'Site Message', width: '300px' }, { field: 'operatorName', header: 'Operator Name', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: null,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled: true
          },
          {
            caption: 'Notifications',
            dataKey: 'supervisorNotificationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'supervisorNotificationTypeName', header: 'Notification', width: '200px' }, { field: 'customerName', header: 'Client', width: '300px' }, { field: 'customerAddress', header: 'Address', width: '300px' }, { field: 'mainArea', header: 'Main Area', width: '300px' }, { field: 'operatorName', header: 'Operator', width: '300px' }, { field: 'occuranceBookNumber', header: 'OBNumber', width: '200px' }, { field: 'acknowledged', header: 'Acknowledged', width: '200px' }, { field: 'messageDatetime', header: 'Message Date', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SUPERVISOR_NOTIFICATION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled: true
          },
          {
            caption: 'Weather Services',
            dataKey: 'vehicleRegistrationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'weatherName', header: 'Weather Name', width: '250px' }, { field: 'weatherDate', header: 'Weather Date', width: '200px' }, { field: 'timeOfDataDate', header: 'Time', width: '200px' },
            { field: 'clouds', header: 'Clouds', width: '200px' },
            { field: 'cityName', header: 'City Name', width: '200px' }, { field: 'temperature', header: 'Temparature', width: '200px' },
            { field: 'temperatureMin', header: 'Temperature Min', width: '200px' }, { field: 'temperatureMax', header: 'Temperature Max', width: '200px' }, { field: 'feelslike', header: 'Feels Like', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.WEATHER_PATTERN,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled: true
          },
          {
            caption: 'Hourly Stats',
            dataKey: 'pttRegistrationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'pttid', header: 'PTT ID', width: '200px' }, { field: 'imeiNumber', header: 'IMEI Number', width: '200px' }, { field: 'deviceName', header: 'Device', width: '200px' }, { field: 'contractDate', header: 'Contract Date', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: null,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled: true
          },
          {
            caption: 'Operation',
            dataKey: '',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'pttid', header: 'PTT ID', width: '200px' }, { field: 'imeiNumber', header: 'IMEI Number', width: '200px' }, { field: 'deviceName', header: 'Device', width: '200px' }, { field: 'contractDate', header: 'Contract Date', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: null,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled: true
          },
        ]

      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.hourlyPatchData = this.router.getCurrentNavigation().extras?.state?.example
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.selectedHourlyChildIndex = (Object.keys(params['params']).length > 0) ? +params['params']['hourlyChildTab'] : 0;
      this.primengTableConfigProperties.selectedHourlyChildIndex = this.selectedHourlyChildIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
      if (this.selectedTabIndex == 6) {
        this.getDivisionDropDownById();
        this.getSiteTypeDropdown();
      }
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.createHourlyFilterForm()
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData()
    if ( this.selectedTabIndex == 7) {
      setTimeout(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }, 0);
      return
    }
    let otherParams = {};
    setTimeout(() => {
      this.getRequiredListData(null, null, otherParams);
    }, 2000)
  }

  ngAfterViewInit(): void {
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.allPermissions = response[1][EVENT_MANAGEMENT_COMPONENT.SUPERVISOR_DASHBOARD]
      if (this.allPermissions) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.allPermissions);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        let index = this.primengTableConfigProperties.tableComponentConfigs.tabsList.findIndex(x => x.disabled == false)
        if(index !=0){
          this.selectedTabIndex = index
          this.router.navigate(['/event-management/supervisor-dashboard'], { queryParams: { tab: this.selectedTabIndex } })
        }
      }
    })
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams['isSiteMessage'] = true
    otherParams['LoggedInUserId'] = this.loggedInUserData.userId,
      this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (!eventMgntModuleApiSuffixModels) return;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  loadPaginationLazyOperator(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {

            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date') || key.toLowerCase().includes('time')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              }
              else if (key.toLowerCase().includes('timedata')) {
                otherParams[key] = this.momentService.toHoursMints12(this.row['searchColumns'][key]);
              }
              else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        if (CrudType.EDIT) {
          this.openAddEditPage(CrudType.VIEW, row);
        }
        if (RequestType.VISIBILITY_LOGGING == row['clientRequestTypeName']?.toLowerCase()) {
          this.router.navigate(['customer/manage-customers/customer-verification'], { queryParams: { customerId: row['customerId'], siteAddressId: row['customerAddressId'], customerTab: 4, monitoringTab: 8 } });
        }
        else {
          if (RequestType.SIGNAML_TAGGING == row['clientRequestTypeName']?.toLowerCase()) {
            this.router.navigate(['customer/manage-customers/customer-verification'], { queryParams: { customerId: row['customerId'], siteAddressId: row['customerAddressId'], customerTab: 4, monitoringTab: 0 } });
          }
          else if ((RequestType.ZONE_SUSPEND == row['clientRequestTypeName']?.toLowerCase())) {
            this.router.navigate(['customer/manage-customers/customer-verification'], { queryParams: { customerId: row['customerId'], siteAddressId: row['customerAddressId'], customerTab: 4 } });
          }
          else if ((RequestType.PROFILE_CHANGE == row['clientRequestTypeName']?.toLowerCase())) {
            this.router.navigate(['customer/manage-customers/customer-verification'], { queryParams: { customerId: row['customerId'], siteAddressId: row['customerAddressId'] } });
          }
          else if ((RequestType.HOLIDY_INSTRUCTION == row['clientRequestTypeName']?.toLowerCase())) {
            this.router.navigate(["event-management/stack-operator/call-workflow-list/list"], { queryParams: { id: row['occurrenceBookId'], customerId: row['customerId'], customerAddressId: row['customerAddressId'], tab: 0 } })
          }
          else if (RequestType.FRIDGE_REMINDER == row['supervisorNotificationTypeName']?.toLowerCase()) {
            this.supervisorNotificationId = row['supervisorNotificationId'];
            this.occurrenceBookId = row['occurrenceBookId'],
              this.monitoringUserId = row['modifiedUserId'],
              this.UpdateNotification();
            this.router.navigate(["event-management/stack-operator"], { queryParams: { occurrenceBookId: row['occurrenceBookId'], name: '', stackName: '' } });
            this.getStakviewDetails()
          }
          else if (RequestType.PR_CALL == row['supervisorNotificationTypeName']?.toLowerCase()) {
            this.supervisorNotificationId = row['supervisorNotificationId']
            this.UpdateNotification()
            this.router.navigate(["event-management/stack-operator"], { queryParams: { occurrenceBookId: row['occurrenceBookId'], name: '', stackName: '' } });
            this.getStakviewDetails()
          }
          else if (RequestType.VIP_CLIENT == row['supervisorNotificationTypeName']?.toLowerCase()) {
            this.supervisorNotificationId = row['supervisorNotificationId']
            this.UpdateNotification()
            this.router.navigate(["event-management/stack-operator"], { queryParams: { occurrenceBookId: row['occurrenceBookId'], name: '', stackName: '' } });
            this.getStakviewDetails()
          }
          else if (RequestType.NOACESS_PERMICES == row['supervisorNotificationTypeName']?.toLowerCase()) {
            this.supervisorNotificationId = row['supervisorNotificationId']
            this.UpdateNotification();
            this.router.navigate(["event-management/stack-operator"], { queryParams: { occurrenceBookId: row['occurrenceBookId'], name: '', stackName: '' } });
            this.getStakviewDetails()
          }
          else if (RequestType.INCIDENT == row['supervisorNotificationTypeName']?.toLowerCase()) {
            this.supervisorNotificationId = row['supervisorNotificationId']
            this.UpdateNotification()
            this.router.navigate(['customer/manage-customers/customer-verification'], { queryParams: { customerId: row['customerId'], siteAddressId: row['customerAddressId'] } });
          }
        }

        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/supervisor-dashboard/stack-config/add-edit");
            break;
          case 1:
            this.router.navigateByUrl("event-management/supervisor-dashboard/stack-areas/add-edit");
            break;
          case 2:
            this.router.navigateByUrl("event-management/supervisor-dashboard/namedStack-config/add-edit");
            break;
          case 3:
            this.router.navigateByUrl("event-management/supervisor-dashboard/named-stack-mapping/add-edit");
            break;
          case 4:
            this.router.navigateByUrl("event-management/supervisor-dashboard/alarm-group/add-edit");
            break;
          case 5:
            this.router.navigateByUrl("event-management/supervisor-dashboard/vehicle-registration/add-edit");
            break;
          case 6:
            this.router.navigateByUrl("event-management/supervisor-dashboard/ptt-registration/add-edit");
            break;
          case 7:
            this.router.navigateByUrl("event-management/supervisor-dashboard/pda-registration/add-edit");
            break;
        }

        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/supervisor-dashboard/stack-config/view"], { queryParams: { id: editableObject['stackConfigId'] } });
            break;
          case 1:
            this.router.navigate(["event-management/supervisor-dashboard/staff-monitoring/view"], { queryParams: { stackConfigId: editableObject['stackConfigId'], stackAreaConfigId: editableObject['stackAreaConfigId'] } });
            break;
          case 2:
            this.router.navigate(["event-management/supervisor-dashboard/namedStack-config/view"], { queryParams: { id: editableObject['namedStackConfigId'] } });
            break;
          case 3:
            this.router.navigate(["event-management/supervisor-dashboard/named-stack-mapping/view"], { queryParams: { id: editableObject['namedStackId'] } });
            break;
          case 4:
            this.router.navigate(["event-management/supervisor-dashboard/alarm-group/view"], { queryParams: { id: editableObject['alarmGroupId'] } });
            break;
          case 5:
            this.router.navigate(["event-management/supervisor-dashboard/vehicle-registration/view"], { queryParams: { id: editableObject['vehicleRegistrationId'] } });
            break;
          case 6:
            this.router.navigate(["event-management/supervisor-dashboard/ptt-registration/view"], { queryParams: { id: editableObject['pttRegistrationId'] } });
            break;
          case 7:
            this.router.navigate(["event-management/supervisor-dashboard/pda-registration/view"], { queryParams: { id: editableObject['pdaRegistrationId'] } });
            break;
        }
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => { //to set default row count list
      table.rows = 10
    })
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/event-management/supervisor-dashboard'], { queryParams: { tab: this.selectedTabIndex } })
    if (this.selectedTabIndex == 7) {
      setTimeout(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }, 0);
      return
    }
    if (this.selectedTabIndex != 6) {
      let otherParams = {}
      this.getRequiredListData(null, null, otherParams)
    } else {
    }
  }

  onHorlyChildTabChange(event) {
    this.selectedHourlyChildIndex = event.index
    this.router.navigate(['/event-management/supervisor-dashboard'], { queryParams: { tab: this.selectedTabIndex, hourlyChildTab: this.selectedHourlyChildIndex } })
    setTimeout(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    }, 0);

    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  superVisorRefresh() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRefresh) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.refresh++
    this.columnFilterForm.reset()
    this.getRequiredListData();
  }

  toggleQuickAction() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canQuickAction) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.openQuickAction = !this.openQuickAction
  }

  operatorIntructionsDialogModel() {
    const dialogReff = this.dialog.open(OperatorInstructionComponent, { width: '800px', disableClose: true, });
    dialogReff.afterClosed().subscribe(result => {
      this.getRequiredListData();
    });
  }

  createHourlyFilterForm() {
    this.hourlySearchForm = this._fb.group({
      divisionId: ['', Validators.required],
      fromDate: ['', Validators.required],
      toDate: ['', Validators.required],
      siteType: ['']
    })
    if (this.hourlyPatchData) {
      this.hourlySearchForm.get("divisionId").setValue(this.hourlyPatchData?.divisionId)
      this.hourlySearchForm.get("fromDate").setValue(this.momentService.convertToUTC(this.hourlyPatchData?.fromDate))
      this.hourlySearchForm.get("toDate").setValue(this.momentService.convertToUTC(this.hourlyPatchData?.toDate))
      this.hourlySearchForm.get("siteType").setValue(this.hourlyPatchData?.siteType)
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSiteTypeDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SITE_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.siteTypeDropDown = response.resources;
          this.hourlySearchForm.get('siteType').setValue(this.siteTypeDropDown)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onHourlyFilterSubmit() {
    if (this.hourlySearchForm.invalid) {
      return
    }
    var formValue = this.hourlySearchForm.value
    let findDivision = this.divisionDropDown.find(x => x.id == this.hourlySearchForm.value.divisionId)
    if (findDivision) {
      formValue.divisionName = findDivision.displayName
    }
    let siteTypeIds = []
    if (formValue.siteType.length > 0) {
      formValue.siteType?.forEach(element => {
        siteTypeIds.push(element.id)
      });
    }
    let filterData = {
      divisionId: formValue.divisionId,
      divisionName: formValue.divisionName,
      fromDate: formValue.fromDate,
      toDate: formValue.toDate,
      siteTypeIds: siteTypeIds.toString()
    }
    if (Object.keys(filterData).length > 0) {
      Object.keys(filterData).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          filterData[key] = this.momentService.toConvertDate(new Date(filterData[key]))
        } else {
          if (filterData[key]) {
            filterData[key] = filterData[key];
          } else {
            delete filterData[key]
          }
        }
      });
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    switch (this.selectedHourlyChildIndex) {
      case 0:
        this.router.navigate(["event-management/supervisor-dashboard/data-integrity-failure/view"], { queryParams: filterData });
        break;
      case 1:
        this.router.navigate(["event-management/supervisor-dashboard/user-ranking/list"], { queryParams: filterData });
        break;
      case 2:
        this.router.navigate(["event-management/supervisor-dashboard/auto-dispatch-usage/list"], { queryParams: filterData });
        break;
      case 3:
        this.router.navigate(["event-management/supervisor-dashboard/long-phone/list"], { queryParams: filterData });
        break;
      case 4:
        this.router.navigate(["event-management/supervisor-dashboard/long-arrival-response-th-one/list"], { queryParams: filterData });
        break;
      case 5:
        this.router.navigate(["event-management/supervisor-dashboard/long-arrival-response-th-two/list"], { queryParams: filterData });
        break;
      case 6:
        this.router.navigate(["event-management/supervisor-dashboard/long-arrival-response-th-three/list"], { queryParams: filterData });
        break;
      case 7:
        this.router.navigate(["event-management/supervisor-dashboard/volume/list"], { queryParams: filterData });
        break;
      case 8:
        this.router.navigate(["event-management/supervisor-dashboard/service-level/list"], { queryParams: filterData });
        break;
      case 9:
        this.router.navigate(["event-management/supervisor-dashboard/service-median/list"], { queryParams: filterData });
        break;
      case 10:
        this.router.navigate(["event-management/supervisor-dashboard/service-iqr/list"], { queryParams: filterData });
        break;
      case 11:
        this.router.navigate(["event-management/supervisor-dashboard/data-integrity-failure/list"], { queryParams: filterData });
        break;
      case 12:
        this.router.navigate(["event-management/supervisor-dashboard/shift-stats/list"], { queryParams: filterData });
        break;
      case 13:
        this.getHourlyOperatorDialog(filterData)
         this.rxjsService.setDialogOpenProperty(true)
        break;
      default:
        this.router.navigate(["event-management/supervisor-dashboard/data-integrity-failure/view"], { queryParams: filterData });
        break
    }
  }

  getHourlyOperatorDialog(filterData){
    const dialogReff = this.dialogService.open(HourlyOperatorComponent, { showHeader: false,data: { ...filterData, },  width: '750px' });
    dialogReff.onClose.subscribe(result => {
      if (result) {
      }
    });
  }

  updateMessage: string;
  UpdateNotification() {
    const params = { supervisorNotificationId: this.supervisorNotificationId, acknowledgedSupervisorId: this.loggedInUserData.userId }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUPERVISOR_NOTIFICATION, params, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.updateMessage = response.message
      }
    })
  }

  getStakviewDetails() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PHONE_STACK_VIEW,
      prepareGetRequestHttpParams(null, null, {
        OccurrenceBookId: this.occurrenceBookId,
        monitoringUserId: this.monitoringUserId,
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  minDateTODate: any;
  onFromDateChange() {
    let _date = this.hourlySearchForm.get('fromDate').value;
    this.minDateTODate = new Date(_date);
  }

  reprocessAutomatedDialogModel() {
    this.reprocessDialog = true
    this.toggleQuickAction()
  }

  sendReprocessSignal() {
    this.rxjsService.setDialogOpenProperty(true);
      setTimeout(() => {
        this.rxjsService.setDialogOpenProperty(true);
        this.rxjsService.setGlobalLoaderProperty(true);
      }, 1500);
    let formValue = {
      reprocessUserId: this.loggedUser.userId,
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.REPROCESS_SIGNAL_AUTOMATED, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.reprocessDialog = false;
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onPasswordView() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canPasswordViews) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/event-management/supervisor-dashboard/keyholder-password/view'])
  }

  onLoggedIn() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canLoggedIn) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/event-management/supervisor-dashboard/operator-logged-in/view"])

  }

  onHistoryStack() {
    let isAccessDeined = this.getQuickActionIconType(SHORTCUT_KEYS.HISTORY_STACK);
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/event-management/stack-history"])

  }

  navigateToVasDashboardView() {
    this.router.navigate(["/event-management/supervisor-dashboard/vas-dashboard-view"]);
  }

  getQuickActionIconType(actionTypeMenuName): boolean {
    let selectedItem = this.allPermissions.find(item => item.menuName == this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption)
    let foundObj = selectedItem.subMenu.find(item => item.menuName == "Quick Action")['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  onReallocationSuccessful(isSuccess: boolean) {
    if (isSuccess == true) {
      this.refresh++;
    }
  }
}
