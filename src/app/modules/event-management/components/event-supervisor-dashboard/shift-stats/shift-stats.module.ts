import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShiftStatsRoutingModule } from './shift-stats-routing.module';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShiftStatsListComponent } from './shift-stats-list.component';
import { MatRadioModule } from '@angular/material/radio';


@NgModule({
  declarations: [ShiftStatsListComponent],
  imports: [
    CommonModule,
    ShiftStatsRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule
  ]
})
export class ShiftStatsModule { }
