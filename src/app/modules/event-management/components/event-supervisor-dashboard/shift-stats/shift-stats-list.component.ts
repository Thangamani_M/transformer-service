
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-shift-stats-list',
  templateUrl: './shift-stats-list.component.html',
})
export class ShiftStatsListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  filterForm: FormGroup
  filterData: any;
  shiftList: any = [
    { id: 'Day', displayName: 'Day' },
    { id: 'Night', displayName: 'Night' }
  ]
  filterDataObject: { divisionId: any; fromDate: any; siteTypeIds: any; toDate: any; };

  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private momentService: MomentService,
    private _fb: FormBuilder, private router:Router) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Shift Stats",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Hourly Stats', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 6, hourlyChildTab: 12 } }, { displayName: 'Long Phoneback' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Shift Stats',
            dataKey: 'divisionId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'date', header: 'Date', width: '150px' },
            { field: 'shift', header: 'Shift' },
            { field: 'operator', header: 'Operator' },
            { field: 'actionCount', header: 'Actions' },
            { field: 'finishCount', header: 'Finishes' },
            { field: 'medianACKTime', header: 'Median Ack Time' },
            { field: 'medianPhoneTime', header: 'Median Phn Time' },
            { field: 'medianFinishTime', header: 'Median Finish Time' },
            { field: 'aveAcknowledgedTime', header: 'Ave Ack Time' },
            { field: 'avePhoneTime', header: 'Ave Phn Time' },
            { field: 'aveFinishTime', header: 'Ave Finish Time' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SHIFT_STATS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            enableReloadBtn: true,
            enableExportCSV: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;
      this.filterDataObject = {
        divisionId: this.filterData.divisionId,
        fromDate: this.filterData.fromDate,
        siteTypeIds: this.filterData.siteTypeIds,
        toDate: this.filterData.toDate
      }

    });
  }

  ngOnInit(): void {
    this.createFilterForm();
    this.getShiftDetailas();
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      statShift: [[{ id: 'Day' }], Validators.required],
      statDate: [new Date(), Validators.required],
      isDispatcher: [true],
    });
  }

  getShiftDetailas(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.loading = true;
    let shifts = []
    if (this.filterForm.get('statShift').value && this.filterForm.get('statShift').value.length > 0) {
      this.filterForm.get('statShift').value.forEach(element => {
        shifts.push(element.id)
      });
    }
    let filteredData = Object.assign({},
      { statShift: this.filterForm.get('statShift').value ? shifts.toString() : '' },
      { statDate: this.filterForm.get('statDate').value ? this.momentService.convertToUTC(this.filterForm.get('statDate').value) : '' },
      { isDispatcher: this.filterForm.get('isDispatcher').value ? true : false },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "") {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    let searchData = Object.assign({}, filterdNewData, this.filterDataObject)
    otherParams = { ...otherParams, ...searchData };
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;;
        this.totalRecords = data.totalCount;
        if (this.filterForm.get('isDispatcher').value) {
          var column = [{ field: 'date', header: 'Date' }, { field: 'shift', header: 'Shift' }, { field: 'operator', header: 'Operator' }, { field: 'actionCount', header: 'Dispatches' }, { field: 'finishCount', header: 'Finishes' }, { field: 'medianACKTime', header: 'Median Disp Time' }, { field: 'medianFinishTime', header: 'Median Finish Time' }, { field: 'aveAcknowledgedTime', header: 'Ave Disp Time' }, { field: 'aveFinishTime', header: 'Ave Finish Time' }]
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns = column
        } else {
          var column = [{ field: 'date', header: 'Date' }, { field: 'shift', header: 'Shift' }, { field: 'operator', header: 'Operator' }, { field: 'actionCount', header: 'Actions' }, { field: 'finishCount', header: 'Finishes' }, { field: 'medianACKTime', header: 'Median Ack Time' }, { field: 'medianPhoneTime', header: 'Median Phn Time' }, { field: 'medianFinishTime', header: 'Median Finish Time' }, { field: 'aveAcknowledgedTime', header: 'Ave Ack Time' }, { field: 'avePhoneTime', header: 'Ave Phn Time' }, { field: 'aveFinishTime', header: 'Ave Finish Time' }]
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns = column
        }
      }
      else {
        this.dataList = null;;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getShiftDetailas(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.RELOAD:
        this.getShiftDetailas();
        break
      case CrudType.EXPORT:
        this.exportRequest();
        break
    }
  }

  submitFilter() {
    if (this.filterForm.invalid) {
      return
    }
    let otherParams = {}
    this.getShiftDetailas(this.row['pageIndex'], this.row['pageSize'], otherParams);
  }

  refresh() {
    let otherParams = {}
    this.getShiftDetailas(this.row['pageIndex'], this.row['pageSize'], otherParams);
  }
  exportRequest() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  navigateHourly(){
    this.router.navigate(["/event-management/supervisor-dashboard"], { queryParams: { tab: 6,hourlyChildTab:12}, state: { example: this.filterData} });
  }

}

