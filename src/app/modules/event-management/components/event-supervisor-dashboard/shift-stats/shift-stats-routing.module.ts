import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShiftStatsListComponent } from './shift-stats-list.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ShiftStatsListComponent, data: { title: 'Shift Stats' } },
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ShiftStatsRoutingModule { }
