import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatTabsModule } from '@angular/material';
import { OpenAreaListComponent } from './open-area-list.component';
import { OpenAreaListRoutingModule } from './open-area-routing.module';


@NgModule({
  declarations: [OpenAreaListComponent],
  imports: [
    CommonModule,
    OpenAreaListRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatTabsModule
  ]
})
export class OpenAreaListModule { }
