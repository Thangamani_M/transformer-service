import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OpenAreaListComponent } from './open-area-list.component';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: OpenAreaListComponent, data: { title: 'Open Area List' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class OpenAreaListRoutingModule { }
