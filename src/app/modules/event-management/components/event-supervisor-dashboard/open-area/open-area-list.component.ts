import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-open-area-list',
  templateUrl: './open-area-list.component.html',
})
export class OpenAreaListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {}
  openAreaForm: FormGroup;
  mainAreaList: any;
  startTodayDate = new Date();
  todayDate = new Date();

  constructor(
    private rxjsService: RxjsService, private crudService: CrudService,
    private momentService: MomentService, private _fb: FormBuilder, private datePipe :DatePipe) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Open Area",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Open Area' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Open Area',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'displayName', header: 'Main Area' },
              { field: 'calls', header: 'Vehicle' },
              { field: 'customerattended', header: 'Date' },
              { field: 'avgCallDuration', header: 'Shift' },
              { field: 'techscheduleId', header: 'Full/Partial' },
              { field: 'followUp', header: 'Arrivals' },
              { field: 'followUp', header: 'Reason' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.AR_OPEN_AREA,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.createForm();
    this.getMainArea();
  }

  createForm() {
    this.openAreaForm = this._fb.group({
      MainAreaId: ['', Validators.required],
      fromDate:[""],
      toDate :['']
    });
  }

  onSubmit(){
    if (this.openAreaForm.invalid) {
      return;
    }
    this.getOpenAreaList()
  }

  getMainArea() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_AR_OPEN_AREA, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOpenAreaList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    const params = {
      MainAreaId: this.openAreaForm.value.MainAreaId ? this.openAreaForm.value.MainAreaId : '',
      fromDate: this.openAreaForm.value.fromDate ? this.datePipe.transform(this.openAreaForm.value.fromDate, 'yyyy-MM-dd hh:mm') : '',
      toDate: this.openAreaForm.value.toDate ? this.datePipe.transform(this.openAreaForm.value.toDate, 'yyyy-MM-dd hh:mm') : '',
    }
    otherParams = { ...otherParams, ...params };
    Object.keys(otherParams).forEach(key => {
      if (otherParams[key] === "" || otherParams[key].length == 0) {
        delete otherParams[key]
      }
    });
    let filterdNewData = Object.entries(otherParams).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {})
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, filterdNewData)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      else {
        this.dataList = null;
        this.totalRecords = 0
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getOpenAreaList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

}


