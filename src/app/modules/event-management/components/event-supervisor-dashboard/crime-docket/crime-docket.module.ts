import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrimeDocketRoutingModule } from './crime-docket-routing.module';
import { CrimeDocketComponent } from './crime-docket.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MatRadioModule, MatTabsModule } from '@angular/material';
import { CrimeDocketBasicInfoComponent } from './crime-docket-basic-info/crime-docket-basic-info.component';
import { ExistingProductionComponent } from './existing-production/existing-production.component';
import { PostIncidentPrCallComponent } from './post-incident-pr-call/post-incident-pr-call.component';
import { CrimeDocketListComponent } from './crime-docket-list/crime-docket-list.component';


@NgModule({
  declarations: [CrimeDocketComponent, CrimeDocketBasicInfoComponent, ExistingProductionComponent, PostIncidentPrCallComponent, CrimeDocketListComponent],
  imports: [
    CommonModule,
    CrimeDocketRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatRadioModule
  ],
})
export class CrimeDocketModule { }
