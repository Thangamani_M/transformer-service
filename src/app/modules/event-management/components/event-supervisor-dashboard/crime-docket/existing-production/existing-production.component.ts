import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-existing-production',
  templateUrl: './existing-production.component.html',
})
export class ExistingProductionComponent implements OnInit {

  @Input() crimeDocketId: string;
  @Input() crimeDocketNumber: string;
  vehicleRegistrationDetails: any;
  queryAnalystForm: FormGroup;
  loggedUser: any;

  constructor(private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    if (this.crimeDocketId) {
      this.getVehicleRegistrationDetailsById(this.crimeDocketId);
    }
  }

  ngOnInit() {
    this.createVehicleRegistrationForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.crimeDocketId) {
      this.getVehicleRegistrationDetailsById(this.crimeDocketId);
    }
  }

  createVehicleRegistrationForm(): void {
    this.queryAnalystForm = this.formBuilder.group({
      crimeDocketId: [this.crimeDocketId?this.crimeDocketId:null],
      crimeDocketNumber: [''],
      isAlarmSystem: ['false'],
      isMadicalPanic: ['false'],
      isARPadlock: ['false'],
      isElectronicGateAccess: ['false'],
      isOutsidePassives: ['false'],
      isPalisadeFence: ['false'],
      isRazorWire: ['false'],
      isWireFence: ['false'],
      isWindowsProtectedByBurglarBars: ['false'],
      isGlassWindowShop: ['false'],
      isElectricFenceAroundPrem: ['false'],
      isElectricFenceWorking: ['false'],
      isPanic: ['false'],
      isDuress: ['false'],
      isDigipad: ['false'],
      isBeams: ['false'],
      isBrickWall: ['false'],
      isPrecastWall: ['false'],
      isSpikes: ['false'],
      isDoorsProtectedByBurglarGates: ['false'],
      isCCTV: ['false'],
      isGuards: ['false'],
      isElectricFenceTested: ['false'],
      recommendationsExplanations: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(250)]],
      isActive: [true],
      createdUserId: [''],
      modifiedUserId: ['']
    });

    this.queryAnalystForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.queryAnalystForm.get('modifiedUserId').setValue(this.loggedUser.userId)

  }


  getVehicleRegistrationDetailsById(crimeDocketId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_EXISTING_PROTECTION_CRIME_DOCKET, crimeDocketId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleRegistrationDetails = response.resources;
          response.resources.isARPadlock = response.resources.isARPadlock ? 'true' : 'false'
          response.resources.isAlarmSystem = response.resources.isAlarmSystem ? 'true' : 'false'
          response.resources.isBeams = response.resources.isBeams ? 'true' : 'false'
          response.resources.isBrickWall = response.resources.isBrickWall ? 'true' : 'false'
          response.resources.isCCTV = response.resources.isCCTV ? 'true' : 'false'
          response.resources.isDigipad = response.resources.isDigipad ? 'true' : 'false'
          response.resources.isDoorsProtectedByBurglarGates = response.resources.isDoorsProtectedByBurglarGates ? 'true' : 'false'
          response.resources.isDuress = response.resources.isDuress ? 'true' : 'false'
          response.resources.isElectricFenceAroundPrem = response.resources.isElectricFenceAroundPrem ? 'true' : 'false'
          response.resources.isElectricFenceTested = response.resources.isElectricFenceTested ? 'true' : 'false'
          response.resources.isElectricFenceWorking = response.resources.isElectricFenceWorking ? 'true' : 'false'
          response.resources.isElectronicGateAccess = response.resources.isElectronicGateAccess ? 'true' : 'false'
          response.resources.isGlassWindowShop = response.resources.isGlassWindowShop ? 'true' : 'false'
          response.resources.isGuards = response.resources.isGuards ? 'true' : 'false'
          response.resources.isMadicalPanic = response.resources.isMadicalPanic ? 'true' : 'false'
          response.resources.isOutsidePassives = response.resources.isOutsidePassives ? 'true' : 'false'
          response.resources.isPalisadeFence = response.resources.isPalisadeFence ? 'true' : 'false'
          response.resources.isPanic = response.resources.isPanic ? 'true' : 'false'
          response.resources.isPrecastWall = response.resources.isPrecastWall ? 'true' : 'false'
          response.resources.isRazorWire = response.resources.isRazorWire ? 'true' : 'false'
          response.resources.isSpikes = response.resources.isSpikes ? 'true' : 'false'
          response.resources.isWindowsProtectedByBurglarBars = response.resources.isWindowsProtectedByBurglarBars ? 'true' : 'false'
          response.resources.isWireFence = response.resources.isWireFence ? 'true' : 'false'
          this.queryAnalystForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onSubmit(): void {
    if (this.queryAnalystForm.invalid) {
      return;
    }
    let formValue = this.queryAnalystForm.value;
    formValue.isARPadlock = formValue.isARPadlock == 'true' ? true : false
    formValue.isAlarmSystem = formValue.isAlarmSystem == 'true' ? true : false
    formValue.isBeams = formValue.isBeams == 'true' ? true : false
    formValue.isBrickWall = formValue.isBrickWall == 'true' ? true : false
    formValue.isCCTV = formValue.isCCTV == 'true' ? true : false
    formValue.isDigipad = formValue.isDigipad == 'true' ? true : false
    formValue.isDoorsProtectedByBurglarGates = formValue.isDoorsProtectedByBurglarGates == 'true' ? true : false
    formValue.isDuress = formValue.isDuress == 'true' ? true : false
    formValue.isElectricFenceAroundPrem = formValue.isElectricFenceAroundPrem == 'true' ? true : false
    formValue.isElectricFenceTested = formValue.isElectricFenceTested == 'true' ? true : false
    formValue.isElectricFenceWorking = formValue.isElectricFenceWorking == 'true' ? true : false
    formValue.isElectronicGateAccess = formValue.isElectronicGateAccess == 'true' ? true : false
    formValue.isGlassWindowShop = formValue.isGlassWindowShop == 'true' ? true : false
    formValue.isGuards = formValue.isGuards == 'true' ? true : false
    formValue.isMadicalPanic = formValue.isMadicalPanic == 'true' ? true : false
    formValue.isOutsidePassives = formValue.isOutsidePassives == 'true' ? true : false
    formValue.isPalisadeFence = formValue.isPalisadeFence == 'true' ? true : false
    formValue.isPanic = formValue.isPanic == 'true' ? true : false
    formValue.isPrecastWall = formValue.isPrecastWall == 'true' ? true : false
    formValue.isRazorWire = formValue.isRazorWire == 'true' ? true : false
    formValue.isSpikes = formValue.isSpikes == 'true' ? true : false
    formValue.isWindowsProtectedByBurglarBars = formValue.isWindowsProtectedByBurglarBars == 'true' ? true : false
    formValue.isWireFence = formValue.isWireFence == 'true' ? true : false
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.crimeDocketId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_EXISTING_PROTECTION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_EXISTING_PROTECTION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess) {
        this.getVehicleRegistrationDetailsById(this.crimeDocketId)
      }

    })
  }

}
