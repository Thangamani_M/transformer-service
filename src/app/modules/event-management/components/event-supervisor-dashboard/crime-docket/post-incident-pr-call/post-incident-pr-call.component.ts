import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, countryCodes, CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-post-incident-pr-call',
  templateUrl: './post-incident-pr-call.component.html',
})
export class PostIncidentPrCallComponent implements OnInit {

  @Input() crimeDocketId: string;
  @Input() crimeDocketNumber: string;
  vehicleRegistrationDetails: any;
  queryAnalystForm: FormGroup;
  loggedUser: any;
  countryCodes = countryCodes;

  constructor(private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    if (this.crimeDocketId) {
      this.getVehicleRegistrationDetailsById(this.crimeDocketId);
    }
  }

  ngOnInit() {
    this.createVehicleRegistrationForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.crimeDocketId) {
      this.getVehicleRegistrationDetailsById(this.crimeDocketId);
    }
  }

  createVehicleRegistrationForm(): void {
    this.queryAnalystForm = this.formBuilder.group({
      crimeDocketId: [''],
      crimeDocketNumber: [''],
      isSatisfiedIncidentHandled: ['false'],
      satisfiedIncidentHandledComments: ['', [Validators.minLength(2), Validators.maxLength(250)]],
      isSpecialInstructions: ['false'],
      specialInstructionsComments: ['', [Validators.minLength(2), Validators.maxLength(250)]],
      verifyAccessToSite: ['', [Validators.minLength(2), Validators.maxLength(250)]],
      verifyContactDetails: ['', [Validators.minLength(2), Validators.maxLength(250)]],
      submittedBy: [''],
      createdUserId: [''],
      modifiedUserId: ['']
    });

    this.queryAnalystForm.get('submittedBy').setValue(this.loggedUser.userId)
    this.queryAnalystForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.queryAnalystForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.queryAnalystForm.get('isSatisfiedIncidentHandled').valueChanges.subscribe(val => {
      if (val == 'true') {
        this.queryAnalystForm = setRequiredValidator(this.queryAnalystForm, ['satisfiedIncidentHandledComments']);
        this.queryAnalystForm.get('satisfiedIncidentHandledComments').setValidators([Validators.minLength(2), Validators.maxLength(250)])
      }
      else {
        this.queryAnalystForm = clearFormControlValidators(this.queryAnalystForm, ["satisfiedIncidentHandledComments"]);
        this.queryAnalystForm.get('satisfiedIncidentHandledComments').setValidators([Validators.minLength(2), Validators.maxLength(250)])
      }
    })
    this.queryAnalystForm.get('isSpecialInstructions').valueChanges.subscribe(val => {
      if (val == 'true') {
        this.queryAnalystForm = setRequiredValidator(this.queryAnalystForm, ['specialInstructionsComments']);
        this.queryAnalystForm.get('specialInstructionsComments').setValidators([Validators.minLength(2), Validators.maxLength(250)])

      }
      else {
        this.queryAnalystForm = clearFormControlValidators(this.queryAnalystForm, ["specialInstructionsComments"]);
        this.queryAnalystForm.get('specialInstructionsComments').setValidators([Validators.minLength(2), Validators.maxLength(250)])
      }
    })
  }


  getVehicleRegistrationDetailsById(crimeDocketId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_POST_INCIDENT_PR_CALL, null,
      false,
      prepareGetRequestHttpParams(null, null,
        { CrimeDocketId: crimeDocketId, ModifiedUserId: this.loggedUser.userId, }
      ))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleRegistrationDetails = response.resources;
          response.resources.isSatisfiedIncidentHandled = response.resources.isSatisfiedIncidentHandled ? 'true' : 'false'
          response.resources.isSpecialInstructions = response.resources.isSpecialInstructions ? 'true' : 'false'
          this.queryAnalystForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onSubmit(): void {
    if (this.queryAnalystForm.invalid) {
      return;
    }
    let formValue = this.queryAnalystForm.value;
    formValue.isSatisfiedIncidentHandled = formValue.isSatisfiedIncidentHandled == 'true' ? true : false
    formValue.isSpecialInstructions = formValue.isSpecialInstructions == 'true' ? true : false
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.crimeDocketId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_POST_INCIDENT_PR_CALL, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_POST_INCIDENT_PR_CALL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess) {
        this.getVehicleRegistrationDetailsById(this.crimeDocketId)
      }

    })
  }
}
