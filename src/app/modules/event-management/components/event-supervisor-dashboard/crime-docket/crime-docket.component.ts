import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CrimeDocketDogsModel, CrimeDocketModel, CrimeDocketStolenItemsModel, CrimeDocketSuspectsModel, CrimeDocketTakenVehiclesModel, CrimeDocketVehiclesModel, CrimeDocketVictimsModel } from '../models/crime-docket.model';

@Component({
  selector: 'app-crime-docket',
  templateUrl: './crime-docket.component.html',
})
export class CrimeDocketComponent implements OnInit {

  crimeDocketId: any
  crimeDocketNumber: any
  occurrenceBookId: any
  selectedTabIndex: any = 0
  selectedTabCrimDocketIndex: any = 0
  openQuickAction: boolean = false
  crimeDocketForm: FormGroup
  customerName: any;
  loggedUser: any;
  vehicleRegistrationDetails: any
  allDropdowns: any
  crimeDocketExistingProtection: any
  crimeDocketPostIncidentPublicRelationCall: any
  primengTableConfigProperties: any;

  constructor(private activatedRoute: ActivatedRoute, private momentService: MomentService, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.getAllDropdowns()
    if (this.crimeDocketId) {
      this.getExistingProductionDetailsById(this.crimeDocketId)
      this.getPostIncidentDetailsById(this.crimeDocketId)
      this.getVehicleRegistrationDetailsById(this.crimeDocketId);
    } this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.selectedTabCrimDocketIndex = (Object.keys(params['params']).length > 0) ? +params['params']['childTab'] : 0;
      this.crimeDocketId = params['params'] ? params['params']['crimeDocketId'] : null;
      this.crimeDocketNumber = params['params'] ? params['params']['crimeDocketNumber'] : null;
      this.occurrenceBookId = params['params'] ? params['params']['occurrenceBookId'] : null;
      this.customerName = params['params'] ? params['params']['customerName'] : null;
    });

    this.primengTableConfigProperties = {
      tableCaption: 'Crime Docket',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' },
      { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } },{ displayName: 'Crime Docket', relativeRouterUrl: '/event-management/supervisor-dashboard/crime-docket' },{ displayName: this.selectedTabIndex == 0 ? 'Crime Docket' : this.selectedTabIndex == 1 ? 'Existing Protection' : this.selectedTabIndex == 2 ? 'Post Incident PR Call' : '', relativeRouterUrl: ''}],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.createVehicleRegistrationForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.crimeDocketId) {
      this.getExistingProductionDetailsById(this.crimeDocketId)
      this.getPostIncidentDetailsById(this.crimeDocketId)
      this.getVehicleRegistrationDetailsById(this.crimeDocketId);
    }
  }

  createVehicleRegistrationForm(): void {
    let crimeDocketModle = new CrimeDocketModel()
    this.crimeDocketForm = this.formBuilder.group({
      suspects: this.formBuilder.array([]),
      victims: this.formBuilder.array([]),
      stolenItems: this.formBuilder.array([]),
      vehicles: this.formBuilder.array([]),
      dogs: this.formBuilder.array([]),
      takenVehicles: this.formBuilder.array([])
    });
    Object.keys(crimeDocketModle).forEach((key) => {
      this.crimeDocketForm.addControl(key, new FormControl(crimeDocketModle[key]));
    });

    this.crimeDocketForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.crimeDocketForm.get('modifiedUserId').setValue(this.loggedUser.userId)

    if (!this.crimeDocketId) {
      this.getCrimeDocketSuspectsListArray.push(this.createCrimeDocktetSuspectsListModel());
      this.getCrimeDocketVictimsListArray.push(this.createCrimeDocktetVictimsListModel());
      this.getCrimeDocketStolenItemsListArray.push(this.createCrimeDocktetStolenItemsListModel());
      this.getCrimeDocketVehiclesListArray.push(this.createCrimeDocktetVehiclesListModel());
      this.getCrimeDocketDogsListArray.push(this.createCrimeDocktetDogsListModel());
      this.getCrimeDocketTakenVehiclesArray.push(this.createCrimeDocktetTakenVehiclesListModel());
    }
  }

  //Create FormArray
  get getCrimeDocketSuspectsListArray(): FormArray {
    if (!this.crimeDocketForm) return;
    return this.crimeDocketForm.get("suspects") as FormArray;
  }
  //Create FormArray
  get getCrimeDocketVictimsListArray(): FormArray {
    if (!this.crimeDocketForm) return;
    return this.crimeDocketForm.get("victims") as FormArray;
  }
  //Create FormArray
  get getCrimeDocketStolenItemsListArray(): FormArray {
    if (!this.crimeDocketForm) return;
    return this.crimeDocketForm.get("stolenItems") as FormArray;
  }
  //Create FormArray
  get getCrimeDocketVehiclesListArray(): FormArray {
    if (!this.crimeDocketForm) return;
    return this.crimeDocketForm.get("vehicles") as FormArray;
  }
  //Create FormArray
  get getCrimeDocketDogsListArray(): FormArray {
    if (!this.crimeDocketForm) return;
    return this.crimeDocketForm.get("dogs") as FormArray;
  }
  //Create FormArray
  get getCrimeDocketTakenVehiclesArray(): FormArray {
    if (!this.crimeDocketForm) return;
    return this.crimeDocketForm.get("takenVehicles") as FormArray;
  }

  //Create FormArray controls
  createCrimeDocktetSuspectsListModel(crimeDocketSuspectsModel?: CrimeDocketSuspectsModel): FormGroup {
    let crimeDocketSuspectsModelControl = new CrimeDocketSuspectsModel(crimeDocketSuspectsModel);
    let formControls = {};
    Object.keys(crimeDocketSuspectsModelControl).forEach((key) => {
      formControls[key] = [{ value: crimeDocketSuspectsModelControl[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createCrimeDocktetVictimsListModel(crimeDocketSuspectsModel?: CrimeDocketVictimsModel): FormGroup {
    let crimeDocketSuspectsModelControl = new CrimeDocketVictimsModel(crimeDocketSuspectsModel);
    let formControls = {};
    Object.keys(crimeDocketSuspectsModelControl).forEach((key) => {
      formControls[key] = [{ value: crimeDocketSuspectsModelControl[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createCrimeDocktetStolenItemsListModel(crimeDocketSuspectsModel?: CrimeDocketStolenItemsModel): FormGroup {
    let crimeDocketSuspectsModelControl = new CrimeDocketStolenItemsModel(crimeDocketSuspectsModel);
    let formControls = {};
    Object.keys(crimeDocketSuspectsModelControl).forEach((key) => {
      formControls[key] = [{ value: crimeDocketSuspectsModelControl[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createCrimeDocktetVehiclesListModel(crimeDocketSuspectsModel?: CrimeDocketVehiclesModel): FormGroup {
    let crimeDocketSuspectsModelControl = new CrimeDocketVehiclesModel(crimeDocketSuspectsModel);
    let formControls = {};
    Object.keys(crimeDocketSuspectsModelControl).forEach((key) => {
      formControls[key] = [{ value: crimeDocketSuspectsModelControl[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createCrimeDocktetDogsListModel(crimeDocketSuspectsModel?: CrimeDocketDogsModel): FormGroup {
    let crimeDocketSuspectsModelControl = new CrimeDocketDogsModel(crimeDocketSuspectsModel);
    let formControls = {};
    Object.keys(crimeDocketSuspectsModelControl).forEach((key) => {
      formControls[key] = [{ value: crimeDocketSuspectsModelControl[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createCrimeDocktetTakenVehiclesListModel(crimeDocketSuspectsModel?: CrimeDocketTakenVehiclesModel): FormGroup {
    let crimeDocketSuspectsModelControl = new CrimeDocketTakenVehiclesModel(crimeDocketSuspectsModel);
    let formControls = {};
    Object.keys(crimeDocketSuspectsModelControl).forEach((key) => {
      formControls[key] = [{ value: crimeDocketSuspectsModelControl[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  onSequenceChangeEvent(event: any) {
    this.selectedTabIndex = event.index;
    this.router.navigate(['/event-management/supervisor-dashboard/crime-docket/view'], { queryParams: { tab: this.selectedTabIndex, crimeDocketId: this.crimeDocketId, crimeDocketNumber: this.crimeDocketNumber, occurrenceBookId: this.occurrenceBookId } })
    this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.selectedTabIndex == 0 ? 'Crime Docket' : this.selectedTabIndex == 1 ? 'Existing Protection' : this.selectedTabIndex == 2 ? 'Post Incident PR Call' : '';
  }

  onCrimeDocketChangeEvent(event: any) {
    this.selectedTabCrimDocketIndex = event.index;
  }

  toggleQuickAction() {
    this.openQuickAction = !this.openQuickAction
  }

  getAllDropdowns() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_PREREQUISITE, null,
      false,
    )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.allDropdowns = response.resources
        }
      })
  }

  getVehicleRegistrationDetailsById(crimeDocketId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_Id, crimeDocketId,
      false,
    )
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.vehicleRegistrationDetails = response.resources;
          response.resources.activationTime = new Date(response.resources.activationTime)
          response.resources.gprsArrivalTime = new Date(response.resources.gprsArrivalTime)
          response.resources.responseTime = new Date(this.momentService.setTime(response.resources.responseTime))
          this.crimeDocketForm.patchValue(response.resources)
          if (response.resources.suspects.length > 0) {
            response.resources.suspects.forEach((crimeDocketSuspectsModel: CrimeDocketSuspectsModel) => {
              this.getCrimeDocketSuspectsListArray.push(this.createCrimeDocktetSuspectsListModel(crimeDocketSuspectsModel));
            });
          } else {
            this.getCrimeDocketSuspectsListArray.push(this.createCrimeDocktetSuspectsListModel());
          }
          if (response.resources.victims.length > 0) {
            response.resources.victims.forEach((crimeDocketSuspectsModel: CrimeDocketVictimsModel) => {
              this.getCrimeDocketVictimsListArray.push(this.createCrimeDocktetVictimsListModel(crimeDocketSuspectsModel));
            });
          } else {
            this.getCrimeDocketVictimsListArray.push(this.createCrimeDocktetVictimsListModel());
          }
          if (response.resources.stolenItems.length > 0) {
            response.resources.stolenItems.forEach((crimeDocketSuspectsModel: CrimeDocketStolenItemsModel) => {
              this.getCrimeDocketStolenItemsListArray.push(this.createCrimeDocktetStolenItemsListModel(crimeDocketSuspectsModel));
            });
          } else {
            this.getCrimeDocketStolenItemsListArray.push(this.createCrimeDocktetStolenItemsListModel());
          }
          if (response.resources.vehicles.length > 0) {
            response.resources.vehicles.forEach((crimeDocketSuspectsModel: CrimeDocketVehiclesModel) => {
              this.getCrimeDocketVehiclesListArray.push(this.createCrimeDocktetVehiclesListModel(crimeDocketSuspectsModel));
            });
          } else {
            this.getCrimeDocketVehiclesListArray.push(this.createCrimeDocktetVehiclesListModel());
          }
          if (response.resources.dogs.length > 0) {
            response.resources.dogs.forEach((crimeDocketSuspectsModel: CrimeDocketDogsModel) => {
              this.getCrimeDocketDogsListArray.push(this.createCrimeDocktetDogsListModel(crimeDocketSuspectsModel));
            });
          } else {
            this.getCrimeDocketDogsListArray.push(this.createCrimeDocktetDogsListModel());
          }
          if (response.resources.takenVehicles.length > 0) {
            response.resources.takenVehicles.forEach((crimeDocketSuspectsModel: CrimeDocketTakenVehiclesModel) => {
              this.getCrimeDocketTakenVehiclesArray.push(this.createCrimeDocktetTakenVehiclesListModel(crimeDocketSuspectsModel));
            });
          } else {
            this.getCrimeDocketTakenVehiclesArray.push(this.createCrimeDocktetTakenVehiclesListModel());
          }

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  addRowSuspect() {
    let crimeDocketSuspectsModel = new CrimeDocketSuspectsModel()
    this.getCrimeDocketSuspectsListArray.insert(0, this.createCrimeDocktetSuspectsListModel(crimeDocketSuspectsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  addRowVictim() {
    let crimeDocketSuspectsModel = new CrimeDocketVictimsModel()
    this.getCrimeDocketVictimsListArray.insert(0, this.createCrimeDocktetVictimsListModel(crimeDocketSuspectsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  addRowStolenItem() {
    let crimeDocketSuspectsModel = new CrimeDocketStolenItemsModel()
    this.getCrimeDocketStolenItemsListArray.insert(0, this.createCrimeDocktetStolenItemsListModel(crimeDocketSuspectsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  addRowDogs() {
    let crimeDocketSuspectsModel = new CrimeDocketDogsModel()
    this.getCrimeDocketDogsListArray.insert(0, this.createCrimeDocktetDogsListModel(crimeDocketSuspectsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  addRowVehicle() {
    let crimeDocketSuspectsModel = new CrimeDocketVehiclesModel()
    this.getCrimeDocketVehiclesListArray.insert(0, this.createCrimeDocktetVehiclesListModel(crimeDocketSuspectsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  addRowTakenVehicle() {
    let crimeDocketSuspectsModel = new CrimeDocketTakenVehiclesModel()
    this.getCrimeDocketTakenVehiclesArray.insert(0, this.createCrimeDocktetTakenVehiclesListModel(crimeDocketSuspectsModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  getExistingProductionDetailsById(crimeDocketId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_EXISTING_PROTECTION_CRIME_DOCKET, crimeDocketId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          response.resources.modifiedUserId = this.loggedUser.userId
          this.crimeDocketExistingProtection = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getPostIncidentDetailsById(crimeDocketId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_POST_INCIDENT_PR_CALL, null,
      false,
      prepareGetRequestHttpParams(null, null,
        { CrimeDocketId: crimeDocketId, ModifiedUserId: this.loggedUser.userId, }
      ))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.crimeDocketPostIncidentPublicRelationCall = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.crimeDocketForm.invalid) {
      return;
    }
    if (this.crimeDocketId) {  // for save
      let formValue = this.crimeDocketForm.value;
      formValue.suspects = formValue.crimeDocketSuspects
      formValue.victims = formValue.crimeDocketVictims
      formValue.stolenItems = formValue.crimeDocketStolenItems
      formValue.vehicles = formValue.crimeDocketVehicles
      formValue.dogs = formValue.crimeDocketDogs
      formValue.takenVehicles = formValue.crimeDocketTakenVehicles
      let finalFormValue = {
        crimeDocket: formValue,
        crimeDocketExistingProtection: this.crimeDocketExistingProtection,
        crimeDocketPostIncidentPublicRelationCall: {
          crimeDocketId: this.crimeDocketPostIncidentPublicRelationCall.crimeDocketId,
          isSatisfiedIncidentHandled: this.crimeDocketPostIncidentPublicRelationCall.isSatisfiedIncidentHandled,
          satisfiedIncidentHandledComments: this.crimeDocketPostIncidentPublicRelationCall.satisfiedIncidentHandledComments,
          isSpecialInstructions: this.crimeDocketPostIncidentPublicRelationCall.isSpecialInstructions,
          specialInstructionsComments: this.crimeDocketPostIncidentPublicRelationCall.specialInstructionsComments,
          submittedBy: this.crimeDocketPostIncidentPublicRelationCall.submittedBy,
          verifyAccessToSite: this.crimeDocketPostIncidentPublicRelationCall.verifyAccessToSite,
          verifyContactDetails: this.crimeDocketPostIncidentPublicRelationCall.verifyContactDetails,
          premisesNoCountryCode: this.crimeDocketPostIncidentPublicRelationCall.premisesNoCountryCode,
          modifiedUserId: this.loggedUser.userId
        }

      }
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_COMPLETED, finalFormValue)
      crudService.subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        if (response.isSuccess) {
          this.getVehicleRegistrationDetailsById(this.crimeDocketId)
        }

      })
    } else { // for draft
      let formValue = this.crimeDocketForm.value;
      if (this.customerName == 'Mr Bradley Richmond ') {
        formValue.occurrenceBookId = this.occurrenceBookId
      }
      else {
        formValue.occurrenceBookId = this.occurrenceBookId
      }
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CRIME_DOCKET_DRAFT, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        if (response.isSuccess) {
          this.crimeDocketId = response.resources
          this.getVehicleRegistrationDetailsById(this.crimeDocketId)
        }

      })
    }

  }

}
