import { Component, Input, OnInit } from '@angular/core';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-crime-docket-basic-info',
  templateUrl: './crime-docket-basic-info.component.html',
})
export class CrimeDocketBasicInfoComponent implements OnInit {

  @Input() occurrenceBookId: string;
  selectedTabIndex = 0;
  primengTableConfigProperties: any
  crimeDocetViewDetails: any

  constructor(private rxjsService: RxjsService, private crudService: CrudService) {
    this.primengTableConfigProperties = {
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getClientDetailsById().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.crimeDocetViewDetails = response.resources;
      this.onShowValue(response.resources);
    })
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  getClientDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CRIME_DOCKET_OCCURENCE_BOOK,
      this.occurrenceBookId
    );
  }

  onShowValue(response?: any) {
    this.crimeDocetViewDetails = [
      {
        name: 'Basic Info', columns: [
          { name: 'Division', value: response?.divisionName },
          { name: 'Main Area', value: response?.mainAreaName },
          { name: 'Sub Area', value: response?.subAreaName },
          { name: 'Shift', value: response?.shiftName },
          { name: 'Customer Name', value: response?.customerName },
          { name: 'Customer ID', value: response?.customerNumber },
          { name: 'Incident Date', value: response?.incidentDate },
          { name: 'In LSS Scheme', value: response?.isLSS },
          { name: 'Site Type', value: response?.siteTypeName },
          { name: 'Building No.', value: response?.buildingNumber },
          { name: 'Building Name', value: response?.buildingName },
          { name: 'Street No', value: response?.streetNumber },
          { name: 'Street Name', value: response?.streetAddress },
          { name: 'Street Type', value: response?.streetType },
          { name: 'Suburb', value: response?.suburbName },
          { name: 'Province', value: response?.provinceName },
          { name: 'Postal Code', value: response?.postalCode },
        ]
      }
    ]
  }
}