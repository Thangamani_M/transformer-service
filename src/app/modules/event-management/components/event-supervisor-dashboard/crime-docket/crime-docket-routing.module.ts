import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrimeDocketListComponent } from './crime-docket-list/crime-docket-list.component';
import { CrimeDocketComponent } from './crime-docket.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: CrimeDocketListComponent, data: { title: 'Crime Docket List' } },
  { path: 'view', component: CrimeDocketComponent, data: { title: 'Crime Docket View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CrimeDocketRoutingModule { }
