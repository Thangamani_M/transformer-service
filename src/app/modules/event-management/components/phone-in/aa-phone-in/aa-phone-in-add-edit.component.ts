import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AaPhoneInDialogComponent } from './aa-phone-in-dialog/aa-phone-in-dialog.component';
declare var google: any;

@Component({
  selector: 'app-aa-phone-in-add-edit',
  templateUrl: './aa-phone-in-add-edit.component.html',
  styleUrls: ['./aa-phone-in-add-edit.component.scss']
})
export class AaPhoneInAddEditComponent implements OnInit {
  dispatchForm: FormGroup;
  options: any;
  overlays: any;
  openMapDialog: boolean = false;
  map: any;
  loggedUser: any;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  makeList: any = []
  modelList: any = []
  requstorAddressList: any = []
  addressList: any = []
  vehicleColorList: any;
  customerId: any;
  addressId:any;
  feature: string;
  featureIndex: string;
  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.feature = this.activatedRoute.snapshot.queryParams?.feature_name;
    this.featureIndex = this.activatedRoute.snapshot.queryParams?.featureIndex;
    
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.map = google.maps.Map;
  }

  ngOnInit() {
    this.createDispatchForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getMakeList()
    this.getVehicleColor()
    this.getModelList()
    this.options = {
      center: {
        lat: -25.746020,
        lng: 28.187120
      },
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
  }

  createDispatchForm(): void {
    this.dispatchForm = this.formBuilder.group({
      customerAddressId: [this.addressId],
      automobileAssistanceMembershipNumber: ['', Validators.required],
      automobileAssistanceReferenceNumber: ['', Validators.required],
      clientName: ['', Validators.required],
      clientContactNumberCountryCode: ['+27', Validators.required],
      clientContactNumber: ['', Validators.required],
      locationBuildingNumber: [''],
      locationBuildingName: [''],
      locationStreetTypeId: [''],
      locationStreetNumber: [''],
      locationStreetAddress: [''],
      locationSuburbName: [''],
      locationCityName: [''],
      locationProvinceName: [''],
      locationPostalCode: [''],
      locationLongitude: [''],
      locationLatitude: [''],
      location: ['', Validators.required],
      locationFullAddress: [''],
      locationJSONAddress: [''],
      vehicleMakeName: [null, Validators.required],
      vehicleModelName: [null, Validators.required],
      vehicleColor: [null, Validators.required],
      vehicleRegistrationNumber: ['', Validators.required],
      vehicleDescription: [''],
      createdUserId: [this.loggedUser.userId],
      modifiedUserId: [this.loggedUser.userId],
      CustomerId:[this.customerId]
    });
  }

  getMakeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MAKE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.makeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getModelList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MODEL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.modelList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getVehicleColor() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_COLOR, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleColorList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSearchAddress(address) {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ADDRESS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        SearchText: address.query,
        IsAfrigisSearch: false,
      }), 1).subscribe((res) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        if (Array.isArray(res.resources)) {
          this.addressList = res.resources;
        } else {
          this.dispatchForm.get('location').setValue(null)
          this.addressList = [];

        }
      })
  }


  openMap() {
   
    this.options = {};
    this.overlays = {};
    let location = [ -25.746020,28.187120]
    this.openMapDialog = true
    if (!location) {
    } else {
      location = []
      location[0] = -25.746020 // fidelity south afcia coordinates by default
      location[1] = 28.187120 // fidelity south afcia coordinates by default
    }
    this.options = {
      center: { lat: Number(location[0]), lng: Number(location[1]) },
      zoom: 12
    };

    setTimeout(() => {
      this.map.setCenter({
        lat: Number(location[0]),
        lng: Number(location[1])
      });
    }, 500);

    this.overlays = [new google.maps.Marker({ position: { lat: Number(location[0]), lng: Number(location[1]) }, icon: "assets/img/map-icon.png" })];
  }
 

  handleMapClick(event) {
    //event: MouseEvent of Google Maps api
    this.dispatchForm.get('locationLongitude').setValue(event.latLng.lat())
    this.dispatchForm.get('locationLatitude').setValue(event.latLng.lng())
    this.openMapDialog = false
    this.rxjsService.setFormChangeDetectionProperty(true)

  }
  

  setMap(event) {
    this.map = event.map;
  }
  onSelectAddress(event) {
    this.dispatchForm.get('locationLatitude').patchValue(event.latitude)
    this.dispatchForm.get('locationLongitude').patchValue(event.longitude)

    this.dispatchForm.get('locationFullAddress').patchValue(event.fullAddress)

    this.dispatchForm.get('locationBuildingNumber').patchValue(event.buildingNo)
    this.dispatchForm.get('locationBuildingName').patchValue(event.buildingName)
    this.dispatchForm.get('locationStreetTypeId').patchValue(event.addressId)
    this.dispatchForm.get('locationStreetNumber').patchValue(event.streetNo)
    this.dispatchForm.get('locationStreetAddress').patchValue(event.streetName)
    this.dispatchForm.get('locationSuburbName').patchValue(event.suburbName)
    this.dispatchForm.get('locationCityName').patchValue(event.cityName)
    this.dispatchForm.get('locationProvinceName').patchValue(event.provinceName)
    this.dispatchForm.get('locationPostalCode').patchValue(event.postalCode)
  }


  navigatTo(){
      let queryParams = {addressId: this.addressId};
      if(this.feature && this.featureIndex) {
        queryParams['feature_name'] = this.feature;
        queryParams['featureIndex'] = this.featureIndex;
      }
      this.router.navigate(["customer/manage-customers/view/"+[this.customerId]], { queryParams: queryParams});
  }

  onSubmit(): void {
    this.dispatchForm.get('location').markAsDirty();
    if (this.dispatchForm.invalid) {
      return;
    }
    let formValue = this.dispatchForm.value;
    formValue.clientContactNumber = formValue.clientContactNumber.split(' ').join('')
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_AUTOMOBILE_ASSISTANCE, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.dispatchForm.reset()
        this.dispatchForm.get('customerAddressId').setValue(this.addressId)
        this.dispatchForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.dispatchForm.get('modifiedUserId').setValue(this.loggedUser.userId)
        let queryParams = {addressId: this.addressId};
        if(this.feature && this.featureIndex) {
          queryParams['feature_name'] = this.feature;
          queryParams['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(["customer/manage-customers/view/" + [this.customerId]], { queryParams: queryParams});
      }
    })
  }


  openDialog(id): void {
    const dialogRef = this.dialog.open(AaPhoneInDialogComponent, {
      width: '750px',
      data: id
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

}
