
import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { AaPhoneInAddEditComponent } from '../aa-phone-in-add-edit.component';

@Component({
  selector: 'app-aa-phone-in-dialog',
  templateUrl: './aa-phone-in-dialog.component.html',
  styleUrls: ['./aa-phone-in-dialog.component.scss']
})
export class AaPhoneInDialogComponent implements OnInit {

  dispatchId: string;
  dispatchDetails: any;
  dispatchForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  customerAddressId: any
  @Input() resetId: any
  constructor(public dialogRef: MatDialogRef<AaPhoneInAddEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,  private store: Store<AppState>,  private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.getDispatchDetailsById(this.data)

  }

  getDispatchDetailsById(dispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_AUTOMOBILE_ASSISTANCE, dispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onNoClick(): void {
    this.dialogRef.close();
  }


}


