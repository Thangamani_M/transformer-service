import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { GMapModule } from 'primeng/gmap';
import { AaPhoneInAddEditComponent } from './aa-phone-in-add-edit.component';
import { AaPhoneInDialogComponent } from './aa-phone-in-dialog/aa-phone-in-dialog.component';
import { AaPhoneInRoutingModule } from './aa-phone-in-routing.module';


@NgModule({
  declarations: [AaPhoneInAddEditComponent, AaPhoneInDialogComponent],
  imports: [
    CommonModule,
    AaPhoneInRoutingModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    GMapModule
  ],
  entryComponents: [AaPhoneInDialogComponent]
})
export class AaPhoneInModule { }
