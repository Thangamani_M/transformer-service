import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AaPhoneInAddEditComponent } from './aa-phone-in-add-edit.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: AaPhoneInAddEditComponent, data: { title: 'Phone In add edit' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class AaPhoneInRoutingModule { }
