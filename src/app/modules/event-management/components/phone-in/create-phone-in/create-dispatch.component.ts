import { Component, Inject, Input, OnInit, Optional } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { AddContactDialogComponent } from '@modules/customer/components/customer/customer-management/add-contact-dialog/add-contact-dialog.component';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
import { QuickAddServiceCallInitiationModel, QuickCommentsListModel } from "../../../../../../app/modules/event-management/models/configurations/create-dispatch.model";
@Component({
  selector: 'app-create-dispatch',
  templateUrl: './create-dispatch.component.html',
  styleUrls: ['./create-dispatch.component.scss']
})
export class CreateDispatchComponent implements OnInit {

  callDetailsData: any;
  comments: FormArray;
  dispatchId: string;
  dispatchForm: FormGroup;
  loggedUser: any;
  contactList: any = [];
  contactNumberList: any = []
  keyholderId: any = '';
  alarmTypeId: any = '';
  priority: any = '';
  notepad: any = '';
  show: boolean;
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  alarmTypeList: any = []
  priorityList: any = [
    { displayName: '1' },
    { displayName: '2' },
    { displayName: '3' },
    { displayName: '4' },
    { displayName: '5' },
    { displayName: '6' },
    { displayName: '7' },
    { displayName: '8' },
    { displayName: '9' },
    { displayName: '10' },
  ]
  isBookTechnician = false;
  callInitiationId: any;
  showWarringMsg: boolean = false
  isProced: boolean = false;
  faultDescription: any = [];
  @Input() addressId: any = '';
  @Input() customerId: any;
  @Input() resetId: any
  isCode50Enable: boolean = false
  customerPartitionId: any;
  occurrenceBookPhoneInId: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    public _fb: FormBuilder, private httpCancelService: HttpCancelService, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService,
    private dialog: MatDialog,
    @Optional() public dialogRef: MatDialogRef<AddContactDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
    this.dispatchId = this.activatedRoute.snapshot.queryParams.id;
    this.customerId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.getCustomerPartitionId()
      .subscribe(data => {
        this.customerPartitionId = data;  // Customer profile Only
      });
  }
  ngOnInit() {
    this.getCode50Enable()
    this.createForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getAlarmType();
    this.loadActionTypes();
    this.dispatchForm.get('isBookTechnician').valueChanges.subscribe(val => {
      if (val) {
        this.dispatchForm = setRequiredValidator(this.dispatchForm, ["keyholderId", "contactNo", "faultDescriptionId", "isCode50"]);
      } else {
        this.dispatchForm = clearFormControlValidators(this.dispatchForm, ["keyholderId", "contactNo", "faultDescriptionId", "isCode50"]);
      }
    })

    this.dispatchForm.get('keyholderId').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let contact = this.contactList.find(x => x.keyHolderId == val)
      if (contact) {
        this.dispatchForm.get('contactNo').setValue(contact ? contact?.contactNumber ? contact?.contactNumber : contact?.mobile1 : null)
        this.contactNumberList = []
        if (contact.mobile1) {
          this.contactNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
        }
        if (contact.mobile2) {
          this.contactNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
        }
        if (contact.officeNo) {
          this.contactNumberList.push({ id: contact.officeNo, value: contact.officeNo })
        }
        if (contact.premisesNo) {
          this.contactNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
        }
        if (contact.contactNumber) { //for new contact
          this.contactNumberList.push({ id: contact.contactNumber, value: contact.contactNumber })
        }
        this.dispatchForm.get('contactName').setValue(contact ? contact?.keyHolderName ? contact?.keyHolderName : contact?.customerName : null)
        this.dispatchForm.get('isNewContact').setValue(contact ? contact?.isNewContact : false);
      }

    })
  }

  getCode50Enable() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CODE_50_ENABLE,
      undefined, null, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.customerId, CustomerAddressId: this.addressId }))
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess) {
          this.isCode50Enable = response.resources.isCode50
        }
      })
  }

  loadActionTypes() {
    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        TechnicalMgntModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID, undefined, null, prepareGetRequestHttpParams(null, null,
          { CustomerId: this.customerId })),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL)
    ];
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              resp.resources.forEach(element => {
                element.isNewContact = false
              });
              resp.resources.forEach(element => {
                this.contactList.push(element)
              });
              this.dispatchForm.get('keyholderId').setValue(this.callDetailsData?.keyholderId ? this.callDetailsData?.keyholderId : this.callDetailsData?.callinitiationContactId)
              break;
            case 1:
              this.faultDescription = resp.resources;
              break;
          }

        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createForm(): void {
    let quickAddServiceCallModel = new QuickAddServiceCallInitiationModel();
    this.dispatchForm = this._fb.group({
      comments: this._fb.array([])
    });
    Object.keys(quickAddServiceCallModel).forEach((key) => {
      this.dispatchForm.addControl(key, new FormControl(quickAddServiceCallModel[key]));
    });
    this.dispatchForm = setRequiredValidator(this.dispatchForm, ["alarmTypeId", "priority"]);
    if (this.dispatchForm.get('isBookTechnician').value == true) {
      this.dispatchForm = setRequiredValidator(this.dispatchForm, ["keyholderId", "contactNo", "faultDescriptionId", "isCode50"]);
    }
    this.dispatchForm.get('addressId').setValue(this.addressId);
    this.dispatchForm.get('customerId').setValue(this.customerId);
    this.dispatchForm.get('customerAddressId').setValue(this.addressId);
    this.dispatchForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.dispatchForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }


  getAlarmType() {
    let canPhoneIn = "true";
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARAM_TYPE_CAN_PHONE_IN,
      prepareGetRequestHttpParams(null, null,
        { CanPhoneIn: canPhoneIn })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.alarmTypeList = response.resources;
          }
        });
  }

  createCommentsListModel(quickCommentsListModel?: QuickCommentsListModel): FormGroup {
    let StockIdRequestDescriptionListModelControl = new QuickCommentsListModel(quickCommentsListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionListModelControl).forEach((key) => {
      if (key === 'comments') {
        // [Validators.required]
        formControls[key] = [{ value: StockIdRequestDescriptionListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionListModelControl[key], disabled: false }]
      }
    });
    return this._fb.group(formControls);
  }

  get getCommentsListArray(): FormArray {
    if (!this.dispatchForm) return;
    return this.dispatchForm.get("comments") as FormArray;
  }



  getQuickAdd() {
    if (this.dispatchForm.get("isBookTechnician").value) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_QUICK_ADD,
      undefined, null, prepareGetRequestHttpParams(
        null, null,
        { CustomerId: this.customerId, AddressId: this.addressId }))
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.callDetailsData = response.resources;
        this.showWarringMsg = response.resources.warningMessage ? true : this.callDetailsData.debtorId ? false : true
        this.isProced = this.callDetailsData.debtorId ? true : false
        if (!this.isProced) {
          this.dispatchForm.get('isBookTechnician').setValue(null)
        }
        this.callDetailsData.warningMsg = this.callDetailsData?.warningMessage ? this.callDetailsData?.warningMessage : this.callDetailsData?.debtorId ? null : 'No Debtor created for the customer. Unable to create a service call'
        this.dispatchForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.dispatchForm.get('modifiedUserId').setValue(this.loggedUser.userId)
        this.dispatchForm.patchValue(response.resources)
        this.comments = this.getCommentsListArray;
        if (response.resources.isNewContact) {
          let newcontactData = {
            keyHolderId: response.resources.callinitiationContactId,
            keyHolderName: response.resources.contactName,
            contactNumber: response.resources.contactNoCountryCode + ' ' + response.resources.contactNo,
            isNewContact: true
          }
          this.contactList.push(newcontactData)
          this.dispatchForm.get('keyholderId').setValue(this.callDetailsData?.keyholderId ? this.callDetailsData?.keyholderId : this.callDetailsData?.callinitiationContactId)
        }
        this.dispatchForm.get('contactNo').setValue(response.resources?.contactNo ? response.resources?.contactNoCountryCode + ' ' + response.resources?.contactNo : null)

        if (response.resources.comments.length > 0) {
          response.resources.comments.forEach((stockIdRequestDescriptionListModel: QuickCommentsListModel) => {
            this.comments.push(this.createCommentsListModel(stockIdRequestDescriptionListModel));
          })
          this.comments.push(this.createCommentsListModel());
        }
        else {
          this.comments.push(this.createCommentsListModel());
        }
      })
  }
  openAddContact(val) {
    const dialogRef = this.dialog.open(AddContactDialogComponent, {
      width: '850px',
      data: {
        customerId: this.customerId,
        customerAddressId: this.addressId,
        isProceed: this.callDetailsData.debtorId ? true : false,
        warningMsg: this.callDetailsData?.warningMessage ? this.callDetailsData?.warningMessage : this.callDetailsData?.debtorId ? null : 'No Debtor created for the customer. Unable to create a service call'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contactList.push(result)
      }
    });
  }

  dispachPost() {
    if (this.dispatchForm.invalid) {
      return;
    }
    let formValue = this.dispatchForm.value;
    delete formValue.occurrenceBookPhoneInId;
    formValue.partitionId = this.customerPartitionId ? this.customerPartitionId : null;
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_PHONEIN, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.occurrenceBookPhoneInId = response.resources
      }
    })
  }

  onSubmit(): void {
    if (this.dispatchForm.invalid) {
      return;
    }
    let formValue = this.dispatchForm.value;
    formValue.partitionId = this.customerPartitionId ? this.customerPartitionId : null;
    formValue.occurrenceBookPhoneInId = this.occurrenceBookPhoneInId ? this.occurrenceBookPhoneInId : null;
    if (!formValue.occurrenceBookPhoneInId) {
      delete formValue.occurrenceBookPhoneInId
    }
    if (!formValue.callInitiationId) {
      delete formValue.callInitiationId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (formValue.isBookTechnician) {
      let contactNo = formValue.contactNo.substr(formValue.contactNo.indexOf(' ') + 1)
      let contactNoCountryCode = formValue.contactNo.substr(0, formValue.contactNo.indexOf(' '))
      formValue.contactNoCountryCode = contactNoCountryCode;
      formValue.contactNo = contactNo;
      formValue.contactName = this.contactList?.find(el => el?.keyHolderId == formValue.keyholderId)?.customerName;

      let crudService1 = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_QUICK_ADD, formValue)
      crudService1.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.callInitiationId = response.resources
          formValue.callInitiationId = this.callInitiationId ? this.callInitiationId : null;
          delete formValue.occurrenceBookPhoneInId;
          let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_PHONEIN, formValue)
          crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.dispatchId = response.resources
              this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { tab: 0, childtab: 0, id: response.resources } })
              this.router.navigate(["customer/manage-customers/view/" + [this.customerId]], { queryParams: { addressId: this.addressId } });
            } else {
              this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { tab: 0, childtab: 0, id: response.resources } })
              this.router.navigate(["customer/manage-customers/view/" + [this.customerId]], { queryParams: { addressId: this.addressId } });
            }
          })
        }
      })
    }
    else {
      formValue.callInitiationId = this.callInitiationId ? this.callInitiationId : null;
      if (!formValue.occurrenceBookPhoneInId) {
        delete formValue.occurrenceBookPhoneInId
      }
      if (!formValue.callInitiationId) {
        delete formValue.callInitiationId
      }
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_PHONEIN, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.dispatchId = response.resources
          this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: 0, childtab: 0, id: response.resources } })
        } else {
          this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: 0, childtab: 0, id: response.resources } })
        }
      })
    }
  }
}
