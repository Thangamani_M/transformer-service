
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';

@Component({
  selector: 'app-view-community-dispatch',
  templateUrl: './view-community-dispatch.component.html'
})
export class ViewCommunityDispatchComponent implements OnInit {

  dispatchId: string;
  dispatchDetails: any;
  @Input() customerId: any;
  @Input() addressId: any;
  dispacherViewDetails: any = [];

  constructor(private rxjsService: RxjsService, 
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute) {
      this.dispatchId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    if (this.dispatchId) {
      this.getDispatchDetailsById(this.dispatchId)
    }
  }

  getDispatchDetailsById(dispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_COMMUNITY_PHONEIN, dispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchDetails = response.resources;
          this.onShowValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  navigateTo(){
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  onShowValue(response?: any) {
    this.dispacherViewDetails = [
      { name: 'Alarm Type', value: response?.alarmType },
      { name: 'Priority', value: response?.priority },
      { name: 'Requestor Name', value: response?.requestorName },
      { name: 'Requestor Number', value: response?.requestorContactNumber},
      { name: 'Requestor Location', value: response?.requestorLocationFullAddress },
      { name: 'Location', value: response?.locationFullAddress },
      { name: 'Feedback', value: response?.phoneInFeedbackName },
      { name: 'Feedback', value: response?.notepad },
  
    ]
  }

}