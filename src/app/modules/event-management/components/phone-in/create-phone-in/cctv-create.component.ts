import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-cctv-create',
  templateUrl: './cctv-create.component.html',
  styleUrls: ['./cctv-create.component.scss']
})

export class CctvCreateComponent implements OnInit {

  cctvCreateFrom: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  vehicleModelList: any = []
  vehicleColorList: any = []
  vehicleMakeList: any = []
  incidentList: any;
  raceList: any;
  customerDropDown: any;
  genderList: any;
  @Input() customerId: any;
  @Input() addressId: any;
  @ViewChildren('input1') rows1: QueryList<any>;
  occurrenceBookCommunityCCTVPersonList: FormArray;
  occurrenceBookIncidentTypeList: FormArray;
  cameraProfileId: any;
  constructor(private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getCustomerDropDownById();
    this.getGenderDropDown();
    this.getRaceDropDown();
    this.getIncidentDropDown()
    this.createCctvForm();
    this.getVehicleModel();
    this.getVehicleColor();
    this.getVehicleMake();
    this.cctvCreateFrom.get('cameraProfileId').valueChanges.subscribe(val => {
      if (val) {
        this.getLotitudeAndLongitude(val.id)
      }
    })
  }

  createCctvForm(): void {
    this.cctvCreateFrom = this.formBuilder.group({
      createdUserId: [this.loggedUser.userId],
      locationLatitude: [''],
      locationLongitude: [''],
      cameraProfileId: ['', Validators.required],
      isVehicle: ['true', Validators.required],
      vehicleMakeId: [null],
      vehicleModelId: [null],
      vehicleColorId: [null],
      numberOfOccupants: [''],
      isPerson: ['true', Validators.required],
      numberOfPerson: [''],
      notepad: [''],
      occurrenceBookCommunityCCTVPersonList: this.formBuilder.array([
        this.occurenceFormArray()
      ]),
      occurrenceBookIncidentTypeList: this.formBuilder.array([
        this.incidentFormArray()
      ]),
    });
  }

  // create form array
  get occurrenceBookCommunityCCTVPersonListArray(): FormArray {
    return this.cctvCreateFrom.get("occurrenceBookCommunityCCTVPersonList") as FormArray;
  }

  // create form array+
  get occurrenceBookIncidentTypeListArray(): FormArray {
    return this.cctvCreateFrom.get("occurrenceBookIncidentTypeList") as FormArray;
  }

  focusInAndOutFormArrayFields1(): void {
    this.rows1.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  occurenceFormArray(): FormGroup {
    return this.formBuilder.group({
      genderId: [null],
      raceId: [null]
    })
  }

  incidentFormArray(): FormGroup {
    return this.formBuilder.group({
      incidentTypeId: ['', Validators.required],
    })
  }

  addPersonList() {
    if (this.occurrenceBookCommunityCCTVPersonListArray.invalid) {
      this.focusInAndOutFormArrayFields1();
      return;
    }
    this.occurrenceBookCommunityCCTVPersonListArray.insert(0, this.occurenceFormArray());
  };

  removePerson(index) {
    this.occurrenceBookCommunityCCTVPersonListArray.removeAt(index);
  }

  getCustomerDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CAMERA_PROFILE_SEARCH,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        customerId: this.customerId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.customerDropDown = res.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getLotitudeAndLongitude(cameraProfileId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CAMERA_PROFILE, cameraProfileId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.cctvCreateFrom.get('locationLatitude').setValue(response.resources?.latitude)
          this.cctvCreateFrom.get('locationLongitude').setValue(response.resources?.longitude)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getGenderDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_GENDER, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.genderList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRaceDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_RACE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.raceList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getIncidentDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_EVENT_TYPES_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.incidentList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getVehicleModel() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MODEL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleModelList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getVehicleMake() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MAKE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleMakeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getVehicleColor() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_COLOR, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleColorList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  vehicalChange(dat) {
    if (dat == "no") {
      this.cctvCreateFrom.controls.numberOfOccupants.disable();
      this.cctvCreateFrom.controls.vehicleMakeId.disable();
      this.cctvCreateFrom.controls.vehicleModelId.disable();
      this.cctvCreateFrom.controls.vehicleColorId.disable();
    } else if (dat == 'yes') {
      this.cctvCreateFrom.controls.numberOfOccupants.enable();
      this.cctvCreateFrom.controls.vehicleMakeId.enable();
      this.cctvCreateFrom.controls.vehicleModelId.enable();
      this.cctvCreateFrom.controls.vehicleColorId.enable();
    }
  }
  changePerson(data) {
    if (data == 'no') {
      this.cctvCreateFrom.controls.numberOfPerson.disable();
      (this.cctvCreateFrom.get('occurrenceBookCommunityCCTVPersonList')).disable();

    }
    else if (data == 'yes') {
      this.cctvCreateFrom.controls.numberOfPerson.enable();
      (this.cctvCreateFrom.get('occurrenceBookCommunityCCTVPersonList')).enable();


    }
  }

  onSubmit(): void {
    if (this.cctvCreateFrom.invalid) {
      return;
    }
    let formValue = this.cctvCreateFrom.value;
    formValue.cameraProfileId = formValue.cameraProfileId.id
    formValue.isVehicle = formValue.isVehicle == 'true' ? true : false;
    formValue.isPerson = formValue.isPerson == 'true' ? true : false;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_COMMUNITY_CCTV, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false);
        let forms = this.cctvCreateFrom.get('occurrenceBookCommunityCCTVPersonList') as FormArray;
        forms.clear()
        this.occurrenceBookCommunityCCTVPersonListArray.insert(0, this.occurenceFormArray());
        this.cctvCreateFrom.reset()
      }
    })
  }


}

