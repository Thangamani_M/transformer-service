import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-third-party-create-dispatch',
  templateUrl: './third-party-create-dispatch.component.html'
})
export class ThirdPartyCreateDispatchComponent implements OnInit {

  dispatchForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  alarmTypeList: any = []
  thirdpartyNameList: any = []
  minDate = 0;
  @Input() resetId: any;
  @Input() customerId: any;
  @Input() addressId: any;
  @ViewChild("input", { static: false }) row;
  
  constructor(private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createDispatchForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getThirdPartyNameList();
    this.getAlarmType();
    this.onFormControlChange();
  }

  createDispatchForm(): void {
    this.dispatchForm = this.formBuilder.group({
      thirdPartyId: ['', Validators.required],
      thirdPartyContactNumber: ['', Validators.required],
      contactNumberCountryCode: ['+27'],
      alarmTypeId: ['', Validators.required],
      phoneInTime: ['', Validators.required],
      multipleSignals: ['false', Validators.required],
      controllerName: ['', Validators.required],
      thirdPartyOccurrenceBookNumber: ['', Validators.required],
      estimatedTimeOfArrival: ['', Validators.required],
      notepad: [''],
      createdUserId: [this.loggedUser.userId],
      modifiedUserId: [this.loggedUser.userId]
    });
  }

  onFormControlChange(){
    this.dispatchForm
    .get("contactNumberCountryCode")
    .valueChanges.subscribe((contactNumberCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(contactNumberCountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });

    this.dispatchForm
      .get("thirdPartyContactNumber")
      .valueChanges.subscribe((thirdPartyContactNumber: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dispatchForm.get("contactNumberCountryCode").value
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.dispatchForm
          .get("thirdPartyContactNumber")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.dispatchForm
          .get("thirdPartyContactNumber")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  };

  getThirdPartyNameList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_THIRD_PARTY_DETAILS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.thirdpartyNameList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAlarmType() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.dispatchForm.invalid) {
      return;
    }
    let formValue = this.dispatchForm.value;
    formValue.multipleSignals = formValue.multipleSignals == 'true' ? true : false
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_THIRD_PARTY_MONITORING, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { tab: 2, childtab: 0, id: response.resources } })
      }
    })
  }
}