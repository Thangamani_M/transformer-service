import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-view-client-fire',
  templateUrl: './view-client-fire.component.html'
})
export class ViewClientFireComponent implements OnInit {

    dispatchId: string;
    dispatchDetails: any;
    dispatchForm: FormGroup;
    loggedUser: any;
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    countryCodes = countryCodes;
    @Input() customerId: any;
    @Input() addressId: any;
    fireViewDetails:any = [];

    constructor(private activatedRoute: ActivatedRoute, private router: Router,  private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
      this.dispatchId = this.activatedRoute.snapshot.queryParams.id;
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
    }
  
    ngOnInit() {
      this.createDispatchForm();
      if (this.dispatchId) {
        this.getDispatchDetailsById(this.dispatchId)
      }
    }
  
    createDispatchForm(): void {
      this.dispatchForm = this.formBuilder.group({
        occurrenceBookFireId: [this.dispatchId, Validators.required],
        controllerName: ['', Validators.required],
        controllerOBNumber: ['', Validators.required],
        estimatedTimeOfArrival: ['', Validators.required],
        createdUserId: [this.loggedUser.userId],
        modifiedUserId: [this.loggedUser.userId]
      });
    }

    navigateTo(){
      this.router.navigate(["customer/manage-customers/view/"+[this.customerId]], { queryParams: { addressId: this.addressId}});
    }
  
    getDispatchDetailsById(dispatchId: string) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_FIRE, dispatchId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.dispatchDetails = response.resources;
            this.onShowValue(response.resources)
            this.dispatchForm.get('occurrenceBookFireId').setValue(this.dispatchId)
            this.dispatchForm.get('controllerName').setValue(response.resources.controllerName)
            this.dispatchForm.get('controllerOBNumber').setValue(response.resources.controllerOBNumber)
            this.dispatchForm.get('estimatedTimeOfArrival').setValue(response.resources.estimatedTimeOfArrival)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  
    onSubmit(): void {
      if (this.dispatchForm.invalid) {
        return;
      }
      let formValue = this.dispatchForm.value;
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_FIRE, formValue)
  
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.dispatchId = response.resources
         this.router.navigate(["customer/manage-customers/view/"+[this.customerId]], { queryParams: { addressId: this.addressId}});
        }
      })
    }

    onShowValue(response?: any) {
      this.fireViewDetails = [
        { name: 'Customer Code', value: response?.customerNumber },
        { name: 'OB Number', value: response?.occurrenceBookNumber },
        { name: 'Address', value: response?.fullAddress },
        { name: 'Contact Name', value: response?.keyHolderName },
        { name: 'Contact Number', value: response?.contactNumber },
        { name: 'Severity', value: response?.severityName },
        { name: 'Location of Fire', value: response?.locationOfFire },
        { name: 'Additional Hazards', value: response?.additionalHazards },
        { name: 'Additional Info', value: response?.additionalInformation },
      ]
    }
  
  
  }
  
  
