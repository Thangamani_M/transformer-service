import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-view-dispatch',
  templateUrl: './view-dispatch.component.html'
})
export class ViewDispatchComponent implements OnInit {
  dispatchId: string;
  dispatchDetails: any;
  dispatchForm: FormGroup;
  @Input() customerId: any;
  @Input() addressId: any;
  dispachViewDetails: any = []

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.dispatchId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    if (this.dispatchId) {
      this.getDispatchDetailsById(this.dispatchId)
    }
  }

  getDispatchDetailsById(dispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_PHONEIN, dispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchDetails = response.resources;
          this.onShowValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateTo() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  onSubmit(): void {
    if (this.dispatchForm.invalid) {
      return;
    }
    let formValue = this.dispatchForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_PHONEIN, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: 0, childtab: 0, id: response.resources } })
      }
    })
  }

  onShowValue(response?: any) {
    this.dispachViewDetails = [
      { name: 'Alarm Type', value: response?.alarmType },
      { name: 'Priority', value: response?.priority },
      { name: 'Requestor Name', value: response?.customerName },
      { name: 'Requestor Number', value: response?.customerNumber },
      { name: 'Requestor Location', value: response?.location },
      { name: 'Feedback', value: response?.feedback },
      { name: 'Notepad', value: response?.notepad },
    ]
  }

}
