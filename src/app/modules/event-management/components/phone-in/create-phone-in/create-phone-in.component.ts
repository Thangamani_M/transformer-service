import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

enum CustomerProfielType {
  STANDARD_CUSTOMER = 'Standard Customer',
  REPEAT_SITE = 'Repeater Site',
  EXECUGUARD = 'ExecuGuard',
  TEST_PROFILE = 'Test Profile',
  CASUAL_GUARDING = 'Casual Guarding',
  ADD_SITE = 'Adt Site',
  NKA_CUSTOMER = 'NKA Customers',
  HUB_PROFILE = 'Hub Profile',
  NO_CLIENT = 'No Client',
  WHATSAPP_PROFILE = 'Whatsapp Profile',
  CCTV_CAMERA_PROFILE = 'CCTV-Camera Profile',
  COMMUNITY_PROFILE='Community Profile'
}

@Component({
  selector: 'app-create-phone-in',
  templateUrl: './create-phone-in.component.html',
  styleUrls: ['./create-phone-in.component.scss']
})
export class CreatePhoneInComponent extends PrimeNgTableVariablesModel  implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;

  selectedTabIndex: any = 0;
  selectedChildTabIndex: any = 0
  public bradCrum: MatMenuItem[];
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any
  row: any = {}
  viewId: any
  customerId: any;
  addressId: any;
  customerProfile: any;
  feature: string;
  featureIndex: string;
  clientTab: boolean = false;
  communityTab: boolean = false;
  partyMonitoring: boolean = false;
  cctvTab:boolean = false;
  customerPartitionId: any;
  customerInfoDetails:any;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
      super();
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.feature = this.activatedRoute.snapshot.queryParams.feature_name;
    this.featureIndex = this.activatedRoute.snapshot.queryParams.featureIndex;
    let queryParams = { addressId: this.addressId };
    if (this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }

    this.rxjsService.getCustomerDate()
      .subscribe(data => {
      this.customerInfoDetails = data.contactPerson  // customer Information
      });

    this.primengTableConfigProperties = {
      tableCaption: "Phone In",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: 'customer/manage-customers' }, { displayName: 'Customer View', relativeRouterUrl: 'customer/manage-customers/view/' + this.customerId, queryParams: queryParams }, { displayName: 'Phone In' },{ displayName: 'Phone In' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Client',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stackName', header: 'Stack Name', width: '200px' }, { field: 'divisionNames', header: 'Division', width: '200px' }, { field: 'namedStackConfigName', header: 'Named Stack', width: '200px' }, { field: 'customerTypeNames', header: 'Customer Type', width: '150px' }, { field: 'alarmGroupNames', header: 'Alarm Group', width: '150px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.STACK_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },
          {
            caption: 'Community',
            dataKey: 'stackAreaConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'areaName', header: 'Area Name', width: '200px' }, { field: 'stackAreaConfigDivisions', header: 'Division', width: '200px' }, { field: 'stackAreaConfigMainAreas', header: 'Main Area', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.STACK_AREAS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },

          {
            caption: '3rd Party Monitoring',
            dataKey: 'namedStackConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'namedStackConfigName', header: 'Named Stack Name', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.NAMED_STACK_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },
          {
            caption: 'CCTV',
            dataKey: 'namedStackId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'namedStackConfigName', header: 'Named Stack Mapping Name', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.NAMED_STACK_MAPPING,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },
        ]

      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.selectedChildTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['childtab'] : 0;
      this.viewId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : null;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.customerInfoDetails ? `View Customer: ${this.customerInfoDetails}` : 'View Customer';
    });
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.rxjsService.getCustomerProfile()
      .subscribe(data => {
        this.customerProfile = data;  // Customer profile Only
      });
      // depending on customer Profile type View The Customer
    if (this.customerProfile == CustomerProfielType.STANDARD_CUSTOMER || this.customerProfile == CustomerProfielType.REPEAT_SITE ||
      this.customerProfile == CustomerProfielType.EXECUGUARD || this.customerProfile == CustomerProfielType.TEST_PROFILE ||
      this.customerProfile == CustomerProfielType.CASUAL_GUARDING || this.customerProfile == CustomerProfielType.ADD_SITE
      || this.customerProfile == CustomerProfielType.NKA_CUSTOMER) {
      this.clientTab = true;
    }

    if (this.customerProfile == CustomerProfielType.HUB_PROFILE || this.customerProfile == CustomerProfielType.NO_CLIENT
      || this.customerProfile == CustomerProfielType.WHATSAPP_PROFILE || this.customerProfile == CustomerProfielType.COMMUNITY_PROFILE) {
      this.communityTab = true;
    }

    if (this.customerProfile == CustomerProfielType.STANDARD_CUSTOMER) {
      this.partyMonitoring = true;
    }

    if (this.customerProfile == CustomerProfielType.CCTV_CAMERA_PROFILE) {
      this.cctvTab = true;
    }
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
    }
  }


  onTabChange(event) {
    this.tables.forEach(table => { //to set default row count list
      table.rows = 10
    })

    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: this.selectedTabIndex, childtab: 0 } })
    this.getRequiredListData()

  }

  onChildTabChange(event) {
    this.selectedChildTabIndex = event.index
    this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: this.selectedTabIndex, childtab: this.selectedChildTabIndex } })

  }

}
