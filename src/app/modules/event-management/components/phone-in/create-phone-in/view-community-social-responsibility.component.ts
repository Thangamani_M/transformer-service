import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';

@Component({
  selector: 'app-view-community-social-responsibility',
  templateUrl: './view-community-social-responsibility.component.html'
})
export class ViewCommunitySocialResponsibilityComponent implements OnInit {

  dispatchId: string;
  dispatchDetails: any;
  dispatchForm: FormGroup;
  @Input() customerId: any
  @Input() addressId: any
  socialViewDetails:any = []

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private rxjsService: RxjsService, private crudService: CrudService) {
    this.dispatchId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    if (this.dispatchId) {
      this.getDispatchDetailsById(this.dispatchId)
    }
  }

  getDispatchDetailsById(dispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_SOCIAL_RESPONSIBILITY, dispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchDetails = response.resources;
          this.onShowValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateTo(){
    this.router.navigate(["customer/manage-customers/view/"+[this.customerId]], { queryParams: { addressId: this.addressId}});
  }

  onShowValue(response?: any) {
    this.socialViewDetails = [
      { name: 'Alarm Type', value: response?.alarmType },
      { name: 'Request Type', value: response?.requestorType },
      { name: 'Requestor Name', value: response?.requestorName },
      { name: 'Requestor Number', value: response?.requestorContactNumber },
      { name: 'Requestor Location', value: response?.requestorLocationFullAddress },
      { name: 'Incident Location', value: response?.incidentLocationFullAddress },
      { name: 'Notepad', value: response?.notepad },
    ]
  }

}


