import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-community-social-responsibility',
  templateUrl: './community-social-responsibility.component.html'
})
export class CommunitySocialResponsibilityComponent implements OnInit {
  dispatchForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  alarmTypeList: any = [];
  requestTypeList: any = [];
  @Input() customerId: any;
  @Input() addressId: any;
  requstorAddressList: any = [];
  addressList: any = [];
  @ViewChild("input", { static: false }) row;

  constructor(private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createDispatchForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getAlarmType();
    this.getRequestTypeList();
    this.onFormControlChange();
  }

  createDispatchForm(): void {
    this.dispatchForm = this.formBuilder.group({
      customerAddressId: [this.addressId, Validators.required],
      alarmTypeId: ['', Validators.required],
      requestTypeId: ['', Validators.required],
      requestorName: ['', Validators.required],
      requestorContactNumberCountryCode: ['+27', Validators.required],
      requestorContactNumber: ['', Validators.required],
      requestorLocationBuildingNumber: [''],
      requestorLocationBuildingName: [''],
      requestorLocationStreetTypeId: [''],
      requestorLocationStreetNumber: [''],
      requestorLocationStreetAddress: [''],
      requestorLocationSuburbName: [''],
      requestorLocationCityName: [''],
      requestorLocationProvinceName: [''],
      requestorLocationPostalCode: [''],
      requestorLocationLongitude: [''],
      requestorLocationLatitude: [''],
      requestorLocationFullAddress: ['', Validators.required],
      requestorLocationJSONAddress: [''],
      incidentLocationBuildingNumber: [''],
      incidentLocationBuildingName: [''],
      incidentLocationStreetTypeId: [''],
      incidentLocationStreetNumber: [''],
      incidentLocationStreetAddress: [''],
      incidentLocationSuburbName: [''],
      incidentLocationCityName: [''],
      incidentLocationProvinceName: [''],
      incidentLocationPostalCode: [''],
      incidentLocationLongitude: [''],
      incidentLocationLatitude: [''],
      incidentLocationFullAddress: ['', Validators.required],
      incidentLocationJSONAddress: [''],
      notepad: [''],
      createdUserId: [this.loggedUser.userId],
      modifiedUserId: [this.loggedUser.userId]
    });
  };

  onFormControlChange() {
    this.dispatchForm
      .get("requestorContactNumberCountryCode")
      .valueChanges.subscribe((requestorContactNumberCountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(requestorContactNumberCountryCode);
        setTimeout(() => {
          this.row.nativeElement.focus();
          this.row.nativeElement.blur();
        });
      });

    this.dispatchForm
      .get("requestorContactNumber")
      .valueChanges.subscribe((requestorContactNumber: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dispatchForm.get("requestorContactNumberCountryCode").value
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.dispatchForm
          .get("requestorContactNumber")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.dispatchForm
          .get("requestorContactNumber")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  };

  getAlarmType() {
    let canPhoneIn = "true";
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION,
      prepareGetRequestHttpParams(null, null,
        { CanPhoneIn: canPhoneIn })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.alarmTypeList = response.resources;
          }
        });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getRequestTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_REQUEST_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.requestTypeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSearchAddress(address) {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ADDRESS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        SearchText: address.query,
        IsAfrigisSearch: false,
      }), 1).subscribe((res) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.resources) {
          this.addressList = res.resources;
        }
      })
  };

  onSelectAddress(event) {
    this.dispatchForm.get('incidentLocationLatitude').patchValue(event.latitude)
    this.dispatchForm.get('incidentLocationLongitude').patchValue(event.longitude)
    this.dispatchForm.get('incidentLocationBuildingNumber').patchValue(event.buildingNo)
    this.dispatchForm.get('incidentLocationBuildingName').patchValue(event.buildingName)
    this.dispatchForm.get('incidentLocationStreetTypeId').patchValue(event.addressId)
    this.dispatchForm.get('incidentLocationStreetNumber').patchValue(event.streetNo)
    this.dispatchForm.get('incidentLocationStreetAddress').patchValue(event.streetName)
    this.dispatchForm.get('incidentLocationSuburbName').patchValue(event.suburbName)
    this.dispatchForm.get('incidentLocationCityName').patchValue(event.cityName)
    this.dispatchForm.get('incidentLocationProvinceName').patchValue(event.provinceName)
    this.dispatchForm.get('incidentLocationPostalCode').patchValue(event.postalCode)
  }

  onSearchRequstorAddress(address) {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ADDRESS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        SearchText: address.query,
        IsAfrigisSearch: false,
      }), 1).subscribe((res) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.resources) {
          this.requstorAddressList = res.resources;
        }
      })
  };

  onSelectRequestorAddress(event) {
    this.dispatchForm.get('requestorLocationLatitude').patchValue(event.latitude);
    this.dispatchForm.get('requestorLocationLongitude').patchValue(event.longitude);
    this.dispatchForm.get('requestorLocationBuildingNumber').patchValue(event.buildingNo);
    this.dispatchForm.get('requestorLocationBuildingName').patchValue(event.buildingName);
    this.dispatchForm.get('requestorLocationStreetTypeId').patchValue(event.addressId);
    this.dispatchForm.get('requestorLocationStreetNumber').patchValue(event.streetNo);
    this.dispatchForm.get('requestorLocationStreetAddress').patchValue(event.streetName);
    this.dispatchForm.get('requestorLocationSuburbName').patchValue(event.suburbName);
    this.dispatchForm.get('requestorLocationCityName').patchValue(event.cityName);
    this.dispatchForm.get('requestorLocationProvinceName').patchValue(event.provinceName);
    this.dispatchForm.get('requestorLocationPostalCode').patchValue(event.postalCode);
  };

  onSubmit(): void {
    if (this.dispatchForm.invalid) {
      this.dispatchForm.markAllAsTouched();
      return;
    }
    let formValue = this.dispatchForm.value;
    formValue.incidentLocationFullAddress = formValue.incidentLocationFullAddress.fullAddress
    formValue.requestorLocationFullAddress = formValue.requestorLocationFullAddress.fullAddress
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_SOCIAL_RESPONSIBILITY, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: 1, childtab: 3, id: response.resources } })
      }
    });
  }
}