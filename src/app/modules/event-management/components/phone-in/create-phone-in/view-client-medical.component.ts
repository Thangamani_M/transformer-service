import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-view-client-medical',
  templateUrl: './view-client-medical.component.html'
})
export class ViewClientMedicalComponent implements OnInit {
  dispatchId: string;
  dispatchDetails: any;
  dispatchForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @Input() customerId: any;
  @Input() addressId: any;
  medicaleViewDetails: any = []

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.dispatchId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createDispatchForm();
    if (this.dispatchId) {
      this.getDispatchDetailsById(this.dispatchId)
    }
  }

  createDispatchForm(): void {
    this.dispatchForm = this.formBuilder.group({
      occurrenceBookMedicalId: [this.dispatchId, Validators.required],
      controllerName: ['', Validators.required],
      controllerOBNumber: ['', Validators.required],
      estimatedTimeOfArrival: ['', Validators.required],
      createdUserId: [this.loggedUser.userId],
      modifiedUserId: [this.loggedUser.userId]
    });
  }

  getDispatchDetailsById(dispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEDICAL, dispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchDetails = response.resources;
          this.onShowValue(response.resources)
          this.dispatchForm.get('occurrenceBookMedicalId').setValue(this.dispatchId)
          this.dispatchForm.get('controllerName').setValue(response.resources.controllerName)
          this.dispatchForm.get('controllerOBNumber').setValue(response.resources.controllerOBNumber)
          this.dispatchForm.get('estimatedTimeOfArrival').setValue(response.resources.estimatedTimeOfArrival)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateTo() {
    this.router.navigate(["customer/manage-customers/view/" + [this.customerId]], { queryParams: { addressId: this.addressId } });
  }

  onSubmit(): void {
    if (this.dispatchForm.invalid) {
      return;
    }
    let formValue = this.dispatchForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEDICAL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.dispatchId = response.resources
        this.router.navigate(["customer/manage-customers/view/" + [this.customerId]], { queryParams: { addressId: this.addressId } });
      }
    });
  }

  onShowValue(response?: any) {
    this.medicaleViewDetails = [
      { name: 'Customer Code', value: response?.customerNumber },
      { name: 'OB Number', value: response?.occurrenceBookNumber },
      { name: 'Address', value: response?.fullAddress },
      { name: 'Contact Name', value: response?.keyHolderName },
      { name: 'Contact Number', value: response?.contactNumber },
      { name: 'Number of Injured Persons', value: response?.noOfInjured },
      { name: 'Location of Injured Persons', value: response?.locationOfInjuredPersonsOnProperty },
      { name: 'Age and Sex of Parties', value: response?.ageAndSexOfParties },
      { name: 'Additional Info', value: response?.additionalInformation },
    ]
  }
}