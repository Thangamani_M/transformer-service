import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService ,
  agentLoginDataSelector,
  SnackbarService,
  ResponseMessageTypes,
  ExtensionModalComponent} from '@app/shared';
import { AddContactDialogComponent } from '@modules/customer/components/customer/customer-management/add-contact-dialog/add-contact-dialog.component';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-client-medical',
  templateUrl: './client-medical.component.html'
})
export class ClientMedicalComponent implements OnInit {

  dispatchForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  customerNameList: any = []
  countryCodes = countryCodes;
  @Input() customerId: any
  @Input() addressId: any
  newKeyHolderId: any;
  customerPartitionId: any;
  agentExtensionNo: string;
  CallWorkFlowLists: any;
  @ViewChild("input", { static: false }) row;

  constructor(private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private dialog: MatDialog, 
    private crudService: CrudService, private snakbarService:SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.rxjsService.getCustomerPartitionId()
    .subscribe(data => {
      this.customerPartitionId = data;  // Customer profile Only
    });

    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
  }

  ngOnInit() {
    this.createDispatchForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getCustomerNameList();
    this.getThirdPartyDetails();
    this.onFormControlChange();
  }

  createDispatchForm(): void {
    this.dispatchForm = this.formBuilder.group({
      customerAddressId: [this.addressId, Validators.required],
      keyHolderId: ['', Validators.required],
      contactName: [''],
      contactNumber: ['', Validators.required],
      contactNumberCountryCode: ['+27'],
      noOfInjured: ['', Validators.required],
      locationOfInjuredPersonsOnProperty: [''],
      ageAndSexOfParties: [''],
      additionalInformation: [''],
      createdUserId: [this.loggedUser.userId],
      modifiedUserId: [this.loggedUser.userId],
      customerId: [this.customerId, Validators.required],
      isNewKeyHolder: [false],
      partitionId: [],
      thirdPartyType: ['',],
      officeContactNumberCountryCode: ["+27",],
      officeContactNumber: ['',]
    });

    

  };

  onFormControlChange(){

    this.dispatchForm.get('thirdPartyType').valueChanges.subscribe((val) => {
      if (val) {
        const val1 = this.CallWorkFlowLists.find(el => el?.companyName == val)?.officeContactNumber
        this.dispatchForm.get('officeContactNumber').setValue(val1)
      }
    });

    this.dispatchForm.get('keyHolderId').valueChanges.subscribe(val => {
      if (val) {
        const selectedval = this.customerNameList.find(el => el?.id == val)?.contactNo;
        const selectedval2 = this.customerNameList.find(el => el?.id == val)?.displayName;
        const selectedval1 = this.customerNameList.find(el => el?.id == val)?.contactNoCountryCode;
        this.dispatchForm.get("contactName").setValue(selectedval2)
        if (selectedval1) {
          this.dispatchForm.get("contactNumber").setValue(selectedval)
          this.dispatchForm.get("contactNumberCountryCode").setValue(selectedval1)
        }
        else {
          this.dispatchForm.get("contactNumber").setValue(selectedval)
          this.dispatchForm.get("contactNumberCountryCode").setValue(selectedval1)
        }
      }
    });

    this.dispatchForm.get('keyHolderId').valueChanges.subscribe(val => {
      if (val) {
        const selectedval = this.customerNameList.find(el => el?.id == val)?.contactNo;
        const selectedval2 = this.customerNameList.find(el => el?.id == val)?.displayName;
        const selectedval1 = this.customerNameList.find(el => el?.id == val)?.contactNoCountryCode;
        this.dispatchForm.get("contactName").setValue(selectedval2)
        if (selectedval1) {
          this.dispatchForm.get("contactNumber").setValue(selectedval)
          this.dispatchForm.get("contactNumberCountryCode").setValue(selectedval1)
        }
        else {
          this.dispatchForm.get("contactNumber").setValue(selectedval);
          this.dispatchForm.get("contactNumberCountryCode").setValue(selectedval1);
        }
      }
    });

    this.dispatchForm
    .get("contactNumberCountryCode")
    .valueChanges.subscribe((contactNumberCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(contactNumberCountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });

    this.dispatchForm
      .get("contactNumber")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dispatchForm.get("monitoringCenterNumberCountryCode").value
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.dispatchForm
          .get("contactNumber")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.dispatchForm
          .get("contactNumber")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  };
  

  getCustomerNameList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER,
      prepareGetRequestHttpParams(null, null,
        {
          CustomerId: this.customerId,
          CustomerAddressId: this.addressId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.customerNameList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getCustomerNumber(keyholderId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER_ID, keyholderId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchForm.get('contactNumber').setValue(response.resources.contactNo)
          this.dispatchForm.get('contactNumberCountryCode').setValue(response.resources.contactNoCountryCode)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openAddContact(val) {
    const contactData = {
      customerId: this.customerId,
      customerAddressId: this.addressId
    };
    const dialogRef = this.dialog.open(AddContactDialogComponent, {
      width: '850px',
      data: contactData
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.newKeyHolderId = result.keyHolderId
        const contNo = result?.contactNumber.slice(4, 13)
        this.customerNameList.push({ id: result.keyHolderId, displayName: result.keyHolderName, contactNo: contNo });
      }
    });
  }

  onSubmit(): void {
    if (this.dispatchForm.invalid) {
      return;
    }
    let formValue = this.dispatchForm.value;
    delete formValue['thirdPartyType']
    delete formValue['officeContactNumberCountryCode']
    delete formValue['officeContactNumber']
    formValue.partitionId = this.customerPartitionId?this.customerPartitionId: null;
    formValue.contactNumber = formValue.contactNumber.split(' ').join('')
    if (formValue.keyHolderId == this.newKeyHolderId) {
      formValue.keyHolderId = null
    }
    if (formValue.contactNumber) {
      formValue.contactNumber = formValue.contactNumber.toString().replace(/\s/g, "");
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEDICAL_PHONEIN, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: 0, childtab: 2, id: response.resources } })
      }
    })
  }

  callDail(e, e1): void {  
    let countryCode = e.value
    let mobileNumber = e1.value
    let number = mobileNumber.split(' ').join('')
    let number1 = countryCode + number
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snakbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else {
      let data = {
        customerContactNumber: number1,
        customerId: this.customerId,
        clientName: number1,
        siteAddressId: this.addressId
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  getThirdPartyDetails() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_THIRD_PARTY_DETAILS_TYPE_CUSTOMER_ADDRESS,
      undefined, null, prepareGetRequestHttpParams(null, null,
        { TypeId: 2,  CustomerId: this.customerId,CustomerAddressId:this.addressId }))
      .subscribe(response => {
        this.CallWorkFlowLists = response.resources
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

}
