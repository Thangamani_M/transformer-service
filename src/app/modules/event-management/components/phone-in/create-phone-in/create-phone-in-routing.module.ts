import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePhoneInComponent } from './create-phone-in.component';
const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CreatePhoneInComponent, data: { title: 'Create Phone In' } }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CreatePhoneInRoutingModule { }
