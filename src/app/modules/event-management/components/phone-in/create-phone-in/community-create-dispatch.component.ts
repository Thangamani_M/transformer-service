import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-community-create-dispatch',
  templateUrl: './community-create-dispatch.component.html',
  styleUrls: ['./create-dispatch.component.scss']
})
export class CommunityCreateDispatchComponent implements OnInit {

  dispatchForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  alarmTypeList: any = []
  priorityList: any = [
    { displayName: '1' },
    { displayName: '2' },
    { displayName: '3' },
    { displayName: '4' },
    { displayName: '5' },
    { displayName: '6' },
    { displayName: '7' },
    { displayName: '8' },
    { displayName: '9' },
    { displayName: '10' },
  ]
  feedbackList: any = []
  @Input() customerId: any
  @Input() addressId: any
  requstorAddressList: any = []
  addressList: any = []
  vehicleColorList: any;
  vehicleMakeList: any;
  raceList: any;
  genderList: any;
  vehicleModelList: any;
  @ViewChild("input", { static: false }) row;

  constructor(private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.getGenderDropDown();
    this.getRaceDropDown();
    this.getVehicleColor();
    this.getVehicleMake();
    this.getVehicleModel();
    this.createDispatchForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getAlarmType();
    this.getFeedbackList();
    this.onFormControlChange();
  }

  createDispatchForm(): void {
    this.dispatchForm = this.formBuilder.group({
      createdUserId: [this.loggedUser.userId],
      alarmTypeId: ['', Validators.required],
      priority: ['', Validators.required],
      requestorName: ['', Validators.required],
      requestorContactNumberCountryCode: ['+27', Validators.required],
      requestorContactNumber: ['', Validators.required],
      requestorLocationBuildingNumber: [''],
      requestorLocationBuildingName: [''],
      requestorLocationStreetTypeId: [''],
      requestorLocationStreetNumber: [''],
      requestorLocationStreetAddress: [''],
      requestorLocationSuburbName: [''],
      requestorLocationCityName: [''],
      requestorLocationProvinceName: [''],
      requestorLocationPostalCode: [''],
      requestorLocationLongitude: [''],
      requestorLocationLatitude: [''],
      requestorLocationFullAddress: [''],
      requestorLocationJSONAddress: [''],
      locationBuildingNumber: [''],
      locationBuildingName: [''],
      locationStreetTypeId: [''],
      locationStreetNumber: [''],
      locationStreetAddress: [''],
      locationSuburbName: [''],
      locationCityName: [''],
      locationProvinceName: [''],
      locationPostalCode: [''],
      locationLongitude: [''],
      locationLatitude: [''],
      locationFullAddress: ['', Validators.required],
      locationJSONAddress: [''],
      feedbackId: ['', Validators.required],
      notepad: [''],
      isVehicle: ['true', Validators.required],
      isPerson: ['true', Validators.required],
      vehicleMakeId: [null],
      vehicleModelId: [null],
      vehicleColorId: [null],
      numberOfOccupants: [''],
      numberOfPerson: [''],
      customerid:[this.customerId],
      customerAddressId:[this.addressId],
      occurrenceBookCommunityPhoneInPersonList: this.formBuilder.array([
        this.occurenceFormArray()
      ]),
    });
  }

  onFormControlChange(){
    this.dispatchForm
    .get("requestorContactNumberCountryCode")
    .valueChanges.subscribe((requestorContactNumberCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(requestorContactNumberCountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });

    this.dispatchForm
      .get("requestorContactNumber")
      .valueChanges.subscribe((requestorContactNumber: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dispatchForm.get("requestorContactNumberCountryCode").value
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.dispatchForm
          .get("requestorContactNumber")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.dispatchForm
          .get("requestorContactNumber")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  };

  get occurrenceBookCommunityCCTVPersonList(): FormArray {
    return this.dispatchForm.get("occurrenceBookCommunityPhoneInPersonList") as FormArray;
  }

  occurenceFormArray(): FormGroup {
    return this.formBuilder.group({
      genderId: [null],
      raceId: [null]
    })
  }

  getGenderDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_GENDER, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.genderList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRaceDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_RACE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.raceList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getVehicleModel() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MODEL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleModelList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getVehicleMake() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MAKE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleMakeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getVehicleColor() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_COLOR, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleColorList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAlarmType() {
    let canPhoneIn = "true";
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION,
      prepareGetRequestHttpParams(null, null,
        { CanPhoneIn: canPhoneIn })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.alarmTypeList = response.resources;
          }
        });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getFeedbackList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_PHONEIN_FEEDBACK, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.feedbackList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSearchAddress(address) {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ADDRESS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        SearchText: address.query,
        IsAfrigisSearch: false,
      }), 1).subscribe((res) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.resources) {
          this.addressList = res.resources;
        }
      })
  }

  onSelectAddress(event) {
    this.dispatchForm.get('locationLatitude').patchValue(event.latitude)
    this.dispatchForm.get('locationLongitude').patchValue(event.longitude)
    this.dispatchForm.get('locationBuildingNumber').patchValue(event.buildingNo)
    this.dispatchForm.get('locationBuildingName').patchValue(event.buildingName)
    this.dispatchForm.get('locationStreetTypeId').patchValue(event.addressId)
    this.dispatchForm.get('locationStreetNumber').patchValue(event.streetNo)
    this.dispatchForm.get('locationStreetAddress').patchValue(event.streetName)
    this.dispatchForm.get('locationSuburbName').patchValue(event.suburbName)
    this.dispatchForm.get('locationCityName').patchValue(event.cityName)
    this.dispatchForm.get('locationProvinceName').patchValue(event.provinceName)
    this.dispatchForm.get('locationPostalCode').patchValue(event.postalCode)
  }

  vehicalChange(dat) {
    if (dat == "no") {
      this.dispatchForm.controls.numberOfOccupants.disable();
      this.dispatchForm.controls.vehicleMakeId.disable();
      this.dispatchForm.controls.vehicleModelId.disable();
      this.dispatchForm.controls.vehicleColorId.disable();
    } else if (dat == 'yes') {
      this.dispatchForm.controls.numberOfOccupants.enable();
      this.dispatchForm.controls.vehicleMakeId.enable();
      this.dispatchForm.controls.vehicleModelId.enable();
      this.dispatchForm.controls.vehicleColorId.enable();
    }
  }
  changePerson(data) {
    if (data == 'no') {
      this.dispatchForm.controls.numberOfPerson.disable();
      (this.dispatchForm.get('occurrenceBookCommunityPhoneInPersonList')).disable();
    }
    else if (data == 'yes') {
      this.dispatchForm.controls.numberOfPerson.enable();
      (this.dispatchForm.get('occurrenceBookCommunityPhoneInPersonList')).enable();

    }
  }

  ///add person
  addPersonList() {
    this.occurrenceBookCommunityCCTVPersonList.insert(0, this.occurenceFormArray());
  };

  removePerson(index) {
    this.occurrenceBookCommunityCCTVPersonList.removeAt(index);
  }

  onSearchRequstorAddress(address) {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ADDRESS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        SearchText: address.query,
        IsAfrigisSearch: false,
      }), 1).subscribe((res) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        if (res.resources) {
          this.requstorAddressList = res.resources;
        }
      })
  }

  onSelectRequestorAddress(event) {
    this.dispatchForm.get('requestorLocationLatitude').patchValue(event.latitude)
    this.dispatchForm.get('requestorLocationLongitude').patchValue(event.longitude)
    this.dispatchForm.get('requestorLocationBuildingNumber').patchValue(event.buildingNo)
    this.dispatchForm.get('requestorLocationBuildingName').patchValue(event.buildingName)
    this.dispatchForm.get('requestorLocationStreetTypeId').patchValue(event.addressId)
    this.dispatchForm.get('requestorLocationStreetNumber').patchValue(event.streetNo)
    this.dispatchForm.get('requestorLocationStreetAddress').patchValue(event.streetName)
    this.dispatchForm.get('requestorLocationSuburbName').patchValue(event.suburbName)
    this.dispatchForm.get('requestorLocationCityName').patchValue(event.cityName)
    this.dispatchForm.get('requestorLocationProvinceName').patchValue(event.provinceName)
    this.dispatchForm.get('requestorLocationPostalCode').patchValue(event.postalCode)
  }

  onSubmit(): void {
    this.dispatchForm.get('locationFullAddress').markAllAsTouched();
    if (this.dispatchForm.invalid) {
      return;
    }
    let formValue = this.dispatchForm.value;
    formValue.vehicleMakeId = formValue?.vehicleMakeId?.id?formValue.vehicleMakeId?.id:null;
    formValue.vehicleModelId = formValue?.vehicleModelId?.id?formValue.vehicleModelId?.id:null;
    formValue.vehicleColorId = formValue?.vehicleColorId?.id?formValue.vehicleColorId?.id:null;
    formValue.requestorContactNumber = formValue?.requestorContactNumber.split(' ').join('')
    formValue.occurrenceBookCommunityPhoneInPersonList?.forEach(val => {
      if(val.genderId == null && val.raceId == null){
        delete formValue.occurrenceBookCommunityPhoneInPersonList[0]
       delete formValue.occurrenceBookCommunityPhoneInPersonList
       formValue.occurrenceBookCommunityPhoneInPersonList = null
      }

  })
    formValue.isVehicle = formValue.isVehicle == 'true' ? true : false;
    formValue.isPerson = formValue.isPerson == 'true' ? true : false;
    formValue.locationFullAddress = formValue.locationFullAddress.fullAddress
    formValue.requestorLocationFullAddress = formValue.requestorLocationFullAddress.fullAddress
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_COMMUNITY_PHONEIN, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: 1, childtab: 0, id: response.resources } })
      }
    })
  }
}