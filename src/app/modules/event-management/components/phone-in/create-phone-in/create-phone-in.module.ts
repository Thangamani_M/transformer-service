import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AddContactDialogModule } from '@modules/customer/components/customer/customer-management/add-contact-dialog/add-contact-dialog.module';
import { CctvCreateComponent } from './cctv-create.component';
import { ClientFireComponent } from './client-fire.component';
import { ClientMedicalComponent } from './client-medical.component';
import { CommunityCreateDispatchComponent } from './community-create-dispatch.component';
import { CommunityFireComponent } from './community-fire.component';
import { CommunityMedicalComponent } from './community-medical.component';
import { CommunitySocialResponsibilityComponent } from './community-social-responsibility.component';
import { CreateDispatchComponent } from './create-dispatch.component';
import { CreatePhoneInRoutingModule } from './create-phone-in-routing.module';
import { CreatePhoneInComponent } from './create-phone-in.component';
import { ThirdPartyCreateDispatchComponent } from './third-party-create-dispatch.component';
import { ThirdPartyViewDispatchComponent } from './third-party-view-dispatch.component';
import { ViewClientFireComponent } from './view-client-fire.component';
import { ViewClientMedicalComponent } from './view-client-medical.component';
import { ViewCommunityDispatchComponent } from './view-community-dispatch.component';
import { ViewCommunityFireComponent } from './view-community-fire.component';
import { ViewCommunityMedicalComponent } from './view-community-medical.component';
import { ViewCommunitySocialResponsibilityComponent } from './view-community-social-responsibility.component';
import { ViewDispatchComponent } from './view-dispatch.component';



@NgModule({
  declarations: [CreatePhoneInComponent, CreateDispatchComponent, ClientFireComponent, ClientMedicalComponent, ViewDispatchComponent, ViewClientMedicalComponent, ViewClientFireComponent, ThirdPartyCreateDispatchComponent, ThirdPartyViewDispatchComponent, CommunityCreateDispatchComponent, CommunityFireComponent, CommunityMedicalComponent, ViewCommunityMedicalComponent, ViewCommunityFireComponent, ViewCommunityDispatchComponent, CommunitySocialResponsibilityComponent, ViewCommunitySocialResponsibilityComponent, CctvCreateComponent],
  imports: [
    CommonModule,
    CreatePhoneInRoutingModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    AddContactDialogModule
  ]
})
export class CreatePhoneInModule { }
