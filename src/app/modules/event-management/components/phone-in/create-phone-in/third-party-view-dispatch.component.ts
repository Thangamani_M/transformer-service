import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';

@Component({
  selector: 'app-third-party-view-dispatch',
  templateUrl: './third-party-view-dispatch.component.html'
})
export class ThirdPartyViewDispatchComponent implements OnInit {

  dispatchId: string;
  dispatchDetails: any;
  dispatchForm: FormGroup;
  thirdPartyDispachViewDetails: any = []

  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.dispatchId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    this.createDispatchForm();
    if (this.dispatchId) {
      this.getDispatchDetailsById(this.dispatchId)
    }
  }

  createDispatchForm(): void {
  }

  getDispatchDetailsById(dispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_THIRD_PARTY_MONITORING, dispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchDetails = response.resources;
          this.onShowValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.thirdPartyDispachViewDetails = [
      { name: 'Customer Code', value: response?.customerNumber },
      { name: 'OB Number', value: response?.thirdPartyOccurrenceBookNumber },
      { name: 'Address', value: response?.fullAddress },
      { name: 'Contact Name', value: response?.keyHolderName },
      { name: 'Contact Number', value: response?.thirdPartyContactNumber },
      { name: '3rd Party Name', value: response?.thirdPartyName },
      { name: 'Alarm Type', value: response?.alarmType },
      { name: 'Time', value: response?.phoneInTime },
      { name: 'Multiple Signals', value: response?.multipleSignals },
      { name: 'Notepad', value: response?.notepad },
    ]
  }

  onSubmit(): void {
  }
}