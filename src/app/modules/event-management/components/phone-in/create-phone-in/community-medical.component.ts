import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-community-medical',
  templateUrl: './community-medical.component.html'
})
export class CommunityMedicalComponent implements OnInit {

  dispatchForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  customerNameList: any = []
  addressList: any = []
  countryCodes = countryCodes;
  @Input() customerId: any;
  @Input() addressId: any;
  @ViewChild("input", { static: false }) row;

  constructor(private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createDispatchForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getCustomerNameList();
    this.onFormControlChange();
  }

  createDispatchForm(): void {
    this.dispatchForm = this.formBuilder.group({
      customerAddressId: [this.addressId, Validators.required],
      keyHolderId: ['', Validators.required],
      contactName: [''],
      contactNumber: ['', Validators.required],
      contactNumberCountryCode: ['+27'],
      noOfInjured: ['', [Validators.required,Validators.min(1),Validators.max(9999)]],
      locationBuildingNumber: [''],
      locationBuildingName: [''],
      locationStreetTypeId: [''],
      locationStreetNumber: [''],
      locationStreetAddress: [''],
      locationSuburbName: [''],
      locationCityName: [''],
      locationProvinceName: [''],
      locationPostalCode: [''],
      locationLongitude: [''],
      locationLatitude: [''],
      locationFullAddress: ['', Validators.required],
      locationJSONAddress: [''],
      locationOfInjuredPersonsOnProperty: [''],
      ageAndSexOfParties: [''],
      additionalInformation: [''],
      createdUserId: [this.loggedUser.userId],
      modifiedUserId: [this.loggedUser.userId],
      isNewKeyHolder: [false]
    });
  };

  onFormControlChange(){
    this.dispatchForm
    .get("contactNumberCountryCode")
    .valueChanges.subscribe((contactNumberCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(contactNumberCountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });

    this.dispatchForm
      .get("contactNumber")
      .valueChanges.subscribe((contactNumber: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.dispatchForm.get("contactNumberCountryCode").value
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.dispatchForm
          .get("contactNumber")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.dispatchForm
          .get("contactNumber")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  };


  getCustomerNameList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER,
      prepareGetRequestHttpParams(null, null,
        {
          CustomerId: this.customerId,
          CustomerAddressId: this.addressId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.customerNameList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getCustomerNumber(keyholderId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER_ID, keyholderId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchForm.get('contactNumber').setValue(response.resources.contactNo)
          this.dispatchForm.get('contactNumberCountryCode').setValue(response.resources.contactNoCountryCode)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSearchAddress(address) {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ADDRESS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        SearchText: address.query,
        IsAfrigisSearch: false,
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.addressList = res.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSelectAddress(event) {
    this.dispatchForm.get('locationLatitude').patchValue(event.latitude)
    this.dispatchForm.get('locationLongitude').patchValue(event.longitude)
    this.dispatchForm.get('locationBuildingNumber').patchValue(event.buildingNo)
    this.dispatchForm.get('locationBuildingName').patchValue(event.buildingName)
    this.dispatchForm.get('locationStreetTypeId').patchValue(event.addressId)
    this.dispatchForm.get('locationStreetNumber').patchValue(event.streetNo)
    this.dispatchForm.get('locationStreetAddress').patchValue(event.streetName)
    this.dispatchForm.get('locationSuburbName').patchValue(event.suburbName)
    this.dispatchForm.get('locationCityName').patchValue(event.cityName)
    this.dispatchForm.get('locationProvinceName').patchValue(event.provinceName)
    this.dispatchForm.get('locationPostalCode').patchValue(event.postalCode)
  }

  onSubmit(): void {
    if (this.dispatchForm.invalid) {
      this.dispatchForm.markAllAsTouched();
      return;
    }
    let formValue = this.dispatchForm.value;
    formValue.locationFullAddress = formValue.locationFullAddress.fullAddress
    if (!formValue.isNewKeyHolder) {
      var contact = this.customerNameList.find(x => x.id == formValue.keyHolderId.id)
    }
    if (typeof formValue.keyHolderId == 'object') {
      formValue.isNewKeyHolder = false
      formValue.contactName = formValue.isNewKeyHolder ? formValue.keyHolderId.id : contact.displayName;
      formValue.keyHolderId = formValue.isNewKeyHolder ? null : formValue.keyHolderId.id;
    }
    else {
      formValue.isNewKeyHolder = true
      formValue.contactName = formValue.keyHolderId
      formValue.keyHolderId = formValue.isNewKeyHolder ? null : formValue.keyHolderId.id;
    }
    formValue.customerId = this.customerId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_COMMUNITY_MEDICAL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/event-management/create-phone-in/add-edit'], { queryParams: { customerId: this.customerId, addressId: this.addressId, tab: 1, childtab: 2, id: response.resources } })
      }
    });
  }
}