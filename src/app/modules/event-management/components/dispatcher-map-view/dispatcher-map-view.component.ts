import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { AppState } from '@app/reducers';
import {
  clearFormControlValidators,
  CrudService, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, defaultLeafletLatitude, defaultLeafletLongitude, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SignalrConnectionService, SignalRTriggers, SnackbarService
} from '@app/shared';
import { HubConnection } from '@aspnet/signalr';
import { DispatcherSearchByDetails, EventMgntModuleApiSuffixModels, VehicleStatus } from '@modules/event-management/shared';
import { EVENT_MANAGEMENT_COMPONENT, MAPVIEW_ACTION } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { DispatcherSiteStatuses } from '@modules/user';
import { Store } from '@ngrx/store';
import "leaflet";
import 'leaflet-draw';
import { combineLatest, forkJoin, Observable, of, Subscription, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, scan, take, takeWhile } from 'rxjs/operators';

declare let $;
declare let L;
// add in this const if any new modal is introduced in this component
const LIST_OF_BOOTSTRAP_MODAL_TEMPLATE_REFERENCE = ['#dispatch_unallocated_vehicle', '#dispatch_allocated_vehicle', '#search_free_text_modal',
  '#search_dispatcher_area_modal', '#search_layers_modal', '#selected_vehicle_summary_modal', '#dispatch_allocated_vehicle_to_address',
  '#response_officer_chat_modal', '#selected_customer_alarm_summary_modal', '#dispatch_vehicle_from_customer_alarm_address_modal']
export interface IpolyLayersObject {
  isUpdatedCustomerAddress: boolean,
  isUpdatedLayers: boolean,
  isUpdatedReceivingUsers: boolean,
  isUpdatedResponseOfficers: boolean,
  isUpdatedRoots: boolean,
  receivingUserId: any[],
  features: any[],
  polyLayers: any[],
  isShowLayerLabelName: boolean
}
@Component({
  selector: 'app-dispatcher-map-view',
  templateUrl: './dispatcher-map-view.component.html',
  styleUrls: ['./dispatcher-map-view.component.scss']
})
export class DispatcherMapViewComponent implements OnInit {
  polyLayersObject: IpolyLayersObject;
  geoJsonData: IpolyLayersObject;
  boundaryRequestId = "";
  loggedInUserData: LoggedInUserModel;
  selectedVehicleObject: any;
  modalTitle = 'Cancel Dispatch to address options';
  actionBtnName = 'Reassign';
  selectedAllocatedVehicleIndex = 0;
  selectedUnAllocatedVehicleIndex = 0;
  declineReason = "";
  customerDistance;
  distances = [];
  fromUrl = "";
  isDisabled: boolean;
  isNotificationBtnClicked = false;
  legendItems = {};
  PREFIX_IMAGE_RELATIVE_PATH = '../../../../assets/img/dispatcher-map-images/';
  shouldShowLegend = false;
  vehicleStatus = VehicleStatus;
  isFormSubmitted = false;
  actionIconsForVehicle = [];
  actionIconsForCustomer = [];
  occurrenceBookId = '';
  responseOfficerId = '';
  allROList = [];
  searchByDetailModel;
  searchDetailsForm: FormGroup;
  selectedOption;
  zoomInOptions = { latitude: defaultLeafletLatitude, longitude: defaultLeafletLongitude };
  zoomInOptionsForDispatcherArea = { southWestLatitude: 0, southWestLongitude: 0, northEastLatitude: 0, northEastLongitude: 0 };
  dispatcherAddresses = [];
  mapLayerTypes = [];
  monitoringDataUser: any;
  patrolId = '';
  showLayerLabelName = false;
  formConfigs = formConfigs;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  @ViewChild('dispatch_unallocated_vehicle', { static: false }) dispatch_unallocated_vehicle: ElementRef<any>;
  @ViewChild('dispatch_allocated_vehicle', { static: false }) dispatch_allocated_vehicle: ElementRef<any>;
  @ViewChild('search_free_text_modal', { static: false }) search_free_text_modal: ElementRef<any>;
  @ViewChild('search_dispatcher_area_modal', { static: false }) search_dispatcher_area_modal: ElementRef<any>;
  @ViewChild('search_layers_modal', { static: false }) search_layers_modal: ElementRef<any>;
  @ViewChild('selected_vehicle_summary_modal', { static: false }) selected_vehicle_summary_modal: ElementRef<any>;
  @ViewChild('dispatch_allocated_vehicle_to_address', { static: false }) dispatch_allocated_vehicle_to_address: ElementRef<any>;
  @ViewChild('response_officer_chat_modal', { static: false }) response_officer_chat_modal: ElementRef<any>;
  @ViewChild('selected_customer_alarm_summary_modal', { static: false }) selected_customer_alarm_summary_modal: ElementRef<any>;
  @ViewChild('dispatch_vehicle_from_customer_alarm_address_modal', { static: false }) dispatch_vehicle_from_customer_alarm_address_modal: ElementRef<any>;
  primengTableConfigProperties: any;
  setInterval;
  allPermissions = [];
  servicesPermissions = [];
  timer$: Subscription;

  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private crudService: CrudService, private formBuilder: FormBuilder, private signalrConnectionService: SignalrConnectionService,
    private httpCancelService: HttpCancelService, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: '',
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.allPermissions = response[1][EVENT_MANAGEMENT_COMPONENT.DISPATCHER_MAP_VIEW]

      });
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.getDispatcherAddresses();
    this.combineLatestNgrxStoreData();
    this.getLegendItems();
    this.hubConnectionForSalesAPIConfigs();
    this.onBootstrapModalEventChanges();
    this.customerDistance = "";
    setTimeout(() => {
      this.actionOnBootstrapModal('show', 3);
    });
    this.getMapMonitoringDataByUser();
    this.createSearchDetailsForm();
    this.onFormatedAddressFormControlValueChanges();
    // as long as staying in the dispatcher component this request should be called every 10 seconds
    this.setInterval = setInterval(() => {
      if (this.monitoringDataUser.stackAreaConfigId && this.loggedInUserData.userId) {
        this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAP_MONITORING_CUSTOM, {
          stackAreaConfigId: this.monitoringDataUser.stackAreaConfigId,
          userId: this.loggedInUserData.userId
        })
          .subscribe();
      }
    }, 10000); // 10 seconds
  }

  afterDataRenderedInMapAction(event) {
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getForkJoinRequests(type): void {
    forkJoin([
      this.getDispatcherLiveDataByUserId(type)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                respObj.resources.showLayerLabelName = false;
                this.geoJsonData = respObj.resources;
                this.timer$?.unsubscribe();
                this.refreshDispatcherLiveDataEveryFiveMinutes();
                if (respObj.resources?.features.length == 0) {
                  this.rxjsService.setGlobalLoaderProperty(false);
                }
                this.preparePolyLayersForDifferentShapes();
                break;
            }
          }
        });
      });
  }

  refreshDispatcherLiveDataEveryFiveMinutes() {
    this.timer$ = timer(0, 1000).pipe(scan(acc => --acc, 300),
      takeWhile(pre => pre >= 0))
      .subscribe((timeLeftInSeconds: number) => {
        if (timeLeftInSeconds == 0) {
          this.getForkJoinRequests('initial with 5 minutes auto refresh');
          this.timer$.unsubscribe();
        }
      });
  }

  preparePolyLayersForDifferentShapes() {
    if (this.geoJsonData?.['features'].length > 0) {
      let data = this.prepareCoordinatesForLeafLetMapComponent();
      let features = [];
      data.forEach((feature) => {
        let shapeType = feature.properties.shapeType;
        if (shapeType == 'Polygon') {
          // for single polygon
          if (feature.coordinates.length == 1) {
            features.push(L.polygon(feature.coordinates, feature.properties));
          }
          else {
            // for multipolygon
            feature.coordinates.forEach((coordinate) => {
              features.push(L.polygon(coordinate, feature.properties));
            });
          }
        }
        else if (shapeType == 'Point' && feature.coordinates[0]) {
          features.push(L.marker(feature.coordinates, feature.properties));
        }
        else if (shapeType == 'LineString') {
          features.push(L.polyline(feature.coordinates, feature.properties));
        }
      });
      this.geoJsonData.polyLayers = features;
    }
    this.polyLayersObject = this.geoJsonData;
  }

  getLegendItems() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCHER_MAP_LEGEND)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.legendItems = response.resources;
        }
      });
  }

  getDispatcherAddresses() {
    if (this.dispatcherAddresses.length > 0) {
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_STACK_AREA_CONFIG)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.dispatcherAddresses = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  getMapViewLayerTypes() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_MAP_VIEW_LAYER_TYPES)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.mapLayerTypes = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  prepareCoordinatesForLeafLetMapComponent() {
    let data = [];
    let features = this.geoJsonData['features'];
    features.forEach((feature) => {
      let coordinates;
      if (feature.geometry.type == 'Polygon') {
        if (typeof feature.geometry.coordinates === 'string') {
          let coordinatesStr = feature.geometry.coordinates;
          coordinatesStr = coordinatesStr.replace(/^"(.*)"$/, '$1');
          coordinatesStr = coordinatesStr.replace(/[()]/g, '')
          coordinates = JSON.parse(coordinatesStr);
        }
        else {
          coordinates = feature.geometry.coordinates;
        }
        if (coordinates.length > 1) {
          coordinates.forEach((innerCoordinates) => {
            innerCoordinates.forEach((latLong, index) => {
              latLong.forEach((nestedLeatLngArr) => {
                let nestedLeatLngArrCopy = JSON.parse(JSON.stringify(nestedLeatLngArr));
                if (Math.sign(nestedLeatLngArr[0]) == 1) {
                  nestedLeatLngArr[0] = nestedLeatLngArrCopy[1];
                  nestedLeatLngArr[1] = nestedLeatLngArrCopy[0];
                }
              });
            });
          })
        }
        else {
          coordinates[0].forEach((latLong, index) => {
            let latLongArr = JSON.parse(JSON.stringify(latLong));
            if (Math.sign(latLongArr[0]) == 1) {
              latLong[0] = latLongArr[1];
              latLong[1] = latLongArr[0];
            }
          });
        }
        feature.properties.shapeType = feature.geometry.type;
        let obj = { coordinates, properties: feature.properties };
        data.push(obj);
      }
      else if (feature.geometry.type == 'Point') {
        let nestedLeatLngArrCopy = JSON.parse(JSON.stringify(feature.geometry.coordinates));
        if (Math.sign(feature.geometry.coordinates[0]) == 1) {
          feature.geometry.coordinates[0] = nestedLeatLngArrCopy[1];
          feature.geometry.coordinates[1] = nestedLeatLngArrCopy[0];
        }
        feature.properties.shapeType = feature.geometry.type;
        feature.properties.coordinates = feature.geometry.coordinates;
        let obj = { coordinates: feature.geometry.coordinates, properties: feature.properties };
        data.push(obj);
      }
      else if (feature.geometry.type == 'LineString') {
        feature.geometry.coordinates.forEach((innerLatLong, index) => {
          let latLongArr = JSON.parse(JSON.stringify(innerLatLong));
          if (Math.sign(latLongArr[0]) == 1) {
            innerLatLong[0] = latLongArr[1];
            innerLatLong[1] = latLongArr[0];
          }
        });
        feature.properties.shapeType = feature.geometry.type;
        feature.properties.coordinates = feature.geometry.coordinates;
        let obj = { coordinates: feature.geometry.coordinates, properties: feature.properties };
        data.push(obj);
      }
    });
    return data;
  }

  createSearchDetailsForm(): void {
    let dispatcherSearchByDetailsModel = new DispatcherSearchByDetails();
    this.searchDetailsForm = this.formBuilder.group({});
    Object.keys(dispatcherSearchByDetailsModel).forEach((key) => {
      this.searchDetailsForm.addControl(key, new FormControl(dispatcherSearchByDetailsModel[key]));
    });
    this.searchDetailsForm = setRequiredValidator(this.searchDetailsForm, ["formatedAddress"]);
  }

  filterAddressOrVehicleByKeywordSearch(searchText: string): Observable<IApplicationResponse> {
    if (this.searchByDetailModel == 'Address') {
      if (searchText.length < 3) {
        return of();
      }
      return this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS,
        null,
        true,
        prepareRequiredHttpParams({
          searchText: searchText.replace(/ /g, ''),
          isAfrigisSearch: false,
        })
      );
    } else if (this.searchByDetailModel == 'Vehicle') {
      return this.crudService.get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_RESPONSE_OFFICER_DISPATCHER_SEARCH,
        null,
        true,
        prepareRequiredHttpParams({
          SearchValue: searchText,
          LoggedInUserId: this.loggedInUserData.userId
        }));
    }
    else if (this.searchByDetailModel == 'Site') {
      return this.crudService.get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_RESPONSE_OFFICER_DISPATCHER_SEARCH,
        null,
        true,
        prepareRequiredHttpParams({
          SearchValue: searchText,
          LoggedInUserId: this.loggedInUserData.userId
        }));
    }
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedOption = selectedObject;
  }

  hubConnectionInstanceForSalesAPI: HubConnection;
  hubConnectionForSalesAPIConfigs() {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
      // Live data retrieval for RO tracking ( dispatcher map view )
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.DispatcherMapViewTrigger, geoJsonData => {
        if (geoJsonData.receivingUserId.includes(this.loggedInUserData.userId)) {
          this.geoJsonData = geoJsonData;
          this.preparePolyLayersForDifferentShapes();
        }
      });
    })
      .catch(error => console.error(error, "Sales API Error from dispatcher-map-view component..!!"));
  }

  getListOfBootstrapModalTemplateReferences() {
    return LIST_OF_BOOTSTRAP_MODAL_TEMPLATE_REFERENCE;
  }

  actionOnBootstrapModal(templateActionType: string, index?: number) {
    if (index !== undefined && index !== null) {
      switch (index) {
        case 0:
          $(this.dispatch_unallocated_vehicle.nativeElement).modal(`${templateActionType}`);
          if (templateActionType == 'show') {
            this.getNearByCustomerDistances('reassign');
          }
          break;
        case 1:
          $(this.dispatch_allocated_vehicle.nativeElement).modal(`${templateActionType}`);
          if (templateActionType == 'show') {
            this.getNearByRODistances();
          }
          break;
        case 2:
          $(this.search_free_text_modal.nativeElement).modal(`${templateActionType}`);
          if (templateActionType == 'show') {
          }
          break;
        case 3:
          $(this.search_dispatcher_area_modal.nativeElement).modal(`${templateActionType}`);
          break;
        case 4:
          $(this.search_layers_modal.nativeElement).modal(`${templateActionType}`);
          break;
        case 5:
          $(this.selected_vehicle_summary_modal.nativeElement).modal(`${templateActionType}`);
          break;
        case 6:
          $(this.dispatch_allocated_vehicle_to_address.nativeElement).modal(`${templateActionType}`);
          if (templateActionType == 'show') {
            this.getNearByCustomerDistances();
          }
          break;
        case 7:
          $(this.response_officer_chat_modal.nativeElement).modal(`${templateActionType}`);
          break;
        case 8:
          $(this.selected_customer_alarm_summary_modal.nativeElement).modal(`${templateActionType}`);
          break;
        case 9:
          $(this.dispatch_vehicle_from_customer_alarm_address_modal.nativeElement).modal(`${templateActionType}`);
          break;
      }
    }
    else {
      LIST_OF_BOOTSTRAP_MODAL_TEMPLATE_REFERENCE.forEach((templateReferece) => {
        $(`${templateReferece}`).modal(`${templateActionType}`);
      });
      if (templateActionType == 'hide') {
        this.rxjsService.setDialogOpenProperty(false);
      }
    }
  }

  getNearByCustomerDistances(from = 'accept all') {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_OCCURRENCE_BOOK, undefined, false,
      prepareRequiredHttpParams({
        responseOfficerVehicleId: this.selectedVehicleObject.responseOfficerVehicleId,
        responseOfficerId: this.selectedVehicleObject.responseOfficerId,
        isAllocated: from == 'accept all' ? false : true
      })).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.distances = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  getNearByRODistances() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_RO_AVAILABLE,
      this.occurrenceBookId).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.distances = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  getAllROAgainstOccurenceBookId() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_RO_LIST, undefined, false,
      prepareRequiredHttpParams({
        occurrenceBookId: this.occurrenceBookId
      })).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.allROList = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onBootstrapModalEventChanges(): void {
    this.getListOfBootstrapModalTemplateReferences().forEach((templateRefName) => {
      ['shown.bs.modal', 'hidden.bs.modal'].forEach((bootstrapPopupAction) => {
        $(`${templateRefName}`).on(`${bootstrapPopupAction}`, (e) => {
          if (bootstrapPopupAction == 'shown.bs.modal') {
            this.isFormSubmitted = false;
            this.rxjsService.setDialogOpenProperty(true);
          }
          else {
            if (templateRefName == '#search_free_text_modal') {
              this.searchDetailsForm = clearFormControlValidators(this.searchDetailsForm, ['formatedAddress']);
              this.searchDetailsForm.updateValueAndValidity();
            }
            this.rxjsService.setDialogOpenProperty(false);
          }
        });
      });
    });
  }

  getDispatcherLiveDataByUserId(type: string): Observable<IApplicationResponse> {
    if (type == 'initial' && !this.searchByDetailModel) {
      return this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCHER_MAP_VIEW,
        undefined, false,
        prepareRequiredHttpParams({ userId: this.loggedInUserData.userId }));
    }
    else if (type == 'initial with 5 minutes auto refresh' && !this.searchByDetailModel) {
      return this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCHER_MAP_VIEW,
        undefined, false,
        prepareRequiredHttpParams({ userId: this.loggedInUserData.userId, isDefaultLoaderDisabled: true }));
    }
    else {
      return this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCHER_MAP_VIEW,
        undefined, false,
        prepareRequiredHttpParams({
          userId: this.loggedInUserData.userId,
          searchByLayerType: this.searchByDetailModel
        }));
    }
  }

  onOpenPopup(selectedVehicleObject) {
    this.selectedVehicleObject = null;
    this.selectedAllocatedVehicleIndex = 0;
    this.selectedUnAllocatedVehicleIndex = 0;
    this.selectedVehicleObject = selectedVehicleObject;
    this.occurrenceBookId = selectedVehicleObject.occurrenceBookId;
    if (selectedVehicleObject?.occurrenceBookType !== 'Patrol') {
      if (selectedVehicleObject.legentType == 'Site') {
        this.actionIconsForCustomer = ['vehicle-available.png'];
        this.actionOnBootstrapModal('show', 8);
        this.getDispatcherOccurrenceBookDetails();
      }
      else if (selectedVehicleObject.legentType == 'Vehicle') {
        this.responseOfficerId = selectedVehicleObject.id;
        if (selectedVehicleObject.iconName == 'vehicle-available.png' || selectedVehicleObject.iconName == 'vehicle-on-site.png') {
          this.actionIconsForVehicle = [`vehicle-available.png`];
        }
        else {
          this.actionIconsForVehicle = ['vehicle-allocated.png'];
        }
        this.actionOnBootstrapModal('show', 5);
        this.getDispatcherVehicleDetails();
      }
    }
    else if (selectedVehicleObject?.occurrenceBookType == 'Patrol') {
      this.actionOnBootstrapModal('show', 8);
      this.patrolId = selectedVehicleObject?.patrolId;
      this.getDispatcherOccurrenceBookDetails(selectedVehicleObject.occurrenceBookType);
    }
  }

  getDispatcherVehicleDetails() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCHER_MAP_VEHICLE_DETAILS, undefined, false,
      prepareRequiredHttpParams({ responseOfficerId: this.responseOfficerId })).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.selectedVehicleObject = { ...this.selectedVehicleObject, ...response.resources };
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  getDispatcherOccurrenceBookDetails(occurrenceBookType = 'OccurrenceBook') {
    let apiSuffix: EventMgntModuleApiSuffixModels = occurrenceBookType == 'OccurrenceBook' ? EventMgntModuleApiSuffixModels.DISPATCHER_MAP_OCCURRENCE_BOOK_DETAILS :
      EventMgntModuleApiSuffixModels.DISPATCHER_MAP_PATROL_DETAILS;
    let retrievableId = occurrenceBookType == 'OccurrenceBook' ? this.occurrenceBookId : this.patrolId;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, apiSuffix, retrievableId).subscribe((response) => {
      if (response.isSuccess && response.resources && response.statusCode == 200) {
        this.selectedVehicleObject = { ...this.selectedVehicleObject, ...response.resources };
        if (this.selectedVehicleObject.occurrenceBookStatus == DispatcherSiteStatuses.ALLOCATED ||
          this.selectedVehicleObject.occurrenceBookStatus == DispatcherSiteStatuses.DISPATCHED) {
          this.actionIconsForCustomer = ['vehicle-available.png', 'vehicle-allocated.png', 'chat.png'];
        }
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  getAllocatedVehicleReassignDetails() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_OCCURRENCE_BOOK_DETAILS, undefined, false,
      prepareRequiredHttpParams({
        responseOfficerVehicleId: this.selectedVehicleObject.responseOfficerVehicleId,
        occurrenceBookId: this.occurrenceBookId
      })).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.selectedVehicleObject = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onClickPopupIcon(type: string, iconName: string, selectedVehicleObject) {
    let actionRestrictedStatuses = ['Logout', 'Shift Change Request', 'Break', 'Break Taken More Time', 'Safety Alert', 'PreActive'];
    if (actionRestrictedStatuses.includes(selectedVehicleObject?.status)) {
      return;
    }
    this.declineReason = "";
    this.customerDistance = '';
    let templateIndex;
    if (type)
      switch (iconName.trimRight()) {
        case "vehicle-allocated.png":
          if (type == 'vehicle') {
            this.getNearByCustomerDistances('reassign');
            this.getAllocatedVehicleReassignDetails();
            templateIndex = 0;
          }
          else {
            this.getNearByRODistances();
            templateIndex = 1;
          }
          break;
        case "vehicle-dispatched.png":
          if (type == 'vehicle') {
            this.getNearByCustomerDistances('reassign');
            this.getAllocatedVehicleReassignDetails();
            templateIndex = 0;
          }
          else {
            this.getNearByRODistances();
            templateIndex = 1;
          }
          break;
        case "vehicle-available.png":
          if (type == 'vehicle') {
            this.getNearByCustomerDistances();
            templateIndex = 6;
          }
          else {
            this.getNearByRODistances();
            templateIndex = 9;
          }
          break;
        case "chat.png":
          templateIndex = 7;
          break;
        case "vehicle-dispatched.png":
          break;
        default:
          templateIndex = 2;
          break;
      }
    setTimeout(() => {
      this.actionOnBootstrapModal('hide');                           // to hide all bootstrap modals before open new one
    });
    setTimeout(() => {
      this.actionOnBootstrapModal('show', templateIndex);
    });
  }

  swapLatitudeToLongitude(coordinates) {
    coordinates.forEach((latLong, index) => {
      let latLongArr = JSON.parse(JSON.stringify(latLong));
      if (Math.sign(latLongArr[0]) == 1) {
        latLong[0] = latLongArr[1];
        latLong[1] = latLongArr[0];
      }
    });
    return coordinates;
  }

  getDistrictsByDivisionId(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, EventMgntModuleApiSuffixModels.ACTION_ARRIVAL,
      prepareRequiredHttpParams({ divisionId: "" })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
        }
      });
  }

  onMatTabChanged(type: string, selectedIndex: number) {
    this.isFormSubmitted = false;
    this.declineReason = "";
    switch (type) {
      case "action from RO vehicle":
        this.selectedUnAllocatedVehicleIndex = selectedIndex;
        switch (this.selectedUnAllocatedVehicleIndex) {
          case 0:
            this.actionBtnName = 'Reassign';
            break;
          case 1:
            this.allROList = [];
            this.getAllROAgainstOccurenceBookId();
            this.actionBtnName = 'Cancel Dispatch';
            break;
          case 2:
            this.actionBtnName = 'Make Available';
            break;
          case 3:
            this.actionBtnName = 'Manual Dispatch';
            break;
        }
        break;
      case "action from Customer Place":
        this.modalTitle = "Dispatch Options";
        this.selectedAllocatedVehicleIndex = selectedIndex;
        switch (this.selectedAllocatedVehicleIndex) {
          case 0:
            this.actionBtnName = 'Redirect';
            break;
          case 1:
            this.actionBtnName = 'Cancel Dispatch';
            break;
        }
        break;
    }
  }

  onIconClicked(popupType: string) {
    if (popupType == 'details') {
      let isAccessDeined = this.getAllPermissionByActionIconType(MAPVIEW_ACTION.ADDRESS_SEARCH_BY_DETAILS);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
    } else if (popupType == 'dispatcher area') {
      let isAccessDeined = this.getAllPermissionByActionIconType(MAPVIEW_ACTION.ADDESS_SEARCH_BY_DISPATCHER_AREA);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      
    } else if (popupType == 'layers') {
      let isAccessDeined = this.getAllPermissionByActionIconType(MAPVIEW_ACTION.ADDESS_SEARCH_BY_LAYERS);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
    } else {
    }

    this.isFormSubmitted = false;
    this.rxjsService.setDialogOpenProperty(true);
    let templateIndex;
    switch (popupType) {
      case 'details':
        templateIndex = 2;
        this.searchByDetailModel = 'Address';
        break;
      case 'dispatcher area':
        this.customerDistance = this.monitoringDataUser.stackAreaConfigId;
        this.getDispatcherAddresses();
        templateIndex = 3;
        break;
      case 'notification':
        return;
      case 'layers':
        this.getMapViewLayerTypes();
        templateIndex = 4;
        this.searchByDetailModel = this.monitoringDataUser.mapViewLayerTypeId;
        break;
    }
    this.actionOnBootstrapModal('show', templateIndex);
  }

  onFormatedAddressFormControlValueChanges() {
    this.searchDetailsForm
      .get("formatedAddress")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext) {
            this.getLoopableObjectRequestObservable = of();
          }
          else {
            this.getLoopableObjectRequestObservable = this.filterAddressOrVehicleByKeywordSearch(searchtext);
          }
        });
  }

  onRadioBtnSelected(type: string, $event: MatRadioChange) {
    if (type == 'Search by Details') {
      this.searchDetailsForm.reset(new DispatcherSearchByDetails());
    }
  }

  onSubmit(type: string) {
    if (type !== 'search by details') {
    }
    let apiType = 'post';
    this.isFormSubmitted = true;
    let payload: any = {
      createdUserId: this.loggedInUserData.userId,
      occurrenceBookId: this.occurrenceBookId,
      responseOfficerId: this.responseOfficerId,
      reasons: this.declineReason
    }, moduleApiSuffixModels: EventMgntModuleApiSuffixModels;
    switch (type) {
      case "action from RO vehicle":
        switch (this.selectedUnAllocatedVehicleIndex) {
          case 0:
            moduleApiSuffixModels = EventMgntModuleApiSuffixModels.VEHICLE_REASSIGN;
            payload.newoccurrenceBookId = this.customerDistance;
            break;
          case 1:
            moduleApiSuffixModels = EventMgntModuleApiSuffixModels.VEHICLE_CANCELLED;
            break;
          case 2:
            moduleApiSuffixModels = EventMgntModuleApiSuffixModels.VEHICLE_MAKE_AVAILABLE;
            break;
          case 3:
            moduleApiSuffixModels = EventMgntModuleApiSuffixModels.VEHICLE_DESPATCHED;
            break;
        }
        break;
      case "action from Customer Place":
        switch (this.selectedAllocatedVehicleIndex) {
          case 0:
            moduleApiSuffixModels = EventMgntModuleApiSuffixModels.VEHICLE_REDIRECT;
            delete payload.responseOfficerId;
            payload.newResponseOfficerId = this.customerDistance;
            break;
          case 1:
            moduleApiSuffixModels = EventMgntModuleApiSuffixModels.VEHICLE_CANCELLED;
            payload.responseOfficerId = this.customerDistance;
            break;
        }
        break;
      case "response officer chat":
        payload = {
          createdUserId: this.loggedInUserData.userId,
          occurrenceBookId: this.occurrenceBookId,
          chatMessage: this.declineReason
        }
        moduleApiSuffixModels = EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_CHAT_OCCURANCE_BOOK;
        break;
      case "search by details":
        if (this.searchDetailsForm.invalid) {
          return;
        }
        this.zoomInOptions = { latitude: this.selectedOption.latitude, longitude: this.selectedOption.longitude };
        this.searchDetailsForm.reset(new DispatcherSearchByDetails());
        this.actionOnBootstrapModal('hide');
        break;
      case "dispatcher area":
        payload.stackAreaConfigId = this.customerDistance;
        payload.userId = this.loggedInUserData.userId;
        delete payload.occurrenceBookId;
        delete payload.reasons;
        delete payload.createdUserId;
        delete payload.responseOfficerId;
        moduleApiSuffixModels = EventMgntModuleApiSuffixModels.MAP_MONITORING_CUSTOM;
        let selectedDispatcherCoordinates = this.dispatcherAddresses.find(dA => dA['id'] == this.customerDistance);
        this.zoomInOptionsForDispatcherArea = {
          southWestLatitude: selectedDispatcherCoordinates.southWestLatitude,
          southWestLongitude: selectedDispatcherCoordinates.southWestLongitude, northEastLatitude: selectedDispatcherCoordinates.northEastLatitude,
          northEastLongitude: selectedDispatcherCoordinates.northEastLongitude
        };
        break;
      case "layers":
        payload.mapViewLayerTypeId = this.searchByDetailModel;
        payload.userId = this.loggedInUserData.userId;
        payload.showLayerLabelName = this.showLayerLabelName;
        delete payload.occurrenceBookId;
        delete payload.reasons;
        delete payload.createdUserId;
        delete payload.responseOfficerId;
        moduleApiSuffixModels = EventMgntModuleApiSuffixModels.MAP_MONITORING_LAYER;
        apiType = 'update';
        break;
      case "dispatch to address":
        payload.responseOfficerChatMessage = "Allocated";
        payload.occurrenceBookId = this.customerDistance;
        payload.responseOfficerId = [this.responseOfficerId];
        if (this.selectedVehicleObject.legentType == "Site") {
          payload.isFollowResponseOfficerVehicle = this.selectedVehicleObject.isFollowLocation;
        }
        moduleApiSuffixModels = EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_RO_VEHICLE_ALLOCATED;
        break;
      case "dispatch from address":
        payload.responseOfficerChatMessage = "Allocated";
        payload.occurrenceBookId = this.occurrenceBookId;
        payload.responseOfficerId = [this.customerDistance];
        if (this.selectedVehicleObject.legentType == "Site") {
          payload.isFollowResponseOfficerVehicle = this.selectedVehicleObject.isFollowLocation;
        }
        moduleApiSuffixModels = EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_RO_VEHICLE_ALLOCATED;
        break;
    }
    if (moduleApiSuffixModels == EventMgntModuleApiSuffixModels.VEHICLE_REASSIGN ||
      moduleApiSuffixModels == EventMgntModuleApiSuffixModels.VEHICLE_REDIRECT) {
      if (!this.declineReason || !this.customerDistance) {
        return;
      }
      this.onFormSubmit(type, moduleApiSuffixModels, payload, apiType);
    }
    else if (moduleApiSuffixModels == EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_CHAT_OCCURANCE_BOOK) {
      if (!this.declineReason) {
        return;
      }
      this.onFormSubmit(type, moduleApiSuffixModels, payload, apiType);
    }
    else if (type !== 'action from RO vehicle' && moduleApiSuffixModels == EventMgntModuleApiSuffixModels.VEHICLE_CANCELLED ||
      moduleApiSuffixModels == EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_RO_VEHICLE_ALLOCATED ||
      moduleApiSuffixModels == EventMgntModuleApiSuffixModels.MAP_MONITORING_CUSTOM) {
      if (!this.customerDistance) {
        return;
      }
      this.onFormSubmit(type, moduleApiSuffixModels, payload, apiType);
    }
    else if (type == 'action from RO vehicle' && moduleApiSuffixModels == EventMgntModuleApiSuffixModels.VEHICLE_CANCELLED) {
      this.onFormSubmit(type, moduleApiSuffixModels, payload, apiType);
    }
    else if (type !== 'search by details') {
      this.onFormSubmit(type, moduleApiSuffixModels, payload, apiType);
    }
  }

  onFormSubmit(type: string, moduleApiSuffixModel, payload, apiType = 'post') {
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService = apiType == 'post' ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, moduleApiSuffixModel, payload) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, moduleApiSuffixModel, payload);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.actionOnBootstrapModal('hide');
        if (type == 'dispatcher area' || type == 'layers') {
          this.getForkJoinRequests('initial');
        }
        else {
          this.updateNewStatusIconInTheMap(type, moduleApiSuffixModel, response.resources);
        }
        this.getMapMonitoringDataByUser();
      }
      if (type == 'dispatcher area' || type == 'layers' || type == 'search by details') {
        this.rxjsService.setPopupLoaderProperty(false);
      }
      else {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  updateNewStatusIconInTheMap(type: string, moduleApiSuffixModels: EventMgntModuleApiSuffixModels, resultObj) {
    if (type == 'response officer chat') return;
    let filteredDispatcherFeature;
    if (this.selectedVehicleObject.legentType == "Vehicle") {
      filteredDispatcherFeature = this.geoJsonData['features'].find(f => f.properties.id == this.selectedVehicleObject.id);
    }
    else if (this.selectedVehicleObject.legentType == "Site") {
      filteredDispatcherFeature = this.geoJsonData['features'].find(f => f.properties.id == this.customerDistance);
    }
    switch (moduleApiSuffixModels) {
      case EventMgntModuleApiSuffixModels.VEHICLE_REASSIGN:
        filteredDispatcherFeature.properties.iconName = "vehicle-allocated.png";
        filteredDispatcherFeature.properties.status = 'Allocated';
        break;
      case EventMgntModuleApiSuffixModels.VEHICLE_CANCELLED:
        filteredDispatcherFeature.properties.iconName = "vehicle-available.png";
        filteredDispatcherFeature.properties.status = 'Available';
        if (this.selectedVehicleObject.legentType == "Site") {
          let filteredDispatcherFeatureObj = this.geoJsonData['features'].find(f => f.properties.occurrenceBookId == this.selectedVehicleObject.occurrenceBookId);
          filteredDispatcherFeatureObj.properties.iconName = resultObj.iconName;
          filteredDispatcherFeatureObj.properties.status = resultObj.occurrenceBookStatusName;
        }
        break;
      case EventMgntModuleApiSuffixModels.VEHICLE_MAKE_AVAILABLE:
        filteredDispatcherFeature.properties.iconName = "vehicle-available.png";
        filteredDispatcherFeature.properties.status = 'Available';
        break;
      case EventMgntModuleApiSuffixModels.VEHICLE_DESPATCHED:
        filteredDispatcherFeature.properties.iconName = "vehicle-dispatched.png";
        filteredDispatcherFeature.properties.status = 'Dispatched';
        break;
      case EventMgntModuleApiSuffixModels.VEHICLE_REDIRECT:
        filteredDispatcherFeature.properties.iconName = "vehicle-allocated.png";
        filteredDispatcherFeature.properties.status = 'Allocated';
        let filteredDispatcherFeatureObj = this.geoJsonData['features'].find(f => f.properties.id == resultObj.cancelledResponseOfficerId);
        filteredDispatcherFeatureObj.properties.iconName = "vehicle-available.png";
        filteredDispatcherFeatureObj.properties.status = 'Available';
        break;
      case EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_RO_VEHICLE_ALLOCATED:
        if (this.selectedVehicleObject.legentType == "Site") {
          let filteredDispatcherFeatureObj = this.geoJsonData['features'].find(f => f.properties.occurrenceBookId == this.selectedVehicleObject.occurrenceBookId);
          filteredDispatcherFeatureObj.properties.iconName = "site-allocated.png";
          filteredDispatcherFeatureObj.properties.status = "Allocated";
        }
        filteredDispatcherFeature.properties.iconName = "vehicle-allocated.png";
        filteredDispatcherFeature.properties.status = 'Allocated';
        break;
    }
    this.geoJsonData = { ...this.geoJsonData };
    this.preparePolyLayersForDifferentShapes();
  }

  onCloseBootstrapModal(type?: string) {
    if (type == 'search by details') {
      this.searchDetailsForm.get('formatedAddress').clearValidators();
      this.searchDetailsForm.updateValueAndValidity();
    }
    else if (type == 'dispatcher area') {
      if (this.geoJsonData?.['features']?.length > 0) {
        this.onBootstrapModalEventChanges();
      }
      else {
        setTimeout(() => {
          this.actionOnBootstrapModal('show', 3);
        }, 3000);
      }
      return;
    }
    this.onBootstrapModalEventChanges();
  }

  getMapMonitoringDataByUser() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAP_MONITORING_USER, this.loggedInUserData.userId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.monitoringDataUser = response.resources;
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  getAllPermissionByActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.allPermissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  ngOnDestroy() {
    setTimeout(() => {
      $(this.search_dispatcher_area_modal.nativeElement).modal(`hide`);
    }, 2000)
    this.hubConnectionInstanceForSalesAPI?.off(SignalRTriggers.DispatcherMapViewTrigger);
    clearInterval(this.setInterval);
    this.timer$?.unsubscribe();
  }
}
