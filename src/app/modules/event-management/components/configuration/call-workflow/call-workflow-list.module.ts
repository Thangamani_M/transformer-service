import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from '@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module';
import { MaterialModule } from '@app/shared/material.module';
import { SafeUrlPipe } from '@app/shared/pipes/safe-url.pipe';
import { QuickAddServiceCallDialogModule } from '@modules/customer/components/customer/customer-management/quick-add-service-call-dialog/quick-add-service-call-dialog.module';
import { EventsMgmtSharedModule } from '@modules/event-management/shared/events-mgmt-shared.module';
import { CallWorkFlowListRoutingModule } from './call-workflow-list-routing.module';
import { CallWorkFlowListComponent } from './call-workflow-list.component';
import { ExistDispatchDialogComponent } from './dialog-model/exist-dispatch-dialog/exist-dispatch-dialog.component';
import { PhonerFeedbackMessageComponent } from './dialog-model/phoner-feedback-message/phoner-feedback-message.component';
import { ShotsFiredClientDialogComponent } from './dialog-model/shots-fired-client-dialog/shots-fired-client-dialog.component';
import { ShotsFiredHubDialogComponent } from './dialog-model/shots-fired-hub-dialog/shots-fired-hub-dialog.component';
import { VideofiedSignalComponent } from './dialog-model/videofied-signal/videofied-signal.component';
import { HolidayInstructionsViewDetailsComponent } from './holiday-instructions-view-details/holiday-instructions-view-details.component';
import { PhoneViewDetailsComponent } from './phone-view-details/phone-view-details.component';
import { ResponseInstructionsViewDetailsModule } from './response-instructions-view-details/response-instructions-view-details.module';
@NgModule({
  declarations: [CallWorkFlowListComponent, PhoneViewDetailsComponent, VideofiedSignalComponent, ExistDispatchDialogComponent, PhonerFeedbackMessageComponent, HolidayInstructionsViewDetailsComponent, ShotsFiredClientDialogComponent, ShotsFiredHubDialogComponent,
    SafeUrlPipe],
  imports: [
    CommonModule,
    CallWorkFlowListRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ResponseInstructionsViewDetailsModule,
    EventsMgmtSharedModule,
    QuickAddServiceCallDialogModule,
    PrimengConfirmDialogPopupModule,
  ],
  entryComponents: [ExistDispatchDialogComponent, PhonerFeedbackMessageComponent, ShotsFiredClientDialogComponent, ShotsFiredHubDialogComponent, VideofiedSignalComponent]
})
export class CallWorkFlowListModule { }
