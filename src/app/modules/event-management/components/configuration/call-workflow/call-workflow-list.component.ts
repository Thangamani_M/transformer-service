
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, AGENT_LOGIN_ERROR_MESSAGE, clearFormControlValidators, CommonPaginationConfig, countryCodes, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, defaultPopupType, ExtensionModalComponent, filterFormControlNamesWhichHasValues, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, NO_DATA_FOUND_MESSAGE, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SignalRTriggers, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { SignalrConnectionService } from '@app/shared/services/signalr-connection.service';
import { HubConnection } from '@aspnet/signalr';
import { CustomerModuleApiSuffixModels, ThirdPartyResponseFormModel } from '@modules/customer';
import { QuickAddServiceCallDialogComponent } from '@modules/customer/components/customer/customer-management/quick-add-service-call-dialog/quick-add-service-call-dialog.component';
import { PasswordPopupComponent } from '@modules/event-management/components/configuration/call-workflow/password-popup/password-popup.component';
import { SharedClientAssitanceComponent } from '@modules/event-management/shared/components';
import { QuickActionName } from '@modules/event-management/shared/enums/call-work-flow-quick-action-name.enum';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, forkJoin, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, takeUntil } from 'rxjs/operators';
import { EventSignalListComponent } from '../call-workflow/event-signal-list/event-signal-list.component';
import { AddMessageOperatorModalComponent } from '../stack-winelands/add-message-operator-modal.component';
import { ExistDispatchDialogComponent } from './dialog-model/exist-dispatch-dialog/exist-dispatch-dialog.component';
import { ShotsFiredClientDialogComponent } from './dialog-model/shots-fired-client-dialog/shots-fired-client-dialog.component';
import { ShotsFiredHubDialogComponent } from './dialog-model/shots-fired-hub-dialog/shots-fired-hub-dialog.component';
@Component({
  selector: 'app-call-workflow-list',
  templateUrl: './call-workflow-list.component.html',
  styleUrls: ['./call-workflow-list.component.scss']
})
export class CallWorkFlowListComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  selectedTabIndex: any = 0;
  selectedMainTabIndex: any = 0;
  primengTableConfigProperties: any;
  dataList = [];
  loading: boolean;
  manualDispatchAddConfirmDialog: boolean;
  techAreaDropdown = [];
  todayDate = new Date();
  status = [{ label: 'Active', value: true }, { label: 'In-Active', value: false }];
  selectedColumns: any[];
  selectedRows: any[] = [];
  totalRecords: any;
  pageLimit: any = [25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  sendRoMessageForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  searchColumns: any;
  row: any = {};
  occurrenceBookId: string;
  openQuickAction = false;
  dispatchDialog = false;
  clientVehicleDialog = false;
  prioritizeSignalDialog = false;
  roMessageDialog = false;
  medicalDialog = false;
  medicalEditDialog = false;
  fireDialog = false;
  fireEditDialog = false;
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true });
  callWorkFlowVideoDialog = false
  getAccessCodeDialog = false
  manualDispatchDialog = false
  manualDispatchCancelConfirmDialog = false
  supervisorMessageDialog = false
  pageSize: number = 10;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  showFilterForm = false;
  filterForm: FormGroup;
  clusterList = [];
  subAreaList = [];
  vehicleCategoryList = [];
  vehicleStatusList = [];
  dispatchModeList = [];
  roDispatchActionForm: FormGroup;
  showAllDispatchList = false;
  scrollEnabled: boolean;
  initialLoad = false;
  selectedKeyHolderId: any;
  @ViewChildren('input') rows: QueryList<any>;
  medicalForm: FormGroup;
  medicalUpdateForm: FormGroup;
  medicalDetails: any;
  fireForm: FormGroup;
  fireUpdateForm: FormGroup;
  fireDetails: any;
  customerNameList = [];
  feedBackListDetails = [];
  feedBackListAdditional = [];
  eventMemoList = [];
  incidentFeedbackSummaryDetails:any = [];
  thirdPartyExternalList: any = [];
  thirdPartyInternalList: any = [];
  severities = [];
  customerId = '';
  customerAddressId: any;
  supervisorRequestForm: FormGroup;
  supervisorMessageForm: FormGroup;
  requestTypeList = [];
  callFeedBackStatusList = [];
  isShowAdditional = false;
  isClientRequestFeedback = false;
  isIncidentFeedback = false;
  isROFeedback = false;
  isThirdPartyFeedback = false;
  quickActionName = '';
  thirdPartyResponseDialog = false;
  thirdPartyResponseData;
  ThirdPartyResponseForm: FormGroup;
  agentExtensionNo = '';
  callLogUnsubscribe = new Subject();
  clientDetails: any;
  contactIndex: any;
  partitionId = '';
  allPermissions = [];
  actionPermissions: any = {};
  phoneActionPermissions = [];
  CallWorkFlowLists: any;
  hubConnectionInstanceForSalesAPI: HubConnection;
  defaultPopupType = defaultPopupType.P_DIALOG;
  NO_DATA_FOUND_MESSAGE = NO_DATA_FOUND_MESSAGE;
  isFilterBtnDisabled = true;
  isClearBtnDisabled = true;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService, public dialog: MatDialog, private snakbarService: SnackbarService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService, private signalrConnectionService: SignalrConnectionService) {
    this.store.pipe(select(loggedInUserData)).subscribe((loggedInUserModel: LoggedInUserModel) => {
      this.loggedInUserData = new LoggedInUserModel(loggedInUserModel);
    });
    this.primengTableConfigProperties = {
      tableCaption: "Holiday/Temp Instructions",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Operator Dashboard', relativeRouterUrl: '' }, { displayName: 'Events Management Platform', relativeRouterUrl: '/event-management/stack-operator' }, { displayName: 'Holiday/Temp Instructions' }],
      tableComponentConfigs: {
        mainTabsList: [
          {
            caption: 'Dispatch',
            dataKey: 'responseOfficerId',
            enableBreadCrumb: true,
            enableScrollable: true,
            checkBox: true,
            enableFieldsSearch: true,
            columns: [{ field: 'clusterName', header: 'Cluster', width: '150px' }, { field: 'subArea', header: 'Sub Area', width: '150px' }, { field: 'vehicleCategoryName', header: 'Vehicle Category', width: '150px' }, { field: 'vehicleRegistrationNumber', header: 'Vehicle', width: '150px' }, { field: 'responseOfficerVehicleStatusName', header: 'Vehicle Status', width: '150px' }, { field: 'responseOfficerName', header: 'RO Name', width: '150px' }, { field: 'allocated', header: 'Allocated', width: '150px' }, { field: 'dispatchModeName', header: 'Dispatch Mode', width: '150px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DESPATCH_VIEW_RO,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          },
        ],
        tabsList: [
          {
            caption: 'SIGNAL HISTORY',
            enableBreadCrumb: true,
            columns: [{ field: 'signalDateTime', header: 'Signal Time', width: '150px' }, { field: 'alarmType', header: 'Alarm', width: '150px' }, { field: 'description', header: 'Alarm Desc', width: '150px' }, { field: 'zoneType', header: 'Zone/User', width: '150px' }, { field: 'zoneNo', header: 'Zone Desc', width: '150px' }, { field: 'rowSignal', header: 'Raw Data', width: '150px' }, { field: 'decoder', header: 'Decoder', width: '150px' }, { field: 'accountCode', header: 'Transmitter', width: '150px' }, , { field: '', header: '', width: '100px' }],
          },
          {
            caption: 'ACTION & ARRIVALS',
            enableBreadCrumb: true,
            columns: [{ field: 'signalText', header: '', width: '150px' },
            { field: 'alarm', header: 'March', width: '150px' },
            { field: 'alarmDesc', header: 'April', width: '150px' },
            { field: 'zone', header: 'May', width: '150px' },
            { field: 'zoneDesc', header: 'June', width: '150px' },
            { field: 'rowData', header: 'July', width: '150px' },
            { field: 'decoder', header: 'August', width: '150px' },
            { field: 'transmitter', header: 'Month to Date', width: '150px' }],
          },
          {
            caption: 'CONTACTS NUMBERS',
            enableBreadCrumb: true,
            columns: [{ field: 'stackName', header: 'Signal Time', width: '200px' }, { field: 'divisionNames', header: 'Alarm', width: '200px' }, { field: 'namedStackConfigName', header: 'Alarm Desc', width: '200px' }, { field: 'customerTypeNames', header: 'Zone/User', width: '150px' }, { field: 'alarmGroupNames', header: 'Zone Desc', width: '150px' }, { field: 'createdUserName', header: 'Raw Data', width: '200px' }, { field: 'decoder', header: 'Decoder', width: '200px' }, { field: 'transmitter', header: 'Transmitter', width: '200px' }],
          },
          {
            caption: 'EME NUMBERS',
            enableBreadCrumb: true,
            columns: [{ field: 'stackName', header: 'Signal Time', width: '200px' }, { field: 'divisionNames', header: 'Alarm', width: '200px' }, { field: 'namedStackConfigName', header: 'Alarm Desc', width: '200px' }, { field: 'customerTypeNames', header: 'Zone/User', width: '150px' }, { field: 'alarmGroupNames', header: 'Zone Desc', width: '150px' }, { field: 'createdUserName', header: 'Raw Data', width: '200px' }, { field: 'decoder', header: 'Decoder', width: '200px' }, { field: 'transmitter', header: 'Transmitter', width: '200px' }],
          },
          {
            caption: 'EVENT MEMO',
            enableBreadCrumb: true,
            columns: [{ field: 'stackName', header: 'Signal Time', width: '200px' }, { field: 'divisionNames', header: 'Alarm', width: '200px' }, { field: 'namedStackConfigName', header: 'Alarm Desc', width: '200px' }, { field: 'customerTypeNames', header: 'Zone/User', width: '150px' }, { field: 'alarmGroupNames', header: 'Zone Desc', width: '150px' }, { field: 'createdUserName', header: 'Raw Data', width: '200px' }, { field: 'decoder', header: 'Decoder', width: '200px' }, { field: 'transmitter', header: 'Transmitter', width: '200px' }],
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
  }

  prepareBreadCrumbs() {
    let displayName = '', tableCaption = '';
    switch (this.selectedMainTabIndex) {
      case 0:
        displayName = 'Holiday/Temp Instructions';
        tableCaption = 'Holiday/Temp Instructions';
        break;
      case 1:
        displayName = 'Response Instructions'
        tableCaption = 'Response Instructions';
        break;
      case 2:
        displayName = 'Call Work Flow';
        tableCaption = 'Call Work Flow';
        break;
      case 3:
        displayName = 'Dispatch';
        tableCaption = 'Dispatch';
        break;
      case 4:
        displayName = 'Feedback';
        tableCaption = 'Feedback';
        break;
      case 5:
        displayName = 'Finish';
        tableCaption = 'Finish';
        break;
    }
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = displayName;
    this.primengTableConfigProperties.tableCaption = tableCaption;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.rxjsService.setGlobalLoaderProperty(false);
    this.searchKeywordRequest();
    this.columnFilterRequest();
  }

  createSupervisorRequestFrom() {
    this.supervisorRequestForm = this._fb.group({
      occurrenceBookId: [''],
      clientRequestTypeId: ['', Validators.required],
      comments: ['', Validators.required],
      createdUserId: [''],
    });
  }

  createSupervisorMessageForm() {
    this.supervisorMessageForm = this._fb.group({
      occurrenceBookId: [this.occurrenceBookId ? this.occurrenceBookId : ''],
      clientName: [''],
      address: [''],
      operatorId: [this.loggedInUserData.userId, Validators.required],
      supervisorMainAreaId: ['', Validators.required],
      supervisorSubAreaId: ['', Validators.required],
      isSiteMessage: ['true', Validators.required],
      messageDateTime: [new Date().toISOString(), Validators.required],
      message: ['', Validators.required],
      isActive: [true, Validators.required],
      createdUserId: [this.loggedInUserData.userId],
    });
  }

  ngAfterViewInit() {
    const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-view')[0];
    if (scrollableBody) {
      scrollableBody.onscroll = (x) => {
        this.scrollEnabled = true;
        var st = Math.floor(scrollableBody.scrollTop + scrollableBody.offsetHeight)
        let max = scrollableBody.scrollHeight - 150;
        if (st >= max) {
          if (this.dataList.length < this.totalRecords) {
            this.onCRUDRequested(CrudType.GET, this.row);
          }
        }
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$),
      this.activatedRoute.queryParamMap]
    ).subscribe((response) => {
      if (response[0]) {
        this.occurrenceBookId = response[0]?.id;
        this.customerId = response[0]?.customerId;
        this.customerAddressId = response[0]?.customerAddressId;
        this.partitionId = response[0]?.partitionId;
        this.contactIndex = response[0]?.contact;
        this.rxjsService.setCustomerAddresId(this.customerAddressId);
        this.rxjsService.setCustomerId(this.customerId);
      }
      this.allPermissions = response[1][EVENT_MANAGEMENT_COMPONENT.DASHBOARD];
      if (this.allPermissions) {
        this.actionPermissions = this.allPermissions.find(item => item.menuName == "Action");
        this.phoneActionPermissions = this.actionPermissions['subMenu'].find(item => item.menuName == "Phone");
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.allPermissions);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      if (response[2]) {
        this.selectedMainTabIndex = (Object.keys(response[2]['params']).length > 0) ? +response[2]['params']['tab'] : 0;
        this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
        this.prepareBreadCrumbs();
        this.onTabChange({ index: this.selectedMainTabIndex });
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {})
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.selectedRows = [];
    this.dataList = [];
    otherParams = !otherParams ? {} : otherParams;
    if (this.selectedMainTabIndex == 3) {
      otherParams['occurrenceBookId'] = this.occurrenceBookId ? this.occurrenceBookId : null;
    }
    else {
      otherParams['occurrenceBookId'] = this.occurrenceBookId ? this.occurrenceBookId : null;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    if (this.selectedMainTabIndex == 2) {
      eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    } else {
      eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.mainTabsList[0].apiSuffixModel;
    }
    if (this.initialLoad) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    }
    this.initialLoad = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.resources && data.isSuccess && data.statusCode == 200) {
        if (!this.scrollEnabled) {
          this.dataList = [];
          this.dataList = data.resources;
        } else {
          this.dataList.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = data.totalCount;
        this.row['pageIndex'] = this.row['pageIndex'] + 1;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    },
      error => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (this.selectedTabIndex == 1) {
          this.dataList = [
            { signalText: 'Actions', alarm: '123-123', alarmDesc: '123-123', zone: '123-123', zoneDesc: '123-123', rowData: '123-123', decoder: '123-123', transmitter: '123-123' },
            { signalText: 'Arrivals', alarm: '123-123', alarmDesc: '123-123', zone: '123-123', zoneDesc: '123-123', rowData: '123-123', decoder: '123-123', transmitter: '123-123' },
          ]
        }
        this.totalRecords = this.dataList.length;
      });
  }

  loadPaginationLazy(event) {
    let row = {};
    if (this.searchColumns) {
      row['pageIndex'] = 0;
    }
    else {
      row['pageIndex'] = event.first / event.rows;
    }
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string) {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date') || key.toLowerCase().includes('time')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, otherParams);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string) {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/signal-management/signal-management-config/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/signal-management/signal-management-config/view"], { queryParams: { id: editableObject['signalManagementConfigId'] } });
            break;
        }
    }
  }

  manualRefreshDispatchData() {
    this.selectedTabIndex = 3;
    this.getRequiredListData(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSizeAsTwenty);
  }

  onTabChange(event) {
    this.columnFilterForm = this._fb.group({});
    this.columnFilterRequest();
    this.selectedMainTabIndex = event.index;
    if (event.index == 2) {
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
      this.createSendROMessageForm();
      this.createMedicalForm();
      this.createMedicalUpdateForm();
      this.createSupervisorRequestFrom();
      this.createSupervisorMessageForm();
      this.createFireForm();
      this.createFireUpdateForm();
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Customer,
        this.customerId, false, null).subscribe((response) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.clientDetails = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else if (event.index == 3) {
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.mainTabsList[0].columns);
      this.createFilterForm();
      this.onFilterFormControlChanges();
      this.createDispatchForm();
      this.hubConnectionInstanceForSalesAPIConfigs();
      this.getRequiredListData(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSizeAsTwenty, {});
    }
    else if (this.selectedMainTabIndex == 4) {
      this.getForkJoinRequestsForFeedbackTab();
    }
    this.dataList = [];
    this.totalRecords = null;
    if (event?.tab && event?.tab?.textLabel) {
      this.primengTableConfigProperties.tableCaption = event?.tab?.textLabel == "Phone" ? 'Call Work Flow' : event?.tab?.textLabel;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = event?.tab?.textLabel == "Phone" ? 'Call Work Flow' : event?.tab?.textLabel;
    }
    else {
      this.prepareBreadCrumbs();
    }
    this.openQuickAction = false;
  }

  onFilterFormControlChanges() {
    this.filterForm.valueChanges
      .pipe(
        map((object) => filterFormControlNamesWhichHasValues(object)))
      .subscribe((keyValuesObj) => {
        this.isFilterBtnDisabled = (Object.keys(keyValuesObj).length == 0) ? true : false;
        this.isClearBtnDisabled = Object.keys(keyValuesObj).length > 0 ? false : true;
      });
  }

  hubConnectionInstanceForSalesAPIConfigs() {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CallWorkFlowDispatchTrigger, (data) => {
        if (data) {
          this.dataList = data;
        }
      });
    });
  }

  onChangeStatus(rowData, index) {
    this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
      this.primengTableConfigProperties, rowData)?.onClose?.subscribe((result) => {
        if (!result) {
          this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
        }
      });
  }

  toggleQuickAction() {
    this.openQuickAction = !this.openQuickAction
  }

  onRowSelect(event) {
    this.callWorkFlowVideoDialog = true;
  }

  onRowUnselect(event) {
    this.callWorkFlowVideoDialog = true;
  }

  openAddMessageDialog() {
    this.rxjsService.setDialogOpenProperty(true);
    this.dialog.open(AddMessageOperatorModalComponent, { width: '700px', disableClose: true, data: this.occurrenceBookId });
  }

  openShotsFiredDialog(type = 'normal') {
    let customerType = 1;
    let componentType = type == 'normal' ? ShotsFiredClientDialogComponent : ShotsFiredHubDialogComponent;
    if (customerType == 1) {
      let data = {
        occurrenceBookId: this.occurrenceBookId,
      }
      this.dialogService.open(componentType, { showHeader: false, data: { ...data }, width: '750px' });
    } else {
      return this.snakbarService.openSnackbar('Occurance Book Id Required', ResponseMessageTypes.WARNING)
    }
  }

  openQuickAddServiceCallDialog() {
    if (this.customerAddressId === "00000000-0000-0000-0000-000000000000") {
      return
    }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.QUICK_ADD_SERVICE,
      undefined, null, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.customerId, AddressId: this.customerAddressId }))
      .subscribe(response => {
        this.dialog.open(QuickAddServiceCallDialogComponent, {
          width: '750px',
          data: {
            customerId: this.customerId,
            customerAddressId: this.customerAddressId,
            isProceed: response.resources?.debtorId ? true : false,
            warningMsg: response.resources?.warningMessage ? response.resources?.warningMessage : response.resources?.debtorId ? null : 'No Debtor created for the customer. Unable to create a service call',
            userId: this.loggedInUserData?.userId,
            edit: true
          }
        });
      });
  }

  openModel(actionName: QuickActionName | string) {
    let split = actionName.split('-')
    let isAccessDeined = this.getActionIconType(split.join(' '));
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setDialogOpenProperty(true);
    this.quickActionName = actionName;
    switch (actionName) {
      case QuickActionName.CLIENT_REQUEST_VEHICLE:
        this.clientVehicleDialog = true;
        this.toggleQuickAction();
        break;
      case QuickActionName.FAULTY_ALARM:
        this.opensuspendSignalDialogModal();
        this.toggleQuickAction();
        break;
      case QuickActionName.FIRE:
        this.fireDialog = true;
        this.getThirdPartyDetails(1);
        this.toggleQuickAction();
        break;
      case QuickActionName.FIRE_EDIT:
        this.fireEditDialog = true;
        this.getThirdPartyDetails(1);
        this.toggleQuickAction();
        break;
      case QuickActionName.LOAD_TEMPORARY_WORK_FLOW:
        this.toggleQuickAction();
        break;
      case QuickActionName.MEDICAL:
        this.medicalDialog = true;
        this.getThirdPartyDetails(2);
        this.toggleQuickAction();
        break;
      case QuickActionName.MEDICAL_EDIT:
        this.medicalEditDialog = true;
        this.getThirdPartyDetails(2);
        this.toggleQuickAction();
        break;
      case QuickActionName.MESSAGE:
        this.openAddMessageDialog();
        this.toggleQuickAction();
        break;
      case QuickActionName.THIRD_PARTY_RESPONSE:
        if (!this.thirdPartyResponseData) {
          return this.snackbarService.openSnackbar("3rd Party Service Not Found", ResponseMessageTypes.WARNING);
        }
        this.thirdPartyResponseDialog = true;
        this.initThirdPartyResponseForm();
        this.toggleQuickAction();
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_RESPONSE,
          this.occurrenceBookId, false, null).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.thirdPartyResponseData = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        break;
      case QuickActionName.PASSWORD:
        this.selectedKeyHolderId = ''
        if (this.occurrenceBookId != undefined) {
          let data = {
            customerId: this.customerId,
            addressId: this.customerAddressId,
            occurrenceBookId: this.occurrenceBookId,
            LoggedInUserId: this.loggedInUserData.userId,
            partitionId: this.partitionId
          }
          this.dialogService.open(PasswordPopupComponent, { showHeader: false, data: { ...data, isPasswordDialog: true }, width: '500px' });
        } else {
          return this.snakbarService.openSnackbar('Occurance Book Id Required', ResponseMessageTypes.WARNING);
        }
        this.toggleQuickAction();
        break;
      case QuickActionName.PRIORITIZE_DISPATCH:
        this.dispatchDialog = true
        this.toggleQuickAction();
        break;
      case QuickActionName.PRIORITIZE_SIGNAL:
        this.prioritizeSignalDialog = true;
        this.toggleQuickAction();
        break;
      case QuickActionName.RO_INSTRUCTIONS:
        this.toggleQuickAction();
        break;
      case QuickActionName.SEND_RO_MESSAGE:
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_RESPONSE_OFFICER_VEHICLE_OB, this.occurrenceBookId, false, null)
          .subscribe((response: IApplicationResponse) => {
            if (response.resources.length === 0) {
              this.snackbarService.openSnackbar("There is no Response Officers allocated for this Occurrence Book", ResponseMessageTypes.ERROR);
            } else {
              this.sendRoMessageForm.get('chatMessage').setValue(null);
              this.createSendROMessageForm();
              this.roMessageDialog = true;
            }
          });
        this.toggleQuickAction();
        break;
      case QuickActionName.SEND_SUPERVISOR_MESSAGE:
        this.supervisorMessageDialog = true;
        this.toggleQuickAction();
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS,
          this.customerId + '/' + this.customerAddressId + '?CustomerId=' + this.customerId + '&customerAddressId=' + this.customerAddressId,
        )
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.supervisorMessageForm.get('clientName').setValue(response.resources.displayName);
              this.supervisorMessageForm.get('address').setValue(response.resources?.fullAddress);
              this.supervisorMessageForm.get('supervisorMainAreaId').setValue(response.resources.mainAreaId);
              this.supervisorMessageForm.get('supervisorSubAreaId').setValue(response.resources.subAreaId);
            }
          });
        break;
      case QuickActionName.GET_ACCESS_CODE:
        this.getAccessCodeDialog = true;
        this.toggleQuickAction();
        break;
      case QuickActionName.GATE_ACCESS:
        this.toggleQuickAction();
        break;
      case QuickActionName.RESPONSE_INSTRUCTIONS:
        this.toggleQuickAction();
        break;
      case QuickActionName.SHOTS_FIRED:
        this.openShotsFiredDialog();
        this.toggleQuickAction();
        break;
      case QuickActionName.SHOTS_FIRED_HUB:
        this.openShotsFiredDialog('hub');
        this.toggleQuickAction();
        break;
      case QuickActionName.QUICK_ADD_SERVICE_CALL:
        this.openQuickAddServiceCallDialog();
        this.toggleQuickAction();
        break;
      case QuickActionName.SUPERVISOR_REQUEST:
        this.openSupervisorRequestModal();
        break;
    }
  }

  quicActionSubmit(actionName: QuickActionName | string) {
    switch (actionName) {
      case QuickActionName.CLIENT_REQUEST_VEHICLE:
        let formValue = { occurrenceBookId: this.occurrenceBookId, modifiedUserId: this.loggedInUserData.userId };
        this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_REQUEST_DISPATCH, formValue)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.quicActionCancel(actionName);
            }
          });
        break;
      case QuickActionName.FIRE_EDIT:
        this.filterForm.reset();
        this.fireEditDialog = false;
        this.fireDialog = true;
        break;
      case QuickActionName.MEDICAL_EDIT:
        this.medicalForm.reset();
        this.medicalEditDialog = false;
        this.medicalDialog = true;
        break;
      case QuickActionName.PRIORITIZE_DISPATCH:
        let priorityDispch = { occurrenceBookId: this.occurrenceBookId, modifiedUserId: this.loggedInUserData.userId };
        this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PRIORITIZATION_DISPACHE, priorityDispch)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.quicActionCancel(actionName);
            }
          })
        break;
      case QuickActionName.PRIORITIZE_SIGNAL:
        let prioritizeSignalFormValue = { occurrenceBookId: this.occurrenceBookId, modifiedUserId: this.loggedInUserData.userId };
        this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_PRIORITIZATION_MANUAL, prioritizeSignalFormValue)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.quicActionCancel(actionName)
            }
          });
        break;
      case QuickActionName.SEND_SUPERVISOR_MESSAGE:
        if (this.supervisorMessageForm.invalid) {
          return;
        }
        let supervisorMessageForm = this.supervisorMessageForm.value;
        supervisorMessageForm.isSiteMessage = supervisorMessageForm.isSiteMessage == 'true' ? true : false;
        supervisorMessageForm.occurrenceBookId = this.occurrenceBookId;
        supervisorMessageForm.createdUserId = this.loggedInUserData.userId;
        this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUPERVISOR_MESSAGE, supervisorMessageForm)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.quicActionCancel(actionName);
            }
          })
        break;
      case QuickActionName.SUPERVISOR_REQUEST:
        if (this.supervisorRequestForm.invalid) {
          return;
        }
        let supervisorRequestFromValue = this.supervisorRequestForm.value;
        supervisorRequestFromValue.occurrenceBookId = this.occurrenceBookId;
        supervisorRequestFromValue.createdUserId = this.loggedInUserData.userId;
        this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUPERVISOR_REQUEST, supervisorRequestFromValue)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.quicActionCancel(actionName);
            }
          })
        break;
      case QuickActionName.THIRD_PARTY_RESPONSE:
        if (this.ThirdPartyResponseForm.invalid) {
          return;
        }
        let thirdPartyResponseFormValue = this.ThirdPartyResponseForm.value;
        for (const item of this.thirdPartyResponseData.operatorInstructionlist) {
          if (item.isLinkToPage) {
            thirdPartyResponseFormValue.operatorInstructionList.push({ operatorInstructions: `${item.comments} ${item.urlLink}` })
          } else {
            thirdPartyResponseFormValue.operatorInstructionList.push({ operatorInstructions: item.comments })
          }
        }
        thirdPartyResponseFormValue.occurrenceBookId = this.occurrenceBookId;
        thirdPartyResponseFormValue.createdUserId = this.loggedInUserData.userId;
        this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_RESPONSE, thirdPartyResponseFormValue)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.quicActionCancel(actionName);
            }
          });
        break;
    }
  }

  quicActionCancel(actionName: QuickActionName | string) {
    switch (actionName) {
      case QuickActionName.CLIENT_REQUEST_VEHICLE:
        this.clientVehicleDialog = false
        break;
      case QuickActionName.FAULTY_ALARM:
        this.opensuspendSignalDialogModal();
        break;
      case QuickActionName.FIRE:
        this.fireDialog = false;
        this.fireForm.reset()
        break;
      case QuickActionName.FIRE_EDIT:
        this.fireEditDialog = false;
        break;
      case QuickActionName.MEDICAL:
        this.medicalDialog = false
        break;
      case QuickActionName.MEDICAL_EDIT:
        this.medicalEditDialog = false
        break;
      case QuickActionName.PRIORITIZE_DISPATCH:
        this.dispatchDialog = false
        break;
      case QuickActionName.PRIORITIZE_SIGNAL:
        this.prioritizeSignalDialog = false;
        break;
      case QuickActionName.RO_INSTRUCTIONS:
        break;
      case QuickActionName.GET_ACCESS_CODE:
        this.getAccessCodeDialog = false
        break;
      case QuickActionName.SEND_RO_MESSAGE:
        this.roMessageDialog = false;
        break;
      case QuickActionName.SEND_SUPERVISOR_MESSAGE:
        this.supervisorMessageDialog = false;
        this.supervisorMessageForm.reset();
        break;
      case QuickActionName.THIRD_PARTY_RESPONSE:
        this.thirdPartyResponseDialog = false;
        this.callLogUnsubscribe.next();
        this.callLogUnsubscribe.complete();
        break;
    }
    this.rxjsService.setDialogOpenProperty(false);
  }

  opensuspendSignalDialogModal() {
    let data = {
      occurrenceBookId: this.occurrenceBookId,
      customerId: this.customerId,
      customerAddressId: this.customerAddressId
    }
    this.dialogService.open(EventSignalListComponent, { showHeader: false, data: { ...data, }, width: '750px' });
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      clusterConfigId: [''],
      subAreaId: [''],
      vehicleCategoryId: [''],
      responseOfficerVehicleStatusId: [''],
      dispatchModeId: [''],
    });
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { clusterConfigId: this.filterForm.get('clusterConfigId').value ? this.filterForm.get('clusterConfigId').value.id : '' },
      { subAreaId: this.filterForm.get('subAreaId').value ? this.filterForm.get('subAreaId').value.id : '' },
      { vehicleCategoryId: this.filterForm.get('vehicleCategoryId').value ? this.filterForm.get('vehicleCategoryId').value.id : '' },
      { responseOfficerVehicleStatusId: this.filterForm.get('responseOfficerVehicleStatusId').value ? this.filterForm.get('responseOfficerVehicleStatusId').value.id : '' },
      { dispatchModeId: this.filterForm.get('dispatchModeId').value ? this.filterForm.get('dispatchModeId').value.id : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key];
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {});
    filterdNewData['occurrenceBookId'] = this.occurrenceBookId ? this.occurrenceBookId : null;
    this.getRequiredListData(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }

  clearDispatchFilter(val) {
    this.showAllDispatchList = val;
    this.filterForm.reset();
    this.row['pageIndex'] = 0;
    this.getRequiredListData(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, val ? null : {});
    this.showFilterForm = !this.showFilterForm;
  }

  onClickApplyFilterOnDispatchTab() {
    this.showFilterForm = !this.showFilterForm;
    this.openQuickAction = false;
    if (this.showFilterForm && this.clusterList.length == 0 && this.subAreaList.length == 0 && this.vehicleCategoryList.length == 0
      && this.vehicleStatusList.length == 0 && this.dispatchModeList.length == 0) {
      this.getDropdownList();
    }
  }

  getDropdownList() {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CLUSTER_CONFIG),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SUB_AREA),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_CATEGORY),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_RO_VEHICLE_STATUS),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DISPATCH_MODE),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.clusterList = resp.resources;
              break;
            case 1:
              this.subAreaList = resp.resources;
              break;
            case 2:
              this.vehicleCategoryList = resp.resources;
              break;
            case 3:
              this.vehicleStatusList = resp.resources;
              break;
            case 4:
              this.dispatchModeList = resp.resources;
              break;
          }
        }
        if (response.length == ix + 1) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    });
  }

  getForkJoinRequestsAfterPhoneInFireIncident() {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SEVERITY, null, false, null),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.severities = resp.resources;
              break;
          }
        }
        if (response.length == ix + 1) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    });
  }

  getForkJoinRequestsForFeedbackTab() {
    this.loading = true;
    let requiredParams = { OccurrenceBookId: this.occurrenceBookId };
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FEED_BACK_LIST, undefined,
        false, prepareGetRequestHttpParams(null, null, requiredParams)),
      this.crudService.get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_EVENT_MEMO,
        undefined,
        false, prepareGetRequestHttpParams(null, null, requiredParams)),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_FEEDBACK_EXTERNAL_LIST,
        undefined, false, prepareGetRequestHttpParams(null, null, requiredParams)),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_FEEDBACK_INTERNAL_LIST,
        undefined, false, prepareGetRequestHttpParams(null, null, requiredParams)),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CALL_FEEDBACK_STATUS, null, false, null),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.feedBackListDetails = resp.resources;
              this.feedBackListDetails.forEach(element => {
                if (element.feedbackScreen === "ClientRequest") {
                  this.isClientRequestFeedback = true;
                } else if (element.feedbackScreen === "Incident") {
                  this.isIncidentFeedback = true;
                } else if (element.feedbackScreen === "ROFeedback") {
                  this.isROFeedback = true
                } else {
                  this.isThirdPartyFeedback = true;
                }
              });
              break;
            case 1:
              this.eventMemoList = resp.resources;
              break;
            case 2:
              this.thirdPartyExternalList = resp.resources;
              break;
            case 3:
              this.thirdPartyInternalList = resp.resources;
              break;
            case 4:
              this.callFeedBackStatusList = resp.resources;
              break;
          }
        }
        if (response.length == ix + 1) {
          this.getIncidentFeedbackSummary();
        }
      });
    });
  }

  createDispatchForm() {
    this.roDispatchActionForm = this._fb.group({
      createdUserId: [this.loggedInUserData.userId],
      occurrenceBookId: [this.occurrenceBookId ? this.occurrenceBookId : null, Validators.required],
      responseOfficerChatMessage: [''],
      responseOfficerId: [null],
      newOccurrenceBookId: [''],
      reasons: ['']
    });
  }

  createSendROMessageForm() {
    this.sendRoMessageForm = this._fb.group({
      createdUserId: [this.loggedInUserData.userId],
      occurrenceBookId: [this.occurrenceBookId ? this.occurrenceBookId : null, Validators.required],
      chatMessage: ['', Validators.required],
    });
  }

  existDispatchAlert(rowData) {
    const ref = this.dialogService.open(ExistDispatchDialogComponent, {
      showHeader: true,
      header: 'Dispatch Vehicle',
      baseZIndex: 10000,
      width: '450px',
      data: rowData
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
    });
  }

  // Cannot allocate the following Status (Allocated, Dispatched,  Arrived, LongOnSite, Panic,  Break, ShiftChangeRequest)
  dispatchAction() {
    this.roDispatchActionForm = clearFormControlValidators(this.roDispatchActionForm, ["responseOfficerChatMessage"]);
    let responseOfficerIds = [];
    this.selectedRows.forEach(element => {
      responseOfficerIds.push(element.responseOfficerId);
    })
    this.roDispatchActionForm.get('responseOfficerId').setValue(responseOfficerIds);
    if (this.roDispatchActionForm.invalid) {
      this.roDispatchActionForm.markAllAsTouched();
      this.roDispatchActionForm.markAsDirty();
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_RO_VEHICLE_ALLOCATED, this.roDispatchActionForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.onSuccessAPIResponse();
        }
      });
  }

  cancelDispatchActionConfirm() {
    this.roDispatchActionForm = clearFormControlValidators(this.roDispatchActionForm, ["responseOfficerChatMessage"]);
    if (this.roDispatchActionForm.invalid) {
      this.roDispatchActionForm.markAllAsTouched();
      this.roDispatchActionForm.markAsDirty();
      return;
    }
    this.manualDispatchCancelConfirmDialog = true;
  }

  //Cannot cancel the following Status ( Available, Cancelled, Arrived, LongOnSite)
  cancelDispatchAction() {
    this.manualDispatchCancelConfirmDialog = false;
    let responseOfficerIds = [];
    this.selectedRows.forEach(element => {
      responseOfficerIds.push(element.responseOfficerId);
    });
    this.roDispatchActionForm.get('responseOfficerId').setValue(responseOfficerIds);
    if (this.roDispatchActionForm.invalid) {
      return;
    }
    let formValue = this.roDispatchActionForm.value;
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_RO_VEHICLE_CANCELLED, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.onSuccessAPIResponse();
        }
      });
  }

  replaceVehicle() {
    this.roDispatchActionForm = clearFormControlValidators(this.roDispatchActionForm, ["responseOfficerChatMessage"]);
    let responseOfficerIds = [];
    this.selectedRows.forEach(ele => {
      responseOfficerIds.push(ele.responseOfficerId);
    });
    this.roDispatchActionForm.get('responseOfficerId').setValue(responseOfficerIds);
    this.roDispatchActionForm.get('occurrenceBookId').setValue(this.occurrenceBookId);
    if (this.roDispatchActionForm.invalid) {
      this.roDispatchActionForm.markAllAsTouched();
      this.roDispatchActionForm.markAsDirty();
      return;
    }
    this.roDispatchActionForm.value.newOccurrenceBookId = this.occurrenceBookId;
    this.roDispatchActionForm.value.reasons = this.roDispatchActionForm.get('responseOfficerChatMessage').value;
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_REASSIGN, this.roDispatchActionForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.onSuccessAPIResponse();
        }
      });
  }

  sendMessage() {
    this.roDispatchActionForm = setRequiredValidator(this.roDispatchActionForm, ["responseOfficerChatMessage"]);
    if (this.roDispatchActionForm.invalid) {
      this.roDispatchActionForm.markAllAsTouched();
      this.roDispatchActionForm.markAsDirty();
      return;
    }
    let payload = {
      createdUserId: this.loggedInUserData.userId,
      occurrenceBookId: this.occurrenceBookId,
      chatMessage: this.roDispatchActionForm.get('responseOfficerChatMessage').value
    }
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_CHAT_OCCURANCE_BOOK, payload)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.onSuccessAPIResponse();
        }
      });
  }

  onSuccessAPIResponse() {
    this.showAllDispatchList = true;
    this.filterForm.reset();
    this.getRequiredListData(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, {});
  }

  onSubmitROMessage() {
    if (this.sendRoMessageForm.invalid) {
      return;
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.sendRoMessageForm.value.occurrenceBookId = this.occurrenceBookId;
    this.sendRoMessageForm.value.createdUserId = this.loggedInUserData.userId;
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_CHAT_OCCURANCE_BOOK, this.sendRoMessageForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.roMessageDialog = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          this.rxjsService.setDialogOpenProperty(false);
        }
        else {
          this.roMessageDialog = false;
          this.snakbarService.openSnackbar('Response Officer Chat - No Responce Officers allocated.', ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
  }

  onChangeKeyHolder(event) {
    this.selectedKeyHolderId = event.target.value;
  }

  createMedicalForm() {
    this.medicalForm = this._fb.group({
      occurrenceBookId: [this.occurrenceBookId, Validators.required],
      keyHolderId: ['', Validators.required],
      contactNumber: ['', Validators.required],
      contactNumberCountryCode: ['+27'],
      noOfInjured: ['', Validators.required],
      locationOfInjuredPersonsOnProperty: [''],
      ageAndSexOfParties: [''],
      additionalInformation: [''],
      contactName: [''],
      createdUserId: [this.loggedInUserData.userId],
      modifiedUserId: [this.loggedInUserData.userId],
      thirdPartyType: ['',],
      officeContactNumberCountryCode: ["+27",],
      officeContactNumber: ['',]
    });
    this.medicalForm.get('keyHolderId').valueChanges.subscribe((val) => {
      if (val) {
        this.getCustomerNumber(val)
        const val1 = this.customerNameList.find(el => el?.id == val)?.displayName
        this.medicalForm.get('contactName').setValue(val1)
      }
    });
    this.medicalForm.get('thirdPartyType').valueChanges.subscribe((val) => {
      if (val) {
        const val1 = this.CallWorkFlowLists.find(el => el?.companyName == val)?.officeContactNumber
        this.medicalForm.get('officeContactNumber').setValue(val1)
      }
    });
  }

  createMedicalUpdateForm() {
    this.medicalUpdateForm = this._fb.group({
      occurrenceBookMedicalId: ['', Validators.required],
      controllerName: ['', Validators.required],
      controllerOBNumber: ['', Validators.required],
      estimatedTimeOfArrival: ['', Validators.required],
      createdUserId: [this.loggedInUserData.userId],
      modifiedUserId: [this.loggedInUserData.userId]
    });
  }

  getCustomerNumber(keyholderId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER_ID, keyholderId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          if (this.medicalEditDialog) {
            this.medicalForm.get('contactNumber').setValue(response.resources.contactNo);
            this.medicalForm.get('contactNumberCountryCode').setValue(response.resources.contactNoCountryCode);
          }
          if (this.fireEditDialog) {
            this.fireForm.get('contactNumber').setValue(response.resources.contactNo);
            this.fireForm.get('contactNumberCountryCode').setValue(response.resources.contactNoCountryCode);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMedicalDetailsById(dispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEDICAL, dispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.medicalDetails = response.resources;
          this.medicalForm.patchValue({
            controllerName: response.resources.controllerName, controllerOBNumber: response.resources.controllerOBNumber,
            estimatedTimeOfArrival: response.resources.estimatedTimeOfArrival
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmitMedical() {
    this.medicalForm.patchValue({
      occurrenceBookId: this.occurrenceBookId, createdUserId: this.loggedInUserData.userId, modifiedUserId: this.loggedInUserData.userId
    });
    if (this.medicalForm.invalid) {
      return;
    }
    let formValue = this.medicalForm.value;
    let a = formValue.contactNumber;
    let b = a.split(' ').join('');
    formValue.contactNumber = b;
    delete formValue['thirdPartyType'];
    delete formValue['officeContactNumberCountryCode'];
    delete formValue['officeContactNumber'];
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEDICAL, formValue).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.medicalForm.reset();
        this.quicActionSubmit('medicalEdit');
        this.medicalUpdateForm.get('occurrenceBookMedicalId').setValue(response.resources);
        this.getMedicalDetailsById(response.resources);
      }
    });
  }

  onSubmitMedicalUpdate() {
    this.medicalUpdateForm.patchValue({
      createdUserId: this.loggedInUserData.userId,
      modifiedUserId: this.loggedInUserData.userId
    });
    if (this.medicalUpdateForm.invalid) {
      return;
    }
    let formValue = this.medicalUpdateForm.value;
    formValue.contactNumber = formValue.contactNumber.split(' ').join('')
    delete formValue.thirdPartyType;
    delete formValue.officeContactNumberCountryCode;
    delete formValue.officeContactNumber;
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEDICAL, formValue).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.medicalUpdateForm.reset();
        this.quicActionCancel('medical');
      }
    });
  }

  createFireForm() {
    this.fireForm = this._fb.group({
      occurrenceBookId: [this.occurrenceBookId, Validators.required],
      keyHolderId: ['', Validators.required],
      contactNumber: ['', Validators.required],
      contactNumberCountryCode: ['+27'],
      severityId: ['', Validators.required],
      locationOfFire: [''],
      additionalHazards: [''],
      additionalInformation: [''],
      contactName: [''],
      createdUserId: [this.loggedInUserData.userId],
      modifiedUserId: [this.loggedInUserData.userId],
      thirdPartyType: ['',],
      officeContactNumberCountryCode: ["+27",],
      officeContactNumber: ['',]
    });
    this.fireForm.get('keyHolderId').valueChanges.subscribe((val) => {
      if (val) {
        this.getCustomerNumber(val)
        const val1 = this.customerNameList.find(el => el?.id == val)?.displayName
        this.fireForm.get('contactName').setValue(val1)
      }
    });
    this.fireForm.get('thirdPartyType').valueChanges.subscribe((val) => {
      if (val) {
        const val1 = this.CallWorkFlowLists.find(el => el?.companyName == val)?.officeContactNumber
        this.fireForm.get('officeContactNumber').setValue(val1)
      }
    });
  }

  createFireUpdateForm() {
    this.fireUpdateForm = this._fb.group({
      occurrenceBookFireId: ['', Validators.required],
      controllerName: ['', Validators.required],
      controllerOBNumber: ['', Validators.required],
      estimatedTimeOfArrival: ['', Validators.required],
      createdUserId: [this.loggedInUserData.userId],
      modifiedUserId: [this.loggedInUserData.userId]
    });
  }

  getFireDetailsById(dispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_FIRE, dispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.fireDetails = response.resources;
          this.fireUpdateForm.patchValue({
            controllerName: response.resources.controllerName,
            controllerOBNumber: response.resources.controllerOBNumber,
            estimatedTimeOfArrival: response.resources.estimatedTimeOfArrival
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmitFire() {
    this.fireForm.patchValue({
      createdUserId: this.loggedInUserData.userId,
      modifiedUserId: this.loggedInUserData.userId,
      occurrenceBookId: this.occurrenceBookId
    });
    if (this.fireForm.invalid) {
      return;
    }
    let formValue = this.fireForm.value;
    let a = formValue.contactNumber;
    let b = a.split(' ').join('');
    formValue.contactNumber = b;
    delete formValue['thirdPartyType'];
    delete formValue['officeContactNumberCountryCode'];
    delete formValue['officeContactNumber'];
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_FIRE, formValue).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.filterForm.reset();
        this.quicActionSubmit('fireEdit');
        this.fireUpdateForm.get('occurrenceBookFireId').setValue(response.resources);
        this.getFireDetailsById(response.resources);
      }
    });
  }

  onSubmitFireUpdate() {
    this.fireForm.patchValue({
      createdUserId: this.loggedInUserData.userId,
      modifiedUserId: this.loggedInUserData.userId,
    });
    if (this.fireUpdateForm.invalid) {
      return;
    }
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_FIRE, this.fireUpdateForm.value).
      subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.fireUpdateForm.reset();
          this.quicActionCancel('fire');
        }
      });
  }

  getFeedbackListAdditional() {
    this.loading = true;
    let params = {
      OccurrenceBookId: this.occurrenceBookId,
      CustomerId: this.customerId,
      CustomerAddressId: this.customerAddressId
    };
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.FEEDBACK_ADDITIONAL,
      undefined,
      false, prepareRequiredHttpParams(params)
    ).subscribe(data => {
      if (data.resources && data.isSuccess && data.statusCode == 200) {
        this.feedBackListAdditional = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.loading = false;
    });
  }

  getIncidentFeedbackSummary() {
    if (this.customerAddressId === "00000000-0000-0000-0000-000000000000") {
      return
    }
    this.loading = true;
    let otherParams = {
      OccurrenceBookId: this.occurrenceBookId,
      CustomerId: this.customerId,
      CustomerAddressId: this.customerAddressId
    };
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.FEEDBACK_INCIDENT_FEEDBACK_SUMMARY_LIST,
      undefined,
      false, prepareGetRequestHttpParams(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, otherParams)
    ).subscribe(data => {
      if (data.resources && data.isSuccess && data.statusCode == 200) {
        this.incidentFeedbackSummaryDetails = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.loading = false;
    });
  }

  chatMessage;
  onFeedbackSubmit(type: string) {
    if (type == 'ro feedback') {
      this.feedBackListDetails.forEach(element => {
        element.chatMessage = this.chatMessage;
        element.responseOfficerChatTime = new Date();
        element.isRead = true;
      });
    }
    this.removeUnwantedFeedbackListDetailsObjProps();
    let apiSuffix = type == 'incident' ? EventMgntModuleApiSuffixModels.FEEDBACK_INCIDENTFEEDBACK_ADD : type == 'client request' ?
      EventMgntModuleApiSuffixModels.FEEDBACK_CLIENTFEEDBACK_ADD : type == 'third party' ?
        EventMgntModuleApiSuffixModels.THIRD_PARTY_FEEDBACK_ADD : EventMgntModuleApiSuffixModels.FEEDBACK_ROFEEDBACK_ADD;
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, apiSuffix, this.feedBackListDetails).subscribe();
  }

  removeUnwantedFeedbackListDetailsObjProps() {
    this.feedBackListDetails.forEach(element => {
      element.createdUserId = this.loggedInUserData.userId;
      element.occurrenceBookId = this.occurrenceBookId;
      element.phonerId = this.loggedInUserData.userId;
      element.mobileNumberCountryCode = element.contactNoCountryCode;
      element.mobileNumber = element.mobileNo;
      element.callStartTime = new Date();
      element.callEndTime = new Date();
      delete element['contactNoCountryCode'];
      delete element['countryCodeWithMobile'];
      delete element['feedbackScreen'];
      delete element['keyHolderName'];
      delete element['mobileNo'];
    });
  }

  showAdditionalContact() {
    this.getFeedbackListAdditional();
    this.isShowAdditional = true;
  }

  openSupervisorRequestModal() {
    const ref = this.dialogService.open(SharedClientAssitanceComponent, {
      showHeader: true,
      header: 'Client Assitance',
      baseZIndex: 10000,
      width: '650px',
      data: { occurrenceBookId: this.occurrenceBookId }
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
    });
  }

  initThirdPartyResponseForm = () => {
    let thirdPartyModel = new ThirdPartyResponseFormModel();
    this.ThirdPartyResponseForm = this._fb.group({});
    Object.keys(thirdPartyModel).forEach((key) => {
      this.ThirdPartyResponseForm.addControl(key, new FormControl(thirdPartyModel[key]));
    });
    this.ThirdPartyResponseForm = setRequiredValidator(this.ThirdPartyResponseForm, ["controllerName", "thirdPartyOBNumber", "eta"]);
    this.ThirdPartyResponseForm.patchValue({
      createdUserId: this.loggedInUserData.userId, isActive: true, operatorId: this.loggedInUserData.userId,
      occurrenceBookId: this.occurrenceBookId, thirdPartyId: this.thirdPartyResponseData?.thirdPartyId,
    });
  }

  listenCallLog = () => {
    this.rxjsService.getUniqueCallId()
      .pipe(takeUntil(this.callLogUnsubscribe))
      .subscribe(data => {
        if (data && data.UniqueCallid) {
          this.ThirdPartyResponseForm.get('callLogId').setValue(data.UniqueCallid);
          this.rxjsService.setUniqueCallId(null);
        }
      });
  }

  Dial(e) {
    if (!this.agentExtensionNo) {
      this.agentExtensionPopupOpen();
      this.dialog.closeAll();
    } else {
      let data = {
        customerContactNumber: e,
        customerId: this.thirdPartyResponseData?.customerId,
        siteAddressId: this.customerAddressId,
        clientName: this.thirdPartyResponseData && this.thirdPartyResponseData.displayName,
        occurrenceBookId: this.occurrenceBookId
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
      this.callLogUnsubscribe.next();
      this.listenCallLog()
    }
  }

  getCustomerDetailsById(customerId: string) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Customer, customerId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.clientDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getActionIconType(actionTypeMenuName): boolean {
    if (!this.phoneActionPermissions) {
      return true
    }
    let foundObj = this.phoneActionPermissions['subMenu'].find(fSC => fSC.menuName.toLowerCase() == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  getAllPermissionByActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.allPermissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  getThirdPartyDetails(event) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_THIRD_PARTY_DETAILS_TYPE,
      undefined, null, prepareGetRequestHttpParams(null, null,
        { TypeId: event, OccurrenceBookId: this.occurrenceBookId }))
      .subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.CallWorkFlowLists = response.resources;
        }
        if (this.medicalDialog || this.fireDialog) {
          this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_KEY_HOLDER,
            this.occurrenceBookId, false, null).subscribe((response) => {
              if (response.isSuccess && response.statusCode == 200 && response.resources) {
                this.customerNameList = response.resources;
              }
              this.rxjsService.setPopupLoaderProperty(false);
            });
        }
        else {
          this.rxjsService.setPopupLoaderProperty(false);
        }
      });
  }

  callDail(e, e1) {
    let countryCode = e.value
    let mobileNumber = e1.value
    let number = mobileNumber.split(' ').join('')
    let number1 = countryCode + number
    if (!this.agentExtensionNo) {
      this.fireEditDialog = false;
      this.medicalEditDialog = false;
      this.toggleQuickAction();
      this.agentExtensionPopupOpen();
    } else {
      let data = {
        customerContactNumber: number1,
        customerId: this.customerId,
        clientName: number1,
        siteAddressId: this.customerAddressId,
        occurrenceBookId: this.occurrenceBookId
      }
      this.dialog.closeAll();
      this.fireEditDialog = false;
      this.medicalEditDialog = false;
      this.toggleQuickAction()
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  agentExtensionPopupOpen() {
    this.dialog.closeAll();
    this.snackbarService.openSnackbar(AGENT_LOGIN_ERROR_MESSAGE, ResponseMessageTypes.WARNING);
    this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
  }
}
