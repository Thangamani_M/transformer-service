import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, AGENT_LOGIN_ERROR_MESSAGE, CommonPaginationConfig, ConfirmDialogModel, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, ExtensionModalComponent, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, MomentService, NO_DATA_FOUND_MESSAGE, prepareGetRequestHttpParams, PrimengConfirmDialogPopupComponent, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SignalrConnectionService, SignalRTriggers, SnackbarService, TableFilterFormService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { HubConnection } from '@aspnet/signalr';
import { CallworkFlowListModel } from "@modules/event-management/models/configurations/phone-view-call-flow.model";
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, forkJoin, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { ArmDisarmScheduleDialogComponent } from '../arm-disarm-schedule-dialog/arm-disarm-schedule-dialog.component';
import { VideofiedSignalComponent } from '../dialog-model/videofied-signal/videofied-signal.component';
@Component({
  selector: 'app-phone-view-details',
  templateUrl: './phone-view-details.component.html'
})
export class PhoneViewDetailsComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  @Input() occurrenceBookId
  @Input() customerAddressId;
  @Input() customerId;
  @Input() contactIndex;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  observableResponse;
  CallWorkFlowLists: FormArray;
  searchKeyword: FormControl;
  columnFilterForm: FormGroup;
  addMessageForm: FormGroup;
  today = new Date();
  searchColumns;
  agentExtensionNo;
  callflowForm: FormGroup;
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  phoneViewContactNumberDetails;
  emeNumberDetails;
  specialInstructionDetails;
  customerKeyholderList = [];
  commentsDetails;
  eventMemoList;
  instructorinstructionsalarmtype;
  hubConnectionInstanceForSalesAPI: HubConnection;
  NO_DATA_FOUND_MESSAGE = NO_DATA_FOUND_MESSAGE;

  constructor(private crudService: CrudService, public dialog: MatDialog, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private tableFilterFormService: TableFilterFormService, private signalrConnectionService: SignalrConnectionService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {
    super();
    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data;
      })
    this.rxjsService.getCustomerId()
      .subscribe(data => {
        this.customerId = data;
      })
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Holiday/Temp Instructions",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Management', relativeRouterUrl: '' }, { displayName: 'Holiday/Temp Instructions' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SIGNAL HISTORY',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'signalDateTime', header: 'Signal Time', width: '150px' },
            { field: 'alarmType', header: 'Alarm', width: '150px' },
            { field: 'description', header: 'Alarm Desc', width: '150px' },
            { field: 'zoneNo', header: 'Zone/User', width: '150px' },
            { field: 'zoneType', header: 'Zone/user Desc', width: '150px' },
            { field: 'rowSignal', header: 'Raw Data', width: '150px' },
            { field: 'decoder', header: 'Decoder', width: '150px' },
            { field: 'accountCode', header: 'Transmitter', width: '150px' },
            { field: 'status', header: 'Status', width: '180px' },
            { field: '', header: 'Action', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_OCCURRENCE_BOOK_SIGNAL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          },
          {
            caption: 'ACTION & ARRIVALS',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableScrollable: true,
            columns: [
              { field: 'type', header: ' ', width: '100px' },
              { field: 'month6Ago', header: '--', width: '100px' },
              { field: 'month5Ago', header: '--', width: '100px' },
              { field: 'month4Ago', header: '--', width: '100px' },
              { field: 'month3Ago', header: '--', width: '100px' },
              { field: 'month2Ago', header: '--', width: '100px' },
              { field: 'month1Ago', header: '--', width: '100px' },
              { field: 'toDate', header: 'Month To Date', width: '100px' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.ACTION_ARRIVAL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          },
          {
            caption: 'CONTACTS NUMBERS',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'stackName', header: 'Signal Time', width: '200px' }, { field: 'divisionNames', header: 'Alarm', width: '200px' }, { field: 'namedStackConfigName', header: 'Alarm Desc', width: '200px' }, { field: 'customerTypeNames', header: 'Zone/User', width: '150px' }, { field: 'alarmGroupNames', header: 'Zone Desc', width: '150px' }, { field: 'createdUserName', header: 'Raw Data', width: '200px' }, { field: 'decoder', header: 'Decoder', width: '200px' }, { field: 'transmitter', header: 'Transmitter', width: '200px' }],
          },
          {
            caption: 'EME NUMBERS',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'stackName', header: 'Signal Time', width: '200px' }, { field: 'divisionNames', header: 'Alarm', width: '200px' }, { field: 'namedStackConfigName', header: 'Alarm Desc', width: '200px' }, { field: 'customerTypeNames', header: 'Zone/User', width: '150px' }, { field: 'alarmGroupNames', header: 'Zone Desc', width: '150px' }, { field: 'createdUserName', header: 'Raw Data', width: '200px' }, { field: 'decoder', header: 'Decoder', width: '200px' }, { field: 'transmitter', header: 'Transmitter', width: '200px' }],
          },
          {
            caption: 'EVENT MEMO',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'stackName', header: 'Signal Time', width: '200px' }, { field: 'divisionNames', header: 'Alarm', width: '200px' }, { field: 'namedStackConfigName', header: 'Alarm Desc', width: '200px' }, { field: 'customerTypeNames', header: 'Zone/User', width: '150px' }, { field: 'alarmGroupNames', header: 'Zone Desc', width: '150px' }, { field: 'createdUserName', header: 'Raw Data', width: '200px' }, { field: 'decoder', header: 'Decoder', width: '200px' }, { field: 'transmitter', header: 'Transmitter', width: '200px' }],
          },
        ]
      }
    }
    this.columnFilterForm = this._fb.group({});
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.hubConnectionInstanceForSalesAPIConfigs();
    this.columnFilterRequest()
    this.createcallflowForm();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.getForkJoinRequests();
    this.getRequiredListData(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSizeAsTwenty);
    // remove this later after mani's confirmation
    //this.commentsDetailsByyId();
    if (this.contactIndex) {
      this.rxjsService.getTabActinCallflow().subscribe((selectedTabIndex: number) => {
        this.selectedTabIndex = 2;
      });
    }
  }

  getForkJoinRequests() {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.INSTRUCTOR_INSTRUCTIONS_ALARMTYPE, this.occurrenceBookId, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SPECIAL_INSTRUCTION_OCCURANCE_BOOK, this.occurrenceBookId, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER, null,
        false, prepareGetRequestHttpParams(null, null, { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId })),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PHONE_CALL_FEEDBACK, this.occurrenceBookId, false, null)
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.instructorinstructionsalarmtype = resp.resources;
              break;
            case 1:
              this.specialInstructionDetails = resp.resources;
              break;
            case 2:
              this.customerKeyholderList = resp.resources;
              break;
            case 3:
              if (resp.resources.length > 0) {
                this.callflowForm.get('isText').setValue(resp.resources[0].isText);
                this.callflowForm.get('isPhoneCall').setValue(resp.resources[0].isPhoneCall);
                this.callflowForm.get('isNeedToVerifyWithCustomer').setValue(resp.resources[0].isNeedToVerifyWithCustomer);
              }
              this.getKeyHolderList();
              break;
          }
        }
      });
    });
  }


  hubConnectionInstanceForSalesAPIConfigs() {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CallWorkFlowPhoneCallFlowTrigger, (data) => {
        if (data) {
          while (this.CallWorkFlowLists.length) {
            this.CallWorkFlowLists.removeAt(this.CallWorkFlowLists.length - 1);
          }
          this.CallWorkFlowLists = this.getCallworkGroupEmployeeListArray;
          data.forEach((callworkflowlistModel: CallworkFlowListModel) => {
            callworkflowlistModel.phonerId = this.loggedInUserData?.userId
            this.CallWorkFlowLists.push(this.createCallworkFlowListListModel(callworkflowlistModel));
          });
        }
      });
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CallWorkFlowSignalHistoryTrigger, (data) => {
        if (data) {
          this.dataList = data;
        }
      });
    });
  }

  ngAfterViewInit() {
    if (!this.tables.first) return
    const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-view')[0];
    scrollableBody.onscroll = (x) => {
      this.scrollEnabled = true;
      var st = Math.floor(scrollableBody.scrollTop + scrollableBody.offsetHeight)
      let max = scrollableBody.scrollHeight - 150;
      if (st >= max) {
        if (this.dataList.length < this.totalRecords) {
          this.onCRUDRequested(CrudType.GET, this.row);
        }
      }
    }
  }

  getDynamicMonthList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ACTION_ARRIVAL_MONTHS, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[1].header = response.resources.month6Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[2].header = response.resources.month5Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[3].header = response.resources.month4Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[4].header = response.resources.month3Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[5].header = response.resources.month2Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[6].header = response.resources.month1Ago
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmitAcknowledgement() {
    this.rxjsService.setGlobalLoaderProperty(true);
    let formValue = { occurrenceBookId: this.occurrenceBookId, acknowledgedBy: this.loggedInUserData.userId };
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_ACKNOWLEDGEMENT_PHONER_INSTRUCTION, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createcallflowForm() {
    this.callflowForm = this._fb.group({
      keyHolders: this._fb.array([]),
      isText: false,
      isPhoneCall: false,
      isNeedToVerifyWithCustomer: false,
      occurrenceBookId: [this.occurrenceBookId],
    });
  }

  getKeyHolderList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CALL_FLOW_PHONER, this.occurrenceBookId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          while (this.CallWorkFlowLists?.length) {
            this.CallWorkFlowLists.removeAt(this.CallWorkFlowLists?.length - 1);
          }
          this.CallWorkFlowLists = this.getCallworkGroupEmployeeListArray;
          response.resources.forEach((callworkflowlistModel: CallworkFlowListModel) => {
            callworkflowlistModel.phonerId = this.loggedInUserData?.userId;
            this.CallWorkFlowLists.push(this.createCallworkFlowListListModel(callworkflowlistModel));
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createCallworkFlowListListModel(callworkflowlistModel?: CallworkFlowListModel): FormGroup {
    let CallworkFlowListFormControlModel = new CallworkFlowListModel(callworkflowlistModel);
    let formControls = {};
    Object.keys(CallworkFlowListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: CallworkFlowListFormControlModel[key], disabled: false }]
    });
    let forms = this._fb.group(formControls);
    return forms;
  }

  get getCallworkGroupEmployeeListArray(): FormArray {
    if (!this.callflowForm) return;
    return this.callflowForm.get("keyHolders") as FormArray;
  }

  openCallDialog(obj) {
    const dialogData = new ConfirmDialogModel("Confirmation", "Call Non-Workflow Number");
    const alertDialog = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
      showHeader: false,
      width: "450px",
      data: { ...dialogData, isConfirm: true, isClose: true, OkButtonName: 'Acknowledge', CancelButtonName: 'Cancel' },
      baseZIndex: 5000,
    });
    alertDialog.onClose.subscribe(dialogResult => {
      if (dialogResult) {
        this.makeCall(obj);
      }
    });
  }

  makeCall(e): void {
    let selectedRow = this.customerKeyholderList?.filter(x => x?.id == e?.value?.keyHolderId);
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar(AGENT_LOGIN_ERROR_MESSAGE, ResponseMessageTypes.WARNING);
      this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else {
      let data = {
        customerContactNumber: (e?.mobileNumberCountryCode && e?.mobileNumber)
          ? `${e?.mobileNumberCountryCode}${e?.mobileNumber}` :
          (e?.contactNoCountryCode && e?.contactNo) ?
            `${e?.contactNoCountryCode}${e?.contactNo}` :
            e?.officeContactNumber ? e?.officeContactNumber : (e?.value?.mobileNumberCountryCode && e?.value?.mobileNumber)
              ? `${e?.value?.mobileNumberCountryCode}${e?.value?.mobileNumber}` : e?.value?.mobileNumber ? e?.value?.mobileNumber : '',
        customerId: this.customerId,
        clientName: selectedRow && selectedRow[0]?.displayName ? selectedRow[0]?.displayName : e?.value?.callReceiverName ? e?.value?.callReceiverName :
          e?.contactPersonName ? e?.contactPersonName : '',
        siteAddressId: this.customerAddressId,
        occurrenceBookId: this.occurrenceBookId
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  };

  onFeedbackSubmit(callFlowObj) {
    let payload = {
      occurrenceBookCallFlowId: callFlowObj?.value.occurrenceBookCallFlowId,
      callFeedback: callFlowObj?.value.callFeedback,
      modifiedUserId: this.loggedInUserData.userId
    };
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OB_CALL_FEEDBACK_SUBMIT, payload)
      .subscribe((response) => {
        if (response.statusCode && response.isSuccess) {
          this.getKeyHolderList();
        }
      });
  }

  changeNeedToVerifyWithCustomer() {
    let formData = this.callflowForm.value;
    formData.createdUserId = this.loggedInUserData.userId;
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PHONE_CALL_FEEDBACK, formData).subscribe();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key];
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {});
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          this.scrollEnabled = false;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.initialLoad) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    }
    this.initialLoad = true;
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['occurrenceBookId'] = this.occurrenceBookId ? this.occurrenceBookId : null
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.resources && data.isSuccess && data.statusCode == 200) {
        this.observableResponse = data.resources;
        if (!this.scrollEnabled) {
          this.dataList = [];
          this.dataList = this.observableResponse;
        } else {
          this.observableResponse.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = data.totalCount;
        this.row['pageIndex'] = this.row['pageIndex'] + 1
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
      }
    });
  }

  getActionAndArrivals(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
      CustomerId: this.customerId,
      CustomerAddressId: this.customerAddressId
    };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ACTION_ARRIVAL,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.resources && data.isSuccess && data.statusCode == 200) {
        this.observableResponse = data.resources;
        if (!this.scrollEnabled) {
          this.dataList = [];
          this.dataList = this.observableResponse;
        } else {
          this.observableResponse.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = data.totalCount;
        this.row['pageIndex'] = this.row['pageIndex'] + 1;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
      }
    });
  }

  getContactNumbers(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.KEY_HOLDER_OCCERENCE_BOOK,
      this.occurrenceBookId,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.resources && data.isSuccess && data.statusCode == 200) {
        this.phoneViewContactNumberDetails = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getEMENumbers(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = { occurrenceBookId: this.occurrenceBookId };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.NOTIFIED_BODY_NUMBER_OCCURRENCE_BOOK,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.resources && data.isSuccess && data.statusCode == 200) {
        this.emeNumberDetails = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  loadPaginationLazy(event) {
    let row = {};
    if (this.searchColumns) {
      row['pageIndex'] = 0;
    }
    else {
      row['pageIndex'] = event.first / 20;
    }
    row["pageSize"] = 20;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.scrollEnabled = false;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/signal-management/signal-management-work-list/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/signal-management/signal-management-work-list/view"], { queryParams: { id: editableObject['signalManagementConfigId'] } });
            break;
        }
    }
  }

  onTabChange(event) {
    this.row = {};
    this.columnFilterForm = this._fb.group({});
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    switch (this.selectedTabIndex) {
      case 0:
        this.getRequiredListData(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, {});
        break;
      case 1:
        this.getDynamicMonthList();
        this.getActionAndArrivals(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, {});
        break;
      case 2:
        this.getContactNumbers();
        break;
      case 3:
        this.getEMENumbers();
        break;
      case 4:
        this.getEventMemoList();
        break;
    }
  }

  onChangeStatus(rowData, index) {
    this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
      this.primengTableConfigProperties, rowData)?.onClose?.subscribe((result) => {
        if (!result) {
          this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
        }
      });
  }

  openArmDisarmDialog() {
    this.rxjsService.setDialogOpenProperty(true);
    this.dialogService.open(ArmDisarmScheduleDialogComponent, {
      header: 'Arm Disarm Schedule',
      width: '950px',
      data: this.occurrenceBookId
    });
  }

  onSubmitUndefinedSignalAknowledgement() {
    let obj = {
      occurrenceBookId: this.occurrenceBookId,
      isInstructorInstructionNeedToAcknowledge: true,
      AcknowledgedBy: this.loggedInUserData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_ACKNOWLEDGEMENT_PHONER_INTERACTION, obj)
      .subscribe();
  }

  openMediaPath(rowData) {
    rowData['occurrenceBookId'] = this.occurrenceBookId;
    this.dialog.open(VideofiedSignalComponent, {
      width: '750px',
      data: rowData
    });
  }

  commentsDetailsByyId() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUPERVISOR_REQUEST_OCCURRENCE_BOOK_COMMENTS, this.occurrenceBookId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.commentsDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getEventMemoList() {
    let otherParams = {};
    this.eventMemoList = null;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_EVENT_MEMO_REPORT,
      this.occurrenceBookId,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.eventMemoList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}