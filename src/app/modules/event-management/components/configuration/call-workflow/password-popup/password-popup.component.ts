import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { defaultPopupType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-password-popup',
  templateUrl: './password-popup.component.html'
})
export class PasswordPopupComponent implements OnInit {
  qucickActionName: any
  passwordDialog: boolean = false;
  incorrectPasswordDialog: boolean = false;
  passwordCorrectDialog: boolean = false;
  isSitePassword: boolean = false;
  clientVehicleDialog: boolean;
  selectedKeyHolderId: any;
  keyHolderPassword: boolean = false;
  KeyHolderPasswordList: any;
  loggedUser: any;
  keyHolderList: any = [];
  customerData: any;
  customerAddressId: any;
  customerId = '';
  OccurrenceBookId = '';
  Type: any;
  Selectedpassword: any;
  occurrenceBookId: string;
  siteDropDown=[];
  referenceDropId: any;
  siteType: any;
  sitePasswordPasswordList=[];
  partitionId: any;
  isDistress: boolean;
  defaultPopupType = defaultPopupType.ANGULAR_MATERIAL_DRAGGABLE;

  constructor(private crudService: CrudService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, public dialogService: DialogService, private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.rxjsService.setDialogOpenProperty(true);
    this.customerId = config?.data.customerId
    this.OccurrenceBookId = this.config?.data?.occurrenceBookId
    this.customerAddressId = config?.data.addressId
    this.partitionId = this.config?.data?.partitionId
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.setCustomerAddresId(this.customerAddressId)
    this.rxjsService.setCustomerId(this.customerId)
  }

  ngOnInit(): void {
    this.passwordDialog = true;
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }

  btnCloseClick() {
    this.ref.close();
  }

  openKeyHolder() {
    this.isSitePassword = false;
    this.passwordDialog = false;
    this.keyHolderPassword = true;
    this.getKeyHolder();
  }

  getKeyHolder(otherParams?: object) {
    if (this.partitionId == null || this.partitionId == "null") {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER,
      undefined, false, prepareRequiredHttpParams(
        otherParams
      )
    ).subscribe((response: IApplicationResponse) => {
      this.keyHolderList = [];
      if (response.resources) {
        this.keyHolderList = response.resources;
        this.siteType = this.keyHolderList.filter(v => v.type == 'Customer');
        this.siteDropDown = this.siteType;
        if (this.siteDropDown.length > 0) {
          this.referenceDropId = this.siteType[0]?.id
          let otherParams = {
            LoggedInUserId: this.loggedUser.userId,
            ReferenceId: this.referenceDropId,
            Type: 'Customer',
            OccurrenceBookId: this.OccurrenceBookId,
            IsClientTesting: false,
          }
          this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSWORD, null, false,
            prepareGetRequestHttpParams(null, null, otherParams))
            .subscribe((response: IApplicationResponse) => {
              if (response.resources) {
                this.sitePasswordPasswordList = response.resources;
              }
              this.rxjsService.setPopupLoaderProperty(false);
            });
        }
      }
    });
  }

  SitePassword() {
    this.openKeyHolder();
    this.isSitePassword = true;
    this.passwordDialog = false;
    this.keyHolderPassword = false;
  }

  getSitePassWord() {
    let otherParams = {
      LoggedInUserId: this.loggedUser.userId,
      ReferenceId: this.referenceDropId,
      Type: 'Customer',
      IsClientTesting: false,
    }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSWORD, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.KeyHolderPasswordList = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onChangeKeyHolder(event) {
    this.selectedKeyHolderId = event.value.id;
    this.Type = this.keyHolderList.find(v => v.id == event.value.id).type;
    let otherParams = {
      LoggedInUserId: this.loggedUser.userId,
      ReferenceId: this.selectedKeyHolderId,
      Type: this.Type,
      IsClientTesting: false,
    }
    if (this.isSitePassword) {
      otherParams['CustomerId'] = this.customerData;
    }
    if (this.OccurrenceBookId) {
      otherParams['OccurrenceBookId'] = this.OccurrenceBookId;
    } else {
      otherParams['IsClientTesting'] = true;
    }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSWORD, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.KeyHolderPasswordList = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  getPasswordList(occurrenceBookId) {
    let otherParams = {
      ReferenceId: this.selectedKeyHolderId ? this.selectedKeyHolderId : this.referenceDropId,
      distressWord: this.Selectedpassword,
      Type: this.Type ? this.Type : 'Customer',
      occurrenceBookId: occurrenceBookId,
      LoggedInUserId: this.loggedUser.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSEORD_VALIDATE, otherParams).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.isDistress = response.resources?.isDestress
        if (response.resources['valid']) {
          this.snackbarService.openSnackbar("Password confirmation successful", ResponseMessageTypes.SUCCESS);
          this.keyHolderPassword = false;
          this.Selectedpassword = null;
          this.isSitePassword = false;
          this.passwordCorrectDialog = true;
        } else {
          this.isSitePassword = false;
          this.keyHolderPassword = false;
          this.incorrectPasswordDialog = true;
        }
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  onSubmitKeyHolder(passwordList) {
    this.Selectedpassword = passwordList;
    this.getPasswordList(this.OccurrenceBookId);
  }

  onSubmitAcknowledge() {
    let data = {
      occurrenceBookId: this.OccurrenceBookId,
      acknowledgedBy: this.loggedUser.userId,
      keyholderId: this.selectedKeyHolderId,
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_ACKNOWLEGED_PASSWORD, data)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.incorrectPasswordDialog = false;
        this.isSitePassword = false;
        this.ref.close(this.OccurrenceBookId);
      }
    });
  }

  onEscalateDispatch() {
    this.passwordDialog = false;
    this.incorrectPasswordDialog = false;
    this.clientVehicleDialog = true;
  }

  onSubmitRequestDispatch() {
    let formValue = { occurrenceBookId: this.OccurrenceBookId, modifiedUserId: this.loggedUser.userId };
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_REQUEST_DISPATCH, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.clientVehicleDialog = false;
          this.ref.close(this.OccurrenceBookId);
        }
      });
  }

  cancelDispatch() {
    let data = {
      occurrenceBookId: this.OccurrenceBookId,
      modifiedUserId: this.loggedUser.userId
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_CANCELLED, data)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.passwordCorrectDialog = false;
        this.btnCloseClick();
      }
    });
  }
}
