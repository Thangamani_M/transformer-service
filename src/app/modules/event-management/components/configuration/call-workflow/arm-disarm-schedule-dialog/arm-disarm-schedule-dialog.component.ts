import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { ArmDisarmListModel, ArmDisarmModel } from '@modules/event-management/models/configurations/arm-disarm.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { RescheduleDialogComponent } from '../reschedule-dialog/reschedule-dialog.component';
@Component({
  selector: 'app-arm-disarm-schedule-dialog',
  templateUrl: './arm-disarm-schedule-dialog.component.html',
})
export class ArmDisarmScheduleDialogComponent implements OnInit {
  armDisarmDialog: boolean = false
  armDisarmForm: FormGroup
  armDisarmList: FormArray
  minDate: any = 0;
  loggedUser: any
  customerAddressId: any
  customerId = '';
  customerData: any;
  rescheduleModal: boolean = false;
  rescheduleForm: FormGroup;

  constructor(public dialogService: DialogService, public ref: DynamicDialogRef, private momentService: MomentService, private store: Store<AppState>, private rxjsService: RxjsService, public _fb: FormBuilder, public config: DynamicDialogConfig, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data
      });
    this.rxjsService.getCustomerId()
      .subscribe(data => {
        this.customerId = data
      });
  }

  ngOnInit(): void {
    this.getArmDisarmData();
    this.createArmDisarmForm();
    this.createRescheduleForm();
  }

  createArmDisarmForm(armDisarmModel?: ArmDisarmModel) {
    let actualTimingModelControl = new ArmDisarmModel(armDisarmModel);
    this.armDisarmForm = this._fb.group({
      armDisarmList: this._fb.array([])
    });
    Object.keys(actualTimingModelControl).forEach((key) => {
      this.armDisarmForm.addControl(key, new FormControl(actualTimingModelControl[key]));
    });
  }

  createRescheduleForm() {
    this.rescheduleForm = this._fb.group({
      rescheduleTime: [null, [Validators.required, Validators.min(1), Validators.max(15)]],
      modifiedUserId: [''],
      occurrenceBookId: ['']
    });
  }

  get getarmDisarmListArray(): FormArray {
    if (!this.armDisarmForm) return;
    return this.armDisarmForm.get("armDisarmList") as FormArray;
  }

  createarmDisarmListModel(armDisarmListModel: ArmDisarmListModel): FormGroup {
    let actualTimingListModelContol = new ArmDisarmListModel(armDisarmListModel);
    let formControls = {};
    Object.keys(actualTimingListModelContol).forEach((key) => {
      if (key == 'disarmStartTime' || key == 'disarmEndTime' || key == 'armStartTime' || key == 'armEndTime') {
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: false }]
      }
    });
    let formContrlsGroup = this._fb.group(formControls)
    formContrlsGroup.get('disarmStartTime').valueChanges.subscribe(val => {
      formContrlsGroup.get('disarmEndTime').setValue(null);
    })
    formContrlsGroup.get('armStartTime').valueChanges.subscribe(val => {
      formContrlsGroup.get('armEndTime').setValue(null);
    })
    return formContrlsGroup;
  }

  getArmDisarmData() {
    let otherParams = {}
    otherParams['OccurrenceBookId'] = this.config.data;
    this.armDisarmDialog = true;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ARM_DISARM_SCHEDULE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.createArmDisarmForm()
          if (response.resources.length > 0) {
            this.armDisarmList = this.getarmDisarmListArray;
            response.resources.forEach((actualTimingListModel: ArmDisarmListModel) => {
              actualTimingListModel.disarmStartTime = actualTimingListModel.disarmStartTime ? this.momentService.setTime(actualTimingListModel.disarmStartTime) : null,
                actualTimingListModel.disarmEndTime = actualTimingListModel.disarmEndTime ? this.momentService.setTime(actualTimingListModel.disarmEndTime) : null,
                actualTimingListModel.armStartTime = actualTimingListModel.armStartTime ? this.momentService.setTime(actualTimingListModel.armStartTime) : null,
                actualTimingListModel.armEndTime = actualTimingListModel.armEndTime ? this.momentService.setTime(actualTimingListModel.armEndTime) : null,
                actualTimingListModel.modifiedUserId = this.loggedUser.userId ? this.loggedUser.userId : ''
              this.armDisarmList.push(this.createarmDisarmListModel(actualTimingListModel));
            });
          }
        }
      });
  }

  onSubmitArmDisarm() {
    this.ref.close();
    this.dialogService.open(RescheduleDialogComponent, {
      header: 'Reschedule',
      width: '450px',
      data: this.config.data
    });
  }

  close() {
    this.ref.close();
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
