import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, NO_DATA_FOUND_MESSAGE, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-response-instructions-view-details',
  templateUrl: './response-instructions-view-details.component.html',
  styleUrls: ['./response-instructions-view-details.component.scss']
})
export class ResponseInstructionsViewDetailsComponent implements OnInit {
  @Input() occurrenceBookId = '';
  @Input() customerAddressId = '';
  @Input() customerId = '';
  @Input() config: any;
  loggedInUserData: LoggedInUserModel;
  responseInstrcutionDetails;
  NO_DATA_FOUND_MESSAGE=NO_DATA_FOUND_MESSAGE;

  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private router: Router,
    private store: Store<AppState>,
    private rxjsService: RxjsService, private httpCancelService: HttpCancelService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getResponseInstructionDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getResponseInstructionDetails() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONSE_INSTRUCTION, this.occurrenceBookId ? this.occurrenceBookId : this.config?.data?.occurrenceBookId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.responseInstrcutionDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmitAcknowledgement() {
    let formValue = { occurrenceBookId: this.occurrenceBookId, acknowledgedBy: this.loggedInUserData.userId };
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_ACKNOWLEDGEMENT_RESPONSE_INSTRUCTION, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.router.navigate(["/event-management/stack-operator/call-workflow-list/list"], { queryParams: { id: this.occurrenceBookId, customerId: this.customerId, customerAddressId: this.customerAddressId, tab: 2 } });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
