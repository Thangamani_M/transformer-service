import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResponseInstructionsViewDetailsComponent } from './response-instructions-view-details.component';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TooltipModule } from 'primeng/tooltip';
import { FormsModule } from '@angular/forms';
import { ResponseInstructionsDetailsComponent } from './response-instructions-details/response-instructions-details.component';
@NgModule({
  declarations: [ResponseInstructionsViewDetailsComponent,ResponseInstructionsDetailsComponent],
  imports: [
    CommonModule,
    ScrollPanelModule,
    TooltipModule,
    FormsModule
  ],
  exports:[ResponseInstructionsViewDetailsComponent],
  entryComponents: [ResponseInstructionsDetailsComponent]
})
export class ResponseInstructionsViewDetailsModule { }
