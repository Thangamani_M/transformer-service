import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-response-instructions-details',
  templateUrl: './response-instructions-details.component.html'
})
export class ResponseInstructionsDetailsComponent implements OnInit {

  constructor(public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,) { }

  ngOnInit(): void {
  }

}
