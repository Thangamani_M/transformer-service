
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudType, defaultPopupType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-event-signal-list',
  templateUrl: './event-signal-list.component.html',
})
export class EventSignalListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};
  zoneSuspendForm: FormGroup
  loggedUser: UserLogin;
  ZoneTime: any;
  timeDelay: any;
  TimeFormat: any;
  minDateTODate: Date;
  minDateTODate1: Date;
  minDateTODate2: Date;

  constructor(
    private rxjsService: RxjsService, private crudService: CrudService,
    private momentService: MomentService,
    private _fb: FormBuilder, private store: Store<AppState>,
    private snackbarService: SnackbarService,
    public config: DynamicDialogConfig, public ref: DynamicDialogRef,) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'occurrenceBookSignalId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableScrollable: true,
            checkBox: true,
            enableFieldsSearch: true,
            columns: [{ field: 'signalDateTime', header: 'Signal Time', width: '250px',isDateTime: true },
            { field: 'alarmType', header: 'Alarm', width: '150px' },
            { field: 'description', header: 'Alarm Desc', width: '150px' },
            { field: 'zoneType', header: 'Zone/User', width: '150px' },
            { field: 'zoneNo', header: 'Zone Desc', width: '150px' },
            { field: 'rowSignal', header: 'Raw Data', width: '150px', isDate: true },
            { field: 'decoder', header: 'Decoder', width: '150px' },
            { field: 'accountCode', header: 'Transmitter', width: '150px' },
            { field: '', header: '', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_SIGNAL_OCCURRENCE_BOOK,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            enableReloadBtn: true,
          }
        ]
      }
    }

    this.zoneSuspendForm = this._fb.group({
      createdUserId: [this.loggedUser.userId],
      occurrenceBookSignals: [[],],
      zoneSuspendToDateTime: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.getZoneSusPendDelayTime();
  }

  btnCloseClick() {
    this.ref.close();
  }

  getZoneSusPendDelayTime(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    const params = { customerId: this.config?.data?.customerId, CustomerAddressId: this.config?.data?.customerAddressId }
    otherParams = { ...otherParams, ...params };
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ZONE_SUSPEND_DELAY_MINS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ZoneTime = response.resources
        this.ZoneTime = response.resources
        this.timeDelay = response?.resources?.timeDelay
        if (this.timeDelay >= 60) {
          this.TimeFormat = this.timeDelay / 60

          if (24 >= this.TimeFormat) {
            this.minDateTODate = new Date();
            this.minDateTODate1 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate());
            this.minDateTODate2 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate(), this.minDateTODate.getHours() + this.TimeFormat);
          }
          else {
            let dateTime = this.TimeFormat / 24
            this.minDateTODate = new Date();
            this.minDateTODate1 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getMonth());
            this.minDateTODate2 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate() + dateTime, this.minDateTODate.getHours() + this.TimeFormat);
          }
        }
        else {
          this.minDateTODate = new Date();
          this.minDateTODate1 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate());
          this.minDateTODate2 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate(), this.minDateTODate.getHours(), this.minDateTODate + this.timeDelay);
        }
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
      occurrenceBookId: this.config?.data?.occurrenceBookId,
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.rxjsService.setPopupLoaderProperty(false);
      this.loading = false;
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {});
    }
  }

  onSelectedRows(e) {
    this.selectedRows = e;
  }

  onSubmit() {
    if (this.zoneSuspendForm.invalid) {
      return;
    }
    let formValue = this.zoneSuspendForm.value;
    var occurrenceBookSignals = [];
    formValue.createdUserId = formValue.createdUserId;
    formValue.zoneSuspendToDateTime = formValue.zoneSuspendToDateTime;
    this.selectedRows.forEach(element => {
      occurrenceBookSignals = element['occurrenceBookSignalId']
      formValue.occurrenceBookSignals.push(occurrenceBookSignals);
    });
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ZONE_SUSPEND_POST_API, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.zoneSuspendForm.reset();
        this.ref.close();
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
