import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { clearFormControlValidators, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
@Component({
  selector: 'shots-fired-client-dialog-component',
  templateUrl: './shots-fired-client-dialog.component.html',
})
export class ShotsFiredClientDialogComponent implements OnInit {
  addMessageForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private store: Store<AppState>, 
    public config: DynamicDialogConfig, public ref: DynamicDialogRef,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
    });

  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.createAddForm()
  }
  createAddForm() {
    this.addMessageForm = this._fb.group({
      createdUserId: [this.loggedInUserData.userId],
      occurrenceBookId: [this.config.data.occurrenceBookId ? this.config.data?.occurrenceBookId : null, Validators.required],
      isInjuriesSustained: ['false', Validators.required],
      injuryDescription: ['', Validators.required],
      isMedicalAssistanceRequired: ['false', Validators.required],
      isCustomerOnSite: ['false', Validators.required],
      isFiredByResponseOfficer: [''],
      isFiredBySuspects: [''],
      isFiredByClient: [''],
      isFiredByOthers: [''],
      comments: ['', Validators.required],
    })
    this.addMessageForm.get('isInjuriesSustained').valueChanges.subscribe((isInjuriesSustained: any) => {
      if (isInjuriesSustained == 'true') {
        this.addMessageForm = setRequiredValidator(this.addMessageForm, ['injuryDescription']);
      }
      else {
        this.addMessageForm = clearFormControlValidators(this.addMessageForm, ["injuryDescription"]);
      }
    });
  }
  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onSubmit() {
    if (this.addMessageForm.invalid) {
      return
    }
    let formValue = this.addMessageForm.value
    formValue.isInjuriesSustained = formValue.isInjuriesSustained == 'true' ? true : false
    formValue.isMedicalAssistanceRequired = formValue.isMedicalAssistanceRequired == 'true' ? true : false
    formValue.isCustomerOnSite = formValue.isCustomerOnSite == 'true' ? true : false
    formValue.isFiredByResponseOfficer = formValue.isFiredByResponseOfficer == 'true' ? true : false
    formValue.isFiredBySuspects = formValue.isFiredBySuspects == 'true' ? true : false
    formValue.isFiredByClient = formValue.isFiredByClient == 'true' ? true : false
    formValue.isFiredByOthers = formValue.isFiredByOthers == 'true' ? true : false
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SHOTS_FIRED_CLIENT, this.addMessageForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.addMessageForm.reset()
          this.rxjsService.setGlobalLoaderProperty(false);
          this.ref.close(true);
        }
      })
  }

  btnCloseClick() {
    this.ref.close(false);
  }

}

