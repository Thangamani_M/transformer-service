import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { clearFormControlValidators, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest } from 'rxjs';


@Component({
  selector: 'shots-fired-hub-dialog-component',
  templateUrl: './shots-fired-hub-dialog.component.html',
})
export class ShotsFiredHubDialogComponent implements OnInit {
  addMessageForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  addressList: any = [];

  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private store: Store<AppState>,
    public config: DynamicDialogConfig, public ref: DynamicDialogRef,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {


    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
    });

  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.createAddForm()
  }
  createAddForm() {
    this.addMessageForm = this._fb.group({
      createdUserId: [this.loggedInUserData.userId],
      occurrenceBookId: [this.config.data?.occurrenceBookId ? this.config.data?.occurrenceBookId : null, Validators.required],
      isInjuriesSustained: ['false', Validators.required],
      injuryDescription: ['', Validators.required],
      isMedicalAssistanceRequired: ['false', Validators.required],
      isFiredByResponseOfficer: [''],
      isFiredBySuspects: [''],
      isFiredByClient: [''],
      isFiredByOthers: [''],
      comments: ['', Validators.required],
      locationBuildingNumber: [''],
      locationBuildingName: [''],
      locationStreetTypeId: [''],
      locationStreetNumber: [''],
      locationStreetAddress: [''],
      locationSuburbName: [''],
      locationCityName: [''],
      locationProvinceName: [''],
      locationPostalCode: [''],
      locationLongitude: [''],
      locationLatitude: [''],
      locationFullAddress: ['', Validators.required],
      locationJSONAddress: [''],
    })
    this.addMessageForm.get('isInjuriesSustained').valueChanges.subscribe((isInjuriesSustained: any) => {
      if (isInjuriesSustained == 'true') {
        this.addMessageForm = setRequiredValidator(this.addMessageForm, ['injuryDescription']);
      }
      else {
        this.addMessageForm = clearFormControlValidators(this.addMessageForm, ["injuryDescription"]);
      }
    });
  }
  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onSearchAddress(address) {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ADDRESS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        SearchText: address.query,
        IsAfrigisSearch: false,
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.addressList = res.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSelectAddress(event) {
    this.addMessageForm.get('locationLatitude').patchValue(event.latitude)
    this.addMessageForm.get('locationLongitude').patchValue(event.longitude)

    this.addMessageForm.get('locationBuildingNumber').patchValue(event.buildingNo)
    this.addMessageForm.get('locationBuildingName').patchValue(event.buildingName)
    this.addMessageForm.get('locationStreetTypeId').patchValue(event.addressId)
    this.addMessageForm.get('locationStreetNumber').patchValue(event.streetNo)
    this.addMessageForm.get('locationStreetAddress').patchValue(event.streetName)
    this.addMessageForm.get('locationSuburbName').patchValue(event.suburbName)
    this.addMessageForm.get('locationCityName').patchValue(event.cityName)
    this.addMessageForm.get('locationProvinceName').patchValue(event.provinceName)
    this.addMessageForm.get('locationPostalCode').patchValue(event.postalCode)
  }


  onSubmit() {
    if (this.addMessageForm.invalid) {
      this.addMessageForm.markAllAsTouched();
      return
    }
    let formValue = this.addMessageForm.value
    formValue.isInjuriesSustained = formValue.isInjuriesSustained == 'true' ? true : false
    formValue.isMedicalAssistanceRequired = formValue.isMedicalAssistanceRequired == 'true' ? true : false
    formValue.isFiredByResponseOfficer = formValue.isFiredByResponseOfficer == 'true' ? true : false
    formValue.isFiredBySuspects = formValue.isFiredBySuspects == 'true' ? true : false
    formValue.isFiredByClient = formValue.isFiredByClient == 'true' ? true : false
    formValue.isFiredByOthers = formValue.isFiredByOthers == 'true' ? true : false
    formValue.locationFullAddress = formValue.locationFullAddress.fullAddress;
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SHOTS_FIRED_HUB, this.addMessageForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.addMessageForm.reset()
          this.rxjsService.setGlobalLoaderProperty(false);
          this.ref.close(true);
        }
      })

  }

  btnCloseClick() {
    this.ref.close(false);
  }

}

