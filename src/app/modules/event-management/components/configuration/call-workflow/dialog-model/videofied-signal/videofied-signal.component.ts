import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PhonerFeedbackMessageComponent } from '../phoner-feedback-message/phoner-feedback-message.component';
@Component({
  selector: 'videofied-signal-component',
  templateUrl: './videofied-signal.component.html'
})
export class VideofiedSignalComponent implements OnInit {
  videoFiedSignalForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  // get the component instance to have access to plyr instance
  // or get it from plyrInit event
  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private store: Store<AppState>,
    public dialogRef: MatDialogRef<PhonerFeedbackMessageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.createAddForm()
  }
  createAddForm() {
    this.videoFiedSignalForm = this._fb.group({
      createdUserId: [''],
      occurrenceBookId: [''],
      chatMessage: ['', Validators.required],
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onSubmit() {
    if (this.videoFiedSignalForm.invalid) {
      return
    }
    let formValue = this.videoFiedSignalForm.value
    formValue.createdUserId = this.loggedInUserData.userId;
    formValue.occurrenceBookId = this.data.occurrenceBookId;
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_CHAT_OCCURANCE_BOOK, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.videoFiedSignalForm.reset()
          this.rxjsService.setGlobalLoaderProperty(false);
          this.dialogRef.close(true);
        }
      })

  }

  close() {
    this.dialogRef.close(false);
  }

}

