import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-exist-dispatch-dialog',
  templateUrl: './exist-dispatch-dialog.component.html',
})
export class ExistDispatchDialogComponent implements OnInit {
  showDialogSpinner: boolean = false;
  data: any = [];
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
  }
  close() {
    this.ref.close(false);
  }

}
