import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reschedule-dialog',
  templateUrl: './reschedule-dialog.component.html',
})
export class RescheduleDialogComponent implements OnInit {

  loggedUser: any
  customerAddressId: any
  customerId: any;
  customerData: any;
  rescheduleModal: boolean = false;
  rescheduleForm: FormGroup;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private httpCancelService: HttpCancelService, public ref: DynamicDialogRef, private momentService: MomentService, private store: Store<AppState>, private rxjsService: RxjsService, public _fb: FormBuilder, public config: DynamicDialogConfig, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data


      })

    this.rxjsService.getCustomerId()
      .subscribe(data => {
        this.customerId = data

      })

  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createRescheduleForm();
  }

  createRescheduleForm() {
    this.rescheduleForm = this._fb.group({
      rescheduleTime: [null, [Validators.required]],
      modifiedUserId: [''],
      occurrenceBookId: ['']
    })
  }

  onSubmitReschedule() {
    if (this.rescheduleForm.invalid) {
      return;
    }
    let formValue = this.rescheduleForm.value;
    formValue.modifiedUserId = this.loggedUser.userId;
    formValue.occurrenceBookId = this.config.data;
    formValue['rescheduleTime'] = this.momentService.convertTwelveToTwentyFourTime(this.rescheduleForm.value.rescheduleTime);

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ARM_DISARM_SCHEDULE_OCCURENCE_BOOK, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
        this.ref.close();
      }
    })
  }

  close() {
    this.ref.close();
  }

}
