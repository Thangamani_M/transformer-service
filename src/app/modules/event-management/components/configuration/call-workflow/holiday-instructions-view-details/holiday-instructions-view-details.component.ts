import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-holiday-instructions-view-details',
  templateUrl: './holiday-instructions-view-details.component.html',
})
export class HolidayInstructionsViewDetailsComponent implements OnInit {
  @Input() occurrenceBookId = ''
  loggedInUserData: LoggedInUserModel;
  holidayInstructionDetails;

  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private store: Store<AppState>, public dialog: MatDialog,
    private rxjsService: RxjsService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.openHolidayInstructionDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onSubmitAcknowledgement() {
    let formValue = { occurrenceBookId: this.occurrenceBookId, acknowledgedBy: this.loggedInUserData.userId };
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_ACKNOWLEDGEMENT_HOLIDAY_INSTRUCTION, formValue)
      .subscribe(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openHolidayInstructionDetails() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.HOLIDAY_TEMP_INSTRUCTION_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        OccurrenceBookId: this.occurrenceBookId
      })).subscribe((res) => {
        if (res.resources && res.isSuccess && res.statusCode == 200) {
          this.holidayInstructionDetails = res.resources[0];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
