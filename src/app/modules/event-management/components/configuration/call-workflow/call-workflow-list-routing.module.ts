import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallWorkFlowListComponent } from './call-workflow-list.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: CallWorkFlowListComponent, data: { title: 'Call Workflow' },canActivate: [AuthGuard]  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CallWorkFlowListRoutingModule { }
