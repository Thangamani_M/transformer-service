import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-call-workflow-list-dashboard',
  templateUrl: './call-workflow-list-dashboard.component.html',
})
export class CallWorkflowListDashboardComponent implements OnInit {
  selectedTabIndex = 0
  selectedMainTabIndex = 0;
  occurrenceBookId = '';
  customerId = '';
  customerAddressId = '';
  primengTableConfigProperties: any;
  partitionId = '';

  constructor(private activatedRoute: ActivatedRoute, 
    private router: Router) {
    this.primengTableConfigProperties = {
      tableCaption: "Holiday/Temp Instructions",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Operator Dashboard', relativeRouterUrl: '' }, { displayName: 'Events Management Platform', relativeRouterUrl: '/event-management/stack-operator' }, { displayName: 'Holiday/Temp Instructions' }],
      tableComponentConfigs: {
        mainTabsList: [
          {
            caption: 'Dispatch',
          },
        ],
        tabsList: [
          {
            caption: 'SIGNAL HISTORY',
          },
          {
            caption: 'ACTION & ARRIVALS',
          },
          {
            caption: 'CONTACTS NUMBERS',
          },
          {
            caption: 'EME NUMBERS',
          },
          {
            caption: 'EVENT MEMO',
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.activatedRoute.queryParams,
      this.activatedRoute.queryParamMap
    ]
    ).subscribe((response) => {
      if (response[0]) {
        this.occurrenceBookId = response[0].id;
        this.customerId = response[0].customerId;
        this.customerAddressId = response[0].customerAddressId;
        this.partitionId = response[0].partitionId;
      }
      if (response[1]) {
        this.selectedMainTabIndex = (Object.keys(response[1]['params']).length > 0) ? +response[1]['params']['tab'] : 0;
        this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
        this.onMainTabChange({ index: this.selectedMainTabIndex });
      }
    });
  }

  onMainTabChange(event) {
    this.router.navigate(['/event-management/stack-operator/call-workflow-list/list'], {
      queryParams: {
        id: this.occurrenceBookId, customerId: this.customerId,
        customerAddressId: this.customerAddressId, partitionId: this.partitionId, tab: event.index
      }
    });
  }
}
