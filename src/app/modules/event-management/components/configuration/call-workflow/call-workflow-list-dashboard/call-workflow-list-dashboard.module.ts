import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';
import { ResponseInstructionsViewDetailsModule } from '../response-instructions-view-details/response-instructions-view-details.module';
import { CallWorkflowListDashboardRoutingModule } from './call-workflow-list-dashboard-routing.module';
import { CallWorkflowListDashboardComponent } from './call-workflow-list-dashboard.component';
@NgModule({
  declarations: [CallWorkflowListDashboardComponent],
  imports: [
    CommonModule,
    CallWorkflowListDashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    ResponseInstructionsViewDetailsModule
  ]
})
export class CallWorkflowListDashboardModule { }
