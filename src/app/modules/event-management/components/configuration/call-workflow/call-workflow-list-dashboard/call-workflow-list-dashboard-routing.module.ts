import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallWorkflowListDashboardComponent } from './call-workflow-list-dashboard.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: CallWorkflowListDashboardComponent, data: { title: 'Call Workflow' },canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CallWorkflowListDashboardRoutingModule { }
