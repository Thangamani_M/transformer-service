import { CommonModule, DatePipe } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule, MAT_DATE_LOCALE } from '@angular/material';
import { SharedModule } from '@app/shared';
import { UsefulNumberContactRoutingModule } from './useful-numbers-contact.routing.module';
import { UsefulNumberContactComponent } from './useful-numbers-contact/useful-numbers-contact.component';

@NgModule({
  declarations: [UsefulNumberContactComponent],
  imports: [
    CommonModule,
    UsefulNumberContactRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTabsModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-EN' },
  { provide: MAT_DATE_LOCALE, useValue: 'en-EN' }, DatePipe]
})
export class UsefulNumberContactModule { }
