import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CommonPaginationConfig, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-ro-configuration-list',
  templateUrl: './ro-configuration-list.component.html',
  styleUrls: ['./ro-configuration-list.component.scss']
})
export class RoConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  status1: any = [];
  columnFilterForm: FormGroup;
  AutoDispacthForm: FormGroup;
  searchColumns: any
  dropDown: boolean = false;
  tabsClick: boolean = true;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  responseOfficerId: any;
  autodispachDetails: any;
  dataForExcel = []
  pageSize: number = 10;
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private httpCancelService: HttpCancelService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "RO Configurations ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configurations', relativeRouterUrl: '' }, { displayName: 'RO Accept Time' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'RO Accept Time',
            dataKey: 'roAcceptTimeId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' }, { field: 'mainArea', header: 'Main Area', width: '200px' }, { field: 'time', header: 'Time', width: '150px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.RO_ACCEPT_TIME,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Dispatch Exception',
            dataKey: 'dispatchExceptionId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'dispatchExceptionName', header: 'Dispatch Exception', width: '200px' }, { field: 'dispatchExceptionDivisions', header: 'Division', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'dispatchTime', header: 'Time', width: '200px' }, { field: 'distance', header: 'Distance', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DISPATCH_EXCEPTION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Feedback Offsite',
            dataKey: 'feedbackOffsiteId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'feedbackOffsiteName', header: 'Feedback Offsite', width: '200px' }, { field: 'feedbackOffsiteDivisions', header: 'Division', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'distance', header: 'Distance', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.FEEDBACK_OFFSITE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'FindU Dispatch',
            dataKey: 'findUDispatchId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'findUDispatchName', header: 'FindU Dispatch Name', width: '200px' }, { field: 'findUDispatchDivisions', header: 'Division', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.FINDU_DISPATCH,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Proactive Patrol',
            dataKey: 'proactivePatrolId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'proactivePatrolName', header: 'Proactive Patrol Name', width: '200px' }, { field: 'proactivePatrolDivisions', header: 'Division', width: '200px' }, { field: 'proactivePatrolTypes', header: 'Patrol Type', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.PROACTIVE_PATROL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Incidents',
            dataKey: 'incidentId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'incidentName', header: 'Incident Name', width: '200px' }, { field: 'incidentDivisions', header: 'Division', width: '200px' }, { field: 'incidentTypes', header: 'Incident Type', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.INCIDENT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Alarm Type Config',
            dataKey: 'alarmTypeConfigId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'alarmTypeConfigName', header: 'Alarm Type Name', width: '200px' }, { field: 'alarmTypeConfigDivisions', header: 'Division', width: '200px' }, { field: 'alarmTypeConfigList', header: 'Alarm Type', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.ALARM_TYPE_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Property Access',
            dataKey: 'propertyAccessId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'propertyAccessName', header: 'Property Access Name', width: '200px' }, { field: 'propertyAccessDivisions', header: 'Division', width: '200px' }, { field: 'propertyAccessReasonLists', header: 'Property Access Reason', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.PROPERTY_ACCESS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'CMC Feedback',
            dataKey: 'cmcFeedbackId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'cmcFeedbackName', header: 'CMC Feedback Name', width: '200px' }, { field: 'cmcFeedbackDivisions', header: 'Division', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CMC_FEEDBACK,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Decline Setup',
            dataKey: 'declineSetupId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'declineSetupName', header: 'Decline Setup', width: '200px' }, { field: 'declineSetupDivisions', header: 'Division', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'declineSetupReasons', header: 'Reason', width: '200px' }, { field: 'delayTime', header: 'Delay Time', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DECLINE_SETUP,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Check List',
            dataKey: 'vehicleCheckListId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'vehicleCheckListName', header: 'Check List Name', width: '200px' }, { field: 'vehicleCheckListDivisions', header: 'Division', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CHECK_LIST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Safety Alert',
            dataKey: 'safetyAlertConfigId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' }, { field: 'safetyAlertConfigMainAreas', header: 'Main Area', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SAFETY_ALERT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Time OnSite',
            dataKey: 'timeOnSiteId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' }, { field: 'timeOnSiteMainAreas', header: 'Main Area', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.TIME_ON_SITE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },

          {
            caption: 'RO Community Patrol',
            dataKey: 'roCommunityPatrolId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'roCommunityPatrolName', header: 'RO Community Patrol', width: '200px' }, { field: 'roCommunityPatrolDivisions', header: 'Division', width: '200px' }, { field: 'description', header: 'Description', width: '200px' }, { field: 'roCommunityPatrolTypes', header: 'RO Community Patrol Type', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.RO_COMMUNITY_PATROL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Reporting Config',
            dataKey: 'reportingConfigId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'reportingConfigName', header: 'Reporting Config', width: '200px' }, { field: 'reportingConfigDivisions', header: 'Division', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'measure1', header: 'Measure 1', width: '200px' }, { field: 'measure2', header: 'Measure 2', width: '200px' }, { field: 'measure3', header: 'Measure 3', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.REPORTING_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Auto Dispatch Configuration',
            dataKey: 'responseOfficerCurrentLogId',
            enableBreadCrumb: true,
            enableExportCSVSelected: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'mainArea', header: 'Main Area', width: '200px' },
            { field: 'subArea', header: 'Sub Area', width: '200px' },
            { field: 'responseOfficerName', header: 'Response Officer', width: '200px' },
            { field: 'occurrenceBookNumber', header: 'Occurrence Book', width: '200px' },
            { field: 'vehicleRegistrationNumber', header: 'Registration Number', width: '200px' },
            { field: 'status', header: 'Status', width: '200px' },
            { field: 'loginDateTime', header: 'Login Time', width: '200px' },
            { field: 'latitude', header: 'Latitude', width: '200px' },
            { field: 'longitude', header: 'Longitude', width: '200px' },
            { field: 'isAutoLogOut', header: 'Auto LogOut', width: '200px' },
            { field: 'isForceLogOut', header: 'Force LogOut', width: '200px' },
            { field: 'isManualLogOut', header: 'Manual LogOut', width: '200px' },
            { field: 'autoLogOutOnDateTime', header: 'Auto LogOut On', width: '200px' },
            { field: 'forceLogOutOnDateTime', header: 'Force LogOut On', width: '200px' },
            { field: 'isPDACommunicationActive', header: 'PDA Communication', width: '200px' },
            { field: 'isPTTCommunicationActive', header: 'PTT Communication', width: '200px' },
            { field: 'logDateTime', header: 'Log Date Time', width: '200px' },
            { field: 'manualLogOutOnDateTime', header: 'Manual Log Time', width: '200px' },
            { field: 'pdaCommunicationActiveOnDateTime', header: 'PDA Active On', width: '200px' },
            { field: 'pttCommunicationActiveOnDateTime', header: 'PTT Active On', width: '200px' },
            { field: 'pdaCommunicationFailedOnDateTime', header: 'PDA Failed On', width: '200px' },
            { field: 'statusUpdatedOn', header: 'Status Update On', width: '200px' },
            { field: 'imeiNumber', header: 'IMEI Number', width: '200px' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DEVICE_STATUS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableRefreshButton: true,
            enableLogoutButton: true,
            disabled: true
          },
          {
            caption: 'Long StandOff',
            dataKey: 'longStandOffId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' }, { field: 'longStandOffMainAreas', header: 'Main Area', width: '200px' }, { field: 'description', header: 'Description', width: '300px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.LONG_STANDOFF,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT, enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Open Area',
            dataKey: 'openAreaReasonId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'openAreaReason', header: 'Open Area Reason', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '150px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.OPEN_AREA_REASON,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT, enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Useful Numbers',
            dataKey: 'usefulNumberId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'groupName', header: 'Group Name', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '300px' },
            { field: 'usefulNumberDivisions', header: 'Useful Number Divisions', width: '300px' },
            { field: 'usefulNumberContacts', header: 'Useful Number Contacts', width: '300px' },
            { field: 'usefulNumberContactList', header: 'Useful Number ContactList', width: '300px' },
            { field: 'usefulNumberDivisionList', header: 'Useful Number DivisionList', width: '300px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.USEFUL_NUMBER,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableAddActionBtn: true,
            disabled: true
          },
        ]
      }
    }
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status1 = [
      { label: 'Yes', value: true },
      { label: 'No', value: false },
    ];
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    this.createSignalForm();
    this.getAutoDispachDetails();
    this.getRequiredListData();
  }

  createSignalForm() {
    this.AutoDispacthForm = this._fb.group({
      responseOfficerId: [''],
      forceLogOutBy: ['']
    });
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][EVENT_MANAGEMENT_COMPONENT.RO_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        let index = this.primengTableConfigProperties.tableComponentConfigs.tabsList.findIndex(x => x.disabled == false)
        if (index != 0) {
          this.selectedTabIndex = index
          this.router.navigate(['/event-management/ro-configuration'], { queryParams: { tab: this.selectedTabIndex } })
        }
      }
    });
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  refresh() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRefresh) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      if (data.isSuccess && data.resources && data.statusCode == 200) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
        this.loading = false;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/ro-configuration/ro-accept-time/add-edit");
            break;
          case 1:
            this.router.navigateByUrl("event-management/ro-configuration/dispatch-exception/add-edit");
            break;
          case 2:
            this.router.navigateByUrl("event-management/ro-configuration/feedback-offsite/add-edit");
            break;
          case 3:
            this.router.navigateByUrl("event-management/ro-configuration/find-u-dispatch/add-edit");
            break;
          case 4:
            this.router.navigateByUrl("event-management/ro-configuration/proactive-patrol/add-edit");
            break;
          case 5:
            this.router.navigateByUrl("event-management/ro-configuration/incidents/add-edit");
            break;
          case 6:
            this.router.navigateByUrl("event-management/ro-configuration/alarm-type-config/add-edit");
            break
          case 7:
            this.router.navigateByUrl("event-management/ro-configuration/property-access/add-edit");
            break;
          case 8:
            this.router.navigateByUrl("event-management/ro-configuration/cmc-feedback/add-edit");
            break;
          case 9:
            this.router.navigateByUrl("event-management/ro-configuration/decline-setup/add-edit");
            break;
          case 10:
            this.router.navigateByUrl("event-management/ro-configuration/check-list/add-edit");
            break;
          case 11:
            this.router.navigateByUrl("event-management/ro-configuration/safety-alert/add-edit");
            break;
          case 12:
            this.router.navigateByUrl("event-management/ro-configuration/time-on-site/add-edit");
            break;
          case 13:
            this.router.navigateByUrl("event-management/ro-configuration/ro-community-patrol/add-edit");
            break;
          case 14:
            this.router.navigateByUrl("event-management/ro-configuration/reporting-config/add-edit");
            break;
          case 15:
            break;
          case 16:
            this.router.navigateByUrl("event-management/ro-configuration/long-standoff/add-edit");
            break;
          case 17:
            this.router.navigateByUrl("event-management/ro-configuration/open-area/add-edit");
            break;
          case 18:
            this.router.navigateByUrl("event-management/ro-configuration/useful-numbers/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        let totalTabs = this.primengTableConfigProperties.tableComponentConfigs.tabsList.length;
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/ro-configuration/ro-accept-time/view"], { queryParams: { id: editableObject['roAcceptTimeId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 1:
            this.router.navigate(["event-management/ro-configuration/dispatch-exception/view"], { queryParams: { id: editableObject['dispatchExceptionId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 2:
            this.router.navigate(["event-management/ro-configuration/feedback-offsite/view"], { queryParams: { id: editableObject['feedbackOffsiteId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 3:
            this.router.navigate(["event-management/ro-configuration/find-u-dispatch/view"], { queryParams: { id: editableObject['findUDispatchId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 4:
            this.router.navigate(["event-management/ro-configuration/proactive-patrol/view"], { queryParams: { id: editableObject['proactivePatrolId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 5:
            this.router.navigate(["event-management/ro-configuration/incidents/view"], { queryParams: { id: editableObject['incidentId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 6:
            this.router.navigate(["event-management/ro-configuration/alarm-type-config/view"], { queryParams: { id: editableObject['alarmTypeConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 7:
            this.router.navigate(["event-management/ro-configuration/property-access/view"], { queryParams: { id: editableObject['propertyAccessId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 8:
            this.router.navigate(["event-management/ro-configuration/cmc-feedback/view"], { queryParams: { id: editableObject['cmcFeedbackId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 9:
            this.router.navigate(["event-management/ro-configuration/decline-setup/view"], { queryParams: { id: editableObject['declineSetupId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 10:
            this.router.navigate(["event-management/ro-configuration/check-list/view"], { queryParams: { id: editableObject['vehicleCheckListId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 11:
            this.router.navigate(["event-management/ro-configuration/safety-alert/view"], { queryParams: { id: editableObject['safetyAlertConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 12:
            this.router.navigate(["event-management/ro-configuration/time-on-site/view"], { queryParams: { id: editableObject['timeOnSiteId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 13:
            this.router.navigate(["event-management/ro-configuration/ro-community-patrol/view"], { queryParams: { id: editableObject['roCommunityPatrolId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 14:
            this.router.navigate(["event-management/ro-configuration/reporting-config/view"], { queryParams: { id: editableObject['reportingConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 15:
            break;
          case 16:
            this.router.navigate(["event-management/ro-configuration/long-standoff/view"], { queryParams: { id: editableObject['longStandOffId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 17:
            this.router.navigate(["event-management/ro-configuration/open-area/view"], { queryParams: { id: editableObject['openAreaReasonId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 18:
            this.router.navigate(["event-management/ro-configuration/useful-numbers/view"], { queryParams: { id: editableObject['usefulNumberId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
        }
    }
  }

  onTabChange(event) {
    if (event.index == 25) {
    } else {
      this.tables.forEach(table => {
        table.rows = +CommonPaginationConfig.defaultPageSize;
      });
      this.row = {};
      this.dropDown = false;
      this.tabsClick = true;
      this.columnFilterForm = this._fb.group({});
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
      this.columnFilterRequest();
      this.dataList = [];
      this.totalRecords = null;
      this.selectedTabIndex = event.index;
      this.router.navigate(['/event-management/ro-configuration'], { queryParams: { tab: this.selectedTabIndex } });
      this.getRequiredListData();
    }
  }

  updateLogOut() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canLogout) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.selectedRows.length == 0) {
      this.snackbarService.openSnackbar("Please select atleast one item", ResponseMessageTypes.WARNING);
      return;
    }
    let forValue = this.AutoDispacthForm.value;
    forValue.forceLogOutBy = this.loggedInUserData.userId;
    forValue.responseOfficerId = this.selectedRows[0].responseOfficerId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONCE_OFFICER_LOGOUT, forValue)
      .subscribe();
  }

  getAutoDispachDetails() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.AUTO_DISPACH_EXPORT, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.autodispachDetails = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  roConfigurationTabs(i) {
    let indexOf = this.primengTableConfigProperties.tableComponentConfigs.tabsList.findIndex(x => x.caption == i.value.caption)
    if (indexOf > -1) {
      this.selectedTabIndex = indexOf
      this.row = {}
      this.columnFilterForm = this._fb.group({})
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
      this.columnFilterRequest();
      this.dataList = [];
      this.totalRecords = null;
      this.router.navigate(['/event-management/ro-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    }
  }

  tabsChange() {
    this.dropDown = true;
    this.tabsClick = false;
  }

  exportExcel() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    import("xlsx").then(xlsx => {
      let worksheet = xlsx.utils.json_to_sheet(this.autodispachDetails);
      var wscols = [
        { wch: 15 }, { wch: 15 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 },
        { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 },
        { wch: 25 }, { wch: 25 }, { wch: 20 }, { wch: 20 }, { wch: 15 }, { wch: 15 },
      ];
      worksheet['!cols'] = wscols;
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
