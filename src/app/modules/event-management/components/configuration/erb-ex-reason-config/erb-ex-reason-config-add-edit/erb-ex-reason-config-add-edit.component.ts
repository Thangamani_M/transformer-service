import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ERBExReasonConfigAddEditModel } from '@modules/event-management/models/configurations/erb-ex-reason-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-erb-ex-reason-config-add-edit',
  templateUrl: './erb-ex-reason-config-add-edit.component.html',
})
export class ErbExReasonConfigAddEditComponent implements OnInit {

  erbExReasonConfigAddEditForm: FormGroup;
  viewable: boolean;
  userData: UserLogin;
  primengTableConfigProperties: any;
  id: any;
  btnName: any;
  erbExReasonConfigDetail: any;
  selectedTabIndex: any = 0;
  isSubmitted: boolean;
  totaltab = 0
  selectedTab = 0
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private router: Router,
    private activateRoute: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService) {
    this.activateRoute.queryParamMap.subscribe((params) => {
      this.id = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = (Object.keys(params['params']).length > 0) ? params['params']['totalTabs'] : '' || 40;
      this.totaltab = (Object.keys(params['params']).length > 0) ? params['params']['selectedTab'] : '';

      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.primengTableConfigProperties = {
      tableCaption: this.id && !this.viewable ? 'Update ERB Exclusion Reason Config' : this.viewable ? 'View ERB Exclusion Reason Config' : 'Add ERB Exclusion Reason Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '' }, { displayName: 'ERB Exclusion Reason Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 31 } }, { displayName: 'Add ERB Exclusion Reason Config', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    }
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    if (this.viewable) {
      this.erbExReasonConfigDetail = [
        { name: 'Reason Name', value: '' },
        { name: 'Description', value: '' },
        { name: 'Created On', value: '' },
        { name: 'Modified On', value: '' },
        { name: 'Created By', value: '' },
        { name: 'Modified By', value: '' },
      ]
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
      this.viewValue();
    }
  }

  initForm(erbExReasonConfigAddEditModel?: ERBExReasonConfigAddEditModel) {
    let erbExReasonConfigModel = new ERBExReasonConfigAddEditModel(erbExReasonConfigAddEditModel);
    this.erbExReasonConfigAddEditForm = this.formBuilder.group({});
    Object.keys(erbExReasonConfigModel).forEach((key) => {
      if (!this.viewable) {
        this.erbExReasonConfigAddEditForm.addControl(key, new FormControl(erbExReasonConfigModel[key]));
      }
    });
    if (!this.viewable) {
      this.erbExReasonConfigAddEditForm = setRequiredValidator(this.erbExReasonConfigAddEditForm, ["reasonName", "description"])
    }
    if (this.id && !this.viewable) {
      this.patchValue();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  viewValue() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REASON_CONFIG, this.id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources) {
          this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = response.resources.reasonName;
          if (this.viewable) {
            this.erbExReasonConfigDetail = [
              { name: 'Reason Name', value: response.resources.reasonName },
              { name: 'Description', value: response.resources.description },
              { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
              { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
              { name: 'Created By', value: response.resources.createdUserName },
              { name: 'Modified By', value: response.resources.modifiedUserName },
            ];
          } else if (this.id && !this.viewable) {
            this.erbExReasonConfigAddEditForm.patchValue({
              reasonName: response.resources.reasonName,
              description: response.resources.description,
            })
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.id && !this.viewable ? 'Update ERB Exclusion Reason Config' : this.viewable ? 'View ERB Exclusion Reason Config' : 'Add ERB Exclusion Reason Config';
    this.btnName = this.id ? 'Update' : 'Add';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.viewable ? 'View ERB Exclusion Reason Config' : this.id && !this.viewable ? 'Update ERB Exclusion Reason Config' : 'Add ERB Exclusion Reason Config';
  }

  patchValue() {
    this.viewValue();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/event-management/event-configuration/erb-exclusion-reason-config/add-edit'], { queryParams: { id: this.id, totalTabs:this.totaltab, selectedTab:this.selectedTab }, skipLocationChange: true })
  }

  onSubmit() {
    if (this.erbExReasonConfigAddEditForm?.invalid) {
      this.erbExReasonConfigAddEditForm.markAllAsTouched();
      return;
    }
    let reqObj = {
      reasonName: this.erbExReasonConfigAddEditForm.get('reasonName').value,
      description: this.erbExReasonConfigAddEditForm.get('description').value,
    }
    this.isSubmitted = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let api = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REASON_CONFIG, reqObj);
    if (this.id) {
      reqObj['modifiedUserId'] = this.userData?.userId;
      reqObj['ERBExclusionReasonConfigId'] = this.id;
      api = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REASON_CONFIG, reqObj);
    } else if (!this.id) {
      reqObj['createdUserId'] = this.userData?.userId;
    }
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess) {
        this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: 31 }, skipLocationChange: true })
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
}
