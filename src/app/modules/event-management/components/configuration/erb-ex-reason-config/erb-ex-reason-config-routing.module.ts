import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErbExReasonConfigAddEditComponent } from './erb-ex-reason-config-add-edit';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ErbExReasonConfigAddEditComponent, data: { title: 'ERB Exclusion Reason Config Add/Edit' } },
  { path: 'view', component: ErbExReasonConfigAddEditComponent, data: { title: 'ERB Exclusion Reason Config View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ERBExReasonConfigRoutingModule { }
