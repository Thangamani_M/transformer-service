import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { ErbExReasonConfigAddEditComponent } from './erb-ex-reason-config-add-edit';
import { ERBExReasonConfigRoutingModule } from './erb-ex-reason-config-routing.module';


@NgModule({
  declarations: [ErbExReasonConfigAddEditComponent],
  imports: [
    CommonModule,
    ERBExReasonConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class ERBExcReasonConfigModule { }
