import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { DeclineSetupModel, DeclineSetupReasonListModel } from '@modules/event-management/models/configurations/decline-setup.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-decline-setup-add-edit',
  templateUrl: './decline-setup-add-edit.component.html',
})
export class DeclineSetupAddEditComponent implements OnInit {
  declineSetupId: any
  divisionDropDown: any = [];
  declineSetupForm: FormGroup;
  declineSetupReasonList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  declineSetupDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  public colorCode: string = '#278ce2';
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.declineSetupId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.declineSetupId ? 'Update Decline Setup':'Add Decline Setup';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Decline Setup', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 9 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.declineSetupId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/decline-setup/view' , queryParams: {id: this.declineSetupId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createDeclineSetupForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.declineSetupId) {
      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let cmcSmsGroup = new DeclineSetupModel(response.resources);
        this.declineSetupDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.declineSetupDetails?.declineSetupName ? this.title +' - '+ this.declineSetupDetails?.declineSetupName : this.title + ' --', relativeRouterUrl: '' });
        this.declineSetupForm.patchValue(cmcSmsGroup);
        var declineSetupDivisionsList = [];
        response.resources.declineSetupDivisionsList.forEach(element => {
          declineSetupDivisionsList.push(element.divisionId);
        });
        this.declineSetupForm.get('declineSetupDivisionsList').setValue(declineSetupDivisionsList)
        this.declineSetupReasonList = this.getDeclineSetupReasonListArray;
        response.resources.declineSetupReasonList.forEach((declineSetupReasonListModel) => {
          declineSetupReasonListModel.DeclineSetupReasonListId = declineSetupReasonListModel.DeclineSetupReasonListId;
          this.declineSetupReasonList.push(this.createDeclineSetupReasonListModel(declineSetupReasonListModel));
        });
      })
    } else {
      this.declineSetupReasonList = this.getDeclineSetupReasonListArray;
      this.declineSetupReasonList.push(this.createDeclineSetupReasonListModel());
    }
  }

  createDeclineSetupForm(): void {
    let declineSetupModel = new DeclineSetupModel();
    this.declineSetupForm = this.formBuilder.group({
      declineSetupReasonList: this.formBuilder.array([]),
      delayTime: [null, [Validators.required, Validators.min(10), Validators.max(9999)]]
    });
    Object.keys(declineSetupModel).forEach((key) => {
      this.declineSetupForm.addControl(key, new FormControl(declineSetupModel[key]));
    });
    this.declineSetupForm = setRequiredValidator(this.declineSetupForm, ["declineSetupName", "description", "declineSetupDivisionsList"]);
    this.declineSetupForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.declineSetupForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getDeclineSetupReasonListArray(): FormArray {
    if (!this.declineSetupForm) return;
    return this.declineSetupForm.get("declineSetupReasonList") as FormArray;
  }

  //Create FormArray controls
  createDeclineSetupReasonListModel(declineSetupReasonListModel?: DeclineSetupReasonListModel): FormGroup {
    let declineSetupReasonListFormControlModel = new DeclineSetupReasonListModel(declineSetupReasonListModel);
    let formControls = {};
    Object.keys(declineSetupReasonListFormControlModel).forEach((key) => {
      if (key === 'declineSetupReasonListName' || key === 'DeclineSetupReasonListId' || key === 'colorCode') {
        formControls[key] = [{ value: declineSetupReasonListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.declineSetupId) {
        formControls[key] = [{ value: declineSetupReasonListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: declineSetupReasonListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.DECLINE_SETUP,
      this.declineSetupId
    );
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Decline Reason already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getDeclineSetupReasonListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.declineSetupReasonListName)) {
        duplicate.push(k.value.declineSetupReasonListName);
      }
      filterKey.push(k.value.declineSetupReasonListName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addDeclineSetupReason(): void {
    if (!this.onChange()) {
      return;
    }
    if (this.getDeclineSetupReasonListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.declineSetupReasonList = this.getDeclineSetupReasonListArray;
    let declineSetupReasonListModel = new DeclineSetupReasonListModel();
    this.declineSetupReasonList.insert(0, this.createDeclineSetupReasonListModel(declineSetupReasonListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeDeclineSetupReason(i: number): void {
    if (this.getDeclineSetupReasonListArray.controls[i].value.declineSetupReasonListName == '') {
      this.getDeclineSetupReasonListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getDeclineSetupReasonListArray.controls[i].value.DeclineSetupReasonListId && this.getDeclineSetupReasonListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DECLINE_SETUP_REASON,
          this.getDeclineSetupReasonListArray.controls[i].value.DeclineSetupReasonListId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getDeclineSetupReasonListArray.removeAt(i);
            }
            if (this.getDeclineSetupReasonListArray.length === 0) {
              this.addDeclineSetupReason();
            };
          });
      }
      else {
        this.getDeclineSetupReasonListArray.removeAt(i);

      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.declineSetupForm.controls.declineSetupDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.declineSetupForm.controls.declineSetupDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.declineSetupForm.controls.declineSetupDivisionsList.patchValue([]);
    }
  }

  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }
    if (this.declineSetupForm.invalid) {
      return;
    }
    let formValue = this.declineSetupForm.value;
    if (formValue.declineSetupDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.declineSetupDivisionsList = formValue.declineSetupDivisionsList.filter(item => item != '')
      formValue.declineSetupDivisionsList = formValue.declineSetupDivisionsList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.declineSetupId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DECLINE_SETUP, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DECLINE_SETUP, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=9');
      }
    })
  }

  onCRUDRequested(type){}

}
