import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-decline-setup-view',
  templateUrl: './decline-setup-view.component.html',
})
export class DeclineSetupViewComponent implements OnInit {

declineSetupId: any;
declineSetupDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any
declineSetupDetailsViewDetails: any
declineSetupReasonList: any;
totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  };

constructor(private activatedRoute: ActivatedRoute,private store: Store<AppState>,
   private router: Router, private rxjsService: RxjsService,
   private snackbarService:SnackbarService, private crudService: CrudService) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.declineSetupId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption: 'View Decline Setup',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
    { displayName: 'Decline Setup', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab:  9} }
      , { displayName: '', }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  };
  this.combineLatestNgrxStoreData();
  if (this.declineSetupId) {
    this.getdeclineSetupDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.declineSetupDetails = response.resources;
      this.onShowValue(response.resources);
      this.declineSetupReasonList = response.resources.declineSetupReasonList
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + response.resources.declineSetupName;
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}

combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.RO_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onShowValue(response?: any) {
  this.declineSetupDetailsViewDetails = [
    { name: 'Decline Setup Name', value: response?.declineSetupName },
    { name: 'Description', value: response?.description },
    { name: 'Division', value: response?.declineSetupDivisions },
    { name: 'Delay Time', value: response?.delayTime},
    { name: 'Created On', value: response?.createdDate, isDateTime: true },
    { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
    { name: 'Created By', value: response?.createdUserName },
    { name: 'Modified By', value: response?.modifiedUserName },
    { name: 'Status', value: response.isActive == true ? 'Active' : 'In-Active', statusClass: response.isActive == true ? "status-label-green" : 'status-label-red' },
  ]
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(['event-management/ro-configuration/decline-setup/add-edit'], { queryParams: { id: this.declineSetupId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
}

getdeclineSetupDetails(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.DECLINE_SETUP,
    this.declineSetupId
  );
}

}
