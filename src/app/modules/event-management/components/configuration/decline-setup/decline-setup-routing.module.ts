import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeclineSetupAddEditComponent } from './decline-setup-add-edit.component';
import { DeclineSetupViewComponent } from './decline-setup-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: DeclineSetupAddEditComponent, data: { title: 'DeclineSetup Add/Edit' } },
  { path: 'view', component: DeclineSetupViewComponent, data: { title: 'DeclineSetup View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DeclineSetupRoutingModule { }
