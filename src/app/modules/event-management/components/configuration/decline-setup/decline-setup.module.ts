import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { ColorPickerModule } from 'primeng/colorpicker';
import { DeclineSetupAddEditComponent } from './decline-setup-add-edit.component';
import { DeclineSetupRoutingModule } from './decline-setup-routing.module';
import { DeclineSetupViewComponent } from './decline-setup-view.component';

@NgModule({
  declarations: [DeclineSetupAddEditComponent, DeclineSetupViewComponent],
  imports: [
    CommonModule,
    DeclineSetupRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ColorPickerModule,
    MatSelectModule
  ]
})
export class DeclineSetupModule { }
