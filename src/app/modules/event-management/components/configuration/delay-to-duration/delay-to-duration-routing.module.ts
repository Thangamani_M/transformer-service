import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DelayToDurationAddEditComponent } from './delay-to-duration-add-edit.component';
import { DelayToDurationViewComponent } from './delay-to-duration-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: DelayToDurationAddEditComponent, data: { title: 'Delay To Duration Add/Edit' } },
  { path: 'view', component: DelayToDurationViewComponent, data: { title: 'Delay To Duration View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DelayToDurationRoutingModule { }
