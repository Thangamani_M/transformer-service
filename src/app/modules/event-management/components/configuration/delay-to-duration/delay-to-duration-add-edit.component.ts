import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { DelayToDurationModel } from '@modules/event-management/models/configurations/delay-to-duration.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-delay-to-duration-add-edit',
  templateUrl: './delay-to-duration-add-edit.component.html',
})
export class DelayToDurationAddEditComponent implements OnInit {

  delayDurationId: string;
  delayDurationDetails: any;
  divisionDropDown: any = [];
  delayDurationForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.delayDurationId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.delayDurationId ? 'Update Delay To Duration':'Add Delay To Duration';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Delay To Duration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 12 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.delayDurationId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/delay-to-duration/view' , queryParams: {id: this.delayDurationId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createDelayDurationForm();
    this.getDivisionDropDown();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.delayDurationId) {
      this.getDelayDurationDetailsById(this.delayDurationId);
      return
    }

  }

  createDelayDurationForm(): void {
    let delayToDurationModel = new DelayToDurationModel();
    // create form controls dynamically from model class
    this.delayDurationForm = this.formBuilder.group({
      maxDuration: [null, [Validators.required, Validators.min(10), Validators.max(9999),]]
    });
    Object.keys(delayToDurationModel).forEach((key) => {
      this.delayDurationForm.addControl(key, new FormControl(delayToDurationModel[key]));
    });
    this.delayDurationForm = setRequiredValidator(this.delayDurationForm, ["divisionId"]);
    this.delayDurationForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.delayDurationForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDelayDurationDetailsById(delayDurationId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DELAY_DURATION, delayDurationId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.delayDurationDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.delayDurationDetails?.divisionName ? this.title +' - '+ this.delayDurationDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
          this.delayDurationForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
  }
  onSubmit(): void {
    if (this.delayDurationForm.invalid) {
      return;
    }
    let formValue = this.delayDurationForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.delayDurationId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DELAY_DURATION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DELAY_DURATION, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=12');
      }
    })
  }

  onCRUDRequested(type){}

}
