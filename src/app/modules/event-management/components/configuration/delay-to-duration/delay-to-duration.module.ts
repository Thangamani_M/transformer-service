import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { DelayToDurationAddEditComponent } from './delay-to-duration-add-edit.component';
import { DelayToDurationRoutingModule } from './delay-to-duration-routing.module';
import { DelayToDurationViewComponent } from './delay-to-duration-view.component';

@NgModule({
  declarations: [DelayToDurationAddEditComponent, DelayToDurationViewComponent],
  imports: [
    CommonModule,
    DelayToDurationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DelayToDurationModule { }
