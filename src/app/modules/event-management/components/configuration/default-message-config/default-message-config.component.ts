import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { DefaultMessageConfigModel } from '@modules/event-management/models/configurations/default-message-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-default-message-config',
  templateUrl: './default-message-config.component.html',
})
export class DefaultMessageConfigComponent implements OnInit {

  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 4 } });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  defaultMessageConfigForm: FormGroup;
  defaultMessageConfigDetails: any;
  defaultMessagesDropDown: any = [];
  defaultmessageid: any;
  totaltab;
  selectedTab;
  @ViewChild('allSelectedDefaultMessages', { static: false }) public allSelectedDefaultMessages: MatOption;
  primengTableConfigProperties: any;
  title:string;

  constructor(public dialogService: DialogService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.defaultmessageid = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = this.activatedRoute.snapshot.queryParams.totaltab;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.defaultmessageid ? 'Update Default Messages':'Add Default Messages';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Default Messages', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 24 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.defaultmessageid){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/default-message-config/view' , queryParams: {id: this.defaultmessageid,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createDefaultMessageConfigForm();
    this.getDefualtMessages()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.defaultmessageid) {
      this.getDefaultMessageById(this.defaultmessageid)
    }
  }

  createDefaultMessageConfigForm(): void {
    let defaultMessageConfigModel = new DefaultMessageConfigModel();
    this.defaultMessageConfigForm = this.formBuilder.group({
    });
    Object.keys(defaultMessageConfigModel).forEach((key) => {
      this.defaultMessageConfigForm.addControl(key, new FormControl(defaultMessageConfigModel[key]));
    });
    this.defaultMessageConfigForm = setRequiredValidator(this.defaultMessageConfigForm, ["defaultMessage", "defaultMessageTypeMappings"]);
    if (this.defaultmessageid) {
      this.defaultMessageConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    }
    else {
      this.defaultMessageConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    }
  }

  getDefualtMessages() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DEFULAT_MESSAGE_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.defaultMessagesDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDefaultMessageById(defaultmessageid: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DEFULAT_MESSAGE_CONFIG, defaultmessageid, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.defaultMessageConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.defaultMessageConfigDetails?.defaultMessage ? this.title +' - '+ this.defaultMessageConfigDetails?.defaultMessage : this.title + ' --', relativeRouterUrl: '' });
          this.defaultMessageConfigForm.patchValue(response.resources)
          this.defaultMessageConfigForm.get('defaultMessageId').setValue(response.resources.defaultMessageId)
          this.defaultMessageConfigForm.get("")
          var defaultMessageTypeMappings = [];
          response.resources.defaultMessageTypeMappings.forEach(element => {
            defaultMessageTypeMappings.push(element.defaultMessageTypeId);
          });
          this.defaultMessageConfigForm.get('defaultMessageTypeMappings').setValue(defaultMessageTypeMappings)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleDefaultMessageOne(all) {
    if (this.allSelectedDefaultMessages.selected) {
      this.allSelectedDefaultMessages.deselect();
      return false;
    }
    if (this.defaultMessageConfigForm.controls.defaultMessageTypeMappings.value.length == this.defaultMessagesDropDown.length)
      this.allSelectedDefaultMessages.select();
  }

  toggleAllDefaultMessages() {
    if (this.allSelectedDefaultMessages.selected) {
      this.defaultMessageConfigForm.controls.defaultMessageTypeMappings
        .patchValue([...this.defaultMessagesDropDown.map(item => item.id), '']);
    } else {
      this.defaultMessageConfigForm.controls.defaultMessageTypeMappings.patchValue([]);
    }
  }

  onSubmit(): void {
    if (this.defaultMessageConfigForm.invalid) {
      return;
    }
    let formValue = this.defaultMessageConfigForm.value;
    if (formValue.defaultMessageTypeMappings[0]?.hasOwnProperty('defaultMessageTypeId')) {
      formValue = formValue;
    } else {
      formValue.defaultMessageTypeMappings = formValue.defaultMessageTypeMappings.filter(item => item != '')
      formValue.defaultMessageTypeMappings = formValue.defaultMessageTypeMappings.map(defaultMessageTypeId => ({ defaultMessageTypeId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.defaultmessageid) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DEFULAT_MESSAGE_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DEFULAT_MESSAGE_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.defaultMessageConfigForm.reset();
        this.router.navigateByUrl('/event-management/event-configuration?tab=24');
      }
    })
  };

  ngAfterViewInit() {
    this.cdr.detectChanges();
  };

  onCRUDRequested(type){}

}
