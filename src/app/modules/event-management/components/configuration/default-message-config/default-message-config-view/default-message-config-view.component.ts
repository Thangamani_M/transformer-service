import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, SnackbarService, MomentService, RxjsService, CrudService, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, ModulesBasedApiSuffix, IApplicationResponse, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-default-message-config-view',
  templateUrl: './default-message-config-view.component.html',
})
export class DefaultMessageConfigViewComponent implements OnInit {
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  defaultmessageid: string;
  messageDefalut:any;
  defaultMessageDetails: any;
  primengTableConfigProperties:any;
  totaltab = 0
  selectedTab = 0
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private router: Router, public momentService: MomentService, private rxjsService: RxjsService, private crudService: CrudService,  private store: Store<AppState>) {
    this.defaultmessageid = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.primengTableConfigProperties = {
      tableCaption: this.defaultmessageid && 'View Delay Dispatch',
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData()
    this.getDefaultMessageById(this.defaultmessageid);
  }
  
  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDefaultMessageById(defaultmessageid: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DEFULAT_MESSAGE_CONFIG, defaultmessageid, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.messageDefalut = response.resources
          this.defaultMessageDetails = response.resources;
          this.onShowValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.defaultMessageDetails = [
      { name: 'Default Message', value: response?.defaultMessage },
      { name: 'Default Message Type', value: response?.defaultMessageTypeName },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified By', value: response?.modifiedUserName },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
    ]
  }
  

  edit() {
    if (this.defaultmessageid) {
      if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['event-management/event-configuration/default-message-config/add-edit'], { queryParams: { id: this.defaultmessageid, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
    }
  }
}
