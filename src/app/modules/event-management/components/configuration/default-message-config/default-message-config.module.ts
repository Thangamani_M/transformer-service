import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { DefaultMessageConfigRoutingModule } from './default-message-config-routing.module';
import { DefaultMessageConfigComponent } from './default-message-config.component';
import { DefaultMessageConfigViewComponent } from './default-message-config-view/default-message-config-view.component';

@NgModule({
  declarations: [DefaultMessageConfigComponent, DefaultMessageConfigViewComponent],
  imports: [
    CommonModule,
    DefaultMessageConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class DefaultMessageConfigModule { }
