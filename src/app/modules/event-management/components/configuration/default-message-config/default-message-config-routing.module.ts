import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultMessageConfigViewComponent } from './default-message-config-view/default-message-config-view.component';
import { DefaultMessageConfigComponent } from './default-message-config.component';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: DefaultMessageConfigComponent, data: { title: 'Default Message Add/Edit' } },
  { path: 'view', component: DefaultMessageConfigViewComponent, data: { title: 'Default Message View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DefaultMessageConfigRoutingModule { }
