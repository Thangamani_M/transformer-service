import { CommonModule, DatePipe } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { KeyboardShortcutsModule } from 'ng-keyboard-shortcuts';
import { NgxPrintModule } from 'ngx-print';
import { StackHistoryRoutingModule } from './stack-history-routing.module';
import { StackHistoryDetailsComponent } from './stack-history/stack-history-details/stack-history-details.component';
import { StackHistoryModalComponent } from './stack-history/stack-history-modal.component';
import { StackHistoryComponent } from './stack-history/stack-history.component';
import { StackWinelandsSignalHistoryComponent } from './stack-winelands/signal-history/signal-history.component';
@NgModule({
  declarations: [StackHistoryComponent, StackHistoryModalComponent,StackHistoryDetailsComponent, StackWinelandsSignalHistoryComponent],

  imports: [
    CommonModule,
    StackHistoryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    MaterialModule,
    NgxPrintModule,
    KeyboardShortcutsModule.forRoot()
  ],
  exports: [ StackWinelandsSignalHistoryComponent,StackHistoryComponent, StackHistoryDetailsComponent],
  providers: [{ provide: LOCALE_ID, useValue: 'en-EN' },
  { provide: MAT_DATE_LOCALE, useValue: 'en-EN' }, DatePipe],
  entryComponents: [StackHistoryModalComponent, StackHistoryDetailsComponent]
})
export class StackHistoryModule { }
