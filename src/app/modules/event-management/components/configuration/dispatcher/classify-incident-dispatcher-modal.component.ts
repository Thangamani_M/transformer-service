import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudType, ResponseMessageTypes } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'classify-incident-dispatcher-modal-component',
  templateUrl: './classify-incident-dispatcher-modal.component.html',
  styleUrls: ['./dispatcher-list.component.scss'],
})
export class ClassifyIncidentDispatcherModalComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  classifyIncidentModalForm: FormGroup;
  userData: any;
  occurrenceBookId: any;
  dispatcherData: any;

  constructor(
    private rxjsService: RxjsService, private snackbarService: SnackbarService, private crudService: CrudService, private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
    private momentService: MomentService, private httpCancelService: HttpCancelService, private formBuilder: FormBuilder, private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'incidentTypeId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'incidentTypeName', header: 'Incident' },
            { field: 'description', header: 'Description' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CLASSIFY_INCIDENT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            enableReloadBtn: true,
            enableExportCSV: false,
          }
        ]
      }
    }
    this.occurrenceBookId = this.data?.data?.occurrenceBookId
  }

  ngOnInit(): void {
    this.createIncidentForm();
    this.dispatcherData = this.data?.data;
    this.getRequiredListData();
  }

  createIncidentForm() {
    this.classifyIncidentModalForm = this.formBuilder.group({
      createdUserId: [this.userData.userId],
      occurrenceBookId: [this.occurrenceBookId ? this.occurrenceBookId : null, Validators.required],
      incidentTypeId: [null, Validators.required],
    })
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
      occurrenceBookId: this.data?.data?.occurrenceBookId,
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};

        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onSubmit(): void {
    if (this.selectedRows.length == 0) {
      return this.snackbarService.openSnackbar('Please select atleast one Incident', ResponseMessageTypes.WARNING)
    }
    let incidentTypeIds: any = []
    this.selectedRows.forEach(element => {
      incidentTypeIds.push(element['incidentTypeId'])
    })
    this.classifyIncidentModalForm.get('incidentTypeId').setValue(incidentTypeIds)
    if (this.classifyIncidentModalForm.invalid) {
      this.classifyIncidentModalForm.markAllAsTouched();
      return;
    }
    let formValue = this.classifyIncidentModalForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_INCIDENT_TYPE, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.dialog.closeAll();
        }
      })
    this.rxjsService.setDialogOpenProperty(false);
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e
  }

}

