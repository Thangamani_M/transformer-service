import { ChangeDetectorRef, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CommonPaginationConfig, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SELECT_REQUIRED_SIGNAL, setRequiredValidator, SignalRTriggers, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { SessionService } from '@app/shared/services/session.service';
import { SignalrConnectionService } from '@app/shared/services/signalr-connection.service';
import { HubConnection } from '@aspnet/signalr';
import { QuickAddServiceCallDialogComponent } from '@modules/customer/components/customer/customer-management/quick-add-service-call-dialog/quick-add-service-call-dialog.component';
import { PasswordPopupComponent } from '@modules/event-management/components/configuration/call-workflow/password-popup/password-popup.component';
import { FilterFormModel } from '@modules/event-management/models/configurations/filter-form.model';
import { PrintStackModel } from '@modules/event-management/models/configurations/print-stack.model';
import { SharedClientAssitanceComponent } from '@modules/event-management/shared/components';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DASHBOARD, EVENT_MANAGEMENT_COMPONENT, SERVICES, SHORTCUT_KEYS } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { ShortcutInput } from 'ng-keyboard-shortcuts';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { BreakRequestModel, ResponseOfficerDetails, VehicleShiftChangeRequestModel } from '../../../models/configurations/break-request-modal.model';
import { ArmDisarmScheduleDialogComponent } from '../call-workflow/arm-disarm-schedule-dialog/arm-disarm-schedule-dialog.component';
import { EventSignalListComponent } from '../call-workflow/event-signal-list/event-signal-list.component';
import { ResponseInstructionsDetailsComponent } from '../call-workflow/response-instructions-view-details/response-instructions-details/response-instructions-details.component';
import { StackHistoryDetailsComponent } from '../stack-history/stack-history-details/stack-history-details.component';
import { LogawayConfirmPopupComponent } from '../stack-winelands/popup-models/logaway-confirm-popup/logaway-confirm-popup.component';
import { StackDispachPartitionDetailsComponent } from '../stack-winelands/stack-dispach-partition-details/stack-dispach-partition-details.component';
import { StackDispachPatrolDetailsComponent } from '../stack-winelands/stack-dispach-patrol-details/stack-dispach-patrol-details.component';
import { MonitoringUserTypes } from '../stack-winelands/stack-winelands.component';
import { ThirdPartyGenerateUrlComponent } from '../third-party-details/third-party-generate-url.component';
import { AddMessageModalComponent } from './add-message-modal.component';
import { ClassifyIncidentDispatcherModalComponent } from './classify-incident-dispatcher-modal.component';
import { DispatcherListModalComponent } from './dispatcher-list-modal.component';
declare var google;
@Component({
  selector: 'app-dispatcher-list',
  templateUrl: './dispatcher-list.component.html',
  styleUrls: ['./dispatcher-list.component.scss']
})
export class DispatcherListComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  responseOfficerBreakId;
  selectedTabIndex = 0;
  primengTableConfigProperties;
  dataList = [];
  dataListScroll = [];
  loading = true;
  status: any = [];
  selectedColumns: any[];
  selectedRows;
  totalRecords;
  pageLimit = [20, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today = new Date();
  searchColumns;
  findUDialog = false;
  panicAppDialog = false;
  row = {};
  signalData;
  filterForm: FormGroup
  showFilterForm = false;
  scrollEnabled = false;
  areaDropDown = [];
  stackDropDown = [];
  queryParamsData;
  areaName = '';
  stackName = '';
  filterData;
  holidayInstructionDialog = false;
  operatorInstructionsDialog = false;
  breakRequestDialog = false;
  breakRequestForm: FormGroup;
  shiftChangeRequestForm: FormGroup;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  responseOfficerDetails;
  isSelected = true;
  isSelectedShiftChangeRequest = true;
  eventMemoList;
  openOperatorInstructionDetails;
  isShowVehicleStatusList = false;
  isShowEventMemo = false;
  moveToStackDialog = false;
  occurrenceBookId = '';
  moveToStackDetails;
  moveToStackForm: FormGroup;
  callMeForm: FormGroup;
  memoForm: FormGroup;
  operatorInstructionDetails;
  callMeDetails: any = { isRespond: false };
  shiftChangeRequestDetails;
  shortcutKeyDialog = false;
  shortcuts: ShortcutInput[] = [];
  arriveDialog = false
  printDialog = false;
  memoDialog = false;
  supervisorMessageDialog = false;
  showServiceSubmenu = false;
  @ViewChild('content', { static: true }) content: ElementRef;
  arriveList = [];
  selectedArrive: any[] = [];
  arriveForm: FormGroup;
  options;
  openMapDialog = false;
  callMeDialog = false;
  shiftChangeRequestDialog = false;
  map;
  overlays;
  selectedRowIndex = 0;
  getMemoDetails;
  selectedOccurrenceBookId = '';
  printStackReportName = "";
  printStackForm: FormGroup
  oBookReportTemplateDropdownList = [];
  oBookReportTemplateList = [];
  selectedFieldsRow = [];
  occurrenceBookListReportTemplateId = '';
  selectedFieldsOBook = {};
  pageSize: number = 10;
  eventPrintData;
  refresh: number = 0;
  countDetails;
  pastText: string;
  selectedTabChangeIndex;
  isSignalInfoTabShow = false;
  isAlarmTabShow = false;
  isRoMessageTabShow = false;
  isStackAreaLoading = true
  isStackLoading = true;
  vehiclePanicDialog = false;
  falsePanicDialog = false;
  holidayInstructionDetails;
  findUDetails;
  panicAppDetails;
  startMonitoring;
  selectedRowIndexMessage;
  updateMessage;
  dispachersignalhistory = false;
  allPermissions = [];
  shortcutKeysPermissions = [];
  servicesPermissions = [];
  dataListSubject$ = new Subject();

  constructor(private crudService: CrudService, private sessionService: SessionService, private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute, private dialog: MatDialog, private cdr: ChangeDetectorRef,
    public dialogService: DialogService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private snackbarService: SnackbarService, private signalrConnectionService: SignalrConnectionService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {
    this.queryParamsData = {};
    this.map = google?.maps?.Map;
    this.queryParamsData['stackAreaConfigId'] = this.sessionService.getItem('stackAreaConfigId') ? this.sessionService.getItem('stackAreaConfigId') : this.activatedRoute.snapshot.queryParams.stackAreaConfigId
    this.queryParamsData['stackConfigId'] = this.sessionService.getItem('stackConfigId') ? this.sessionService.getItem('stackConfigId') : this.activatedRoute.snapshot.queryParams.stackConfigId
    this.areaName = this.activatedRoute.snapshot.queryParams.name;
    this.stackName = this.activatedRoute.snapshot.queryParams.stackName;
    if (this.queryParamsData['stackAreaConfigId']) {
      this.rxjsService.setStackAreaConfigId(this.queryParamsData['stackAreaConfigId']);
    }
    this.primengTableConfigProperties = {
      tableCaption: "Dispatch",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dashboard', relativeRouterUrl: '' }, { displayName: 'Event Management', relativeRouterUrl: '' }, { displayName: 'Dispatcher View' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dispatch',
            dataKey: 'occurrenceBookId',
            enableBreadCrumb: true,
            enableScrollable: true,
            checkBox: true,
            rowExpantable: true,
            rowExpantableIndex: 0,
            columns: [{ field: 'displayName', header: 'Client Name', width: '150px' },
            { field: 'highPriority', header: 'Priority', width: '70px' },
            { field: 'signalTime', header: 'Time', width: '70px' },
            { field: 'subAreaName', header: 'Area', width: '70px' },
            { field: 'operator', header: 'Operator', width: '100px' },
            { field: 'allocatedVehicles', header: 'Allocated', width: '100px' },
            { field: 'dispatchedVehicles', header: 'Dispatch', width: '100px' },
            { field: 'vehicleStatus', header: 'Vehicle Status', width: '190px' },
            { field: 'vehicleArrivedTimes', header: 'Arrive', width: '110px' },
            { field: 'incident', header: 'Incident', width: '70px' },
            { field: 'message', header: 'Message', width: '150px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DISPATCHER_STACK_VIEW,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  setMap(event) {
    this.map = event.map;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.signalrConnectionService.hubConnection();
    this.hubConnectionDispatcherStackLiveDataConfig();
    this.getAllDropdowns();
    this.createBreakRequestForm();
    this.createShiftChangeRequestForm();
    this.createArriveForm();
    this.createMemoForm();
    this.createMoveToStackForm();
    this.createCallMeForm();
    this.createFilterForm();
    this.responseOfficerDetails = new ResponseOfficerDetails();
    this.isShowVehicleStatusList = true;
    this.selectedTabIndex = 0;
    if (this.queryParamsData?.stackAreaConfigId) {
      this.filterForm.get('stackAreaConfigId').setValue(this.queryParamsData?.stackAreaConfigId ? this.queryParamsData?.stackAreaConfigId : '');
      this.filterForm.get('stackConfigId').setValue(this.queryParamsData?.stackConfigId ? this.queryParamsData?.stackConfigId : '');
      this.getRequiredListData(this.queryParamsData);
    }
    this.options = {
      center: {
        lat: -37.6878,
        lng: 176.1651
      },
      zoom: 11,
      mapTypeId: google?.maps?.MapTypeId?.ROADMAP
    };
    if (!this.queryParamsData?.stackAreaConfigId) {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(true);
    }
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.startMonitoring = setInterval(() => {
      this.onStackMonitoring()
    }, 10000);
    this.rxjsService.getSelctedRowIndex().subscribe((data: any) => {
      this.selectedRowIndexMessage = data;
    });
    this.onFormControlChanges();
  }

  onFormControlChanges() {
    this.filterForm.get('stackAreaConfigId').valueChanges.subscribe((stackAreaConfigId: string) => {
      this.areaName = this.areaDropDown.find(e => e.id == stackAreaConfigId)?.displayName;
    });
    this.filterForm.get('stackConfigId').valueChanges.subscribe((stackConfigId: string) => {
      this.stackName = this.stackDropDown.find(e => e.id == stackConfigId)?.displayName;
    });
  }

  hubConnectionInstanceForSalesAPI: HubConnection;
  hubConnectionDispatcherStackLiveDataConfig() {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
      // real data after the each signal's action the from stack
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.DispatcherStackViewChangesTrigger, data => {
        let receivingUser = data?.receivingUsers?.find(d => d.receivingUserId == this.loggedInUserData.userId.toLowerCase());
        if (receivingUser) {
          this.isStackLoading = receivingUser.isForcedStackDistribution;
          this.isStackAreaLoading = receivingUser.isForcedStackDistribution;
          if (data?.dispatcherStackViewChangedListItems) {
            data?.dispatcherStackViewChangedListItems?.forEach((dispatcherStackViewChangedListItem) => {
              let filteredOBRowIndex = this.dataList.findIndex(dL => dL['occurrenceBookId'] == dispatcherStackViewChangedListItem['occurrenceBookId']);
              if (filteredOBRowIndex > -1 && this.dataList.find(dL => dL['occurrenceBookId'] == dispatcherStackViewChangedListItem['occurrenceBookId'])) {
                this.dataList.splice(filteredOBRowIndex, 1, dispatcherStackViewChangedListItem);
              }
              else if (filteredOBRowIndex == -1) {
                this.dataList.push(dispatcherStackViewChangedListItem);
              }
            });
          }
          // Prefetch the stack counts and config ID's from the signalR
          if (data?.signals) {
            this.countDetails = data?.signals;
            this.filterForm.get('stackConfigId').setValue(data?.signals.stackConfigId ? data?.signals.stackConfigId :
              this.queryParamsData?.stackConfigId);
            this.filterForm.get('stackAreaConfigId').setValue(data?.signals.stackAreaConfigId ? data?.signals.stackAreaConfigId :
              this.queryParamsData?.stackAreaConfigId);
          }
          // Remove the stack from the dashboard
          data?.dispatcherStackViewRemovedListItems?.forEach((occurrenceBookId: string, index) => {
            let filteredOBRowIndex = this.dataList.findIndex(dL => dL['occurrenceBookId'] == occurrenceBookId);
            if (filteredOBRowIndex > -1) { // only splice array when item is found
              this.dataList.splice(filteredOBRowIndex, 1);
            }
            // Expand the very next stack once after the last stack is removed from the list
            if (data.dispatcherStackViewRemovedListItems.length == index + 1 && filteredOBRowIndex > -1) {
              this.selectedRows = this.dataList[(filteredOBRowIndex == this.dataList.length) ? filteredOBRowIndex - 1 : filteredOBRowIndex];
              this.sessionService.setItem('selectedStack', JSON.stringify(this.selectedRows));
              this.sessionService.setItem('selectedRowIndex', filteredOBRowIndex);
              this.onRowClickExpand(this.selectedRows, filteredOBRowIndex);
            }
          });
          this.dataListSubject$.next({ listUpdated: true });
        }
      });
      this.hubConnectionInstanceForSalesAPI.onclose(() => {
      });
    })
      .catch(error => console.error(error, "Sales API Error from dispatcher component..!!"));
  }

  sortTheStackByPriority() {
    // Sort by the priority field of the stack
    this.dataList = this.dataList.sort((prevObj, nextObj) => {
      if (prevObj.priority === nextObj.priority) {
        // signalDateTime is only important when priorities are the same
        return new Date(nextObj.signalDateTime).valueOf() - new Date(prevObj.signalDateTime).valueOf();
      }
      return prevObj.priority > nextObj.priority ? 1 : -1;
    });
    let selectedStackObject = JSON.parse(this.sessionService.getItem('selectedStack') ?
      this.sessionService.getItem('selectedStack') : "{}");
    if (Object.keys(selectedStackObject).length > 0) {
      if (selectedStackObject) {
        let foundStackIndexAfterSort = this.dataList.findIndex(dL => dL['occurrenceBookId'] == selectedStackObject.occurrenceBookId);
        this.selectedRows = this.dataList[foundStackIndexAfterSort];
        this.onRowClickExpand(this.selectedRows, foundStackIndexAfterSort);
      }
      else {
        this.onRowClickExpand(this.dataList[0], 0);
      }
    }
    else {
      this.onRowClickExpand(this.dataList[0], 0);
    }
    this.scrollToRowExpandedPosition();
  }

  stackDashboardRedirectionLogics() {
    if (this.countDetails?.monitoringUserType == MonitoringUserTypes.STACK_OPERATOR) {
      this.router.navigateByUrl("/event-management/stack-operator");
    }
    else if (this.countDetails?.monitoringUserType == MonitoringUserTypes.STACK_DISPATCHER) {
      this.router.navigateByUrl("/event-management/stack-dispatcher");
    }
    this.filterForm.get('stackConfigId').setValue(this.countDetails?.stackConfigId ? this.countDetails.stackConfigId : this.filterForm.value.stackConfigId);
    this.filterForm.get('stackAreaConfigId').setValue(this.countDetails?.stackAreaConfigId ? this.countDetails.stackAreaConfigId : this.filterForm.value.stackAreaConfigId);
  }

  onStackMonitoring() {
    if (!this.filterForm.value.stackAreaConfigId || !this.filterForm.value.stackConfigId) {
      return;
    }
    let data = {
      userId: this.loggedInUserData.userId,
      stackAreaConfigId: this.filterForm.value.stackAreaConfigId,
      stackConfigId: this.filterForm.value.stackConfigId,
      maximumRows: this.row['pageSize'],
      occurrenceBookId: this.selectedRows?.occurrenceBookId
    }
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_MONITORING_DISPATCHER, data).subscribe();
  }

  ngAfterViewInit() {
    // Whenever the table gets updated dynamically the below method calls so that we can scroll once data is loaded fully
    this.dataListSubject$?.subscribe(() => {
      this.sortTheStackByPriority();
    });
    this.shortcuts.push(
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F1,
        label: "Shortcut Keys",
        description: "Acknowledgement",
        command: () => {
          this.get_F1_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F2,
        label: "Shortcut Keys",
        description: "Stack View",
        command: () => {
          this.get_F2_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F3,
        label: "Shortcut Keys",
        description: "Arrive",
        command: () => {
          this.get_F3_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F4,
        label: "Shortcut Keys",
        description: "Contact",
        command: () => {
          this.get_F4_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F5,
        label: "Shortcut Keys",
        description: "On hold / Transfer",
        command: () => {
          this.get_F5_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F6,
        label: "Shortcut Keys",
        description: "Services",
        command: () => {
          this.get_F6_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F7,
        label: "Shortcut Keys",
        description: "Search",
        command: () => {
          this.get_F7_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F8,
        label: "Shortcut Keys",
        description: "Print",
        command: () => {
          this.get_F8_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F9,
        label: "Shortcut Keys",
        description: "Messages",
        command: () => {
          this.get_F9_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F10,
        label: "Shortcut Keys",
        description: "Memo",
        command: () => {
          this.get_F10_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F11,
        label: "Shortcut Keys",
        description: "Incident",
        command: () => {
          this.get_F11_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_F12,
        label: "Shortcut Keys",
        description: "Log away",
        command: () => {
          this.get_F12_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_ENTER,
        label: "Shortcut Keys",
        description: "Client History",
        command: () => {
          this.get_ENTER_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_V,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "Verify Password",
        command: () => {
          this.get_CTRL_V_ShorcutActions();
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_F,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "Find Signal",
        command: () => {
          this.get_CTRL_F_ShorcutActions();
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_HOME,
        label: "Shortcut Keys",
        description: "Top of Stack",
        command: () => {
          this.get_HOME_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_END,
        label: "Shortcut Keys",
        description: "Bottom of Stack",
        command: () => {
          this.get_END_ShorcutActions();
        },
        preventDefault: true
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_S,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "Supervisor message",
        command: () => {
          this.get_CTRL_S_ShorcutActions();
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_A,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "Active Stack",
        command: () => {
          this.get_CTRL_A_ShorcutActions();
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_H,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "History Stack",
        command: () => {
          this.get_CTRL_H_ShorcutActions();
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_G,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "Usefull Numbers",
        command: () => {
          this.get_CTRL_G_ShorcutActions();
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_R,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "Refresh",
        command: () => {
          this.get_CTRL_R_ShorcutActions();
          this.refresh++;
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_M,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "Map",
        command: () => {
          this.get_CTRL_M_ShorcutActions();
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_U,
        preventDefault: true,
        label: "Shortcut Keys",
        description: "Operator Traking",
        command: () => {
          this.get_CTRL_U_ShorcutActions();
        },
      },
      {
        key: EventMgntModuleApiSuffixModels.SHORTCUT_ESC,
        label: "Shortcut Keys",
        description: "Exit",
        command: () => {
          this.shortcutKeyDialog = !this.shortcutKeyDialog
        },
        preventDefault: true
      },
    );
  }

  getAllDropdowns() {
    this.getStackAreaConfigDropdown();
    this.getStackConfigDropdown();
  }

  getStackAreaConfigDropdown() {
    this.isStackAreaLoading = true
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_AREA_CONFIG,
      prepareGetRequestHttpParams(null, null, {
        isDefaultLoaderDisabled: true
      }))
      .subscribe((resp: IApplicationResponse) => {
        this.isStackAreaLoading = false;
        if (resp.isSuccess && resp.statusCode === 200) {
          this.areaDropDown = resp.resources;
          if (this.queryParamsData['stackAreaConfigId']) {
            this.rxjsService.setStackAreaConfigId(this.queryParamsData['stackAreaConfigId']);
            let areaNameData = this.areaDropDown.find(e => e.id == this.queryParamsData['stackAreaConfigId']);
            this.areaName = areaNameData.displayName;
          }
        }
      });
  }

  getStackConfigDropdown() {
    this.isStackLoading = true
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_CONFIG,
      prepareGetRequestHttpParams(null, null, {
        isDefaultLoaderDisabled: true
      }))
      .subscribe((resp: IApplicationResponse) => {
        this.isStackLoading = false;
        if (resp.isSuccess && resp.statusCode === 200) {
          this.stackDropDown = resp.resources;
          if (this.queryParamsData['stackAreaConfigId']) {
            this.rxjsService.setStackAreaConfigId(this.queryParamsData['stackAreaConfigId']);
            let stackNameData = this.stackDropDown.find(e => e.id == this.queryParamsData['stackConfigId']);
            this.stackName = stackNameData.displayName;
          }
        }
      });
  }

  scrollToRowExpandedPosition() {
    setTimeout(() => {
      let selectedStack = JSON.parse(this.sessionService.getItem('selectedStack'));
      const el = document.getElementById(selectedStack?.['occurrenceBookId']);
      el?.scrollIntoView({
        behavior: "smooth",
        block: "end"
      });
    });
  }

  isSelectedRow() {
    if (!this.selectedRows) {
      this.snackbarService.openSnackbar(SELECT_REQUIRED_SIGNAL, ResponseMessageTypes.WARNING);
      return false;
    } else {
      return true;
    }
  }

  onBreadCrumbClick(breadCrumbItem: object) {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([breadCrumbItem['relativeRouterUrl']], { queryParams: breadCrumbItem['queryParams'] });
    } else {
      this.router.navigateByUrl(breadCrumbItem['relativeRouterUrl']);
    }
  }

  createMoveToStackForm() {
    this.moveToStackForm = this._fb.group({
      occurrenceBookId: [''],
      modifiedUserId: [''],
      stackConfigId: []
    });
  }

  createCallMeForm() {
    this.callMeForm = this._fb.group({
      callMeId: [''],
      dispatcherId: ['']
    });
  }

  createBreakRequestForm() {
    let breakRequestModel = new BreakRequestModel();
    this.breakRequestForm = this._fb.group({
      breakDuration: [null, [Validators.required, Validators.min(1), Validators.max(99)]]
    });
    Object.keys(breakRequestModel).forEach((key) => {
      this.breakRequestForm.addControl(key, new FormControl(breakRequestModel[key]));
    });
    this.breakRequestForm = setRequiredValidator(this.breakRequestForm, ["reason"]);
    this.breakRequestForm.get('repliedBy').setValue(this.loggedInUserData.userId)
  }

  createShiftChangeRequestForm() {
    let vehicleShiftChangeRequestModel = new VehicleShiftChangeRequestModel();
    this.shiftChangeRequestForm = this._fb.group({
    });
    Object.keys(vehicleShiftChangeRequestModel).forEach((key) => {
      this.shiftChangeRequestForm.addControl(key, new FormControl(vehicleShiftChangeRequestModel[key]));
    });
    this.shiftChangeRequestForm = setRequiredValidator(this.shiftChangeRequestForm, ["comments"]);
    this.shiftChangeRequestForm.get('approvedBy').setValue(this.loggedInUserData.userId);
    this.shiftChangeRequestForm.get('responsedUserId').setValue(this.loggedInUserData.userId);
  }

  createFilterForm() {
    let filterFormModel = new FilterFormModel();
    this.filterForm = this._fb.group({
    });
    Object.keys(filterFormModel).forEach((key) => {
      this.filterForm.addControl(key, new FormControl(filterFormModel[key]));
    });
    this.filterForm = setRequiredValidator(this.filterForm, ["stackAreaConfigId", "stackConfigId"]);
  }

  submitFilter() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.filterForm.invalid) {
      this.filterForm.markAllAsTouched();
      return;
    }
    let filteredData = Object.assign({},
      { stackAreaConfigId: this.filterForm.get('stackAreaConfigId').value ? this.filterForm.get('stackAreaConfigId').value : '' },
      { stackConfigId: this.filterForm.get('stackConfigId').value ? this.filterForm.get('stackConfigId').value : '' },
      { onHoldStack: this.filterForm.get('onHoldStack').value ? this.filterForm.get('onHoldStack').value : '' },
    );
    let areaNameData = this.areaDropDown.find(e => e.id == this.filterForm.get('stackAreaConfigId').value);
    this.areaName = areaNameData.displayName
    let stackNameData = this.stackDropDown.find(e => e.id == this.filterForm.get('stackConfigId').value);
    this.stackName = stackNameData.displayName
    this.queryParamsData['stackAreaConfigId'] = this.filterForm.get('stackAreaConfigId').value
    this.queryParamsData['stackConfigId'] = this.filterForm.get('stackConfigId').value
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key];
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {})
    this.scrollEnabled = false;
    this.getRequiredListData(filterdNewData);
    this.showFilterForm = !this.showFilterForm;
    this.queryParamsData['stackAreaConfigId'] = this.filterForm.get('stackAreaConfigId').value ? this.filterForm.get('stackAreaConfigId').value : '';
    this.queryParamsData['stackConfigId'] = this.filterForm.get('stackConfigId').value ? this.filterForm.get('stackConfigId').value : '';
    this.sessionService.setItem('stackAreaConfigId', this.queryParamsData['stackAreaConfigId']);
    this.sessionService.setItem('stackConfigId', this.queryParamsData['stackConfigId']);
  }

  resetForm() {
    this.filterForm.reset();
    this.dataList = [];
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].rowExpantableIndex = 0;
    this.primengTableConfigProperties;
    this.getRequiredListData(this.queryParamsData);
    this.eventMemoList = [];
    this.selectedRows = {};
    this.showFilterForm = !this.showFilterForm;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.allPermissions = response[1][EVENT_MANAGEMENT_COMPONENT.DASHBOARD]
      if (this.allPermissions) {
        this.servicesPermissions = this.allPermissions.find(item => item.menuName == "Services")
        this.shortcutKeysPermissions = this.allPermissions.find(item => item.menuName == "Shortcut Keys")
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.allPermissions);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {});
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          this.scrollEnabled = false
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(otherParams?: object) {
    this.loading = true;
    otherParams['monitoringUserId'] = this.loggedInUserData.userId;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.scrollEnabled) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.getCountDetails();
      if (data.isSuccess) {
        if (!this.scrollEnabled) {
          this.dataList = [];
          this.dataList = data.resources;
        } else {
          data.resources.forEach(element => {
            this.dataList.push(element);
          });
        }
        if (this.selectedRowIndexMessage) {
          this.dataList = [];
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].rowExpantable = true;
          this.dataList = data.resources;
          if (Object.keys(this.sessionService.getItem('selectedStack') ?
            this.sessionService.getItem('selectedStack') : {}).length > 0) {
            this.getSelectedStackAndIndexFromSessionAndExpand();
          }
          else {
            this.onRowClickExpand(this.dataList[this.selectedRowIndexMessage], this.selectedRowIndexMessage);
          }
        }
        else {
          this.totalRecords = data.totalCount;
          this.row['pageIndex'] = this.row['pageIndex'] + 1;
          if (Object.keys(this.sessionService.getItem('selectedStack') ?
            this.sessionService.getItem('selectedStack') : {}).length > 0) {
            this.getSelectedStackAndIndexFromSessionAndExpand();
          }
          else {
            this.onRowClickExpand(this.dataList[0], 0);
          }
        }
      } else {
        data.resources = [];
        this.dataList = data.resources;
        this.totalRecords = 0;
        this.selectedRows = null;
      }
      this.dataListSubject$.next({ listUpdated: true });
    }, error => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
    this.cdr.detectChanges();
  }

  getSelectedStackAndIndexFromSessionAndExpand() {
    this.selectedRows = JSON.parse(this.sessionService.getItem('selectedStack'));
    this.selectedRowIndex = +this.sessionService.getItem('selectedRowIndex');
    this.onRowClickExpand(this.dataList[this.selectedRowIndex], this.selectedRowIndex);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string) {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
      case CrudType.FILTER:
        let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.FILTER);
        if (isAccessDeined) {
          return this.showPermissionRestricedMessage();
        }
        this.showFilterForm = !this.showFilterForm;
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string) {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/call-workflow/call-workflow-list/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/stack-winelands/call-workflow-list/list"], { queryParams: { id: editableObject['customerId'] } });
            break;
        }
    }
  }

  onTabChangeEvent(event) {
    this.selectedTabChangeIndex = event.index;
  }

  getCountDetails() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPACHER_STACK_SIGNAL_COUNT,
      prepareGetRequestHttpParams(null, null, {
        StackAreaConfigId: this.filterForm.value.stackAreaConfigId,
        StackConfigId: this.filterForm.value.stackConfigId,
        isDefaultLoaderDisabled: true
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.countDetails = response.resources;
        }
      });
  }

  navigateToStackOperator() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.filterForm.invalid) {
      this.filterForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setStackAreaConfigId(this.filterForm.value.stackAreaConfigId);
    this.sessionService.setItem('stackAreaConfigId', this.filterForm.value.stackAreaConfigId)
    this.sessionService.setItem('stackConfigId', this.filterForm.value.stackConfigId)
    this.router.navigate(["/event-management/stack-operator"], {
      queryParams: {
        stackAreaConfigId: this.filterForm.value.stackAreaConfigId,
        stackConfigId: this.filterForm.value.stackConfigId,
        name: this.areaName,
        stackName: this.stackName
      },
    });
  }

  onClickNewTab(a, rawData) {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.ACTION);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    this.responseOfficerBreakId = rawData.responseOfficerBreakId;
    this.responseOfficerDetails = rawData;
    if (rawData.typeName === "OccurrenceBook") {
      let data = {
        createdUserId: this.loggedInUserData.userId,
        occurrenceBookId: rawData.occurrenceBookId
      }
      this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_DISPATCHER, data).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          window.open(
            `event-management/stack-operator/call-workflow-list/list?id=${rawData.occurrenceBookId}&customerId=${rawData.customerId}&customerAddressId=${rawData.customerAddressId}&partitionId=${rawData?.partitionId}&tab=3`, "_blank");
        }
      });
    }
    else if (rawData.typeName === "VehicleCallMe") {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(true);
      this.getCallMeDetails(rawData.occurrenceBookId);
      this.callMeDialog = true;
      this.createCallMeForm();
    }
    else if (rawData.typeName === "VehicleShiftChangingRequest") {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(true);
      this.getShiftChangeRequestDetails(rawData.occurrenceBookId);
      this.shiftChangeRequestDialog = true;
      this.createShiftChangeRequestForm();
    }
    else if (rawData.typeName === "VehiclePanic") {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(true);
      this.getShiftChangeRequestDetails(rawData.occurrenceBookId);
      this.vehiclePanicDialog = true;
    }
    else {
      this.isSelected = true;
      this.breakRequestDialog = true;
      this.createBreakRequestForm();
    }
  }

  FalsePanicOpenDialog() {
    this.falsePanicDialog = true;
    this.vehiclePanicDialog = false;
  }

  dispachBacup() {
    this.vehiclePanicDialog = false;
    window.open(
      `event-management/stack-operator/call-workflow-list/list?id=${this.selectedRows.occurrenceBookId}&customerId=${this.selectedRows.customerId}&tab=3`, "_blank");
  }

  findUDilaogDetails(rowData) {
    if (rowData) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GET_FINDU_DETAILS, rowData.occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.findUDialog = true;
            this.findUDetails = response.resources;
          }
          else {
            this.findUDialog = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  PanicAppDialog(rowData) {
    if (rowData) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GET_PANIC_DETAILS, rowData.occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.panicAppDialog = true;
            this.panicAppDetails = response.resources;
          }
          else {
            this.panicAppDialog = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => {
      table.rows = 20;
    });
    this.row = {};
    this.columnFilterForm = this._fb.group({});
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(['/event-management/signal-management'], { queryParams: { tab: this.selectedTabIndex } });
    this.getRequiredListData();
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows) ? [this.selectedRows] : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string) {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  refreshStackDispatcher(refreshType?: string) {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.REFRESH);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (refreshType == 'manual refresh') {
      this.isLoading = true;
    }
    this.eventMemoList = undefined;
    if (!this.queryParamsData?.stackAreaConfigId) {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(true);
      const dialogReff = this.dialog.open(DispatcherListModalComponent, { width: '700px', disableClose: true });
      dialogReff.afterClosed().subscribe(result => {
      });
      dialogReff.componentInstance.outputAreaName.subscribe(name => {
        this.areaName = name;
      })
      dialogReff.componentInstance.outputStackName.subscribe(stack => {
        this.stackName = stack;
      })
      dialogReff.componentInstance.outputData.subscribe(ele => {
        if (ele) {
          this.queryParamsData['isDefaultLoaderDisabled'] = true;
          this.getRequiredListData(ele);
          this.queryParamsData = ele;
        }
      });
    } else {
      this.dataList = [];
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].rowExpantableIndex = 0;
      this.primengTableConfigProperties;
      if (this.filterForm.get('onHoldStack').value) {
        this.queryParamsData = { ...this.queryParamsData, onHoldStack: this.filterForm.get('onHoldStack').value };
        this.queryParamsData['isDefaultLoaderDisabled'] = true;
        this.getRequiredListData(this.queryParamsData);
      }
      this.queryParamsData['isDefaultLoaderDisabled'] = true;
      this.getRequiredListData(this.queryParamsData);
      this.refresh++;
      this.selectedTabChangeIndex = 0;
      this.isAlarmTabShow = false;
      this.isSignalInfoTabShow = false;
      this.isRoMessageTabShow = false;
      this.selectedRows = {};
    }
  }

  openIncidentModal(rowData) {
    this.dialog.open(ClassifyIncidentDispatcherModalComponent, { width: '700px', disableClose: true, data: { data: rowData } });
  }

  getUpdateMessage() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCHER_STACK_VIEW,
      undefined, null, prepareGetRequestHttpParams(null, null,
        { monitoringUserId: this.loggedInUserData.userId, OccurrenceBookId: this.selectedOccurrenceBookId }))
      .subscribe(response => {
        this.updateMessage = response.resources;
        if (this.updateMessage) {
          this.dataList[this.selectedRowIndexMessage].message = this.updateMessage[0]?.message;
          this.dataList = [...this.dataList];
          this.getEventMemoList(this.selectedRows?.occurrenceBookId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openMessageModal(rowData, selectedRowIndex) {
    const dialogReff = this.dialog.open(AddMessageModalComponent, { width: '700px', disableClose: true, data: rowData.occurrenceBookId });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
        this.rxjsService.setSelctedRowIndex(selectedRowIndex);
        this.getUpdateMessage();
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  openArriveModal(rowData) {
    this.getArriveList(rowData.occurrenceBookId);
    this.arriveDialog = true;
  }

  openHolidayInstructionDialog(row) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.HOLIDAY_TEMP_INSTRUCTION_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        OccurrenceBookId: row.occurrenceBookId
      }), 1).subscribe((res) => {
        if (res.resources && res.statusCode == 200 && res.isSuccess) {
          this.holidayInstructionDialog = true;
          this.holidayInstructionDetails = res.resources[0]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  escalationDispatch() {
    let data = {
      occurrenceBookId: this.selectedRows['occurrenceBookId'],
      modifiedUserId: this.loggedInUserData?.userId
    }
    this.crudService.update(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.HOLIDAY_TEMP_INSTRUCTION_ESCALATION, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.holidayInstructionDialog = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openOperatorInstructionsDialog(rowData) {
    if (rowData) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SPECIAL_INSTRUCTION_OCCURANCE_BOOK, rowData.occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
            this.openOperatorInstructionDetails = rowData;
            this.operatorInstructionsDialog = true;
            this.operatorInstructionDetails = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  openPartitionDialog(row) {
    if (this.selectedRows) {
      let data = {
        occurrenceBookId: this.selectedRows.occurrenceBookId,
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows.customerAddressId,
      }
      const dialogReff = this.dialogService.open(StackDispachPartitionDetailsComponent, { showHeader: false, data: { ...data, isPasswordDialog: true }, width: '750px' });
      dialogReff.onClose.subscribe(result => {
        if (result) {
          let data = this.dataList.findIndex(x => x.occurrenceBookId == result.occurrenceBookId)
          this.onRowClickExpand(this.dataList[data], data)
        }
      });

    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  patrolDetails() {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.PATROLS);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      let data = {
        occurrenceBookId: this.selectedRows.occurrenceBookId,
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows.customerAddressId,
      }
      const dialogReff = this.dialogService.open(StackDispachPatrolDetailsComponent, { showHeader: false, data: { ...data, }, width: '750px' });
      dialogReff.onClose.subscribe(result => {
        if (result) {
          let data = this.dataList.findIndex(x => x.occurrenceBookId == result.occurrenceBookId)
          this.onRowClickExpand(this.dataList[data], data)
        }
      });

    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  openDispacherSignalHistoryDialog() {
    this.dispachersignalhistory = true;
  }

  isSelectedRadio(e) {
    if (e.value) {
      this.isSelected = true;
      this.breakRequestForm.get('breakDuration').setValidators([Validators.required, Validators.min(1), Validators.max(99)]);
      this.breakRequestForm.controls['breakDuration'].updateValueAndValidity();
      this.breakRequestForm.controls['reason'].clearValidators()
      this.breakRequestForm.controls['reason'].updateValueAndValidity();
      this.breakRequestForm.controls['reason'].setValue('');
    } else {
      this.isSelected = false;
      this.breakRequestForm.get('reason').setValidators([Validators.required])
      this.breakRequestForm.controls['reason'].updateValueAndValidity();
      this.breakRequestForm.controls['breakDuration'].clearValidators()
      this.breakRequestForm.controls['breakDuration'].updateValueAndValidity();
      this.breakRequestForm.get('breakDuration').setValue('')
    }
  }


  openResponseInstructionDialog(row?: any) {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.RESPONSE_INSTRUCTIONS);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    this.rxjsService.setDialogOpenProperty(true)
    if (this.selectedRows) {
      let data = {
        occurrenceBookId: this.selectedRows.occurrenceBookId,
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows.customerAddressId,
        responseInstructionDialog: true
      }
      this.dialogService.open(ResponseInstructionsDetailsComponent, {
        showHeader: true,
        header: 'Response Instructions', styleClass: "customModal",
        data: { ...data, }, width: '950px'
      });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  onSubmit() {
    if (this.breakRequestForm.value.isRadioButton) {
      this.breakRequestForm.get('breakDuration').setValidators([Validators.required, Validators.min(1), Validators.max(99)]);
      this.breakRequestForm.controls['breakDuration'].updateValueAndValidity();
      this.breakRequestForm.controls['reason'].clearValidators()
      this.breakRequestForm.controls['reason'].updateValueAndValidity();
      this.breakRequestForm.controls['reason'].setValue('');
    }
    else {
      this.breakRequestForm.get('reason').setValidators([Validators.required])
      this.breakRequestForm.controls['reason'].updateValueAndValidity();
      this.breakRequestForm.controls['breakDuration'].clearValidators()
      this.breakRequestForm.controls['breakDuration'].updateValueAndValidity();
      this.breakRequestForm.get('breakDuration').setValue('')
    }
    if (this.breakRequestForm.invalid) {
      return;
    }
    if (this.breakRequestForm.get('isRadioButton').value) {
      delete this.breakRequestForm.value['reason'];
    }
    let formValue = this.breakRequestForm.value;
    formValue.responseOfficerBreakId = this.responseOfficerBreakId;
    let apiSuffix = this.breakRequestForm.get('isRadioButton').value ? EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_BREAK_APPROVED :
      EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_BREAK_DECLINED;
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, apiSuffix, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.breakRequestDialog = false;
        this.getRequiredListData(this.queryParamsData);
      }
    });
  }

  getOccurrenceBookMemo() {
    if (this.selectedRows) {
      let occurrenceBookId = this.selectedRows['occurrenceBookId']
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEMO_ID, occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.memoDialog = true;
            this.getMemoDetails = response.resources;
            this.getEventMemoList(this.selectedRows.occurrenceBookId);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  contactNavigation() {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.CONTACT_NUMBERS);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      window.open(
        `event-management/stack-operator/call-workflow-list/list?id=${this.selectedRows.occurrenceBookId}&customerId=${this.selectedRows.customerId}&customerAddressId=${this.selectedRows.customerAddressId}&contact=${this.rxjsService.setTabActinCallflow(2)}&tab=2`, "_blank");
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  logAway() {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.LOGAWAY);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      let clientName = this.selectedRows['displayName'];
      let obNumber = this.selectedRows['occurrenceBookNumber'];
      let occurrenceBookId = this.selectedRows['occurrenceBookId']
      this.dialogService.open(LogawayConfirmPopupComponent, {
        showHeader: true,
        header: 'Log Away Confirmation',
        baseZIndex: 1000,
        width: '450px',
        data: { clientName, obNumber, occurrenceBookId }
      });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  dispachFalsePanic() {
    let formValue = {
      clientName: this.selectedRows['displayName'],
      obNumber: this.selectedRows['occurrenceBookNumber'],
      occurrenceBookId: this.selectedRows['occurrenceBookId'],
      modifiedUserId: this.loggedInUserData?.userId
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_LOGAWAY, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.falsePanicDialog = false;
      }
    })
  }

  getEventMemoList(occurrenceBookId) {
    this.isLoading = true;
    this.eventMemoList = undefined;
    let otherParams = {};
    otherParams['isDefaultLoaderDisabled'] = true;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_EVENT_MEMO_REPORT,
      occurrenceBookId,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventMemoList = response.resources;
          this.isLoading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onClickThirdPartyLink(data) {
    const dialogReff = this.dialog.open(ThirdPartyGenerateUrlComponent, { width: '700px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  onRowSelect(event) {
    this.selectedRowIndex = event.index;
    this.sessionService.setItem('selectedStack', JSON.stringify(this.dataList[this.selectedRowIndex]));
    this.sessionService.setItem('selectedRowIndex', this.selectedRowIndex);
    if (event.data.occurrenceBookId) {
      this.selectedOccurrenceBookId = event.data.occurrenceBookId;
      this.isSignalInfoTabShow = true;
      this.isAlarmTabShow = true;
      this.isRoMessageTabShow = true;
      this.selectedTabChangeIndex = 0;
      this.getEventMemoList(event.data.occurrenceBookId)
    }
    if (event.data.typeName === "OccurrenceBook") {
      if (this.isShowEventMemo) {
        this.getEventMemoList(event.data.occurrenceBookId)
      }
    }
  }

  onRowUnselect(event) {
    this.selectedRowIndex = null
    this.eventMemoList = [];
    this.isSignalInfoTabShow = false;
    this.isAlarmTabShow = false;
    this.isRoMessageTabShow = false;
  }

  onRowClickExpand(rowData, rowIndex?, fromType = 'from ts') {
    if (fromType == 'from template') {
      // Store in session where it can be retrieved whenever the manual refresh or real data changes in the stack
      this.sessionService.setItem('selectedStack', JSON.stringify(rowData));
      this.sessionService.setItem('selectedRowIndex', rowIndex);
    }
    if (rowData) {
      this.selectedRowIndex = rowIndex;
      this.selectedRows = rowData;
      // Store in session where it can be retrieved whenever the manual refresh or real data changes in the stack
      this.sessionService.setItem('selectedStack', JSON.stringify(rowData));
      this.sessionService.setItem('selectedRowIndex', rowIndex);
      let selectedStack = JSON.parse(this.sessionService.getItem('selectedStack'));
      if (!selectedStack || (selectedStack && selectedStack?.occurrenceBookId !== rowData.occurrenceBookId) || this.eventMemoList?.length == 0 || !this.eventMemoList) {
        this.getEventMemoList(rowData.occurrenceBookId);
      }
      if (rowData.occurrenceBookId) {
        this.selectedOccurrenceBookId = rowData.occurrenceBookId;
        this.isSignalInfoTabShow = true;
        this.isAlarmTabShow = true;
        this.isRoMessageTabShow = true;
        this.selectedTabChangeIndex = 0;
      }
      if (rowData.typeName === "OccurrenceBook") {
        if (this.isShowEventMemo) {
          this.getEventMemoList(rowData.occurrenceBookId);
        }
      }
    } else {
      this.selectedRows = null;
      this.selectedOccurrenceBookId = null;
      this.eventMemoList = [];
      this.occurrenceBookId = null;
    }
  }

  onRowClickCollapse() {
    this.selectedRowIndex = null;
    this.eventMemoList = [];
    this.isSignalInfoTabShow = false;
    this.isAlarmTabShow = false;
    this.isRoMessageTabShow = false;
  }

  vehicle = true;
  event = false;
  isLoading = false;
  onChangeVehicleEvent(status) {
    if (this.dataList?.length == 0 || !this.dataList) {
      return;
    }
    if (status == "vehicle status") {
      let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.VEHICLE_STATUS);
      if (isAccessDeined) {
        return this.showPermissionRestricedMessage();
      }
      this.isShowVehicleStatusList = true;
      this.vehicle = false;
      this.event = true;
      this.isShowEventMemo = false;
    } else {
      let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.EVENT_MEMO);
      if (isAccessDeined) {
        return this.showPermissionRestricedMessage();
      }
      this.isShowEventMemo = true;
      this.event = false;
      this.vehicle = true;
      this.isShowVehicleStatusList = false;
      let selectedStack = JSON.parse((this.sessionService.getItem('selectedStack') ?
        this.sessionService.getItem('selectedStack') : '{}'));
      if (selectedStack?.occurrenceBookId) {
        this.getEventMemoList(selectedStack.occurrenceBookId);
      }
    }
  }
  // Move To Stack
  openMoveToStackModal() {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.MOVE_TO_STACK);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      let occurrenceBookId = this.selectedRows['occurrenceBookId']
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MOVE_TO_STACK, occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.moveToStackDialog = true;
            this.moveToStackDetails = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  transferToStack() {
    if (this.moveToStackForm.invalid) {
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    let data = {
      occurrenceBookId: this.moveToStackDetails.occurrenceBookId,
      modifiedUserId: this.loggedInUserData.userId,
      stackConfigId: this.moveToStackForm.value.stackConfigId,
    }
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_MOVE, data).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.moveToStackDialog = false;
        this.moveToStackForm.reset
        this.selectedRows = {};
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  onRightClick(event) {
    event.preventDefault();
    if (this.dataList.length > 0) {
      this.shortcutKeyDialog = true;
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  getArriveList(id) {
    let otherParams = {};
    otherParams['OccurrenceBookId'] = id;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_RESPONSE_OFFICER_VEHICLE_DISPATCHED_OR_LIST,
      undefined,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.arriveList = response.resources
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createArriveForm() {
    this.arriveForm = this._fb.group({
      occurrenceBookId: [''],
      responseOfficerChatMessage: [''],
      createdUserId: [this.loggedInUserData.userId],
      responseOfficerId: [[], Validators.required],
    });
  }

  createMemoForm() {
    this.memoForm = this.formBuilder.group({
      createdUserId: [Validators.required],
      occurrenceBookId: [Validators.required],
      memo: ['', Validators.required],
      isActives: [true]
    });
  }

  onSubmitArrive() {
    if (this.selectedArrive.length == 0) {
      return;
    }
    let formValue = this.arriveForm.value;
    formValue.occurrenceBookId = this.selectedRows.occurrenceBookId;
    this.selectedArrive.forEach(element => {
      formValue.responseOfficerId.push(element.responseOfficerId);
    });
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_RESPONSE_OFFICER_VEHICLE_ARRIVED_REQUEST, formValue).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.arriveDialog = false;
      }
    });
  }

  openMap(rowData) {
    this.options = {};
    this.overlays = {};
    this.openMapDialog = true;
    this.options = {
      center: { lat: Number(rowData?.currentLatitude), lng: Number(rowData?.currentLongitude) },
      zoom: 12
    };
    setTimeout(() => {
      this.map.setCenter({
        lat: Number(rowData?.currentLatitude),
        lng: Number(rowData?.currentLongitude)
      });
    }, 500);
    this.overlays = [
      new google.maps.Marker({ position: { lat: Number(rowData?.currentLatitude), lng: Number(rowData?.currentLongitude) }, icon: "assets/img/map-icon.png", title: rowData.displayName }),
      new google.maps.Marker({ position: { lat: Number(rowData?.latitude), lng: Number(rowData?.longitude) }, icon: "assets/img/map-icon.png", title: rowData.displayName })
    ];
  }

  getCallMeDetails(occurrenceBookId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CALL_ME, occurrenceBookId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.callMeDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getShiftChangeRequestDetails(occurrenceBookId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SHIFT_CHNAGING, occurrenceBookId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.shiftChangeRequestDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmitCallMe() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    let data = this.callMeForm.value;
    data.callMeId = this.callMeDetails.callMeId;
    data.dispatcherId = this.loggedInUserData.userId;
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CALL_ME, data)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.callMeDialog = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  isShiftChangeRequest(e) {
    if (e.value) {
      this.isSelectedShiftChangeRequest = true;
      this.shiftChangeRequestForm.controls['comments'].clearValidators();
      this.shiftChangeRequestForm.controls['comments'].updateValueAndValidity();
      this.shiftChangeRequestForm.controls['comments'].setValue('');
    } else {
      this.isSelectedShiftChangeRequest = false;
      this.shiftChangeRequestForm.get('comments').setValidators([Validators.required]);
      this.shiftChangeRequestForm.controls['comments'].updateValueAndValidity();
    }
  }

  onSubmitShiftChangeRequest() {
    if (this.shiftChangeRequestForm.value.isApproved) {
      this.shiftChangeRequestForm.controls['comments'].clearValidators();
      this.shiftChangeRequestForm.controls['comments'].updateValueAndValidity();
      this.shiftChangeRequestForm.controls['comments'].setValue('');
    }
    else {
      this.shiftChangeRequestForm.get('comments').setValidators([Validators.required]);
      this.shiftChangeRequestForm.controls['comments'].updateValueAndValidity();
    }
    if (this.shiftChangeRequestForm.invalid) {
      return;
    }
    if (this.shiftChangeRequestForm.get('isApproved').value) {
      delete this.shiftChangeRequestForm.value['comments'];
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    let formValue = this.shiftChangeRequestForm.value;
    formValue.shiftChangingId = this.shiftChangeRequestDetails.shiftChangingId;
    formValue.approvedBy = this.loggedInUserData.userId;
    let apiSuffix = this.shiftChangeRequestForm.get('isApproved').value ? EventMgntModuleApiSuffixModels.SHIFT_CHNAGING_APROVED :
      EventMgntModuleApiSuffixModels.SHIFT_CHNAGING_DECLINED;
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, apiSuffix, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
        this.shiftChangeRequestDialog = false;
        this.getRequiredListData(this.queryParamsData);
      }
    });
  }

  openPasswordModal() {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.PASSWORD);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      let data = {
        LoggedInUserId: this.loggedInUserData.userId,
        occurrenceBookId: this.selectedRows.occurrenceBookId,
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows.customerAddressId,
        partitionId: this.selectedRows?.partitionId
      }
      const dialogReff = this.dialogService.open(PasswordPopupComponent, { showHeader: false, data: { ...data, isPasswordDialog: true }, width: '500px' });
      dialogReff.onClose.subscribe(result => {
        if (result) {
          let data = this.dataList.findIndex(x => x.occurrenceBookId == this.selectedRows.occurrenceBookId)
          this.onRowClickExpand(this.dataList[data], data);
          this.getEventMemoList(this.selectedRows.occurrenceBookId);
        }
      });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  captureEmailAndCell() {
    let isAccessDeined = this.getServicePermissionByActionIconType(SERVICES.CAPTURE_EMAIL_AND_CELL);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (!this.isSelectedRow()) return '';

    const ref = this.dialogService.open(SharedClientAssitanceComponent, {
      showHeader: true,
      header: 'Client Assitance',
      baseZIndex: 10000,
      width: '650px',
      data: { occurrenceBookId: this.selectedOccurrenceBookId }
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
    });
  }

  opensuspendSignalDialogModal() {
    let isAccessDeined = this.getServicePermissionByActionIconType(SERVICES.TEMP_SUSPEND_SIGNAL);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (!this.isSelectedRow()) return '';
    if (this.selectedRows) {
      this.dialogService.open(EventSignalListComponent, { showHeader: false, data: { ...this.selectedRows, }, width: '750px' });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  showSelectRequiredSignalMessage() {
    return this.snackbarService.openSnackbar(SELECT_REQUIRED_SIGNAL, ResponseMessageTypes.WARNING);
  }

  openQuickAddServiceCall() {
    let isAccessDeined = this.getServicePermissionByActionIconType(SERVICES.ADD_CLIENT_SERVICE_CALL);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.showServiceSubmenu = false
      const dialogRef = this.dialog.open(QuickAddServiceCallDialogComponent, {
        width: '750px',
        data: {
          customerId: this.selectedRows.customerId,
          customerAddressId: this.selectedRows.customerAddressId,
          userId: this.loggedInUserData?.userId,
          edit: true
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        this.rxjsService.setDialogOpenProperty(false);
      });
    }
  }

  openArmDisarmDialog() {
    let isAccessDeined = this.getServicePermissionByActionIconType(SERVICES.RESCHEDULE_O_C);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.showServiceSubmenu = false
      const ref = this.dialogService.open(ArmDisarmScheduleDialogComponent, {
        header: 'Arm Disarm Schedule',
        width: '950px',
        data: this.selectedRows.occurrenceBookId
      });
      ref.onClose.subscribe((result) => {
        if (result) {
        }
      });
    }
  }

  onMemoSubmit() {
    this.rxjsService.setDialogOpenProperty(true)
    if (this.memoForm.invalid) {
      return;
    }
    if (this.selectedRows) {
      let formValue = this.memoForm.value;
      formValue.occurrenceBookId = this.selectedRows['occurrenceBookId']
      formValue.createdUserId = this.loggedInUserData?.userId,
        this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEMO_POST, formValue).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.memoDialog = false;
            this.memoForm.reset();
          }
        });
    }
  }
  memoDialogCancel() {
    this.memoForm.reset()
    this.memoDialog = false;
  }

  getEventPrintReport(occurrenceBookId) {
    let otherParams = {}
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_SIGNAL_REPORT,
      occurrenceBookId,
      false)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventPrintData = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_EVENT_MEMO_REPORT,
      occurrenceBookId,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventPrintData.listData = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  displayPrintStack = false;
  openPrintStackModal() {
    this.printDialog = false
    this.displayPrintStack = true;
    this.initOccurrenceBookListForm();
  }

  initOccurrenceBookListForm() {
    let printStackModel = new PrintStackModel();
    this.printStackForm = this._fb.group({});
    Object.keys(printStackModel).forEach((key) => {
      this.printStackForm.addControl(key, new FormControl(printStackModel[key]));
    });
    this.printStackForm = setRequiredValidator(this.printStackForm, ["occurrenceBookListReportTemplateName", "occurrenceBookListReportTemplateColumnsList"])
    this.printStackForm.get("createdUserId").setValue(this.loggedInUserData.userId)
  }
  getOBbookColumns() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_BOOK_REPORT_DROPDOWN).subscribe(response => {
      if (response.isSuccess) {
        this.oBookReportTemplateDropdownList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  getOBbookTemplates() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_BOOK_REPORT_LIST).subscribe(response => {
      if (response.isSuccess) {
        this.oBookReportTemplateList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  showPermissionRestricedMessage() {
    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }

  get_F1_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.ACKNOWLEDGE);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.onClickNewTab(null, this.selectedRows);
    }
  }

  get_F2_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.DISPATCH);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.router.navigate(['/event-management/stack-operator'],
      {
        queryParams: {
          stackAreaConfigId: this.filterForm.value.stackAreaConfigId,
          stackConfigId: this.filterForm.value.stackConfigId,
          name: this.areaName,
          stackName: this.stackName
        }
      });
  }

  get_F3_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.DISPATCH);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }

  }

  get_F4_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.CONTACT);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.contactNavigation();
    }
  }

  get_F5_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.ON_HOLD_TRANSFER);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.openMoveToStackModal();
    }
  }

  get_F6_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.SERVICES);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.shortcutKeyDialog = false;
    this.showServiceSubmenu = true;
  }

  get_F7_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.SEARCH);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.router.navigate(['customer/advanced-search']);
  }

  get_F8_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.PRINT);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.getOBbookColumns();
      this.getOBbookTemplates();
      this.getEventPrintReport(this.selectedRows.occurrenceBookId);
      this.printDialog = true;
      this.shortcutKeyDialog = false;
    }
  }

  get_F9_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.MESSAGES);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.openMessageModal(this.selectedRows, this.selectedRowIndex);
      this.shortcutKeyDialog = false;
    }
  }

  get_F10_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.MEMO);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.getOccurrenceBookMemo();
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  get_F11_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.INCIDENT);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.openIncidentModal(this.selectedRows);
      this.shortcutKeyDialog = false;
    }
  }

  get_F12_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.LOG_AWAY);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.logAway();
    }
  }

  get_ENTER_ShorcutActions() {
    let isAccessDeined = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.CLIENT_HISTORY);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.rxjsService.setViewCustomerData({
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows?.customerAddressId,
        customerTab: 4,
        monitoringTab: null,
      })
      this.rxjsService.navigateToViewCustomerPage();
    }
  }

  get_CTRL_V_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.VERIFY_PASSWORD);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.openPasswordModal();
    }
  }

  get_CTRL_F_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.FIND_SIGNAL);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.router.navigateByUrl('event-management/find-signal');
  }

  get_HOME_ShorcutActions() {
    let isAccessDeined = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.TOP_OF_STACK);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    const scrollableBody = this.content.nativeElement.firstChild.getElementsByClassName('ui-scrollpanel-content')[0];
    scrollableBody.scrollTop = 0;
  }

  get_END_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.BOTTOM_OF_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    const scrollableBody = this.content.nativeElement.firstChild.getElementsByClassName('ui-scrollpanel-content')[0];
    scrollableBody.scrollTop = 500;
  }

  get_CTRL_S_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.SUPERVISOR_MESSAGE);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.supervisorMessageDialog = true;
      this.shortcutKeyDialog = false;
    }
  }

  get_CTRL_A_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.ACTIVE_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.shortcutKeyDialog = !this.shortcutKeyDialog;
    this.refreshStackDispatcher();
  }

  get_CTRL_H_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.HISTORY_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.getStackHistoryDetails();
    this.shortcutKeyDialog = false;
  }

  get_CTRL_G_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.USEFULL_NUMBERS);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.router.navigate(['event-management/useful-numbers-contact']);
  }

  get_CTRL_R_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.REFRESH);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.shortcutKeyDialog = false;
    this.refreshStackDispatcher();
  }

  get_CTRL_M_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.MAP);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.openMap(this.selectedRows);
    }
  }

  get_CTRL_U_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.OPERATOR_TRAKING);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.openOperatorInstructionsDialog(this.selectedRows);
    }
    this.shortcutKeyDialog = false;
  }

  onClickShortCutkey(key) {
    if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F1) {
      this.get_F1_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F2) {
      this.get_F2_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F3) {
      if (this.isSelectedRow()) {
        this.openArriveModal(this.selectedRows);
        this.shortcutKeyDialog = false;
      }
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F4) {
      this.get_F4_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F5) {
      this.get_F5_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F6) {
      this.get_F6_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F7) {
      this.get_F7_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F8) {
      this.get_F8_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F9) {
      this.get_F9_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F10) {
      this.get_F10_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F11) {
      this.get_F11_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F12) {
      this.get_F12_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_ENTER) {
      this.get_ENTER_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_V) {
      this.get_CTRL_V_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_F) {
      this.get_CTRL_F_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_HOME) {
      this.get_HOME_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_END) {
      this.get_END_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_S) {
      this.get_CTRL_S_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_A) {
      this.get_CTRL_A_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_H) {
      this.get_CTRL_H_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_G) {
      this.get_CTRL_G_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_R) {
      this.get_CTRL_R_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_M) {
      this.get_CTRL_M_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_ESC) {
      this.shortcutKeyDialog = false;
    } else {
      this.shortcutKeyDialog = false;
    }
  }

  loadTemplate(event) {
    let lists = event.value.occurrenceBookListReportTemplateColumns.split(", ");
    let _selectedRow = [];
    this.occurrenceBookListReportTemplateId = event.value.occurrenceBookListReportTemplateId;
    this.printStackForm.get('occurrenceBookListReportTemplateName').setValue(event.value.occurrenceBookListReportTemplateName);
    this.printStackForm.get('occurrenceBookListReportTemplateId').setValue(event.value.occurrenceBookListReportTemplateId);
    this.printStackForm.get('occurrenceBookListReportTemplateColumnsList').setValue(event.value.occurrenceBookListReportTemplateColumnsList);
    for (const list of lists) {
      let data = this.oBookReportTemplateDropdownList.find(item => item.displayName == list)
      _selectedRow.push(data);
    }
    this.selectedFieldsRow = _selectedRow;
    this.mapSelectedOBookColumn();
  }

  moveToHoldOrUnhold(holdType: string) {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    let data = {
      occurrenceBookId: this.moveToStackDetails.occurrenceBookId,
      modifiedUserId: this.loggedInUserData.userId
    }
    let apiSuffix = holdType == 'hold' ? EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_ONHOLD :
      EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_UNHOLD;
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, apiSuffix, data)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.moveToStackDialog = false;
        this.selectedRows = [];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  onRowSelectOBook() {
    this.mapSelectedOBookColumn();
  }

  onRowUnselectBook() {
    this.mapSelectedOBookColumn();
  }

  mapSelectedOBookColumn() {
    this.selectedFieldsOBook = {};
    this.selectedFieldsRow.map(item => {
      this.selectedFieldsOBook[item.displayName] = this._convertText(item.displayName);
    });
  }

  _convertText(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult;
  }

  resetLoadTemplate() {
    this.selectedFieldsRow = [];
    this.occurrenceBookListReportTemplateId = "";
    this.printStackForm.reset();
  }

  saveOBookTemplate() {
    let _ids = [];
    this.selectedFieldsRow.map(item => {
      _ids.push({ occurrenceBookListReportTemplateId: 0, occurrenceBookListReportStaticColumnId: item.id });
    });
    this.printStackForm.get("occurrenceBookListReportTemplateColumnsList").setValue(_ids);
    if (!this.printStackForm.valid) return '';
    let crudService: Observable<IApplicationResponse> = this.occurrenceBookListReportTemplateId ? this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_BOOK_REPORT_LIST, this.printStackForm.value)
      : this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_BOOK_REPORT_LIST, this.printStackForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getOBbookTemplates();
        this.printStackForm.reset();
        this.printStackForm.get('occurrenceBookListReportTemplateName').setErrors(null);
        this.selectedFieldsRow = [];
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  copyToClipboard(element) {
    element.select();
    document.execCommand('copy');
  }

  async pastToClipboard() {
    this.pastText = await navigator.clipboard.readText()
  }

  redirectToAddService() {
    let isAccessDeined = this.getAllPermissionByActionIconType(DASHBOARD.GUARD_REQUEST);
    if (isAccessDeined) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      window.open(
        `billing/risk-watch-guard-service/add-service?customerId=${this.selectedRows.customerId}`);
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  openPassword() {
    let data = {
      occurrenceBookId: this.selectedOccurrenceBookId,
      customerId: this.selectedRows.customerId,
      customerAddressId: this.selectedRows.customerAddressId,
      LoggedInUserId: this.loggedInUserData.userId,
    }
    this.dialogService.open(PasswordPopupComponent, { showHeader: false, data: { ...data, isPasswordDialog: true }, width: '500px' });
  }

  getStackHistoryDetails() {
    this.rxjsService.setDialogOpenProperty(true);
    this.dialogService.open(StackHistoryDetailsComponent, {
      showHeader: true,
      header: 'Stack History',
      baseZIndex: 10000,
      height: '80%',
      data: { stackAreaConfigId: this.filterForm.value.stackAreaConfigId, stackConfigId: this.filterForm.value.stackConfigId, breadcrumFalse: false }
    });
  }

  getServicePermissionByActionIconType(actionTypeMenuName): boolean {
    if (!this.servicesPermissions) {
      return true;
    }
    let foundObj = this.servicesPermissions['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  getAllPermissionByActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.allPermissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  getShortCutKeyPermissionByActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.shortcutKeysPermissions['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  ngOnDestroy() {
    if (this.startMonitoring) {
      clearInterval(this.startMonitoring);
    }
    this.signalrConnectionService.unObserveEvent();
    this.dialog.closeAll();
    this.sessionService.removeItem('selectedStack');
    this.sessionService.removeItem('selectedRowIndex');
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setPopupLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(false);
    this.hubConnectionInstanceForSalesAPI?.off(SignalRTriggers.DispatcherStackViewChangesTrigger);
  }
}