
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, forkJoin, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-alarm-list-grid',
  templateUrl: './alarm-list-grid.component.html',
  styleUrls: ['./dispatcher-list.component.scss'],
  styles: [`
        .loading-text {
            display: block;
            background-color: #f1f1f1;
            min-height: 19px;
            animation: pulse 1s infinite ease-in-out;
            text-indent: -99999px;
            overflow: hidden;
        }
    `]
})
export class AlarmListGridComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;

  observableResponse;
  selectedTabIndex: any = 0;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  dataList: any = [];
  dataListScroll: any = [];
  loading: boolean;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  pageLimit: any = [20, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any
  row: any = {}
  filterForm: FormGroup
  showFilterForm: boolean = false
  divisionList: any = []
  mainAreaList: any = []
  subAreaList: any = []
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {

    this.primengTableConfigProperties = {
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal Management Work List',
            dataKey: 'customerId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'divisionName', header: 'Type', width: '200px' }, { field: 'customerName', header: 'Type Desc', width: '200px' }, { field: 'customerNumber', header: 'Cust Code', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_WORK_LIST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            ebableFilterActionBtn: false
          },


        ]

      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;

    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.dataList = [
      {
        customerName: 'Mr Andrew Johnson',
        customerNumber: 123456,
        divisionName: 'PT',
        mainArea: 'sasdsad'
      },
      {
        customerName: 'Mr San John',
        customerNumber: 21524522,
        divisionName: 'CE',
        mainArea: 'sfsgf'
      },
      {
        customerName: 'Mr Harry Liaza',
        customerNumber: 325625222,
        divisionName: 'DB',
        mainArea: 'dsgdfhh'
      },
      {
        customerName: 'Mr xyz Johnson',
        customerNumber: 123456,
        divisionName: 'PT',
        mainArea: 'sasdsad'
      },
      {
        customerName: 'Mr abc John',
        customerNumber: 21524522,
        divisionName: 'CE',
        mainArea: 'sfsgf'
      },
      {
        customerName: 'Mr ross Liaza',
        customerNumber: 325625222,
        divisionName: 'DB',
        mainArea: 'dsgdfhh'
      }
    ];
    
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData()
    this.row['pageIndex'] = 0
    this.row['pageSize'] = 20
    this.createFilterForm()
    this.getAllDataList()
  }

  ngAfterViewInit() {
    const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-view')[0];
    scrollableBody.onscroll = (x) => {
      this.scrollEnabled = true;
      var st = Math.floor(scrollableBody.scrollTop + scrollableBody.offsetHeight)
      let max = scrollableBody.scrollHeight - 150;


    }
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      divisionId: [''],
      mainAreaId: [''],
      subAreaId: [''],
    });
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value : '' },
      { mainAreaId: this.filterForm.get('mainAreaId').value ? this.filterForm.get('mainAreaId').value : '' },
      { subAreaId: this.filterForm.get('subAreaId').value ? this.filterForm.get('subAreaId').value : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.row['pageIndex'] = 0
    this.observableResponse = this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], filterdNewData);

    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.filterForm.reset()
    this.row['pageIndex'] = 0
    this.observableResponse = this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], null);
    this.showFilterForm = !this.showFilterForm;
  }

  getAllDataList(): void {
    forkJoin([this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DIVISION_LIST)
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSubAreaList(selectedMainAreaId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_SUB_AREA, selectedMainAreaId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          this.scrollEnabled = false
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.initialLoad) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    }
    this.initialLoad = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        if (!this.scrollEnabled) {
          this.dataList = []
          this.dataList = this.observableResponse;
        } else {
          this.observableResponse.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = 0;
        this.row['pageIndex'] = this.row['pageIndex'] + 1

      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }


  loadPaginationLazy(event) {

  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/signal-management/signal-management-work-list/add-edit");
            break;

        }

        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/signal-management/signal-management-work-list/view"], { queryParams: { id: editableObject['signalManagementConfigId'] } });
            break;

        }
    }
  }



  onTabChange(event) {
    this.tables.forEach(table => { //to set default row count list
      table.rows = 20
    })

    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/event-management/signal-management'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()

  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }



  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }


}

