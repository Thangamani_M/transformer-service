
import { Component, Input, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, ComponentProperties, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, getPDropdownData, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { environment } from '@environments/environment';
import { OpenAreasListModel, OpenAreasModel, ResetVehicleModel } from '@modules/event-management/models/configurations/open-areas-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
declare var google: any;
@Component({
  selector: 'app-dispatcher-list-grid',
  templateUrl: './dispatcher-list-grid.component.html',
  styleUrls: ['./dispatcher-list.component.scss'],
  styles: [`
        .loading-text {
            display: block;
            background-color: #f1f1f1;
            min-height: 19px;
            animation: pulse 1s infinite ease-in-out;
            text-indent: -99999px;
            overflow: hidden;
        }
    `]
})
export class DispatcherListGridComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;

  observableResponse;
  selectedTabIndex = 0;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties;
  dataList = [];
  dataListScroll = [];
  loading: boolean;
  public bradCrum: MatMenuItem[];
  status = [];
  selectedColumns = [];
  selectedRows: string[] = [];
  totalRecords;
  pageLimit = [20, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today = new Date();
  searchColumns;
  row = {};
  filterForm: FormGroup;
  showFilterForm: boolean = false;
  mainAreaList = [];
  subAreaList = [];
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  openAreaDialog = false;
  openAreasForm: FormGroup;
  timeOnSiteExtentionForm: FormGroup;
  openAreasList: FormArray;
  loggedUser;
  openAreaReasonDropDown = [];
  shortcutMenus = false;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  timeOnSiteExtentModal: boolean = false;
  isTimeOnSiteExtentionButton: boolean = false;
  responseOfficerId;
  @Input() stackAreaConfigId: string;
  @Input() refresh: any;
  @Input() selectedrowData: any;
  resetVehicleDialog: boolean = false;
  dispacherRow: any;
  reAssignVehicleDialog: boolean = false;
  autoDispacheDialog: boolean = false;
  reAssignVehicleToSignalDetails: any = [];
  officerAssistaceForm: FormGroup
  map: any;
  overlays
  options: any
  openMapDialog: boolean = false;
  lat: any;
  lang: any;
  officerAssistanceDialog:boolean = false;
  officerAssistanceDetails:any;
  officerAssistanceDetailsView:any;
  vehiclesData:any = {};
  resetVehicleDialogForm: FormGroup;
  responseOfficerList:any = [];
  showResetVehicleControls: boolean;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService, private snakbarService: SnackbarService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'subAreaId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'subArea', header: 'Vehicles', width: '50px' }, { field: 'status', header: 'Status', width: '50px' },
            { field: 'percentageLong', header: '%Long', width: '50px' },
            { field: 'deviceStatus', header: 'Device Status', width: '50px' }, { field: 'wait', header: 'Wait', width: '50px' },
            { field: 'call', header: 'Call', width: '50px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DISPATCHER_VIEW_SUB_AREA,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            ebableFilterActionBtn: false
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
    this.map = google.maps.Map;
  }

  ngOnInit(): void {
    this.createOpenAreasForm();
    this.createTimeOnSiteExtentionForm();
    this.openAreaDialog = false;
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    this.row['pageIndex'] = 0;
    this.row['pageSize'] = 20;
    this.createFilterForm();
    this.createOfficerAssistanceForm();
    this.createResetVehicleDialogForm();
    this.options = {
      center: {
        lat: -25.746020,
        lng: 28.187120
      },
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes || this.stackAreaConfigId) {
      if (changes['refresh']) {
        this.getRequiredListData();
      }
    }
  }

  createOpenAreasForm(openAreasModel?: OpenAreasModel) {
    let openAreasModelControl = new OpenAreasModel(openAreasModel);
    this.openAreasForm = this._fb.group({
      openAreasList: this._fb.array([])
    });
    Object.keys(openAreasModelControl).forEach((key) => {
      this.openAreasForm.addControl(key, new FormControl(openAreasModelControl[key]));
    });
  }

  createTimeOnSiteExtentionForm() {
    this.timeOnSiteExtentionForm = this._fb.group({
      extendedMins: [null, [Validators.required, Validators.min(1), Validators.max(15)]],
      modifiedUserId: [''],
      responseOfficerId: [''],
    });
  }
  createOfficerAssistanceForm() {
    this.officerAssistaceForm = this._fb.group({
      notifiedBy: [""],
      latitude: [''],
      longitude: [''],
      createdUserId:[this.loggedUser.userId],
      distressLocationTypeId:[""],
      message:["" , [Validators.required]],
      location:[""]
    });
  }

  //Create FormArray
  get openAreasListArray(): FormArray {
    if (!this.openAreasForm) return;
    return this.openAreasForm.get("openAreasList") as FormArray;
  }

  //Create FormArray controls
  createOpenAreasListModel(openAreasListModel: OpenAreasListModel): FormGroup {
    let openAreasListModelControl = new OpenAreasListModel(openAreasListModel);
    let formControls = {};
    Object.keys(openAreasListModelControl).forEach((key) => {
      if (key == 'modifiedUserId' || key == 'subAreaId' || key == 'responseOfficerId' || key == 'isOnAir' || key == 'openAreaReasonId') {
        formControls[key] = [{ value: openAreasListModelControl[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: openAreasListModelControl[key], disabled: false }]
      }
    });
    let forms = this._fb.group(formControls);
    forms.get('isOnAir').valueChanges.subscribe(val => {
      if (val == true) {
        forms.controls['openAreaReasonId'].disable()
        forms.controls['openAreaReasonId'].clearValidators()
      }
      else {
        forms.controls['openAreaReasonId'].enable()
      }
    })
    return forms;
  }

  getOpenReason() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_OPEN_AREA_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.openAreaReasonDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOpenAreas() {
    let otherParams = {}
    otherParams['StackAreaConfigId'] = this.stackAreaConfigId

    this.openAreaDialog = true;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_AREAS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200 && response.isSuccess) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.createOpenAreasForm()
          if (response.resources.length > 0) {
            this.openAreasList = this.openAreasListArray;
            response.resources.forEach((openAreasListModel: OpenAreasListModel) => {
              openAreasListModel.openAreaId = openAreasListModel.openAreaId ? openAreasListModel.openAreaId : null,
                openAreasListModel.mainAreaId = openAreasListModel.mainAreaId ? openAreasListModel.mainAreaId : null,
                openAreasListModel.subAreaId = openAreasListModel.subAreaId ? openAreasListModel.subAreaId : null,
                openAreasListModel.responseOfficerId = openAreasListModel.responseOfficerId ? openAreasListModel.responseOfficerId : null,
                openAreasListModel.subArea = openAreasListModel.subArea ? openAreasListModel.subArea : null,
                openAreasListModel.modifiedUserId = this.loggedUser.userId ? this.loggedUser.userId : '',
                openAreasListModel.isOnAir = openAreasListModel.isOnAir ? openAreasListModel.isOnAir : false,
                openAreasListModel.openAreaReasonId = openAreasListModel.openAreaReasonId ? openAreasListModel.openAreaReasonId : null,
                this.openAreasList.push(this.createOpenAreasListModel(openAreasListModel));
            });
          }
        }
      });
  }

  ngAfterViewInit() {
    const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-view')[0];
    scrollableBody.onscroll = (x) => {
      this.scrollEnabled = true;
      var st = Math.floor(scrollableBody.scrollTop + scrollableBody.offsetHeight)
      let max = scrollableBody.scrollHeight - 200;
      if (st >= max) {
        if (this.dataList.length < this.totalRecords) {
          this.row['pageIndex'] = 0
          this.row['pageSize'] = 20
          let otherParams = {}
          this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], otherParams);
        }
      }
    }
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      divisionId: [''],
      mainAreaId: [''],
      subAreaId: [''],
    });
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value : '' },
      { mainAreaId: this.filterForm.get('mainAreaId').value ? this.filterForm.get('mainAreaId').value : '' },
      { subAreaId: this.filterForm.get('subAreaId').value ? this.filterForm.get('subAreaId').value : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.row['pageIndex'] = 0
    this.observableResponse = this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.filterForm.reset()
    this.row['pageIndex'] = 0
    this.observableResponse = this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], null);
    this.showFilterForm = !this.showFilterForm;
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getResponceOfficerAssistance(id) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONCE_OFFICER_ASSISTANCE_GET, id)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.officerAssistanceDetailsView = response.resources
          this.officerAssistanceDetails = response.resources;
          this.officerAssistanceDetails = [
            { name: 'Vehicle Area', value: response.resources?.subArea },
            { name: 'Response Officer', value: response.resources?.userName },
          ]
          this.officerAssistaceForm.get("distressLocationTypeId").setValue(response.resources?.defaultDistressLocationTypeId);
            this.officerAssistaceForm.get("createdUserId").setValue(this.loggedUser?.userId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSubAreaList(selectedMainAreaId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_SUB_AREA, selectedMainAreaId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.subAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }



  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          this.scrollEnabled = false
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = { stackAreaConfigId: this.stackAreaConfigId };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    otherParams['isDefaultLoaderDisabled'] = true
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.DISPATCHER_VIEW_SUB_AREA,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        if (!this.scrollEnabled) {
          this.dataList = [];
          this.dataList = this.observableResponse;
        } else {
          this.observableResponse.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = 0;
        this.row['pageIndex'] = this.row['pageIndex'] + 1
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    });
  }

  handleMapClick(event) {
    this.lat = event.latLng.lat()
    this.lang = event.latLng.lng()
    this.officerAssistaceForm.get('latitude').setValue(this.lat)
    this.officerAssistaceForm.get('longitude').setValue(this.lang)
    let location =`${this.lang},${this.lat}`
    this.officerAssistaceForm.get('location').setValue(location)

    this.openMapDialog = false
    this.rxjsService.setFormChangeDetectionProperty(true)
  }

  setMap(event) {
    this.map = event.map;
  }


  openMap(event) {
    this.options = {};
    this.overlays = {};
    let location = []
    this.openMapDialog = true
    if (location) {
    } else {
      location = []
      location[0] = -25.746020 // fidelity south afcia coordinates by default
      location[1] = 28.187120 // fidelity south afcia coordinates by default
    }
    this.options = {
      center: { lat: Number(location[0]), lng: Number(location[1]) },
      zoom: 12
    };

    setTimeout(() => {
      this.map.setCenter({
        lat: Number(this.lat? this.lat:location[0]),
        lng: Number( this.lang? this.lang:location[1])
      });
    }, 500);

    this.overlays = [new google.maps.Marker({ position: { lat: Number(this.lat? this.lat:location[0]), lng: Number(this.lang?this.lang:location[1]) }, icon: "assets/img/map-icon.png" })];
  }

  omit_number(event) {
    var key;
    key = event.charCode;  //         key = event.keyCode;  (Both can be used)
    return ((key > 47 && key < 58) || key == 45 || key == 46);
  }


  offiserAssistance() {
    if(this.officerAssistaceForm.invalid){
      return
    }
    let data = this.officerAssistaceForm.value
    data.notifiedBy = this.dispacherRow?.responseOfficerId;
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISTRESS_RESPONCE_OFFICER_OTHER, data)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.officerAssistanceDialog = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.officerAssistaceForm.reset();
      }
    });
  }


  loadPaginationLazy(event) {
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        this.dispacherRow = row;
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/signal-management/signal-management-work-list/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/signal-management/signal-management-work-list/view"], { queryParams: { id: editableObject['signalManagementConfigId'] } });
            break;
        }
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => { //to set default row count list
      table.rows = 20
    });
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/event-management/signal-management'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData();
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData();
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  openShortcutMenus(rowData) {
    this.vehiclesData = rowData
    this.isTimeOnSiteExtentionButton = false;
    if (rowData.deviceStatus == "Arrived") {
      this.responseOfficerId = rowData.responseOfficerId;
      this.shortcutMenus = true;
      this.isTimeOnSiteExtentionButton = true;
    }
    this.shortcutMenus = true;
  }

  openAreaModal() {
    this.shortcutMenus = false;
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.openAreaDialog = true;
    this.createOpenAreasForm();
    this.getOpenAreas();
    this.getOpenReason();
  }

  mobileDeviceConfig() {
    window.open(
      `/event-management/ro-configuration?tab=15`);
  }
  callDelayConfig() {
    window.open(
      `/event-management/event-configuration?tab=11`);
  }
  setBacupConfig() {
    window.open(
      `/event-management/event-configuration?tab=15`);
  }

  onSubmitOpenAreas() {
    let formValue = this.openAreasForm.getRawValue();
    formValue.openAreasList.forEach(element => {
      element.openAreaId = element.openAreaId ? element.openAreaId : null,
        element.modifiedUserId = this.loggedUser.userId ? this.loggedUser.userId : null,
        element.subAreaId = element.subAreaId ? element.subAreaId : null,
        element.responseOfficerId = element.responseOfficerId ? element.responseOfficerId : null,
        element.isOnAir = element.isOnAir ? element.isOnAir : false,
        element.openAreaReasonId = element.openAreaReasonId ? element.openAreaReasonId : null
    });
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_AREAS, formValue.openAreasList)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.rxjsService.setDialogOpenProperty(false);
          this.openAreaDialog = false;
          this.shortcutMenus = false;
        }
      });
  }

  openTimeOnSiteExtentionModal() {
    this.timeOnSiteExtentionForm.get('extendedMins').setValue(null)
    this.shortcutMenus = false;
    this.timeOnSiteExtentModal = true;
  }

  onSubmitTimeOnSiteExtent() {
    if (this.timeOnSiteExtentionForm.invalid) {
      return;
    }
    let formValue = this.timeOnSiteExtentionForm.value;
    formValue.modifiedUserId = this.loggedUser.userId;
    formValue.responseOfficerId = this.dispacherRow?.responseOfficerId,
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_SUB_AREA_TIMEON_EXTENDED, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.timeOnSiteExtentionForm.reset()
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
        this.timeOnSiteExtentModal = false;
      }
    });
  }

  openPTT(event) {
    let params = event?.pttUsername ? event?.pttUsername : '';
    if(params){
      window.open(environment.PTT_UI + '?email=' + params, "ModalPopUp", "location=no,width=500,height=250,scrollbars=yes,top=100,left=700,resizable = yes");

    }else {
      return this.snakbarService.openSnackbar('Stack area is not configured for PTT', ResponseMessageTypes.WARNING)

    }
  }

  createResetVehicleDialogForm(resetVehicleModel?: ResetVehicleModel) {
    let resetVehicleDialogModel = new ResetVehicleModel(resetVehicleModel);
    this.resetVehicleDialogForm = this._fb.group({});
    Object.keys(resetVehicleDialogModel).forEach((key) => {
      if (typeof resetVehicleDialogModel[key] === 'object') {
        this.resetVehicleDialogForm.addControl(key, new FormArray(resetVehicleDialogModel[key]));
      } else {
        this.resetVehicleDialogForm.addControl(key, new FormControl(resetVehicleDialogModel[key]));
      }
    });
    this.resetVehicleDialogForm.get('createdUserId').setValue(this.loggedInUserData?.userId);
    this.resetVehicleDialogForm = setRequiredValidator(this.resetVehicleDialogForm, ["vehicleRegistrationNumber", "responseOfficerId", "subAreaId"]);
    this.resetVehicleDialogForm.get('vehicleRegistrationNumber').setValidators([Validators.required, Validators.maxLength(20)]);
    this.resetVehicleDialogForm.updateValueAndValidity();
  }

  resetVehivleToAvailable() {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.dispacherRow?.responseOfficerId) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONSE_OFFICER_CURRENT_LOG_ALREADY_LOGGED_IN, this.dispacherRow?.responseOfficerId)
        .subscribe((resp: IApplicationResponse) => {
          if (resp?.isSuccess && resp?.statusCode == 200) {
            this.showResetVehicleControls = resp?.resources;
            if (this.dispacherRow?.responseOfficerId && this.showResetVehicleControls == false) {
              this.getResponseOfficerDropdown();
            } else {
              this.resetVehicleDialogForm = clearFormControlValidators(this.resetVehicleDialogForm, ["vehicleRegistrationNumber", "responseOfficerId", "subAreaId"]);
              this.openResetVehicleDialog();
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          } else {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
      })
    } else {
      this.getResponseOfficerDropdown();
    }
  }

  getResponseOfficerDropdown() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_RESPONSE_OFFICER_NOT_LOGGED_IN)
    .subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.responseOfficerList = getPDropdownData(response?.resources);
        this.resetVehicleDialogForm.get('subAreaId').setValue(this.dispacherRow?.subAreaId);
        this.openResetVehicleDialog();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  openResetVehicleDialog(val = true) {
    this.resetVehicleDialog = val;
    this.shortcutMenus = false;
    this.rxjsService.setDialogOpenProperty(val);
  }

  reAssignVehicleToSignal() {
    this.shortcutMenus = false;
    this.reAssignVehicleDialog = true;
    this.reAssignVehicleToSignalDetails = [
      { name: 'Do you Want to reassign', value: this.dispacherRow?.subArea },
      { name: 'OB Number', value: this.selectedrowData?.occurrenceBookNumber },
      { name: 'Address', value: this.selectedrowData?.fullAddress },
      { name: 'Signal', value: this.selectedrowData?.firstSignal },
    ]
  }

  resetToavialableVehicleSubmit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    if ((this.dispacherRow?.responseOfficerId && this.showResetVehicleControls == false) || !this.dispacherRow?.responseOfficerId) {
      this.onResetVeicleFormSubmit();
    } else {
      let data = {
        ResponseOfficerId: this.dispacherRow?.responseOfficerId,
        createdUserId: this.loggedInUserData.userId
      }
      this.rxjsService.setFormChangeDetectionProperty(true);
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONCE_OFFICER_AVAILABLE, data)
      crudService.subscribe((response: IApplicationResponse) => {
        this.onAfterResetVehicle(response);
      });
    }
  }

  onResetVeicleFormSubmit() {
    if (this.resetVehicleDialogForm?.invalid) {
      this.resetVehicleDialogForm?.markAllAsTouched();
      return;
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RO_MANUAL_LOGIN_BY_OTHER, this.resetVehicleDialogForm?.value)
    .subscribe((res: IApplicationResponse) => {
      this.onAfterResetVehicle(res);
    })
  }

  onAfterResetVehicle(res) {
    if (res?.isSuccess && res?.statusCode == 200) {
      this.resetVehicleDialog = false;
      this.getRequiredListData();
    }
    this.rxjsService.setDialogOpenProperty(false);
  }

  reassignToavialableVehicleSubmit() {
    if (!this.selectedrowData) {
      return this.snakbarService.openSnackbar('Select the required signal', ResponseMessageTypes.WARNING)
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    let data = {
      ResponseOfficerId: this.dispacherRow?.responseOfficerId,
      OccurrenceBookId: this.selectedrowData?.occurrenceBookId,
      createdUserId: this.loggedInUserData.userId,
      newOccurrenceBookId: this.selectedrowData?.occurrenceBookId,
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONCE_OFFICER_REASSIGN, data)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.reAssignVehicleDialog = false;
        this.getRequiredListData();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  checkAutoDispach() {
    this.autoDispacheDialog = true;
    this.shortcutMenus = false;
  }

  checkAutoDispachSubit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    setTimeout(() => {
      this.rxjsService.setDialogOpenProperty(true);
    }, 2000);
    let data = {
      ResponseOfficerId: this.dispacherRow?.responseOfficerId,
      modifiedUserId: this.loggedInUserData.userId,
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESPONCE_OFFICER_AVAILABLE_ENABLE_AUTODISPCHE, data)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.autoDispacheDialog = true;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  officerAssistace(){
    this.getResponceOfficerAssistance(this.vehiclesData?.subAreaId)

    this.officerAssistanceDialog = true;
    this.shortcutMenus = false;
  }
}
