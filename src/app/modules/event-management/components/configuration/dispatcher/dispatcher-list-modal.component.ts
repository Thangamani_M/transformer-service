import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { DispatcherListModal } from '@modules/event-management/models/configurations/dispatcher-list-modal-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'dispatcher-list-modal-component',
  templateUrl: './dispatcher-list-modal.component.html',
  styleUrls: ['./dispatcher-list.component.scss'],
})
export class DispatcherListModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  @Output() outputAreaName = new EventEmitter<any>();
  @Output() outputStackName = new EventEmitter<any>();
  dispatcherListModalForm: FormGroup;
  areaDropDown = [];
  stackDropDown = [];
  areaName;
  stackName;
  userData;
  @ViewChild('fileInput', null) myFileInputField: ElementRef;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
    private crudService: CrudService, private router: Router,
    private dialog: MatDialog, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createDispatcherListModalForm();
    this.getArea();
    this.getStack();
  }

  createDispatcherListModalForm(): void {
    let dispatcherListModal = new DispatcherListModal();
    this.dispatcherListModalForm = this.formBuilder.group({
    });
    Object.keys(dispatcherListModal).forEach((key) => {
      this.dispatcherListModalForm.addControl(key, new FormControl(dispatcherListModal[key]));
    });
    this.dispatcherListModalForm = setRequiredValidator(this.dispatcherListModalForm, ["stackAreaConfigId", "stackConfigId"]);
  }

  getArea() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_AREA_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.areaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  getStack() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stackDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  onChangeArea(value) {
    value = this.dispatcherListModalForm.value.stackAreaConfigId;
    let found = this.areaDropDown.find(e => e.id == value);
    this.areaName = found.displayName
  }

  onChangeStack(value) {
    value = this.dispatcherListModalForm.value.stackConfigId;
    let found = this.stackDropDown.find(e => e.id == value);
    this.stackName = found.displayName
  }

  onSubmit(): void {
    if (this.dispatcherListModalForm.invalid) {
      this.dispatcherListModalForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setDialogOpenProperty(false);
    this.rxjsService.setStackAreaConfigId(this.dispatcherListModalForm.value.stackAreaConfigId);
    this.dialog.closeAll();
    this.outputData.emit(this.dispatcherListModalForm.value);
    this.outputAreaName.emit(this.areaName);
    this.outputStackName.emit(this.stackName);
  }

  navigateToStackOperator() {
    if (this.dispatcherListModalForm.invalid) {
      this.dispatcherListModalForm.markAllAsTouched();
      return;
    }
    this.router.navigate(["/event-management/stack-operator"], {
      queryParams: {
        stackAreaConfigId: this.dispatcherListModalForm.value.stackAreaConfigId,
        stackConfigId: this.dispatcherListModalForm.value.stackConfigId,
        name: this.areaName,
        stackName: this.stackName
      },
    });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
    this.dialog.closeAll();
  }
}
