import { CommonModule, DatePipe } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { FindSignalRoutingModule } from './find-signal-routing.module';
import { FindSignalComponent } from './find-signal/find-signal.component';

@NgModule({
  declarations: [FindSignalComponent],
  imports: [
    CommonModule,
    FindSignalRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-EN' },
  { provide: MAT_DATE_LOCALE, useValue: 'en-EN' }, DatePipe]
})
export class FindSignalModule { }
