import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { ColorPickerModule } from 'primeng/colorpicker';
import { PropertyAccessAddEditComponent } from './property-access-add-edit.component';
import { PropertyAccessRoutingModule } from './property-access-routing.module';
import { PropertyAccessViewComponent } from './property-access-view.component';



@NgModule({
  declarations: [PropertyAccessAddEditComponent, PropertyAccessViewComponent],
  imports: [
    CommonModule,
    PropertyAccessRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    ColorPickerModule
  ]
})
export class PropertyAccessModule { }
