import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { PropertyAccessModel, PropertyAccessReasonListModel } from '@modules/event-management/models/configurations/property-access.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-property-access-add-edit',
  templateUrl: './property-access-add-edit.component.html',
})
export class PropertyAccessAddEditComponent implements OnInit {
  incidentId: any
  divisionDropDown: any = [];
  propertyAccessForm: FormGroup;
  propertyAccessReasonList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  incidentDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  public colorCode: string = '#278ce2';
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.incidentId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.incidentId ? 'Update Property Access':'Add Property Access';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Property Access', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 7 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.incidentId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/property-access/view' , queryParams: {id: this.incidentId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createpropertyAccessForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.incidentId) {

      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let cmcSmsGroup = new PropertyAccessModel(response.resources);

        this.incidentDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.incidentDetails?.propertyAccessName ? this.title +' - '+ this.incidentDetails?.propertyAccessName : this.title + ' --', relativeRouterUrl: '' });
        this.propertyAccessForm.patchValue(cmcSmsGroup);

        var propertyAccessDivisionsList = [];
        response.resources.propertyAccessDivisionsList.forEach(element => {
          propertyAccessDivisionsList.push(element.divisionId);
        });
        this.propertyAccessForm.get('propertyAccessDivisionsList').setValue(propertyAccessDivisionsList)
        this.propertyAccessReasonList = this.getpropertyAccessReasonListArray;
        response.resources.propertyAccessReasonList.forEach((propertyAccessReasonListModel) => {
          propertyAccessReasonListModel.propertyAccessReasonListId = propertyAccessReasonListModel.propertyAccessReasonListId;
          this.propertyAccessReasonList.push(this.createpropertyAccessReasonListModel(propertyAccessReasonListModel));
        });
      })
    } else {
      this.propertyAccessReasonList = this.getpropertyAccessReasonListArray;
      this.propertyAccessReasonList.push(this.createpropertyAccessReasonListModel());
    }
  }

  createpropertyAccessForm(): void {
    let propertyAccessModel = new PropertyAccessModel();
    this.propertyAccessForm = this.formBuilder.group({
      propertyAccessReasonList: this.formBuilder.array([])
    });
    Object.keys(propertyAccessModel).forEach((key) => {
      this.propertyAccessForm.addControl(key, new FormControl(propertyAccessModel[key]));
    });
    this.propertyAccessForm = setRequiredValidator(this.propertyAccessForm, ["propertyAccessName", "description", "propertyAccessDivisionsList"]);
    this.propertyAccessForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.propertyAccessForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getpropertyAccessReasonListArray(): FormArray {
    if (!this.propertyAccessForm) return;
    return this.propertyAccessForm.get("propertyAccessReasonList") as FormArray;
  }


  //Create FormArray controls
  createpropertyAccessReasonListModel(propertyAccessReasonListModel?: PropertyAccessReasonListModel): FormGroup {
    let propertyAccessReasonListFormControlModel = new PropertyAccessReasonListModel(propertyAccessReasonListModel);
    let formControls = {};
    Object.keys(propertyAccessReasonListFormControlModel).forEach((key) => {
      if (key === 'propertyAccessReasonListName' || key === 'propertyAccessReasonListId' || key === 'colorCode') {
        formControls[key] = [{ value: propertyAccessReasonListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.incidentId) {
        formControls[key] = [{ value: propertyAccessReasonListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: propertyAccessReasonListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.PROPERTY_ACCESS,
      this.incidentId
    );
  }



  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Access Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getpropertyAccessReasonListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.propertyAccessReasonListName)) {
        duplicate.push(k.value.propertyAccessReasonListName);
      }
      filterKey.push(k.value.propertyAccessReasonListName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }



  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (!this.onChange()) {
      return;
    }

    if (this.getpropertyAccessReasonListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };

    this.propertyAccessReasonList = this.getpropertyAccessReasonListArray;
    let propertyAccessReasonListModel = new PropertyAccessReasonListModel();
    this.propertyAccessReasonList.insert(0, this.createpropertyAccessReasonListModel(propertyAccessReasonListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Access Name already exist", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.getpropertyAccessReasonListArray.controls[i].value.incidentTypeName == '') {
      this.getpropertyAccessReasonListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getpropertyAccessReasonListArray.controls[i].value.propertyAccessReasonListId && this.getpropertyAccessReasonListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PROPERTY_ACCESS_REASON,
          this.getpropertyAccessReasonListArray.controls[i].value.propertyAccessReasonListId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getpropertyAccessReasonListArray.removeAt(i);
            }
            if (this.getpropertyAccessReasonListArray.length === 0) {
              this.addCmcSmsGroupEmployee();
            };
          });
      }
      else {
        this.getpropertyAccessReasonListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.propertyAccessForm.controls.propertyAccessDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.propertyAccessForm.controls.propertyAccessDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.propertyAccessForm.controls.propertyAccessDivisionsList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }

    if (this.propertyAccessForm.invalid) {
      return;
    }


    let formValue = this.propertyAccessForm.value;
    if (formValue.propertyAccessDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.propertyAccessDivisionsList = formValue.propertyAccessDivisionsList.filter(item => item != '')
      formValue.propertyAccessDivisionsList = formValue.propertyAccessDivisionsList.map(divisionId => ({ divisionId }))
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.incidentId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PROPERTY_ACCESS, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PROPERTY_ACCESS, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=7');
      }
    })
  }

  onCRUDRequested(type){}

}

