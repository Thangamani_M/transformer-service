import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PropertyAccessAddEditComponent } from './property-access-add-edit.component';
import { PropertyAccessViewComponent } from './property-access-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: PropertyAccessAddEditComponent, data: { title: 'Property Access Add/Edit' } },
  { path: 'view', component: PropertyAccessViewComponent, data: { title: 'Property Access View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PropertyAccessRoutingModule { }
