
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-find-signal',
  templateUrl: './find-signal.component.html',
})
export class FindSignalComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  divisionDropDown: any[];
  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  dataList: any
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  findSignalForm: FormGroup;
  row: any = {}
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  params: any;
  scrollEnabled: boolean = false;
  pageSize: number = 10;
  allPermissions: any = []

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,
    private router: Router,
    private store: Store<AppState>,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private _fb: FormBuilder,
    private snackbarService: SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Find Signal",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Management', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Find Signal',
            dataKey: 'occurrenceBookId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'accountCode', header: 'Transmitter', width: '100px' },
            { field: 'customerName', header: 'Client Name', width: '150px' },
            { field: 'occurrenceBookNumber', header: 'OB Number', width: '150px' },
            { field: 'fullAddress', header: 'Full Address', width: '200px' },
            { field: 'customerNumber', header: 'Customer ID', width: '150px' },
            { field: 'divisionName', header: 'Division', width: '50px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.RO_ACCEPT_TIME,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          }
        ]

      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.createForm()
    this.getDivisionDropDownById()
    this.combineLatestNgrxStoreData()
  }

  ngAfterViewInit() {

  }

  // Create Search Final Signal Form

  createForm() {
    this.findSignalForm = this._fb.group({
      Transmitter: [''],
      DisplayName: [''],
      OccurrenceBookNumber: [''],
      StreetAddress: [''],
      CustomerNumber: [''],
      BuildingName: [''],
      DivisionId: ['', Validators.required]
    });

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.allPermissions = response[1][EVENT_MANAGEMENT_COMPONENT.FIND_SIGNAL]
      if (this.allPermissions) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.allPermissions);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  searchFianlSignal() {
    let isAccessDeined = this.getAllPermissionByActionIconType('Filter');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.findSignalForm.invalid) {
      return;
    }
    let filteredData = Object.assign({},
      { AccountCode: this.findSignalForm.value.Transmitter ? this.findSignalForm.value.Transmitter : '' },
      { CustomerName: this.findSignalForm.value.DisplayName ? this.findSignalForm.value.DisplayName : '' },
      { OccurrenceBookNumber: this.findSignalForm.value.OccurrenceBookNumber ? this.findSignalForm.value.OccurrenceBookNumber : '' },
      { StreetAddress: this.findSignalForm.value.StreetAddress ? this.findSignalForm.value.StreetAddress : '' },
      { CustomerNumber: this.findSignalForm.value.CustomerNumber ? this.findSignalForm.value.CustomerNumber : '' },
      { BuildingName: this.findSignalForm.value.BuildingName ? this.findSignalForm.value.BuildingName : '' },
      { DivisionIds: this.findSignalForm.value.DivisionId ? this.findSignalForm.value.DivisionId : '' }

    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.row['pageIndex'] = 0
    this.dataList = this.getFianlSignal(this.row['pageIndex'], this.row['pageSize'], filterdNewData);

  }

  getFianlSignal(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.findSignalForm.value.DivisionId) {
      const params = { DivisionIds: this.findSignalForm.value.DivisionId }
      otherParams = { ...otherParams, ...params };
    }

    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_SIGNAL_DIVISION,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.dataList = []
      this.totalRecords = 0;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }



  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.findSignalForm.value.DivisionId) {
          const params = { DivisionIds: this.findSignalForm.value.DivisionId }
          otherParams = { ...otherParams, ...params };
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getFianlSignal(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    this.router.navigate(["/event-management/stack-operator"], {
      queryParams: {
        occurrenceBookId: editableObject['occurrenceBookId'],
        isFindSignal: true
      },
    })
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.findSignalForm.controls.DivisionId
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.findSignalForm.controls.DivisionId.patchValue([]);
    }
  }
  toggleDivisionOne() {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.findSignalForm.controls.DivisionId.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  clearSearch() {
    this.dataList = null;
    this.totalRecords = 0;
    this.findSignalForm.reset();
  }

  getAllPermissionByActionIconType(actionTypeMenuName): boolean {
    if (!this.allPermissions) {
      return true
    }
    let foundObj = this.allPermissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }
}
