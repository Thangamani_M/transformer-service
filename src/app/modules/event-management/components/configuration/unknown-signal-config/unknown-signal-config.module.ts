import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { UnknownSignalConfigAddEditComponent } from './unknown-signal-config-add-edit.component';
import { UnknownSignalConfigRoutingModule } from './unknown-signal-config-routing.module';
import { UnknownSignalConfigViewComponent } from './unknown-signal-config-view.component';



@NgModule({
  declarations: [UnknownSignalConfigViewComponent,UnknownSignalConfigAddEditComponent],
  imports: [
    CommonModule,
    UnknownSignalConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class UnknownSignalConfigModule { }
