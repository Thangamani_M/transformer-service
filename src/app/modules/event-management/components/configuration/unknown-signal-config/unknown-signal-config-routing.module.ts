import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UnknownSignalConfigAddEditComponent } from './unknown-signal-config-add-edit.component';
import { UnknownSignalConfigViewComponent } from './unknown-signal-config-view.component';


const routes: Routes = [{ path: '', redirectTo: 'add-edit', pathMatch: 'full' },
{ path: 'add-edit', component: UnknownSignalConfigAddEditComponent, data: { title: 'Unknown Signal Config  Add/Edit' } },
{ path: 'view', component: UnknownSignalConfigViewComponent, data: { title: 'Unknown Signal Config Config View' } },];;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class UnknownSignalConfigRoutingModule { }
