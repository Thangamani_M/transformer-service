import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-unknown-signal-config-view',
  templateUrl: './unknown-signal-config-view.component.html',
})
export class UnknownSignalConfigViewComponent implements OnInit {

unknownSignalConfigId: any
unknownSignalDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any;
totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
  tableComponentConfigs: {
    tabsList: []
  }
}

constructor(private activatedRoute: ActivatedRoute,
   private router: Router,
    private rxjsService: RxjsService, 
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.unknownSignalConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption: 'View Unwanted Carrier Config',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
    { displayName: 'Unwanted Carrier Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 38 } }
      , { displayName: '', }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  this.rxjsService.setGlobalLoaderProperty(false);
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  }
  this.combineLatestNgrxStoreData();
  if (this.unknownSignalConfigId) {
    this.getunknownSignalDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.unknownSignalDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + this.unknownSignalDetails.frequency;
      this.unknownSignalDetails = [
        { name: 'Frequency', value: response.resources.frequency },
        { name: 'Duration', value: response.resources.duration },
        { name: 'Alarm', value: response.resources.alarm},
        { name: 'Min Frequency', value: response.resources.minimumSignalCountinFrequency },
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}

combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(["/event-management/event-configuration/unknown-signal-config/add-edit"], { queryParams: { id: this.unknownSignalConfigId,totalTabs:this.totaltab, selectedTab:this.selectedTab} });
}

getunknownSignalDetails(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.UNDEFINED_SIGNALS,
    this.unknownSignalConfigId
  );
}

}