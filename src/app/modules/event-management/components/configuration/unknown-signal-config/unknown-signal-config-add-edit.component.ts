import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { UnknownSignalConfigModel } from '@modules/event-management/models/configurations/unknown-signal-config.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-unknown-signal-config-add-edit',
  templateUrl: './unknown-signal-config-add-edit.component.html',
})
export class UnknownSignalConfigAddEditComponent implements OnInit {

  unknownSignalConfigId: string;
  unknownDetails: any;
  alarmTypeWithDescription: any;
  unknownSignalForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.unknownSignalConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.unknownSignalConfigId ? 'Update Unwanted Carrier Config':'Add Unwanted Carrier Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Unwanted Carrier Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 38} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.unknownSignalConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/unknown-signal-config/view' , queryParams: {id: this.unknownSignalConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createUnknownSignalForm();
    this.getAlarmTypeWithDescription();
    this.rxjsService.setGlobalLoaderProperty(false);
    if(this.unknownSignalConfigId){
      this.getUnknownSignalById(this.unknownSignalConfigId);
    }


  }

  createUnknownSignalForm(): void {
    let spikeConfigModel = new UnknownSignalConfigModel();
    // create form controls dynamically from model class
    this.unknownSignalForm = this.formBuilder.group({
      frequency: [null,[ Validators.required,Validators.min(1), Validators.max(999)]],
      duration: [null, [Validators.required, Validators.min(1), Validators.max(999)]],
      alarmId: ['', Validators.required],
      minimumSignalCountinFrequency:[null, [Validators.required,Validators.min(1), Validators.max(999)]]
    });
  }

  getUnknownSignalById(unknownSignalConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UNDEFINED_SIGNALS, unknownSignalConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          let openClosingSetupModel = new UnknownSignalConfigModel(response.resources);
          this.unknownDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.unknownDetails?.frequency ? this.title +' - '+ this.unknownDetails?.frequency : this.title + ' --', relativeRouterUrl: '' });
          this.unknownSignalForm.patchValue(openClosingSetupModel);

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }



  getAlarmTypeWithDescription() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypeWithDescription = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onSubmit(): void {
    if (this.unknownSignalForm.invalid) {
      return;
    }

    let formValue = this.unknownSignalForm.value;
    formValue.modifiedUserId = this.loggedUser.userId;
    let formValueUpdate = this.unknownSignalForm.value;
    formValueUpdate.unknownSignalConfigId = this.unknownSignalConfigId
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.unknownSignalConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UNDEFINED_SIGNALS, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UNDEFINED_SIGNALS, formValueUpdate)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=38');
      }
    })
  }

  onCRUDRequested(type){}

}
