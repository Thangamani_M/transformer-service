import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ROCommunityPatrolAddEditComponent } from './ro-community-patrol-add-edit.component';
import { ROCommunityPatrolRoutingModule } from './ro-community-patrol-routing.module';
import { ROCommunityPatrolViewComponent } from './ro-community-patrol-view.component';



@NgModule({
  declarations: [ROCommunityPatrolAddEditComponent, ROCommunityPatrolViewComponent],
  imports: [
    CommonModule,
    ROCommunityPatrolRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    ColorPickerModule
  ]
})
export class ROCommunityPatrolModule { }
