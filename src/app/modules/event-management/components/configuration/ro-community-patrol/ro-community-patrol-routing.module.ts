import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROCommunityPatrolAddEditComponent } from './ro-community-patrol-add-edit.component';
import { ROCommunityPatrolViewComponent } from './ro-community-patrol-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ROCommunityPatrolAddEditComponent, data: { title: 'RO Community Patrol Add/Edit' } },
  { path: 'view', component: ROCommunityPatrolViewComponent, data: { title: 'RO Community Patrol View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ROCommunityPatrolRoutingModule { }
