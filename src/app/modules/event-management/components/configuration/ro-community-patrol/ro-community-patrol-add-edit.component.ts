import { Component, OnInit, QueryList, ViewChild, ViewChildren, ViewContainerRef } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { ROCommunityPatrolModel, ROCommunityPatrolTypesListModel } from '@modules/event-management/models/configurations/ro-community-patrol.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-ro-community-patrol-add-edit',
  templateUrl: './ro-community-patrol-add-edit.component.html',
})
export class ROCommunityPatrolAddEditComponent implements OnInit {

  roCommunityPatrolId: any
  divisionDropDown: any = [];
  rOCommunityPatrolForm: FormGroup;
  rOCommunityPatrolTypesList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  rOCommunityPatrolDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  public colorCode: string = '#278ce2';
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, public vcRef: ViewContainerRef, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.roCommunityPatrolId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.roCommunityPatrolId ? 'Update RO Community Patrol':'Add RO Community Patrol';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'RO Community Patrol', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 13 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.roCommunityPatrolId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/ro-community-patrol/view' , queryParams: {id: this.roCommunityPatrolId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createROCommunityPatrolForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.roCommunityPatrolId) {
      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let cmcSmsGroup = new ROCommunityPatrolModel(response.resources);
        this.rOCommunityPatrolDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.rOCommunityPatrolDetails?.roCommunityPatrolName ? this.title +' - '+ this.rOCommunityPatrolDetails?.roCommunityPatrolName : this.title + ' --', relativeRouterUrl: '' });
        this.rOCommunityPatrolForm.patchValue(cmcSmsGroup);
        var rOCommunityPatrolDivisionsList = [];
        response.resources.rOCommunityPatrolDivisionsList.forEach(element => {
          rOCommunityPatrolDivisionsList.push(element.divisionId);
        });
        this.rOCommunityPatrolForm.get('rOCommunityPatrolDivisionsList').setValue(rOCommunityPatrolDivisionsList)
        this.rOCommunityPatrolTypesList = this.getrOCommunityPatrolTypeListArray;
        response.resources.rOCommunityPatrolTypesList.forEach((rOCommunityPatrolTypesListModel) => {
          rOCommunityPatrolTypesListModel.roCommunityPatrolTypeId = rOCommunityPatrolTypesListModel.roCommunityPatrolTypeId;
          this.rOCommunityPatrolTypesList.push(this.createROCommunityPatrolTypeList(rOCommunityPatrolTypesListModel));

        });
      })
    } else {
      this.rOCommunityPatrolTypesList = this.getrOCommunityPatrolTypeListArray;
      this.rOCommunityPatrolTypesList.push(this.createROCommunityPatrolTypeList());
    }
  }

  createROCommunityPatrolForm(): void {
    let rOCommunityPatrolModel = new ROCommunityPatrolModel();
    this.rOCommunityPatrolForm = this.formBuilder.group({
      rOCommunityPatrolTypesList: this.formBuilder.array([])
    });
    Object.keys(rOCommunityPatrolModel).forEach((key) => {
      this.rOCommunityPatrolForm.addControl(key, new FormControl(rOCommunityPatrolModel[key]));
    });
    this.rOCommunityPatrolForm = setRequiredValidator(this.rOCommunityPatrolForm, ["roCommunityPatrolName", "description", "rOCommunityPatrolDivisionsList"]);
    this.rOCommunityPatrolForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.rOCommunityPatrolForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

 
  public onChangeColor(color: string, i): any {
    if (color) {
      this.rOCommunityPatrolForm.get('rOCommunityPatrolTypesList')['controls'][i].controls['colorCode'].setValue(color)
      this.rxjsService.setFormChangeDetectionProperty(true);
    } else {
      this.rOCommunityPatrolForm.get('rOCommunityPatrolTypesList')['controls'][i].controls['colorCode'].setValue(null)
      this.rxjsService.setFormChangeDetectionProperty(true);
    }
    return color;
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  
  //Create FormArray
  get getrOCommunityPatrolTypeListArray(): FormArray {
    if (!this.rOCommunityPatrolForm) return;
    return this.rOCommunityPatrolForm.get("rOCommunityPatrolTypesList") as FormArray;
  }


  //Create FormArray controls
  createROCommunityPatrolTypeList(rOCommunityPatrolTypesListModel?: ROCommunityPatrolTypesListModel): FormGroup {
    let rOCommunityPatrolTypesListFormControlModel = new ROCommunityPatrolTypesListModel(rOCommunityPatrolTypesListModel);
    let formControls = {};
    Object.keys(rOCommunityPatrolTypesListFormControlModel).forEach((key) => {
      if (key === 'roCommunityPatrolTypeName' || key === 'roCommunityPatrolTypeId' || key === 'colorCode') {
        formControls[key] = [{ value: rOCommunityPatrolTypesListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.roCommunityPatrolId) {
        formControls[key] = [{ value: rOCommunityPatrolTypesListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: rOCommunityPatrolTypesListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.RO_COMMUNITY_PATROL,
      this.roCommunityPatrolId
    );
  }

  onChange(i?) {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Patrol type already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getrOCommunityPatrolTypeListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.roCommunityPatrolTypeName)) {
        duplicate.push(k.value.roCommunityPatrolTypeName);
      }
      filterKey.push(k.value.roCommunityPatrolTypeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }


  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (!this.onChange()) {
      return;
    }
    if (this.getrOCommunityPatrolTypeListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.rOCommunityPatrolTypesList = this.getrOCommunityPatrolTypeListArray;
    let rOCommunityPatrolTypesListModel = new ROCommunityPatrolTypesListModel();
    this.rOCommunityPatrolTypesList.insert(0, this.createROCommunityPatrolTypeList(rOCommunityPatrolTypesListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    if (this.getrOCommunityPatrolTypeListArray.controls[i].value.roCommunityPatrolTypeName == '') {
      this.getrOCommunityPatrolTypeListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getrOCommunityPatrolTypeListArray.controls[i].value.roCommunityPatrolTypeId && this.getrOCommunityPatrolTypeListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RO_COMMUNITY_PATROL_TYPES,
          this.getrOCommunityPatrolTypeListArray.controls[i].value.roCommunityPatrolTypeId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getrOCommunityPatrolTypeListArray.removeAt(i);
            }
            if (this.getrOCommunityPatrolTypeListArray.length === 0) {
              this.addCmcSmsGroupEmployee();
            };
          });
      }
      else {
        this.getrOCommunityPatrolTypeListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.rOCommunityPatrolForm.controls.rOCommunityPatrolDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions(e?) {
    if (this.allSelectedDivision.selected) {
      this.rOCommunityPatrolForm.controls.rOCommunityPatrolDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.rOCommunityPatrolForm.controls.rOCommunityPatrolDivisionsList.patchValue([]);
    }
  }

  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }
    if (this.rOCommunityPatrolForm.invalid) {
      return;
    }
    let formValue = this.rOCommunityPatrolForm.value;
    if (formValue.rOCommunityPatrolDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.rOCommunityPatrolDivisionsList = formValue.rOCommunityPatrolDivisionsList.filter(item => item != '')
      formValue.rOCommunityPatrolDivisionsList = formValue.rOCommunityPatrolDivisionsList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.roCommunityPatrolId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RO_COMMUNITY_PATROL, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RO_COMMUNITY_PATROL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=13');
      }
    })
  }

  onCRUDRequested(type){}

}
