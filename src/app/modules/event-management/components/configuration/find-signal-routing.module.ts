import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FindSignalComponent } from './find-signal/find-signal.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  {
    path: '', component: FindSignalComponent, data: { title: 'Find Signal' },canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class FindSignalRoutingModule { }
