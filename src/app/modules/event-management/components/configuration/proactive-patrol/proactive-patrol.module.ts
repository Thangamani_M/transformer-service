import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ProactivePatrolAddEditComponent } from './proactive-patrol-add-edit.component';
import { ProactivePatrolRoutingModule } from './proactive-patrol-routing.module';
import { ProactivePatrolViewComponent } from './proactive-patrol-view.component';



@NgModule({
  declarations: [ProactivePatrolAddEditComponent, ProactivePatrolViewComponent],
  imports: [
    CommonModule,
    ProactivePatrolRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    ColorPickerModule
  ]
})
export class ProactivePatrolModule { }
