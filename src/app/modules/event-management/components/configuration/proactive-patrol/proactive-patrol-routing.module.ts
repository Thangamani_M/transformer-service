import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProactivePatrolAddEditComponent } from './proactive-patrol-add-edit.component';
import { ProactivePatrolViewComponent } from './proactive-patrol-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ProactivePatrolAddEditComponent, data: { title: 'Proactive Patrol Add/Edit' } },
  { path: 'view', component: ProactivePatrolViewComponent, data: { title: 'Proactive Patrol View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ProactivePatrolRoutingModule { }
