import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { ProactivePatrolModel, ProactivePatrolTypeListModel } from '@modules/event-management/models/configurations/proactive-patrol.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-proactive-patrol-add-edit',
  templateUrl: './proactive-patrol-add-edit.component.html',
})
export class ProactivePatrolAddEditComponent implements OnInit {
  proactivePatrolId: any
  divisionDropDown: any = [];
  proactivePatrolForm: FormGroup;
  proactivePatrolTypeList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  cmcSmsGroupDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  public colorCode: string = '#278ce2';
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.proactivePatrolId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.proactivePatrolId ? 'Update Proactive Patrol':'Add Proactive Patrol';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Proactive Patrol', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 4 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.proactivePatrolId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/proactive-patrol/view' , queryParams: {id: this.proactivePatrolId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createproactivePatrolForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.proactivePatrolId) {

      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let cmcSmsGroup = new ProactivePatrolModel(response.resources);

        this.cmcSmsGroupDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.cmcSmsGroupDetails?.proactivePatrolName ? this.title +' - '+ this.cmcSmsGroupDetails?.proactivePatrolName : this.title + ' --', relativeRouterUrl: '' });
        this.proactivePatrolForm.patchValue(cmcSmsGroup);

        var proactivePatrolDivisionList = [];
        response.resources.proactivePatrolDivisionList.forEach(element => {
          proactivePatrolDivisionList.push(element.divisionId);
        });
        this.proactivePatrolForm.get('proactivePatrolDivisionList').setValue(proactivePatrolDivisionList)

        this.proactivePatrolTypeList = this.getproactivePatrolTypeListArray;
        response.resources.proactivePatrolTypeList.forEach((proactivePatrolTypeListModel) => {
          proactivePatrolTypeListModel.proactivePatrolTypeId = proactivePatrolTypeListModel.proactivePatrolTypeId;
          this.proactivePatrolTypeList.push(this.createproactivePatrolTypeListModel(proactivePatrolTypeListModel));

        });
      })
    } else {
      this.proactivePatrolTypeList = this.getproactivePatrolTypeListArray;
      this.proactivePatrolTypeList.push(this.createproactivePatrolTypeListModel());
    }
  }

  createproactivePatrolForm(): void {
    let proactivePatrolModel = new ProactivePatrolModel();
    this.proactivePatrolForm = this.formBuilder.group({
      proactivePatrolTypeList: this.formBuilder.array([])
    });
    Object.keys(proactivePatrolModel).forEach((key) => {
      this.proactivePatrolForm.addControl(key, new FormControl(proactivePatrolModel[key]));
    });
    this.proactivePatrolForm = setRequiredValidator(this.proactivePatrolForm, ["proactivePatrolName", "description", "proactivePatrolDivisionList"]);
    this.proactivePatrolForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.proactivePatrolForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getproactivePatrolTypeListArray(): FormArray {
    if (!this.proactivePatrolForm) return;
    return this.proactivePatrolForm.get("proactivePatrolTypeList") as FormArray;
  }


  //Create FormArray controls
  createproactivePatrolTypeListModel(proactivePatrolTypeListModel?: ProactivePatrolTypeListModel): FormGroup {
    let proactivePatrolTypeListFormControlModel = new ProactivePatrolTypeListModel(proactivePatrolTypeListModel);
    let formControls = {};
    Object.keys(proactivePatrolTypeListFormControlModel).forEach((key) => {
      if (key === 'proactivePatrolTypeName' || key === 'proactivePatrolTypeId' || key === 'colorCode') {
        formControls[key] = [{ value: proactivePatrolTypeListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.proactivePatrolId) {
        formControls[key] = [{ value: proactivePatrolTypeListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: proactivePatrolTypeListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.PROACTIVE_PATROL,
      this.proactivePatrolId
    );
  }
  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Patrol Type already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getproactivePatrolTypeListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.proactivePatrolTypeName)) {
        duplicate.push(k.value.proactivePatrolTypeName);
      }
      filterKey.push(k.value.proactivePatrolTypeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }



  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (!this.onChange()) {
      return;
    }

    if (this.getproactivePatrolTypeListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.proactivePatrolTypeList = this.getproactivePatrolTypeListArray;
    let proactivePatrolTypeListModel = new ProactivePatrolTypeListModel();
    this.proactivePatrolTypeList.insert(0, this.createproactivePatrolTypeListModel(proactivePatrolTypeListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    if (this.getproactivePatrolTypeListArray.controls[i].value.proactivePatrolTypeName == '') {
      this.getproactivePatrolTypeListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getproactivePatrolTypeListArray.controls[i].value.proactivePatrolTypeId && this.getproactivePatrolTypeListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PROACTIVE_PATROL_TYPE,
          this.getproactivePatrolTypeListArray.controls[i].value.proactivePatrolTypeId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getproactivePatrolTypeListArray.removeAt(i);
            }
            if (this.getproactivePatrolTypeListArray.length === 0) {
              this.addCmcSmsGroupEmployee();
            };
          });
      }
      else {
        this.getproactivePatrolTypeListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.proactivePatrolForm.controls.proactivePatrolDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.proactivePatrolForm.controls.proactivePatrolDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.proactivePatrolForm.controls.proactivePatrolDivisionList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }

    if (this.proactivePatrolForm.invalid) {
      return;
    }
    let formValue = this.proactivePatrolForm.value;
    if (formValue.proactivePatrolDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.proactivePatrolDivisionList = formValue.proactivePatrolDivisionList.filter(item => item != '')
      formValue.proactivePatrolDivisionList = formValue.proactivePatrolDivisionList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.proactivePatrolId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PROACTIVE_PATROL, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PROACTIVE_PATROL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=4');
      }
    })
  }

  onCRUDRequested(type){}

}
