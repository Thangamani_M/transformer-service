import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StackHistoryComponent } from './stack-history/stack-history.component';
import { StackWinelandsSignalHistoryComponent } from './stack-winelands/signal-history/signal-history.component';



const routes: Routes = [
  {
    path: '', component: StackHistoryComponent, data: { title: 'Stack History' },
  },
  {
    path: 'signal-history', component: StackWinelandsSignalHistoryComponent, data: { title: 'Signal History' }
  },
  { path: 'call-workflow-list', loadChildren: () => import('../configuration/call-workflow/call-workflow-list.module').then(m => m.CallWorkFlowListModule) },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StackHistoryRoutingModule { }
