import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-stack-history-details',
  templateUrl: './stack-history-details.component.html',
})
export class StackHistoryDetailsComponent implements OnInit {

  constructor(public ref: DynamicDialogRef,public config: DynamicDialogConfig,) {

   }

  ngOnInit(): void {
  }

}
