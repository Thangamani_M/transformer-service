import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, QueryList, SimpleChange, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, debounceTimeForSearchkeyword, HttpCancelService, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { SignalrConnectionService } from '@app/shared/services/signalr-connection.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { StackHistoryModalComponent } from './stack-history-modal.component';

@Component({
  selector: 'app-stack-history',
  templateUrl: './stack-history.component.html',
  styleUrls: ['./stack-history.component.scss'],
  styles: [`
        .loading-text {
            display: block;
            background-color: #f1f1f1;
            min-height: 19px;
            animation: pulse 1s infinite ease-in-out;
            text-indent: -99999px;
            overflow: hidden;
        }
    `],
  providers: [SignalrConnectionService]
})
export class StackHistoryComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  @ViewChildren('input') rows: QueryList<any>;
  @Input() stackConfigId: any;
  @Input() stackAreaConfigId: any;
  @Input() config: any;
  expanded: boolean;
  observableResponse;
  selectedTabIndex: any = 0;
  selectedIndex: any = 0;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  dataList: any = [];
  dataListScroll: any = [];
  loading: boolean;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedColumns: any[];
  selectedRows: any[] = [];
  totalRecords: any;
  pageLimit: number[] = [10, 50, 75, 100]
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any
  row: any = {}
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  selectedData: any = {};
  loggedUser: any;
  occurrenceBookId: any;
  selectedTabChangeIndex: any;
  selectedOccurrenceBookId: any;
  isAlarmTabShow: boolean = false;
  isSignalInfoTabShow: boolean = false;
  isRoMessageTabShow: boolean = false;
  pageSize: number = 10;
  firstDay = new Date();

  constructor(private crudService: CrudService, private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private signalrConnectionService: SignalrConnectionService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {

    this.activatedRoute.queryParams.subscribe(params => {
      if (params.isFindSignal) {
        this.selectedData['occurrenceBookId'] = params.occurrenceBookId ? params.occurrenceBookId : null;
      }
    });
    var date = new Date();
    this.firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

    this.primengTableConfigProperties = {
      tableCaption: "",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '' }, { displayName: 'Activity', relativeRouterUrl: '/event-management/supervisor-dashboard?tab=2' }, { displayName: 'History Stack' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stack History',
            dataKey: 'occurrenceBookId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: true,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'displayName', header: 'Client Name', width: '170px' }, { field: 'fullAddress', header: 'Address', width: '120px' }, { field: 'priority', header: 'Priority', width: '80px' }, { field: 'signalTime', header: 'Time', width: '70px' }, { field: 'subAreaName', header: 'Area', width: '70px' }, { field: 'operator', header: 'Operator', width: '80px' }, { field: 'message', header: 'Message', width: '120px' }, { field: 'incident', header: 'Incident', width: '70px' }, { field: 'vehicleStatus', header: 'Vehicle Status', width: '120px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.HISTORY_STACK,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            ebableFilterActionBtn: true
          },


        ]

      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });


  }

  ngOnInit(): void {
    if (!this.config) {
      this.openHistoryStockModal()
    }

    this.selectedTabIndex = 0;
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData()
    this.row['pageIndex'] = 0
    this.row['pageSize'] = 20
    if (this.config?.stackConfigId && this.config?.stackAreaConfigId) {
      this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'],)
    }
  }
  ngOnChanges(changes: SimpleChange): void {
    if (changes) {
      if (this.config?.stackConfigId) {
        this.getRequiredListData()
      }

    }

  }

  ngAfterViewInit() {
    const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-view')[0];
    scrollableBody.onscroll = (x) => {
      this.scrollEnabled = true;
      var st = Math.floor(scrollableBody.scrollTop + scrollableBody.offsetHeight)
      let max = scrollableBody.scrollHeight - 150;
      if (st >= max) {
        if (this.dataList.length < this.totalRecords) {
          this.row['pageIndex'] = 0
          this.row['pageSize'] = 20
          if (this.selectedData) {
            this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], this.selectedData);
          }

        }
      }
    }


  }


  openHistoryStockModal() {
    const ref = this.dialogService.open(StackHistoryModalComponent, {
      showHeader: false,
      width: '70%',
      height: '70%',
      data: { stackAreaConfigId: this.stackAreaConfigId, stackConfigId: this.stackConfigId }
    });
    ref.onClose.subscribe((result) => {
      this.isRoMessageTabShow = true
      Object.keys(result).forEach(key => {
        if (result[key] === "") {
          delete result[key]
        }
      });
      let searchColumns = Object.entries(result).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

      this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], searchColumns);
      this.selectedData = searchColumns;
    });
  }



  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          this.scrollEnabled = false
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.config?.stackConfigId && this.config?.stackAreaConfigId && this.isRoMessageTabShow == false) {
      const params = {
        stackConfigId: this.config?.stackConfigId,
        stackAreaConfigId: this.config?.stackAreaConfigId,
        createdUserId: this.loggedUser.userId,
        toDate: this.datePipe.transform(this.today, 'yyyy-MM-dd'),
        fromDate: this.datePipe.transform(this.firstDay, 'yyyy-MM-dd'),
      }
      otherParams = { ...otherParams, ...params };
      if (!this.config?.breadcrumFalse == false) {
        otherParams['isDefaultLoaderDisabled'] = true
      }
    }

    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.initialLoad) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    }
    this.initialLoad = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        if (!this.scrollEnabled) {
          this.dataList = []
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].rowExpantable = true;
          this.dataList = this.observableResponse;
        } else {
          this.observableResponse.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = data.totalCount;
        this.row['pageIndex'] = this.row['pageIndex'] 

      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.openHistoryStockModal()
        break;
      default:
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => { //to set default row count list
      table.rows = 20
    })

    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/event-management/signal-management'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()

  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onRowSelect(event) {
    if (event.data.occurrenceBookId) {
      this.selectedOccurrenceBookId = event.data.occurrenceBookId;
      this.isAlarmTabShow = true;
      this.isSignalInfoTabShow = true;
      this.isRoMessageTabShow = true;
      this.selectedTabChangeIndex = 0;
    }
  }

  onRowUnselect(event) {
    this.isAlarmTabShow = false;
    this.isSignalInfoTabShow = false;
    this.isRoMessageTabShow = false;
  }

  onClickExpantableRow(rowData) {
    if (rowData.occurrenceBookId) {
      this.selectedOccurrenceBookId = rowData.occurrenceBookId;
      this.isAlarmTabShow = true;
      this.isSignalInfoTabShow = true;
      this.isRoMessageTabShow = true;
      this.selectedTabChangeIndex = 0;
    }
  }

  onUnclickExpantableRow() {
    this.isAlarmTabShow = false;
    this.isSignalInfoTabShow = false;
    this.isRoMessageTabShow = false;
  }

  onRightClick(e) { }

  openIncidentModal(rowData) { }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  ngOnDestroy(): void {
    this.signalrConnectionService.unObserveEvent();
  }

}