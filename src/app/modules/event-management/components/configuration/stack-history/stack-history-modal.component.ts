import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';

@Component({
  selector: 'stack-history-modal-component',
  templateUrl: './stack-history-modal.component.html',
  styleUrls: ['./stack-history.component.scss'],
})
export class StackHistoryModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  @Output() outputAreaName = new EventEmitter<any>();
  @Output() outputStackName = new EventEmitter<any>();


  leadId: string;
  stackWinelandsModalForm: FormGroup;
  public formData = new FormData();
  userData: any;
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  submitted = false;
  customerId: any;
  areaDropDown: any = [];
  stackDropDown: any = [];
  siteIdDropDown: any = [];
  sourceDropDown: any = [];
  areaName: any;
  stackName: any;
  todayDate: any;
  formDate = new Date();
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  startTodayDate=0;

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private crudService: CrudService, public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private momentService: MomentService,  private store: Store<AppState>) {
    this.leadId = this.activatedRoute.snapshot.queryParams.leadId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createStackWinelandsModalForm();
    this.getArea();
    this.getStack();
    this.getSiteIdDropdown();
    this.getSourceDropdown();
  }

  createStackWinelandsModalForm(): void {
    this.stackWinelandsModalForm = this.formBuilder.group({
      stackAreaConfigId: [''],
      stackConfigId: [''],
      fromDate: [''],
      toDate: [''],
      operatorId: [''],
      occurrenceBookId: [''],
      siteId: [''],
      oBNumber: [''],
      source: [''],
      operator: [''],
      dispatcher: [''],
      createdUserId: [this.userData.userId, Validators.required],
    });

    this.stackWinelandsModalForm = setRequiredValidator(this.stackWinelandsModalForm, ["fromDate", "toDate", "stackConfigId", "stackAreaConfigId"]);
  }




  getArea() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_AREA_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.areaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  getStack() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stackDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  getSiteIdDropdown() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_OPERATOR, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.siteIdDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  getSourceDropdown() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DATA_SOURCE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.sourceDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  onSubmit(): void {
    if (this.stackWinelandsModalForm.invalid) {
      this.stackWinelandsModalForm.markAllAsTouched();
      return;
    }
  let formValue =   this.stackWinelandsModalForm.value
    
  
   formValue.toDate = this.momentService.toDateTimeSeconds(this.stackWinelandsModalForm.get('toDate').value)
   formValue.fromDate = this.momentService.toDateTimeSeconds(this.stackWinelandsModalForm.get('fromDate').value)
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(formValue)
  }

  cancelDialog() {
    this.ref.close()
  }


  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
  
  minDateTODate: any;
  minDateTODate1: any;
  onFromDateChange(event) {
    let _date = this.stackWinelandsModalForm.get('fromDate').value;
    this.minDateTODate = new Date(_date);
    this.minDateTODate1 =new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate());
  }

}
