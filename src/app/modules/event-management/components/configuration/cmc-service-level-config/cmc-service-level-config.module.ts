import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { CMCServiceLevelConfigAddEditComponent } from './cmc-service-level-config-add-edit.component';
import { CMCServiceLevelConfigRoutingModule } from './cmc-service-level-config-routing.module';
import { CMCServiceLevelConfigViewComponent } from './cmc-service-level-config-view.component';

@NgModule({
  declarations: [CMCServiceLevelConfigAddEditComponent, CMCServiceLevelConfigViewComponent],
  imports: [
    CommonModule,
    CMCServiceLevelConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class CMCServiceLevelConfigModule { }
