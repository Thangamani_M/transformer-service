import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CMCServiceLevelConfigModel } from '@modules/event-management/models/configurations/cmc-service-level-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-cmc-service-level-config-add-edit',
  templateUrl: './cmc-service-level-config-add-edit.component.html',
})
export class CMCServiceLevelConfigAddEditComponent implements OnInit {

  cmcServiceLevelConfigId: any;
  cmcServiceLevelConfigDetails: any;
  cmcServiceLevelConfigForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.cmcServiceLevelConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.cmcServiceLevelConfigId ? 'Update CMC Service Level Config':'Add CMC Service Level Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'CMC Service Level Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 30 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.cmcServiceLevelConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/cmc-service-level-config/view' , queryParams: {id: this.cmcServiceLevelConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createCmcServiceLevelConfigForm();

    if (this.cmcServiceLevelConfigId) {
      this.getCmcServiceLevelConfigDetailsById(this.cmcServiceLevelConfigId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createCmcServiceLevelConfigForm(): void {
    let cMCServiceLevelConfigModel = new CMCServiceLevelConfigModel();
    // create form controls dynamically from model class
    this.cmcServiceLevelConfigForm = this.formBuilder.group({
      phonebackServiceLevel: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
      dispatchTimeServiceLevel: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
      finishTimeServiceLevel: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
    });
    Object.keys(cMCServiceLevelConfigModel).forEach((key) => {
      this.cmcServiceLevelConfigForm.addControl(key, new FormControl(cMCServiceLevelConfigModel[key]));
    });
    this.cmcServiceLevelConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.cmcServiceLevelConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.cmcServiceLevelConfigId) {
      this.cmcServiceLevelConfigForm.removeControl('cmcServiceLevelConfigId');
      this.cmcServiceLevelConfigForm.removeControl('modifiedUserId');
    } else {
      this.cmcServiceLevelConfigForm.removeControl('createdUserId');
    }
  }


  getCmcServiceLevelConfigDetailsById(cmcServiceLevelConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SERVICE_LEVEL_CONFIG, cmcServiceLevelConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.cmcServiceLevelConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.title, relativeRouterUrl: '' });
          this.cmcServiceLevelConfigForm.patchValue(this.cmcServiceLevelConfigDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.cmcServiceLevelConfigForm.invalid) {
      return;
    }
    let formValue = this.cmcServiceLevelConfigForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.cmcServiceLevelConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SERVICE_LEVEL_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SERVICE_LEVEL_CONFIG, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=30');
      }
    })
  }

  onCRUDRequested(type){}

}
