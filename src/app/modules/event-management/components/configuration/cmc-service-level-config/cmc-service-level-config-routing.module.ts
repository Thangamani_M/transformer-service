import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CMCServiceLevelConfigAddEditComponent } from './cmc-service-level-config-add-edit.component';
import { CMCServiceLevelConfigViewComponent } from './cmc-service-level-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CMCServiceLevelConfigAddEditComponent, data: { title: 'CMC Service Level Config Add/Edit' } },
  { path: 'view', component: CMCServiceLevelConfigViewComponent, data: { title: 'CMC Service Level Config View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CMCServiceLevelConfigRoutingModule { }
