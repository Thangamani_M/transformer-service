import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { CmcFeedbackAddEditComponent } from './cmc-feedback-add-edit.component';
import { CmcFeedbackRoutingModule } from './cmc-feedback-routing.module';
import { CmcFeedbackViewComponent } from './cmc-feedback-view.component';

@NgModule({
  declarations: [CmcFeedbackAddEditComponent, CmcFeedbackViewComponent],
  imports: [
    CommonModule,
    CmcFeedbackRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class CmcFeedbackModule { }
