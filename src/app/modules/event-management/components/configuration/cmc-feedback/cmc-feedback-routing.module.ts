import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CmcFeedbackAddEditComponent } from './cmc-feedback-add-edit.component';
import { CmcFeedbackViewComponent } from './cmc-feedback-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CmcFeedbackAddEditComponent, data: { title: 'CMC Feedback Add/Edit' } },
  { path: 'view', component: CmcFeedbackViewComponent, data: { title: 'CMC Feedback View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CmcFeedbackRoutingModule { }
