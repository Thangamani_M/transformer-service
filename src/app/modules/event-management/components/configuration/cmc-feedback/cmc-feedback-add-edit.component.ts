import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CMCFeedbackModel, CMCFeedbackStandByTimeLimitsListModel } from '@modules/event-management/models/configurations/cmc-feedback.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cmc-feedback-add-edit',
  templateUrl: './cmc-feedback-add-edit.component.html',
})
export class CmcFeedbackAddEditComponent implements OnInit {

  cmcFeedbackId: any
  divisionDropDown: any = [];
  checkListDropDown: any = [];
  cmcFeedbackForm: FormGroup;
  cmcFeedbackStandByTimeLimitsList: FormArray;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  incidentDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.cmcFeedbackId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title = this.cmcFeedbackId ? 'Update CMC Feedback':'Add CMC Feedback';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'CMC Feedback', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 8 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.cmcFeedbackId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/cmc-feedback/view' , queryParams: {id: this.cmcFeedbackId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createcmcFeedbackForm();
    this.getDivisionDropDownById();
    this.getCheckListDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.cmcFeedbackId) {
      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let cmcSmsGroup = new CMCFeedbackModel(response.resources);
        this.incidentDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.incidentDetails?.cmcFeedbackName ? this.title +' - '+ this.incidentDetails?.cmcFeedbackName : this.title + ' --', relativeRouterUrl: '' });
        this.cmcFeedbackForm.patchValue(cmcSmsGroup);
        var cmcFeedbackDivisionsList = [];
        response.resources.cmcFeedbackDivisionsList.forEach(element => {
          cmcFeedbackDivisionsList.push(element.divisionId);
        });
        this.cmcFeedbackForm.get('cmcFeedbackDivisionsList').setValue(cmcFeedbackDivisionsList)
        this.cmcFeedbackStandByTimeLimitsList = this.getcmcFeedbackStandByTimeLimitsListArray;
        response.resources.cmcFeedbackStandByTimeLimitsList.forEach((cmcFeedbackStandByTimeLimitsListModel: CMCFeedbackStandByTimeLimitsListModel) => {
          this.cmcFeedbackStandByTimeLimitsList.push(this.createcmcFeedbackStandByTimeLimitsListModel(cmcFeedbackStandByTimeLimitsListModel));
        });
      })
    }
  }

  createcmcFeedbackForm(): void {
    let incidentModel = new CMCFeedbackModel();
    this.cmcFeedbackForm = this.formBuilder.group({
      cmcFeedbackStandByTimeLimitsList: this.formBuilder.array([])
    });
    Object.keys(incidentModel).forEach((key) => {
      this.cmcFeedbackForm.addControl(key, new FormControl(incidentModel[key]));
    });
    this.cmcFeedbackForm = setRequiredValidator(this.cmcFeedbackForm, ["cmcFeedbackName", "description", "cmcFeedbackDivisionsList"]);
    this.cmcFeedbackForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.cmcFeedbackForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCheckListDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CMC_FEEDBACK_STANDBY_ITEMS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.checkListDropDown = response.resources;
          if (!this.cmcFeedbackId) {
            this.cmcFeedbackStandByTimeLimitsList = this.getcmcFeedbackStandByTimeLimitsListArray;
            response.resources.forEach((cmcFeedbackStandByTimeLimitsListModel: any) => {
              cmcFeedbackStandByTimeLimitsListModel.cmcFeedbackStandByItemId = cmcFeedbackStandByTimeLimitsListModel.id
              this.cmcFeedbackStandByTimeLimitsList.push(this.createcmcFeedbackStandByTimeLimitsListModel(cmcFeedbackStandByTimeLimitsListModel));
            });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getcmcFeedbackStandByTimeLimitsListArray(): FormArray {
    if (!this.cmcFeedbackForm) return;
    return this.cmcFeedbackForm.get("cmcFeedbackStandByTimeLimitsList") as FormArray;
  }

  //Create FormArray controls
  createcmcFeedbackStandByTimeLimitsListModel(cmcFeedbackStandByTimeLimitsListModel?: CMCFeedbackStandByTimeLimitsListModel): FormGroup {
    let cmcFeedbackStandByTimeLimitsListFormControlModel = new CMCFeedbackStandByTimeLimitsListModel(cmcFeedbackStandByTimeLimitsListModel);
    let formControls = {};
    Object.keys(cmcFeedbackStandByTimeLimitsListFormControlModel).forEach((key) => {
      if (key === 'cmcFeedbackStandByItemId' || key === 'standOffSeconds' || key === 'cmcEscalationSeconds') {
        formControls[key] = [{ value: cmcFeedbackStandByTimeLimitsListFormControlModel[key], disabled: false }, [Validators.required]];
        formControls['standOffSeconds'] = [{ value: cmcFeedbackStandByTimeLimitsListFormControlModel['standOffSeconds'], disabled: false }, [Validators.required, Validators.min(10), Validators.max(99999999)]];
        formControls['cmcEscalationSeconds'] = [{ value: cmcFeedbackStandByTimeLimitsListFormControlModel['cmcEscalationSeconds'], disabled: false }, [Validators.required, Validators.min(10), Validators.max(99999999)]];
      } else if (this.cmcFeedbackId) {
        formControls[key] = [{ value: cmcFeedbackStandByTimeLimitsListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CMC_FEEDBACK,
      this.cmcFeedbackId
    );
  }

  OnChange(index): boolean {
    if (this.getcmcFeedbackStandByTimeLimitsListArray.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.getcmcFeedbackStandByTimeLimitsListArray.value.slice();
      cloneArray.splice(index, 1);

      var findIndex = cloneArray.some(x => x.cmcFeedbackStandByItemId === this.getcmcFeedbackStandByTimeLimitsListArray.value[index].cmcFeedbackStandByItemId);
      if (findIndex) {
        this.snackbarService.openSnackbar("Vehicle check item already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }

    }
  }


  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (this.getcmcFeedbackStandByTimeLimitsListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Vehicle check item already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.cmcFeedbackStandByTimeLimitsList = this.getcmcFeedbackStandByTimeLimitsListArray;
    let cmcFeedbackStandByTimeLimitsListModel = new CMCFeedbackStandByTimeLimitsListModel();
    this.cmcFeedbackStandByTimeLimitsList.insert(0, this.createcmcFeedbackStandByTimeLimitsListModel(cmcFeedbackStandByTimeLimitsListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    if (this.getcmcFeedbackStandByTimeLimitsListArray.controls[i].value.incidentTypeName == '') {
      this.getcmcFeedbackStandByTimeLimitsListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getcmcFeedbackStandByTimeLimitsListArray.controls[i].value.cmcsmsGroupEmployeeId && this.getcmcFeedbackStandByTimeLimitsListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_FEEDBACK,
          this.getcmcFeedbackStandByTimeLimitsListArray.controls[i].value.cmcsmsGroupEmployeeId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.getcmcFeedbackStandByTimeLimitsListArray.removeAt(i);
            }
            if (this.getcmcFeedbackStandByTimeLimitsListArray.length === 0) {
              this.addCmcSmsGroupEmployee();
            };
          });
      }
      else {
        this.getcmcFeedbackStandByTimeLimitsListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.cmcFeedbackForm.controls.cmcFeedbackDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.cmcFeedbackForm.controls.cmcFeedbackDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.cmcFeedbackForm.controls.cmcFeedbackDivisionsList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (this.cmcFeedbackForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Vehicle check item already exist", ResponseMessageTypes.WARNING);
      return;
    }
    let formValue = this.cmcFeedbackForm.value;
    if (formValue.cmcFeedbackDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.cmcFeedbackDivisionsList = formValue.cmcFeedbackDivisionsList.filter(item => item != '')
      formValue.cmcFeedbackDivisionsList = formValue.cmcFeedbackDivisionsList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.cmcFeedbackId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_FEEDBACK, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_FEEDBACK, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=8');
      }
    })
  }

  onCRUDRequested(type){}

}
