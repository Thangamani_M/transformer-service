import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlarmTypeConfigAddEditComponent } from './alarm-type-config-add-edit.component';
import { AlarmTypeConfigViewComponent } from './alarm-type-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: AlarmTypeConfigAddEditComponent, data: { title: 'Alarm Type Config Add/Edit' } },
  { path: 'view', component: AlarmTypeConfigViewComponent, data: { title: 'Alarm Type Config View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class AlarmTypeConfigRoutingModule { }
