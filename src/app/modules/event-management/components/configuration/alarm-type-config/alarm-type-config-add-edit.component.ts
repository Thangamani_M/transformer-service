import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AlarmTypeConfigListListModel, AlarmTypeConfigModel } from '../../../models/configurations/alarm-type-config-model';
@Component({
  selector: 'app-alarm-type-config-add-edit',
  templateUrl: './alarm-type-config-add-edit.component.html',
})
export class AlarmTypeConfigAddEditComponent implements OnInit {

  alarmTypeConfigId: any;
  alarmTypeList: any = [];
  divisionDropDown: any = [];
  alarmTypeConfigForm: FormGroup;
  alarmTypeConfigListList: FormArray;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  alarmConfigTypeDetails: any;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.alarmTypeConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.alarmTypeConfigId ? 'Update Alarm Type Config':'Add Alarm Type Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Alarm Type Config', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 6 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.alarmTypeConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/alarm-type-config/view' , queryParams: {id: this.alarmTypeConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createAlarmTypeConfigForm();
    this.getDivisionDropDownById();
    this.getAlarmTypeList()
    if (this.alarmTypeConfigId) {
      this.getAlarmConfigTypeDetailsById().subscribe((response: IApplicationResponse) => {
        let alarmTypeConfig = new AlarmTypeConfigModel(response.resources);
        this.alarmConfigTypeDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.alarmConfigTypeDetails?.alarmTypeConfigName ? this.title +' - '+ this.alarmConfigTypeDetails?.alarmTypeConfigName : this.title + ' --', relativeRouterUrl: '' });

        this.alarmTypeConfigForm.patchValue(alarmTypeConfig);
        var alarmTypeConfigDivisionList = [];
        response.resources.alarmTypeConfigDivisionList.forEach(element => {
          alarmTypeConfigDivisionList.push(element.divisionId);
        });
        this.alarmTypeConfigForm.get('alarmTypeConfigDivisionList').setValue(alarmTypeConfigDivisionList)
        this.alarmTypeConfigListList = this.getalarmTypeConfigListListArray;
        response.resources.alarmTypeConfigListList.forEach((alarmTypeConfigListListModel: AlarmTypeConfigListListModel) => {
          this.alarmTypeConfigListList.push(this.createAlarmTypeConfigListModel(alarmTypeConfigListListModel));
        });
      })
    }
  }

  createAlarmTypeConfigForm(): void {
    let alarmTypeConfigModel = new AlarmTypeConfigModel();
    this.alarmTypeConfigForm = this.formBuilder.group({
      alarmTypeConfigListList: this.formBuilder.array([])
    });
    Object.keys(alarmTypeConfigModel).forEach((key) => {
      this.alarmTypeConfigForm.addControl(key, new FormControl(alarmTypeConfigModel[key]));
    });
    this.alarmTypeConfigForm = setRequiredValidator(this.alarmTypeConfigForm, ["alarmTypeConfigName", "alarmTypeConfigDivisionList", "description"]);
    this.alarmTypeConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.alarmTypeConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.alarmTypeConfigId) {
      this.alarmTypeConfigForm.removeControl('alarmTypeConfigId');
      this.alarmTypeConfigForm.removeControl('modifiedUserId');
    } else {
      this.alarmTypeConfigForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAlarmTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_CONFIG_LIST, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypeList = response.resources;
          if (!this.alarmTypeConfigId) {
            this.alarmTypeConfigListList = this.getalarmTypeConfigListListArray;
            response.resources.forEach((alarmTypeConfigListListModel: any) => {
              alarmTypeConfigListListModel.alarmTypeId = alarmTypeConfigListListModel.alarmTypeId;
              alarmTypeConfigListListModel.isAutoDispatchMode = true
              alarmTypeConfigListListModel.alarmTypeDescription = alarmTypeConfigListListModel.alarmTypeDescription;
              this.alarmTypeConfigListList.push(this.createAlarmTypeConfigListModel(alarmTypeConfigListListModel));
            });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  //Get Details 
  getAlarmConfigTypeDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ALARM_TYPE_CONFIG,
      this.alarmTypeConfigId
    );
  }
  get getalarmTypeConfigListListArray(): FormArray {
    if (!this.alarmTypeConfigForm) return;
    return this.alarmTypeConfigForm.get("alarmTypeConfigListList") as FormArray;
  }
  //Create FormArray controls
  createAlarmTypeConfigListModel(alarmTypeConfigListListModel?: AlarmTypeConfigListListModel): FormGroup {
    let alarmTypeConfigListListFormControlModel = new AlarmTypeConfigListListModel(alarmTypeConfigListListModel);
    let formControls = {};
    Object.keys(alarmTypeConfigListListFormControlModel).forEach((key) => {
      if (key === 'alarmTypeId' || key === 'alarmTypeDescription' || key === 'isAutoDispatchMode' || key === 'isNormalModeRequest' || key === 'isStormModeRequest') {
        formControls[key] = [{ value: alarmTypeConfigListListFormControlModel[key], disabled: false }]
      } else if (this.alarmTypeConfigId) {
        formControls[key] = [{ value: alarmTypeConfigListListFormControlModel[key], disabled: false }]
      }
    });
    let forms = this.formBuilder.group(formControls);
    forms.get('isAutoDispatchMode').valueChanges.subscribe(val => {
      if (val) {
        forms.controls['isNormalModeRequest'].setValue(true)
        forms.controls['isStormModeRequest'].setValue(true)
      }
      else {
        forms.controls['isNormalModeRequest'].setValue(false)
        forms.controls['isStormModeRequest'].setValue(false)
      }
    })
    return forms
  }

  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.alarmTypeConfigForm.controls.alarmTypeConfigDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.alarmTypeConfigForm.controls.alarmTypeConfigDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.alarmTypeConfigForm.controls.alarmTypeConfigDivisionList.patchValue([]);
    }
  }
  isChecked: boolean;
  isAutoDispatchMode: boolean;

  onSubmit(): void {
    if (this.alarmTypeConfigForm.invalid) {
      return;
    }
    let formValue = this.alarmTypeConfigForm.value;
    if (formValue.alarmTypeConfigDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.alarmTypeConfigDivisionList = formValue.alarmTypeConfigDivisionList.filter(item => item != '')
      formValue.alarmTypeConfigDivisionList = formValue.alarmTypeConfigDivisionList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.alarmTypeConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=6');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}