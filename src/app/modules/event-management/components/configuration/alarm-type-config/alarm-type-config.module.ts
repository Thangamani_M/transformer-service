import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { AlarmTypeConfigAddEditComponent } from './alarm-type-config-add-edit.component';
import { AlarmTypeConfigRoutingModule } from './alarm-type-config-routing.module';
import { AlarmTypeConfigViewComponent } from './alarm-type-config-view.component';

@NgModule({
  declarations: [AlarmTypeConfigAddEditComponent, AlarmTypeConfigViewComponent],
  imports: [
    CommonModule,
    AlarmTypeConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class AlarmTypeConfigModule { }
