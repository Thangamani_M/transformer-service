import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-distribution-group-view',
  templateUrl: './distribution-group-view.component.html',
})
export class DistributionGroupViewComponent implements OnInit {
  
distributionGroupId: any
distributionGroupDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any
distrubutionDetails: any;
totaltab = 0
selectedTab = 0
pageLevelProperties: any = {
  tableComponentConfigs: {
    tabsList: []
  }
}

constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.distributionGroupId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption: 'View Distribution Group',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
    { displayName: 'Distribution Group', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 17 } }
      , { displayName: '', }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: true,
          enableBreadCrumb: true,
          enableViewBtn:true
        }
      ]
    }
  }
}

ngOnInit(): void {
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  }
  this.combineLatestNgrxStoreData()
  if (this.distributionGroupId) {
    this.getdistributionGroupDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.onShowValue(response.resources);
      this.distributionGroupDetails = response.resources.distributionGroupEmployeeList;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + response.resources.distributionGroupName;
    })
  }
}

combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onShowValue(response?: any) {
  this.distrubutionDetails = [
    { name: 'Distribution Group', value: response?.distributionGroupName },
    { name: 'Description', value: response?.description },
    { name: 'Division', value: response?.divisionName },
    { name: 'Main Area', value: response?.distributionGroupMainAreas },
    { name: 'Created On', value: response?.createdDate, isDateTime: true },
    { name: 'Modified On', value: response?.modifiedDate ,isDateTime: true},
    { name: 'Created By', value: response?.createdUserName },
    { name: 'Modified By', value: response?.modifiedUserName },
    { name: 'Status', value: response.isActive == true ? 'Active' : 'In-Active', statusClass: response.isActive == true ? "status-label-green" : 'status-label-red' },
  ]
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(['event-management/event-configuration/distribution-group/add-edit'], { queryParams: { id: this.distributionGroupId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
}

getdistributionGroupDetails(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.DISTRIBUTION_GROUP,
    this.distributionGroupId
  );
}

}
