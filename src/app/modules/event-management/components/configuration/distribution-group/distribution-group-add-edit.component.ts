import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, countryCodes, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { DistributionGroupEmployeeListModel, DistributionGroupModel } from '@modules/event-management/models/configurations/distribution-group-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-distribution-group-add-edit',
  templateUrl: './distribution-group-add-edit.component.html',
})
export class DistributionGroupAddEditComponent implements OnInit {

  distributionGroupId: any
  divisionDropDown: any = [];
  mainAreaList: any = [];
  distributionGroupForm: FormGroup;
  distributionGroupEmployeeList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  distributionGroupDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  countryCodes = countryCodes;
  selectedDivisionId: any;
  @ViewChildren('input') rows: QueryList<any>;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService) {
    this.distributionGroupId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.distributionGroupId ? 'Update Distribution Group':'Add Distribution Group';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Distribution Group', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 17 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.distributionGroupId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/distribution-group/view' , queryParams: {id: this.distributionGroupId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createDistributionGroupForm();
    this.getDivisionDropDownById();
    this.distributionGroupForm.get("divisionId").valueChanges.subscribe(data => {
      this.getMainAreaList(data);
    })
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.distributionGroupId) {
      this.getDistributionGroupDetailsById().subscribe((response: IApplicationResponse) => {
        this.distributionGroupDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.distributionGroupDetails?.distributionGroupName ? this.title +' - '+ this.distributionGroupDetails?.distributionGroupName : this.title + ' --', relativeRouterUrl: '' });
        this.distributionGroupDetails.distributionGroupMainAreaList.forEach(element => {
          element.id = element.mainAreaId
        });
        this.distributionGroupForm.patchValue(this.distributionGroupDetails);
        this.distributionGroupEmployeeList = this.getDistributionGroupEmployeeListArray;
        response.resources.distributionGroupEmployeeList.forEach((distributionGroupEmployeeListModel: DistributionGroupEmployeeListModel) => {
          distributionGroupEmployeeListModel.mobileNumber = distributionGroupEmployeeListModel.mobileNumber.replace(/^(\d{0,2})(\d{0,3})(\d{0,4})/, '$1 $2 $3');
          this.distributionGroupEmployeeList.push(this.createDistributionGroupEmployeeListModel(distributionGroupEmployeeListModel));
        });
      })
    } else {
      this.distributionGroupEmployeeList = this.getDistributionGroupEmployeeListArray;
      this.distributionGroupEmployeeList.push(this.createDistributionGroupEmployeeListModel());
    }
  }

  createDistributionGroupForm(): void {
    let distributionGroupModel = new DistributionGroupModel();
    this.distributionGroupForm = this.formBuilder.group({
      distributionGroupEmployeeList: this.formBuilder.array([])
    });

    Object.keys(distributionGroupModel).forEach((key) => {
      this.distributionGroupForm.addControl(key, new FormControl(distributionGroupModel[key]));
    });
    this.distributionGroupForm = setRequiredValidator(this.distributionGroupForm, ["distributionGroupName", "description", "divisionId", "distributionGroupMainAreaList"]);
    this.distributionGroupForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.distributionGroupForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.distributionGroupForm.addControl('distributionGroupEmployeeList', this.formBuilder.array([]));
  }

  removeDuplicatesBy(keyFn, array) {
    var mySet = new Set();
    return array.filter(function (x) {
      var key = keyFn(x), isNew = !mySet.has(key);
      if (isNew) mySet.add(key);
      return isNew;
    });
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getDistributionGroupEmployeeListArray(): FormArray {
    if (!this.distributionGroupForm) return;
    return this.distributionGroupForm.get("distributionGroupEmployeeList") as FormArray;
  }

  //Create FormArray controls
  createDistributionGroupEmployeeListModel(distributionGroupEmployeeListModel?: DistributionGroupEmployeeListModel): FormGroup {
    let distributionGroupEmployeeListFormControlModel = new DistributionGroupEmployeeListModel(distributionGroupEmployeeListModel);
    let formControls = {};
    Object.keys(distributionGroupEmployeeListFormControlModel).forEach((key) => {
      if (key === 'employeeName' || key === 'mobileNumberCountryCode' || key == 'mobileNumber') {
        formControls[key] = [{ value: distributionGroupEmployeeListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.distributionGroupId) {
        formControls[key] = [{ value: distributionGroupEmployeeListFormControlModel[key], disabled: false }]
      }
      else {
        formControls[key] = [{ value: distributionGroupEmployeeListFormControlModel[key], disabled: false }]
      }
    });
    let forms = this.formBuilder.group(formControls);
    forms.get("mobileNumberCountryCode")
      .valueChanges.subscribe((mobileNumberCountryCode: string) => {
        switch (mobileNumberCountryCode) {
          case "+27":
            forms.get("mobileNumber").setValidators([Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength),
            Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),])
            break;
          default:
            forms.get("mobileNumber").setValidators([Validators.minLength(
              formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),])
            break;
        }
        setTimeout(() => {
          this.focusInAndOutFormArrayFields();
        });
      });

    forms.get("mobileNumber")
      .valueChanges.subscribe((mobileNumber: string) => {
        switch (forms.get("mobileNumberCountryCode").value) {
          case "+27":
            forms.get("mobileNumber").setValidators([Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength),
            Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),])
            break;
          default:
            forms.get("mobileNumber").setValidators([Validators.minLength(
              formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),])
            break;
        }
      })
    return forms
  }

  //Get Details 
  getDistributionGroupDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.DISTRIBUTION_GROUP,
      this.distributionGroupId
    );
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Employee Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getDistributionGroupEmployeeListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.employeeName)) {
        duplicate.push(k.value.employeeName);
      }
      filterKey.push(k.value.employeeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChangeNumber() {
    const duplicate = this.duplicateNumber();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Mobile Number already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateNumber() {
    const filterKey = [];
    const duplicate = [];
    this.getDistributionGroupEmployeeListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.mobileNumber)) {
        duplicate.push(k.value.mobileNumber);
      }
      filterKey.push(k.value.mobileNumber);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Add Employee Details
  addDistributionGroupEmployee(): void {
    if (!this.onChange()) {
      return;
    }
    if (!this.onChangeNumber()) {
      return;
    }
    if (this.getDistributionGroupEmployeeListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.distributionGroupEmployeeList = this.getDistributionGroupEmployeeListArray;
    let distributionGroupEmployeeListModel = new DistributionGroupEmployeeListModel();
    this.distributionGroupEmployeeList.insert(0, this.createDistributionGroupEmployeeListModel(distributionGroupEmployeeListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeDistributionEmployee(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getDistributionGroupEmployeeListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getDistributionGroupEmployeeListArray.controls[i].value.distributionGroupEmployeeId && this.getDistributionGroupEmployeeListArray.length > 1) {
            this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISTRUBUTION_GROUP_EMPLOYEE,
              this.getDistributionGroupEmployeeListArray.controls[i].value.distributionGroupEmployeeId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.getDistributionGroupEmployeeListArray.removeAt(i);
                }
                if (this.getDistributionGroupEmployeeListArray.length === 0) {
                  this.addDistributionGroupEmployee();
                };
              });
          }
          else {
            this.getDistributionGroupEmployeeListArray.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }
    if (!this.onChangeNumber()) {
      return;
    }
    if (this.distributionGroupForm.invalid) {
      this.distributionGroupForm.markAllAsTouched();
      return;
    }
    let formValue = this.distributionGroupForm.value;
    formValue.distributionGroupMainAreaList.forEach(element => {
      element.mainAreaId = element.id
    });
    formValue.distributionGroupEmployeeList.forEach(element => {
      element.mobileNumber = element.mobileNumber.replace(/[^\d]/g, '');
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.distributionGroupId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISTRIBUTION_GROUP, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISTRIBUTION_GROUP, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=17');
      }
    })
  }

  onCRUDRequested(type){}
}