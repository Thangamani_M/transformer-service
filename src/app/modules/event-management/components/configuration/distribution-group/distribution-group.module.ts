import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { DistributionGroupAddEditComponent } from './distribution-group-add-edit.component';
import { DistributionGroupRoutingModule } from './distribution-group-routing.module';
import { DistributionGroupViewComponent } from './distribution-group-view.component';

@NgModule({
  declarations: [DistributionGroupAddEditComponent, DistributionGroupViewComponent],
  imports: [
    CommonModule,
    DistributionGroupRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
  ]
})
export class DistributionGroupModule { }