import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DistributionGroupAddEditComponent } from './distribution-group-add-edit.component';
import { DistributionGroupViewComponent } from './distribution-group-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: DistributionGroupAddEditComponent, data: { title: 'Distribution Group Add/Edit' } },
  { path: 'view', component: DistributionGroupViewComponent, data: { title: 'Distribution Group View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DistributionGroupRoutingModule { }