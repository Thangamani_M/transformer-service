import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';


@Component({
  selector: 'app-spike-identification-config-view',
  templateUrl: './spike-identification-config-view.component.html',
})
export class SpikeIdentificationConfigViewComponent implements OnInit {

spikeIdentificationConfigId: any;
spikeDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any;
totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
  tableComponentConfigs: {
    tabsList: []
  }
};

constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.spikeIdentificationConfigId = (Object.keys(params['params']).length > 0) ? params['params']['spikeIdentificationConfigId'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption: 'View Spike Identification Config',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
    { displayName: 'Spike Identification Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 37 } }
      , { displayName: '', }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  this.rxjsService.setGlobalLoaderProperty(false);
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  };
  this.combineLatestNgrxStoreData();
  if (this.spikeIdentificationConfigId) {
    this.GetSpikeDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.spikeDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + this.spikeDetails.windowPeriods;
      this.spikeDetails = [
        { name: 'Window Periods', value: response.resources.windowPeriods },
        { name: 'Week Review', value: response.resources.weekReview },
        { name: 'Deviation Value', value: response.resources.deviationValue},
        { name: 'Modified Date', value: response.resources.modifiedDate, isDateTime: true },
        { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}

combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(["/event-management/event-configuration/spike-identificaton-config/add-edit"], { queryParams: { spikeIdentificationConfigId: this.spikeIdentificationConfigId,totalTabs:this.totaltab, selectedTab:this.selectedTab  } });
}
  GetSpikeDetails():Observable <IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SPIKE_IDETIFICATION_CONFIG_POST,
      prepareGetRequestHttpParams(null, null, {
        spikeIdentificationConfigId: this.spikeIdentificationConfigId
      }));
  }

}
