import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { SpikeIdentificationConfigAddEditComponent } from './spike-identification-config-add-edit.component';
import { SpikeIdentificationConfigRoutingModule } from './spike-identification-config-routing.module';
import { SpikeIdentificationConfigViewComponent } from './spike-identification-config-view.component';


@NgModule({
  declarations: [SpikeIdentificationConfigAddEditComponent, SpikeIdentificationConfigViewComponent],
  imports: [
    CommonModule,
    SpikeIdentificationConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class SpikeIdentificationConfigModule { }
