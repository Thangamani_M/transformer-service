import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpikeIdentificationConfigAddEditComponent } from './spike-identification-config-add-edit.component';
import { SpikeIdentificationConfigViewComponent } from './spike-identification-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [{ path: '', redirectTo: 'add-edit', pathMatch: 'full' },
{ path: 'add-edit', component: SpikeIdentificationConfigAddEditComponent, data: { title: 'Spike Identification Config  Add/Edit' } },
{ path: 'view', component: SpikeIdentificationConfigViewComponent, data: { title: 'Spike Identification Config View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SpikeIdentificationConfigRoutingModule { }
