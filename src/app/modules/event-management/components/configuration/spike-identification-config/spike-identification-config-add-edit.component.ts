import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { SpikeIdentificationConfigModel } from '@modules/event-management/models/configurations/spike-identification-config-model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-spike-identification-config-add-edit',
  templateUrl: './spike-identification-config-add-edit.component.html',
})
export class SpikeIdentificationConfigAddEditComponent implements OnInit {

  spikeIdentificationConfigId: string;
  spikeDetails: any;
  divisionDropDown: any;
  spikeIdentificationForm: FormGroup;
  loggedUser: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  spickActive: any;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.spikeIdentificationConfigId = this.activatedRoute.snapshot.queryParams.spikeIdentificationConfigId;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.spikeIdentificationConfigId ? 'Update Spike Identification Config':'Add Spike Identification Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Spike Identification Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 37 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.spikeIdentificationConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/spike-identificaton-config/view' , queryParams: {spikeIdentificationConfigId: this.spikeIdentificationConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createDelayDurationForm();
    this.getDivisionDropDown();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getSpickIdentyFicationActive()
    if (this.spikeIdentificationConfigId) {
      this.GetSpikeDetails();
      return
    }

  }

  createDelayDurationForm(): void {
    let spikeConfigModel = new SpikeIdentificationConfigModel();
    this.spikeIdentificationForm = this.formBuilder.group({
      windowPeriods: ['', Validators.required],
      weekReview: ['', Validators.required],
      deviationValue: ['', Validators.required],
    });
    Object.keys(spikeConfigModel).forEach((key) => {
      this.spikeIdentificationForm.addControl(key, new FormControl(spikeConfigModel[key]));
    });
    this.spikeIdentificationForm = setRequiredValidator(this.spikeIdentificationForm, ["windowPeriods", 'weekReview', 'deviationValue']);
    this.spikeIdentificationForm.get('createdUserId').setValue(this.loggedUser.userId)
  }

  GetSpikeDetails() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SPIKE_IDETIFICATION_CONFIG_POST,
      prepareGetRequestHttpParams(null, null, {
        spikeIdentificationConfigId: this.spikeIdentificationConfigId
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.spikeDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.spikeDetails?.windowPeriods ? this.title +' - '+ this.spikeDetails?.windowPeriods : this.title + ' --', relativeRouterUrl: '' });
          this.spikeIdentificationForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getSpickIdentyFicationActive() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SPICK_IDENTYFICATION_ACTIIVE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.spickActive = response.resources
          this.spikeIdentificationForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SPIKE_IDENTIFICATION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
      });
  }

  onSubmit(): void {
    if (this.spikeIdentificationForm.invalid) {
      return;
    }
    let formValue = this.spikeIdentificationForm.value;
    formValue.createdDate = '2021-08-20T06:20:24.7z'
    let formUpdate = this.spikeIdentificationForm.value
    formUpdate.modifiedUserId = this.loggedUser.userId;
    formUpdate.modifiedDate = '2021-08-20T06:20:24.7z';
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.spikeIdentificationConfigId && this.spickActive.length == 1) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SPIKE_IDETIFICATION_CONFIG_POST, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SPIKE_IDETIFICATION_CONFIG_POST, formUpdate)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=37');
      }
    })
  }

  onCRUDRequested(type){}

}
