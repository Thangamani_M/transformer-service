import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { OpenAreaRoutingModule } from './open-area-routing.module';
import { OpenAreaViewComponent } from './open-area-view/open-area-view.component';
import { OpenAreaAddEditComponent } from './open-area.add-edit.component';




@NgModule({
  declarations: [OpenAreaAddEditComponent, OpenAreaViewComponent],
  imports: [
    CommonModule,
    OpenAreaRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class OpenAreaModule { }
