import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OpenAreaViewComponent } from './open-area-view/open-area-view.component';
import { OpenAreaAddEditComponent } from './open-area.add-edit.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: OpenAreaAddEditComponent, data: { title: 'Open Area Add/Edit' } },
  { path: 'view', component:OpenAreaViewComponent , data: { title: 'Open Area View' },canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class OpenAreaRoutingModule { }
