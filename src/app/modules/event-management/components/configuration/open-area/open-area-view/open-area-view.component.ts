import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { RxjsService, CrudService, ModulesBasedApiSuffix, IApplicationResponse, CrudType, SnackbarService, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, currentComponentPageBasedPermissionsSelector$ } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-open-area-view',
  templateUrl: './open-area-view.component.html',
})
export class OpenAreaViewComponent implements OnInit {

openAreaReasonId: any
openAreaSetupDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any;
totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
      tableComponentConfigs: {
        tabsList: []
      }
    };

constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.openAreaReasonId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption:  'View Open Area Setup',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/ro-configuration' }, 
    { displayName: 'Open Area Setup', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 17 } }
    , {displayName:'',}],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  this.rxjsService.setGlobalLoaderProperty(false);
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  };
  this.combineLatestNgrxStoreData()
  if (this.openAreaReasonId) {
    this.getopenAreaSetupDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.openAreaSetupDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --"+ this.openAreaSetupDetails.openAreaReason;
      this.openAreaSetupDetails = [
        { name: 'Open Area Reason', value: response.resources.openAreaReason },
        { name: 'Description', value: response.resources.description },
        { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}

combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.RO_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}
onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(['event-management/ro-configuration/open-area/add-edit'], { queryParams: { id: this.openAreaReasonId , totalTabs:this.totaltab, selectedTab:this.selectedTab } });
}

getopenAreaSetupDetails(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.OPEN_AREA_REASON,
    this.openAreaReasonId
  );
}

}
