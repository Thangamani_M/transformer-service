import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { OpenAreaConfigModel } from '@modules/event-management/models/configurations/open-area.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable} from 'rxjs';
@Component({
  selector: 'app-open-area-add-edit',
  templateUrl: './open-area-add-edit.component.html',
})
export class OpenAreaAddEditComponent implements OnInit {

  selectedTabIndex = 0;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 4 } });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  selectedOptions: any = [];
  selectedOptionsObject: any = [];
  feedbackOffsiteDetails: any;
  selectedDivisionId: any;
  openAreaConfigForm: FormGroup;
  status: any = [];
  loading = false;
  dataList: any;
  totalRecords: any;
  selectedColumns: any[];
  selectedRows: string[] = [];
  selectedRow: any;
  applicationResponse: IApplicationResponse;
  searchColumns: any;
  columnFilterForm: FormGroup;
  observableResponse;
  groupList = [];
  row: any = {};
  today: any = 0;
  pageLimit: any = [10, 25, 50, 75, 100];
  totaltab = 0;
  selectedTab = 0;
  openAreaReasonId: any;
  openAreaDetails: any;
  primengTableConfigProperties: any;
  title:string;
  
  constructor(public dialogService: DialogService,
   private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.openAreaReasonId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.title =this.openAreaReasonId ? 'Update':'Add';
    this.primengTableConfigProperties = {
      tableCaption: this.title + ' Open Area Setup',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Open Area Setup', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 17} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.openAreaReasonId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/open-area/view' , queryParams: {id: this.openAreaReasonId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createOpenAreaConfigForm();
    if(this.openAreaReasonId){
      this.getOpenAreaSetupDetails(this.openAreaReasonId)
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createOpenAreaConfigForm(): void {
    let openAreaConfigModel = new OpenAreaConfigModel();
    this.openAreaConfigForm = this.formBuilder.group({
    });
    Object.keys(openAreaConfigModel).forEach((key) => {
      this.openAreaConfigForm.addControl(key, new FormControl(openAreaConfigModel[key]));
    });
    this.openAreaConfigForm = setRequiredValidator(this.openAreaConfigForm, ["openAreaReason", "description","isActive"]);
   
  }



 
  getOpenAreaSetupDetails(openAreaReasonId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_AREA_REASON, openAreaReasonId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.openAreaDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.openAreaDetails?.openAreaReason ? this.title +' - '+ this.openAreaDetails?.openAreaReason : this.title + ' --', relativeRouterUrl: '' });
          this.openAreaConfigForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {

    if (this.openAreaConfigForm.invalid) {
      return;
    }
    let formValue = this.openAreaConfigForm.value;
    formValue.createdUserId = this.loggedUser.userId
    let formValueUpdate = this.openAreaConfigForm.value;
    formValueUpdate.modifiedUserId = this.loggedUser.userId
    formValueUpdate.openAreaReasonId = this.openAreaReasonId
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = (!this.openAreaReasonId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_AREA_REASON, formValue) :
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_AREA_REASON, formValueUpdate)

  crudService.subscribe((response: IApplicationResponse) => {
    if (response.isSuccess) {
      this.router.navigateByUrl('/event-management/ro-configuration?tab=17');
    }
  })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}