import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckListAddEditComponent } from './check-list-add-edit.component';
import { CheckListViewComponent } from './check-list-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CheckListAddEditComponent, data: { title: 'Check List Add/Edit' } },
  { path: 'view', component: CheckListViewComponent, data: { title: 'Check List View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CheckListRoutingModule { }
