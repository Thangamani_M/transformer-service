import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CheckListItemsListModel, CheckListModel } from '@modules/event-management/models/configurations/check-list.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-check-list-add-edit',
  templateUrl: './check-list-add-edit.component.html',
})
export class CheckListAddEditComponent implements OnInit {
  cmcFeedbackId: any
  divisionDropDown: any = [];
  checkListDropDown: any = [];
  checkListForm: FormGroup;
  checkListItemsList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  incidentDetails: any;
  isDuplicate: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService) {
    this.cmcFeedbackId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title = this.cmcFeedbackId  ? 'Update Check List':'Add Check List';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Check List', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 10 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.cmcFeedbackId ){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/check-list/view' , queryParams: {id: this.cmcFeedbackId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createcheckListForm();
    this.getDivisionDropDownById();
    this.getCheckListDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.cmcFeedbackId) {

      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let cmcSmsGroup = new CheckListModel(response.resources);
        this.incidentDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.incidentDetails?.vehicleCheckListName ? this.title +' - '+ this.incidentDetails?.vehicleCheckListName : this.title + ' --', relativeRouterUrl: '' });
        this.checkListForm.patchValue(cmcSmsGroup);
        var checkListDivisionsList = [];
        response.resources.checkListDivisionsList.forEach(element => {
          checkListDivisionsList.push(element.divisionId);
        });
        this.checkListForm.get('checkListDivisionsList').setValue(checkListDivisionsList)

        this.checkListItemsList = this.getcheckListItemsListArray;
        response.resources.checkListItemsList.forEach((checkListItemListModel: CheckListItemsListModel) => {
          this.checkListItemsList.push(this.createcheckListItemsListModel(checkListItemListModel));
        });
      })
    } else {
      this.checkListItemsList = this.getcheckListItemsListArray;
      this.checkListItemsList.push(this.createcheckListItemsListModel());
    }
  }

  createcheckListForm(): void {
    let checkListModel = new CheckListModel();
    this.checkListForm = this.formBuilder.group({
      checkListItemsList: this.formBuilder.array([]),
      checkAfterReloggingPeriod: [null, [Validators.required, Validators.min(1), Validators.max(99)]]
    });
    Object.keys(checkListModel).forEach((key) => {
      this.checkListForm.addControl(key, new FormControl(checkListModel[key]));
    });
    this.checkListForm = setRequiredValidator(this.checkListForm, ["vehicleCheckListName", "description", "checkListDivisionsList"]);
    this.checkListForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.checkListForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCheckListDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CHECK_LIST_OPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.checkListDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getcheckListItemsListArray(): FormArray {
    if (!this.checkListForm) return;
    return this.checkListForm.get("checkListItemsList") as FormArray;
  }

  //Create FormArray controls
  createcheckListItemsListModel(checkListItemsListModel?: CheckListItemsListModel): FormGroup {
    let checkListItemsListFormControlModel = new CheckListItemsListModel(checkListItemsListModel);
    let formControls = {};
    Object.keys(checkListItemsListFormControlModel).forEach((key) => {
      if (key === 'vehicleCheckListItemName' || key === 'checkListOptionId' || key === 'isAlertSupervisor') {
        formControls[key] = [{ value: checkListItemsListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.cmcFeedbackId) {
        formControls[key] = [{ value: checkListItemsListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CHECK_LIST,
      this.cmcFeedbackId
    );
  }

  OnChange(index): boolean {
    if (this.getcheckListItemsListArray.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.getcheckListItemsListArray.value.slice();
      cloneArray.splice(index, 1);
      var findIndex = cloneArray.some(x => x.vehicleCheckListItemName === this.getcheckListItemsListArray.value[index].vehicleCheckListItemName);
      if (findIndex) {
        this.snackbarService.openSnackbar("Vehicle check item already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }

    }
  }


  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (this.getcheckListItemsListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Vehicle check item already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.checkListItemsList = this.getcheckListItemsListArray;
    let checkListItemsListModel = new CheckListItemsListModel();
    this.checkListItemsList.insert(0, this.createcheckListItemsListModel(checkListItemsListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeCmcSmsEmployee(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getcheckListItemsListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getcheckListItemsListArray.controls[i].value.checlListItemId && this.getcheckListItemsListArray.length > 1) {
            this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CHECK_LIST,
              this.getcheckListItemsListArray.controls[i].value.checlListItemId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.getcheckListItemsListArray.removeAt(i);
                }
                if (this.getcheckListItemsListArray.length === 0) {
                  this.addCmcSmsGroupEmployee();
                };
              });
          }
          else {
            this.getcheckListItemsListArray.removeAt(i);
            this.isDuplicate = false;
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.checkListForm.controls.checkListDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.checkListForm.controls.checkListDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.checkListForm.controls.checkListDivisionsList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (this.checkListForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Vehicle check item already exist", ResponseMessageTypes.WARNING);
      return;
    }
    let formValue = this.checkListForm.value;
    if (formValue.checkListDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.checkListDivisionsList = formValue.checkListDivisionsList.filter(item => item != '')
      formValue.checkListDivisionsList = formValue.checkListDivisionsList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.cmcFeedbackId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CHECK_LIST, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CHECK_LIST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=10');
      }
    })
  }

  onCRUDRequested(type){}

}

