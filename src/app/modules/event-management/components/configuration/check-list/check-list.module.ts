import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { CheckListAddEditComponent } from './check-list-add-edit.component';
import { CheckListRoutingModule } from './check-list-routing.module';
import { CheckListViewComponent } from './check-list-view.component';

@NgModule({
  declarations: [CheckListAddEditComponent, CheckListViewComponent],
  imports: [
    CommonModule,
    CheckListRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class CheckListModule { }
