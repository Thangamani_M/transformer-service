import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlarmGroupAddEditComponent } from './alarm-group-add-edit.component';
import { AlarmGroupViewComponent } from './alarm-group-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: AlarmGroupAddEditComponent, data: { title: 'Alarm Group Add/Edit' } },
  { path: 'view', component: AlarmGroupViewComponent, data: { title: 'Alarm Group View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class AlarmGroupRoutingModule { }
