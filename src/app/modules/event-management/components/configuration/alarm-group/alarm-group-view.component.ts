import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-alarm-group-view',
  templateUrl: './alarm-group-view.component.html',
})
export class AlarmGroupViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  alarmGroupId: any;
  alarmGroupDetails: any = [];
  alarmTypeList: any = [];
  detailsAlaramType: any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private rxjsService: RxjsService, private datePipe: DatePipe, private activatedRoute: ActivatedRoute,
    private crudService: CrudService,private snackbarService:SnackbarService, private momentService: MomentService, private router: Router,  private store: Store<AppState>) {
    super();
    this.alarmGroupId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.primengTableConfigProperties = {
      tableCaption: 'View Alarm Group',
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Alarm Group', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 4 } }, { displayName: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'id',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableViewBtn: true,
            columns: [{ field: 'alarm', header: 'Alarms', width: '80px' },
            { field: 'description', header: 'Description', width: '120px' },
            { field: 'priority', header: 'Priority', width: '80px' },
            { field: 'cancelAlarm', header: 'Cancel Alarm', width: '80px' },
            { field: 'canPhoneIn', header: 'Phone-in?', width: '70px' },
            { field: 'allowTag', header: 'Allow Tagging', width: '80px' },
            { field: 'dateCreated', header: 'Created On', width: '130px' ,isDateTime:true },
            { field: 'alarmActionName', header: 'Status', width: '120px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.ALARM_TYPE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    }
    this.combineLatestNgrxStoreData()
    this.getalarmGroupDetailsById(this.alarmGroupId);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getalarmGroupDetailsById(alarmGroupId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_GROUP, alarmGroupId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.detailsAlaramType = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.detailsAlaramType?.alarmGroupName;
          this.alarmGroupDetails = response.resources;
          this.alarmTypeList = response.resources?.alarmTypeList
          let otherParams = {}
          this.getSignalHistoryList(this.row['pageIndex'], this.row['pageSize'], otherParams);
          this.onShowValue(response.resources);
        }

      });
  }

  onShowValue(response?: any) {
    this.alarmGroupDetails = [
      { name: 'Alarm Group Name', value: response?.alarmGroupName },
      { name: 'Description', value: response?.description },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified By', value: response?.modifiedUserName },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Status', value: response.isActive == true ? 'Active' : 'In-Active', statusClass: response.isActive == true ? "status-label-green" : 'status-label-pink' },
    ]
  }

  getSignalHistoryList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess && data.resources) {
          this.dataList = data.resources;
          this.dataList.map(item => {
            item.checkHidden = true
            item.dateCreated = this.datePipe.transform(item?.dateCreated, 'dd-MM-yyyy, HH:mm:ss a');
          })
          if (this.alarmGroupId) {
            this.dataList.forEach((row: any) => {
              this.alarmTypeList.forEach(element => {
                if (row.id == element.alarmTypeId) {
                  this.selectedRows.push(row)
                  this.selectedRows = [...this.selectedRows, row]
                  row.checkedValue = row
                }
              });
            });
          }
          else {
            this.dataList.forEach(element => {
              this.dataList.push(element);
              if (this.alarmGroupId) {
                this.dataList.forEach((row: any) => {
                  this.alarmTypeList.forEach(element => {
                    if (row.id == element.alarmTypeId) {
                      let existDate = this.selectedRows.find(x => x.id == row.alarmTypeId)
                      if (!existDate) {
                        this.selectedRows = [...this.selectedRows, row]
                        row.checkedValue = row
                      }
                    }
                  });
                });
              }
            });
          }
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.edit();
        break;
    }
  }

  onChangeSelecedRows(event) {
    // this.selectedRows = event
  }

  edit() {
    if (this.alarmGroupId) {
      if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['event-management/event-configuration/alarm-group/add-edit'], { queryParams: { id: this.alarmGroupId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
