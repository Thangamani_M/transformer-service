import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { AlarmGroupAddEditComponent } from './alarm-group-add-edit.component';
import { AlarmGroupRoutingModule } from './alarm-group-routing.module';
import { AlarmGroupViewComponent } from './alarm-group-view.component';

@NgModule({
  declarations: [AlarmGroupViewComponent, AlarmGroupAddEditComponent],
  imports: [
    CommonModule,
    AlarmGroupRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AlarmGroupModule { }
