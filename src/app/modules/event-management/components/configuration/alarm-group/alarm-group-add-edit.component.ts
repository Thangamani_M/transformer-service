import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { AlarmGroupModel } from '@modules/event-management/models/configurations/alarm-group.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-alarm-group-add-edit',
  templateUrl: './alarm-group-add-edit.component.html',
})
export class AlarmGroupAddEditComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  delayDurationForm: FormGroup;
  loggedUser: UserLogin;
  alarmGroupId: any;
  alarmGroupDetails: any = [];
  alarmTypeList: any = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  totaltab = 0;
  selectedTab = 0;
  options = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];
  currentPageIndex = 0
  title:string;

  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private momentService: MomentService, private formBuilder: FormBuilder, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private router: Router) {
    super();
    this.alarmGroupId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.alarmGroupId ? 'Update Alarm Group':'Add Alarm Group';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Alarm Group', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 4 } }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'id',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'alarm', header: 'Alarms', width: '100px' },
            { field: 'description', header: 'Description', width: '150px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'cancelAlarm', header: 'Cancel Alarm', width: '150px' },
            { field: 'canPhoneIn', header: 'Phone-in?', width: '150px', type: 'dropdown', options:this.options},
            { field: 'allowTag', header: 'Allow Tagging', width: '150px',type: 'dropdown', options:this.options },
            { field: 'dateCreated', header: 'Created On', width: '150px', isDate:true},
            { field: 'alarmActionName', header: 'Status', width: '150px'}],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.ALARM_TYPE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
    if(this.alarmGroupId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/alarm-group/view' , queryParams: {id: this.alarmGroupId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createDelayDurationForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.alarmGroupId) {
      this.getalarmGroupDetailsById(this.alarmGroupId);
    } else {
      this.getSignalHistoryList();
    }
  }

  getalarmGroupDetailsById(alarmGroupId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_GROUP, alarmGroupId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmGroupDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.alarmGroupDetails?.alarmGroupName ? this.title +' - '+ this.alarmGroupDetails?.alarmGroupName : this.title + ' --', relativeRouterUrl: '' });
          this.delayDurationForm.patchValue(response.resources)
          let otherParams = {}
          this.getSignalHistoryList(this.row['pageIndex'], this.row['pageSize'], otherParams);
        }

      });
  }

  getSignalHistoryList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.selectedRows = []
          let uniqueAlarmTypeList= [...new Map(this.alarmGroupDetails.alarmTypeList.map(item => [item['alarmTypeId'], item])).values()]
          uniqueAlarmTypeList.push(...this.selectedRowsAll)
          if (this.alarmGroupId) {
            this.dataList.forEach((row: any) => {
              uniqueAlarmTypeList.forEach((element:any) => {
                if (row.id == element.alarmTypeId || row.id == element.id) {
                  this.selectedRows.push(row)
                  this.selectedRowsAll.push(row)
                }
              });
            });
          }
          else {
            this.dataList.forEach(element => {
              if (this.alarmGroupId) {
                this.dataList.forEach((row: any) => {
                  uniqueAlarmTypeList.forEach((element:any) => {
                    if (row.id == element.alarmTypeId || row.id == element.id) {
                      let existDate = this.selectedRows.find(x => x.id == row.alarmTypeId)
                      if (!existDate) {
                        this.selectedRows.push(row)
                        this.selectedRowsAll.push(row)

                      }
                    }
                  });
                });
              }
            });
          }
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;

        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
    }

  createDelayDurationForm(): void {
    let alarmGroupModel = new AlarmGroupModel();
    // create form controls dynamically from model class
    this.delayDurationForm = this.formBuilder.group({});
    Object.keys(alarmGroupModel).forEach((key) => {
      this.delayDurationForm.addControl(key, new FormControl(alarmGroupModel[key]));
    });
    this.delayDurationForm = setRequiredValidator(this.delayDurationForm, ["alarmGroupName", "description"]);
    this.delayDurationForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.delayDurationForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }
  selectedRowsAll = []

  filterUnSelectedArray(mainArray, selectedArray) {
    return mainArray.filter(mainObj => {
      return selectedArray.every(selectedObj => {
        return mainObj.id != selectedObj.id;
      });
    });
  }

  onChangeSelecedRows(event) {
    this.selectedRowsAll.push(...event)
    this.selectedRowsAll = [...new Map(this.selectedRowsAll.map(item => [item['id'], item])).values()]
    let unSelected = this.filterUnSelectedArray(this.dataList, event)
    this.selectedRowsAll= this.filterUnSelectedArray(this.selectedRowsAll, unSelected)
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onSubmit(): void {
    if (this.delayDurationForm.invalid) {
      return;
    }
    this.alarmTypeList = [];
    this.selectedRowsAll.forEach(item => {
      this.alarmTypeList.push({
        alarmTypeId: item.id,
        alarmGroupId: this.delayDurationForm.value.alarmGroupId ? this.delayDurationForm.value.alarmGroupId : 0,
        createdUserId: this.loggedUser.userId,
        modifiedUserId: this.loggedUser.userId
      });
    });

    this.delayDurationForm.get('alarmTypeList').setValue(this.alarmTypeList)
    let formValue = this.delayDurationForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.alarmGroupId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_GROUP, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_GROUP, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=4');
        this.getalarmGroupDetailsById(this.alarmGroupId)
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
