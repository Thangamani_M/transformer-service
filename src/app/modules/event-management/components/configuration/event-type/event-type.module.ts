import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { EventTypeAddEditComponent } from './event-type-add-edit.component';
import { EventTypeRoutingModule } from './event-type-routing.module';
import { EventTypeViewComponent } from './event-type-view.component';

@NgModule({
  declarations: [EventTypeAddEditComponent, EventTypeViewComponent],
  imports: [
    CommonModule,
    EventTypeRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class EventTypeModule { }
