import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventTypeModel } from '@modules/event-management/models/configurations/event-type-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-event-type-add-edit',
  templateUrl: './event-type-add-edit.component.html',
})
export class EventTypeAddEditComponent implements OnInit {

  eventTypeId: any;
  eventTypeDetails: any;
  eventTypeForm: FormGroup;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService) {
    this.eventTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.eventTypeId ? 'Update Event Type':'Add Event Type';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Event Type', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 9 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.eventTypeId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/event-type/view' , queryParams: {id: this.eventTypeId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createEventTypeForm();
    if (this.eventTypeId) {
      this.getEventTypeDetailsById(this.eventTypeId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createEventTypeForm(): void {
    let eventTypeModel = new EventTypeModel();
    this.eventTypeForm = this.formBuilder.group({});
    Object.keys(eventTypeModel).forEach((key) => {
      this.eventTypeForm.addControl(key, new FormControl(eventTypeModel[key]));
    });
    this.eventTypeForm = setRequiredValidator(this.eventTypeForm, ["incidentName", "description"]);
    this.eventTypeForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.eventTypeForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.eventTypeId) {
      this.eventTypeForm.removeControl('eventTypeId');
      this.eventTypeForm.removeControl('modifiedUserId');
    } else {
      this.eventTypeForm.removeControl('createdUserId');
    }
  }

  getEventTypeDetailsById(eventTypeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_TYPE, eventTypeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.eventTypeDetails?.incidentName ? this.title +' - '+ this.eventTypeDetails?.incidentName : this.title + ' --', relativeRouterUrl: '' });
          this.eventTypeForm.patchValue(this.eventTypeDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.eventTypeForm.invalid) {
      return;
    }
    let formValue = this.eventTypeForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.eventTypeId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_TYPE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_TYPE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=9');
      }
    })
  }

  onCRUDRequested(type){}

}
