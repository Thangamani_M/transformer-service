import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventTypeAddEditComponent } from './event-type-add-edit.component';
import { EventTypeViewComponent } from './event-type-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: EventTypeAddEditComponent, data: { title: 'Event Type Add/Edit' } },
  { path: 'view', component: EventTypeViewComponent, data: { title: 'Event Type View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class EventTypeRoutingModule { }
