import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PttRegistrationAddEditComponent } from './ptt-registration-add-edit.component';
import { PttRegistrationRoutingModule } from './ptt-registration-routing.module';
import { PttRegistrationViewComponent } from './ptt-registration-view.component';

@NgModule({
  declarations: [PttRegistrationAddEditComponent, PttRegistrationViewComponent],
  imports: [
    CommonModule,
    MaterialModule,
    PttRegistrationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class PttRegistrationModule { }