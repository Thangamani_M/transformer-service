import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { PttRegistrarionModel } from '@modules/event-management/models/configurations/ptt-registration.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-ptt-registration-add-edit',
  templateUrl: './ptt-registration-add-edit.component.html',
})
export class PttRegistrationAddEditComponent implements OnInit {

  pttRegistrationId: any;
  pttRegistrationDetails: any;
  loggedUser: any;
  pttRegistrarionForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,private momentService: MomentService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.pttRegistrationId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.pttRegistrationId ? 'Update PTT Registration':'Add PTT Registration';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'PTT Registration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 6 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.pttRegistrationId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/ptt-registration/view' , queryParams: {id: this.pttRegistrationId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createPttRegistrationForm();
    if (this.pttRegistrationId) {
      this.getPttRegistrationDetailsById(this.pttRegistrationId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createPttRegistrationForm(): void {
    let pttRegistrarionModel = new PttRegistrarionModel();
    // create form controls dynamically from model class
    this.pttRegistrarionForm = this.formBuilder.group({});
    Object.keys(pttRegistrarionModel).forEach((key) => {
      this.pttRegistrarionForm.addControl(key, new FormControl(pttRegistrarionModel[key]));
    });
    this.pttRegistrarionForm = setRequiredValidator(this.pttRegistrarionForm, ["pttid", "imeiNumber", "deviceName"]);
    this.pttRegistrarionForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.pttRegistrarionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getPttRegistrationDetailsById(pttRegistrationId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PTT_REGISTRATION, pttRegistrationId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.pttRegistrationDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.pttRegistrationDetails?.pttid ? this.title +' - '+ this.pttRegistrationDetails?.pttid : this.title + ' --', relativeRouterUrl: '' });
          this.pttRegistrarionForm.get('pttRegistrationId').setValue(response.resources.pttRegistrationId);
          this.pttRegistrarionForm.get('pttid').setValue(response.resources.pttid);
          this.pttRegistrarionForm.get('imeiNumber').setValue(response.resources.imeiNumber);
          this.pttRegistrarionForm.get('deviceName').setValue(response.resources.deviceName);
          this.pttRegistrarionForm.get('isActive').setValue(response.resources.isActive);
          this.pttRegistrarionForm.get('contractDate').setValue(new Date(response.resources.contractDate))
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.pttRegistrarionForm.invalid) {
      return;
    }
    let formValue = this.pttRegistrarionForm.value;
    formValue.contractDate = this.momentService.localToUTC(this.pttRegistrarionForm.get('contractDate').value);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.pttRegistrationId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PTT_REGISTRATION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PTT_REGISTRATION, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=6');
      }
    })
  }

  onCRUDRequested(type){}

}
