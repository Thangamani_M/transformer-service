import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PttRegistrationAddEditComponent } from './ptt-registration-add-edit.component';
import { PttRegistrationViewComponent } from './ptt-registration-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: PttRegistrationAddEditComponent, data: { title: 'PTT Registration Add/Edit' } },
  { path: 'view', component: PttRegistrationViewComponent, data: { title: 'PTT Registration View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PttRegistrationRoutingModule { }