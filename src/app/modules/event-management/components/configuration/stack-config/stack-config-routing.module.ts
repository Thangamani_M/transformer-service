import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StackConfigAddEditComponent } from './stack-config-add-edit.component';
import { StackConfigViewComponent } from './stack-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: StackConfigAddEditComponent, data: { title: 'Stack Config Add/Edit' } },
  { path: 'view', component: StackConfigViewComponent, data: { title: 'Stack Config View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StackConfigRoutingModule { }
