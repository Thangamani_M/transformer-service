import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { StackConfigAddEditComponent } from './stack-config-add-edit.component';
import { StackConfigRoutingModule } from './stack-config-routing.module';
import { StackConfigViewComponent } from './stack-config-view.component';



@NgModule({
  declarations: [StackConfigAddEditComponent, StackConfigViewComponent],
  imports: [
    CommonModule,
    StackConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class StackConfigModule { }
