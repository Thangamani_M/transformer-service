import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService,CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { StackConfigModel } from '@modules/event-management/models/configurations/stack-config.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-stack-config-add-edit',
  templateUrl: './stack-config-add-edit.component.html',
})
export class StackConfigAddEditComponent implements OnInit {

  stackConfigId: string;
  stackConfigDetails: any;
  divisionDropDown: any = [];
  nameStackDropDown: any = [];
  customerTypeDropDown: any = [];
  alarmGroupDropDown: any = [];
  stackConfigForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  @ViewChild('allSelectedCustomer', { static: false }) private allSelectedCustomer: MatOption;
  @ViewChild('allSelectedAlarmGroup', { static: false }) private allSelectedAlarmGroup: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.stackConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.stackConfigId ? 'Update Stack':'Add Stack';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Stack Configuration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 0} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.stackConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/stack-config/view' , queryParams: {id: this.stackConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createStackConfigForm();
    this.getDivisionDropDown();
    this.getNamedStackDropDown()
    this.getAlarmGroupDropDown()
    this.getCustomerTypeDropDown()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.stackConfigId) {
      this.getStackConfigDetailsById().subscribe((response: IApplicationResponse) => {
        let stackConfig = new StackConfigModel(response.resources);
        this.stackConfigDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.stackConfigDetails?.stackName ? this.title +' - '+ this.stackConfigDetails?.stackName : this.title + ' --', relativeRouterUrl: '' });
        this.stackConfigForm.patchValue(stackConfig);
        var stackConfigDivisionList = [];
        response.resources.stackConfigDivisionList.forEach(element => {
          stackConfigDivisionList.push(element.divisionId);
        });
        var stackConfigAlarmGroupList = [];
        response.resources.stackConfigAlarmGroupList.forEach(element => {
          stackConfigAlarmGroupList.push(parseInt(element.alarmGroupId));
        });
        var stackConfigCustomerProfileTypeList = []
        response.resources.stackConfigCustomerProfileTypeList.forEach(element => {
          stackConfigCustomerProfileTypeList.push(parseInt(element.customerProfileTypeId))
        });
        this.stackConfigForm.get('stackConfigAlarmGroups').setValue(stackConfigAlarmGroupList)
        this.stackConfigForm.get('stackConfigDivisions').setValue(stackConfigDivisionList)
        this.stackConfigForm.get('stackConfigCustomerProfileTypes').setValue(stackConfigCustomerProfileTypeList)

      })
    }

  }

   createStackConfigForm(): void {
    let stackConfigModel = new StackConfigModel();
    // create form controls dynamically from model class
    this.stackConfigForm = this.formBuilder.group({});
    Object.keys(stackConfigModel).forEach((key) => {
      this.stackConfigForm.addControl(key, new FormControl(stackConfigModel[key]));
    });
    this.stackConfigForm = setRequiredValidator(this.stackConfigForm, ["stackName", "stackConfigDivisions", "stackConfigAlarmGroups", "stackConfigCustomerProfileTypes"]);
    this.stackConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.stackConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.stackConfigId) {
      this.stackConfigForm.removeControl('modifiedUserId')
    } else {
      this.stackConfigForm.removeControl('createdUserId')
    }

  }

  getStackConfigDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.STACK_CONFIG,
      this.stackConfigId
    );
  }

  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getNamedStackDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_NAMED_STACK_CONFIG_MAPPED, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.nameStackDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAlarmGroupDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_GROUP, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmGroupDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCustomerTypeDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_UX_CUSTOMER_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.customerTypeDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleDivisionOne() {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.stackConfigForm.controls.stackConfigDivisions.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.stackConfigForm.controls.stackConfigDivisions
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.stackConfigForm.controls.stackConfigDivisions.patchValue([]);
    }
  }

  toggleCustomeOne() {
    if (this.allSelectedCustomer.selected) {
      this.allSelectedCustomer.deselect();
      return false;
    }
    if (this.stackConfigForm.controls.stackConfigCustomerProfileTypes.value.length == this.customerTypeDropDown.length)
      this.allSelectedCustomer.select();
  }

  toggleAllCustomers() {
    if (this.allSelectedCustomer.selected) {
      this.stackConfigForm.controls.stackConfigCustomerProfileTypes
        .patchValue([...this.customerTypeDropDown.map(item => item.id), '']);
    } else {
      this.stackConfigForm.controls.stackConfigCustomerProfileTypes.patchValue([]);
    }
  }

  toggleAlarmOne() {
    if (this.allSelectedAlarmGroup.selected) {
      this.allSelectedAlarmGroup.deselect();
      return false;
    }
    if (this.stackConfigForm.controls.stackConfigAlarmGroups.value.length == this.alarmGroupDropDown.length)
      this.allSelectedAlarmGroup.select();
  }

  toggleAllAlarmGroups() {
    if (this.allSelectedAlarmGroup.selected) {
      this.stackConfigForm.controls.stackConfigAlarmGroups
        .patchValue([...this.alarmGroupDropDown.map(item => item.id), '']);
    } else {
      this.stackConfigForm.controls.stackConfigAlarmGroups.patchValue([]);
    }
  }

  onSubmit(): void {
    if (this.stackConfigForm.invalid) {
      return;
    }
    let formValue = this.stackConfigForm.value;

    if (formValue.stackConfigDivisions[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.stackConfigDivisions = formValue.stackConfigDivisions.filter(item => item != '')
      formValue.stackConfigDivisions = formValue.stackConfigDivisions.map(divisionId => ({ divisionId }))
    }

    if (formValue.stackConfigAlarmGroups[0].hasOwnProperty('alarmGroupId')) {
      formValue = formValue;
    } else {
      formValue.stackConfigAlarmGroups = formValue.stackConfigAlarmGroups.filter(item => item != '')
      formValue.stackConfigAlarmGroups = formValue.stackConfigAlarmGroups.map(alarmGroupId => ({ alarmGroupId }))
    }

    if (formValue.stackConfigCustomerProfileTypes[0].hasOwnProperty('customerProfileTypeId')) {
      formValue = formValue;
    } else {
      formValue.stackConfigCustomerProfileTypes = formValue.stackConfigCustomerProfileTypes.filter(item => item != '')
      formValue.stackConfigCustomerProfileTypes = formValue.stackConfigCustomerProfileTypes.map(customerProfileTypeId => ({ customerProfileTypeId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.stackConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=0');
      }
    })
  }

  onCRUDRequested(type){}

}