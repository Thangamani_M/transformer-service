import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { ReportingConfigModel } from '@modules/event-management/models/configurations/reporting-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reporting-config-add-edit',
  templateUrl: './reporting-config-add-edit.component.html',
})
export class ReportingConfigAddEditComponent implements OnInit {

  reportingConfigId: any
  divisionDropDown: any = [];
  reportingConfigForm: FormGroup;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  reportingConfigDetails: any;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.reportingConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.reportingConfigId ? 'Update Reporting Config':'Add Reporting Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Reporting Config', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 14 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.reportingConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/reporting-config/view' , queryParams: {id: this.reportingConfigId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createReportingConfigForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.reportingConfigId) {
      this.getReportingConfigDetailsById(this.reportingConfigId);
      this.getDivisionDropDownById();
      return
    }
  }

  createReportingConfigForm(): void {
    let reportingConfigModel = new ReportingConfigModel();
    // create form controls dynamically from model class
    this.reportingConfigForm = this.formBuilder.group({
      measure1: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
      measure2: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
      measure3: [null, [Validators.required, Validators.min(1), Validators.max(9999)]]
    });
    Object.keys(reportingConfigModel).forEach((key) => {
      this.reportingConfigForm.addControl(key, new FormControl(reportingConfigModel[key]));
    });
    this.reportingConfigForm = setRequiredValidator(this.reportingConfigForm, ["reportingConfigName", "description", "reportingConfigDivisionList"]);
    this.reportingConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.reportingConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.reportingConfigId) {
      this.reportingConfigForm.removeControl('reportingConfigId');
      this.reportingConfigForm.removeControl('modifiedUserId');
    } else {
      this.reportingConfigForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getReportingConfigDetailsById(reportingConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.REPORTING_CONFIG, reportingConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reportingConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.reportingConfigDetails?.reportingConfigName ? this.title +' - '+ this.reportingConfigDetails?.reportingConfigName : this.title + ' --', relativeRouterUrl: '' });
          this.reportingConfigForm.patchValue(response.resources);
          var reportingConfigDivisionList = [];
          response.resources.reportingConfigDivisionList.forEach(element => {
            reportingConfigDivisionList.push(element.divisionId);
          });
          this.reportingConfigForm.get('reportingConfigDivisionList').setValue(reportingConfigDivisionList)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.reportingConfigForm.controls.reportingConfigDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions(e?) {
    if (this.allSelectedDivision.selected) {
      this.reportingConfigForm.controls.reportingConfigDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.reportingConfigForm.controls.reportingConfigDivisionList.patchValue([]);
    }
  }

  onSubmit(): void {
    if (this.reportingConfigForm.invalid) {
      return;
    }

    let formValue = this.reportingConfigForm.value;
    if (formValue.reportingConfigDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.reportingConfigDivisionList = formValue.reportingConfigDivisionList.filter(item => item != '')
      formValue.reportingConfigDivisionList = formValue.reportingConfigDivisionList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.reportingConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.REPORTING_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.REPORTING_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=14');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}