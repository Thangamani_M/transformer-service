import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-reporting-config-view',
  templateUrl: './reporting-config-view.component.html',
 })
 export class ReportingConfigViewComponent implements OnInit {

reportingConfigId: any
reportingConfigDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any;
totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  };

constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.reportingConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption:  'View Reporting Config',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/ro-configuration' }, 
    { displayName: 'View Reporting Config', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 14 } }
    , {displayName:'',}],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  this.rxjsService.setGlobalLoaderProperty(false);
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  }
  this.combineLatestNgrxStoreData()
  if (this.reportingConfigId) {
    this.getreportingConfigDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.reportingConfigDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --"+ this.reportingConfigDetails.reportingConfigName;
      this.reportingConfigDetails = [
        { name: 'Reporting Config Name', value: response.resources.reportingConfigName },
        { name: 'Division', value: response.resources.reportingConfigDivisions },
        { name: 'Description', value: response.resources.description },
        { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
        { name: 'Measure 1', value: response.resources.measure1 > 9 ?""+this.reportingConfigDetails.measure1+" "+'Seconds':""+this.reportingConfigDetails.measure1+" "+'Second'},
        { name: 'Measure 2', value: response.resources.measure2 > 9 ?""+this.reportingConfigDetails.measure2+" "+'Seconds':""+this.reportingConfigDetails.measure2+" "+'Second'},
        { name: 'Measure 3', value: response.resources.measure3 > 9 ?""+this.reportingConfigDetails.measure3+" "+'Seconds':""+this.reportingConfigDetails.measure3+" "+'Second'},
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}

combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.RO_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}
onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(['event-management/ro-configuration/reporting-config/add-edit'], { queryParams: { id: this.reportingConfigId ,  totalTabs:this.totaltab, selectedTab:this.selectedTab} });
}

getreportingConfigDetails(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.REPORTING_CONFIG,
    this.reportingConfigId
  );
}

}
