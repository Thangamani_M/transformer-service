import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportingConfigAddEditComponent } from './reporting-config-add-edit.component';
import { ReportingConfigViewComponent } from './reporting-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ReportingConfigAddEditComponent, data: { title: 'Reporting Config Add/Edit' } },
  { path: 'view', component: ReportingConfigViewComponent, data: { title: 'Reporting Config View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ReportingConfigRoutingModule { }
