import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { ReportingConfigAddEditComponent } from './reporting-config-add-edit.component';
import { ReportingConfigRoutingModule } from './reporting-config-routing.module';
import { ReportingConfigViewComponent } from './reporting-config-view.component';



@NgModule({
  declarations: [ReportingConfigAddEditComponent, ReportingConfigViewComponent],
  imports: [
    CommonModule,
    ReportingConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class ReportingConfigModule { }
