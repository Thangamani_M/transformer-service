import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoAcceptTimeAddEditComponent } from './ro-accept-time-add-edit.component';
import { RoAcceptTimeViewComponent } from './ro-accept-time-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: RoAcceptTimeAddEditComponent, data: { title: 'Ro Accept Time Add/Edit' } },
  { path: 'view', component: RoAcceptTimeViewComponent, data: { title: 'Ro Accept Time View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class RoAcceptTimeRoutingModule { }