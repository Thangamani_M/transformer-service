import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { RoAcceptTimeAddEditComponent } from './ro-accept-time-add-edit.component';
import { RoAcceptTimeRoutingModule } from './ro-accept-time-routing.module';
import { RoAcceptTimeViewComponent } from './ro-accept-time-view.component';


@NgModule({
  declarations: [RoAcceptTimeAddEditComponent, RoAcceptTimeViewComponent],
  imports: [
    CommonModule,
    RoAcceptTimeRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
  ]
})
export class RoAcceptTimeModule { }