import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { RoAcceptTimeModel } from '@modules/event-management/models/configurations/ro-accept-time-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-ro-accept-time-add-edit',
  templateUrl: './ro-accept-time-add-edit.component.html',
})
export class RoAcceptTimeAddEditComponent implements OnInit {

  roAcceptTimeId: any
  divisionDropDown: any = [];
  mainAreaList: any = [];
  roAcceptTimeForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  roAcceptTimeDetails: any;
  selectedDivisionId: any;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.roAcceptTimeId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.roAcceptTimeId ? 'Update RO Accept Time':'Add RO Accept Time';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'RO Accept Time', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 0} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.roAcceptTimeId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/ro-accept-time/view' , queryParams: {id: this.roAcceptTimeId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createRoAcceptTimeFormForm();
    this.roAcceptTimeForm.get("divisionId").valueChanges.subscribe(data => {
      this.getMainAreaList(data);
    })

    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.roAcceptTimeId) {
      this.getRoAcceptTimeDetailsById(this.roAcceptTimeId);
      return
    }
  }

  createRoAcceptTimeFormForm(): void {
    let roAcceptTimeModel = new RoAcceptTimeModel();
    // create form controls dynamically from model class
    this.roAcceptTimeForm = this.formBuilder.group({
      time: [null, [Validators.required, Validators.min(1), Validators.max(9999)]]
    });
    Object.keys(roAcceptTimeModel).forEach((key) => {
      this.roAcceptTimeForm.addControl(key, new FormControl(roAcceptTimeModel[key]));
    });
    this.roAcceptTimeForm = setRequiredValidator(this.roAcceptTimeForm, ["divisionId", "mainAreaId"]);
    this.roAcceptTimeForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.roAcceptTimeForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.roAcceptTimeId) {
      this.roAcceptTimeForm.removeControl('ROAcceptTimeId');
      this.roAcceptTimeForm.removeControl('modifiedUserId');
    } else {
      this.roAcceptTimeForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRoAcceptTimeDetailsById(roAcceptTimeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RO_ACCEPT_TIME, roAcceptTimeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.roAcceptTimeDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.roAcceptTimeDetails?.divisionName ? this.title +' - '+ this.roAcceptTimeDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
          this.roAcceptTimeForm.patchValue(response.resources);

          this.roAcceptTimeForm.get('ROAcceptTimeId').setValue(this.roAcceptTimeDetails.roAcceptTimeId)

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onChangeDivision() {
    this.roAcceptTimeForm.controls.mainAreaId.setValue('');
  }

  onSubmit(): void {
    if (this.roAcceptTimeForm.invalid) {
      return;
    }

    let formValue = this.roAcceptTimeForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.roAcceptTimeId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RO_ACCEPT_TIME, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RO_ACCEPT_TIME, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=0');
      }
    })
  }

  onCRUDRequested(type){}

}