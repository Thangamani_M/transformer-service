import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-ro-accept-time-view',
  templateUrl: './ro-accept-time-view.component.html',
})
export class RoAcceptTimeViewComponent implements OnInit {
  
roAcceptTimeId: any;
roAcceptTimeDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any;
totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  };

constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.roAcceptTimeId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption: 'View Ro Accept Time',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
    { displayName: 'Ro Accept Time', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 0 } }
      , { displayName: '', }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  this.rxjsService.setGlobalLoaderProperty(false);
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  }
  this.combineLatestNgrxStoreData()
  if (this.roAcceptTimeId) {
    this.getroAcceptTimeDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.roAcceptTimeDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + this.roAcceptTimeDetails.divisionName;
      this.roAcceptTimeDetails = [
        { name: 'Division', value: response.resources?.divisionName },
        { name: 'Main Area', value: response.resources?.mainArea },
        { name: 'Time', value: response.resources?.time+" Seconds"},
        { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
        { name: 'Created On', value: response.resources?.createdDate, isDateTime: true },
        { name: 'Modified On', value: response.resources?.modifiedDate, isDateTime: true },
        { name: 'Created By', value: response.resources?.createdUserName },
        { name: 'Modified By', value: response.resources?.modifiedUserName },
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}

combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.RO_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(['event-management/ro-configuration/ro-accept-time/add-edit'], { queryParams: { id: this.roAcceptTimeId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
}

getroAcceptTimeDetails(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.RO_ACCEPT_TIME,
    this.roAcceptTimeId
  );
}

}