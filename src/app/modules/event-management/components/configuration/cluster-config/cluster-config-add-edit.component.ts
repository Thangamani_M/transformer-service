import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { ClusterConfigModel, ClusterConfigSubAreaListModel } from '@modules/event-management/models/configurations/cluster-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cluster-config-add-edit',
  templateUrl: './cluster-config-add-edit.component.html',
})
export class ClusterConfigAddEditComponent implements OnInit {

  clusterConfigId: any
  divisionDropDown: any = [];
  mainAreaDropDown: any = [];
  dispatchModeDropDown: any = [];
  subAreaDropDown = [];
  vehicleCategoryDropDown = [];
  selectedDivisionId: any;
  selectedMainAreaId: any;
  clusterConfigForm: FormGroup;
  clusterConfigSubAreaList: FormArray;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  clusterConfigDetails: any;
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService : ReusablePrimeNGTableFeatureService,
    private crudService: CrudService) {
    this.clusterConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.clusterConfigId ? 'Update Cluster Configuration':'Add Cluster Configuration';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Cluster Configuration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 15 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.clusterConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/cluster-config/view' , queryParams: {id: this.clusterConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createclusterConfigForm();
    this.getDivisionDropDownById();
    this.getVehicleCategoryList();
    this.getDispatchModeList();

    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.clusterConfigId) {
      this.getClusterConfigDetailsById().subscribe((response: IApplicationResponse) => {
        let clusterConfigModel = new ClusterConfigModel(response.resources);
        this.clusterConfigDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.clusterConfigDetails?.clusterName ? this.title +' - '+ this.clusterConfigDetails?.clusterName : this.title + ' --', relativeRouterUrl: '' });
        this.clusterConfigForm.patchValue(clusterConfigModel);
        this.clusterConfigSubAreaList = this.getClusterConfigSubAreaListArray;
        response.resources.clusterConfigSubAreaList.forEach((clusterConfigSubAreaListModel: ClusterConfigSubAreaListModel) => {
          this.clusterConfigSubAreaList.push(this.createClusterConfigSubAreaListModel(clusterConfigSubAreaListModel));
        });
      })
    } else {
      this.clusterConfigSubAreaList = this.getClusterConfigSubAreaListArray;
      this.clusterConfigSubAreaList.push(this.createClusterConfigSubAreaListModel());
    }

    this.clusterConfigForm.get("divisionId").valueChanges.subscribe(data => {
      if (data) {
        this.getMainAreaList(data);
      }
    })

    this.clusterConfigForm.get("mainAreaId").valueChanges.subscribe(data => {
      this.selectedMainAreaId = data;
      if (this.selectedMainAreaId) {
        this.getSubAreaList();
      }
    })
  }

  createclusterConfigForm(): void {
    let clusterConfigModel = new ClusterConfigModel();
    this.clusterConfigForm = this.formBuilder.group({
      clusterConfigSubAreaList: this.formBuilder.array([])
    });
    Object.keys(clusterConfigModel).forEach((key) => {
      this.clusterConfigForm.addControl(key, new FormControl(clusterConfigModel[key]));
    });
    this.clusterConfigForm = setRequiredValidator(this.clusterConfigForm, ["divisionId", "mainAreaId", "clusterName"]);
    this.clusterConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.clusterConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSubAreaList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUB_AREA, undefined, false, prepareGetRequestHttpParams(null, null, {
      MainAreaId: this.selectedMainAreaId,
      ClusterConfigId: this.clusterConfigId ? this.clusterConfigId : 0
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getVehicleCategoryList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_CATEGORY, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleCategoryDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDispatchModeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCH_MODE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchModeDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onChangeDivision() {
    this.isDuplicate = false;
    this.clusterConfigForm.get('mainAreaId').setValue('');
    const arr = <FormArray>this.clusterConfigForm.controls.clusterConfigSubAreaList;
    arr.controls = [];
    this.clusterConfigSubAreaList = this.getClusterConfigSubAreaListArray;
    this.clusterConfigSubAreaList.push(this.createClusterConfigSubAreaListModel());
  }

  onChangeMainArea() {
    this.isDuplicate = false;
    const arr = <FormArray>this.clusterConfigForm.controls.clusterConfigSubAreaList;
    arr.controls = [];
    this.clusterConfigSubAreaList = this.getClusterConfigSubAreaListArray;
    this.clusterConfigSubAreaList.push(this.createClusterConfigSubAreaListModel());
  }

  //Create FormArray
  get getClusterConfigSubAreaListArray(): FormArray {
    if (!this.clusterConfigForm) return;
    return this.clusterConfigForm.get("clusterConfigSubAreaList") as FormArray;
  }

  //Create FormArray controls
  createClusterConfigSubAreaListModel(clusterConfigSubAreaListModel?: ClusterConfigSubAreaListModel): FormGroup {
    let clusterConfigSubAreaListFormControlModel = new ClusterConfigSubAreaListModel(clusterConfigSubAreaListModel);
    let formControls = {};
    Object.keys(clusterConfigSubAreaListFormControlModel).forEach((key) => {
      if (key === 'subAreaId' || key === 'vehicleCategoryId' || key === 'dispatchModeId') {
        formControls[key] = [{ value: clusterConfigSubAreaListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.clusterConfigId) {
        formControls[key] = [{ value: clusterConfigSubAreaListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getClusterConfigDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CLUSTER_CONFIGURATION,
      this.clusterConfigId
    );
  }

  OnChange(index): boolean {
    if (this.getClusterConfigSubAreaListArray.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.getClusterConfigSubAreaListArray.value.slice();
      cloneArray.splice(index, 1);
      var findIndex = cloneArray.some(x => x.subAreaId === this.getClusterConfigSubAreaListArray.value[index].subAreaId);
      if (findIndex) {
        this.snackbarService.openSnackbar("Sub Area already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }

    }
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Add Sub Area Details
  addClusterCongifSubArea(): void {
    if (this.getClusterConfigSubAreaListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Sub Area already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.clusterConfigSubAreaList = this.getClusterConfigSubAreaListArray;
    let clusterConfigSubAreaListtModel = new ClusterConfigSubAreaListModel();
    this.clusterConfigSubAreaList.insert(0, this.createClusterConfigSubAreaListModel(clusterConfigSubAreaListtModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeConfigSubArea(i: number): void {
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Sub Area already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getClusterConfigSubAreaListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getClusterConfigSubAreaListArray.controls[i].value.clusterConfigSubAreaId && this.getClusterConfigSubAreaListArray.length > 1) {
            this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLUSTER_CONFIG_SUB_AREA,
              this.getClusterConfigSubAreaListArray.controls[i].value.clusterConfigSubAreaId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.getClusterConfigSubAreaListArray.removeAt(i);
                  this.isDuplicate = false;

                }
                if (this.getClusterConfigSubAreaListArray.length === 0) {
                  this.addClusterCongifSubArea();
                };
              });
          }
          else {
            this.getClusterConfigSubAreaListArray.removeAt(i);
            this.isDuplicate = false;
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    if (this.clusterConfigForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Sub Area already exist", ResponseMessageTypes.WARNING);
      return;
    }
    let formValue = this.clusterConfigForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.clusterConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLUSTER_CONFIGURATION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLUSTER_CONFIGURATION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=15');
      }
    })
  }

  onCRUDRequested(type){}

}