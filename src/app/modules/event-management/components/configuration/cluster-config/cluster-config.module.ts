import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { ClusterConfigAddEditComponent } from './cluster-config-add-edit.component';
import { ClusterConfigRoutingModule } from './cluster-config-routing.module';
import { ClusterConfigViewComponent } from './cluster-config-view.component';

@NgModule({
  declarations: [ClusterConfigAddEditComponent, ClusterConfigViewComponent],
  imports: [
    CommonModule,
    ClusterConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class ClusterConfigModule { }
