import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClusterConfigAddEditComponent } from './cluster-config-add-edit.component';
import { ClusterConfigViewComponent } from './cluster-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ClusterConfigAddEditComponent, data: { title: 'Cluster Config Add/Edit' } },
  { path: 'view', component: ClusterConfigViewComponent, data: { title: 'Cluster Config View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ClusterConfigRoutingModule { }
