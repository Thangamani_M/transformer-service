import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivePatrolMessageComponent } from './active-patrol-message.component';
import { ActivePatrolMessageViewComponent } from './active-patrol-message.view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';
const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ActivePatrolMessageComponent, data: { title: 'Active Patrol Message Add/Edit' } },
  { path: 'view', component: ActivePatrolMessageViewComponent, data: { title: 'Active Patrol Message View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
  
})
export class ActivePatrolMessageRoutingModule { }
