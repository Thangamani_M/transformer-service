import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
import { ActivePatrolMessageModel } from './../../../models/configurations';
@Component({
  selector: 'app-active-patrol-message',
  templateUrl: './active-patrol-message.component.html',
})
export class ActivePatrolMessageComponent implements OnInit {

  activePatrolMessageId: string;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  activePatrolMessageForm: FormGroup;
  activePatrolMessageDetails: any;
  PageType;

  constructor(private activateRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private router: Router, private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.activePatrolMessageId = this.activateRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createactivePatrolMessageForm();
    if (this.activePatrolMessageId) {
      this.getactivePatrolMessageValues();
      this.PageType = "Update";
    } else {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.PageType = "Add";
    }
  }
  getactivePatrolMessageValues() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ACTIVE_PATROL_MESSAGE, this.activePatrolMessageId, true).subscribe((response) => {
      let activePatrolMessageAddEditModel = new ActivePatrolMessageModel(response.resources);
      this.activePatrolMessageDetails = response.resources;
      this.activePatrolMessageForm.setValue(activePatrolMessageAddEditModel);
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  createactivePatrolMessageForm(): void {
    let active_patrol_message_model = new ActivePatrolMessageModel();
    this.activePatrolMessageForm = this.formBuilder.group({
    });
    Object.keys(active_patrol_message_model).forEach((key) => {
      this.activePatrolMessageForm.addControl(key, new FormControl(active_patrol_message_model[key]));
    });
    this.activePatrolMessageForm = setRequiredValidator(this.activePatrolMessageForm, ["message", "description"]);
  }

  onSubmit(): void {
    if (this.activePatrolMessageForm.invalid) {
      return;
    }
    if (this.activePatrolMessageId) {
      this.activePatrolMessageForm.value.modifiedUserId = this.userData.userId;
      this.activePatrolMessageForm.value.activePatrolMessageId = this.activePatrolMessageId;
    } else {
      this.activePatrolMessageForm.value.createdUserId = this.userData.userId;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.activePatrolMessageId
      ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ACTIVE_PATROL_MESSAGE, this.activePatrolMessageForm.value)
      : this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ACTIVE_PATROL_MESSAGE, this.activePatrolMessageForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: 22 }, skipLocationChange: true });
      }
    })
  }

}
