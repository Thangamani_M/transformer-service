import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-active-patrol-message-view',
  templateUrl: './active-patrol-message.view.component.html',
})
export class ActivePatrolMessageViewComponent implements OnInit {
  activePatrolMessageId: string;
  activePatrolMessageDetails: any;
  primengTableConfigProperties:any;

  constructor(private activatedRoute: ActivatedRoute, private snackbarService:SnackbarService,  private store: Store<AppState>,private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.activePatrolMessageId = this.activatedRoute.snapshot.queryParams.id
    this.primengTableConfigProperties = {
      tableCaption: '',
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [ 
          {
            enableAction: true,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getActivePatrolMessageById(this.activePatrolMessageId);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ).subscribe((response) => {
      let permission = response[1][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        permission.forEach((element, index) => {
          if (index == 0) return
          this.primengTableConfigProperties.tableComponentConfigs.tabsList.push({})
        });
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getActivePatrolMessageById(activePatrolMessageId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ACTIVE_PATROL_MESSAGE, activePatrolMessageId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.activePatrolMessageDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  edit() {
    if (this.activePatrolMessageId) {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[22].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['event-management/event-configuration/active-patrol-message/add-edit'], { queryParams: { id: this.activePatrolMessageId } });

    }
  }


}
