import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { ActivePatrolMessageRoutingModule } from './active-patrol-message-routing.component';
import { ActivePatrolMessageComponent } from './active-patrol-message.component';
import { ActivePatrolMessageViewComponent } from './active-patrol-message.view.component';

@NgModule({
  declarations: [ActivePatrolMessageComponent, ActivePatrolMessageViewComponent],
  imports: [
    CommonModule,
    ActivePatrolMessageRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule
  ]
})
export class ActivePatrolMessageModule { }
