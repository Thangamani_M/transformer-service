import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { BoundaryLimitAddEditComponent } from './boundary-limit-add-edit.component';
import { BoundaryLimitRoutingModule } from './boundary-limit-routing.module';
import { BoundaryLimitViewComponent } from './boundary-limit-view.component';

@NgModule({
  declarations: [BoundaryLimitAddEditComponent, BoundaryLimitViewComponent],
  imports: [
    CommonModule,
    BoundaryLimitRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class BoundaryLimitModule { }
