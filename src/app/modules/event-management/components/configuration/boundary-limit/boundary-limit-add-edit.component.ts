import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { BoundaryLimitModel } from '@modules/event-management/models/configurations/boundary-limit.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-boundary-limit-add-edit',
  templateUrl: './boundary-limit-add-edit.component.html',
})
export class BoundaryLimitAddEditComponent implements OnInit {

  boundaryLimitId: any
  divisionDropDown: any = [];
  mainAreaList: any = [];
  boundaryLimitForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  selectedOptions: any = [];
  boundaryLimitDetails: any;
  selectedDivisionId: any;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.boundaryLimitId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.boundaryLimitId ? 'Update Boundary Limit':'Add Boundary Limit';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Boundary Limit', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 13 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.boundaryLimitId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/boundary-limit/view' , queryParams: {id: this.boundaryLimitId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createboundaryLimitForm();
    this.boundaryLimitForm.get("divisionId").valueChanges.subscribe(data => {
      this.getMainAreaList(data);
      this.selectedOptions = [];
    })

    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.boundaryLimitId) {
      this.getBoundaryLimitDetailsById(this.boundaryLimitId);
      return
    }
  }

  createboundaryLimitForm(): void {
    let boundaryLimitModel = new BoundaryLimitModel();
    // create form controls dynamically from model class
    this.boundaryLimitForm = this.formBuilder.group({
      maxDistance: [null, [Validators.required, Validators.min(1), Validators.max(999)]]
    });
    Object.keys(boundaryLimitModel).forEach((key) => {
      this.boundaryLimitForm.addControl(key, new FormControl(boundaryLimitModel[key]));
    });
    this.boundaryLimitForm = setRequiredValidator(this.boundaryLimitForm, ["divisionId", "mainAreaId"]);
    this.boundaryLimitForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.boundaryLimitForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.boundaryLimitId) {
      this.boundaryLimitForm.removeControl('boundaryLimitId');
      this.boundaryLimitForm.removeControl('modifiedUserId');
    } else {
      this.boundaryLimitForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getBoundaryLimitDetailsById(boundaryLimitId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOUNDARY_LIMIT, boundaryLimitId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.boundaryLimitDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.boundaryLimitDetails?.divisionName ? this.title +' - '+ this.boundaryLimitDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
          this.boundaryLimitForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSelectDivision() {
    this.boundaryLimitForm.controls.mainAreaId.setValue('');
  }

  onSubmit(): void {
    if (this.boundaryLimitForm.invalid) {
      return;
    }
    let formValue = this.boundaryLimitForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.boundaryLimitId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOUNDARY_LIMIT, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOUNDARY_LIMIT, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=13');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}