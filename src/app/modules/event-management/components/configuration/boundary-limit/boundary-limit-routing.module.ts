import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoundaryLimitAddEditComponent } from './boundary-limit-add-edit.component';
import { BoundaryLimitViewComponent } from './boundary-limit-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: BoundaryLimitAddEditComponent, data: { title: 'Boundary Limit Add/Edit' } },
  { path: 'view', component: BoundaryLimitViewComponent, data: { title: 'Boundary Limit View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class BoundaryLimitRoutingModule { }
