import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { VehicleRegistrationAddEditComponent } from './vehicle-registration-add-edit.component';
import { VehicleRegistrationRoutingModule } from './vehicle-registration-routing.module';
import { VehicleRegistrationViewComponent } from './vehicle-registration-view.component';

@NgModule({
  declarations: [VehicleRegistrationViewComponent, VehicleRegistrationAddEditComponent],
  imports: [
    CommonModule,
    VehicleRegistrationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class VehicleRegistrationModule { }
