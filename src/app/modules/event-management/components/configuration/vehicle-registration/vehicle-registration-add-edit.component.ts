import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig,  HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { VehicleRegistrarionModel } from '@modules/event-management/models/configurations/vehicle-registration.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-vehicle-registration-add-edit',
  templateUrl: './vehicle-registration-add-edit.component.html',
})
export class VehicleRegistrationAddEditComponent implements OnInit {

  vehicleRegistrationId: string;
  vehicleRegistrationDetails: any;
  pttDropDown: any = [];
  pdaDropDown: any = [];
  vehicalDropDown: any
  vehicleRegistrarionForm: FormGroup;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.vehicleRegistrationId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.vehicleRegistrationId ? 'Update Vehicle Registration':'Add Vehicle Registration';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Vehicle Registration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 5 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.vehicleRegistrationId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/vehicle-registration/view' , queryParams: {id: this.vehicleRegistrationId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createVehicleRegistrationForm();
    this.getPTTDropDown();
    this.getPDADropDown()
    this.getVehicalMakeDropDown()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.vehicleRegistrationId) {
      this.getVehicleRegistrationDetailsById(this.vehicleRegistrationId);
      return
    }

  }
 
  /// White Space not allowed
  space(e: any) {
    if (e.key == " ") {
      return false
    }
    else {
      return true
    }
  }
  createVehicleRegistrationForm(): void {
    let vehicleRegistrarionModel = new VehicleRegistrarionModel();
    // create form controls dynamically from model class
    this.vehicleRegistrarionForm = this.formBuilder.group({});
    Object.keys(vehicleRegistrarionModel).forEach((key) => {
      this.vehicleRegistrarionForm.addControl(key, new FormControl(vehicleRegistrarionModel[key]));
    });
    this.vehicleRegistrarionForm = setRequiredValidator(this.vehicleRegistrarionForm, ["vehicleRegistrationNumber", "assetNumber", "fleetNumber", "pdaRegistrationId", "pttRegistrationId", "vehicleTypeId"]);
    this.vehicleRegistrarionForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.vehicleRegistrarionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getVehicleRegistrationDetailsById(vehicleRegistrationId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_REGISTRATION, vehicleRegistrationId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleRegistrationDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.vehicleRegistrationDetails?.vehicleRegistrationNumber ? this.title +' - '+ this.vehicleRegistrationDetails?.vehicleRegistrationNumber : this.title + ' --', relativeRouterUrl: '' });
          this.vehicleRegistrarionForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getPTTDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_PTT_UNOCCUPIED, this.vehicleRegistrationId ? this.vehicleRegistrationId : '0', false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.pttDropDown = response.resources;
        }
      });
  }

  getVehicalMakeDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_DROPDOEN, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicalDropDown = response.resources;
        }
      });
  }

  getPDADropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_PDA_UNOCCUPIED, this.vehicleRegistrationId ? this.vehicleRegistrationId : '0', false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.pdaDropDown = response.resources;
        }
      });
  }
  onSubmit(): void {
    if (this.vehicleRegistrarionForm.invalid) {
      return;
    }
    let formValue = this.vehicleRegistrarionForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.vehicleRegistrationId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_REGISTRATION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_REGISTRATION, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=5');
      }
    })
  }

  onCRUDRequested(type){}

}