import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-vehicle-registration-view',
  templateUrl: './vehicle-registration-view.component.html',
})
export class VehicleRegistrationViewComponent implements OnInit {

vehicleRegistrationId: any
vehicleRegistrationDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any
totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
  tableComponentConfigs: {
    tabsList: []
  }
}

constructor(private activatedRoute: ActivatedRoute,private snackbarService: SnackbarService, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,  private store: Store<AppState>) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.vehicleRegistrationId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption: this.vehicleRegistrationId && 'View Vehicle Registration',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' }, 
    { displayName: 'Vehicle Registration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 5 } }
    , {displayName:'',}],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  this.rxjsService.setGlobalLoaderProperty(false);
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  }

  this.combineLatestNgrxStoreData()
  if (this.vehicleRegistrationId) {
    this.getVehicleRegistrationDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.vehicleRegistrationDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --"+ this.vehicleRegistrationDetails.vehicleRegistrationNumber;
      this.vehicleRegistrationDetails = [
        { name: 'Vehicle Registration Number', value: response.resources.vehicleRegistrationNumber },
        { name: 'Asset Number', value: response.resources.assetNumber },
        { name: 'Fleet Number', value: response.resources.fleetNumber },
        { name: 'PDA Registration', value: response.resources.pdaid},
        { name: 'Tracking', value: response.resources.tracking },
        { name: 'Vehicle Type', value: response.resources.vehicleType },
        { name: 'Vehicle Make', value: response.resources.vehicleMake },
        { name: 'Vehicle Model', value: response.resources.vehicleModel },
        { name: 'PTT Registration', value: response.resources.pttid },
        {name: 'Created By', value: response.resources.createdUserName },
        { name: 'Created On', value: response.resources.createdDate, isDateTime: true},
        { name: 'Modified By', value: response.resources.modifiedUserName },
        { name: 'Modified On', value: response.resources.modifiedDate , isDateTime:true},
        { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}



combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(['event-management/event-configuration/vehicle-registration/add-edit'], { queryParams: { id: this.vehicleRegistrationId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
}

getVehicleRegistrationDetails(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.VEHICLE_REGISTRATION,
    this.vehicleRegistrationId
  );
}

}
