import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleRegistrationAddEditComponent } from './vehicle-registration-add-edit.component';
import { VehicleRegistrationViewComponent } from './vehicle-registration-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: VehicleRegistrationAddEditComponent, data: { title: 'Vehicle Registration Add/Edit' } },
  { path: 'view', component: VehicleRegistrationViewComponent, data: { title: 'Vehicle Registration View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class VehicleRegistrationRoutingModule { }
