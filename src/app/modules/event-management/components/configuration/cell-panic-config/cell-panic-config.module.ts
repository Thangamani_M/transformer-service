import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { CellPanicConfigAddEditComponent } from './cell-panic-config-add-edit.component';
import { CellPanicConfigRoutingModule } from './cell-panic-config-routing.module';
import { CellPanicConfigViewComponent } from './cell-panic-config-view.component';

@NgModule({
  declarations: [CellPanicConfigAddEditComponent, CellPanicConfigViewComponent],
  imports: [
    CommonModule,
    CellPanicConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class CellPanicConfigModule { }
