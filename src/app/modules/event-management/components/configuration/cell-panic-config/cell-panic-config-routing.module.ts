import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CellPanicConfigAddEditComponent } from './cell-panic-config-add-edit.component';
import { CellPanicConfigViewComponent } from './cell-panic-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CellPanicConfigAddEditComponent, data: { title: 'Cell Panic Add/Edit' } },
  { path: 'view', component: CellPanicConfigViewComponent, data: { title: 'Cell Panic View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CellPanicConfigRoutingModule { }
