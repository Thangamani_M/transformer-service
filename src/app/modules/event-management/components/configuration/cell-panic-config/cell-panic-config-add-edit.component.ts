import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-cell-panic-config-add-edit',
  templateUrl: './cell-panic-config-add-edit.component.html',
})
export class CellPanicConfigAddEditComponent implements OnInit {

  statsConfigId: any;
  divisionDropDown: any = [];
  alarmTypeDropDown: any = [];
  eventTypeDropDown: any = [];
  statsConfigForm: FormGroup;
  loggedUser: any;
  statsConfigDetails: any;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.statsConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.statsConfigId ? 'Update Cell Panic Config':'Add Cell Panic Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Cell Panic Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 27} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.statsConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/cell-panic-config/view' , queryParams: {id: this.statsConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createStatsConfigForm();
    this.getAlarmTypeList();
    this.getEventTypeList();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.statsConfigId) {
      this.getStatsConfigDetailsById(this.statsConfigId);
      this.getAlarmTypeList();
      this.getEventTypeList();
      return
    }
  }

  createStatsConfigForm(): void {
    // create form controls dynamically from model class
    this.statsConfigForm = this.formBuilder.group({
      cellPanicConfigId: [''],
      decoderId: ['', Validators.required],
      setNameId: ['', Validators.required],
      createdUserId: ['', Validators.required],
      modifiedUserId: ['', Validators.required],
    });
    this.statsConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.statsConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getAlarmTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DECODERS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypeDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getEventTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SETNAMES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getStatsConfigDetailsById(statsConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CELL_PANIC_CONFIG, statsConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.statsConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.statsConfigDetails?.decoderName ? this.title +' - '+ this.statsConfigDetails?.decoderName : this.title + ' --', relativeRouterUrl: '' });
          this.statsConfigForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.statsConfigForm.invalid) {
      return;
    }
    let formValue = this.statsConfigForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.statsConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CELL_PANIC_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CELL_PANIC_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=27');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}
