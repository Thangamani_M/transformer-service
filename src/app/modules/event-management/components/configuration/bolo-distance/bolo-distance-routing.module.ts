import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BoloDistanceAddEditComponent } from './bolo-distance-add-edit.component';
import { BoloDistanceViewComponent } from './bolo-distance-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: BoloDistanceAddEditComponent, data: { title: 'BOLO Distance Add/Edit' } },
  { path: 'view', component: BoloDistanceViewComponent, data: { title: 'BOLO Distance View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class BoloDistanceRoutingModule { }
