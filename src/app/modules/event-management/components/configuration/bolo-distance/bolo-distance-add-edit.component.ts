import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { BoloDistanceModel } from '@modules/event-management/models/configurations/bolo-distance.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-bolo-distance-add-edit',
  templateUrl: './bolo-distance-add-edit.component.html',
})
export class BoloDistanceAddEditComponent implements OnInit {
  boloDistanceId: any;
  boloDistanceDetails: any;
  divisionDropDown: any = [];
  branchDropdown: any = [];
  loggedUser: any;
  boloDistanceForm: FormGroup;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.boloDistanceId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.boloDistanceId ? 'Update BOLO Distance':'Add BOLO Distance';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'BOLO Distance', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 16 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.boloDistanceId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/bolo-distance/view' , queryParams: {id: this.boloDistanceId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createboloDistanceForm();
    this.getDivisionDropDownById();
    this.getBranchDropDown();
    if (this.boloDistanceId) {
      this.getboloDistanceDetailsById(this.boloDistanceId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createboloDistanceForm(): void {
    let boloDistanceModel = new BoloDistanceModel();
    this.boloDistanceForm = this.formBuilder.group({});
    Object.keys(boloDistanceModel).forEach((key) => {
      this.boloDistanceForm.addControl(key, new FormControl(boloDistanceModel[key]));
    });
    this.boloDistanceForm = setRequiredValidator(this.boloDistanceForm, ["boloDistanceName", "boloDistanceDivisionList", "branchId", "distance"]);
    this.boloDistanceForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.boloDistanceForm.get('distance').setValidators([Validators.required,Validators.min(10),Validators.max(999)])
    this.boloDistanceForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.boloDistanceId) {
      this.boloDistanceForm.removeControl('boloDistanceId');
      this.boloDistanceForm.removeControl('modifiedUserId');
    } else {
      this.boloDistanceForm.removeControl('createdUserId');
    }
  }
  
  toggleDivisionOne() {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.boloDistanceForm.controls.boloDistanceDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.boloDistanceForm.controls.boloDistanceDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), ''
      ]);
    } else {
      this.boloDistanceForm.controls.boloDistanceDivisionList.patchValue([]);
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getBranchDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_BRANCH, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.branchDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getboloDistanceDetailsById(boloDistanceId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOLO_DISTANCE, boloDistanceId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.boloDistanceDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.boloDistanceDetails?.boloDistanceName ? this.title +' - '+ this.boloDistanceDetails?.boloDistanceName : this.title + ' --', relativeRouterUrl: '' });
          this.boloDistanceForm.patchValue(this.boloDistanceDetails);
          var boloDistanceDivisionList = [];
          response.resources.boloDistanceDivisionList.forEach(element => {
            boloDistanceDivisionList.push(element.divisionId);
          });
          this.boloDistanceForm.get('boloDistanceDivisionList').setValue(boloDistanceDivisionList)

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onSubmit(): void {
    if (this.boloDistanceForm.invalid) {
      return;
    }
    let formValue = this.boloDistanceForm.value;
    if (formValue.boloDistanceDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.boloDistanceDivisionList = formValue.boloDistanceDivisionList.filter(item => item != '')
      formValue.boloDistanceDivisionList = formValue.boloDistanceDivisionList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.boloDistanceId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOLO_DISTANCE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.BOLO_DISTANCE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=16');
      }
    })
  }

  onCRUDRequested(type){}

}
