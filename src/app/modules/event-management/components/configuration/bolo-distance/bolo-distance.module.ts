import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { BoloDistanceAddEditComponent } from './bolo-distance-add-edit.component';
import { BoloDistanceRoutingModule } from './bolo-distance-routing.module';
import { BoloDistanceViewComponent } from './bolo-distance-view.component';

@NgModule({
  declarations: [BoloDistanceAddEditComponent, BoloDistanceViewComponent],
  imports: [
    CommonModule,
    BoloDistanceRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class BoloDistanceModule { }
