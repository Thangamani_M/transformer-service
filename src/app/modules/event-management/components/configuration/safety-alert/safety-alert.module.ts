import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { SafetyAlertAddEditComponent } from './safety-alert-add-edit.component';
import { SafetyAlertRoutingModule } from './safety-alert-routing.module';
import { SafetyAlertViewComponent } from './safety-alert-view.component';



@NgModule({
  declarations: [SafetyAlertAddEditComponent, SafetyAlertViewComponent],
  imports: [
    CommonModule,
    SafetyAlertRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SafetyAlertModule { }
