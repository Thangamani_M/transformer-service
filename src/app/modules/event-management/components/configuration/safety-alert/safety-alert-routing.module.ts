import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SafetyAlertAddEditComponent } from './safety-alert-add-edit.component';
import { SafetyAlertViewComponent } from './safety-alert-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SafetyAlertAddEditComponent, data: { title: 'Safety Alert Add/Edit' } },
  { path: 'view', component: SafetyAlertViewComponent, data: { title: 'Safety Alert View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SafetyAlertRoutingModule { }
