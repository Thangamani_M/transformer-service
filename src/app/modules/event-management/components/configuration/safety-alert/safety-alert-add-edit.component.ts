import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { SafetyAlertConfigMainAreasListModel, SafetyAlertModel } from '@modules/event-management/models/configurations/safety-alert.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
    selector: 'app-safety-alert-add-edit',
    templateUrl: './safety-alert-add-edit.component.html',
})
export class SafetyAlertAddEditComponent implements OnInit {
    safetyAlertConfigId: any
    divisionDropDown: any = [];
    mainAreaDropDown: any = [];
    safetyAlertForm: FormGroup;
    safetyAlertConfigMainAreasList: FormArray;
    loggedUser: any;
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    safetyAlertDetails: any;
    isDuplicate: boolean;
    @ViewChildren('input') rows: QueryList<any>;
    totaltab = 0;
    selectedTab = 0;
    primengTableConfigProperties: any;
    title:string;

    constructor(private activatedRoute: ActivatedRoute,
        private snackbarService: SnackbarService,
        private router: Router,
        private httpCancelService: HttpCancelService,
        private store: Store<AppState>,
        private formBuilder: FormBuilder,
        private rxjsService: RxjsService,
        private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
        private crudService: CrudService) {
        this.safetyAlertConfigId = this.activatedRoute.snapshot.queryParams.id;
        this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
        this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.title =this.safetyAlertConfigId ? 'Update Safety Alert':'Add Safety Alert';
        this.primengTableConfigProperties = {
          tableCaption: this.title,
          selectedTabIndex: 0,
          breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
          { displayName: 'Safety Alert', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 11 } }],
          tableComponentConfigs: {
            tabsList: [
              {
                enableBreadCrumb: true,
                enableAction: false,
                enableEditActionBtn: false,
                enableClearfix: true,
              }
            ]
          }
        }
        if(this.safetyAlertConfigId){
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/safety-alert/view' , queryParams: {id: this.safetyAlertConfigId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
        }
        else{
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
        }
    }

    ngOnInit() {
        this.createSafetyAlertForm();
        this.getDivisionDropDownById();
        this.safetyAlertForm.get("divisionId").valueChanges.subscribe(data => {
            this.getMainAreaDropDownById(data);

        })
        this.rxjsService.setGlobalLoaderProperty(false);
        if (this.safetyAlertConfigId) {

            this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
                let cmcSmsGroup = new SafetyAlertModel(response.resources);

                this.safetyAlertDetails = response.resources;
                this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.safetyAlertDetails?.divisionName ? this.title +' - '+ this.safetyAlertDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
                this.safetyAlertForm.patchValue(cmcSmsGroup);

                this.safetyAlertConfigMainAreasList = this.getSafetyAlertConfigMainAreasListArray;
                response.resources.safetyAlertConfigMainAreasList.forEach((safetyAlertConfigMainAreasListModel: SafetyAlertConfigMainAreasListModel) => {
                    this.safetyAlertConfigMainAreasList.push(this.createSafetyAlertConfigMainAreasListModel(safetyAlertConfigMainAreasListModel));

                });
            })
        } else {
            this.safetyAlertConfigMainAreasList = this.getSafetyAlertConfigMainAreasListArray;
            this.safetyAlertConfigMainAreasList.push(this.createSafetyAlertConfigMainAreasListModel());

        }
    }

    createSafetyAlertForm(): void {
        let safetyAlertModel = new SafetyAlertModel();
        this.safetyAlertForm = this.formBuilder.group({
            safetyAlertConfigMainAreasList: this.formBuilder.array([])
        });
        Object.keys(safetyAlertModel).forEach((key) => {
            this.safetyAlertForm.addControl(key, new FormControl(safetyAlertModel[key]));
        });
        this.safetyAlertForm = setRequiredValidator(this.safetyAlertForm, ["divisionId"]);
        this.safetyAlertForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.safetyAlertForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    }

    getDivisionDropDownById() {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    this.divisionDropDown = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    getMainAreaDropDownById(selectedDivisionId) {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    this.mainAreaDropDown = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    //Create FormArray
    get getSafetyAlertConfigMainAreasListArray(): FormArray {
        if (!this.safetyAlertForm) return;
        return this.safetyAlertForm.get("safetyAlertConfigMainAreasList") as FormArray;
    }


    //Create FormArray controls
    createSafetyAlertConfigMainAreasListModel(safetyAlertConfigMainAreasListModel?: SafetyAlertConfigMainAreasListModel): FormGroup {
        let safetyAlertConfigMainAreasListFormControlModel = new SafetyAlertConfigMainAreasListModel(safetyAlertConfigMainAreasListModel);
        let formControls = {};
        Object.keys(safetyAlertConfigMainAreasListFormControlModel).forEach((key) => {
            if (key === 'mainAreaId' || key === 'safetyAlertTimer' || key === 'passwordWait' || key === 'isActive') {
                formControls[key] = [{ value: safetyAlertConfigMainAreasListFormControlModel[key], disabled: false }, [Validators.required]];
                formControls['safetyAlertTimer'] = [{ value: safetyAlertConfigMainAreasListFormControlModel['safetyAlertTimer'], disabled: false }, [Validators.required, Validators.min(10), Validators.max(9999)]];
                formControls['passwordWait'] = [{ value: safetyAlertConfigMainAreasListFormControlModel['passwordWait'], disabled: false }, [Validators.required, Validators.min(10), Validators.max(99999999)]];

            } else if (this.safetyAlertConfigId) {
                formControls[key] = [{ value: safetyAlertConfigMainAreasListFormControlModel[key], disabled: false }]
            }
        });
        return this.formBuilder.group(formControls);
    }

    //Get Details 
    getCmcSmsDetailsById(): Observable<IApplicationResponse> {
        return this.crudService.get(
            ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            EventMgntModuleApiSuffixModels.SAFETY_ALERT,
            this.safetyAlertConfigId
        );
    }
    onSelectDivision() {
        this.isDuplicate = false;
        const arr = <FormArray>this.safetyAlertForm.controls.safetyAlertConfigMainAreasList;
        arr.controls = [];
        this.safetyAlertConfigMainAreasList = this.getSafetyAlertConfigMainAreasListArray;
        this.safetyAlertConfigMainAreasList.push(this.createSafetyAlertConfigMainAreasListModel());
    }

    OnChange(index): boolean {
        if (this.getSafetyAlertConfigMainAreasListArray.length > 1) {
            this.isDuplicate = false;
            var cloneArray = this.getSafetyAlertConfigMainAreasListArray.value.slice();
            cloneArray.splice(index, 1);

            var findIndex = cloneArray.some(x => x.mainAreaId === this.getSafetyAlertConfigMainAreasListArray.value[index].mainAreaId);
            if (findIndex) {
                this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
                this.isDuplicate = true;
                return false;
            } else {
                this.isDuplicate = false;
            }

        }
    }

    focusInAndOutFormArrayFields(): void {
        this.rows.forEach((item) => {
            item.nativeElement.focus();
            item.nativeElement.blur();
        })
    }
    //Add Employee Details
    addCmcSmsGroupEmployee(): void {
        if (this.getSafetyAlertConfigMainAreasListArray.invalid) {
            this.focusInAndOutFormArrayFields();
            return;
        };
        if (this.isDuplicate) {
            this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
            return;
        }
        this.safetyAlertConfigMainAreasList = this.getSafetyAlertConfigMainAreasListArray;
        let safetyAlertConfigMainAreasListModel = new SafetyAlertConfigMainAreasListModel();
        this.safetyAlertConfigMainAreasList.insert(0, this.createSafetyAlertConfigMainAreasListModel(safetyAlertConfigMainAreasListModel));
        this.rxjsService.setFormChangeDetectionProperty(true);
    }

    removeCmcSmsEmployee(i: number): void {
        this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
            onClose?.subscribe(dialogResult => {
                if (dialogResult) {
                    if (this.getSafetyAlertConfigMainAreasListArray.length === 1) {
                        this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
                        return
                    }
                    if (this.getSafetyAlertConfigMainAreasListArray.controls[i].value.safetyAlertConfigMainAreaId && this.getSafetyAlertConfigMainAreasListArray.length > 1) {
                        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SAFETY_ALERT_CONFIG_MAIN_AREA,
                            this.getSafetyAlertConfigMainAreasListArray.controls[i].value.safetyAlertConfigMainAreaId).subscribe((response: IApplicationResponse) => {
                                if (response.isSuccess) {
                                    this.getSafetyAlertConfigMainAreasListArray.removeAt(i);
                                }
                                if (this.getSafetyAlertConfigMainAreasListArray.length === 0) {
                                    this.addCmcSmsGroupEmployee();
                                };
                            });
                    }
                    else {
                        this.getSafetyAlertConfigMainAreasListArray.removeAt(i);
                        this.rxjsService.setFormChangeDetectionProperty(true);
                    }
                }
            });
    };


    onSubmit(): void {
        if (this.safetyAlertForm.invalid) {
            return;
        }

        if (this.isDuplicate) {
            this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
            return;
        }

        let formValue = this.safetyAlertForm.value;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> = (!this.safetyAlertConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SAFETY_ALERT, formValue) :
            this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SAFETY_ALERT, formValue)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
                this.router.navigateByUrl('/event-management/ro-configuration?tab=11');
            }
        })
    }

    onCRUDRequested(type){}

}

