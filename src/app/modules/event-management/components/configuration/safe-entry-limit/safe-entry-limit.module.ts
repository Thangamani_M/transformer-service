import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { SafeEntryLimitAddEditComponent } from './safe-entry-limit-add-edit.component';
import { SafeEntryLimitRoutingModule } from './safe-entry-limit-routing.module';
import { SafeEntryLimitViewComponent } from './safe-entry-limit-view.component';

@NgModule({
  declarations: [SafeEntryLimitAddEditComponent, SafeEntryLimitViewComponent],
  imports: [
    CommonModule,
    SafeEntryLimitRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class SafeEntryLimitModule { }