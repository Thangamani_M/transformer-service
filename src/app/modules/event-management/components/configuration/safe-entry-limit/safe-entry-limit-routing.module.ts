import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SafeEntryLimitAddEditComponent } from './safe-entry-limit-add-edit.component';
import { SafeEntryLimitViewComponent } from './safe-entry-limit-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SafeEntryLimitAddEditComponent, data: { title: 'Safe Entry Limit Add/Edit' } },
  { path: 'view', component: SafeEntryLimitViewComponent, data: { title: 'Safe Entry Limit View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SafeEntryLimitRoutingModule { }