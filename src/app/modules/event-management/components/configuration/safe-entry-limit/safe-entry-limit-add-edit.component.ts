import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { SafeEntryLimitAddEditModel } from '@modules/event-management/models/configurations/safe-entry-limit.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-safe-entry-limit-config-add-edit',
  templateUrl: './safe-entry-limit-add-edit.component.html',
})
export class SafeEntryLimitAddEditComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  safeEntryLimitAddEditForm: FormGroup;
  safeEntryLimitCustomersList: any = [];
  loggedUser: UserLogin;
  safeEntryLimitId: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  divisionDropDown: any;
  safeEntryLimitDetails: any;
  divisionIds: string;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  title:string;

  constructor(private rxjsService: RxjsService,  private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private momentService: MomentService, private formBuilder: FormBuilder, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private router: Router) {
    super();
    this.safeEntryLimitId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.safeEntryLimitId ? 'Update Safe Entry Limit':'Add Safe Entry Limit';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Safe Entry Limit', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 8 } }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'customerNumber', header: 'Customer Code', width: '80px' }, { field: 'displayName', header: 'Customer Name', width: '120px' }, { field: 'streetAddress', header: 'Address', width: '80px' },
            { field: 'mobileNumber', header: 'Mobile Number', width: '80px' }, { field: 'customerTypeName', header: 'Customer Type', width: '120px' }, { field: 'namedStack', header: 'Named Stack', width: '80px' },
            { field: 'email', header: 'Email', width: '120px' }, { field: 'addressCreatedDate', header: 'Created On', width: '120px', isDate:true }, { field: 'isActive', header: 'Status', width: '120px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
    if(this.safeEntryLimitId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/safe-entry-limit/view' , queryParams: {id: this.safeEntryLimitId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {

    this.createSafeEntryLimitAddEditForm();
    this.getDivisionDropDown();
    let otherParams = {}
    this.rxjsService.setGlobalLoaderProperty(false);
    this.row['pageIndex'] = 0
    this.row['pageSize'] = 20
    if (this.safeEntryLimitId) {
      this.getSafeEntryLimitDetailsById(this.safeEntryLimitId);
    } else {
      this.getCustomerAddress(this.row['pageIndex'], this.row['pageSize']);
    }
  }

  createSafeEntryLimitAddEditForm(): void {
    let safeEntryLimitAddEditModel = new SafeEntryLimitAddEditModel();
    this.safeEntryLimitAddEditForm = this.formBuilder.group({
      maxCount: [null, [Validators.required, Validators.min(1), Validators.max(99)]]
    });
    Object.keys(safeEntryLimitAddEditModel).forEach((key) => {
      this.safeEntryLimitAddEditForm.addControl(key, new FormControl(safeEntryLimitAddEditModel[key]));
    });
    this.safeEntryLimitAddEditForm = setRequiredValidator(this.safeEntryLimitAddEditForm, ["safeEntryLimitName", "safeEntryLimitDivisionsList"]);
    this.safeEntryLimitAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.safeEntryLimitAddEditForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.safeEntryLimitId) {
      this.safeEntryLimitAddEditForm.removeControl('modifiedUserId');
      this.safeEntryLimitAddEditForm.removeControl('safeEntryLimitId');
    } else {
      this.safeEntryLimitAddEditForm.removeControl('createdUserId');
    }
  }

  toggleDivisionOne() {
    this.divisionIds = this.safeEntryLimitAddEditForm.controls.safeEntryLimitDivisionsList.value.join(",");
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.divisionIds) {
      this.getCustomerAddress();
      this.rxjsService.setGlobalLoaderProperty(false);
    } else {
      this.dataList = [];
    }
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.safeEntryLimitAddEditForm.controls.safeEntryLimitDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {

    if (this.allSelectedDivision.selected) {
      this.safeEntryLimitAddEditForm.controls.safeEntryLimitDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
      let value = this.safeEntryLimitAddEditForm.controls.safeEntryLimitDivisionsList.value.join(',')
      this.divisionIds = value.replace(/,([^,]*)$/, '$1')
      this.getCustomerAddress();
      this.rxjsService.setGlobalLoaderProperty(false);

    } else {
      this.safeEntryLimitAddEditForm.controls.safeEntryLimitDivisionsList.patchValue([]);
      this.dataList = [];
    }
  }


  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSafeEntryLimitDetailsById(safeEntryLimitId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SAFE_ENTRY_LIMIT, safeEntryLimitId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.safeEntryLimitDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.safeEntryLimitDetails?.safeEntryLimitName ? this.title +' - '+ this.safeEntryLimitDetails?.safeEntryLimitName : this.title + ' --', relativeRouterUrl: '' });
          this.safeEntryLimitAddEditForm.patchValue(this.safeEntryLimitDetails);
          var safeEntryLimitDivisionsList = [];
          let abc = '';
          response.resources.safeEntryLimitDivisionsList.forEach((element, index) => {
            safeEntryLimitDivisionsList.push(element.divisionId);

            if (index == 0) {
              abc = abc + '' + element.divisionId
            } else {
              abc = abc + ',' + element.divisionId
            }
          });

          this.divisionIds = abc;
          this.safeEntryLimitAddEditForm.get('safeEntryLimitDivisionsList').setValue(safeEntryLimitDivisionsList)
          this.getCustomerAddress(this.row['pageIndex'], this.row['pageSize']);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCustomerAddress(pageIndex?: string, pageSize?: string, otherParams?: object) {
    if (this.divisionIds == undefined) {
      return
    }

    let parms = { DivisionIds: this.divisionIds }
    otherParams = { ...otherParams, ...parms };
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS_DIVISION,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(resp => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.dataList = resp.resources;
      this.totalRecords = resp.totalCount;
      if (this.safeEntryLimitId) {
        this.dataList.forEach((row: any) => {
          this.safeEntryLimitDetails.safeEntryLimitCustomersList.forEach(element => {
            if (row.customerId == element.customerId) {
              this.selectedRows.push(row)
              this.selectedRows = [...this.selectedRows]
            }
          });
        });
      }
    });
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getCustomerAddress(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onChangeSelecedRows(event) {
    this.selectedRows = event
  }

  onSubmit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true)
    if (this.safeEntryLimitAddEditForm.invalid) {
      return;
    }
    this.safeEntryLimitCustomersList = [];
    this.selectedRows.forEach(item => {
      this.safeEntryLimitCustomersList.push({
        customerId: item.customerId,
        addressId: item.customerAddressId
      });
    });
    this.safeEntryLimitAddEditForm.get('safeEntryLimitCustomersList').setValue(this.safeEntryLimitCustomersList)
    let formValue = this.safeEntryLimitAddEditForm.value;
    if (formValue.safeEntryLimitDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.safeEntryLimitDivisionsList = formValue.safeEntryLimitDivisionsList.filter(item => item != '')
      formValue.safeEntryLimitDivisionsList = formValue.safeEntryLimitDivisionsList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.safeEntryLimitId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SAFE_ENTRY_LIMIT, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SAFE_ENTRY_LIMIT, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=8');
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}