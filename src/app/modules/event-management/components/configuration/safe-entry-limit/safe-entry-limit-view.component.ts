import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { SafeEntryLimitView } from '@modules/event-management/models/configurations/safe-entry-limit.model';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-safe-entry-SafeEntryLimitViewlimit-view',
  templateUrl: './safe-entry-limit-view.component.html',
})
export class SafeEntryLimitViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  SafeENtryLimitViewDetails:any;
  row: any = {};
  loggedUser: UserLogin;
  namedStackMappingDetails: any;
  namedStackMappingId: any;
  safeEntryLimitDetails: any;
  divisionIds: any;
  safeEntryLimitId: any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }
  

  constructor(private rxjsService: RxjsService,private snackbarService:SnackbarService, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private momentService: MomentService, private store: Store<AppState>,
    private router: Router) {
    super();
    this.safeEntryLimitId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View Safe Entry Limit',
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Safe Entry Limit', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 8 } }, { displayName: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableViewBtn: true,
            columns: [{ field: 'customerNumber', header: 'Customer Id', width: '80px' },
            { field: 'displayName', header: 'Customer Name', width: '120px' },
            { field: 'streetAddress', header: 'Address', width: '80px' },
            { field: 'mobileNumber', header: 'Mobile Number', width: '80px' },
            { field: 'customerTypeName', header: 'Customer Type', width: '120px' },
            { field: 'namedStack', header: 'Named Stack', width: '80px' },
            { field: 'email', header: 'Email', width: '120px' },
            { field: 'addressCreatedDate', header: 'Created On', width: '120px', isDateTime:true },
            { field: 'isActive', header: 'Status', width: '120px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    }
    this.combineLatestNgrxStoreData()
    this.safeEntryLimitDetails = new SafeEntryLimitView();
    this.getsafeEntryLimitById(this.safeEntryLimitId)
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onShowValue(response?: any) {
    this.SafeENtryLimitViewDetails = [
      { name: 'Division', value: response?.safeEntryLimitDivisions },
      { name: 'Description', value: response?.safeEntryLimitName },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified By', value: response?.modifiedUserName },
      { name: 'Modified On', value: response?.modifiedDate ,isDateTime: true},
      { name: 'Safe Entry Limit', value: response?.maxCount},
      { name: 'Status', value: response?.isActive == true ? 'Active' : 'In Active', statusClass: response?.isActive==true?"status-label-green":'status-label-pink'},
    ]
  }

  getsafeEntryLimitById(safeEntryLimitId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SAFE_ENTRY_LIMIT, safeEntryLimitId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.safeEntryLimitDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.safeEntryLimitDetails?.safeEntryLimitName;
          this.onShowValue(response.resources);
          var safeEntryLimitDivisionsList = [];
          let abc = '';
          response.resources.safeEntryLimitDivisionsList.forEach((element, index) => {
            safeEntryLimitDivisionsList.push(element.divisionId);
            this.divisionIds = safeEntryLimitDivisionsList;
            if (index == 0) {
              abc = abc + '' + element.divisionId
            } else {
              abc = abc + ',' + element.divisionId
            }
          });
          this.getSignalHistoryList();
        }

      });
  }

  getSignalHistoryList(pageIndex?: string, pageSize?: string, searchKey?: string) {
    if (this.divisionIds == undefined) {
      return
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS_DIVISION,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        search: searchKey ? searchKey : '',
        DivisionIds: this.divisionIds
      })
    ).subscribe(resp => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.dataList = resp.resources;

      this.dataList.map(item => {
        item.checkHidden = true
      })
      this.totalRecords = resp.totalCount;
      if (this.safeEntryLimitId) {
        this.dataList.forEach((row: any) => {
          this.safeEntryLimitDetails.safeEntryLimitCustomersList.forEach(element => {
            if (row.customerId == element.customerId) {
              this.selectedRows.push(row)
              this.selectedRows = [...this.selectedRows]
              row.checkedValue = row
            }
          });
        });
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }


  onChangeSelecedRows(event) {
    this.selectedRows = event
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  navigateToEdit() {
    if (this.safeEntryLimitId) {
      if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['event-management/event-configuration/safe-entry-limit/add-edit'], { queryParams: { id: this.safeEntryLimitId ,totalTabs:this.totaltab, selectedTab:this.selectedTab} });
    }
  }

}
