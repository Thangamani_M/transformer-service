import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { DecoderAccountConfigModel } from '@modules/event-management/models/configurations/decoder-account-config.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-decoder-account-config-add-edit',
  templateUrl: './decoder-account-config-add-edit.component.html',
})
export class DecoderAccountConfigAddEditComponent implements OnInit {

  decoderAccountConfigId: any;
  eventTypeDetails: any;
  decoderAccountConfigForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  decoderList: any = [];
  commsTypeList: any = [];
  setNameList: any = [];
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.decoderAccountConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.decoderAccountConfigId ? 'Update Decoder Account Config':'Add Decoder Account Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Decoder Account Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 26 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.decoderAccountConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/decoder-account-config/view' , queryParams: {id: this.decoderAccountConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_DECODERS, undefined, null),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_SETNAMES),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_COMMSTYPES),
    ];
    this.loadActionTypes(dropdownsAndData);
    this.createdecoderAccountConfigForm();

    if (this.decoderAccountConfigId) {
      this.getEventTypeDetailsById(this.decoderAccountConfigId);
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.decoderList = resp.resources;
              break;
            case 1:
              this.setNameList = resp.resources
              break;
            case 2:
              this.commsTypeList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createdecoderAccountConfigForm(): void {
    let eventTypeModel = new DecoderAccountConfigModel();
    this.decoderAccountConfigForm = this.formBuilder.group({
    });
    Object.keys(eventTypeModel).forEach((key) => {
      this.decoderAccountConfigForm.addControl(key, new FormControl(eventTypeModel[key]));
    });
    this.decoderAccountConfigForm = setRequiredValidator(this.decoderAccountConfigForm, ["decoderId", 'setNamesList', "commsTypeName"]);
    this.decoderAccountConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.decoderAccountConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)

    this.decoderAccountConfigForm.get('isManualAccountCodeGeneration').valueChanges.subscribe((code: boolean) => {
      if (code) {
        this.decoderAccountConfigForm.get('length').patchValue('');
        this.decoderAccountConfigForm.get('length').clearValidators();
        this.decoderAccountConfigForm.get('length').updateValueAndValidity();
      }
      else {
        this.decoderAccountConfigForm = setRequiredValidator(this.decoderAccountConfigForm, ['length']);
        this.decoderAccountConfigForm.controls['length'].setValidators([Validators.required, , Validators.min(1), Validators.max(50)])
      }
    });
  }

  getEventTypeDetailsById(decoderAccountConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DECODER_ACCOUNT_CONFIGURATIONS, decoderAccountConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.eventTypeDetails?.decoderName ? this.title +' - '+ this.eventTypeDetails?.decoderName : this.title + ' --', relativeRouterUrl: '' });
          this.eventTypeDetails.setNamesList.forEach((element) => {
            element.id = element.setNameId
          });
          this.decoderAccountConfigForm.patchValue(this.eventTypeDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.decoderAccountConfigForm.invalid) {
      return;
    }
    let formValue = this.decoderAccountConfigForm.value;
    if ((typeof (formValue.commsTypeName)) === 'object') {
      formValue.commsTypeName = formValue.commsTypeName.displayName
    }
    formValue.setNamesList.forEach((element) => {
      element.setNameId = element.id
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.decoderAccountConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DECODER_ACCOUNT_CONFIGURATIONS, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DECODER_ACCOUNT_CONFIGURATIONS, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=26');
      }
    })
  }

  onCRUDRequested(type){}

}
