import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DecoderAccountConfigAddEditComponent } from './decoder-account-config-add-edit.component';
import { DecoderAccountConfigViewComponent } from './decoder-account-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';




const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: DecoderAccountConfigAddEditComponent, data: { title: 'Decoder Account Add/Edit' } },
  { path: 'view', component: DecoderAccountConfigViewComponent, data: { title: 'Decoder Account View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DecoderAccountConfigRoutingModule { }
