import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { DecoderAccountConfigAddEditComponent } from './decoder-account-config-add-edit.component';
import { DecoderAccountConfigRoutingModule } from './decoder-account-config-routing.module';
import { DecoderAccountConfigViewComponent } from './decoder-account-config-view.component';

@NgModule({
  declarations: [DecoderAccountConfigAddEditComponent, DecoderAccountConfigViewComponent],
  imports: [
    CommonModule,
    DecoderAccountConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule
  ]
})
export class DecoderAccountConfigModule { }
