import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-decoder-account-config-view',
  templateUrl: './decoder-account-config-view.component.html',
})
export class DecoderAccountConfigViewComponent implements OnInit {
  decoderAccountConfigId: string;
  eventTypeDetails: any;
  primengTableConfigProperties:any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  };

  constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.decoderAccountConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;

    this.primengTableConfigProperties = {
      tableCaption: 'View Decoder Account Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Decoder Account Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 26 } }, { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData()
    this.getEventTypeDetailsById(this.decoderAccountConfigId);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getEventTypeDetailsById(decoderAccountConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DECODER_ACCOUNT_CONFIGURATIONS, decoderAccountConfigId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.eventTypeDetails?.decoderName;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  edit() {
    if (this.decoderAccountConfigId) {
      if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['event-management/event-configuration/decoder-account-config/add-edit'], { queryParams: { id: this.decoderAccountConfigId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit();
        break;
    }
  }
}

