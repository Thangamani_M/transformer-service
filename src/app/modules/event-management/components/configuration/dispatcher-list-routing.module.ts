import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DispatcherListComponent } from './dispatcher/dispatcher-list.component';
import { DispatcherSignalHistoryComponent } from './dispatcher/signal-history/signal-history.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  {
    path: '', component: DispatcherListComponent, data: { title: 'Dispatcher' },canActivate: [AuthGuard]
  },
  {
    path: 'signal-history', component: DispatcherSignalHistoryComponent, data: { title: 'Signal History' }
  },
  { path: 'call-workflow-list', loadChildren: () => import('../configuration/call-workflow/call-workflow-list.module').then(m => m.CallWorkFlowListModule), data: { preload: true } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DispatcherListRoutingModule { }
