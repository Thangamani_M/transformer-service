import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { PatrolConfigModel } from '@modules/event-management/models/configurations/fail-to-test-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-patrol-config-add-edit',
  templateUrl: './patrol-config-add-edit.component.html',
})
export class PatrolConfigAddEditComponent implements OnInit {

  patrolConfigAddEditForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  selectedDivisionId: any;
  divisionDropDown: any;
  mainAreaList: any;
  id: any;
  patrolDetails: any;
  timeObject = new Date();
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;
  constructor(private activatedRoute: ActivatedRoute, public momentService: MomentService, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.id ? 'Update Patrol Config':'Add Patrol Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Patrol Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 29 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.id){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/patrol-config/view' , queryParams: {id: this.id,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createpatrolConfigAddEditForm();
    this.getDivisionDropdown();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.id) {
      this.getDetailsById(this.id);
      return
    }
    this.onFormControlChanges();
  }

  onFormControlChanges() {
    this.patrolConfigAddEditForm.get('patrolConfigMainAreaDetails').valueChanges.subscribe((patrolConfigMainAreaDetails: any) => {
      if (patrolConfigMainAreaDetails.length > 0) {
        this.ismainArea = false;
      }
    });
  }

  emitValue(event: any) {
    this.timeObject = new Date(this.patrolConfigAddEditForm.value.generalPatrolWindowStart);
    this.timeObject.setTime(this.timeObject.getTime() + 1000 * 60);
  }

  endtimeChange(event) {
    if (+event.value < +this.timeObject) {
      this.patrolConfigAddEditForm.get("generalPatrolWindowEnd").setValue(this.timeObject);
    }

  }
  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
 
  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createpatrolConfigAddEditForm(): void {
    let patrolConfigModel = new PatrolConfigModel();
    // create form controls dynamically from model class
    this.patrolConfigAddEditForm = this.formBuilder.group({
    });
    Object.keys(patrolConfigModel).forEach((key) => {
      this.patrolConfigAddEditForm.addControl(key, new FormControl(patrolConfigModel[key]));
    });
    this.patrolConfigAddEditForm = setRequiredValidator(this.patrolConfigAddEditForm, ["patrolConfigName", "patrolConfigDescription", "divisionId", "patrolConfigMainAreaDetails", "generalPatrolWindowStart", "generalPatrolWindowEnd", "generalPatrolCommunicationWindowStart", "generalPatrolCommunicationWindowEnd", "postIncidentPatrolDays", "postIncidentDailyPatrolLimit", "withoutArrivalDays", "dailyLimit", "maxHolidayCount", "holidayPatrolDailyLimit", "maxHolidayPaidPatrolAllowed", "visibilityPatrolMaxDays", "dailyVisibilityPatrolLimit", "maxAllowedPaidPatrol"]);
    this.patrolConfigAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.patrolConfigAddEditForm.get('isStatus').setValue(true);
  }

  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date)
  }


  getDetailsById(id: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PATROL_CONFIG_POST, id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.patrolDetails = JSON.parse(JSON.stringify(response.resources));
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.patrolDetails.patrolConfigName ? this.title +' - '+this.patrolDetails.patrolConfigName : this.title + ' --', relativeRouterUrl: '' });
          this.patrolConfigAddEditForm.controls['patrolConfigName'].patchValue(response.resources.patrolConfigName);
          this.patrolConfigAddEditForm.controls['patrolConfigDescription'].patchValue(response.resources.patrolConfigDescription);
          this.patrolConfigAddEditForm.controls['divisionId'].patchValue(response.resources.divisionId);
          this.patrolConfigAddEditForm.controls['generalPatrolWindowStart'].patchValue(this.formateTime(response.resources.generalPatrolWindowStart));
          this.patrolConfigAddEditForm.controls['generalPatrolWindowEnd'].patchValue(this.formateTime(response.resources.generalPatrolWindowEnd));
          this.patrolConfigAddEditForm.controls['generalPatrolCommunicationWindowStart'].patchValue(this.formateTime(response.resources.generalPatrolCommunicationWindowStart));
          this.patrolConfigAddEditForm.controls['generalPatrolCommunicationWindowEnd'].patchValue(this.formateTime(response.resources.generalPatrolCommunicationWindowEnd));
          

          this.patrolConfigAddEditForm.controls['postIncidentPatrolDays'].patchValue(response.resources.postIncidentPatrolDays);
          this.patrolConfigAddEditForm.controls['postIncidentDailyPatrolLimit'].patchValue(response.resources.postIncidentDailyPatrolLimit);
          this.patrolConfigAddEditForm.controls['isPostIncidentLimitedToPatrolWindow'].patchValue(response.resources.isPostIncidentLimitedToPatrolWindow);
          this.patrolConfigAddEditForm.controls['isPostIncidentPatrolRequireCommunication'].patchValue(response.resources.isPostIncidentPatrolRequireCommunication);
          this.patrolConfigAddEditForm.controls['withoutArrivalDays'].patchValue(response.resources.withoutArrivalDays);
          this.patrolConfigAddEditForm.controls['dailyLimit'].patchValue(response.resources.dailyLimit);
          this.patrolConfigAddEditForm.controls['isAutoPatrolLimitedToPatrolWindow'].patchValue(response.resources.isAutoPatrolLimitedToPatrolWindow);
          this.patrolConfigAddEditForm.controls['isAutoPatrolRequireCommunication'].patchValue(response.resources.isAutoPatrolRequireCommunication);
          this.patrolConfigAddEditForm.controls['maxHolidayCount'].patchValue(response.resources.maxHolidayCount);
          this.patrolConfigAddEditForm.controls['holidayPatrolDailyLimit'].patchValue(response.resources.holidayPatrolDailyLimit);
          this.patrolConfigAddEditForm.controls['isHolidayPatrolLimitedToPatrolWindow'].patchValue(response.resources.isHolidayPatrolLimitedToPatrolWindow);
          this.patrolConfigAddEditForm.controls['isHolidayPatrolRequireCommunication'].patchValue(response.resources.isHolidayPatrolRequireCommunication);
          this.patrolConfigAddEditForm.controls['maxHolidayPaidPatrolAllowed'].patchValue(response.resources.maxHolidayPaidPatrolAllowed);
          this.patrolConfigAddEditForm.controls['isHolidayPaidPatrolLimitedToPatrolWindow'].patchValue(response.resources.isHolidayPaidPatrolLimitedToPatrolWindow);
          this.patrolConfigAddEditForm.controls['isHolidayPaidPatrolRequireCommunication'].patchValue(response.resources.isHolidayPaidPatrolRequireCommunication);
          this.patrolConfigAddEditForm.controls['visibilityPatrolMaxDays'].patchValue(response.resources.visibilityPatrolMaxDays);
          this.patrolConfigAddEditForm.controls['dailyVisibilityPatrolLimit'].patchValue(response.resources.dailyVisibilityPatrolLimit);
          this.patrolConfigAddEditForm.controls['isVisibilityPatrolLimitedToPatrolWindow'].patchValue(response.resources.isVisibilityPatrolLimitedToPatrolWindow);
          this.patrolConfigAddEditForm.controls['isVisibilityPatrolRequireCommunication'].patchValue(response.resources.isVisibilityPatrolRequireCommunication);
          this.patrolConfigAddEditForm.controls['maxAllowedPaidPatrol'].patchValue(response.resources.maxAllowedPaidPatrol);
          this.patrolConfigAddEditForm.controls['isPaidPatrolLimitedToPatrolWindow'].patchValue(response.resources.isPaidPatrolLimitedToPatrolWindow);
          this.patrolConfigAddEditForm.controls['isPaidPatrolRequireCommunication'].patchValue(response.resources.isPaidPatrolRequireCommunication);
          this.patrolConfigAddEditForm.controls['isWelcomePatrolLimitedToPatrolWindow'].patchValue(response.resources.isWelcomePatrolLimitedToPatrolWindow);
          this.patrolConfigAddEditForm.controls['isWelcomePatrolRequireCommunication'].patchValue(response.resources.isWelcomePatrolRequireCommunication);

          this.patrolDetails.patrolConfigMainAreaDetails.forEach(element => {
            element.id = element.mainAreaId
          });

          this.patrolConfigAddEditForm.controls['patrolConfigMainAreaDetails'].patchValue(this.patrolDetails.patrolConfigMainAreaDetails);

          this.getMainAreaList(response.resources.divisionId);
          
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  ismainArea = false;
  onSubmit(): void {
    if (this.patrolConfigAddEditForm.value.patrolConfigMainAreaDetails && this.patrolConfigAddEditForm.value.patrolConfigMainAreaDetails.length > 0) {
      this.ismainArea = false;
    } else {
      this.ismainArea = true;
    }
    if (this.patrolConfigAddEditForm.invalid) {
      this.patrolConfigAddEditForm.markAllAsTouched();
      return;
    }
    let formValue;
    formValue = JSON.parse(JSON.stringify(this.patrolConfigAddEditForm.value));
    
    if (this.patrolConfigAddEditForm.value.generalPatrolWindowStart)
      formValue['generalPatrolWindowStart'] = this.momentService.convertTwelveToTwentyFourTime(this.patrolConfigAddEditForm.value.generalPatrolWindowStart);
    if (this.patrolConfigAddEditForm.value.generalPatrolWindowEnd)
      formValue['generalPatrolWindowEnd'] = this.momentService.convertTwelveToTwentyFourTime(this.patrolConfigAddEditForm.value.generalPatrolWindowEnd);
    if (this.patrolConfigAddEditForm.value.generalPatrolCommunicationWindowStart)
      formValue['generalPatrolCommunicationWindowStart'] = this.momentService.convertTwelveToTwentyFourTime(this.patrolConfigAddEditForm.value.generalPatrolCommunicationWindowStart);
    if (this.patrolConfigAddEditForm.value.generalPatrolCommunicationWindowEnd)
      formValue['generalPatrolCommunicationWindowEnd'] = this.momentService.convertTwelveToTwentyFourTime(this.patrolConfigAddEditForm.value.generalPatrolCommunicationWindowEnd);

      formValue.patrolConfigMainAreaDetails.forEach(element => { 
        element.mainAreaId = element.id
      });

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PATROL_CONFIG_POST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=29');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}
