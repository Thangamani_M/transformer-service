import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-patrol-config-view',
  templateUrl: './patrol-config-view.component.html',
})
export class PatrolConfigViewComponent implements OnInit {
  patrolId: any;
  patrolConfigDetails: any;
  primengTableConfigProperties: any;
  totaltab = 0;
  selectedTab = 0;
  PostIncidentPatrol: any;
  generalPatrolSettiongs: any;
  PatrolConfigDetailsViewDetails: any;
  autoPatrolViewDetails: any;
  holidayDurationViewDetails: any;
  holidayPatrolViewDetails: any;
  holidayPatrolPaidViewDetails: any;
  visibilityPatrolsDetails: any;
  paidPatrolsDetails: any;
  welcomePatrolsViewDetails: any;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private router: Router,
    private commonService: CrudService,
    private rxjsService: RxjsService) {
    this.patrolId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.primengTableConfigProperties = {
      tableCaption: 'View Patrol-Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Patrol-Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 29 } }, { displayName: '', }],      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData();
    this.getpatrolConfigDetailsById(this.patrolId);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getpatrolConfigDetailsById(patrolId: string) {
    this.commonService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PATROL_CONFIG_POST, patrolId, false, null).subscribe((response: IApplicationResponse) => {
      this.patrolConfigDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.patrolConfigDetails?.patrolConfigName;
      this.onShowValue(response.resources);
      this.generalPatrol(response.resources);
      this.postIncidentPatrol(response.resources);
      this.getAutoPatrolDetails(response.resources);
      this.getHolidayDurationDetails(response.resources);
      this.getHolidayPatrolDetails(response.resources);
      this.getHolidayPatrolPaidDetails(response.resources);
      this.getVissiabilityPatrolPaidDetails(response.resources);
      this.getpaidPatrolsDetails(response.resources);
      this.getWelcomePatrolsDetails(response.resources);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  onShowValue(response?: any) {
    this.PatrolConfigDetailsViewDetails = [
      { name: 'Patrol Config Name', value: response?.patrolConfigName },
      { name: 'Patrol Config Description', value: response?.patrolConfigDescription },
      { name: 'Status', value: response?.isStatus == true ? 'Active' : 'In Active', statusClass: response?.isStatus == true ? "status-label-green" : 'status-label-pink' },
    ]
  }

  generalPatrol(response?: any) {
    this.generalPatrolSettiongs = [
      {
        name: 'General Patrol Settings', columns: [
          { name: 'Patrol window Start', value: response?.generalPatrolWindowStart },
          { name: 'Patrol window End', value: response?.generalPatrolWindowEnd },
          { name: 'Communication window Start', value: response?.generalPatrolCommunicationWindowStart },
          { name: 'Communication window End', value: response?.generalPatrolCommunicationWindowEnd },
        ]
      }
    ]
  }

  postIncidentPatrol(response?: any) {
    this.PostIncidentPatrol = [
      {
        name: 'Post Incident Patrol', columns: [
          { name: 'Duration Patrol will take place', value: response?.postIncidentPatrolDays },
          { name: 'Daily Patrol Limit', value: response?.postIncidentDailyPatrolLimit },
          { name: 'Limited to patrol window', value: response?.isPostIncidentLimitedToPatrolWindow ? 'Yes' : 'No' },
          { name: 'Requires Communication', value: response?.isPostIncidentPatrolRequireCommunication? 'Yes' : 'No'  },
        ]
      }
    ]
  }

  getAutoPatrolDetails(response?: any) {
    this.autoPatrolViewDetails = [
      {
        name: 'Auto Patrol', columns: [
          { name: 'Days without Arrival', value: response?.withoutArrivalDays },
          { name: 'Daily Patrol Limit', value: response?.dailyLimit },
          { name: 'Limited to patrol window', value: response?.isAutoPatrolLimitedToPatrolWindow ? 'Yes' : 'No'  },
          { name: 'Requires Communication', value: response?.isAutoPatrolRequireCommunication ? 'Yes' : 'No'  },
        ]
      }
    ]
  }

  getHolidayDurationDetails(response?: any) {
    this.holidayDurationViewDetails = [
      {
        name: 'Holiday Duration', columns: [
          { name: 'Days without Arrival', value: response?.maxHolidayCount },
        ]
      }
    ]
  };

  getHolidayPatrolDetails(response?: any) {
    this.holidayPatrolViewDetails = [
      {
        name: 'Holiday Patrol', columns: [
          { name: 'Max Allowed Patrols', value: response?.holidayPatrolDailyLimit },
          { name: 'Limited to patrol window', value: response?.isHolidayPatrolLimitedToPatrolWindow ? 'Yes' : 'No' },
          { name: 'Requires Communication', value: response?.isHolidayPatrolRequireCommunication? 'Yes' : 'No'  },
        ]
      }
    ]
  };

  getHolidayPatrolPaidDetails(response?: any) {
    this.holidayPatrolViewDetails = [
      {
        name: 'Holiday Patrol(Paid)', columns: [
          { name: 'Max Allowed Patrols', value: response?.maxHolidayPaidPatrolAllowed },
          { name: 'Limited to patrol window', value: response?.isHolidayPaidPatrolLimitedToPatrolWindow ? 'Yes' : 'No' ,},
          { name: 'Requires Communication', value: response?.isHolidayPaidPatrolRequireCommunication ? 'Yes' : 'No' },
        ]
      }
    ]
  };

  getVissiabilityPatrolPaidDetails(response?: any) {
    this.visibilityPatrolsDetails = [
      {
        name: 'Visibility Patrols', columns: [
          { name: 'Duration Patrols will take place', value: response?.dailyVisibilityPatrolLimit },
          { name: 'Daily Patrol Limit', value: response?.visibilityPatrolMaxDays },
          { name: 'Limited to patrol window', value: response?.isVisibilityPatrolLimitedToPatrolWindow? 'Yes' : 'No'  },
          { name: 'Requires Communication', value: response?.isVisibilityPatrolRequireCommunication ? 'Yes' : 'No' },
        ]
      }
    ]
  };

  getpaidPatrolsDetails(response?: any) {
    this.paidPatrolsDetails = [
      {
        name: 'Paid Patrols', columns: [
          { name: 'Max Allowed Patrols', value: response?.maxAllowedPaidPatrol },
          { name: 'Limited to patrol window', value: response?.isPaidPatrolLimitedToPatrolWindow? 'Yes' : 'No'  },
          { name: 'Requires Communication', value: response?.isPaidPatrolRequireCommunication ? 'Yes' : 'No' },
        ]
      }
    ]
  };

  getWelcomePatrolsDetails(response?: any) {
    this.welcomePatrolsViewDetails = [
      {
        name: 'Welcome Patrols', columns: [
          { name: 'Limited to patrol window', value: response?.isWelcomePatrolLimitedToPatrolWindow ? 'Yes' : 'No' },
          { name: 'Requires Communication', value: response?.isWelcomePatrolRequireCommunication ? 'Yes' : 'No' },
        ]
      }
    ]
  };

  edit() {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/event-management/event-configuration/patrol-config/add-edit"], { queryParams: { id: this.patrolId, totalTabs: this.totaltab, selectedTab: this.selectedTab } });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit();
        break;
    }
  }
}
