import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { PatrolConfigAddEditComponent } from './patrol-config-add-edit.component';
import { PatrolConfigRoutingModule } from './patrol-config-routing.module';
import { PatrolConfigViewComponent } from './patrol-config-view/patrol-config-view.component';

@NgModule({
  declarations: [PatrolConfigAddEditComponent, PatrolConfigViewComponent],
  imports: [
    CommonModule,
    PatrolConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule
  ]
})
export class PatrolConfigModule { }
