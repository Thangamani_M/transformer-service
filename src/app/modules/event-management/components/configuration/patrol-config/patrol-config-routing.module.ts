import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatrolConfigAddEditComponent } from './patrol-config-add-edit.component';
import { PatrolConfigViewComponent } from './patrol-config-view/patrol-config-view.component';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: PatrolConfigAddEditComponent, data: { title: 'Patrol Config' } },
  { path: 'view', component: PatrolConfigViewComponent, data: { title: 'Patrol Config' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PatrolConfigRoutingModule { }
