import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-incidents-view',
  templateUrl: './incidents-view.component.html',
})
export class IncidentsViewComponent implements OnInit {

  incidentId: any;
  incidentDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any
  incidentDetailsViewDetails: any
  incidentTypesList: any;
  eventTypeList: any;
  totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  };

  constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.incidentId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View Incident',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Incident', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 5 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.getEventType();
    this.combineLatestNgrxStoreData();
    if (this.incidentId) {
      this.getincidentDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.incidentDetails = response.resources;
        this.onShowValue(response.resources);
        this.incidentTypesList = response.resources.incidentTypesList
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + response.resources.incidentName;
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.RO_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getEventType() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_EVENT_TYPES_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onShowValue(response?: any) {
    this.incidentDetailsViewDetails = [
      { name: 'Incident Name', value: response?.incidentName },
      { name: 'Description', value: response?.description },
      { name: 'Division', value: response?.incidentDivisions },
      { name: 'Status', value: response.isActive == true ? 'Active' : 'In-Active', statusClass: response.isActive == true ? "status-label-green" : 'status-label-red' },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Modified By', value: response?.modifiedUserName },

    ]
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['event-management/ro-configuration/incidents/add-edit'], { queryParams: { id: this.incidentId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
  }

  getincidentDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.INCIDENT,
      this.incidentId
    );
  }

}