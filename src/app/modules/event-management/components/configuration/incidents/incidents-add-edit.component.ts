import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { IncidentModel, IncidentTypesListModel } from '@modules/event-management/models/configurations/incident.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-incidents-add-edit',
  templateUrl: './incidents-add-edit.component.html',
})
export class IncidentsAddEditComponent implements OnInit {
  incidentId: any
  divisionDropDown: any = [];
  incidentForm: FormGroup;
  incidentTypesList: FormArray;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  incidentDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  public colorCode: string = '#278ce2';
  @ViewChildren('input') rows: QueryList<any>;
  eventTypeList: any = [];
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.incidentId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.title =this.incidentId ? 'Update Incident':'Add Incident';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Incident', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 5} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.incidentId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/incidents/view' , queryParams: {id: this.incidentId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createincidentForm();
    this.getDivisionDropDownById();
    this.getEventType();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.incidentId) {

      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let cmcSmsGroup = new IncidentModel(response.resources);

        this.incidentDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.incidentDetails?.incidentName ? this.title +' - '+ this.incidentDetails?.incidentName : this.title + ' --', relativeRouterUrl: '' });
        this.incidentForm.patchValue(cmcSmsGroup);

        var incidentDivisionsList = [];
        response.resources.incidentDivisionsList.forEach(element => {
          incidentDivisionsList.push(element.divisionId);
        });
        this.incidentForm.get('incidentDivisionsList').setValue(incidentDivisionsList)
        this.incidentTypesList = this.getincidentTypesListArray;
        if (response.resources.incidentTypesList.length > 0) {
          response.resources.incidentTypesList.forEach((incidentTypesListModel) => {
            incidentTypesListModel.incidentTypeId = incidentTypesListModel.incidentTypeId;
            this.incidentTypesList.push(this.createincidentTypesListModel(incidentTypesListModel));
          });
        } else {
          this.incidentTypesList.push(this.createincidentTypesListModel());
        }
      })
    } else {
      this.incidentTypesList = this.getincidentTypesListArray;
      this.incidentTypesList.push(this.createincidentTypesListModel());
    }
  }

  public onEventLog(event: string, data: any): void {
  }

  public onChangeColor(color: string, i): any {
    if (color) {
      this.incidentForm.get('incidentTypesList')['controls'][i].controls['colorCode'].setValue(color);
      this.rxjsService.setFormChangeDetectionProperty(true);
    } else {
      this.incidentForm.get('incidentTypesList')['controls'][i].controls['colorCode'].setValue(null);
      this.rxjsService.setFormChangeDetectionProperty(true);
    }
    return color;
  }

  createincidentForm(): void {
    let incidentModel = new IncidentModel();
    this.incidentForm = this.formBuilder.group({
      incidentTypesList: this.formBuilder.array([])
    });
    Object.keys(incidentModel).forEach((key) => {
      this.incidentForm.addControl(key, new FormControl(incidentModel[key]));
    });
    this.incidentForm = setRequiredValidator(this.incidentForm, ["incidentName", "description", "incidentDivisionsList"]);
    this.incidentForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.incidentForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getincidentTypesListArray(): FormArray {
    if (!this.incidentForm) return;
    return this.incidentForm.get("incidentTypesList") as FormArray;
  }


  //Create FormArray controls
  createincidentTypesListModel(incidentTypesListModel?: IncidentTypesListModel): FormGroup {
    let incidentTypesListFormControlModel = new IncidentTypesListModel(incidentTypesListModel);
    let formControls = {};
    Object.keys(incidentTypesListFormControlModel).forEach((key) => {
      if (key === 'eventTypeId' || key === 'incidentTypeId' || key === 'colorCode') {
        formControls[key] = [{ value: incidentTypesListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.incidentId) {
        formControls[key] = [{ value: incidentTypesListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: incidentTypesListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.INCIDENT,
      this.incidentId
    );
  }

  getEventType() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_EVENT_TYPES_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeList = response.resources;

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  OnChange(i?) {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Incident Type already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getincidentTypesListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.eventTypeId)) {
        duplicate.push(k.value.eventTypeId);
      }
      filterKey.push(k.value.eventTypeId);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }



  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (!this.OnChange()) {
      return;
    }

    if (this.getincidentTypesListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };

    this.incidentTypesList = this.getincidentTypesListArray;
    let incidentTypesListModel = new IncidentTypesListModel();
    this.incidentTypesList.insert(0, this.createincidentTypesListModel(incidentTypesListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    if (this.getincidentTypesListArray.controls[i].value.eventTypeId == '') {
      this.getincidentTypesListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getincidentTypesListArray.controls[i].value.incidentTypeId && this.getincidentTypesListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.INCIDENT_TYPES,
          this.getincidentTypesListArray.controls[i].value.incidentTypeId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getincidentTypesListArray.removeAt(i);
            }
            if (this.getincidentTypesListArray.length === 0) {
              this.addCmcSmsGroupEmployee();
            };
          });
      }
      else {
        this.getincidentTypesListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.incidentForm.controls.incidentDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.incidentForm.controls.incidentDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.incidentForm.controls.incidentDivisionsList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (!this.OnChange()) {
      return;
    }

    if (this.incidentForm.invalid) {
      return;
    }



    let formValue = this.incidentForm.value;
    if (formValue.incidentDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.incidentDivisionsList = formValue.incidentDivisionsList.filter(item => item != '')
      formValue.incidentDivisionsList = formValue.incidentDivisionsList.map(divisionId => ({ divisionId }))
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.incidentId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.INCIDENT, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.INCIDENT, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=5');
      }
    })
  }

  onCRUDRequested(type){}

}
