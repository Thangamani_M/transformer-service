import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IncidentsAddEditComponent } from './incidents-add-edit.component';
import { IncidentsViewComponent } from './incidents-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: IncidentsAddEditComponent, data: { title: 'Incidents Add/Edit' } },
  { path: 'view', component: IncidentsViewComponent, data: { title: 'Incidents View' },canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class IncidentsRoutingModule { }
