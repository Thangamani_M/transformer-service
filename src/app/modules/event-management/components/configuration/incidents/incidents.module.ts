import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { ColorPickerModule } from 'primeng/colorpicker';
import { IncidentsAddEditComponent } from './incidents-add-edit.component';
import { IncidentsRoutingModule } from './incidents-routing.module';
import { IncidentsViewComponent } from './incidents-view.component';
@NgModule({
  declarations: [IncidentsAddEditComponent, IncidentsViewComponent],
  imports: [
    CommonModule,
    IncidentsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    ColorPickerModule
  ]
})
export class IncidentsModule { }
