import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-event-configuration-list',
  templateUrl: './event-configuration-list.component.html',
  styleUrls: ['./event-configuration-list.component.scss']
})
export class EventConfigurationListComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  pageSize: number = 10;
  observableResponse;
  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  dataList: any
  loading: boolean;
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any;
  spickActive: boolean = false;
  dropDown: boolean = false;
  tabsClick: boolean = true;
  row: any = {}
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  divisionDropDown: any;
  status1: any;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
    this.primengTableConfigProperties = {
      tableCaption: "Event Configurations ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configurations', relativeRouterUrl: '' }, { displayName: 'Stack Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stack Configuration',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stackName', header: 'Stack Name', width: '150px' },
            { field: 'divisionNames', header: 'Division', width: '150px' },
            { field: 'namedStackConfigName', header: 'Named Stack', width: '150px' },
            { field: 'customerProfileTypeNames', header: 'Customer Profile Type', width: '200px' },
            { field: 'alarmGroupNames', header: 'Alarm Group', width: '150px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.STACK_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Area Configuration',
            dataKey: 'stackAreaConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'areaName', header: 'Area Name', width: '200px' },
            { field: 'stackAreaConfigDivisions', header: 'Division', width: '200px' },
            { field: 'stackAreaConfigMainAreas', header: 'Main Area', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.STACK_AREAS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },

          {
            caption: 'Named Stack Configuration',
            dataKey: 'namedStackConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'namedStackConfigName', header: 'Named Stack Name', width: '200px' },
            { field: 'description', header: 'Description', width: '300px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.NAMED_STACK_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Named Stack Mapping',
            dataKey: 'namedStackId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'namedStackConfigName', header: 'Named Stack Mapping Name', width: '200px' },
            { field: 'description', header: 'Description', width: '300px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.NAMED_STACK,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Alarm Group',
            dataKey: 'alarmGroupId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'alarmGroupName', header: 'Alarm Group Name', width: '200px' },
            { field: 'description', header: 'Description', width: '300px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.ALARM_GROUP,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Vehicle Registration',
            dataKey: 'vehicleRegistrationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'vehicleRegistrationNumber', header: 'Vehicle Registration Number', width: '250px' },
            { field: 'assetNumber', header: 'Asset Number', width: '200px' },
            { field: 'vehicleMake', header: 'Vehicle Make', width: '200px' },
            { field: 'vehicleModel', header: 'Vehicle Model', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.VEHICLE_REGISTRATION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'PTT Registration',
            dataKey: 'pttRegistrationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'pttid', header: 'PTT ID', width: '200px' },
            { field: 'imeiNumber', header: 'IMEI Number', width: '200px' },
            { field: 'deviceName', header: 'Device', width: '200px' },
            { field: 'contractDate', header: 'Contract Date', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.PTT_REGISTRATION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'PDA Registration',
            dataKey: 'pdaRegistrationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'pdaid', header: 'PDA ID', width: '200px' },
            { field: 'imeiNumber', header: 'IMEI Number', width: '200px' },
            { field: 'simCardNumber', header: 'Sim Card Number', width: '200px' },
            { field: 'contractDate', header: 'Contract Date', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.PDA_REGISTRATION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Safe Entry Limit',
            dataKey: 'safeEntryLimitId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'safeEntryLimitDivisions', header: 'Division', width: '200px' },
            { field: 'safeEntryLimitName', header: 'Description', width: '200px' },
            { field: 'maxCount', header: 'Max Count', width: '100px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SAFE_ENTRY_LIMIT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Event Type',
            dataKey: 'eventTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'incidentName', header: 'Incident', width: '200px' },
            { field: 'isEnabledFlag', header: 'Enabled Flag', width: '100px' },
            { field: 'description', header: 'Description', width: '300px' },
            { field: 'isHoldForFinish', header: 'Hold For Finish', width: '100px' },
            { field: 'isRequiresFeedback', header: 'Requires Feedback', width: '100px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.EVENT_TYPE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Signal Automation',
            dataKey: 'signalAutomationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'signalAutomationName', header: 'Signal Automation', width: '200px' },
            { field: 'signalAutomationDivisions', header: 'Division', width: '200px' },
            { field: 'signalAutomationAlarmTypes', header: 'Signal Type', width: '100px' },
            { field: 'waitForRestore', header: 'Wait For Restore', width: '100px' },
            { field: 'waitForDeliveryConfirmation', header: 'Wait For Delivery Confirmation', width: '100px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_AUTOMATION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Delay Dispatch',
            dataKey: 'delayDispatchId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'delayDispatchName', header: 'Delay Dispatch Name', width: '200px' },
            { field: 'delayDispatchDescription', header: 'Description', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'mainArea', header: 'Main Area', width: '200px' },
            { field: 'delayDispatchTiming', header: 'Delay Time', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DELAY_DISPATCH,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Delay To Duration',
            dataKey: 'delayDurationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'maxDuration', header: 'Max Duration (Days)', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DELAY_DURATION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Boundary Limit',
            dataKey: 'boundaryLimitId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'maxDistance', header: 'Max Distance (Mts)', width: '200px' },
            { field: 'mainArea', header: 'Boundary Limit Main Area', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.BOUNDARY_LIMIT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'CMC SMS Group',
            dataKey: 'cmcsmsGroupId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'groupName', header: 'Group Name', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'cmcsmsEmployees', header: 'CMC SMS Employee', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CMS_SMS_GROUP,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Cluster Configuration',
            dataKey: 'clusterConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'clusterName', header: 'Cluster', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'mainArea', header: 'Main Area', width: '200px' },
            { field: 'clusterConfigSubAreas', header: 'Cluster Config Sub Area', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CLUSTER_CONFIGURATION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'BOLO Distance',
            dataKey: 'boloDistanceId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'boloDistanceName', header: 'Bolo Distance Name', width: '200px' },
            { field: 'boloDistanceDivisions', header: 'Division', width: '200px' },
            { field: 'branchName', header: 'Branch', width: '200px' },
            { field: 'distance', header: 'Distance (Mts)', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.BOLO_DISTANCE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Distribution Group',
            dataKey: 'distributionGroupId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'distributionGroupName', header: 'Group Name', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'distributionGroupMainAreas', header: 'Main Area', width: '200px' },
            { field: 'distributionGroupEmployees', header: 'Employee', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DISTRIBUTION_GROUP,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Community Patrol',
            dataKey: 'communityPatrolId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'communityName', header: 'Community Patrol Name', width: '200px' },
            { field: 'communityPatrolDivisions', header: 'Division', width: '100px' },
            { field: 'patrolTime', header: 'Patrol Time', width: '100px' },
            { field: 'restTime', header: 'Rest Time', width: '100px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.COMMUNITY_PATROL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Suspend Delay',
            dataKey: 'maxSuspendDelayId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'maxSuspendDelayName', header: 'Max Suspend Delay Name', width: '200px' },
            { field: 'maxSuspendDelayDivisions', header: 'Division', width: '200px' },
            { field: 'timeDelay', header: 'Time Delay', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SUSPEND_DELAY,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Third Party Details',
            dataKey: 'thirdPartyId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'companyName', header: 'Company', width: '200px' },
            { field: 'monitoringCenterNumber', header: 'Monitoring Center Number', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'contactPersonName', header: 'Contact Person', width: '200px' },
            { field: 'officeContactNumber', header: 'Office Contact Number', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.THIRD_PARTY_DETAILS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Shift Change Way Points',
            dataKey: 'shiftChangeWayPointId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'mainArea', header: 'Main Area', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'shiftChangeWayPointName', header: 'Shift Change Way Point', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SHIFT_CHANGE_WAY_POINT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'VAS Config',
            dataKey: 'vasConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'groupName', header: 'Group Name', width: '200px' },
            { field: 'vasConfigDivisions', header: 'Division', width: '200px' },
            { field: 'feedbackDelay', header: 'Feedback Delay', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.VAS_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Stats Config',
            dataKey: 'statsConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'groupName', header: 'Group Name', width: '200px' },
            { field: 'statsConfigAlarmTypes', header: 'Alarm Type', width: '200px' },
            { field: 'statsConfigEventTypes', header: 'Event Type', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.STATS_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Default Message Config',
            dataKey: 'defaultmessageid',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'defaultMessage', header: 'Default Message', width: '200px' },
            { field: 'defaultMessageTypeName', header: 'Default Message Type', width: '200px' },
            { field: 'modifiedUserName', header: 'Modified By', width: '200px' },
            { field: 'modifiedDate', header: 'Modified On', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DEFULAT_MESSAGE_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },

          {
            caption: 'Client Request Type',
            dataKey: 'clientRequestTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'clientRequestTypeName', header: 'Client Request Type', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CLIENT_REQUEST_TYPE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Decoder Account Config',
            dataKey: 'decoderAccountConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'decoderName', header: 'Decoder', width: '200px' },
            { field: 'setNames', header: 'SetName', width: '200px' },
            { field: 'commsTypeName', header: 'Comms Type', width: '200px' },
            { field: 'accountCodeGenerationMode', header: 'Account Code Generation', width: '200px' },
            { field: 'length', header: 'Length', width: '200px' },
            { field: 'isHexadecimal', header: 'Hexadecimal', width: '200px' },
            { field: 'isPartitionConsidered', header: 'Partition Considered', width: '200px' },
            { field: 'isBOMMandatory', header: 'BOM Mandatory', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DECODER_ACCOUNT_CONFIGURATIONS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Cell Panic Config',
            dataKey: 'cellPanicConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'decoderName', header: 'Decoder Name', width: '200px' },
            { field: 'setName', header: 'Set Name', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CELL_PANIC_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Fail To Test',
            dataKey: 'failToTestId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'alarmType', header: 'Alarm', width: '200px' },
            { field: 'startTime', header: 'Start Time', width: '200px' },
            { field: 'endTime', header: 'End Time', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.FAIL_TO_TEST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Patrol Config',
            dataKey: 'patrolConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'patrolConfigName', header: 'Config Name', width: '200px' },
            { field: 'patrolConfigDescription', header: 'Description', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'mainAreaNames', header: 'Main Area', width: '200px' },
            { field: 'isStatus', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.PATROL_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'CMC Service Level Config',
            dataKey: 'cmcServiceLevelConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'phonebackServiceLevel', header: 'Phone Back Service Level', width: '200px' },
            { field: 'dispatchTimeServiceLevel', header: 'Dispatch Time Service Level', width: '200px' },
            { field: 'finishTimeServiceLevel', header: 'Finish Time Service Level', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CMC_SERVICE_LEVEL_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'ERB Exclusion Reason Config',
            dataKey: 'erbExclusionReasonConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'reasonName', header: 'ERB Exclusion Reason Config', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REASON_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },

          {
            caption: 'Visibility Logging Config',
            dataKey: 'visibilityLoggingConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'division', header: 'Division', width: '100px' },
              { field: 'description', header: 'Description', width: '200px' },
              { field: 'visibilityLoggingconfigId', header: 'Config ID', width: '100px' },
              { field: 'intervalInDays', header: 'Interval In Days', width: '150px' },
              { field: 'numberOfPasses', header: 'Number Of Passes', width: '150px' },
              { field: 'distanceToCapture', header: 'Distance', width: '100px' },
              { field: 'createdUserName', header: 'Created By', width: '100px' },
              { field: 'createdDate', header: 'Created On', width: '150px' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.VISIBILITY_LOGGING_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Signal Influx Classification Type',
            dataKey: 'signalInfluxClassificationTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'signalInfluxClassificationTypeName', header: 'Classification', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '100px' },
            { field: 'createdDate', header: 'Created On', width: '100px' },
            { field: 'isActive', header: 'Status', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.EVENT_CLASSIFICATION_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Operator Instructions',
            dataKey: 'instructorInstructionId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'instructorInstructionTypeName', header: 'Classification', width: '200px' },
            { field: 'comments', header: 'Comments', width: '200px' },
            { field: 'instructorInstructionTriggerTypes', header: 'Trigger Types', width: '200px' },
            { field: 'urlLink', header: 'URL', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '100px' },
            { field: 'createdDate', header: 'Created On', width: '100px' },
            { field: 'isActive', header: 'Status', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.EVENT_INSTRUCTOR_INSTRUCTIONS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Open Closing Setup',
            dataKey: 'openCloseSetupId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'openCloseSetupName', header: 'Open Closing Setup', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'openCloseSetupDivisions', header: 'Division', width: '300px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'modifiedUserName', header: 'Modified By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.OPEN_CLOSING_SETUP,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Signal Pattern Config',
            dataKey: 'SignalPatternId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'generatedAlarmTypeName', header: 'Generated Alarm Type Name', width: '170px' },
              { field: 'signalDurationMinutes', header: 'Signal Duration (Minutes)', width: '150px' },
              { field: 'descriptions', header: 'Descriptions', width: '100px' },
              { field: 'isTechnicianEscalationRequired', header: 'Technician Escalation Required', width: '150px' },
              { field: 'signalPatternAlarmTypes', header: 'AlarmTypes', width: '150px' },
              { field: 'createdUserName', header: 'Created By', width: '100px' },
              { field: 'createdDate', header: 'Created On', width: '200px' },
              { field: 'isActive', header: 'Status', width: '100px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_PATTERN,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Spike Identification Config',
            dataKey: 'spikeIdentificationConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'windowPeriods', header: 'Window Periods', width: '200px' },
            { field: 'weekReview', header: 'Week Review', width: '200px' },
            { field: 'deviationValue', header: 'Deviation Value', width: '300px' },
            { field: 'modifiedUserName', header: 'Modified By' },
            { field: 'modifiedDate', header: 'Modified On' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SPIKE_IDETIFICATION_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Unwanted Carrier Config',
            dataKey: 'unknownSignalConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'alarm', header: 'Alarms' },
              { field: 'frequency', header: 'Frequency' },
              { field: 'duration', header: 'Duration' },
              { field: 'minimumSignalCountinFrequency', header: 'Min Frequency' },
              { field: 'createdUserName', header: 'Created By' },
              { field: 'createdDate', header: 'Create On' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.UNDEFINED_SIGNALS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'PTT Stack Area Config',
            dataKey: 'pttStackAreaConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'areaName', header: 'Area Name' },
            { field: 'channelName', header: 'Channel Name' },
            { field: 'username', header: 'PTT User ID' },
            { field: 'description', header: 'Description' },
            { field: 'createdUserName', header: 'Created By' },
            { field: 'createdDate', header: 'Create On' },
            { field: 'isActive', header: 'Status' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.PTT_STACK_AREA_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
        ]

      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]

    this.status1 = [
      { label: 'Yes', value: true },
      { label: 'No', value: false },
    ]
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        let index = this.primengTableConfigProperties.tableComponentConfigs.tabsList.findIndex(x => x.disabled == false)
        if (index != 0) {
          this.selectedTabIndex = index
          this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: this.selectedTabIndex } })
        }
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      if (data.isSuccess && data.resources && data.statusCode == 200) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);

      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }

        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              }
              else if (key == 'mainAreaName') {
                key = 'mainAreaIds';
                otherParams[key] = this.row['searchColumns']['mainAreaName'];
              }
              else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/event-configuration/stack-config/add-edit");
            break;
          case 1:
            this.router.navigateByUrl("event-management/event-configuration/stack-areas/add-edit");
            break;
          case 2:
            this.router.navigateByUrl("event-management/event-configuration/namedStack-config/add-edit");
            break;
          case 3:
            this.router.navigateByUrl("event-management/event-configuration/named-stack-mapping/add-edit");
            break;
          case 4:
            this.router.navigateByUrl("event-management/event-configuration/alarm-group/add-edit");
            break;
          case 5:
            this.router.navigateByUrl("event-management/event-configuration/vehicle-registration/add-edit");
            break;
          case 6:
            this.router.navigateByUrl("event-management/event-configuration/ptt-registration/add-edit");
            break;
          case 7:
            this.router.navigateByUrl("event-management/event-configuration/pda-registration/add-edit");
            break;
          case 8:
            this.router.navigateByUrl("event-management/event-configuration/safe-entry-limit/add-edit");
            break;
          case 9:
            this.router.navigateByUrl("event-management/event-configuration/event-type/add-edit");
            break;
          case 10:
            this.router.navigateByUrl("event-management/event-configuration/signal-automation/add-edit");
            break;
          case 11:
            this.router.navigateByUrl("event-management/event-configuration/delay-dispatch/add-edit");
            break;
          case 12:
            this.router.navigateByUrl("event-management/event-configuration/delay-to-duration/add-edit");
            break;
          case 13:
            this.router.navigateByUrl("event-management/event-configuration/boundary-limit/add-edit");
            break;
          case 14:
            this.router.navigateByUrl("event-management/event-configuration/cmc-sms-group/add-edit");
            break;
          case 15:
            this.router.navigateByUrl("event-management/event-configuration/cluster-config/add-edit");
            break;
          case 16:
            this.router.navigateByUrl("event-management/event-configuration/bolo-distance/add-edit");
            break;
          case 17:
            this.router.navigateByUrl("event-management/event-configuration/distribution-group/add-edit");
            break;
          case 18:
            this.router.navigateByUrl("event-management/event-configuration/community-patrol/add-edit");
            break;
          case 19:
            this.router.navigateByUrl("event-management/event-configuration/suspend-delay/add-edit");
            break;
          case 20:
            this.router.navigateByUrl("event-management/event-configuration/third-party-details/add-edit");
            break;
          case 21:
            this.router.navigateByUrl("event-management/event-configuration/shift-change-way-point/add-edit");
            break;
          case 22:
            this.router.navigateByUrl("event-management/event-configuration/vas-config/add-edit");
            break;
          case 23:
            this.router.navigateByUrl("event-management/event-configuration/stats-config/add-edit");
            break;
          case 24:
            this.router.navigateByUrl("event-management/event-configuration/default-message-config/add-edit");
            break;
          case 25:
            this.router.navigateByUrl("event-management/event-configuration/client-request-type/add-edit");
            break;
          case 26:
            this.router.navigateByUrl("event-management/event-configuration/decoder-account-config/add-edit");
            break;
          case 27:
            this.router.navigateByUrl("event-management/event-configuration/cell-panic-config/add-edit");
            break;
          case 28:
            this.router.navigateByUrl("event-management/event-configuration/fail-to-test/add-edit");
            break;
          case 29:
            this.router.navigateByUrl("event-management/event-configuration/patrol-config/add-edit");
            break;
          case 30:
            this.router.navigateByUrl("event-management/event-configuration/cmc-service-level-config/add-edit");
            break;
          case 31:
            this.router.navigateByUrl("event-management/event-configuration/erb-exclusion-reason-config/add-edit");
            break;
          case 32:
            this.router.navigateByUrl('event-management/event-configuration/visibility-logging-config/add-edit');
            break;
          case 33:
            this.router.navigateByUrl('event-management/event-configuration/signal-influx-classification/add-edit');
            break;
          case 34:
            this.router.navigateByUrl('event-management/event-configuration/operator-interactions/add-edit');
            break;
          case 35:
            this.router.navigateByUrl("event-management/event-configuration/open-close-setup/add-edit");
            break;
          case 36:
            this.router.navigateByUrl("event-management/event-configuration/signal-pattern/view")
            break;
          case 37:
            this.router.navigateByUrl("event-management/event-configuration/spike-identificaton-config/add-edit");
            break;
          case 38:
            this.router.navigateByUrl("event-management/event-configuration/unknown-signal-config/add-edit");
            break;
          case 39:
            this.router.navigateByUrl("event-management/event-configuration/ptt-stack-area-config/add-edit");
            break;
        }

        break;
      case CrudType.VIEW:
        let totalTabs = this.primengTableConfigProperties.tableComponentConfigs.tabsList.length;
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/event-configuration/stack-config/view"], { queryParams: { id: editableObject['stackConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 1:
            this.router.navigate(["event-management/event-configuration/stack-areas/view"], { queryParams: { id: editableObject['stackAreaConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 2:
            this.router.navigate(["event-management/event-configuration/namedStack-config/view"], { queryParams: { id: editableObject['namedStackConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 3:
            this.router.navigate(["event-management/event-configuration/named-stack-mapping/view"], { queryParams: { id: editableObject['namedStackId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 4:
            this.router.navigate(["event-management/event-configuration/alarm-group/view"], { queryParams: { id: editableObject['alarmGroupId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 5:
            this.router.navigate(["event-management/event-configuration/vehicle-registration/view"], { queryParams: { id: editableObject['vehicleRegistrationId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 6:
            this.router.navigate(["event-management/event-configuration/ptt-registration/view"], { queryParams: { id: editableObject['pttRegistrationId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 7:
            this.router.navigate(["event-management/event-configuration/pda-registration/view"], { queryParams: { id: editableObject['pdaRegistrationId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 8:
            this.router.navigate(["event-management/event-configuration/safe-entry-limit/view"], { queryParams: { id: editableObject['safeEntryLimitId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 9:
            this.router.navigate(["event-management/event-configuration/event-type/view"], { queryParams: { id: editableObject['eventTypeId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 10:
            this.router.navigate(["event-management/event-configuration/signal-automation/view"], { queryParams: { id: editableObject['signalAutomationId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 11:
            this.router.navigate(["event-management/event-configuration/delay-dispatch/view"], { queryParams: { id: editableObject['delayDispatchId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 12:
            this.router.navigate(["event-management/event-configuration/delay-to-duration/view"], { queryParams: { id: editableObject['delayDurationId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 13:
            this.router.navigate(["event-management/event-configuration/boundary-limit/view"], { queryParams: { id: editableObject['boundaryLimitId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 14:
            this.router.navigate(["event-management/event-configuration/cmc-sms-group/view"], { queryParams: { id: editableObject['cmcsmsGroupId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 15:
            this.router.navigate(["event-management/event-configuration/cluster-config/view"], { queryParams: { id: editableObject['clusterConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 16:
            this.router.navigate(["event-management/event-configuration/bolo-distance/view"], { queryParams: { id: editableObject['boloDistanceId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 17:
            this.router.navigate(["event-management/event-configuration/distribution-group/view"], { queryParams: { id: editableObject['distributionGroupId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 18:
            this.router.navigate(["event-management/event-configuration/community-patrol/view"], { queryParams: { id: editableObject['communityPatrolId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 19:
            this.router.navigate(["event-management/event-configuration/suspend-delay/view"], { queryParams: { id: editableObject['maxSuspendDelayId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 20:
            this.router.navigate(["event-management/event-configuration/third-party-details/view"], { queryParams: { id: editableObject['thirdPartyId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 21:
            this.router.navigate(["event-management/event-configuration/shift-change-way-point/view"], { queryParams: { id: editableObject['shiftChangeWayPointId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 22:
            this.router.navigate(["event-management/event-configuration/vas-config/view"], { queryParams: { id: editableObject['vasConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 23:
            this.router.navigate(["event-management/event-configuration/stats-config/view"], { queryParams: { id: editableObject['statsConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 24:
            this.router.navigate(["event-management/event-configuration/default-message-config/view"], { queryParams: { id: editableObject['defaultMessageId'] , totalTabs: totalTabs, selectedTab: this.selectedTabIndex} });
            break;
          case 25:
            this.router.navigate(["event-management/event-configuration/client-request-type/view"], { queryParams: { id: editableObject['clientRequestTypeId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 26:
            this.router.navigate(["event-management/event-configuration/decoder-account-config/view"], { queryParams: { id: editableObject['decoderAccountConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 27:
            this.router.navigate(["event-management/event-configuration/cell-panic-config/view"], { queryParams: { id: editableObject['cellPanicConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 28:
            this.router.navigate(["event-management/event-configuration/fail-to-test/view"], { queryParams: { id: editableObject['failToTestId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 29:
            this.router.navigate(["event-management/event-configuration/patrol-config/view"], { queryParams: { id: editableObject['patrolConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 30:
            this.router.navigate(["event-management/event-configuration/cmc-service-level-config/view"], { queryParams: { id: editableObject['cmcServiceLevelConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 31:
            this.router.navigate(["event-management/event-configuration/erb-exclusion-reason-config/view"], { queryParams: { id: editableObject['erbExclusionReasonConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } });
            break;
          case 32:
            this.router.navigate(['event-management/event-configuration/visibility-logging-config/view'], { queryParams: { id: editableObject['visibilityLoggingconfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } })
            break;
          case 33:
            this.router.navigate(['event-management/event-configuration/signal-influx-classification/view'], { queryParams: { id: editableObject['signalInfluxClassificationTypeId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } })
            break;
          case 34:
            this.router.navigate(['event-management/event-configuration/operator-interactions/view'], { queryParams: { id: editableObject['instructorInstructionId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } })
            break;
          case 35:
            this.router.navigate(['event-management/event-configuration/open-close-setup/view'], { queryParams: { id: editableObject['openCloseSetupId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } })
            break;
          case 36:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['event-management/event-configuration/signal-pattern/view'], { queryParams: { id: editableObject['signalPatternId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } })
            break;
          case 37:
            this.router.navigate(['event-management/event-configuration/spike-identificaton-config/view'], { queryParams: { spikeIdentificationConfigId: editableObject['spikeIdentificationConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } })
            break;
          case 38:
            this.router.navigate(['event-management/event-configuration/unknown-signal-config/view'], { queryParams: { id: editableObject['unknownSignalConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } })
            break;
          case 39:
            this.router.navigate(['event-management/event-configuration/ptt-stack-area-config/view'], { queryParams: { id: editableObject['pttStackAreaConfigId'], totalTabs: totalTabs, selectedTab: this.selectedTabIndex } })
            break;


        }
    }
  }

  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
          this.divisionDropDown.forEach(element => {
            element.label = element.displayName;
            element.value = element.id;

          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onTabChange(event) {
      this.tables.forEach(table => { //to set default row count list
        table.rows = 10
      })

      if (event.index == 32) {
        this.getDivisionDropdown()
      }

      this.row = {}
      this.dropDown = false;
      this.tabsClick = true;
      this.columnFilterForm = this._fb.group({})
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
      this.columnFilterRequest();
      this.dataList = [];
      this.totalRecords = null;
      this.selectedTabIndex = event.index
      this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: this.selectedTabIndex } })
      this.getRequiredListData();
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }

  eventConfigurationTabs(i) {
    let indexOf = this.primengTableConfigProperties.tableComponentConfigs.tabsList.findIndex(x => x.caption == i.value.caption)

    if (indexOf > -1) {
      this.selectedTabIndex = indexOf
      this.row = {}
      this.columnFilterForm = this._fb.group({})
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
      this.columnFilterRequest();
      this.dataList = [];
      this.totalRecords = null;
      this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    }

  }

  tabsChange() {
    this.dropDown = true;
    this.tabsClick = false;
  }
  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
