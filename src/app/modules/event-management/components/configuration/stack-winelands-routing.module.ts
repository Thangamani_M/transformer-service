import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StackWinelandsSignalHistoryComponent } from './stack-winelands/signal-history/signal-history.component';
import { StackWinelandsComponent } from './stack-winelands/stack-winelands.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  {
    path: '', component: StackWinelandsComponent, data: { title: 'Operator Stack View' },canActivate: [AuthGuard]
  },
  {
    path: 'signal-history', component: StackWinelandsSignalHistoryComponent, data: { title: 'Signal History' }
  },
  { path: 'call-workflow-list', loadChildren: () => import('../configuration/call-workflow/call-workflow-list.module').then(m => m.CallWorkFlowListModule), data: { preload: true } },
  { path: 'call-workflow-list-dashboard', loadChildren: () => import('../configuration/call-workflow/call-workflow-list-dashboard/call-workflow-list-dashboard.module').then(m => m.CallWorkflowListDashboardModule), data: { preload: true } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class StackWinelandsRoutingModule { }
