import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { EventConfigurationListComponent } from './event-configuration-list/event-configuration-list.component';
import { EventConfigurationRoutingModule } from './event-configuration-routing.module';

@NgModule({
  declarations: [EventConfigurationListComponent],
  imports: [
    CommonModule,
    EventConfigurationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTabsModule,
    RouterModule
  ]
})
export class EventConfigurationModule { }
