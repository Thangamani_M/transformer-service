import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsefulNumberContactComponent } from './useful-numbers-contact/useful-numbers-contact.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  {
    path: '', component: UsefulNumberContactComponent, data: { title: 'Useful Numbers' },canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class UsefulNumberContactRoutingModule { }
