import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PttStackAreaConfigAddEditComponent } from './ptt-stack-area-config-add-edit/ptt-stack-area-config-add-edit.component';
import { PttStackAreaConfigViewComponent } from './ptt-stack-area-config-add-edit/ptt-stack-area-config-view.component';
import { PttStackAreaConfigRoutingModule } from './ptt-stack-area-config-routing.module';

@NgModule({
  declarations: [PttStackAreaConfigAddEditComponent, PttStackAreaConfigViewComponent],
  imports: [
    CommonModule,
    PttStackAreaConfigRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    ScrollPanelModule,
    RouterModule
  ],
  
})
export class PttStackAreaConfigModule { }
