import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PttStackAreaConfigAddEditComponent } from './ptt-stack-area-config-add-edit/ptt-stack-area-config-add-edit.component';
import { PttStackAreaConfigViewComponent } from './ptt-stack-area-config-add-edit/ptt-stack-area-config-view.component';


const routes: Routes = [
  {path:'', redirectTo: 'add-edit', pathMatch: 'full'},
  { path: 'view', component: PttStackAreaConfigViewComponent, data: { title: 'PTT Stack Area Config View' } },
  { path: 'add-edit', component: PttStackAreaConfigAddEditComponent, data: { title: 'PTT Stack Area Config Add/Edit' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PttStackAreaConfigRoutingModule { }
