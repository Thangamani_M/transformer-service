import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { PTTstackareaConfigModel } from '@modules/event-management/models/configurations/ptt-stack-area-config.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-ptt-stack-area-config-add-edit',
  templateUrl: './ptt-stack-area-config-add-edit.component.html',
})
export class PttStackAreaConfigAddEditComponent implements OnInit {

  pttStackAreaConfigId: any;
  loggedUser: any;
  StackAreaConfigDropDown: any;
  PTTStackAreaConfigAddEditForm: FormGroup;
  PTTStackAreaConfigDetails: any;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private _fb: FormBuilder, private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.pttStackAreaConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.pttStackAreaConfigId ? 'Update PTT Stack Area Config':'Add PTT Stack Area Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'PTT Stack Area Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 39} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.pttStackAreaConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/ptt-stack-area-config/view' , queryParams: {id: this.pttStackAreaConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createpttStackAreaForm();
    this.getStackAreaConfig();
    if (this.pttStackAreaConfigId) {
      this.getpttStackAreaConfigId(this.pttStackAreaConfigId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createpttStackAreaForm(): void {
    this.PTTStackAreaConfigAddEditForm = this._fb.group({
      channelName: ['', [Validators.required]],
      stackAreaConfigId: ['', [Validators.required]],
      username: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
    this.PTTStackAreaConfigAddEditForm.get('description').setValidators([Validators.required]);
    this.PTTStackAreaConfigAddEditForm.get('description').updateValueAndValidity();
  }

  getpttStackAreaConfigId(pttStackAreaConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PTT_STACK_AREA_CONFIG, pttStackAreaConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          let PTTstackareaModel = new PTTstackareaConfigModel(response.resources);
          this.PTTStackAreaConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.title, relativeRouterUrl: '' });
          this.PTTStackAreaConfigAddEditForm.patchValue(PTTstackareaModel);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getStackAreaConfig() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_STACK_AREA_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.StackAreaConfigDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  
  onSubmit(): void {
    if (this.PTTStackAreaConfigAddEditForm.invalid) {
      Object.keys(this.PTTStackAreaConfigAddEditForm.controls).forEach((key) => {
        this.PTTStackAreaConfigAddEditForm.controls[key].markAsDirty();
      });
      return;
    }
    let formValue = this.PTTStackAreaConfigAddEditForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (!this.pttStackAreaConfigId) {
      formValue.createdUserId = this.loggedUser.userId;
      formValue.isActive = true;
    }
    else {
      formValue.pttStackAreaConfigId = this.pttStackAreaConfigId;
      formValue.modifiedUserId = this.loggedUser.userId
    }
    let crudService: Observable<IApplicationResponse> = (!this.pttStackAreaConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PTT_STACK_AREA_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PTT_STACK_AREA_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=39');
      }
    })
  }

  onCRUDRequested(type){}

}
