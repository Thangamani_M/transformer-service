import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-fail-to-test-view',
  templateUrl: './fail-to-test-view.component.html'
})
export class FailToTestViewComponent implements OnInit {
  
failToTestId: any
faliledDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any;
totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
  tableComponentConfigs: {
    tabsList: []
  }
}

constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
  this.activatedRoute.queryParamMap.subscribe((params) => {
    this.failToTestId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
  });
  this.primengTableConfigProperties = {
    tableCaption: 'View Fail To Test',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
    { displayName: 'View Fail To Test', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 28 } }
      , { displayName: '', }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  this.rxjsService.setGlobalLoaderProperty(false);
  for (let index = 0; index < this.totaltab; index++) {
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
  }
  this.combineLatestNgrxStoreData()
  if (this.failToTestId) {
    this.getfaliledDetails().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.faliledDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + this.faliledDetails?.newDecoderOnSiteDelay;
      this.faliledDetails = [
        { name: 'New Decoder On Site Delay', value: response.resources?.newDecoderOnSiteDelay },
        { name: 'Generated Signal Type', value: response.resources?.alarmType },
        { name: 'Start Time', value: response.resources?.startTime },
        { name: 'End Time', value: response.resources?.endTime },
        { name: 'Signal Special - No Signal', value: response.resources?.noSignalHours+ "Hours" },
        { name: 'Created On', value: response.resources?.createdDate, isDateTime: true },
        { name: 'Modified On', value: response.resources?.modifiedDate, isDateTime: true },
        { name: 'Created By', value: response.resources?.createdUserName },
        { name: 'Modified By', value: response.resources?.modifiedUserName },
        { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}

combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(['event-management/event-configuration/fail-to-test/add-edit'], { queryParams: { id: this.failToTestId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
}

getfaliledDetails(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.FAIL_TO_TEST,
    this.failToTestId
  );
}

}
