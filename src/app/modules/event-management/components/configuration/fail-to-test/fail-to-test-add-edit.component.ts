import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { FailToTestModel } from '@modules/event-management/models/configurations/fail-to-test-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-fail-to-test-add-edit',
  templateUrl: './fail-to-test-add-edit.component.html'
})
export class FailToTestAddEditComponent implements OnInit {

  failToTestId: any
  alarmType: any = [];
  failToTestAddEditForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  failToTestDetails: any;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;
  
  constructor(private activatedRoute: ActivatedRoute, private snakbarService: SnackbarService, private snackbarService: SnackbarService, public momentService: MomentService, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.failToTestId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.failToTestId ? 'Update Fail To Test':'Add Fail To Test';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Fail To Test', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 28 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.failToTestId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/fail-to-test/view' , queryParams: {id: this.failToTestId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createfailToTestAddEditForm();
    this.getAlarmType();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.failToTestId) {
      this.getFailToTestDetailsById(this.failToTestId);
      this.getAlarmType();
      return
    }
  }

  createfailToTestAddEditForm(): void {
    let failToTestModel = new FailToTestModel();
    // create form controls dynamically from model class
    this.failToTestAddEditForm = this.formBuilder.group({
      newDecoderOnSiteDelay: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
      noSignalHours: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
    });
    Object.keys(failToTestModel).forEach((key) => {
      this.failToTestAddEditForm.addControl(key, new FormControl(failToTestModel[key]));
    });
    this.failToTestAddEditForm = setRequiredValidator(this.failToTestAddEditForm, ["alarmTypId", "startTime", "endTime"]);
    this.failToTestAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.failToTestAddEditForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.failToTestId) {
      this.failToTestAddEditForm.removeControl('failToTestId');
      this.failToTestAddEditForm.removeControl('modifiedUserId');
    } else {
      this.failToTestAddEditForm.removeControl('createdUserId');
    }
  }

  getAlarmType() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmType = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getFailToTestDetailsById(failToTestId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FAIL_TO_TEST, failToTestId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.failToTestDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.failToTestDetails?.alarmType ? this.title +' - '+ this.failToTestDetails?.alarmType : this.title + ' --', relativeRouterUrl: '' });
          let workingHoursFromdatetime = response.resources.startTime == null ? ("17:00:00").split(":") : response.resources.startTime == '00:00:00' ? ('17:00:00').split(":") : (response.resources.startTime).split(":");
          let workingHoursFromdate = new Date();
          workingHoursFromdate.setHours(workingHoursFromdatetime[0]);
          workingHoursFromdate.setMinutes(workingHoursFromdatetime[1]);
          this.failToTestAddEditForm.controls['startTime'].patchValue(workingHoursFromdate);
          let workingHoursTodatetime = response.resources.endTime == null ? ("17:00:00").split(":") : response.resources.endTime == '00:00:00' ? ('17:00:00').split(":") : (response.resources.endTime).split(":");
          let workingHoursTodate = new Date();
          workingHoursTodate.setHours(workingHoursTodatetime[0]);
          workingHoursTodate.setMinutes(workingHoursTodatetime[1]);
          this.failToTestAddEditForm.controls['endTime'].patchValue(workingHoursTodate);
          response.resources.startTime = workingHoursFromdate
          response.resources.endTime = workingHoursTodate
          this.failToTestDetails = response.resources;
          this.failToTestAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  Validate() {
    //Time validation
    var startTime = moment(this.failToTestAddEditForm.value.startTime, "HH:mm");
    var endTime = moment(this.failToTestAddEditForm.value.endTime, "HH:mm");
    if (endTime <= startTime) {
      this.snackbarService.openSnackbar("End time should not less than or equal to start time", ResponseMessageTypes.WARNING);
      return false;
    }
    return true;
  }


  onSubmit(): void {
    if (!this.Validate())
      return;
    if (this.failToTestAddEditForm.invalid) {
      return;
    }
    let formValue = this.failToTestAddEditForm.value;
    if (this.failToTestAddEditForm.value.startTime)
      formValue['startTime'] = this.momentService.convertTwelveToTwentyFourTime(this.failToTestAddEditForm.value.startTime);
    if (this.failToTestAddEditForm.value.endTime)
      formValue['endTime'] = this.momentService.convertTwelveToTwentyFourTime(this.failToTestAddEditForm.value.endTime);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.failToTestId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FAIL_TO_TEST, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FAIL_TO_TEST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=28');
      }
    })
  }

  endTimeValidation() {
    this.failToTestAddEditForm.get('endTime').setValue(null)
  }

  minTimeValidation() {
    let timValidation = this.momentService.timeDuration(this.failToTestAddEditForm.get('startTime').value, this.failToTestAddEditForm.get('endTime').value)
    if (!timValidation) {
      this.failToTestAddEditForm.get('endTime').setValue(null)
      this.snakbarService.openSnackbar('End time should be greater then Start time', ResponseMessageTypes.WARNING)

    }
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}