import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { FailToTestAddEditComponent } from './fail-to-test-add-edit.component';
import { FailToTestRoutingModule } from './fail-to-test-routing.module';
import { FailToTestViewComponent } from './fail-to-test-view.component';

@NgModule({
  declarations: [FailToTestAddEditComponent, FailToTestViewComponent],
  imports: [
    CommonModule,
    FailToTestRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class FailToTestModule { }
