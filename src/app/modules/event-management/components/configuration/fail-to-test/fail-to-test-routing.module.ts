import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FailToTestAddEditComponent } from './fail-to-test-add-edit.component';
import { FailToTestViewComponent } from './fail-to-test-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: FailToTestAddEditComponent, data: { title: 'Fail To Test Add/Edit' } },
  { path: 'view', component: FailToTestViewComponent, data: { title: 'Fail To Test View' } ,canActivate: [AuthGuard] },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class FailToTestRoutingModule { }
