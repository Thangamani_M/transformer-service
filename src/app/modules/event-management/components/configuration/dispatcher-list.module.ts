import { CommonModule, DatePipe } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { QuickAddServiceCallDialogModule } from '@modules/customer/components/customer/customer-management/quick-add-service-call-dialog/quick-add-service-call-dialog.module';
import { EventsMgmtSharedModule } from '@modules/event-management/shared/events-mgmt-shared.module';
import { KeyboardShortcutsModule } from 'ng-keyboard-shortcuts';
import { NgxPrintModule } from 'ngx-print';
import { GMapModule } from 'primeng/gmap';
import { DispatcherListRoutingModule } from './dispatcher-list-routing.module';
import { AddMessageModalComponent } from './dispatcher/add-message-modal.component';
import { AlarmListGridComponent } from './dispatcher/alarm-list-grid.component';
import { ClassifyIncidentDispatcherModalComponent } from './dispatcher/classify-incident-dispatcher-modal.component';
import { DispatcherListGridComponent } from './dispatcher/dispatcher-list-grid.component';
import { DispatcherListModalComponent } from './dispatcher/dispatcher-list-modal.component';
import { DispatcherListComponent } from './dispatcher/dispatcher-list.component';
import { DispatcherSignalHistoryComponent } from './dispatcher/signal-history/signal-history.component';
import { StackHistoryModule } from './stack-history.module';
import { ThirdPartyDetailsModule } from './third-party-details/third-party-details.module';
@NgModule({
  declarations: [DispatcherListComponent, DispatcherListGridComponent, AlarmListGridComponent, DispatcherListModalComponent, AddMessageModalComponent, ClassifyIncidentDispatcherModalComponent, DispatcherSignalHistoryComponent],
  imports: [
    CommonModule,
    DispatcherListRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
    NgxPrintModule,
    GMapModule,
    KeyboardShortcutsModule.forRoot(),
    EventsMgmtSharedModule,
    QuickAddServiceCallDialogModule,
    StackHistoryModule,
    ThirdPartyDetailsModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-EN' },
  { provide: MAT_DATE_LOCALE, useValue: 'en-EN' }, DatePipe],
  entryComponents: [DispatcherListModalComponent, AddMessageModalComponent, ClassifyIncidentDispatcherModalComponent]

})
export class DispatcherListModule { }
