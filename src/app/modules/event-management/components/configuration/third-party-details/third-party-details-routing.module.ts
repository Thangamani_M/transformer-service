import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ThirdPartyDetailsAddEditComponent } from './third-party-details-add-edit.component';
import { ThirdPartyDetailsViewComponent } from './third-party-details-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ThirdPartyDetailsAddEditComponent, data: { title: 'Third Party Add/Edit' } },
  { path: 'view', component: ThirdPartyDetailsViewComponent, data: { title: 'Third Party View' },canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ThirdPartyDetailsRoutingModule { }
