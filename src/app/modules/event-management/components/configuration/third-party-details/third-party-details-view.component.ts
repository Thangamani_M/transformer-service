import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { ThirdPartyDetails } from "../../../models/configurations/third-party-details-model";
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-third-party-details-view',
  templateUrl: './third-party-details-view.component.html',
})
export class ThirdPartyDetailsViewComponent implements OnInit {
  thirdPartyDetailsViewDetails:any;
  thirdPartyId: any;
  thirdPartyDetails: ThirdPartyDetails;
  THirdpartyDetailsViewDetails:any;
  primengTableConfigProperties:any;
  totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
  tableComponentConfigs: {
    tabsList: []
  }
}

  constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>,
    private router: Router,
    private commonService: CrudService, private rxjsService: RxjsService) {
    this.thirdPartyId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.primengTableConfigProperties = {
      tableCaption: 'Third Party Details',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Third Party Details', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 20} }, { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData();
    this.thirdPartyDetails = new ThirdPartyDetails();
    this.getThirdPartyDetailsById(this.thirdPartyId);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getThirdPartyDetailsById(thirdPartyId: string) {
    this.commonService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_DETAILS, thirdPartyId, false, null).subscribe((response: IApplicationResponse) => {
      this.thirdPartyDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.thirdPartyDetails?.companyName;
      this.onShowValue(response.resources);
      this.onAddressShowValue(response.resources);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onAddressShowValue(response?: any) {
    this.THirdpartyDetailsViewDetails = [
      {
        name: 'Address Info', columns: [
          { name: 'Building No', value: response?.buildingNumber },
          { name: 'Building Name', value: response?.buildingName },
          { name: 'Street Address', value: response?.streetAddress },
          { name: 'Street Number', value: response?.streetNumber },
          { name: 'Suburb', value: response?.suburbName },
          { name: 'City/Town', value: response?.cityName },
          { name: 'Province', value: response?.provinceName },
          { name: 'Postal Code', value: response?.postalCode },
          { name: 'Location Pin', value: response?.latitude },
        ]
      }
    ]
  }

  onShowValue(response?: any) {
    this.thirdPartyDetailsViewDetails = [
      { name: 'Company Name', value: response?.companyName },
      { name: 'Monitoring Center Number', value: response?.monitoringCenterNumber },
      { name: 'Division', value: response?.divisionName },
      { name: 'Contact Person Name', value: response?.contactPersonName },
      { name: 'Office Contact Number', value: response?.officeContactNumber },
      { name: 'Type', value: response?.thirdPartyTypeName },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate ,isDateTime: true},
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Modified By', value: response?.modifiedUserName },
      { name: 'Status', value: response?.isActive == true ? 'Active' : 'In Active', statusClass: response?.isActive==true?"status-label-green":'status-label-pink'},
      
    ]
  }

  edit() {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/event-management/event-configuration/third-party-details/add-edit"], { queryParams: { id: this.thirdPartyId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit();
        break;
    }
  }
}
