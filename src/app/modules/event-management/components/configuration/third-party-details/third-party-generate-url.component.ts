import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { countryCodes, defaultCountryCode, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { PanicAppApiSuffixModels } from '@modules/panic-app';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-third-party-generate-url-component',
  templateUrl: './third-party-generate-url.component.html'
})
export class ThirdPartyGenerateUrlComponent implements OnInit {
  formGroup: FormGroup;
  loggedInUserData: LoggedInUserModel;
  countryCodes = countryCodes;
  formConfigs = formConfigs;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>) {
    this.data.occurrenceBookId = this.data?.occurrenceBookId ? this.data.occurrenceBookId : "AB860E50-DC60-4417-BAD5-E71D90D676BB";
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createFormGroup();
    this.onGenerateURL();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createFormGroup(): void {
    this.formGroup = this.formBuilder.group({});
    let formGroupModel = {
      mobileNoCountryCode: defaultCountryCode, occurrenceBookId: this.data.occurrenceBookId, mobileNo: "", createdUserId: "",
      userId: "", isROOfficer: false, smsContent: ""
    }
    Object.keys(formGroupModel).forEach((key) => {
      this.formGroup.addControl(key, new FormControl(formGroupModel[key]));
    });
    this.formGroup = setRequiredValidator(this.formGroup, ["mobileNo", "smsContent"]);
  }

  onGenerateURL() {
    this.crudService.get(
      ModulesBasedApiSuffix.PANIC_APP,
      PanicAppApiSuffixModels.GENERATE_TRACKING_URL, undefined, false, prepareRequiredHttpParams({
        occurrenceBookId: this.data.occurrenceBookId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.formGroup.get('smsContent').setValue(response.resources.smsContent);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }
    this.formGroup.value.mobileNo = this.formGroup.value.mobileNo.replace(/\D/g, '');
    this.formGroup.value.createdUserId = this.loggedInUserData.userId;
    this.crudService.create(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.SEND_SMS, this.formGroup.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dialog.closeAll();
        }
      });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
