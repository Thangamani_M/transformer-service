import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ThirdPartyDetailsAddEditComponent } from './third-party-details-add-edit.component';
import { ThirdPartyDetailsRoutingModule } from './third-party-details-routing.module';
import { ThirdPartyDetailsViewComponent } from './third-party-details-view.component';
import { ThirdPartyGenerateUrlComponent } from './third-party-generate-url.component';
@NgModule({
  declarations: [ThirdPartyDetailsViewComponent, ThirdPartyDetailsAddEditComponent,ThirdPartyGenerateUrlComponent],
  imports: [
    CommonModule,
    ThirdPartyDetailsRoutingModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents:[ThirdPartyGenerateUrlComponent]
})
export class ThirdPartyDetailsModule { }
