import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, RxjsService, setRequiredValidator } from '@app/shared';
import { CrudService } from '@app/shared/services';
import {
  countryCodes,
  debounceTimeForSearchkeyword,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams
} from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ThirdPartyDetailsModel } from "../../../models/configurations/third-party-details-model";
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';



@Component({
  selector: 'app-third-party-details-add-edit',
  templateUrl: './third-party-details-add-edit.component.html',
})
export class ThirdPartyDetailsAddEditComponent implements OnInit {

  thirdPartyDetailsAddEditForm: FormGroup;
  errorMessage: any;
  countryCodes = countryCodes
  loggedInUserData: LoggedInUserModel;
  createdUserId: any;
  thirdPartyId: any;
  modifiedUserId: any;
  loggedUser: any;
  divisionDropDown: any = [];
  thirdPartyDropDown: any = [];
  thirdPartyDetails: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  formConfigs: any = formConfigs;
  addressList = [];
  checkMaxLengthAddress: boolean;
  inValid: boolean;
  addressObj: any;
  @ViewChild("input", { static: false }) row;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private router: Router, private httpCancelService: HttpCancelService) {
    this.thirdPartyId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.thirdPartyId ? 'Update Third Party Details':'Add Third Party Details';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Third Party Details', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 20} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.thirdPartyId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/third-party-details/view' , queryParams: {id: this.thirdPartyId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createThirdPartyDetailsForm();
    this.getDivisionDropDownById();
    this.getThirdPartyType(); 
    this.onFormControlChange(); 
    this.rxjsService.setGlobalLoaderProperty(false);
    
    this.onFormatedAddressFormControlValueChanges();
    if (this.thirdPartyId) {
      this.getThirdPartyDetailsByIdById(this.thirdPartyId);
      return
    }
  }

  onFormatedAddressFormControlValueChanges() {
    this.thirdPartyDetailsAddEditForm
      .get("fullAddress")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext || searchtext.length <3) {
           return this.getLoopableObjectRequestObservable = of();
          }
          else 
            this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true,
              prepareRequiredHttpParams({
                searchtext,
                isAfrigisSearch:false
              }));
        });
  }

  filterServiceTypeDetailsBySearchOptions(serachValue?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true, prepareGetRequestHttpParams(null, null, {
      searchtext: serachValue,
      isAfrigisSearch:false
    }))
  }
  createThirdPartyDetailsForm(): void {
    let thirdPartyDetailsModel = new ThirdPartyDetailsModel();
    // create form controls dynamically from model class
    this.thirdPartyDetailsAddEditForm = this.formBuilder.group({});
    Object.keys(thirdPartyDetailsModel).forEach((key) => {
      this.thirdPartyDetailsAddEditForm.addControl(key, new FormControl(thirdPartyDetailsModel[key]));
    });
    this.thirdPartyDetailsAddEditForm = setRequiredValidator(this.thirdPartyDetailsAddEditForm, ["companyName", "monitoringCenterNumber", "divisionId", "contactPersonName","thirdPartyTypeId", "officeContactNumber", "fullAddress"]);
    this.thirdPartyDetailsAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.thirdPartyDetailsAddEditForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.thirdPartyId) {
      this.thirdPartyDetailsAddEditForm.removeControl('thirdPartyId');
      this.thirdPartyDetailsAddEditForm.removeControl('modifiedUserId');
    } else {
      this.thirdPartyDetailsAddEditForm.removeControl('createdUserId');
    }
  }

  onFormControlChange(){
    this.thirdPartyDetailsAddEditForm
    .get("monitoringCenterNumberCountryCode")
    .valueChanges.subscribe((monitoringCenterNumberCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(monitoringCenterNumberCountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });

    this.thirdPartyDetailsAddEditForm
      .get("monitoringCenterNumber")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.thirdPartyDetailsAddEditForm.get("monitoringCenterNumberCountryCode").value
        );
      });

    this.thirdPartyDetailsAddEditForm
    .get("officeContactNumberCountryCode")
    .valueChanges.subscribe((officeContactNumberCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode1(officeContactNumberCountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });

    this.thirdPartyDetailsAddEditForm
    .get("officeContactNumber")
    .valueChanges.subscribe((mobileNumber2: string) => {
      this.setPhoneNumberLengthByCountryCode1(
        this.thirdPartyDetailsAddEditForm.get("officeContactNumberCountryCode").value
      );
    });
  }

  setPhoneNumberLengthByCountryCode1(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.thirdPartyDetailsAddEditForm
          .get("officeContactNumber")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.thirdPartyDetailsAddEditForm
          .get("officeContactNumber")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  }
  
  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.thirdPartyDetailsAddEditForm
          .get("monitoringCenterNumber")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.thirdPartyDetailsAddEditForm
          .get("monitoringCenterNumber")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  }

  clearAddressFormGroupValues(): void {
    this.thirdPartyDetailsAddEditForm.patchValue({
      latitude: null,
      longitude: null,
      latLong: null,
      suburbName: null,
      cityName: null,
      provinceName: null,
      postalCode: null,
      streetName: null,
      streetNo: null,
      buildingNo: null,
      buildingName: null,
      estateName: null,
      estateStreetName: null,
      estateStreetNo: null,
      fullAddress:null
    });
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
      selectedObject["longitude"] = selectedObject["longitude"] ? selectedObject["longitude"] : "";
      selectedObject["latitude"] = selectedObject["latitude"] ? selectedObject["latitude"] : "";
      if (selectedObject["latitude"] && selectedObject["longitude"]) {
        selectedObject["latLong"] = `${selectedObject["latitude"]}, ${selectedObject["longitude"]}`;
      } else {
        selectedObject["latLong"] = "";
      }
      let locationPin = selectedObject["longitude"] + ',' + selectedObject["longitude"]
      this.thirdPartyDetailsAddEditForm.patchValue(
        {
          addressId: selectedObject['addressId'],
          locationPin:locationPin,
          latitude: selectedObject["latitude"],
          longitude: selectedObject["longitude"],
          latLong: selectedObject["latLong"],
          suburbName: selectedObject["suburbName"],
          cityName: selectedObject["cityName"],
          provinceName: selectedObject["provinceName"],
          postalCode: selectedObject["postalCode"],
          streetAddress: selectedObject["streetName"],
          streetNumber: selectedObject["streetNo"],
          estateName: selectedObject["estateName"],
          estateStreetName: selectedObject["estateStreetName"],
          estateStreetNo: selectedObject["estateStreetNo"],
          buildingName: selectedObject["buildingName"],
          buildingNumber: selectedObject["buildingNo"],
          addressConfidentLevel: selectedObject["addressConfidentLevelName"],
          addressConfidentLevelId: selectedObject["addressConfidentLevelId"],
          fullAddress: selectedObject["fullAddress"]
        }, { emitEvent: false, onlySelf: true });
    
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getThirdPartyType() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.thirdPartyDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getThirdPartyDetailsByIdById(thirdPartyId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_DETAILS, thirdPartyId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.thirdPartyDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.thirdPartyDetails?.companyName ? this.title +' - '+ this.thirdPartyDetails?.companyName : this.title + ' --', relativeRouterUrl: '' });
          this.thirdPartyDetailsAddEditForm.patchValue(response.resources);
          this.thirdPartyDetailsAddEditForm.controls.locationPin.setValue(response.resources.longitude + ',' + response.resources.latitude)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }



  onSubmit(): void {
    if (this.thirdPartyDetailsAddEditForm.invalid) {
      return;
    }
    let location = this.thirdPartyDetailsAddEditForm.controls.locationPin.value;
    location = location.split(",");

    this.thirdPartyDetailsAddEditForm.controls.longitude.setValue(location[0]);
    this.thirdPartyDetailsAddEditForm.controls.latitude.setValue(location[1]);

    let formValue = this.thirdPartyDetailsAddEditForm.value;
    formValue.monitoringCenterNumber =  formValue.monitoringCenterNumber.split(' ').join('')
    formValue.officeContactNumber =  formValue.officeContactNumber.split(' ').join('')
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.thirdPartyId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_DETAILS, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_DETAILS, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=20');
      }
    })
  }

  onCRUDRequested(type){}

}
