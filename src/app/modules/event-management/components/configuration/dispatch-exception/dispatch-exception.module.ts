import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { DispatchExceptionAddEditComponent } from './dispatch-exception-add-edit.component';
import { DispatchExceptionRoutingModule } from './dispatch-exception-routing.module';
import { DispatchExceptionViewComponent } from './dispatch-exception-view.component';
@NgModule({
  declarations: [DispatchExceptionAddEditComponent, DispatchExceptionViewComponent],
  imports: [
    CommonModule,
    DispatchExceptionRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class DispatchExceptionModule { }
