import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DispatchExceptionAddEditComponent } from './dispatch-exception-add-edit.component';
import { DispatchExceptionViewComponent } from './dispatch-exception-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: DispatchExceptionAddEditComponent, data: { title: 'Dispatch Exception Add/Edit' } },
  { path: 'view', component: DispatchExceptionViewComponent, data: { title: 'Dispatch Exception View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DispatchExceptionRoutingModule { }
