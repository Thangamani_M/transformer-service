import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { DispatchExceptionModel } from '@modules/event-management/models/configurations/dispatch-exception-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dispatch-exception-add-edit',
  templateUrl: './dispatch-exception-add-edit.component.html',
})
export class DispatchExceptionAddEditComponent implements OnInit {

  dispatchExceptionId: any
  divisionDropDown: any = [];
  dispatchExceptionForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  dispatchExceptionDetails: any;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.dispatchExceptionId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.dispatchExceptionId ? 'Update Dispatch Exception':'Add Dispatch Exception';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Dispatch Exception', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 1 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.dispatchExceptionId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/dispatch-exception/view' , queryParams: {id: this.dispatchExceptionId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createDispatchExceptionForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.dispatchExceptionId) {
      this.getDispatchExceptionDetailsById(this.dispatchExceptionId);
      return
    }
  }

  createDispatchExceptionForm(): void {
    let dispatchExceptionModel = new DispatchExceptionModel();
    // create form controls dynamically from model class
    this.dispatchExceptionForm = this.formBuilder.group({
      distance: [null, [Validators.required, Validators.min(10), Validators.max(9999)]],
      dispatchTime: [null, [Validators.required, Validators.min(1), Validators.max(9999)]]
    });
    Object.keys(dispatchExceptionModel).forEach((key) => {
      this.dispatchExceptionForm.addControl(key, new FormControl(dispatchExceptionModel[key]));
    });
    this.dispatchExceptionForm = setRequiredValidator(this.dispatchExceptionForm, ["dispatchExceptionName", "description", "dispatchExceptionDivisionsList"]);
    this.dispatchExceptionForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.dispatchExceptionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.dispatchExceptionId) {
      this.dispatchExceptionForm.removeControl('dispatchExceptionId');
      this.dispatchExceptionForm.removeControl('modifiedUserId');
    } else {
      this.dispatchExceptionForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDispatchExceptionDetailsById(dispatchExceptionId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCH_EXCEPTION, dispatchExceptionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.dispatchExceptionDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.dispatchExceptionDetails?.dispatchExceptionName ? this.title +' - '+ this.dispatchExceptionDetails?.dispatchExceptionName : this.title + ' --', relativeRouterUrl: '' });
          this.dispatchExceptionForm.patchValue(response.resources);
          var dispatchExceptionDivisionsList = [];
          response.resources.dispatchExceptionDivisionsList.forEach(element => {
            dispatchExceptionDivisionsList.push(element.divisionId);
          });
          this.dispatchExceptionForm.get('dispatchExceptionDivisionsList').setValue(dispatchExceptionDivisionsList)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.dispatchExceptionForm.controls.dispatchExceptionDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.dispatchExceptionForm.controls.dispatchExceptionDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.dispatchExceptionForm.controls.dispatchExceptionDivisionsList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (this.dispatchExceptionForm.invalid) {
      return;
    }
    let formValue = this.dispatchExceptionForm.value;
    if (formValue.dispatchExceptionDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.dispatchExceptionDivisionsList = formValue.dispatchExceptionDivisionsList.filter(item => item != '')
      formValue.dispatchExceptionDivisionsList = formValue.dispatchExceptionDivisionsList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.dispatchExceptionId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCH_EXCEPTION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISPATCH_EXCEPTION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=1');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}