import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { VisibilityLoggingConfigAddEditComponent } from './visibility-logging-config-add-edit.component';
import { VisibilityLoggingConfigRoutingModule } from './visibility-logging-config-routing.module';
import { VisibilityLoggingConfigViewComponent } from './visibility-logging-config-view.component';



@NgModule({
  declarations: [VisibilityLoggingConfigAddEditComponent, VisibilityLoggingConfigViewComponent],
  imports: [
    CommonModule,
    VisibilityLoggingConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class VisibilityLoggingConfigModule { }
