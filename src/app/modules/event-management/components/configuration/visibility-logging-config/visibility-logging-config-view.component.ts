import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { RxjsService, CrudService, ModulesBasedApiSuffix, IApplicationResponse, CrudType, SnackbarService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-visibility-logging-config-view',
  templateUrl: './visibility-logging-config-view.component.html',
})
export class VisibilityLoggingConfigViewComponent implements OnInit {

  visibilityLoggingId: any;
  visibilityLoggingConfigDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any;
  totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
  tableComponentConfigs: {
    tabsList: []
  }
};

  constructor(private activatedRoute: ActivatedRoute,
     private router: Router,
      private rxjsService: RxjsService,
       private crudService: CrudService,
       private snackbarService: SnackbarService,
       private store: Store<AppState>) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.visibilityLoggingId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Visibility Logging Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Visibility Logging Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 32 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    }
    this.combineLatestNgrxStoreData();
    if (this.visibilityLoggingId) {
      this.getvisibilityLoggingConfigDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.visibilityLoggingConfigDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + this.visibilityLoggingConfigDetails.division;
        this.visibilityLoggingConfigDetails = [
          { name: 'Division', value: response.resources.division },
          { name: 'Visibility Config Id', value: response.resources.visibilityLoggingconfigId },
          { name: 'Description', value: response.resources.description },
          { name: 'Interval In Days', value: response.resources.intervalInDays },
          { name: 'Number Of Passes', value: response.resources.numberOfPasses },
          { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
          { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
        ];
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['event-management/event-configuration/visibility-logging-config/add-edit'], { queryParams: { id: this.visibilityLoggingId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
  }

  getvisibilityLoggingConfigDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.VISIBILITY_LOGGING_CONFIG,
      this.visibilityLoggingId
    );
  }

}
