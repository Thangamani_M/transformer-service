import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisibilityLoggingConfigAddEditComponent } from './visibility-logging-config-add-edit.component';
import { VisibilityLoggingConfigViewComponent } from './visibility-logging-config-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: VisibilityLoggingConfigAddEditComponent, data: { title: 'Visibility Logging Config Add/Edit' } },
  { path: 'view', component: VisibilityLoggingConfigViewComponent, data: { title: 'Visibility Logging Config View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class VisibilityLoggingConfigRoutingModule { }
