import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, HttpCancelService, RxjsService, CrudService, ModulesBasedApiSuffix, IApplicationResponse, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import {
  VisibilityLoggingCofigModel
} from '@modules/event-management/models/visibility-loggong-config-model';
import { UserLogin } from '@modules/others/models';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-visibility-logging-config-add-edit',
  templateUrl: './visibility-logging-config-add-edit.component.html',
})
export class VisibilityLoggingConfigAddEditComponent implements OnInit {

  visibilityLoggingId: any;
  visibilityConfigDetails: any;
  visibilityLoggingConfigForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  visibilityConfigDropDown: any;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.visibilityLoggingId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.visibilityLoggingId ? 'Update Visibility Logging Config':'Add Visibility Logging Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Visibility Logging Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 32} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.visibilityLoggingId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/visibility-logging-config/view' , queryParams: {id: this.visibilityLoggingId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createvisibilityLoggingConfigForm();
    this.getVisibilityLoggingConfig()
    if (this.visibilityLoggingId) {
      this.getVisibilityLoggingConfigByID(this.visibilityLoggingId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  getVisibilityLoggingConfig() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.visibilityConfigDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createvisibilityLoggingConfigForm(): void {
    let visibilityLoggingCofigModel = new VisibilityLoggingCofigModel();
    // create form controls dynamically from model class
    this.visibilityLoggingConfigForm = this.formBuilder.group({
      divisionId: ['', Validators.required],
      description: ['', Validators.required],
      intervalInDays: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
      numberOfPasses: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
      distanceToCapture: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
    });
    Object.keys(visibilityLoggingCofigModel
    ).forEach((key) => {
      this.visibilityLoggingConfigForm.addControl(key, new FormControl(visibilityLoggingCofigModel
      [key]));
    });
    this.visibilityLoggingConfigForm = setRequiredValidator(this.visibilityLoggingConfigForm, ["divisionId", "description"])
    this.visibilityLoggingConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.visibilityLoggingConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.visibilityLoggingId) {
      this.visibilityLoggingConfigForm.removeControl('modifiedUserId');
    } else {
      this.visibilityLoggingConfigForm.removeControl('createdUserId');
    }
  }

  getVisibilityLoggingConfigByID(visibilityLoggingId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VISIBILITY_LOGGING_CONFIG, visibilityLoggingId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.visibilityConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.visibilityConfigDetails?.division ? this.title +' - '+ this.visibilityConfigDetails?.division : this.title + ' --', relativeRouterUrl: '' });
          this.visibilityLoggingConfigForm.patchValue(this.visibilityConfigDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.visibilityLoggingConfigForm.invalid) {
      return;
    }
    let formValue = this.visibilityLoggingConfigForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.visibilityLoggingId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VISIBILITY_LOGGING_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VISIBILITY_LOGGING_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=32');
      }
    })
  }

  onCRUDRequested(type){}

}