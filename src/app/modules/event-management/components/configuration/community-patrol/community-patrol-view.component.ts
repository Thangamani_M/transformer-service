import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-community-patrol-view',
  templateUrl: './community-patrol-view.component.html',
})
export class CommunityPatrolViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  communityPatrolId: any;
  communityPatrolDetails: any;
  communityPatrolCustomersList: any = [];
  totaltab = 0
  selectedTab = 0
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private snackbarService: SnackbarService, private momentService: MomentService,
    private store: Store<AppState>,
    private router: Router) {
    super();
    this.communityPatrolId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.primengTableConfigProperties = {
      tableCaption: "View Community Patrol",
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Community Patrol', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 18 } }, { displayName: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableViewBtn: true,
            columns: [{ field: 'customerNumber', header: 'Customer Id', width: '80px' },
            { field: 'displayName', header: 'Customer Name', width: '120px' },
            { field: 'streetAddress', header: 'Address', width: '80px' },
            { field: 'mobileNumber', header: 'Mobile Number', width: '80px' },
            { field: 'customerTypeName', header: 'Customer Type', width: '120px' },
            { field: 'email', header: 'Email', width: '120px' },
            { field: 'addressCreatedDate', header: 'Created On', width: '120px' , isDate:true},
            { field: 'isActive', header: 'Status', width: '80px' },],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };

    this.combineLatestNgrxStoreData()
    let otherParams = {}
    this.getSignalHistoryList(null, null, otherParams);
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.communityPatrolId) {
      this.getCommunityPatrolDetailsById(this.communityPatrolId);
    } else {
      this.getSignalHistoryList(this.row['pageIndex'], this.row['pageSize'], otherParams);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSignalHistoryList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.dataList.map(item => {
            item.checkHidden  = true
          })
          if (this.communityPatrolId) {
            this.dataList.forEach((row: any) => {
              this.communityPatrolDetails?.communityPatrolCustomersList.forEach(element => {
                if (row.customerId == element.customerId) {
                  this.selectedRows.push(row)
                  this.selectedRows = [...this.selectedRows, row]
                  row.checkedValue = row
                }
              });
            });
          }
          else {
              if (this.communityPatrolId) {
                this.dataList.forEach((row: any) => {
                  this.communityPatrolDetails?.communityPatrolCustomersList.forEach(element => {
                    if (row.id == element.namedStackConfigId) {
                      let existDate = this.selectedRows.find(x => x.id == row.id)
                      if (!existDate) {
                        this.selectedRows = [...this.selectedRows, row]
                      }
                    }
                  });
                });
              }
          }
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
        case CrudType.EDIT:
          this.edit();
          break;
    }
  }

  getCommunityPatrolDetailsById(communityPatrolId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COMMUNITY_PATROL, communityPatrolId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.communityPatrolDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.communityPatrolDetails?.communityName;
          var communityPatrolDivisionList = [];
          response.resources.communityPatrolDivisionList.forEach(element => {
            communityPatrolDivisionList.push(element.divisionId);
          });
          this.getSignalHistoryList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onChangeSelecedRows(event) {
    this.selectedRows = event
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  edit() {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
      this.router.navigate(['event-management/event-configuration/community-patrol/add-edit'], { queryParams: { id: this.communityPatrolId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
    }

}
