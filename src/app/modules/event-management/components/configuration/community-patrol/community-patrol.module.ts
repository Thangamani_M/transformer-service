import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule, MatPaginatorModule, MatSelectModule, MatTableModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { CommunityPatrolAddEditComponent } from './community-patrol-add-edit.component';
import { CommunityPatrolRoutingModule } from './community-patrol-routing.module';
import { CommunityPatrolViewComponent } from './community-patrol-view.component';
@NgModule({
  declarations: [CommunityPatrolAddEditComponent, CommunityPatrolViewComponent],
  imports: [
    CommonModule,
    CommunityPatrolRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class CommunityPatrolModule { }
