import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CommunityPatrolModel } from '@modules/event-management/models/configurations/community-patrol.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-community-patrol-add-edit',
  templateUrl: './community-patrol-add-edit.component.html',
})
export class CommunityPatrolAddEditComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  communityPatrolForm: FormGroup;
  loggedUser: UserLogin;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  divisionDropDown: any;
  lssDetails: [];
  communityPatrolId: any;
  communityPatrolDetails: any;
  communityPatrolCustomersList: any = [];
  totaltab = 0;
  selectedTab = 0;
  divisionIds: any;
  title:string;

  constructor(private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private snakBarService: SnackbarService,
    private momentService: MomentService,
    private _fb: FormBuilder,
    private store: Store<AppState>,
    private httpCancelService: HttpCancelService,
    private router: Router) {
    super();
    this.communityPatrolId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.communityPatrolId ? 'Update Community Patrol':'Add Community Patrol';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Community Patrol', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 18} }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'customerAddressId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'customerNumber', header: 'Customer Id', width: '80px' },
            { field: 'displayName', header: 'Customer Name', width: '120px' },
            { field: 'streetAddress', header: 'Address', width: '80px' },
            { field: 'mobileNumber', header: 'Mobile Number', width: '80px' },
            { field: 'customerTypeName', header: 'Customer Type', width: '120px' },
            { field: 'email', header: 'Email', width: '120px' },
            { field: 'addressCreatedDate', header: 'Created On', width: '120px', isDate:true },
            { field: 'isActive', header: 'Status', width: '80px'}],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
    if(this.communityPatrolId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/community-patrol/view' , queryParams: {id: this.communityPatrolId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createcommunityPatrolForm();
    this.getDivisionDropDownById();
    this.getLssRegistrationDropDown();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.communityPatrolId) {
      this.getCommunityPatrolDetailsById(this.communityPatrolId);
    } else {
      this.getCommunityPatroList();
    }
  }

  createcommunityPatrolForm(): void {
    let communityPatrolModel = new CommunityPatrolModel();
    // create form controls dynamically from model class
    this.communityPatrolForm = this._fb.group({
      patrolTime: [null, [Validators.required, Validators.min(10), Validators.max(99999)]],
      restTime: [null, [Validators.required, Validators.min(10), Validators.max(99999)]]
    });
    Object.keys(communityPatrolModel).forEach((key) => {
      this.communityPatrolForm.addControl(key, new FormControl(communityPatrolModel[key]));
    });
    this.communityPatrolForm = setRequiredValidator(this.communityPatrolForm, ["communityName", "communityPatrolDivisionList","lssId"])
    this.communityPatrolForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.communityPatrolForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getLssRegistrationDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.LSS_REGISTRATION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.lssDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCommunityPatroList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    if (this.divisionIds == undefined) {
      return
    }
    this.loading = true;
    let parms = { DivisionIds: this.divisionIds}
    parms['DivisionIds'] = this.divisionIds
    otherParams = { ...otherParams, ...parms };
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          if (this.communityPatrolId) {
            this.dataList.forEach((row: any) => {
              this.communityPatrolDetails.communityPatrolCustomersList.forEach(element => {
                if (row.customerId == element.customerId) {
                  this.selectedRows.push(row)
                  this.selectedRows = [...this.selectedRows, row]
                }
              });
            });
          }
          else {
              if (this.communityPatrolId) {
                this.dataList.forEach((row: any) => {
                  this.communityPatrolDetails.communityPatrolCustomersList.forEach(element => {
                    if (row.id == element.namedStackConfigId) {
                      let existDate = this.selectedRows.find(x => x.id == row.id)
                      if (!existDate) {
                        this.selectedRows = [...this.selectedRows, row]
                      }
                    }
                  });
                });
              }
          }
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getCommunityPatroList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  toggleDivisionOne(all) {
    this.divisionIds = this.communityPatrolForm.controls.communityPatrolDivisionList.value.join(",");
    if (this.divisionIds) {
      this.getCommunityPatroList();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.communityPatrolForm.controls.communityPatrolDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.communityPatrolForm.controls.communityPatrolDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
        let value = this.communityPatrolForm.controls.communityPatrolDivisionList.value.join(',')
      this.divisionIds = value.replace(/,([^,]*)$/, '$1')
      this.getCommunityPatroList();
      this.rxjsService.setGlobalLoaderProperty(false);
    } else {
      this.communityPatrolForm.controls.communityPatrolDivisionList.patchValue([]);
    }
  }

  getCommunityPatrolDetailsById(communityPatrolId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COMMUNITY_PATROL, communityPatrolId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.communityPatrolDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.communityPatrolDetails?.communityName ? this.title +' - '+ this.communityPatrolDetails?.communityName : this.title + ' --', relativeRouterUrl: '' });
          this.communityPatrolForm.patchValue(response.resources)
          var communityPatrolDivisionList = [];
          response.resources.communityPatrolDivisionList.forEach(element => {
            communityPatrolDivisionList.push(element.divisionId);
          });
        }
        var communityPatrolDivisionList = [];
          let abc = '';
          response.resources.communityPatrolDivisionList.forEach((element, index) => {
            communityPatrolDivisionList.push(element.divisionId);
            if (index == 0) {
              abc = abc + '' + element.divisionId
            } else {
              abc = abc + ',' + element.divisionId
            }
          });
          this.divisionIds = abc;
          this.communityPatrolForm.get('communityPatrolDivisionList').setValue(communityPatrolDivisionList)
          this.getCommunityPatroList(this.row['pageIndex'], this.row['pageSize']);
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.communityPatrolForm.invalid) {
      return;
    }
    this.communityPatrolCustomersList = [];
    this.selectedRows.forEach(item => {
      this.communityPatrolCustomersList.push({
        customerId: item.customerId,
        addressId: item.customerAddressId,
        isActive: item.isActive ? item.isActive : false
      });
    });
    this.communityPatrolForm.get('communityPatrolCustomersList').setValue(this.communityPatrolCustomersList)
    if (this.communityPatrolForm.value.communityPatrolCustomersList.length == 0) {
      this.snakBarService.openSnackbar('Please select customer', ResponseMessageTypes.WARNING);
      return
    }
    let formValue = this.communityPatrolForm.value;
    if (formValue.communityPatrolDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.communityPatrolDivisionList = formValue.communityPatrolDivisionList.filter(item => item != '')
      formValue.communityPatrolDivisionList = formValue.communityPatrolDivisionList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.communityPatrolId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COMMUNITY_PATROL, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COMMUNITY_PATROL, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=18');
      }
    })
  }

  onChangeSelecedRows(event) {
    this.selectedRows = event
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}