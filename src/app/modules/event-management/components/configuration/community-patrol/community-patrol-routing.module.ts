import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommunityPatrolAddEditComponent } from './community-patrol-add-edit.component';
import { CommunityPatrolViewComponent } from './community-patrol-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CommunityPatrolAddEditComponent, data: { title: 'Community Patrol Add/Edit' } },
  { path: 'view', component: CommunityPatrolViewComponent, data: { title: 'Community Patrol View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CommunityPatrolRoutingModule { }
