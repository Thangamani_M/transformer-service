import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { FeedbackOffsiteModel } from '@modules/event-management/models/configurations/feedback-offsite-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-feedback-offsite-add-edit',
  templateUrl: './feedback-offsite-add-edit.component.html',
})
export class FeedbackOffsiteAddEditComponent implements OnInit {

  feedbackOffsiteId: any
  divisionDropDown: any = [];
  feedbackOffsiteForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  feedbackOffsiteDetails: any;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.feedbackOffsiteId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.feedbackOffsiteId ? 'Update Feedback Offsite':'Add Feedback Offsite';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Feedback Offsite', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 2 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.feedbackOffsiteId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/feedback-offsite/view' , queryParams: {id: this.feedbackOffsiteId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createFeedbackOffsiteForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.feedbackOffsiteId) {
      this.getFeedbackOffsiteDetailsById(this.feedbackOffsiteId);
      this.getDivisionDropDownById();
      return
    }
  }

  createFeedbackOffsiteForm(): void {
    let feedbackOffsiteModel = new FeedbackOffsiteModel();
    this.feedbackOffsiteForm = this.formBuilder.group({
      distance: [null, [Validators.required, Validators.min(10), Validators.max(9999)]]
    });
    Object.keys(feedbackOffsiteModel).forEach((key) => {
      this.feedbackOffsiteForm.addControl(key, new FormControl(feedbackOffsiteModel[key]));
    });
    this.feedbackOffsiteForm = setRequiredValidator(this.feedbackOffsiteForm, ["feedbackOffsiteName", "description", "feedbackOffsiteDivisionList"]);
    this.feedbackOffsiteForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.feedbackOffsiteForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.feedbackOffsiteId) {
      this.feedbackOffsiteForm.removeControl('feedbackOffsiteId');
      this.feedbackOffsiteForm.removeControl('modifiedUserId');
    } else {
      this.feedbackOffsiteForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getFeedbackOffsiteDetailsById(feedbackOffsiteId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FEEDBACK_OFFSITE, feedbackOffsiteId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.feedbackOffsiteDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.feedbackOffsiteDetails?.feedbackOffsiteName ? this.title +' - '+ this.feedbackOffsiteDetails?.feedbackOffsiteName : this.title + ' --', relativeRouterUrl: '' });
          this.feedbackOffsiteForm.patchValue(response.resources);
          var feedbackOffsiteDivisionList = [];
          response.resources.feedbackOffsiteDivisionList.forEach(element => {
            feedbackOffsiteDivisionList.push(element.divisionId);
          });
          this.feedbackOffsiteForm.get('feedbackOffsiteDivisionList').setValue(feedbackOffsiteDivisionList)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.feedbackOffsiteForm.controls.feedbackOffsiteDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.feedbackOffsiteForm.controls.feedbackOffsiteDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.feedbackOffsiteForm.controls.feedbackOffsiteDivisionList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (this.feedbackOffsiteForm.invalid) {
      return;
    }
    let formValue = this.feedbackOffsiteForm.value;
    if (formValue.feedbackOffsiteDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.feedbackOffsiteDivisionList = formValue.feedbackOffsiteDivisionList.filter(item => item != '')
      formValue.feedbackOffsiteDivisionList = formValue.feedbackOffsiteDivisionList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.feedbackOffsiteId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FEEDBACK_OFFSITE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FEEDBACK_OFFSITE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=2');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}