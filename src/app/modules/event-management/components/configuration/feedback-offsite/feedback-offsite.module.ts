import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { FeedbackOffsiteAddEditComponent } from './feedback-offsite-add-edit.component';
import { FeedbackOffsiteRoutingModule } from './feedback-offsite-routing.module';
import { FeedbackOffsiteViewComponent } from './feedback-offsite-view.component';



@NgModule({
  declarations: [FeedbackOffsiteAddEditComponent, FeedbackOffsiteViewComponent],
  imports: [
    CommonModule,
    FeedbackOffsiteRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class FeedbackOffsiteModule { }
