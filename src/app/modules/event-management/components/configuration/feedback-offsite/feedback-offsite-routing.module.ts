import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeedbackOffsiteAddEditComponent } from './feedback-offsite-add-edit.component';
import { FeedbackOffsiteViewComponent } from './feedback-offsite-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: FeedbackOffsiteAddEditComponent, data: { title: 'Feedback Offsite Add/Edit' } },
  { path: 'view', component: FeedbackOffsiteViewComponent, data: { title: 'Feedback Offsite View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class FeedbackOffsiteRoutingModule { }
