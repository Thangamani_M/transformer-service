import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NamedStackAddEditComponent } from './named-stack-add-edit.component';
import { NamedStackViewComponent } from './named-stack-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: NamedStackAddEditComponent, data: { title: 'Named Stack Config Add/Edit' } },
  { path: 'view', component: NamedStackViewComponent, data: { title: 'Named Stack Config View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StackConfigRoutingModule { }
