import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-named-stack-view',
  templateUrl: './named-stack-view.component.html',
})
export class NamedStackViewComponent implements OnInit {

namedStackConfigId: any
namedStackConfigDetails: any;
selectedTabIndex = 0;
primengTableConfigProperties: any;
totaltab = 0;
selectedTab = 0;
pageLevelProperties: any = {
  tableComponentConfigs: {
    tabsList: []
  }
}

constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {

  this.activatedRoute.queryParams.subscribe(param=>{
    this.namedStackConfigId =  param['id']
    this.totaltab =  +param['totalTabs'] || 40
    this.selectedTab =  +param['selectedTab']
  })
  this.primengTableConfigProperties = {
    tableCaption: this.namedStackConfigId && 'View Named Stack Configuration',
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
    { displayName: 'Named Stack Configuration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 2 } }
    , {displayName:'',}],
    tableComponentConfigs: {
      tabsList: [
        {
          enableAction: false,
          enableBreadCrumb: true,
        }
      ]
    }
  }
}

ngOnInit(): void {
  for(let i=0; i < this.totaltab; i++){
    this.pageLevelProperties.tableComponentConfigs.tabsList.push({})
  }
  this.rxjsService.setGlobalLoaderProperty(false);
  this.combineLatestNgrxStoreData()
  if (this.namedStackConfigId) {
    this.getEmailDistributionDetailsById().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.namedStackConfigDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --"+ this.namedStackConfigDetails.namedStackConfigName;
      this.namedStackConfigDetails = [
        { name: 'Named Stack Name', value: response.resources.namedStackConfigName },
        { name: 'Description', value: response.resources.description },
        { name: 'Created By', value: response.resources.createdUserName },
        { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
        { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
        { name: 'Created By', value: response.resources.createdUserName },
        { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
      ];
    })
  }
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
}


combineLatestNgrxStoreData() {
  combineLatest(
    [this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)],
  ).subscribe((response) => {
    let permission = response[1][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
      this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  });
}

onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
  switch (type) {
    case CrudType.EDIT:
      this.onEditButtonClicked();
      break;
  }
}

onEditButtonClicked(): void {
  if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
  this.router.navigate(['event-management/event-configuration/namedStack-config/add-edit'], { queryParams: { id: this.namedStackConfigId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
}

getEmailDistributionDetailsById(): Observable<IApplicationResponse> {
  return this.crudService.get(
    ModulesBasedApiSuffix.EVENT_MANAGEMENT,
    EventMgntModuleApiSuffixModels.NAMED_STACK_CONFIG,
    this.namedStackConfigId
  );
}

}
