import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
import { NamedStackModel } from './../../../models/configurations';

@Component({
  selector: 'app-named-stack-add-edit',
  templateUrl: './named-stack-add-edit.component.html',

})
export class NamedStackAddEditComponent implements OnInit {

  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  namedStackConfigId: string;
  namedStackForm: FormGroup;
  userData: UserLogin;
  namedStackConfigDetails: any;
  PageType;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activateRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService, private httpCancelService: HttpCancelService,
    private store: Store<AppState>) {
    this.namedStackConfigId = this.activateRoute.snapshot.queryParams.id;
    this.totaltab = +this.activateRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activateRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.title =this.namedStackConfigId ? 'Update Named Stack Configuration':'Add Named Stack Configuration';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Named Stack Configuration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 2 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.namedStackConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/namedStack-config/view' , queryParams: {id: this.namedStackConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createnamedStackForm();
    if (this.namedStackConfigId) {
      this.getnamedStakeValues();
      this.PageType = "Update";
    } else {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.PageType = "Add";

    }
  }

  getnamedStakeValues() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.NAMED_STACK_CONFIG, this.namedStackConfigId, true).subscribe((response) => {
      let namedStackAddEditModel = new NamedStackModel(response.resources);
      this.namedStackConfigDetails = response.resources;
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.namedStackConfigDetails ?.namedStackConfigName ? this.title +' - '+ this.namedStackConfigDetails ?.namedStackConfigName : this.title + ' --', relativeRouterUrl: '' });
      this.namedStackForm.setValue(namedStackAddEditModel);
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  createnamedStackForm(): void {
    let Named_Stack_Model = new NamedStackModel();
    // create form controls dynamically from model class
    this.namedStackForm = this.formBuilder.group({
    });
    Object.keys(Named_Stack_Model).forEach((key) => {
      this.namedStackForm.addControl(key, new FormControl(Named_Stack_Model[key]));
    });
    this.namedStackForm = setRequiredValidator(this.namedStackForm, ["namedStackConfigName", "description"]);
  }

  onSubmit(): void {
    if (this.namedStackForm.invalid) {
      return;
    }

    if (this.namedStackConfigId) {
      this.namedStackForm.value.modifiedUserId = this.userData.userId;
      this.namedStackForm.value.namedStackConfigId = this.namedStackConfigId;
    } else {
      this.namedStackForm.value.createdUserId = this.userData.userId;
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.namedStackConfigId
      ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.NAMED_STACK_CONFIG, this.namedStackForm.value)
      : this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.NAMED_STACK_CONFIG, this.namedStackForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: 2 }, skipLocationChange: true });

      }
    })
  }

  onCRUDRequested(type){}

}
