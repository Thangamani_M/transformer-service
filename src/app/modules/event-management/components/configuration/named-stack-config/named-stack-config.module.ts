import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { NamedStackAddEditComponent } from './named-stack-add-edit.component';
import { StackConfigRoutingModule } from './named-stack-config-routing.component';
import { NamedStackViewComponent } from './named-stack-view.component';
@NgModule({
  declarations: [NamedStackAddEditComponent, NamedStackViewComponent],
  imports: [
    CommonModule,
    StackConfigRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class NamedStackConfigModule { }
