import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, HttpCancelService, IApplicationResponse, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import {
  ModulesBasedApiSuffix,
  setRequiredValidator
} from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ShiftChangeWayPointWayModel } from "../../../models/configurations/shift-change-way-point-model";
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
declare var google: any;


@Component({
  selector: 'app-shift-change-way-point-add-edit',
  templateUrl: './shift-change-way-point-add-edit.component.html',
  styleUrls: ['./shift-change-way-point-add-edit.component.scss']
})
export class ShiftChangeWayPointAddEditComponent implements OnInit {

  shiftChangeWayPointAddEditForm: FormGroup;
  divisionList = [];
  mainAreaList = [];
  dropdownsAndData = [];
  shiftChangeWayPointId: any;
  selectedDivisionId: any;
  loggedUser: any;
  map: any;
  options: any;
  overlays: any;
  openMapDialog: boolean = false;
  divisionDropDown: any = [];
  shiftChangeWayPointDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;
  
  constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private router: Router, private httpCancelService: HttpCancelService) {
    this.shiftChangeWayPointId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.map = google.maps?.Map;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.shiftChangeWayPointId ? 'Update Shift Change Way Points':'Add Shift Change Way Points';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Shift Change Way Points', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 21 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.shiftChangeWayPointId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/shift-change-way-point/view' , queryParams: {id: this.shiftChangeWayPointId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createShiftChangeWayPointForm();
    this.shiftChangeWayPointAddEditForm.get("divisionId").valueChanges.subscribe(data => {
      this.getMainAreaList(data);
      this.shiftChangeWayPointAddEditForm.controls.mainAreaId.setValue('')
    })
    this.options = {
      center: {
        lat: -37.6878,
        lng: 176.1651
      },
      zoom: 11,
      mapTypeId: google.maps?.MapTypeId.ROADMAP
    };
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.shiftChangeWayPointId) {
      this.getShiftChangeWayPointByIdById(this.shiftChangeWayPointId);
      return
    }

  }


  createShiftChangeWayPointForm(): void {
    let shiftChangeWayPointWayModel = new ShiftChangeWayPointWayModel();
    // create form controls dynamically from model class
    this.shiftChangeWayPointAddEditForm = this.formBuilder.group({});
    Object.keys(shiftChangeWayPointWayModel).forEach((key) => {
      this.shiftChangeWayPointAddEditForm.addControl(key, new FormControl(shiftChangeWayPointWayModel[key]));
    });
    this.shiftChangeWayPointAddEditForm = setRequiredValidator(this.shiftChangeWayPointAddEditForm, ["divisionId", "mainAreaId", "shiftChangeWayPointName", "shiftChangeWayPoint", "description", "radius"]);
    this.shiftChangeWayPointAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.shiftChangeWayPointAddEditForm.get('radius').setValidators([Validators.required,Validators.min(10),Validators.max(9999)])
    this.shiftChangeWayPointAddEditForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.shiftChangeWayPointId) {
      this.shiftChangeWayPointAddEditForm.removeControl('shiftChangeWayPointId');
      this.shiftChangeWayPointAddEditForm.removeControl('modifiedUserId');
    } else {
      this.shiftChangeWayPointAddEditForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  
  openMap() {
    if (this.shiftChangeWayPointAddEditForm.controls.shiftChangeWayPoint.value) {
      if (this.shiftChangeWayPointAddEditForm.controls.shiftChangeWayPoint.invalid) {
        this.shiftChangeWayPointAddEditForm.get('shiftChangeWayPoint').markAsDirty()
        return
      }
    }
    this.options = {};
    this.overlays = {};
    let location = this.shiftChangeWayPointAddEditForm.controls.shiftChangeWayPoint.value;
    this.openMapDialog = true
    if (location) {
      location = location.split(",");
    } else {
      location = []
      location[0] = -25.746020 // fidelity south afcia coordinates by default
      location[1] = 28.187120 // fidelity south afcia coordinates by default
    }
    this.options = {
      center: { lat: Number(location[0]), lng: Number(location[1]) },
      zoom: 12
    };

    setTimeout(() => {
      this.map.setCenter({
        lat: Number(location[0]),
        lng: Number(location[1])
      });
    }, 500);

    this.overlays = [new google.maps.Marker({ position: { lat: Number(location[0]), lng: Number(location[1]) }, icon: "assets/img/map-icon.png" })];
  }

  handleMapClick(event) {
    //event: MouseEvent of Google Maps api
    this.shiftChangeWayPointAddEditForm.get('shiftChangeWayPoint').setValue(event.latLng.lat() + ',' + event.latLng.lng())
    this.openMapDialog = false
    this.rxjsService.setFormChangeDetectionProperty(true)

  }


  setMap(event) {
    this.map = event.map;
  }

  getShiftChangeWayPointByIdById(shiftChangeWayPointId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SHIFT_CHANGE_WAY_POINT, shiftChangeWayPointId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.shiftChangeWayPointDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.shiftChangeWayPointDetails?.shiftChangeWayPointName ? this.title +' - '+ this.shiftChangeWayPointDetails?.shiftChangeWayPointName : this.title + ' --', relativeRouterUrl: '' });
          this.shiftChangeWayPointAddEditForm.patchValue(response.resources);
          this.shiftChangeWayPointAddEditForm.controls.mainAreaId.setValue(response.resources.mainAreaId)
          this.shiftChangeWayPointAddEditForm.controls.shiftChangeWayPoint.setValue(response.resources.longitude + "," + response.resources.latitude)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }



  onSubmit(): void {
    if (this.shiftChangeWayPointAddEditForm.invalid) {
      return;
    }

    let location = this.shiftChangeWayPointAddEditForm.controls.shiftChangeWayPoint.value;
    location = location.split(",");

    this.shiftChangeWayPointAddEditForm.controls.longitude.setValue(location[0]);
    this.shiftChangeWayPointAddEditForm.controls.latitude.setValue(location[1]);

    let formValue = this.shiftChangeWayPointAddEditForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.shiftChangeWayPointId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SHIFT_CHANGE_WAY_POINT, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SHIFT_CHANGE_WAY_POINT, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=21');
      }
    })
  }

  onCRUDRequested(type){}

}
