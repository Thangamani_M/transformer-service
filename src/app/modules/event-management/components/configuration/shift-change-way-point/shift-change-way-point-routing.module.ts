import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShiftChangeWayPointAddEditComponent } from './shift-change-way-point-add-edit.component';
import { ShiftChangeWayPointViewComponent } from './shift-change-way-point-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ShiftChangeWayPointAddEditComponent, data: { title: 'Shift Change Way Points Add/Edit' } },
  { path: 'view', component: ShiftChangeWayPointViewComponent, data: { title: 'Shift Change Way Points View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ShiftChangeWayPointRoutingModule { }
