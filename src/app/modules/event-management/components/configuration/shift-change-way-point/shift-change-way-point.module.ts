import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { GMapModule } from 'primeng/gmap';
import { ShiftChangeWayPointAddEditComponent } from './shift-change-way-point-add-edit.component';
import { ShiftChangeWayPointRoutingModule } from './shift-change-way-point-routing.module';
import { ShiftChangeWayPointViewComponent } from './shift-change-way-point-view.component';


@NgModule({
  declarations: [ShiftChangeWayPointAddEditComponent, ShiftChangeWayPointViewComponent],
  imports: [
    CommonModule,
    ShiftChangeWayPointRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    GMapModule
  ]
})
export class ShiftChangeWayPointModule { }
