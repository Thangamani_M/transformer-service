import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { RoConfigurationListComponent } from './ro-configuration-list/ro-configuration-list.component';
import { RoConfigurationRoutingModule } from './ro-configuration-routing.module';

@NgModule({
  declarations: [RoConfigurationListComponent],
  imports: [
    CommonModule,
    RoConfigurationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTabsModule
  ]
})
export class RoConfigurationModule { }
