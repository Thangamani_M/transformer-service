import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { NamedStackMappingModel } from '@modules/event-management/models/configurations/named-stack-mapping.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-named-stack-mapping-add-edit',
  templateUrl: './named-stack-mapping-add-edit.component.html',
})
export class NamedStackMappingAddEditComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};
  namedStackMappingForm: FormGroup;
  loggedUser: UserLogin;
  namedStackDropDown: any;
  namedStackId: any;
  namedStackMappingDetails: any;
  namedStackMappingsList: any = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  totaltab = 0;
  selectedTab = 0;
  title:string;

  constructor(private rxjsService: RxjsService,private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private snakBarService: SnackbarService, private momentService: MomentService, private _fb: FormBuilder, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private router: Router) {
    super();
    this.namedStackId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.namedStackId ? 'Update Named Stack Mapping':'Add Named Stack Mapping';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Named Stack Mapping', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 3 } }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'customerNumber', header: 'Customer Id', width: '150px' },
            { field: 'displayName', header: 'Customer Name', width: '120px' },
            { field: 'fullAddress', header: 'Address', width: '150px' },
            { field: 'contactNumber', header: 'Mobile Number', width: '150px' },
            { field: 'customerTypeName', header: 'Customer Type', width: '120px' },
            { field: 'namedStack', header: 'Named Stack', width: '100px' },
            { field: 'email', header: 'Email', width: '120px' },
            { field: 'createdDate', header: 'Created On', width: '120px', isDate:true },],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.NAMED_STACK_CUSTOMER_ADDRESS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
    if(this.namedStackId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/named-stack-mapping/view' , queryParams: {id: this.namedStackId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createNamedStackMappingForm();
    this.getNamedStacks();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.namedStackId) {
      this.getalarmGroupDetailsById(this.namedStackId);
    } else {
      this.getSignalHistoryList();
    }
  }

  createNamedStackMappingForm(): void {
    let namedStackMappingModel = new NamedStackMappingModel();
    // create form controls dynamically from model class
    this.namedStackMappingForm = this._fb.group({});
    Object.keys(namedStackMappingModel).forEach((key) => {
      this.namedStackMappingForm.addControl(key, new FormControl(namedStackMappingModel[key]));
    });
    this.namedStackMappingForm = setRequiredValidator(this.namedStackMappingForm, ["namedStackConfigId", "description"]);
    this.namedStackMappingForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.namedStackMappingForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getNamedStacks() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_NAMED_STACK_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.namedStackDropDown = response.resources;
        }
      });
  }

  getalarmGroupDetailsById(namedStackId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.NAMED_STACK, namedStackId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.namedStackMappingDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.namedStackMappingDetails?.namedStackConfigName ? this.title +' - '+ this.namedStackMappingDetails?.namedStackConfigName : this.title + ' --', relativeRouterUrl: '' });
          this.namedStackMappingForm.patchValue(response.resources)
          this.getSignalHistoryList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getSignalHistoryList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          let uniqueNamedMappingArr= [...new Map(this.namedStackMappingDetails?.namedStackCustomerList.map(item => [item['customerId'], item])).values()]
          uniqueNamedMappingArr.push(...this.selectedRowsAll)
          if (this.namedStackId) {
            this.dataList.forEach((row: any) => {
              uniqueNamedMappingArr.forEach((element:any) => {
                if (row.customerId == element.customerId) {
                  // this.selectedRows.push(row)
                  this.selectedRows = [...this.selectedRows, row]
                  this.selectedRowsAll.push(row)
                }
              });
            });
          }

          else {
            this.dataList.forEach(element => {
              if (this.namedStackId) {
                this.dataList.forEach((row: any) => {
                  uniqueNamedMappingArr.forEach((element:any) => {
                    if (row.id == element.namedStackConfigId) {
                      let existDate = this.selectedRows.find(x => x.id == row.id)
                      if (!existDate) {
                        this.selectedRows = [...this.selectedRows, row]
                        this.selectedRowsAll.push(...this.selectedRows,row)
                      }
                    }
                  });
                });
              }
            });
          }

          this.dataList = data.resources;
          this.totalRecords = data.totalCount;

        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  selectedRowsAll = []

  filterUnSelectedArray(mainArray, selectedArray) {
    return mainArray.filter(mainObj => {
      return selectedArray.every(selectedObj => {
        return mainObj.customerId != selectedObj.customerId;
      });
    });
  }

  onChangeSelecedRows(event) {
    this.selectedRowsAll.push(...event)
    this.selectedRowsAll = [...new Map(this.selectedRowsAll.map(item => [item['customerId'], item])).values()]
    let unSelected = this.filterUnSelectedArray(this.dataList, event)
    this.selectedRowsAll= this.filterUnSelectedArray(this.selectedRowsAll, unSelected)
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  onSubmit(): void {
    if (this.namedStackMappingForm.invalid) {
      return;
    }
    this.namedStackMappingsList = [];
    this.selectedRowsAll.forEach(item => {
      this.namedStackMappingsList.push({
        customerId: item.customerId,
        addressId: item.customerAddressId ? item.customerAddressId : '',
      });
    });
    this.namedStackMappingForm.get('namedStackCustomerList').setValue(this.namedStackMappingsList)
    if (this.namedStackMappingForm.value.namedStackCustomerList.length == 0) {
      this.snakBarService.openSnackbar('Select a Customer to Map with Named Stack Name', ResponseMessageTypes.WARNING);
      return
    }
    if(!this.namedStackId){
      delete this.namedStackMappingForm.value.namedStackId
      delete this.namedStackMappingForm.value.isEnabled
    }
    let formValue = this.namedStackMappingForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.namedStackId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.NAMED_STACK, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.NAMED_STACK, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=3');
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
