import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { NamedStackMappingAddEditComponent } from './named-stack-mapping-add-edit.component';
import { NamedStackMappingRoutingModule } from './named-stack-mapping-routing.module';
import { NamedStackMappingViewComponent } from './named-stack-mapping-view.component';

@NgModule({
  declarations: [NamedStackMappingAddEditComponent, NamedStackMappingViewComponent],
  imports: [
    CommonModule,
    NamedStackMappingRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class NamedStackMappingModule { }
