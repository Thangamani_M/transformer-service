import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NamedStackMappingAddEditComponent } from './named-stack-mapping-add-edit.component';
import { NamedStackMappingViewComponent } from './named-stack-mapping-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: NamedStackMappingAddEditComponent, data: { title: 'Named Stack Mapping Add/Edit' } },
  { path: 'view', component: NamedStackMappingViewComponent, data: { title: 'Named Stack Mapping View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class NamedStackMappingRoutingModule { }
