import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-named-stack-mapping-view',
  templateUrl: './named-stack-mapping-view.component.html',
})
export class NamedStackMappingViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  loggedUser: UserLogin;
  namedStackId: any;
  namedStackMappingDetails: any;
  namedStackMappingId: any;
  namedStackMappingViewDetails:any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  };

  constructor(private rxjsService: RxjsService,private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private momentService: MomentService, private store: Store<AppState>,
    private router: Router) {
    super();
    this.namedStackId = this.activatedRoute.snapshot.queryParams.id;
    this.namedStackMappingId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View Named Stack Mapping',
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Named Stack Mapping', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 3 } }, { displayName: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableViewBtn:true,
            columns: [{ field: 'customerNumber', header: 'Customer Id', width: '80px', },
            { field: 'displayName', header: 'Customer Name', width: '120px' },
            { field: 'fullAddress', header: 'Address', width: '80px' },
            { field: 'contactNumber', header: 'Mobile Number', width: '80px' },
            { field: 'customerTypeName', header: 'Customer Type', width: '120px' },
            { field: 'namedStack', header: 'Named Stack', width: '80px' },
            { field: 'email', header: 'Email', width: '120px' },
            { field: 'createdDate', header: 'Created On', width: '120px', isDateTime:true },],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_NAMED_STACK_MAPPED,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData();
    this.getnamedStackMappingDetailsById(this.namedStackMappingId)
  }


  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getnamedStackMappingDetailsById(namedStackMappingId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.NAMED_STACK, namedStackMappingId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.namedStackMappingDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.namedStackMappingDetails?.namedStackConfigName;
          this.onShowValue(response.resources);
          this.row['pageIndex'] = 0
          this.row['pageSize'] = 20
          let otherParams = {}
          this.getSignalHistoryList(this.row['pageIndex'], this.row['pageSize'], otherParams);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.namedStackMappingViewDetails = [
      { name: 'Named Stack Mapping Name', value: response?.namedStackConfigName },
      { name: 'Description', value: response?.description },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Status', value: response?.isActive == true ? 'Active' : 'In Active', statusClass: response?.isActive==true?"status-label-green":'status-label-pink'},
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate ,isDateTime: true},
      { name: 'Modified By', value: response?.modifiedUserName },
    ]
  }


  getSignalHistoryList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize,{
          NamedStackId:this.namedStackId 
        })
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess && data.statusCode === 200) {
          this.dataList = data.resources;

          this.dataList.map(item => {
            item.checkHidden = true
          })
          if (this.namedStackId) {
            this.dataList?.forEach((row: any) => {
              this.namedStackMappingDetails?.namedStackCustomerList?.forEach(element => {
                if (row.customerId == element.customerId) {
                  this.selectedRows.push(row)
                  this.selectedRows = [...this.selectedRows, row]
                  row.checkedValue = row
                }
              });
            });
          }

          else {
              if (this.namedStackId) {
                this.dataList.forEach((row: any) => {
                  this.namedStackMappingDetails.namedStackCustomerList.forEach(element => {
                    if (row.id == element.namedStackConfigId) {
                      let existDate = this.selectedRows.find(x => x.id == row.id)
                      if (!existDate) {
                        this.selectedRows = [...this.selectedRows, row]
                      }
                    }
                  });
                });
              }
          }
          this.totalRecords = data.totalCount
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
       case CrudType.EDIT:
        this.edit();
        break;
    }
  }

  onChangeSelecedRows(event) {
    this.selectedRows = event
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  edit() {
    if (this.namedStackMappingId) {
      if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['event-management/event-configuration/named-stack-mapping/add-edit'], { queryParams: { id: this.namedStackMappingId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
    }
  }

}
