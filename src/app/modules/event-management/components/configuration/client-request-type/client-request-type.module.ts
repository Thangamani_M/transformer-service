import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { ClientRequestTypeAddEditComponent } from './client-request-type-add-edit.component';
import { ClientRequestTypeRoutingModule } from './client-request-type-routing.module';
import { ClientRequestTypeViewComponent } from './client-request-type-view.component';

@NgModule({
  declarations: [ClientRequestTypeAddEditComponent, ClientRequestTypeViewComponent],
  imports: [
    CommonModule,
    ClientRequestTypeRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class ClientRequestTypeModule { }
