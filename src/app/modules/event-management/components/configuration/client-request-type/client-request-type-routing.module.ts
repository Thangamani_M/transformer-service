import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientRequestTypeAddEditComponent } from './client-request-type-add-edit.component';
import { ClientRequestTypeViewComponent } from './client-request-type-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ClientRequestTypeAddEditComponent, data: { title: 'Client Request Type Type Add/Edit' } },
  { path: 'view', component: ClientRequestTypeViewComponent, data: { title: 'Client Request Type Type View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ClientRequestTypeRoutingModule { }
