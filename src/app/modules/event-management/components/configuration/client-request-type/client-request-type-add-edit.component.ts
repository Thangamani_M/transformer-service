import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { ClientRequestTypeModel } from '@modules/event-management/models/configurations/client-request-type-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-client-request-type-add-edit',
  templateUrl: './client-request-type-add-edit.component.html',
})
export class ClientRequestTypeAddEditComponent implements OnInit {

  clientRequestTypeId: any;
  clientRequestTypeDetails: any;
  clientRequestTypeForm: FormGroup;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.clientRequestTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.clientRequestTypeId ? 'Update Client Request Type':'Add Client Request Type';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Client Request Type', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 25 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.clientRequestTypeId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/client-request-type/view' , queryParams: {id: this.clientRequestTypeId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createClientRequestTypeForm();

    if (this.clientRequestTypeId) {
      this.getClientRequestTypeDetailsById(this.clientRequestTypeId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createClientRequestTypeForm(): void {
    let clientRequestTypeModel = new ClientRequestTypeModel();
    this.clientRequestTypeForm = this.formBuilder.group({});
    Object.keys(clientRequestTypeModel).forEach((key) => {
      this.clientRequestTypeForm.addControl(key, new FormControl(clientRequestTypeModel[key]));
    });
    this.clientRequestTypeForm = setRequiredValidator(this.clientRequestTypeForm, ["clientRequestTypeName", "description"]);
    this.clientRequestTypeForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.clientRequestTypeForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.clientRequestTypeId) {
      this.clientRequestTypeForm.removeControl('clientRequestTypeId');
      this.clientRequestTypeForm.removeControl('modifiedUserId');
    } else {
      this.clientRequestTypeForm.removeControl('createdUserId');
    }
  }

  getClientRequestTypeDetailsById(clientRequestTypeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLIENT_REQUEST_TYPE, clientRequestTypeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.clientRequestTypeDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.clientRequestTypeDetails?.clientRequestTypeName ? this.title +' - '+ this.clientRequestTypeDetails?.clientRequestTypeName : this.title + ' --', relativeRouterUrl: '' });
          this.clientRequestTypeForm.patchValue(this.clientRequestTypeDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.clientRequestTypeForm.invalid) {
      return;
    }
    let formValue = this.clientRequestTypeForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.clientRequestTypeId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLIENT_REQUEST_TYPE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLIENT_REQUEST_TYPE, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=25');
      }
    })
  }

  onCRUDRequested(type){}

}
