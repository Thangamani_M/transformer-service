import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { PdaRegistrationAddEditComponent } from './pda-registration-add-edit.component';
import { PdaRegistrationRoutingModule } from './pda-registration-routing.module';
import { PdaRegistrationViewComponent } from './pda-registration-view.component';

@NgModule({
  declarations: [PdaRegistrationViewComponent, PdaRegistrationAddEditComponent],
  imports: [
    CommonModule,
    PdaRegistrationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PdaRegistrationModule { }
