import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { PdaRegistrationModel } from '@modules/event-management/models/configurations/pda-registration.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-pda-registration-add-edit',
  templateUrl: './pda-registration-add-edit.component.html'
})
export class PdaRegistrationAddEditComponent implements OnInit {

  pdaRegistrationId: string;
  pdaRegistrationDetails: any;
  pttDropDown: any = [];
  pdaDropDown: any = [];
  pdaRegistrarionForm: FormGroup;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,private momentService: MomentService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.pdaRegistrationId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.pdaRegistrationId ? 'Update PDA Registration':'Add PDA Registration';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'PDA Registration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 7 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.pdaRegistrationId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/pda-registration/view' , queryParams: {id: this.pdaRegistrationId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createPdaRegistrationForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.pdaRegistrationId) {
      this.getPdaRegistrationDetailsById(this.pdaRegistrationId);
      return
    }

  }

  createPdaRegistrationForm(): void {
    let pdaRegistrationModel = new PdaRegistrationModel();
    // create form controls dynamically from model class
    this.pdaRegistrarionForm = this.formBuilder.group({});
    Object.keys(pdaRegistrationModel).forEach((key) => {
      this.pdaRegistrarionForm.addControl(key, new FormControl(pdaRegistrationModel[key]));
    });
    this.pdaRegistrarionForm = setRequiredValidator(this.pdaRegistrarionForm, ["pdaid", "imeiNumber", "simCardNumber"]);
    this.pdaRegistrarionForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.pdaRegistrarionForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getPdaRegistrationDetailsById(pdaRegistrationId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PDA_REGISTRATION, pdaRegistrationId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          response.resources.contractDate = response.resources.contractDate ? new Date(response.resources.contractDate) : null
          this.pdaRegistrationDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.pdaRegistrationDetails?.pdaid ? this.title +' - '+ this.pdaRegistrationDetails?.pdaid : this.title + ' --', relativeRouterUrl: '' });
          this.pdaRegistrarionForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  
  onSubmit(): void {
    if (this.pdaRegistrarionForm.invalid) {
      return;
    }
    let formValue = this.pdaRegistrarionForm.value;
    formValue.contractDate = this.pdaRegistrarionForm.value.contractDate ? this.momentService.toFormateType(this.pdaRegistrarionForm.value.contractDate,"YYYY-MM-DD") : this.pdaRegistrarionForm.value.contractDate;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.pdaRegistrationId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PDA_REGISTRATION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PDA_REGISTRATION, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=7');
      }
    })
  }

  onCRUDRequested(type){}

}
