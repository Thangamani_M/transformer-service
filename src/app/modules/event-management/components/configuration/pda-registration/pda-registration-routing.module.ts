import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PdaRegistrationAddEditComponent } from './pda-registration-add-edit.component';
import { PdaRegistrationViewComponent } from './pda-registration-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: PdaRegistrationAddEditComponent, data: { title: 'PDA Registration Add/Edit' } },
  { path: 'view', component: PdaRegistrationViewComponent, data: { title: 'PDA Registration View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PdaRegistrationRoutingModule { }
