import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatsConfigAddEditComponent } from './stats-config-add-edit.component';
import { StatsConfigViewComponent } from './stats-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: StatsConfigAddEditComponent, data: { title: 'Stats Config Add/Edit' } },
  { path: 'view', component: StatsConfigViewComponent, data: { title: 'Stats Config View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StatsConfigRoutingModule { }
