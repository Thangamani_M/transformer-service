import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { StatsConfigAddEditComponent } from './stats-config-add-edit.component';
import { StatsConfigRoutingModule } from './stats-config-routing.module';
import { StatsConfigViewComponent } from './stats-config-view.component';



@NgModule({
  declarations: [StatsConfigAddEditComponent, StatsConfigViewComponent],
  imports: [
    CommonModule,
    StatsConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class StatsConfigModule { }
