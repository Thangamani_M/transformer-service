import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { StatsConfigModel } from '../../../models/configurations/stats-config.model';
@Component({
  selector: 'app-stats-config-add-edit',
  templateUrl: './stats-config-add-edit.component.html',
})
export class StatsConfigAddEditComponent implements OnInit {

  statsConfigId: any
  divisionDropDown: any = [];
  alarmTypeDropDown: any = [];
  eventTypeDropDown: any = [];
  statsConfigForm: FormGroup;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  statsConfigDetails: any;
  @ViewChild('allSelectedAlarm', { static: false }) public allSelectedAlarm: MatOption;
  @ViewChild('allSelectedEvent', { static: false }) public allSelectedEvent: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.statsConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.statsConfigId ? 'Update Stats Config':'Add Stats Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Stats Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 23 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.statsConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/stats-config/view' , queryParams: {id: this.statsConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createStatsConfigForm();
    this.getAlarmTypeList();
    this.getEventTypeList();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.statsConfigId) {
      this.getStatsConfigDetailsById(this.statsConfigId);
      this.getAlarmTypeList();
      this.getEventTypeList();
      return
    }
  }

  createStatsConfigForm(): void {
    let statsConfigModel = new StatsConfigModel();
    // create form controls dynamically from model class
    this.statsConfigForm = this.formBuilder.group({});
    Object.keys(statsConfigModel).forEach((key) => {
      this.statsConfigForm.addControl(key, new FormControl(statsConfigModel[key]));
    });
    this.statsConfigForm = setRequiredValidator(this.statsConfigForm, ["groupName", "description", "statsConfigAlarmTypesList", "statsConfigEventTypesList"]);
    this.statsConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.statsConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.statsConfigId) {
      this.statsConfigForm.removeControl('statsConfigId');
      this.statsConfigForm.removeControl('modifiedUserId');
    } else {
      this.statsConfigForm.removeControl('createdUserId');
    }
  }

  getAlarmTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypeDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getEventTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_TYPE_LIST, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getStatsConfigDetailsById(statsConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STATS_CONFIG, statsConfigId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.statsConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.statsConfigDetails?.groupName ? this.title +' - '+ this.statsConfigDetails?.groupName : this.title + ' --', relativeRouterUrl: '' });
          this.statsConfigForm.patchValue(response.resources);
          var statsConfigAlarmTypesList = [];
          var statsConfigEventTypesList = [];
          response.resources.statsConfigAlarmTypesList.forEach(element => {
            statsConfigAlarmTypesList.push(element.alarmTypeId);
          });
          response.resources.statsConfigEventTypesList.forEach(element => {
            statsConfigEventTypesList.push(element.eventTypeId);
          });
          this.statsConfigForm.get('statsConfigAlarmTypesList').setValue(statsConfigAlarmTypesList);
          this.statsConfigForm.get('statsConfigEventTypesList').setValue(statsConfigEventTypesList);

        }

        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleAlarmOne(all) {
    if (this.allSelectedAlarm.selected) {
      this.allSelectedAlarm.deselect();
      return false;
    }
    if (this.statsConfigForm.controls.statsConfigAlarmTypesList.value.length == this.alarmTypeDropDown.length)
      this.allSelectedAlarm.select();
  }

  toggleAllAlarm() {
    if (this.allSelectedAlarm.selected) {
      this.statsConfigForm.controls.statsConfigAlarmTypesList
        .patchValue([...this.alarmTypeDropDown.map(item => item.id), '']);
    } else {
      this.statsConfigForm.controls.statsConfigAlarmTypesList.patchValue([]);
    }
  }

  toggleEventOne(all) {
    if (this.allSelectedEvent.selected) {
      this.allSelectedEvent.deselect();
      return false;
    }
    if (this.statsConfigForm.controls.statsConfigEventTypesList.value.length == this.eventTypeDropDown.length)
      this.allSelectedEvent.select();
  }

  toggleAllEvent() {
    if (this.allSelectedEvent.selected) {
      this.statsConfigForm.controls.statsConfigEventTypesList
        .patchValue([...this.eventTypeDropDown.map(item => item.id), '']);
    } else {
      this.statsConfigForm.controls.statsConfigEventTypesList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (this.statsConfigForm.invalid) {
      return;
    }

    let formValue = this.statsConfigForm.value;
    if (formValue.statsConfigAlarmTypesList[0].hasOwnProperty('alarmTypeId')) {
      formValue = formValue;
    } else {
      formValue.statsConfigAlarmTypesList = formValue.statsConfigAlarmTypesList.filter(item => item != '')
      formValue.statsConfigAlarmTypesList = formValue.statsConfigAlarmTypesList.map(alarmTypeId => ({ alarmTypeId }))
    }

    if (formValue.statsConfigEventTypesList[0].hasOwnProperty('eventTypeId')) {
      formValue = formValue;
    } else {
      formValue.statsConfigEventTypesList = formValue.statsConfigEventTypesList.filter(item => item != '')
      formValue.statsConfigEventTypesList = formValue.statsConfigEventTypesList.map(eventTypeId => ({ eventTypeId }))
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.statsConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STATS_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STATS_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=23');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onCRUDRequested(type){}

}