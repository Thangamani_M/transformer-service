import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-vas-config-view',
  templateUrl: './vas-config-view.component.html',
})
export class VasConfigViewComponent implements OnInit {

  vasConfigId: any
  vasConfigViewDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any
  vasConfigDetails: any;
  alarmTypesDropDown: any;
  timeDelayDropDown: any;
  vASConfigTimeDelaysList: any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private activatedRoute: ActivatedRoute,
     private router: Router,
      private rxjsService: RxjsService,
       private crudService: CrudService,
       private store: Store<AppState>,
       private snackbarService: SnackbarService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.vasConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.primengTableConfigProperties = {
      tableCaption: 'View VAS Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'VAS Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 22 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    }
    this.combineLatestNgrxStoreData();
    if (this.vasConfigId) {
      this.getvasConfigViewDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.onShowValue(response.resources);
        this.vasConfigViewDetails = response.resources.vASConfigAlarmTypesList;
        this.vASConfigTimeDelaysList = response.resources.vASConfigTimeDelaysList;
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + response.resources.groupName;
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
    this.getAlarmTypeDropDownById();
    this.getTimeDelayDropDownById();
  }

  getAlarmTypeDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypesDropDown = response.resources;

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTimeDelayDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VAS_CONFIG_DAY, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.timeDelayDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onShowValue(response?: any) {
    this.vasConfigDetails = [
      { name: 'Group Name', value: response?.groupName },
      { name: 'Feedback Delay(Sec)', value: response?.feedbackDelay },
      { name: 'Division', value: response?.vasConfigDivisions },
      { name: 'Status', value: response?.isActive == true ? 'Active' : 'In-Active', statusClass: response?.isActive == true ? "status-label-green" : 'status-label-red' },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Modified By', value: response?.modifiedUserName },

    ]
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['event-management/event-configuration/vas-config/add-edit'], { queryParams: { id: this.vasConfigId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
  }

  getvasConfigViewDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.VAS_CONFIG,
      this.vasConfigId
    );
  }

}
