import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { VasConfigAddEditComponent } from './vas-config-add-edit.component';
import { VasConfigRoutingModule } from './vas-config-routing.module';
import { VasConfigViewComponent } from './vas-config-view.component';


@NgModule({
  declarations: [VasConfigAddEditComponent, VasConfigViewComponent],
  imports: [
    CommonModule,
    VasConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class VasConfigModule { }
