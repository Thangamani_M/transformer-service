import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { MomentService } from '@app/shared/services/moment.service';
import { VASConfigAlarmTypesListModel, VASConfigModel, VASConfigTimeDelaysListModel } from '@modules/event-management/models/configurations/vas-config.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-vas-config-add-edit',
  templateUrl: './vas-config-add-edit.component.html',
})
export class VasConfigAddEditComponent implements OnInit {

  vasConfigId: any
  divisionDropDown: any = [];
  alarmTypesDropDown: any = [];
  timeDelayDropDown: any = [];
  vasForm: FormGroup;
  vASConfigAlarmTypesList: FormArray;
  vASConfigTimeDelaysList: FormArray;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  incidentDetails: any;
  isDuplicate: boolean;
  isDuplicate1: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChildren('input1') rows1: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    public momentService: MomentService,
    private snakBarService: SnackbarService,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.vasConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.vasConfigId ? 'Update VAS Config':'Add VAS Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'VAS Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 22 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.vasConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/vas-config/view' , queryParams: {id: this.vasConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createvasForm();
    this.getDivisionDropDownById();
    this.getAlarmTypeDropDownById();
    this.getTimeDelayDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.vasConfigId) {
      this.getVASDetailsById().subscribe((response: IApplicationResponse) => {
        let vASConfigModel = new VASConfigModel(response.resources);
        this.incidentDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.incidentDetails?.groupName ? this.title +' - '+ this.incidentDetails?.groupName : this.title + ' --', relativeRouterUrl: '' });
        this.vasForm.patchValue(vASConfigModel);
        var vASConfigDivisionsList = [];
        response.resources.vASConfigDivisionsList.forEach(element => {
          vASConfigDivisionsList.push(element.divisionId);
        });
        this.vasForm.get('vASConfigDivisionsList').setValue(vASConfigDivisionsList)
        this.vASConfigAlarmTypesList = this.getvASConfigAlarmTypesListArray;
        if (response.resources.vASConfigAlarmTypesList.length > 0) {
          response.resources.vASConfigAlarmTypesList.forEach((vASConfigAlarmTypesListModel) => {
            vASConfigAlarmTypesListModel.vasConfigAlarmTypeIdDummy = vASConfigAlarmTypesListModel.vasConfigAlarmTypeId;
            this.vASConfigAlarmTypesList.push(this.createvASConfigAlarmTypesListModel(vASConfigAlarmTypesListModel));
          });
        } else {
          this.vASConfigAlarmTypesList.push(this.createvASConfigAlarmTypesListModel());
        }
        this.vASConfigTimeDelaysList = this.getvASConfigTimeDelaysListArray;
        if (response.resources.vASConfigTimeDelaysList.length > 0) {
          response.resources.vASConfigTimeDelaysList.forEach((vASConfigTimeDelaysListModel) => {
            vASConfigTimeDelaysListModel.vasConfigTimeDelayIdDummy = vASConfigTimeDelaysListModel.vasConfigTimeDelayId;
            vASConfigTimeDelaysListModel.startTime = this.formateTime(vASConfigTimeDelaysListModel.startTime)
            vASConfigTimeDelaysListModel.endTime = this.formateTime(vASConfigTimeDelaysListModel.endTime)
            this.vASConfigTimeDelaysList.push(this.createvASConfigTimeDelaysListModel(vASConfigTimeDelaysListModel));
          });
        } else {
          this.vASConfigTimeDelaysList.push(this.createvASConfigTimeDelaysListModel());
        }
      })
    } else {
      this.vASConfigAlarmTypesList = this.getvASConfigAlarmTypesListArray;
      this.vASConfigAlarmTypesList.push(this.createvASConfigAlarmTypesListModel());
      this.vASConfigTimeDelaysList = this.getvASConfigTimeDelaysListArray;
      this.vASConfigTimeDelaysList.push(this.createvASConfigTimeDelaysListModel());
    }
  }
  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date)
  }
  formateDateToTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    return timeToRailway;
  }

  OnChange(index): boolean {
    return;
  }

  createvasForm(): void {
    let vASConfigModel = new VASConfigModel();
    this.vasForm = this.formBuilder.group({
      feedbackDelay: [null, [Validators.required, Validators.min(100), Validators.max(99999)]],
      vASConfigAlarmTypesList: this.formBuilder.array([]),
      vASConfigTimeDelaysList: this.formBuilder.array([])
    });
    Object.keys(vASConfigModel).forEach((key) => {
      this.vasForm.addControl(key, new FormControl(vASConfigModel[key]));
    });
    this.vasForm = setRequiredValidator(this.vasForm, ["groupName", "vASConfigAlarmTypesList", "vASConfigTimeDelaysList", "vASConfigDivisionsList"]);
    this.vasForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.vasForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAlarmTypeDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypesDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getTimeDelayDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VAS_CONFIG_DAY, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.timeDelayDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  ifEmpty(val) {
    if (!val)
      this.snakBarService.openSnackbar('Select Start Time', ResponseMessageTypes.WARNING);
    return
  }

  todayEndTime(dateTime) {
    if (!dateTime) return
    let date = new Date(dateTime).setHours(23, 59)
    return new Date(date)
  }

  //Create FormArray
  get getvASConfigAlarmTypesListArray(): FormArray {
    if (!this.vasForm) return;
    return this.vasForm.get("vASConfigAlarmTypesList") as FormArray;
  }

  //Create FormArray
  get getvASConfigTimeDelaysListArray(): FormArray {
    if (!this.vasForm) return;
    return this.vasForm.get("vASConfigTimeDelaysList") as FormArray;
  }

  //Create FormArray controls
  createvASConfigAlarmTypesListModel(vASConfigAlarmTypesListModel?: VASConfigAlarmTypesListModel): FormGroup {
    let vASConfigAlarmTypesListFormControlModel = new VASConfigAlarmTypesListModel(vASConfigAlarmTypesListModel);
    let formControls = {};
    Object.keys(vASConfigAlarmTypesListFormControlModel).forEach((key) => {
      if (key === 'alarmTypeId' || key === 'alarmDescription') {
        formControls[key] = [{ value: vASConfigAlarmTypesListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.vasConfigId) {
        formControls[key] = [{ value: vASConfigAlarmTypesListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createvASConfigTimeDelaysListModel(vASConfigTimeDelaysListModel?: VASConfigTimeDelaysListModel): FormGroup {
    let vASConfigTimeDelaysListFormControlModel = new VASConfigTimeDelaysListModel(vASConfigTimeDelaysListModel);
    let formControls = {};
    Object.keys(vASConfigTimeDelaysListFormControlModel).forEach((key) => {
      if (key === 'vasConfigDayId' || key === 'startTime' || key === 'endTime') {
        formControls[key] = [{ value: vASConfigTimeDelaysListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (key === 'delayTime') {
        formControls[key] = [{ value: vASConfigTimeDelaysListFormControlModel[key], disabled: false }, [Validators.required, Validators.min(1), Validators.max(99999)]]
      } else if (this.vasConfigId) {
        formControls[key] = [{ value: vASConfigTimeDelaysListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getVASDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.VAS_CONFIG,
      this.vasConfigId
    );
  }

  OnChangeAlarmType(event, index) {
    if (this.getvASConfigAlarmTypesListArray.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.getvASConfigAlarmTypesListArray.value.slice();
      cloneArray.splice(index, 1);
      var findIndex = cloneArray.some(x => x.alarmTypeId == this.getvASConfigAlarmTypesListArray.value[index].alarmTypeId);
      if (findIndex) {
        this.snackbarService.openSnackbar("Alarm type already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        this.getvASConfigAlarmTypesListArray.controls[index].get('alarmDescription').setValue('')
        return false;
      } else {
        this.isDuplicate = false;
        let description = this.alarmTypesDropDown.find(x => x.alarmTypeId == event.target.value);
        this.getvASConfigAlarmTypesListArray.controls[index].get('alarmDescription').setValue(description.alarmTypeDescription)
      }

    }
  }

  OnChangeDay(index) {
    if (this.getvASConfigTimeDelaysListArray.length > 1) {
      this.isDuplicate1 = false;
      var cloneArray = this.getvASConfigTimeDelaysListArray.value.slice();
      cloneArray.splice(index, 1);
      var findIndex = cloneArray.some(x => x.vasConfigDayId == this.getvASConfigTimeDelaysListArray.value[index].vasConfigDayId);
      if (findIndex) {
        this.snackbarService.openSnackbar("Days of week already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate1 = true;
        return false;
      } else {
        this.isDuplicate1 = false;
      }

    }
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  focusInAndOutFormArrayFields1(): void {
    this.rows1.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Add Employee Details
  addTimeDelay(): void {
    if (this.getvASConfigTimeDelaysListArray.invalid) {
      this.focusInAndOutFormArrayFields1();
      return;
    };
    if (this.isDuplicate1) {
      this.snackbarService.openSnackbar("Time Delay already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.vASConfigTimeDelaysList = this.getvASConfigTimeDelaysListArray;
    let vASConfigTimeDelaysListModel = new VASConfigTimeDelaysListModel();
    this.vASConfigTimeDelaysList.insert(0, this.createvASConfigTimeDelaysListModel(vASConfigTimeDelaysListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeTimeDelay(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
    onClose?.subscribe(dialogResult => {
      if (dialogResult) {
        if (this.getvASConfigTimeDelaysListArray.length === 1) {
          this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
          return
        }
          if (this.getvASConfigTimeDelaysListArray.controls[i].value.vasConfigTimeDelayIdDummy && this.getvASConfigTimeDelaysListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VAS_CONFIG_TIME_DELAY,
          this.getvASConfigTimeDelaysListArray.controls[i].value.vasConfigTimeDelayIdDummy).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getvASConfigTimeDelaysListArray.removeAt(i);
            }
            if (this.getvASConfigTimeDelaysListArray.length === 0) {
              this.addTimeDelay();
            };
          });
      }
        else {
          this.getvASConfigTimeDelaysListArray.removeAt(i);
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
      }
    });
  }

  //Add Employee Details
  addAlarmTypes(): void {
    if (this.getvASConfigAlarmTypesListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Alarm Type already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.vASConfigAlarmTypesList = this.getvASConfigAlarmTypesListArray;
    let vASConfigAlarmTypesListModel = new VASConfigAlarmTypesListModel();
    this.vASConfigAlarmTypesList.insert(0, this.createvASConfigAlarmTypesListModel(vASConfigAlarmTypesListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeAlarmTypes(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getvASConfigAlarmTypesListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getvASConfigAlarmTypesListArray.controls[i].value.vasConfigAlarmTypeIdDummy && this.getvASConfigAlarmTypesListArray.length > 1) {
                this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VAS_CONFIG_ALARM_TYPE,
                  this.getvASConfigAlarmTypesListArray.controls[i].value.vasConfigAlarmTypeIdDummy).subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess) {
                      this.getvASConfigAlarmTypesListArray.removeAt(i);
                    }
                    if (this.getvASConfigAlarmTypesListArray.length === 0) {
                      this.addAlarmTypes();
                    };
                  });
              }
          else {
            this.getvASConfigAlarmTypesListArray.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
  }

  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.vasForm.controls.vASConfigDivisionsList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions(e?) {
    if (this.allSelectedDivision.selected) {
      this.vasForm.controls.vASConfigDivisionsList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.vasForm.controls.vASConfigDivisionsList.patchValue([]);
    }
  }

  onSubmit(): void {
    if (this.vasForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Alarm type already exist", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.isDuplicate1) {
      this.snackbarService.openSnackbar("Days of week already exist", ResponseMessageTypes.WARNING);
      return;
    }
    let formValue = this.vasForm.value;
    formValue.vASConfigTimeDelaysList.forEach(element => {
      element.startTime = this.formateDateToTime(element.startTime)
      element.endTime = this.formateDateToTime(element.endTime)
    })
    if (formValue.vASConfigDivisionsList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.vASConfigDivisionsList = formValue.vASConfigDivisionsList.filter(item => item != '')
      formValue.vASConfigDivisionsList = formValue.vASConfigDivisionsList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.vasConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VAS_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VAS_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=22');
      }
    })
  }

  onCRUDRequested(type){}

}
