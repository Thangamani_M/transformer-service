import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VasConfigAddEditComponent } from './vas-config-add-edit.component';
import { VasConfigViewComponent } from './vas-config-view.component';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: VasConfigAddEditComponent, data: { title: 'VAS Add/Edit' } },
  { path: 'view', component: VasConfigViewComponent, data: { title: 'VAS View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class VasConfigRoutingModule { }
