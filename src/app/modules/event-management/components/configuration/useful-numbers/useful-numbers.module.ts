import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { UsefulNumbersAddEditComponent } from './useful-numbers-add-edit.component';
import { UsefulNumbersRoutingModule } from './useful-numbers-routing.module';
import { UsefulNumbersViewComponent } from './useful-numbers-view.component';



@NgModule({
  declarations: [UsefulNumbersAddEditComponent, UsefulNumbersViewComponent],
  imports: [
    CommonModule,
    UsefulNumbersRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class UsefulNumbersModule { }
