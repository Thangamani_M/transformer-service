import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsefulNumbersAddEditComponent } from './useful-numbers-add-edit.component';
import { UsefulNumbersViewComponent } from './useful-numbers-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: UsefulNumbersAddEditComponent, data: { title: 'Useful Numbers Add/Edit' } },
  { path: 'view', component: UsefulNumbersViewComponent, data: { title: 'Useful Numbers View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class UsefulNumbersRoutingModule { }
