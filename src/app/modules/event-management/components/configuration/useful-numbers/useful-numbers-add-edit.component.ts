import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD,countryCodes, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { UsefulNumberContactListModel, UsefulNumbersModel } from '@modules/event-management/models/configurations/useful-numbers-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-useful-numbers-add-edit',
  templateUrl: './useful-numbers-add-edit.component.html',
})
export class UsefulNumbersAddEditComponent implements OnInit {

  usefulNumberId: any;
  mainAreaList = [];
  divisionDropDown: any = [];
  usefulNumbersForm: FormGroup;
  usefulNumberContactList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  usefulNumbersDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  @ViewChild('allSelectedMainArea', { static: false }) public allSelectedMainArea: MatOption;
  countryCodes = countryCodes;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService) {
    this.usefulNumberId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.usefulNumberId ? 'Update Useful Numbers':'Add Useful Numbers';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Useful Numbers', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 18 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.usefulNumberId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/useful-numbers/view' , queryParams: {id: this.usefulNumberId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createUsefulNumbersForm();
    this.getDivisionDropDown();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.usefulNumberId) {
      this.getUsefulNumberDetailsById().subscribe((response: IApplicationResponse) => {
        let usefulNumbers = new UsefulNumbersModel(response.resources);
        this.usefulNumbersDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.usefulNumbersDetails?.groupName ? this.title +' - '+ this.usefulNumbersDetails?.groupName : this.title + ' --', relativeRouterUrl: '' });
        this.usefulNumbersForm.patchValue(usefulNumbers);
        var usefulNumberDivisionList = [];
        var usefulNumberMainAreaList = [];
        response.resources.usefulNumberDivisionList.forEach(element => {
          usefulNumberDivisionList.push(element.divisionId);
        });
        response.resources.usefulNumberMainAreaList.forEach(element => {
          usefulNumberMainAreaList.push(element.mainAreaId);
        });
        this.usefulNumbersForm.get('usefulNumberDivisionList').setValue(usefulNumberDivisionList);
        this.usefulNumbersForm.get('usefulNumberMainAreaList').setValue(usefulNumberMainAreaList);
        let seletedDivision = {
          'divisionId': this.usefulNumbersForm.controls.usefulNumberDivisionList.value
        }
        this.getMainAreaList(seletedDivision);
        this.usefulNumberContactList = this.getUsefulNumberContactListArray;
        response.resources.usefulNumberContactList.forEach((usefulNumberContactListModel: UsefulNumberContactListModel) => {
          usefulNumberContactListModel['usefulNumberContactId'] = usefulNumberContactListModel.usefulNumberContactId;
          let number = usefulNumberContactListModel.alternateContactNumber.toString();
          let number1 = usefulNumberContactListModel.contactNumber.toString();
          usefulNumberContactListModel['alternateContactNumber'] = number.replace(/^(\d{0,2})(\d{0,3})(\d{0,4})/, '$1 $2 $3');
          usefulNumberContactListModel['contactNumber'] = number1.replace(/^(\d{0,2})(\d{0,3})(\d{0,4})/, '$1 $2 $3');
          this.usefulNumberContactList.push(this.createUsefulNumbersContactListModel(usefulNumberContactListModel));
        });
      })
    } else {
      this.usefulNumberContactList = this.getUsefulNumberContactListArray;
      this.usefulNumberContactList.push(this.createUsefulNumbersContactListModel());
    }
  }

  createUsefulNumbersForm(): void {
    let usefulNumbersModel = new UsefulNumbersModel();
    this.usefulNumbersForm = this.formBuilder.group({
      usefulNumberContactList: this.formBuilder.array([])
    });
    Object.keys(usefulNumbersModel).forEach((key) => {
      this.usefulNumbersForm.addControl(key, new FormControl(usefulNumbersModel[key]));
    });
    this.usefulNumbersForm = setRequiredValidator(this.usefulNumbersForm, ["groupName", "description", "usefulNumberDivisionList", "usefulNumberMainAreaList"]);
    this.usefulNumbersForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.usefulNumbersForm.get('modifiedUserId').setValue(this.loggedUser.userId)

    this.usefulNumbersForm.get('usefulNumberDivisionList').valueChanges
      .pipe(debounceTime(1500), distinctUntilChanged())
      .subscribe((res: any) => {
        if (res.length == 0) return
        var filtered = res.filter(function (el) { return el; });
        this.getMainAreaList({
          'divisionId': filtered
        });
      })
  }

  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleDivisionOne() {
    if (this.usefulNumbersForm.controls.usefulNumberDivisionList.value.length === 0) {
      this.mainAreaList = [];
    } else {
    }
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.usefulNumbersForm.controls.usefulNumberDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.usefulNumbersForm.controls.usefulNumberDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
      this.rxjsService.setGlobalLoaderProperty(false);

    } else {
      this.usefulNumbersForm.controls.usefulNumberDivisionList.patchValue([]);
    }
  }

  toggleMainAreaOne() {
    if (this.allSelectedMainArea.selected) {
      this.allSelectedMainArea.deselect();
      return false;
    }
    if (this.usefulNumbersForm.controls.usefulNumberMainAreaList.value.length == this.mainAreaList.length)
      this.allSelectedMainArea.select();
  }

  toggleAllMainAreas() {
    if (this.allSelectedMainArea.selected) {
      this.usefulNumbersForm.controls.usefulNumberMainAreaList
        .patchValue([...this.mainAreaList.map(item => item.id), '']);
    } else {
      this.usefulNumbersForm.controls.usefulNumberMainAreaList.patchValue([]);
    }
  }

  //Create FormArray
  get getUsefulNumberContactListArray(): FormArray {
    if (!this.usefulNumbersForm) return;
    return this.usefulNumbersForm.get("usefulNumberContactList") as FormArray;
  }


  //Create FormArray controls
  createUsefulNumbersContactListModel(usefulNumberContactListModel?: UsefulNumberContactListModel): FormGroup {
    let usefulNumbersContactListFormControlModel = new UsefulNumberContactListModel(usefulNumberContactListModel);
    let formControls = {};
    Object.keys(usefulNumbersContactListFormControlModel).forEach((key) => {
      if (key === 'contactName' || key === 'contactNumber' || key === 'contactNumberCountryCode' || key === 'alternateContactNumberCountryCode' || key === 'alternateContactNumber') {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.usefulNumberId) {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }]
      }
    });
    let forms = this.formBuilder.group(formControls);

    forms.get("contactNumberCountryCode")
      .valueChanges.subscribe((contactNumberCountryCode: string) => {
        switch (contactNumberCountryCode) {
          case "+27":
            forms.get("contactNumber").setValidators([Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength),
            Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),])
            break;
          default:
            forms.get("contactNumber").setValidators([Validators.minLength(
              formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),])
            break;
        }
        setTimeout(() => {
          this.focusInAndOutFormArrayFields();
        });
      });

    forms.get("contactNumber")
      .valueChanges.subscribe((contactNumber: string) => {
        switch (forms.get("contactNumberCountryCode").value) {
          case "+27":
            forms.get("contactNumber").setValidators([Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength),
            Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),])
            break;
          default:
            forms.get("contactNumber").setValidators([Validators.minLength(
              formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),])
            break;
        }
      })

    forms.get("alternateContactNumberCountryCode")
      .valueChanges.subscribe((alternateContactNumberCountryCode: string) => {
        switch (alternateContactNumberCountryCode) {
          case "+27":
            forms.get("alternateContactNumber").setValidators([Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength),
            Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),])
            break;
          default:
            forms.get("alternateContactNumber").setValidators([Validators.minLength(
              formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),])
            break;
        }
        setTimeout(() => {
          this.focusInAndOutFormArrayFields();
        });
      });

    forms.get("alternateContactNumber")
      .valueChanges.subscribe((alternateContactNumber: string) => {
        switch (forms.get("alternateContactNumberCountryCode").value) {
          case "+27":
            forms.get("alternateContactNumber").setValidators([Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength),
            Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),])
            break;
          default:
            forms.get("alternateContactNumber").setValidators([Validators.minLength(
              formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),])
            break;
        }
      })
    return forms

  }

  //Get Details 
  getUsefulNumberDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.USEFUL_NUMBER,
      this.usefulNumberId
    );
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Employee Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getUsefulNumberContactListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.contactName)) {
        duplicate.push(k.value.contactName);
      }
      filterKey.push(k.value.contactName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChangeNumber() {
    const duplicate = this.duplicateNumber();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Mobile Number already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateNumber() {
    const filterKey = [];
    const duplicate = [];
    this.getUsefulNumberContactListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.contactNumber)) {
        duplicate.push(k.value.contactNumber);
      }
      filterKey.push(k.value.contactNumber);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (!this.onChange()) {
      return;
    }

    if (!this.onChangeNumber()) {
      return;
    }
    if (this.getUsefulNumberContactListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.usefulNumberContactList = this.getUsefulNumberContactListArray;
    let usefulNumberContactListModel = new UsefulNumberContactListModel();
    this.usefulNumberContactList.insert(0, this.createUsefulNumbersContactListModel(usefulNumberContactListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeCmcSmsEmployee(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getUsefulNumberContactListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getUsefulNumberContactListArray.controls[i].value.usefulNumberContactId && this.getUsefulNumberContactListArray.length > 1) {
            this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.USEFUL_NUMBER_CONTACT,
              this.getUsefulNumberContactListArray.controls[i].value.usefulNumberContactId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.getUsefulNumberContactListArray.removeAt(i);
                }
                if (this.getUsefulNumberContactListArray.length === 0) {
                  this.addCmcSmsGroupEmployee();
                };
              });
          }
          else {
            this.getUsefulNumberContactListArray.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
  }


  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }
    if (!this.onChangeNumber()) {
      return;
    }
    if (this.usefulNumbersForm.invalid) {
      return;
    }
    let formValue = this.usefulNumbersForm.value;
    formValue.modifiedUserId = this.loggedUser.userId;
    if (formValue.usefulNumberDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.usefulNumberDivisionList = formValue.usefulNumberDivisionList.filter(item => item != '')
      formValue.usefulNumberDivisionList = formValue.usefulNumberDivisionList.map(divisionId => ({ divisionId }))
    }
    if (formValue.usefulNumberMainAreaList[0].hasOwnProperty('mainAreaId')) {
      formValue = formValue;
    } else {
      formValue.usefulNumberMainAreaList = formValue.usefulNumberMainAreaList.filter(item => item != '')
      formValue.usefulNumberMainAreaList = formValue.usefulNumberMainAreaList.map(mainAreaId => ({ mainAreaId }))
    }
    formValue.usefulNumberContactList.forEach(element => {
      element.contactNumber = element.contactNumber.replace(/[^\d]/g, '');
      element.alternateContactNumber = element.alternateContactNumber.replace(/[^\d]/g, '');
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.usefulNumberId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.USEFUL_NUMBER, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.USEFUL_NUMBER, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=18');
      }
    })
  }

  onCRUDRequested(type){}

}