import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-useful-numbers-view',
  templateUrl: './useful-numbers-view.component.html',
})
export class UsefulNumbersViewComponent implements OnInit {
  usefulNumberId: any
  usefulNumberDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any
  usefulNumberDetailsViewDetails: any
  usefulNumberContactList: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.usefulNumberId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View Useful Numbers',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Useful Numbers', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 18 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    if (this.usefulNumberId) {
      this.getusefulNumberDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.usefulNumberDetails = response.resources;
        this.usefulNumberContactList = response.resources.usefulNumberContactList
        this.onShowValue(response.resources);
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + response.resources.groupName;
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  getusefulNumberDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.USEFUL_NUMBER,
      this.usefulNumberId
    );
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onShowValue(response?: any) {
    this.usefulNumberDetailsViewDetails = [
      { name: 'Group Name', value: response?.groupName },
      { name: 'Description', value: response?.description },
      { name: 'Division', value: response?.usefulNumberDivisions },
      { name: 'Main Area', value: response?.usefulNumberMainAreas },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Modified By', value: response?.modifiedUserName },
      { name: 'Status', value: response.isActive == true ? 'Active' : 'In-Active', statusClass: response.isActive == true ? "status-label-green" : 'status-label-red' },
    ]
  }

  onEditButtonClicked(): void {
    this.router.navigate(['event-management/ro-configuration/useful-numbers/add-edit'], { queryParams: { id: this.usefulNumberId } });
  }
}