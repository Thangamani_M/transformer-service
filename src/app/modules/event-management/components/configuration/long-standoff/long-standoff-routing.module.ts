import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LongStandoffAddEditComponent } from './long-standoff-add-edit.component';
import { LongStandoffViewComponent } from './long-standoff-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: LongStandoffAddEditComponent, data: { title: 'Long Standoff Add/Edit' } },
  { path: 'view', component: LongStandoffViewComponent, data: { title: 'Long Standoff View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class LongStandoffRoutingModule { }
