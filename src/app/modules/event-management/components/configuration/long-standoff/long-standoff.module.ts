import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { LongStandoffAddEditComponent } from './long-standoff-add-edit.component';
import { LongStandoffRoutingModule } from './long-standoff-routing.module';
import { LongStandoffViewComponent } from './long-standoff-view.component';
@NgModule({
  declarations: [LongStandoffAddEditComponent, LongStandoffViewComponent],
  imports: [
    CommonModule,
    LongStandoffRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LongStandoffModule { }
