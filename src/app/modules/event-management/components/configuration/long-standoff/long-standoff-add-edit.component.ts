import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { LongStandoffMainAreaListModel, LongStandoffModel } from '@modules/event-management/models/configurations/long-standoff-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-long-standoff-add-edit',
  templateUrl: './long-standoff-add-edit.component.html',
})
export class LongStandoffAddEditComponent implements OnInit {
  longStandOffId: any
  divisionDropDown: any = [];
  mainAreaDropDown: any = [];
  selectedDivisionId: any;
  longStandoffConfigForm: FormGroup;
  longStandOffMainAreaList: FormArray;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  longStandoffDetails: any;
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService) {
    this.longStandOffId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.longStandOffId ? 'Update Long Standoff':'Add Long Standoff';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Long Standoff', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 16} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.longStandOffId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/long-standoff/view' , queryParams: {id: this.longStandOffId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createLongStandoffConfigForm();
    this.getDivisionDropDownById();
    this.longStandoffConfigForm.get("divisionId").valueChanges.subscribe(data => {
      this.getMainAreaList(data);

    })
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.longStandOffId) {
      this.getLongStandoffDetailsById().subscribe((response: IApplicationResponse) => {
        let longStandoff = new LongStandoffModel(response.resources);
        this.longStandoffDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.longStandoffDetails?.divisionName ? this.title +' - '+ this.longStandoffDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
        this.longStandoffConfigForm.patchValue(longStandoff);
        this.longStandOffMainAreaList = this.getLongStandOffMainAreaListArray;
        response.resources.longStandOffMainAreaList.forEach((longStandoffMainAreaListModel) => {
          longStandoffMainAreaListModel.longStandOffMainAreaId = longStandoffMainAreaListModel.longStandOffMainAreaId;
          this.longStandOffMainAreaList.push(this.createLongStandoffMainAreaListModel(longStandoffMainAreaListModel));
        });
      })
    } else {
      this.longStandOffMainAreaList = this.getLongStandOffMainAreaListArray;
      this.longStandOffMainAreaList.push(this.createLongStandoffMainAreaListModel());
    }
  }

  createLongStandoffConfigForm(): void {
    let longStandoffModel = new LongStandoffModel();
    this.longStandoffConfigForm = this.formBuilder.group({
      longStandOffMainAreaList: this.formBuilder.array([])
    });
    Object.keys(longStandoffModel).forEach((key) => {
      this.longStandoffConfigForm.addControl(key, new FormControl(longStandoffModel[key]));
    });
    this.longStandoffConfigForm = setRequiredValidator(this.longStandoffConfigForm, ["divisionId", "description"]);
    this.longStandoffConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.longStandoffConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSelectDivision() {
    this.isDuplicate = false;
    const arr = <FormArray>this.longStandoffConfigForm.controls.longStandOffMainAreaList;
    arr.controls = [];
    this.longStandOffMainAreaList = this.getLongStandOffMainAreaListArray;
    this.longStandOffMainAreaList.push(this.createLongStandoffMainAreaListModel());
  }
  //Create FormArray
  get getLongStandOffMainAreaListArray(): FormArray {
    if (!this.longStandoffConfigForm) return;
    return this.longStandoffConfigForm.get("longStandOffMainAreaList") as FormArray;
  }


  //Create FormArray controls
  createLongStandoffMainAreaListModel(longStandoffMainAreaListModel?: LongStandoffMainAreaListModel): FormGroup {
    let longStandoffMainAreaListFormControlModel = new LongStandoffMainAreaListModel(longStandoffMainAreaListModel);
    let formControls = {};
    Object.keys(longStandoffMainAreaListFormControlModel).forEach((key) => {
      if (key === 'mainAreaId' || key === 'standOffEscalation') {
        formControls[key] = [{ value: longStandoffMainAreaListFormControlModel[key], disabled: false }, [Validators.required]]
        formControls['standOffEscalation'] = [{ value: longStandoffMainAreaListFormControlModel['standOffEscalation'], disabled: false }, [Validators.required, Validators.min(10), Validators.max(9999)]];
      } else if (this.longStandOffId) {
        formControls[key] = [{ value: longStandoffMainAreaListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: longStandoffMainAreaListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getLongStandoffDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.LONG_STANDOFF,
      this.longStandOffId
    );
  }

  OnChange(index): boolean {
    if (this.getLongStandOffMainAreaListArray.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.getLongStandOffMainAreaListArray.value.slice();
      cloneArray.splice(index, 1);
      var findIndex = cloneArray.some(x => x.mainAreaId === this.getLongStandOffMainAreaListArray.value[index].mainAreaId);
      if (findIndex) {
        this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }

    }
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Add Main Area Details
  addClusterCongifSubArea(): void {
    if (this.getLongStandOffMainAreaListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.longStandOffMainAreaList = this.getLongStandOffMainAreaListArray;
    let longStandoffMainAreaListModel = new LongStandoffMainAreaListModel();
    this.longStandOffMainAreaList.insert(0, this.createLongStandoffMainAreaListModel(longStandoffMainAreaListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeConfigSubArea(i: number): void {
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getLongStandOffMainAreaListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getLongStandOffMainAreaListArray.controls[i].value.longStandOffMainAreaId && this.getLongStandOffMainAreaListArray.length > 1) {
            this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.LONG_STANDOFF_MAIN_AREA,
              this.getLongStandOffMainAreaListArray.controls[i].value.longStandOffMainAreaId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.getLongStandOffMainAreaListArray.removeAt(i);
                  this.isDuplicate = false;

                }
                if (this.getLongStandOffMainAreaListArray.length === 0) {
                  this.addClusterCongifSubArea();
                };
              });
          }
          else {
            this.getLongStandOffMainAreaListArray.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
  }

  onSubmit(): void {
    if (this.longStandoffConfigForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
      return;
    }
    let formValue = this.longStandoffConfigForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.longStandOffId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.LONG_STANDOFF, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.LONG_STANDOFF, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=16');
      }
    })
  }

  onCRUDRequested(type){}

}