import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-signal-automation-view',
  templateUrl: './signal-automation-view.component.html',
})
export class SignalAutomationViewComponent implements OnInit {

  signalAutomationId: any
  signalAutomationDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private activatedRoute: ActivatedRoute,private snackbarService: SnackbarService, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,  private store: Store<AppState>) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.signalAutomationId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    });
    this.primengTableConfigProperties = {
      tableCaption: this.signalAutomationId && 'View Signal Automation',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Signal Automation', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 10 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    }
    this.combineLatestNgrxStoreData()
    if (this.signalAutomationId) {
      this.getsignalAutomationDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.signalAutomationDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + this.signalAutomationDetails.signalAutomationName;
        this.signalAutomationDetails = [
          { name: 'Signal Automation', value: response.resources.signalAutomationName },
          { name: 'Division', value: response.resources.signalAutomationDivisions },
          { name: 'Signal Type', value: response.resources.signalAutomationAlarmTypes },
          { name: 'Wait For Restore', value: response.resources.waitForRestore },
          { name: 'Wait For Delivery Confirmation', value: response.resources.waitForDeliveryConfirmation },
          { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
          { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
          { name: 'Created By', value: response.resources.createdUserName },
          { name: 'Modified By', value: response.resources.modifiedUserName },
          { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },

        ];
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  
  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['event-management/event-configuration/signal-automation/add-edit'], { queryParams: { id: this.signalAutomationId ,totalTabs:this.totaltab, selectedTab:this.selectedTab} });
  }

  getsignalAutomationDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.SIGNAL_AUTOMATION,
      this.signalAutomationId
    );
  }

}
