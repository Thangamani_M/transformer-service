import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalAutomationAddEditComponent } from './signal-automation-add-edit.component';
import { SignalAutomationViewComponent } from './signal-automation-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SignalAutomationAddEditComponent, data: { title: 'Signal Automation Add/Edit' } },
  { path: 'view', component: SignalAutomationViewComponent, data: { title: 'Signal Automation View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalAutomationRoutingModule { }
