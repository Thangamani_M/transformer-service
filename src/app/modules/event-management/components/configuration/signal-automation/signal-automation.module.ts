import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { SignalAutomationAddEditComponent } from './signal-automation-add-edit.component';
import { SignalAutomationRoutingModule } from './signal-automation-routing.module';
import { SignalAutomationViewComponent } from './signal-automation-view.component';

@NgModule({
  declarations: [SignalAutomationAddEditComponent, SignalAutomationViewComponent],
  imports: [
    CommonModule,
    SignalAutomationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class SignalAutomationModule { }
