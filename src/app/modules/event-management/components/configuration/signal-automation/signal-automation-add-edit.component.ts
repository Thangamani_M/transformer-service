import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { SignalAutomationModel } from '@modules/event-management/models/configurations/signal-automation-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-signal-automation-add-edit',
  templateUrl: './signal-automation-add-edit.component.html',
})
export class SignalAutomationAddEditComponent implements OnInit {

  signalAutomationId: any;
  signalAutomationDetails: any;
  divisionDropDown: any = [];
  alarmTypeDropdown: any = [];
  loggedUser: any;
  signalAutomationForm: FormGroup;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  @ViewChild('allSelectedSignalType', { static: false }) private allSelectedSignalType: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.signalAutomationId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.signalAutomationId ? 'Update Signal Automation':'Add Signal Automation';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Signal Automation', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 10 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.signalAutomationId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/signal-automation/view' , queryParams: {id: this.signalAutomationId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createSignalAutomationForm();
    this.getDivisionDropDownById();
    this.getAlarmDropDown();
    if (this.signalAutomationId) {

      this.getSignalAutomationDetailsById().subscribe((response: IApplicationResponse) => {
        let signalAutomation = new SignalAutomationModel(response.resources);

        this.signalAutomationDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.signalAutomationDetails?.signalAutomationName ? this.title +' - '+ this.signalAutomationDetails?.signalAutomationName : this.title + ' --', relativeRouterUrl: '' });
        this.signalAutomationForm.patchValue(signalAutomation);

        var divisions = [];
        response.resources.divisions.forEach(element => {
          divisions.push(element.divisionId);
        });
        var alarmTypes = [];
        response.resources.alarmTypes.forEach(element => {
          alarmTypes.push(parseInt(element.alarmTypeId));
        });
        this.signalAutomationForm.get('alarmTypes').setValue(alarmTypes)
        this.signalAutomationForm.get('divisions').setValue(divisions)

      })
    }
  }

  createSignalAutomationForm(): void {
    let signalAutomationModel = new SignalAutomationModel();
    // create form controls dynamically from model class
    this.signalAutomationForm = this.formBuilder.group({
      waitForRestore: [null, [Validators.required, Validators.min(10), Validators.max(99999)]],
      waitForDeliveryConfirmation: [null, [Validators.required, Validators.min(10), Validators.max(99999)]]
    });
    Object.keys(signalAutomationModel).forEach((key) => {
      this.signalAutomationForm.addControl(key, new FormControl(signalAutomationModel[key]));
    });
    this.signalAutomationForm = setRequiredValidator(this.signalAutomationForm, ["signalAutomationName", "divisions", "alarmTypes"]);
    this.signalAutomationForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.signalAutomationForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.signalAutomationId) {
      this.signalAutomationForm.removeControl('signalAutomationId');
      this.signalAutomationForm.removeControl('modifiedUserId');
    } else {
      this.signalAutomationForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAlarmDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypeDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSignalAutomationDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.SIGNAL_AUTOMATION,
      this.signalAutomationId
    );
  }

  toggleDivisionOne() {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.signalAutomationForm.controls.divisions.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.signalAutomationForm.controls.divisions
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.signalAutomationForm.controls.divisions.patchValue([]);
    }
  }

  toggleSignalTypeOne() {
    if (this.allSelectedSignalType.selected) {
      this.allSelectedSignalType.deselect();
      return false;
    }
    if (this.signalAutomationForm.controls.alarmTypes.value.length == this.alarmTypeDropdown.length)
      this.allSelectedSignalType.select();
  }

  toggleAllSignalTypes() {
    if (this.allSelectedSignalType.selected) {
      this.signalAutomationForm.controls.alarmTypes
        .patchValue([...this.alarmTypeDropdown.map(item => item.id), '']);
    } else {
      this.signalAutomationForm.controls.alarmTypes.patchValue([]);
    }
  }
  

  onSubmit(): void {
    if (this.signalAutomationForm.invalid) {
      return;
    }
    let formValue = this.signalAutomationForm.value;
    if (formValue.divisions[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.divisions = formValue.divisions.filter(item => item != '')
      formValue.divisions = formValue.divisions.map(divisionId => ({ divisionId }))
    }

    if (formValue.alarmTypes[0].hasOwnProperty('alarmTypeId')) {
      formValue = formValue;
    } else {
      formValue.alarmTypes = formValue.alarmTypes.filter(item => item != '')
      formValue.alarmTypes = formValue.alarmTypes.map(alarmTypeId => ({ alarmTypeId }))
    }


    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.signalAutomationId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_AUTOMATION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_AUTOMATION, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=10');
      }
    })
  }

  onCRUDRequested(type){}

}
