import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';

@Component({
  selector: 'app-curtesy-patrol-time-view',
  templateUrl: './curtesy-patrol-time-view.component.html',
})
export class CurtesyPatrolTimeViewComponent  {


//   curtesyPatrolTimingId: string;
//   curtesyPatrolTimingDetails: any;
//   constructor(private activatedRoute: ActivatedRoute, private router: Router, public momentService: MomentService, private rxjsService: RxjsService, private crudService: CrudService) {
//     this.curtesyPatrolTimingId = this.activatedRoute.snapshot.queryParams.id
//   }

//   ngOnInit() {
//     this.rxjsService.setGlobalLoaderProperty(false);
//     this.getcurtesyPatrolTimingDetailsById(this.curtesyPatrolTimingId);
//   }

//   getcurtesyPatrolTimingDetailsById(curtesyPatrolTimingId: string) {
//     this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CURTESY_PATROL_TIMING, curtesyPatrolTimingId, false, null)

//       .subscribe((response: IApplicationResponse) => {
//         if (response.resources) {
//           this.curtesyPatrolTimingDetails = response.resources;
//         }
//         this.rxjsService.setGlobalLoaderProperty(false);
//       });
//   }


//   edit() {
//     if (this.curtesyPatrolTimingId) {
//       this.router.navigate(['event-management/event-configuration/curtesy-patrol-timing/add-edit'], { queryParams: { id: this.curtesyPatrolTimingId } });
//     }
//   }
 }

