import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule, MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { CurtesyPatrolTimeAddEditComponent } from './curtesy-patrol-time-add-edit.component';
import { CurtesyPatrolTimeRoutingModule } from './curtesy-patrol-time-routing.module';
import { CurtesyPatrolTimeViewComponent } from './curtesy-patrol-time-view.component';
@NgModule({
  declarations: [CurtesyPatrolTimeAddEditComponent,CurtesyPatrolTimeViewComponent],
  imports: [
    CommonModule,
    CurtesyPatrolTimeRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule,
  ]
})
export class CurtesyPatrolTimeModule { }
