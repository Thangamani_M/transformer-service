import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurtesyPatrolTimeAddEditComponent } from './curtesy-patrol-time-add-edit.component';
import { CurtesyPatrolTimeViewComponent } from './curtesy-patrol-time-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CurtesyPatrolTimeAddEditComponent, data: { title: 'Curtesy Patrol Time Add/Edit' } },
  { path: 'view', component: CurtesyPatrolTimeViewComponent, data: { title: 'Curtesy Patrol Time View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CurtesyPatrolTimeRoutingModule { }
