import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CurtesyPatrolTimeModel } from '@modules/event-management/models/configurations/curtesy-patrol-time.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-curtesy-patrol-time-add-edit',
  templateUrl: './curtesy-patrol-time-add-edit.component.html',
})
export class CurtesyPatrolTimeAddEditComponent  {


//   curtesyPatrolTimingId: any;
//   curtesyPatrolTimingDetails: any;
//   divisionDropDown: any = [];
//   mainAreaDropdown: any = [];
//   loggedUser: any;
//   curtesyPatrolTimingForm: FormGroup;
//   formConfigs = formConfigs;
//   isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
//   isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
//   public isInvalid: boolean = false;
//   @ViewChild('allSelectedMainArea', { static: false }) public allSelectedMainArea: MatOption;


//   constructor(private activatedRoute: ActivatedRoute, private router: Router, private snakBarService: SnackbarService, public momentService: MomentService, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
//     this.curtesyPatrolTimingId = this.activatedRoute.snapshot.queryParams.id;
//     this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
//       if (!userData) return;
//       this.loggedUser = userData;
//     });
//   }

//   ngOnInit(): void {
//     this.createcurtesyPatrolTimingForm();
//     this.getDivisionDropDownById();

//     if (this.curtesyPatrolTimingId) {
//       this.getcurtesyPatrolTimingDetailsById(this.curtesyPatrolTimingId);
//       return
//     }
//     this.rxjsService.setGlobalLoaderProperty(false);
//   }

//   createcurtesyPatrolTimingForm(): void {
//     let curtesyPatrolTimingModel = new CurtesyPatrolTimeModel();
//     // create form controls dynamically from model class
//     this.curtesyPatrolTimingForm = this.formBuilder.group({});
//     Object.keys(curtesyPatrolTimingModel).forEach((key) => {
//       this.curtesyPatrolTimingForm.addControl(key, new FormControl(curtesyPatrolTimingModel[key]));
//     });
//     this.curtesyPatrolTimingForm = setRequiredValidator(this.curtesyPatrolTimingForm, ["courtesyPatrolTimingName", "divisionId", "courtesyPatrolTimingMainAreaList", "fromTime", "toTime"]);
//     this.curtesyPatrolTimingForm.get('createdUserId').setValue(this.loggedUser.userId)
//     this.curtesyPatrolTimingForm.get('modifiedUserId').setValue(this.loggedUser.userId)
//     if (!this.curtesyPatrolTimingId) {
//       this.curtesyPatrolTimingForm.removeControl('curtesyPatrolTimingId');
//       this.curtesyPatrolTimingForm.removeControl('modifiedUserId');
//     } else {
//       this.curtesyPatrolTimingForm.removeControl('createdUserId');
//     }

//   }

//   getDivisionDropDownById() {
//     this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
//       .subscribe((response: IApplicationResponse) => {
//         if (response.resources) {
//           this.divisionDropDown = response.resources;
//         }
//         this.rxjsService.setGlobalLoaderProperty(false);
//       });
//   }

//   getMainAreaDropDown(divisionId) {
//     this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, divisionId, false, null)
//       .subscribe((response: IApplicationResponse) => {
//         if (response.resources) {
//           this.mainAreaDropdown = response.resources;
//           if (!this.curtesyPatrolTimingId) {

//           } else {

//             var mainAreaGroupLists: any = [];
//             this.curtesyPatrolTimingForm.controls.courtesyPatrolTimingMainAreaList.value.forEach(element => {
//               mainAreaGroupLists.push(element.mainAreaId);
//             });
//             this.curtesyPatrolTimingForm.get('courtesyPatrolTimingMainAreaList').setValue(mainAreaGroupLists)

//           }
//         }
//         this.rxjsService.setGlobalLoaderProperty(false);
//       });
//   }

//   onChangeDivision() {
//     this.curtesyPatrolTimingForm.get('courtesyPatrolTimingMainAreaList').setValue([]);
//   }

//   toggleMainAreaOne(all) {
//     if (this.allSelectedMainArea.selected) {
//       this.allSelectedMainArea.deselect();
//       return false;
//     }
//     if (this.curtesyPatrolTimingForm.controls.courtesyPatrolTimingMainAreaList.value.length == this.mainAreaDropdown.length)
//       this.allSelectedMainArea.select();
//   }

//   toggleAllMainAreas() {
//     if (this.allSelectedMainArea.selected) {
//       this.curtesyPatrolTimingForm.controls.courtesyPatrolTimingMainAreaList
//         .patchValue([...this.mainAreaDropdown.map(item => item.id), '']);
//     } else {
//       this.curtesyPatrolTimingForm.controls.courtesyPatrolTimingMainAreaList.patchValue([]);
//     }
//   }


//   getcurtesyPatrolTimingDetailsById(curtesyPatrolTimingId: string) {
//     this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CURTESY_PATROL_TIMING, curtesyPatrolTimingId, false, null)
//       .subscribe((response: IApplicationResponse) => {
//         if (response.resources) {
//           this.curtesyPatrolTimingDetails = response.resources;

//           this.curtesyPatrolTimingDetails.fromTime = this.momentService.convertRailayToNormalTime(this.curtesyPatrolTimingDetails.fromTime);
//           this.curtesyPatrolTimingDetails.toTime = this.momentService.convertRailayToNormalTime(this.curtesyPatrolTimingDetails.toTime);

//           this.getMainAreaDropDown(this.curtesyPatrolTimingDetails.divisionId)
//           this.curtesyPatrolTimingForm.patchValue(this.curtesyPatrolTimingDetails);
//           setTimeout(() => {
//             this.curtesyPatrolTimingForm.patchValue(this.curtesyPatrolTimingDetails);         // To resolve form min value data binding validation issue
//           }, 0);
//         }
//         this.rxjsService.setGlobalLoaderProperty(false);
//       });
//   }

//   getSelectedValue(event) {
//     if (!event) return;
//     // this.toggleAddDays({ checked: this.openCloseMonitoringForm.get('allDays').value })
//     if (!this.curtesyPatrolTimingForm.get('toTime').value) return;
//     var isAfter = this.momentService.timeDiff(event, this.curtesyPatrolTimingForm.get('toTime').value);

//     if (isAfter) {
//       this.curtesyPatrolTimingForm.get('toTime').setValue(null)

//     }

//   }

//   ifEmpty(val) {
//     if (!val)
//       this.snakBarService.openSnackbar('Select Start Time', ResponseMessageTypes.WARNING);
//     return
//   }


//   onSubmit(): void {
//     if (this.curtesyPatrolTimingForm.invalid) {
//       return;
//     }
//     let formValue = this.curtesyPatrolTimingForm.value;
//     formValue.fromTime = this.momentService.convertNormalToRailayTime(formValue.fromTime);
//     formValue.toTime = this.momentService.convertNormalToRailayTime(formValue.toTime);
//     formValue.courtesyPatrolTimingMainAreaList = formValue.courtesyPatrolTimingMainAreaList.filter(item => item != '')
//     formValue.courtesyPatrolTimingMainAreaList = formValue.courtesyPatrolTimingMainAreaList.map(mainAreaId => ({ mainAreaId }))
//     formValue.createdUserId = this.loggedUser.userId;
//     formValue.modifiedUserId = this.loggedUser.userId;
//     this.httpCancelService.cancelPendingRequestsOnFormSubmission();
//     let crudService: Observable<IApplicationResponse> = (!this.curtesyPatrolTimingId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CURTESY_PATROL_TIMING, formValue) :
//       this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CURTESY_PATROL_TIMING, formValue)

//     crudService.subscribe((response: IApplicationResponse) => {
//       if (response.isSuccess) {
//         this.router.navigateByUrl('/event-management/event-configuration?tab=18');
//       }
//     })
//   }
 }
