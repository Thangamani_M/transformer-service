import { CommonModule, DatePipe } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { QuickAddServiceCallDialogModule } from '@modules/customer/components/customer/customer-management/quick-add-service-call-dialog/quick-add-service-call-dialog.module';
import { EventsMgmtSharedModule } from '@modules/event-management/shared/events-mgmt-shared.module';
import { KeyboardShortcutsModule } from 'ng-keyboard-shortcuts';
import { NgxPrintModule } from 'ngx-print';
import { GMapModule } from 'primeng/gmap';
import { StackHistoryModule } from './stack-history.module';
import { StackWinelandsRoutingModule } from './stack-winelands-routing.module';
import { AddMessageOperatorModalComponent } from './stack-winelands/add-message-operator-modal.component';
import { ClassifyIncidentModalComponent } from './stack-winelands/classify-incident-modal.component';
import { StackWinelandsPopupComponent } from './stack-winelands/stack-winelands-popup.component';
import { StackWinelandsComponent } from './stack-winelands/stack-winelands.component';
import { ThirdPartyDetailsModule } from './third-party-details/third-party-details.module';
@NgModule({
  declarations: [StackWinelandsComponent, StackWinelandsPopupComponent, ClassifyIncidentModalComponent, AddMessageOperatorModalComponent],
  imports: [
    CommonModule,
    StackWinelandsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
    NgxPrintModule,
    GMapModule,
    KeyboardShortcutsModule.forRoot(),
    StackHistoryModule,
    EventsMgmtSharedModule,
    QuickAddServiceCallDialogModule,
    ThirdPartyDetailsModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-EN' },
  { provide: MAT_DATE_LOCALE, useValue: 'en-EN' }, DatePipe],
  entryComponents: [StackWinelandsPopupComponent, ClassifyIncidentModalComponent, AddMessageOperatorModalComponent]
})
export class StackWinelandsModule { }
