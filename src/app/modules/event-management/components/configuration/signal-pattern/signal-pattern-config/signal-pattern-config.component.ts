import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { signalpatternPatrolConfigModel, signalpatternPatrolModel } from "../../../../models/configurations/signal-pattern-config.model";
import { EventMgntModuleApiSuffixModels } from '../../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-signal-pattern-config',
  templateUrl: './signal-pattern-config.component.html',
  styleUrls: ['./signal-pattern-config.component.scss']
})
export class SignalPatternConfigComponent implements OnInit {
  signalPatternId: any;
  openCloseSetupDetails: any;
  signalDropDown: any = [];
  loggedUser: any;
  signalPatternAlarmTypesList: FormArray;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  patrolConfigAddEditForm: FormGroup;
  @ViewChildren('input') rows: QueryList<any>;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  primengTableConfigProperties: any;
  title:string;

  constructor(private formBuilder: FormBuilder,
    private httpCancelService: HttpCancelService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private router: Router,
    private snackbarService : SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.signalPatternId = this.activatedRoute.snapshot.queryParams.id
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.signalPatternId ? 'Update Signal Pattern Config':'Add Signal Pattern Config';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Signal Pattern Config', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 36} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    
  }

  ngOnInit(): void {
    this.getSignals();
    this.createsignalpatrolConfigAddEditForm();
    if (this.signalPatternId) {
      this.getSignalPatternConfigById(this.signalPatternId);
    } else {
      this.signalPatternAlarmTypesList = this.getsignalpatternListArray;
      this.signalPatternAlarmTypesList.push(this.createcallCategoryListModel());
      this.rxjsService.setFormChangeDetectionProperty(true);
    }

  }


  getSignalPatternConfigById(signalPatternId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_PATTERN, signalPatternId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          let openClosingSetupModel = new signalpatternPatrolConfigModel(response.resources);
          this.openCloseSetupDetails = response.resources;
          this.patrolConfigAddEditForm.patchValue(openClosingSetupModel);
          this.signalPatternAlarmTypesList = this.getsignalpatternListArray;
          response.resources.signalPatternAlarmTypesList.forEach((signalpatternPatrolListModel: signalpatternPatrolModel) => {
            signalpatternPatrolListModel = signalpatternPatrolListModel;
            this.signalPatternAlarmTypesList.push(this.createcallCategoryListModel(signalpatternPatrolListModel));
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSignals() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE_DESCRIPTION_SIGNAL
      , null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  remove(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.signalPatternAlarmTypesList.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          else {
            this.signalPatternAlarmTypesList.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
         }
      });
  }
  // Only Numbers with Decimals
  keyPressNumbersDecimal(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;
  }

  createsignalpatrolConfigAddEditForm(): void {
    let patrolConfigModel = new signalpatternPatrolConfigModel();
    // create form controls dynamically from model class
    this.patrolConfigAddEditForm = this.formBuilder.group({
      signalPatternAlarmTypesList: this.formBuilder.array([])
    });
    Object.keys(patrolConfigModel).forEach((key) => {
      this.patrolConfigAddEditForm.addControl(key, new FormControl(patrolConfigModel[key]));
    });
    this.patrolConfigAddEditForm = setRequiredValidator(this.patrolConfigAddEditForm, ["generatedAlarmTypeId", "signalDurationMinutes", "descriptions", "isTechnicianEscalationRequired"]);
    this.patrolConfigAddEditForm.get('signalDurationMinutes').setValidators([Validators.required, Validators.min(10), Validators.max(999)]);
    this.patrolConfigAddEditForm.get('createdUserId').setValue(this.loggedUser.userId);
  }

  //Create FormArray controls
  createcallCategoryListModel(callCategoryListModel?: signalpatternPatrolModel): FormGroup {
    let callCategoryListFormControlModel = new signalpatternPatrolModel(callCategoryListModel);
    let formControls = {};
    Object.keys(callCategoryListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.required]]
      formControls['signalIntervalSeconds'] = [{ value: callCategoryListFormControlModel['signalIntervalSeconds'], disabled: false }, [Validators.required, Validators.min(10), Validators.max(9999)]];
      formControls['sortOrder'] = [{ value: callCategoryListFormControlModel['sortOrder'], disabled: false }, [Validators.required, Validators.min(1), Validators.max(9999)]];
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  add() {
    if (this.signalPatternAlarmTypesList.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.signalPatternAlarmTypesList = this.getsignalpatternListArray;
    let callCategoryListModel = new signalpatternPatrolModel();
    this.signalPatternAlarmTypesList.insert(0, this.createcallCategoryListModel(callCategoryListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  //Create FormArray
  get getsignalpatternListArray(): FormArray {
    if (!this.patrolConfigAddEditForm) return;
    return this.patrolConfigAddEditForm.get("signalPatternAlarmTypesList") as FormArray;
  }
  onSubmit(): void {
    if (this.patrolConfigAddEditForm.invalid) {
      return;
    }
    let formValue = this.patrolConfigAddEditForm.value;
    if (this.signalPatternId != undefined) {
      this.patrolConfigAddEditForm.get('signalPatternId').setValue(this.signalPatternId);
      this.patrolConfigAddEditForm.get('modifiedUserId').setValue(this.loggedUser.userId);
    }
    else {
      this.patrolConfigAddEditForm.removeControl('signalPatternId');
      this.patrolConfigAddEditForm.removeControl('modifiedUserId');
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.signalPatternId
    ) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_PATTERN, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_PATTERN, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=36');
      }
    })
  }

  onCRUDRequested(type){}

}
