import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { SignalPatternConfigComponent } from './signal-pattern-config/signal-pattern-config.component';
import { SignalPatternRoutingModule } from './signal-pattern-routing.module';


@NgModule({
  declarations: [SignalPatternConfigComponent],
  imports: [
    CommonModule,
    SignalPatternRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SignalPatternModule { }
