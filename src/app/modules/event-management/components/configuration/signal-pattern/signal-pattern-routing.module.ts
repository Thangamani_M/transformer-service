import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalPatternConfigComponent } from "./signal-pattern-config/signal-pattern-config.component";

const routes: Routes = [
  { path: '', redirectTo: 'view', pathMatch: 'full' },
  { path: 'view', component: SignalPatternConfigComponent, data: { title: 'Signal Pattern Patrol Config' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalPatternRoutingModule { }
