import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { PostIncidentPatrolAddEditComponent } from './post-incident-patrol-add-edit.component';
import { PostIncidentPatrolRoutingModule } from './post-incident-patrol-routing.module';
import { PostIncidentPatrolViewComponent } from './post-incident-patrol-view.component';



@NgModule({
  declarations: [PostIncidentPatrolAddEditComponent, PostIncidentPatrolViewComponent],
  imports: [
    CommonModule,
    PostIncidentPatrolRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class PostIncidentPatrolModule { }
