import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { PostIncidentPatrolDetails } from '@modules/event-management/models/configurations/post-incident-patrol-model';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-post-incident-patrol-view',
  templateUrl: './post-incident-patrol-view.component.html',
})
export class PostIncidentPatrolViewComponent implements OnInit {
  postIncidentPatrolId: string;
  postIncidentPatrolDetailsDetails: any;
  PostIncidentPatrolView:any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.postIncidentPatrolId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit() {
    this.postIncidentPatrolDetailsDetails = new PostIncidentPatrolDetails();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getPostIncidentPatrolDetailsDetailsById(this.postIncidentPatrolId);
  }

  getPostIncidentPatrolDetailsDetailsById(postIncidentPatrolId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.POST_INCIDENT_PATROL, postIncidentPatrolId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.postIncidentPatrolDetailsDetails = response.resources;
          this.onShowValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.PostIncidentPatrolView = [
          { name: 'Post Incident Patrol Name', value: response?.postIncidentPatrolName },
          { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-pink' },
          { name: 'Division', value: response?.divisionName},
          { name: 'Duration', value: response?.duration },
          { name: 'Main Areas', value: response?.postIncidentPatrolMainAreas },
          { name: 'Patrolling Limit', value: response?.patrollingLimit },
          { name: 'Created On', value: response?.createdDate,isDateTime: true },
          { name: 'Modified On', value: response?.modifiedDate,isDateTime: true },
          { name: 'Created By', value: response?.createdUserName },
          { name: 'Modified By', value: response?.modifiedUserName},
          
    ]
  }


  edit() {
    if (this.postIncidentPatrolId) {
      this.router.navigate(['event-management/event-configuration/post-incident-patrol/add-edit'], { queryParams: { id: this.postIncidentPatrolId } });
    }
  }
}
