import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostIncidentPatrolAddEditComponent } from './post-incident-patrol-add-edit.component';
import { PostIncidentPatrolViewComponent } from './post-incident-patrol-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: PostIncidentPatrolAddEditComponent, data: { title: 'Post Incident Patrol Add/Edit' } },
  { path: 'view', component: PostIncidentPatrolViewComponent, data: { title: 'Post Incident Patrol View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PostIncidentPatrolRoutingModule { }
