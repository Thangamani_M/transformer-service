import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { PostIncidentPatrolModel } from '@modules/event-management/models/configurations/post-incident-patrol-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-post-incident-patrol-add-edit',
  templateUrl: './post-incident-patrol-add-edit.component.html',
})
export class PostIncidentPatrolAddEditComponent implements OnInit {

  postIncidentPatrolId: any;
  postIncidentPatrolDetails: any;
  divisionDropDown: any = [];
  mainAreaList: any = [];
  selectedDivisionId: any;
  loggedUser: any;
  postIncidentPatrolForm: FormGroup;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });

  selectedOptions: any = [];
  selectedOptionsObject: any = [];

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.postIncidentPatrolId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createPostIncidentPatrolForm();
    this.postIncidentPatrolForm.get("divisionId").valueChanges.subscribe(data => {
      this.getMainAreaList(data);
      this.selectedOptions = [];
    })

    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.postIncidentPatrolId) {
      this.getPostIncidentPatrolDetailsById(this.postIncidentPatrolId);
      return
    }
  }

  createPostIncidentPatrolForm(): void {
    let postIncidentPatrolModel = new PostIncidentPatrolModel();
    // create form controls dynamically from model class
    this.postIncidentPatrolForm = this.formBuilder.group({
      duration: [null, [Validators.required, Validators.min(1), Validators.max(999)]],
      patrollingLimit: [null, [Validators.required, Validators.min(1), Validators.max(99)]]
    });
    Object.keys(postIncidentPatrolModel).forEach((key) => {
      this.postIncidentPatrolForm.addControl(key, new FormControl(postIncidentPatrolModel[key]));
    });
    this.postIncidentPatrolForm = setRequiredValidator(this.postIncidentPatrolForm, ["postIncidentPatrolName", "divisionId", "postIncidentPatrolMainAreasList", "tempPostIncidentPatrolMainAreasList"]);
    this.postIncidentPatrolForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.postIncidentPatrolForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.postIncidentPatrolId) {
      this.postIncidentPatrolForm.removeControl('postIncidentPatrolId');
      this.postIncidentPatrolForm.removeControl('modifiedUserId');
    } else {
      this.postIncidentPatrolForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getPostIncidentPatrolDetailsById(postIncidentPatrolId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.POST_INCIDENT_PATROL, postIncidentPatrolId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.postIncidentPatrolDetails = response.resources;
          this.postIncidentPatrolForm.patchValue(response.resources);
          if (this.postIncidentPatrolDetails['postIncidentPatrolMainAreasList'].length > 0) {
            for (const mainArea of this.postIncidentPatrolDetails['postIncidentPatrolMainAreasList']) {
              this.selectedOptions.push(mainArea['mainAreaId']);
              this.selectedOptionsObject.push({ mainAreaId: mainArea['mainAreaId'] });
            }
            this.postIncidentPatrolForm.get('postIncidentPatrolMainAreasList').setValue(this.selectedOptionsObject);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSelectedMainAreas(mainAreas) {
    this.selectedOptions = mainAreas;
    this.selectedOptionsObject = [];
    if (this.postIncidentPatrolId) {
      for (const mainArea of mainAreas) {
        this.selectedOptionsObject.push({ mainAreaId: mainArea });
      }
    } else {
      for (const mainArea of mainAreas) {
        this.selectedOptionsObject.push({ mainAreaId: mainArea });
      }
    }
    this.postIncidentPatrolForm.controls.postIncidentPatrolMainAreasList.setValue(this.selectedOptionsObject);

  }
  onSubmit(): void {
    if (this.postIncidentPatrolForm.invalid) {
      this.postIncidentPatrolForm.get('tempPostIncidentPatrolMainAreasList').markAllAsTouched();
      return;
    }

    let formValue = this.postIncidentPatrolForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.postIncidentPatrolId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.POST_INCIDENT_PATROL, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.POST_INCIDENT_PATROL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=16');
      }
    })
  }
}
