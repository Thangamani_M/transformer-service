import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OpenCloseSetupAddEditComponent } from './open-close-setup-add-edit.component';
import { OpenCloseSetupViewComponent } from './open-close-setup-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: OpenCloseSetupAddEditComponent, data: { title: 'Open Closing Setup Add/Edit' } },
  { path: 'view', component: OpenCloseSetupViewComponent, data: { title: 'Open Closing Setup View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class OpenCloseSetupRoutingModule { }
