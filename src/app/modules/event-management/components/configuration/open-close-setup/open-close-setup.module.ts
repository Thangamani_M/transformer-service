import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { OpenCloseSetupAddEditComponent } from './open-close-setup-add-edit.component';
import { OpenCloseSetupRoutingModule } from './open-close-setup-routing.module';
import { OpenCloseSetupViewComponent } from './open-close-setup-view.component';




@NgModule({
  declarations: [OpenCloseSetupAddEditComponent, OpenCloseSetupViewComponent],
  imports: [
    CommonModule,
    OpenCloseSetupRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class OpenCloseSetupModule { }
