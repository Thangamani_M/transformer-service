import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-open-close-setup-view',
  templateUrl: './open-close-setup-view.component.html',
})
export class OpenCloseSetupViewComponent implements OnInit {
 
  openCloseSetupId: any
  selectedTabIndex = 0;
  primengTableConfigProperties: any
  closingViewDetails: any;
  openCloseSetupArmList: any;
  openCloseSetupDisarmList: any;
  alarmTypeWithDescription: any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  };

  constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.openCloseSetupId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View Open Closing Setup',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Open Closing Setup', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 35 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData();
    if (this.openCloseSetupId) {
      this.getclusterConfigDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.onShowValue(response.resources);
        this.openCloseSetupArmList = response.resources.openCloseSetupArmList;
        this.openCloseSetupDisarmList = response.resources.openCloseSetupDisarmList
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + response.resources.openCloseSetupName;
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
    this.getAlarmTypeWithDescription()
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onShowValue(response?: any) {
    this.closingViewDetails = [
      { name: 'Open Closing Setup Name', value: response?.openCloseSetupName },
      { name: 'Description', value: response?.description },
      { name: 'Division', value: response?.openCloseSetupDivisions },
      { name: 'Status', value: response?.isActive == true ? 'Active' : 'In Active', statusClass: response?.isActive==true?"status-label-green":'status-label-pink'},
      { name: 'Disarming Window', value: response?.disArmingWindow },
      { name: 'Arming Window', value: response?.armingWindow },
      { name: 'Fail to Open Alarm Type', value: response?.failToOpenAlarmType },
      { name: 'Fail to Close Alarm Type', value: response?.failToCloseAlarmType },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Modified By', value: response?.modifiedUserName },
    ]
  }

    getAlarmTypeWithDescription() {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION, null, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.alarmTypeWithDescription = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['event-management/event-configuration/open-close-setup/add-edit'], { queryParams: { id: this.openCloseSetupId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
  }

  getclusterConfigDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.OPEN_CLOSING_SETUP,
      this.openCloseSetupId
    );
  }

}
