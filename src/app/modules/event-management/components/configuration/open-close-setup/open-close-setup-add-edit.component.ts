import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { OpenCloseSetupArmListModel, OpenCloseSetupDisarmListModel, OpenClosingSetupModel } from '@modules/event-management/models/configurations/open-closing-setup-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-open-close-setup-add-edit',
  templateUrl: './open-close-setup-add-edit.component.html',
})
export class OpenCloseSetupAddEditComponent implements OnInit {

  openCloseSetupId: any;
  alarmTypeWithDescription = [];
  divisionDropDown: any = [];
  openCloseSetupConfigForm: FormGroup;
  openCloseSetupArmList: FormArray;
  openCloseSetupDisarmList: FormArray;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  openCloseSetupDetails: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChildren('input1') rows1: QueryList<any>;
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.openCloseSetupId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.openCloseSetupId ? 'Update Open Closing Setup':'Add Open Closing Setup';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Open Closing Setup', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 35 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.openCloseSetupId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/open-close-setup/view' , queryParams: {id: this.openCloseSetupId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }

  }

  ngOnInit() {
    this.createOpenCloseSetupForm();
    this.getDivisionDropDown();
    this.getAlarmTypeWithDescription();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.openCloseSetupId) {
      this.getOpenCloseSetupDetailsById().subscribe((response: IApplicationResponse) => {
        let openClosingSetupModel = new OpenClosingSetupModel(response.resources);
        this.openCloseSetupDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.openCloseSetupDetails?.openCloseSetupName ? this.title +' - '+ this.openCloseSetupDetails?.openCloseSetupName : this.title + ' --', relativeRouterUrl: '' });
        this.openCloseSetupConfigForm.patchValue(openClosingSetupModel);
        var openCloseSetupDivisionList = [];
        response.resources.openCloseSetupDivisionList.forEach(element => {
          openCloseSetupDivisionList.push(element.divisionId);
        });
        this.openCloseSetupConfigForm.get('openCloseSetupDivisionList').setValue(openCloseSetupDivisionList);
        this.openCloseSetupArmList = this.getOpenCloseSetupArmListArray;
        response.resources.openCloseSetupArmList.forEach((openCloseSetupArmListModel: OpenCloseSetupArmListModel) => {
          openCloseSetupArmListModel.openCloseSetupArmId = openCloseSetupArmListModel.openCloseSetupArmId;
          this.openCloseSetupArmList.push(this.createOpenCloseSetupArmListModel(openCloseSetupArmListModel));
        });
        this.openCloseSetupDisarmList = this.getOpenCloseSetupDisarmListArray;
        response.resources.openCloseSetupDisarmList.forEach((openCloseSetupDisarmListModel: OpenCloseSetupDisarmListModel) => {
          openCloseSetupDisarmListModel.openCloseSetupDisarmId = openCloseSetupDisarmListModel.openCloseSetupDisarmId;
          this.openCloseSetupDisarmList.push(this.createOpenCloseSetupDisArmListModel(openCloseSetupDisarmListModel));
        });
      })
    } else {
      this.openCloseSetupArmList = this.getOpenCloseSetupArmListArray;
      this.openCloseSetupArmList.push(this.createOpenCloseSetupArmListModel());
      this.openCloseSetupDisarmList = this.getOpenCloseSetupDisarmListArray;
      this.openCloseSetupDisarmList.push(this.createOpenCloseSetupDisArmListModel());
    }
  }

  createOpenCloseSetupForm(): void {
    let openCloseSetupModel = new OpenClosingSetupModel();
    this.openCloseSetupConfigForm = this.formBuilder.group({
      disArmingWindow: [null, [Validators.required, Validators.min(1), Validators.max(99)]],
      armingWindow: [null, [Validators.required, Validators.min(1), Validators.max(99)]],
      openCloseSetupArmList: this.formBuilder.array([]),
      openCloseSetupDisarmList: this.formBuilder.array([])
    });
    Object.keys(openCloseSetupModel).forEach((key) => {
      this.openCloseSetupConfigForm.addControl(key, new FormControl(openCloseSetupModel[key]));
    });
    this.openCloseSetupConfigForm = setRequiredValidator(this.openCloseSetupConfigForm, ["openCloseSetupName", "description", "openCloseSetupDivisionList", "failToOpenAlarmTypeId", "failToCloseAlarmTypeId"]);
    this.openCloseSetupConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.openCloseSetupConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAlarmTypeWithDescription() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmTypeWithDescription = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleDivisionOne() {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.openCloseSetupConfigForm.controls.openCloseSetupDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.openCloseSetupConfigForm.controls.openCloseSetupDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.openCloseSetupConfigForm.controls.openCloseSetupDivisionList.patchValue([]);
    }
  }

  //Create FormArray ARM
  get getOpenCloseSetupArmListArray(): FormArray {
    if (!this.openCloseSetupConfigForm) return;
    return this.openCloseSetupConfigForm.get("openCloseSetupArmList") as FormArray;
  }

  //Create FormArray DISARM
  get getOpenCloseSetupDisarmListArray(): FormArray {
    if (!this.openCloseSetupConfigForm) return;
    return this.openCloseSetupConfigForm.get("openCloseSetupDisarmList") as FormArray;
  }

  //Create FormArray controls ARM List
  createOpenCloseSetupArmListModel(openCloseSetupArmListModel?: OpenCloseSetupArmListModel): FormGroup {
    let openCloseSetupArmListFormControlModel = new OpenCloseSetupArmListModel(openCloseSetupArmListModel);
    let formControls = {};
    Object.keys(openCloseSetupArmListFormControlModel).forEach((key) => {
      if (key === 'signalReceivedAlarmId' || key === 'unscheduledCloseAlarmId' || key === 'closeEarlyAlarmId' || key === 'closeLateAlarmId' || key === 'failSignalAlarmId') {
        formControls[key] = [{ value: openCloseSetupArmListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.openCloseSetupId) {
        formControls[key] = [{ value: openCloseSetupArmListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: openCloseSetupArmListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls ARM List
  createOpenCloseSetupDisArmListModel(openCloseSetupDisarmListModel?: OpenCloseSetupDisarmListModel): FormGroup {
    let openCloseSetupDisarmListFormControlModel = new OpenCloseSetupDisarmListModel(openCloseSetupDisarmListModel);
    let formControls = {};
    Object.keys(openCloseSetupDisarmListFormControlModel).forEach((key) => {
      if (key === 'signalReceivedAlarmId' || key === 'openEarlyAlarmId' || key === 'openLateAlarmId' || key === 'afterHoursAlarmId' || key === 'failSignalAlarmId') {
        formControls[key] = [{ value: openCloseSetupDisarmListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.openCloseSetupId) {
        formControls[key] = [{ value: openCloseSetupDisarmListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: openCloseSetupDisarmListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getOpenCloseSetupDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.OPEN_CLOSING_SETUP,
      this.openCloseSetupId
    );
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Signal Recieved already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getOpenCloseSetupArmListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.signalReceivedAlarmId)) {
        duplicate.push(k.value.signalReceivedAlarmId);
      }
      filterKey.push(k.value.signalReceivedAlarmId);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  focusInAndOutFormArrayFields1(): void {
    this.rows1.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add and Reomove Arm List
  addArmList(): void {
    if (!this.onChange()) {
      return;
    }
    if (this.getOpenCloseSetupArmListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.openCloseSetupArmList = this.getOpenCloseSetupArmListArray;
    let openCloseSetupArmListModel = new OpenCloseSetupArmListModel();
    this.openCloseSetupArmList.insert(0, this.createOpenCloseSetupArmListModel(openCloseSetupArmListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeArmList(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getOpenCloseSetupArmListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
            if (this.getOpenCloseSetupArmListArray.controls[i].value.openCloseSetupArmId && this.getOpenCloseSetupArmListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_CLOSING_SETUP_ARM,
          this.getOpenCloseSetupArmListArray.controls[i].value.openCloseSetupArmId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getOpenCloseSetupArmListArray.removeAt(i);
              this.rxjsService.setFormChangeDetectionProperty(true);
            }
            if (this.getOpenCloseSetupArmListArray.length === 0) {
              this.addArmList();
              this.rxjsService.setFormChangeDetectionProperty(true);
            };
          });
      }
          else {
            this.getOpenCloseSetupArmListArray.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  //Add and Reomove Arm List
  addDisArmList(): void {
    if (!this.onChange()) {
      return;
    }
    if (this.getOpenCloseSetupDisarmListArray.invalid) {
      this.focusInAndOutFormArrayFields1();
      return;
    };
    this.openCloseSetupDisarmList = this.getOpenCloseSetupDisarmListArray;
    let openCloseSetupDisArmListModel = new OpenCloseSetupDisarmListModel();
    this.openCloseSetupDisarmList.insert(0, this.createOpenCloseSetupDisArmListModel(openCloseSetupDisArmListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeDisArmList(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getOpenCloseSetupDisarmListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getOpenCloseSetupDisarmListArray.controls[i].value.openCloseSetupDisarmId && this.getOpenCloseSetupDisarmListArray.length > 1) {
                this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_CLOSING_SETUP_DISARM,
                  this.getOpenCloseSetupDisarmListArray.controls[i].value.openCloseSetupDisarmId).subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess) {
                      this.getOpenCloseSetupDisarmListArray.removeAt(i);
                    }
                    if (this.getOpenCloseSetupDisarmListArray.length === 0) {
                      this.addArmList();
                    };
                  });
              }
          else {
            this.getOpenCloseSetupDisarmListArray.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
  }


  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }
    if (this.openCloseSetupConfigForm.invalid) {
      return;
    }
    let formValue = this.openCloseSetupConfigForm.value;
    formValue.modifiedUserId = this.loggedUser.userId;
    if (formValue.openCloseSetupDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.openCloseSetupDivisionList = formValue.openCloseSetupDivisionList.filter(item => item != '')
      formValue.openCloseSetupDivisionList = formValue.openCloseSetupDivisionList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.openCloseSetupId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_CLOSING_SETUP, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OPEN_CLOSING_SETUP, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=35');
      }
    })
  }

  onCRUDRequested(type){}

}