import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CmcSmsGroupAddEditComponent } from './cmc-sms-group-add-edit.component';
import { CmcSmsGroupViewComponent } from './cmc-sms-group-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CmcSmsGroupAddEditComponent, data: { title: 'CMC SMS Group Add/Edit' } },
  { path: 'view', component: CmcSmsGroupViewComponent, data: { title: 'CMC SMS Group View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CmcSmsGroupRoutingModule { }
