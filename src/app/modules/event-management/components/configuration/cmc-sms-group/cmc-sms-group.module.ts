import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { CmcSmsGroupAddEditComponent } from './cmc-sms-group-add-edit.component';
import { CmcSmsGroupRoutingModule } from './cmc-sms-group-routing.module';
import { CmcSmsGroupViewComponent } from './cmc-sms-group-view.component';

@NgModule({
  declarations: [CmcSmsGroupAddEditComponent, CmcSmsGroupViewComponent],
  imports: [
    CommonModule,
    CmcSmsGroupRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class CmcSmsGroupModule { }
