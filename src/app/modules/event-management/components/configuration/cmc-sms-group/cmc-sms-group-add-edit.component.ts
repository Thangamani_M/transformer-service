import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, countryCodes, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CmcSmsGroupEmployeeListModel, CmcSmsGroupModel } from '@modules/event-management/models/configurations/cmc-sms-group-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-cmc-sms-group-add-edit',
  templateUrl: './cmc-sms-group-add-edit.component.html',
})
export class CmcSmsGroupAddEditComponent implements OnInit {

  cmcsmsGroupId: any
  divisionDropDown: any = [];
  cmcSmsGroupForm: FormGroup;
  cmcsmsGroupEmployeeList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  cmcSmsGroupDetails: any;
  countryCodes = countryCodes;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  @ViewChildren('input') rows: QueryList<any>;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.cmcsmsGroupId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.cmcsmsGroupId ? 'Update CMC SMS Group':'Add CMC SMS Group';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'CMC SMS Group', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 14 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.cmcsmsGroupId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/cmc-sms-group/view' , queryParams: {id: this.cmcsmsGroupId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createCmcSmsGroupForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.cmcsmsGroupId) {
      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let cmcSmsGroup = new CmcSmsGroupModel(response.resources);
        this.cmcSmsGroupDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.cmcSmsGroupDetails?.groupName ? this.title +' - '+ this.cmcSmsGroupDetails?.groupName : this.title + ' --', relativeRouterUrl: '' });
        this.cmcSmsGroupForm.patchValue(cmcSmsGroup);
        this.cmcsmsGroupEmployeeList = this.getCmcSmsGroupEmployeeListArray;
        if (response.resources.cmcsmsGroupEmployeeList.length > 0) {
          response.resources.cmcsmsGroupEmployeeList.forEach((cmcSmsGroupEmployeeListModel: CmcSmsGroupEmployeeListModel) => {
            cmcSmsGroupEmployeeListModel.mobileNumber = cmcSmsGroupEmployeeListModel.mobileNumber.replace(/^(\d{0,2})(\d{0,3})(\d{0,4})/, '$1 $2 $3');
            this.cmcsmsGroupEmployeeList.push(this.createCmcSmsGroupEmployeeListModel(cmcSmsGroupEmployeeListModel));
          });
        } else {
          this.cmcsmsGroupEmployeeList.push(this.createCmcSmsGroupEmployeeListModel());
        }
      })
    } else {
      this.cmcsmsGroupEmployeeList = this.getCmcSmsGroupEmployeeListArray;
      this.cmcsmsGroupEmployeeList.push(this.createCmcSmsGroupEmployeeListModel());
    }
  }

  createCmcSmsGroupForm(): void {
    let cmcSmsGroupModel = new CmcSmsGroupModel();
    this.cmcSmsGroupForm = this.formBuilder.group({
      cmcsmsGroupEmployeeList: this.formBuilder.array([])
    });
    Object.keys(cmcSmsGroupModel).forEach((key) => {
      this.cmcSmsGroupForm.addControl(key, new FormControl(cmcSmsGroupModel[key]));
    });
    this.cmcSmsGroupForm = setRequiredValidator(this.cmcSmsGroupForm, ["groupName", "description", "divisionId"]);
    this.cmcSmsGroupForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.cmcSmsGroupForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getCmcSmsGroupEmployeeListArray(): FormArray {
    if (!this.cmcSmsGroupForm) return;
    return this.cmcSmsGroupForm.get("cmcsmsGroupEmployeeList") as FormArray;
  }

  //Create FormArray controls
  createCmcSmsGroupEmployeeListModel(cmcSmsGroupEmployeeListModel?: CmcSmsGroupEmployeeListModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new CmcSmsGroupEmployeeListModel(cmcSmsGroupEmployeeListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {
      if (key === 'employeeName' || key === 'mobileNumberCountryCode' || key === 'mobileNumber') {
        formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.cmcsmsGroupId) {
        formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }]
      }
    });
    let forms = this.formBuilder.group(formControls);

    forms.get("mobileNumberCountryCode")
      .valueChanges.subscribe((mobileNumberCountryCode: string) => {
        switch (mobileNumberCountryCode) {
          case "+27":
            forms.get("mobileNumber").setValidators([Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength),
            Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),])
            break;
          default:
            forms.get("mobileNumber").setValidators([Validators.minLength(
              formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),])
            break;
        }
        setTimeout(() => {
          this.focusInAndOutFormArrayFields();
        });
      });

    forms.get("mobileNumber")
      .valueChanges.subscribe((mobileNumber: string) => {
        switch (forms.get("mobileNumberCountryCode").value) {
          case "+27":
            forms.get("mobileNumber").setValidators([Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength),
            Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),])
            break;
          default:
            forms.get("mobileNumber").setValidators([Validators.minLength(
              formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),])
            break;
        }
      })
    return forms
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CMS_SMS_GROUP,
      this.cmcsmsGroupId
    );
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Employee Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getCmcSmsGroupEmployeeListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.employeeName)) {
        duplicate.push(k.value.employeeName);
      }
      filterKey.push(k.value.employeeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChangeNumber() {
    const duplicate = this.duplicateNumber();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Mobile Number already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateNumber() {
    const filterKey = [];
    const duplicate = [];
    this.getCmcSmsGroupEmployeeListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.mobileNumber)) {
        duplicate.push(k.value.mobileNumber);
      }
      filterKey.push(k.value.mobileNumber);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (!this.onChange()) {
      return;
    }
    if (!this.onChangeNumber()) {
      return;
    }
    if (this.getCmcSmsGroupEmployeeListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.cmcsmsGroupEmployeeList = this.getCmcSmsGroupEmployeeListArray;
    let cmcSmsGroupEmployeeListModel = new CmcSmsGroupEmployeeListModel();
    this.cmcsmsGroupEmployeeList.insert(0, this.createCmcSmsGroupEmployeeListModel(cmcSmsGroupEmployeeListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getCmcSmsGroupEmployeeListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getCmcSmsGroupEmployeeListArray.controls[i].value.cmcsmsGroupEmployeeId && this.getCmcSmsGroupEmployeeListArray.length > 1) {
                this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMC_SMS_GROUP_EMPLOYEE,
                  this.getCmcSmsGroupEmployeeListArray.controls[i].value.cmcsmsGroupEmployeeId).subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess) {
                      this.getCmcSmsGroupEmployeeListArray.removeAt(i);
                    }
                    if (this.getCmcSmsGroupEmployeeListArray.length === 0) {
                      this.addCmcSmsGroupEmployee();
                    };
                  });
              }
          else {
            this.getCmcSmsGroupEmployeeListArray.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }
    if (!this.onChangeNumber()) {
      return;
    }
    if (this.cmcSmsGroupForm.invalid) {
      return;
    }
    let formValue = this.cmcSmsGroupForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.cmcsmsGroupId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMS_SMS_GROUP, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CMS_SMS_GROUP, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=14');
      }
    })
  }

  onCRUDRequested(type){}

}