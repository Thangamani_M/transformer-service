import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoConfigurationListComponent } from './ro-configuration-list/ro-configuration-list.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  {
    path: '', component: RoConfigurationListComponent, data: { title: 'RO Configuration List' },canActivate: [AuthGuard],
  },
  { path: 'ro-accept-time', loadChildren: () => import('../configuration/ro-accept-time/ro-accept-time.module').then(m => m.RoAcceptTimeModule) },
  { path: 'find-u-dispatch', loadChildren: () => import('../configuration/find-u-dispatch/find-u-dispatch.module').then(m => m.FindUDispatchModule) },
  { path: 'dispatch-exception', loadChildren: () => import('../configuration/dispatch-exception/dispatch-exception.module').then(m => m.DispatchExceptionModule) },
  { path: 'feedback-offsite', loadChildren: () => import('../configuration/feedback-offsite/feedback-offsite.module').then(m => m.FeedbackOffsiteModule) },
  { path: 'alarm-type-config', loadChildren: () => import('../configuration/alarm-type-config/alarm-type-config.module').then(m => m.AlarmTypeConfigModule) },
  { path: 'proactive-patrol', loadChildren: () => import('../configuration/proactive-patrol/proactive-patrol.module').then(m => m.ProactivePatrolModule) },
  { path: 'incidents', loadChildren: () => import('../configuration/incidents/incidents.module').then(m => m.IncidentsModule) },
  { path: 'decline-setup', loadChildren: () => import('../configuration/decline-setup/decline-setup.module').then(m => m.DeclineSetupModule) },
  { path: 'property-access', loadChildren: () => import('../configuration/property-access/property-access.module').then(m => m.PropertyAccessModule) },
  { path: 'cmc-feedback', loadChildren: () => import('../configuration/cmc-feedback/cmc-feedback.module').then(m => m.CmcFeedbackModule) },
  { path: 'check-list', loadChildren: () => import('../configuration/check-list/check-list.module').then(m => m.CheckListModule) },
  { path: 'safety-alert', loadChildren: () => import('../configuration/safety-alert/safety-alert.module').then(m => m.SafetyAlertModule) },
  { path: 'vas-config', loadChildren: () => import('../configuration/vas-config/vas-config.module').then(m => m.VasConfigModule) },
  { path: 'time-on-site', loadChildren: () => import('../configuration/time-on-site/time-on-site.module').then(m => m.TimeOnSiteModule) },
  { path: 'ro-community-patrol', loadChildren: () => import('../configuration/ro-community-patrol/ro-community-patrol.module').then(m => m.ROCommunityPatrolModule) },
  { path: 'reporting-config', loadChildren: () => import('../configuration/reporting-config/reporting-config.module').then(m => m.ReportingConfigModule) },
  { path: 'long-standoff', loadChildren: () => import('../configuration/long-standoff/long-standoff.module').then(m => m.LongStandoffModule) },
  { path: 'useful-numbers', loadChildren: () => import('../configuration/useful-numbers/useful-numbers.module').then(m => m.UsefulNumbersModule) },
  { path: 'open-area', loadChildren: () => import('../configuration/open-area/open-area.module').then(m => m.OpenAreaModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class RoConfigurationRoutingModule { }
