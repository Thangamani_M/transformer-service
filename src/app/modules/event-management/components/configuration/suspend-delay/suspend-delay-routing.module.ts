import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuspendDelayAddEditComponent } from './suspend-delay-add-edit.component';
import { SuspendDelayViewComponent } from './suspend-delay-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SuspendDelayAddEditComponent, data: { title: 'Suspend Delay Add/Edit' } },
  { path: 'view', component: SuspendDelayViewComponent, data: { title: 'Suspend Delay View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SuspendDelayRoutingModule { }
