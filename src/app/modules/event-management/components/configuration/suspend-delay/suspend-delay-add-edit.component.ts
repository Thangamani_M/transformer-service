import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { SuspendDelayModel } from '@modules/event-management/models/configurations/suspend-delay.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-suspend-delay-add-edit',
  templateUrl: './suspend-delay-add-edit.component.html',
})
export class SuspendDelayAddEditComponent implements OnInit {


  maxSuspendDelayId: any;
  maxSuspendDelayDetails: any;
  divisionDropDown: any = [];
  branchDropdown: any = [];
  loggedUser: any;
  maxSuspendDelayForm: FormGroup;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.maxSuspendDelayId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.maxSuspendDelayId ? 'Update Suspend Delay':'Add Suspend Delay';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Suspend Delay', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 19} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.maxSuspendDelayId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/suspend-delay/view' , queryParams: {id: this.maxSuspendDelayId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createmaxSuspendDelayForm();
    this.getDivisionDropDownById();
    if (this.maxSuspendDelayId) {
      this.getmaxSuspendDelayDetailsById(this.maxSuspendDelayId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createmaxSuspendDelayForm(): void {
    let suspendDelayModel = new SuspendDelayModel();
    // create form controls dynamically from model class
    this.maxSuspendDelayForm = this.formBuilder.group({
      timeDelay: [null, [Validators.required, Validators.min(10), Validators.max(9999)]],
    });
    Object.keys(suspendDelayModel).forEach((key) => {
      this.maxSuspendDelayForm.addControl(key, new FormControl(suspendDelayModel[key]));
    });
    this.maxSuspendDelayForm = setRequiredValidator(this.maxSuspendDelayForm, ["maxSuspendDelayName", "maxSuspendDelayDivisionList"]);
    this.maxSuspendDelayForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.maxSuspendDelayForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.maxSuspendDelayId) {
      this.maxSuspendDelayForm.removeControl('maxSuspendDelayId');
      this.maxSuspendDelayForm.removeControl('modifiedUserId');
    } else {
      this.maxSuspendDelayForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getmaxSuspendDelayDetailsById(maxSuspendDelayId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUSPEND_DELAY, maxSuspendDelayId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.maxSuspendDelayDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.maxSuspendDelayDetails?.maxSuspendDelayName ? this.title +' - '+ this.maxSuspendDelayDetails?.maxSuspendDelayName : this.title + ' --', relativeRouterUrl: '' });
          this.maxSuspendDelayForm.patchValue(this.maxSuspendDelayDetails);
          var maxSuspendDelayDivisionList = [];
          response.resources.maxSuspendDelayDivisionList.forEach(element => {
            maxSuspendDelayDivisionList.push(element.divisionId);
          });
          this.maxSuspendDelayForm.get('maxSuspendDelayDivisionList').setValue(maxSuspendDelayDivisionList)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }



  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.maxSuspendDelayForm.controls.maxSuspendDelayDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.maxSuspendDelayForm.controls.maxSuspendDelayDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.maxSuspendDelayForm.controls.maxSuspendDelayDivisionList.patchValue([]);
    }
  }

  onSubmit(): void {
    if (this.maxSuspendDelayForm.invalid) {
      return;
    }
    let formValue = this.maxSuspendDelayForm.value;
    formValue.timeDelay = Number(formValue.timeDelay)
    if (formValue.maxSuspendDelayDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.maxSuspendDelayDivisionList = formValue.maxSuspendDelayDivisionList.filter(item => item != '')
      formValue.maxSuspendDelayDivisionList = formValue.maxSuspendDelayDivisionList.map(divisionId => ({ divisionId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.maxSuspendDelayId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUSPEND_DELAY, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUSPEND_DELAY, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=19');
      }
    })
  }

  onCRUDRequested(event){}
  
}
