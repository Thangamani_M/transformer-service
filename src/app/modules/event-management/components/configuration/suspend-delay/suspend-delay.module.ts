import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { SuspendDelayAddEditComponent } from './suspend-delay-add-edit.component';
import { SuspendDelayRoutingModule } from './suspend-delay-routing.module';
import { SuspendDelayViewComponent } from './suspend-delay-view.component';




@NgModule({
  declarations: [SuspendDelayViewComponent, SuspendDelayAddEditComponent],
  imports: [
    CommonModule,
    SuspendDelayRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class SuspendDelayModule { }
