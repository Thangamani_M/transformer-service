import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InstructorInstructionAddEditModel } from '@modules/event-management/models/configurations/instructor-instruction.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'instructor-instruction-add-edit',
  templateUrl: './instructor-instruction-add-edit.component.html',
})
export class InstructorInstructionAddEditComponent implements OnInit {

  instructorInstructionAddEditForm: FormGroup;
  viewable: boolean;
  userData: UserLogin;
  primengTableConfigProperties: any;
  id: any;
  btnName: any;
  instructorInstructionDetail: any;
  selectedTabIndex: any = 0;
  isSubmitted: boolean;
  instructorInstructionType = []
  incidentTypes = []
  alarmTypes = []
  typeList = [];
  instructType = ""
  customerTypes = [];
  totaltab = 0
  selectedTab = 0
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private router: Router,
    private activateRoute: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService) {
    this.activateRoute.queryParamMap.subscribe((params) => {
      this.id = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = (Object.keys(params['params']).length > 0) ? params['params']['totalTabs'] : '' || 40;
      this.totaltab = (Object.keys(params['params']).length > 0) ? params['params']['selectedTab'] : '';
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.primengTableConfigProperties = {
      tableCaption: this.id && !this.viewable ? 'Update Operator Interactions' : this.viewable ? 'View Operator Interactions' : 'Add Instructor Instructions',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '' }, { displayName: 'Operator Interactions', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 34 } }, { displayName: 'Add Operator Interactions', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    }
    this.onLoadValue();
    this.combineLatestNgrxStoreData();
    this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess && !response[1].isSuccess && !response[2].isSuccess && !response[3].isSuccess) return;
      this.instructorInstructionType = response[0].resources;
      this.alarmTypes = response[1].resources;
      this.incidentTypes = response[2].resources;
      this.customerTypes = response[3].resources;

      if (!this.viewable && this.id) {
        this.deceideTypesDropdown(this.instructType)
      }

      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.rxjsService.setGlobalLoaderProperty(true);
    this.formControlChange();
  }

  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    if (this.viewable) {
      this.instructorInstructionDetail = [
        { name: 'Operator Interactions Type', value: '' },
        { name: 'Notes', value: '' },
        { name: 'Is Acknowledge Required?', value: '' },
        { name: 'Is Link To Page?', value: '' },
        { name: 'URL', value: '' },
        { name: 'Operator Interactions Trigger Types', value: '' },
        { name: 'Created On', value: '' },
        { name: 'Modified On', value: '' },
        { name: 'Created By', value: '' },
        { name: 'Modified By', value: '' },
      ]
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
      this.viewValue();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm(instructorInstructionAddEditModel?: InstructorInstructionAddEditModel) {
    let instructorInstruction = new InstructorInstructionAddEditModel(instructorInstructionAddEditModel);
    this.instructorInstructionAddEditForm = this.formBuilder.group({});
    Object.keys(instructorInstruction).forEach((key) => {
      if (!this.viewable) {
        this.instructorInstructionAddEditForm.addControl(key, new FormControl(instructorInstruction[key]));
      }
    });
    if (!this.viewable) {
      this.instructorInstructionAddEditForm = setRequiredValidator(this.instructorInstructionAddEditForm, ["instructorInstructionTypeId", "isAcknowledgeRequired", "comments", "isLinkToPage", "urlLink", "instructorInstructionTriggerTypesList"])
    }
    if (this.id && !this.viewable) {
      this.patchValue();
    }
  }

  viewValue() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_INSTRUCTOR_INSTRUCTIONS, this.id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources) {
          this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = response.resources.instructorInstructionTypeName;
          if (this.viewable) {
            this.instructorInstructionDetail = [
              { name: 'Operator Interactions Type', value: response.resources.instructorInstructionTypeName },
              { name: 'Notes', value: response.resources.comments },
              { name: 'Is Acknowledge Required?', value: response.resources.isAcknowledgeRequired == true ? "Yes" : "No" },
              { name: 'Is Link To Page?', value: response.resources.isLinkToPage == true ? "Yes" : "No" },
              { name: 'URL', value: response.resources.urlLink },
              { name: 'Trigger Types', value: response.resources.instructorInstructionTriggerTypes },
              { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
              { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
              { name: 'Created By', value: response.resources.createdUserName },
              { name: 'Modified By', value: response.resources.modifiedUserName },
            ];
          } else if (this.id && !this.viewable) {
            let typeId = []
            response.resources.instructorInstructionTriggerTypesList.map(item => {
              typeId.push({id:item.referenceId,displayName:response.resources.instructorInstructionTriggerTypes})
            })
            response.resources.instructorInstructionTriggerTypesList = typeId
            this.instructorInstructionAddEditForm.patchValue(response.resources, { onlySelf: true, emitEvent: false })
            this.instructType = response.resources.instructorInstructionTypeId;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.id && !this.viewable ? 'Update Operator Interactions' : this.viewable ? 'View Operator Interactions' : 'Add Operator Interactions';
    this.btnName = this.id ? 'Update' : 'Save';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.viewable ? 'View Operator Interactions' : this.id && !this.viewable ? 'Update Operator Interactions' : 'Add Operator Interactions';
  }

  patchValue() {
    this.viewValue();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {

    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/event-management/event-configuration/operator-interactions/add-edit'], { queryParams: { id: this.id }, skipLocationChange: true })

  }

  // Get all Dropdown data
  getAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_INSTRUCTOR_INSTRUCTIONS_DROPDOWN, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE_DROPDOWN, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_INCIDENT_DROPDOWN, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CUSTOMER_TYPE_OPTIONS, undefined, true),
    )
  }

  formControlChange() {
    this.instructorInstructionAddEditForm.get('instructorInstructionTypeId').valueChanges.subscribe((id: string) => {
      this.instructorInstructionAddEditForm.get('instructorInstructionTriggerTypesList').setValue([]);
      this.deceideTypesDropdown(id);
    });
  }

  deceideTypesDropdown(id) {
    let _type = this.instructorInstructionType.find(item => item.id == id);
    if (_type.displayName == "Alarm Type") {
      this.typeList = this.alarmTypes;
    } else if (_type.displayName == "Incident Type") {
      this.typeList = this.incidentTypes;
    }
    else {
      this.typeList = this.customerTypes;
    }
  }
  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(false)
    if (this.instructorInstructionAddEditForm?.invalid) {
      this.instructorInstructionAddEditForm.markAllAsTouched();
      return;
    }
    let types = [];
    this.instructorInstructionAddEditForm.get('instructorInstructionTriggerTypesList').value.map((id) => {
      types.push({ referenceId: id.id })
    });
    let reqObj = {
      instructorInstructionTypeId: this.instructorInstructionAddEditForm.get('instructorInstructionTypeId').value,
      comments: this.instructorInstructionAddEditForm.get('comments').value,
      isAcknowledgeRequired: this.instructorInstructionAddEditForm.get('isAcknowledgeRequired').value,
      isLinkToPage: this.instructorInstructionAddEditForm.get('isLinkToPage').value,
      urlLink: this.instructorInstructionAddEditForm.get('urlLink').value,
      instructorInstructionTriggerTypesList: types,
    }
    this.isSubmitted = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let api = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_INSTRUCTOR_INSTRUCTIONS, reqObj);
    if (this.id) {
      reqObj['modifiedUserId'] = this.userData?.userId;
      reqObj['instructorInstructionId'] = this.id;
      api = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_INSTRUCTOR_INSTRUCTIONS, reqObj);
    } else if (!this.id) {
      reqObj['createdUserId'] = this.userData?.userId;
    }
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess) {
        this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: 34 }, skipLocationChange: true })
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
}
