import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InstructorInstructionAddEditComponent } from './instructor-instruction-add-edit';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: InstructorInstructionAddEditComponent, data: { title: 'Operator Interactions Add/Edit' } },
  { path: 'view', component: InstructorInstructionAddEditComponent, data: { title: 'Operator Interactions View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class InstructorInstructionRoutingModule { }
