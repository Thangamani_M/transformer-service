import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { InstructorInstructionAddEditComponent } from './instructor-instruction-add-edit';
import { InstructorInstructionRoutingModule } from './instructor-instruction.routing.module';
@NgModule({
  declarations: [InstructorInstructionAddEditComponent],
  imports: [
    CommonModule,
    InstructorInstructionRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class InstructorInstructionModule { }
