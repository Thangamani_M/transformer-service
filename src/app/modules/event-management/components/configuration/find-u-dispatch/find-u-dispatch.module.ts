import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { FindUDispatchAddEditComponent } from './find-u-dispatch-add-edit.component';
import { FindUDispatchRoutingModule } from './find-u-dispatch-routing.module';
import { FindUDispatchViewComponent } from './find-u-dispatch-view.component';
@NgModule({
  declarations: [FindUDispatchAddEditComponent, FindUDispatchViewComponent],
  imports: [
    CommonModule,
    FindUDispatchRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class FindUDispatchModule { }
