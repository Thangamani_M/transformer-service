import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FindUDispatchAddEditComponent } from './find-u-dispatch-add-edit.component';
import { FindUDispatchViewComponent } from './find-u-dispatch-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: FindUDispatchAddEditComponent, data: { title: 'Find U Dispatch Add/Edit' } },
  { path: 'view', component: FindUDispatchViewComponent, data: { title: 'Find U Dispatch View' } ,canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class FindUDispatchRoutingModule { }
