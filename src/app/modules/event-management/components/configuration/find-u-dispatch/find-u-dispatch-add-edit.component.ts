import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { FindUDispatchModel } from '@modules/event-management/models/configurations/findu-dispatch.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-find-u-dispatch-add-edit',
  templateUrl: './find-u-dispatch-add-edit.component.html',
})
export class FindUDispatchAddEditComponent implements OnInit {
  findUDispatchId: string;
  findUDispatchDetails: any;
  divisionDropDown: any = [];
  findUDispatchForm: FormGroup;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.findUDispatchId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.findUDispatchId ? 'Update FindU Dispatch':'Add FindU Dispatch';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'FindU Dispatch', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 3} }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.findUDispatchId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/find-u-dispatch/view' , queryParams: {id: this.findUDispatchId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createfindUDispatchForm();
    this.getDivisionDropDown();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.findUDispatchId) {
      this.getfindUDispatchDetailsById(this.findUDispatchId);
      return
    }

  }

  createfindUDispatchForm(): void {
    let findUDispatchModel = new FindUDispatchModel();
    // create form controls dynamically from model class
    this.findUDispatchForm = this.formBuilder.group({
      deviceMovedDistance: [null, [Validators.required, Validators.min(10), Validators.max(9999)]],
    });
    Object.keys(findUDispatchModel).forEach((key) => {
      this.findUDispatchForm.addControl(key, new FormControl(findUDispatchModel[key]));
    });
    this.findUDispatchForm = setRequiredValidator(this.findUDispatchForm, ["findUDispatchDivisionList", "findUDispatchName", "description"]);
    this.findUDispatchForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.findUDispatchForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getfindUDispatchDetailsById(findUDispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FINDU_DISPATCH, findUDispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.findUDispatchDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.findUDispatchDetails?.findUDispatchName ? this.title +' - '+ this.findUDispatchDetails?.findUDispatchName : this.title + ' --', relativeRouterUrl: '' });
          this.findUDispatchForm.patchValue(response.resources)
          var findUDispatchDivisionList = [];
          response.resources.findUDispatchDivisionList.forEach(element => {
            findUDispatchDivisionList.push(element.divisionId);
          });
          this.findUDispatchForm.get('findUDispatchDivisionList').setValue(findUDispatchDivisionList)

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }


  toggleDivisionOne(all) {
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.findUDispatchForm.controls.findUDispatchDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedDivision.selected) {
      this.findUDispatchForm.controls.findUDispatchDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
    } else {
      this.findUDispatchForm.controls.findUDispatchDivisionList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (this.findUDispatchForm.invalid) {
      return;
    }
    let formValue = this.findUDispatchForm.value;
    if (formValue.findUDispatchDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.findUDispatchDivisionList = formValue.findUDispatchDivisionList.filter(item => item != '')
      formValue.findUDispatchDivisionList = formValue.findUDispatchDivisionList.map(divisionId => ({ divisionId }))
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.findUDispatchId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FINDU_DISPATCH, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FINDU_DISPATCH, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/ro-configuration?tab=3');
      }
    })
  }

  onCRUDRequested(type){}

}
