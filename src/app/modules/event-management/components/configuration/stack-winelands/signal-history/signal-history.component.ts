import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stack-winelands-signal-history',
  templateUrl: './signal-history.component.html',
})
export class StackWinelandsSignalHistoryComponent extends PrimeNgTableVariablesModel implements OnInit {
  @Input() occurrenceBookId: string
  @Input() reload: boolean;
  @Input() isDefaultLoaderDisabled = false;
  primengTableConfigProperties: any;
  row: any = {};

  constructor(private rxjsService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private momentService: MomentService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Signal History",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Signal History' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'stackConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'signalDateTime', header: 'Signal Time', width: '150px', isDateTime: true },
            { field: 'alarmType', header: 'Alarm', width: '150px' },
            { field: 'description', header: 'Alarm Description', width: '150px' },
            { field: 'zoneNo', header: 'Zone/User', width: '150px' },
            { field: 'zoneDescription', header: 'Zone/User Description', width: '150px' },
            { field: 'rawSignal', header: 'Raw Data', width: '150px' },
            { field: 'decoder', header: 'Decoder', width: '150px' },
            { field: 'accountCode', header: 'Transmitter', width: '150px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_SIGNAL_OCCURRENCE_BOOK,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    if (this.occurrenceBookId) {
      let otherParams = {}
      this.getSignalHistoryList(null, null, otherParams);
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.occurrenceBookId) {
      let otherParams = {}
      this.getSignalHistoryList(null, null, otherParams);
    }
  }


  getSignalHistoryList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams['occurrenceBookId'] = this.occurrenceBookId ? this.occurrenceBookId : null;
    otherParams['isDefaultLoaderDisabled'] = this.isDefaultLoaderDisabled;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}