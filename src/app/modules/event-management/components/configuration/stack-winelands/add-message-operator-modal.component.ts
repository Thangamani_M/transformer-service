import { Component, Inject, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudType, CustomDirectiveConfig, defaultPopupType } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { PhonerFeedbackMessageComponent } from '../call-workflow/dialog-model/phoner-feedback-message/phoner-feedback-message.component';
@Component({
  selector: 'add-message-operator-modal-component',
  templateUrl: './add-message-operator-modal.component.html',
})
export class AddMessageOperatorModalComponent extends PrimeNgTableVariablesModel implements OnInit {
  addMessageForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  defaultPopupType=defaultPopupType.ANGULAR_MATERIAL_DRAGGABLE;

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService, private store: Store<AppState>,
    public dialogRef: MatDialogRef<PhonerFeedbackMessageComponent>, private _fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any, public dialogService: DialogService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Signal History",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Signal History' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal History',
            dataKey: 'feedbackMessageId',
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'defaultMessage', header: 'Default Message', width: '100%' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DEFULAT_MESSAGE_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getSignalHistoryList();
    this.createAddForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createAddForm() {
    this.addMessageForm = this._fb.group({
      createdUserId: [this.loggedInUserData.userId],
      occurrenceBookId: [this.data ? this.data : null, Validators.required],
      feedbackMessage: ['', Validators.required],
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.data) {
      let otherParams = {}
      this.getSignalHistoryList(null, null, otherParams);
    }
  }

  getSignalHistoryList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    const obj = { occurrenceBookId: this.data ? this.data : null }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj };
    } else {
      otherParams = obj;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.addMessageForm.get('feedbackMessage').setValue(row['defaultMessage']);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {});
    }
  }

  onSubmit() {
    if (this.addMessageForm.invalid) {
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_FEEDBACK_MESSAGE, this.addMessageForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.addMessageForm.get('feedbackMessage').reset();
          this.rxjsService.setGlobalLoaderProperty(false);
          this.dialogRef.close(false);
        }
      });
  }

  close() {
    this.dialogRef.close(false);
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}