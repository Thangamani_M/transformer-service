import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-logaway-confirm-popup',
  templateUrl: './logaway-confirm-popup.component.html',
})
export class LogawayConfirmPopupComponent implements OnInit {

  showDialogSpinner: boolean = false;
  data: any = [];
  loggedUser: any;

  constructor(public ref: DynamicDialogRef, private rxjsService: RxjsService, private store: Store<AppState>, private httpCancelService: HttpCancelService, public config: DynamicDialogConfig, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
  }

  onSubmit() {
    let formValue = {
      modifiedUserId : this.loggedUser.userId,
      occurrenceBookId: this.config.data.occurrenceBookId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_LOGAWAY, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
        this.ref.close(true)
      }
    })
  }

  close() {
    this.ref.close();
  }

}
