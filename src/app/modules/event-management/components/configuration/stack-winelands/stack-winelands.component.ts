import { ChangeDetectorRef, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CommonPaginationConfig,
  CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, defaultPopupType, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams,
  ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SELECT_REQUIRED_SIGNAL, setRequiredValidator, SignalRTriggers,
  SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { SessionService } from '@app/shared/services/session.service';
import { SignalrConnectionService } from '@app/shared/services/signalr-connection.service';
import { HubConnection } from '@aspnet/signalr';
import { QuickAddServiceCallDialogComponent } from '@modules/customer/components/customer/customer-management/quick-add-service-call-dialog/quick-add-service-call-dialog.component';
import { PasswordPopupComponent } from '@modules/event-management/components/configuration/call-workflow/password-popup/password-popup.component';
import { PrintStackModel } from '@modules/event-management/models/configurations/print-stack.model';
import { SupervisorMessageModel } from '@modules/event-management/models/configurations/supervisor-message.model';
import { SharedClientAssitanceComponent } from '@modules/event-management/shared/components';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DASHBOARD, EVENT_MANAGEMENT_COMPONENT, SERVICES, SHORTCUT_KEYS } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { ShortcutInput } from 'ng-keyboard-shortcuts';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, Observable, of, Subject, Subscription, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, scan, switchMap, takeWhile } from 'rxjs/operators';
import { FilterFormModel } from '../../../models/configurations/filter-form.model';
import { ArmDisarmScheduleDialogComponent } from '../call-workflow/arm-disarm-schedule-dialog/arm-disarm-schedule-dialog.component';
import { EventSignalListComponent } from '../call-workflow/event-signal-list/event-signal-list.component';
import { ResponseInstructionsDetailsComponent } from '../call-workflow/response-instructions-view-details/response-instructions-details/response-instructions-details.component';
import { StackHistoryDetailsComponent } from '../stack-history/stack-history-details/stack-history-details.component';
import { ThirdPartyGenerateUrlComponent } from '../third-party-details/third-party-generate-url.component';
import { AddMessageOperatorModalComponent } from './add-message-operator-modal.component';
import { ClassifyIncidentModalComponent } from './classify-incident-modal.component';
import { LogawayConfirmPopupComponent } from './popup-models/logaway-confirm-popup/logaway-confirm-popup.component';
import { StackDispachPartitionDetailsComponent } from './stack-dispach-partition-details/stack-dispach-partition-details.component';
import { StackDispachPatrolDetailsComponent } from './stack-dispach-patrol-details/stack-dispach-patrol-details.component';
declare var google;

export enum MonitoringUserTypes {
  STACK_OPERATOR = 1,
  STACK_DISPATCHER = 2
}
@Component({
  selector: 'app-stack-winelands',
  templateUrl: './stack-winelands.component.html',
  styleUrls: ['./stack-winelands.component.scss']
})

export class StackWinelandsComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChild('content', { static: true }) content: ElementRef;
  pageSize: number = 10;
  selectedTabIndex = 0;
  primengTableConfigProperties;
  dataList = [];
  loading: boolean;
  selectedColumns = [];
  selectedRows: any = [];
  totalRecords;
  searchKeyword: FormControl;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today = new Date();
  searchColumns;
  row = {};
  filterForm: FormGroup;
  moveToStackForm: FormGroup;
  showFilterForm = false;
  moveToStackDetails;
  getMemoDetails;
  selectionPasswordList = [];
  scrollEnabled = false;
  areaDropDown = [];
  stackDropDown = [];
  queryParamsData: any = {};
  areaName;
  stackName;
  loggedUser;
  holidayInstructionDialog = false;
  signalHistoryDialog = false;
  operatorInstructionsDialog = false;
  showEventMemo: boolean = true;
  passwordDialog = false;
  selectionpasswordDialog = false;
  moveToStackDialog = false;
  occurrenceBookId;
  pastedText = '';
  eventMemoList = [];
  responseInstrcutionDetails;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  selectedTabChangeIndex;
  selectedOccurrenceBookId;
  isAlarmTabShow = false;
  isSignalInfoTabShow = false;
  isRoMessageTabShow = false;
  openOperatorInstructionDetails;
  operatorInstructionDetails: any = [];
  shortcutKeyDialog = false;
  shortcuts: ShortcutInput[] = [];
  printDialog = false;
  memoDialog = false;
  supervisorMessageDialog = false;
  options;
  openMapDialog = false;
  countDetails;
  memoForm: FormGroup;
  map;
  overlays;
  selectedClientData;
  selectedRowIndex = 0;
  showServiceSubmenu = false;
  signalData;
  printStackReportName: string = "";
  printStackForm: FormGroup;
  OBbookColumns = [];
  oBookReportTemplateList = [];
  selectedFieldsRow = [];
  occurrenceBookListReportTemplateId = '';
  selectedFieldsOBook = {};
  eventPrintData: any = {};
  customerAddressId = '';
  displayPrintStack = false;
  areaStackDialog = false;
  isStackAreaLoading: boolean = true
  isStackLoading: boolean = true
  holidayInstructionDetails;
  findUDialog: boolean = false;
  panicAppDialog: boolean = false;
  findUDetails;
  panicAppDetails;
  stackConfigId = '';
  stackAreaConfigId = '';
  startMonitoring;
  selectedRowIndexMessage = '';
  updateMessage;
  onHoldStack: boolean = false;
  allPermissions = [];
  shortcutKeysPermissions = [];
  servicesPermissions = [];
  monitoringUserType: number;
  isLoading = false;
  timer$: Subscription;
  dataListSubject$ = new Subject();
  defaultPopupType = defaultPopupType.P_DIALOG;
  status: any = [];

  constructor(private crudService: CrudService, private sessionService: SessionService, private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService,
    public dialogService: DialogService, private router: Router, private cdr: ChangeDetectorRef,
    private store: Store<AppState>, private momentService: MomentService, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private signalrConnectionService: SignalrConnectionService, private _fb: FormBuilder,
    private httpCancelService: HttpCancelService, public sanitizer: DomSanitizer, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    this.map = google?.maps?.Map;
    this.queryParamsData = {};
    this.queryParamsData['stackAreaConfigId'] = this.sessionService.getItem('stackAreaConfigId') ? this.sessionService.getItem('stackAreaConfigId') : this.activatedRoute.snapshot.queryParams.stackAreaConfigId;
    this.queryParamsData['stackConfigId'] = this.sessionService.getItem('stackConfigId') ? this.sessionService.getItem('stackConfigId') : this.activatedRoute.snapshot.queryParams.stackConfigId;
    if (this.activatedRoute.snapshot.queryParams.isFindSignal) {
      this.queryParamsData['isFindSignal'] = this.activatedRoute.snapshot.queryParams.isFindSignal;
      this.queryParamsData['occurrenceBookId'] = this.activatedRoute.snapshot.queryParams.occurrenceBookId;
    }
    this.areaName = this.activatedRoute.snapshot.queryParams.name;
    this.stackName = this.activatedRoute.snapshot.queryParams.stackName;
    if (this.queryParamsData['stackAreaConfigId']) {
      this.rxjsService.setStackAreaConfigId(this.queryParamsData['stackAreaConfigId']);
    }
    this.primengTableConfigProperties = {
      tableCaption: "",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Dashboard', relativeRouterUrl: '' }, { displayName: 'Event Management', relativeRouterUrl: '' }, { displayName: 'Operator Stack View' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal Management Stack Winelands',
            dataKey: 'occurrenceBookId',
            enableBreadCrumb: true,
            enableScrollable: true,
            checkBox: true,
            rowExpantable: true,
            rowExpantableIndex: 0,
            columns: [{ field: 'displayName', header: 'Client Name', width: '150px' },
            { field: 'fullAddress', header: 'Address', width: '120px' }, { field: 'highPriority', header: 'Priority', width: '80px' },
            { field: 'signalTime', header: 'Time', width: '70px' }, { field: 'subAreaName', header: 'Area', width: '70px' },
            { field: 'operator', header: 'Operator', width: '80px' }, { field: 'message', header: 'Message', width: '120px' },
            { field: 'incident', header: 'Incident', width: '70px' }, { field: 'vehicleStatus', header: 'Vehicle Status', width: '120px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.PHONE_STACK_VIEW,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    // the query params is passed from the openscape incoming call from customer for emergency
    this.activatedRoute.queryParams.subscribe(({ occurrenceBookId }) => {
      if (!occurrenceBookId) return;
      this.areaStackDialog = false;
      this.selectedOccurrenceBookId = occurrenceBookId;
      this.getRequiredListData({ occurrenceBookId }, 'default', 'from OB queryparams');
    });
    this.hubConnectionOperatorStackLiveDataConfig();
    this.getAllDropdowns();
    if (!this.queryParamsData?.stackAreaConfigId && !this.selectedOccurrenceBookId) {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(true);
      this.areaStackDialog = true;
    }
    this.selectedTabIndex = 0;
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.createMoveToStackForm();
    this.createEventMemo();
    this.combineLatestNgrxStoreData();
    this.createFilterForm();
    this.createSupervisorMessageForm();
    if (this.queryParamsData?.stackAreaConfigId) {
      this.filterForm.get('stackAreaConfigId').setValue(this.queryParamsData?.stackAreaConfigId ? this.queryParamsData?.stackAreaConfigId : '')
      this.filterForm.get('stackConfigId').setValue(this.queryParamsData?.stackConfigId ? this.queryParamsData?.stackConfigId : '')
      this.getRequiredListData(this.queryParamsData);
    }
    if (this.showFilterForm) {
      this.filterForm.reset();
    }
    this.options = {
      center: {
        lat: -37.6878,
        lng: 176.1651
      },
      zoom: 11,
      mapTypeId: google?.maps?.MapTypeId?.ROADMAP
    };
    this.startMonitoring = setInterval(() => {
      this.onStackMonitoring()
    }, 10000);
    this.rxjsService.getSelctedRowIndex().subscribe((data: any) => {
      this.selectedRowIndexMessage = data;
    });
    this.onFormControlChanges();
  }

  onFormControlChanges() {
    this.filterForm.get('stackAreaConfigId').valueChanges.subscribe((stackAreaConfigId: string) => {
      this.areaName = this.areaDropDown.find(e => e.id == stackAreaConfigId)?.displayName;
    });
    this.filterForm.get('stackConfigId').valueChanges.subscribe((stackConfigId: string) => {
      this.stackName = this.stackDropDown.find(e => e.id == stackConfigId)?.displayName;
    });
  }

  onStackMonitoring() {
    if (!this.filterForm.value.stackAreaConfigId || !this.filterForm.value.stackConfigId) {
      return;
    }
    let data = {
      userId: this.loggedInUserData.userId,
      stackAreaConfigId: this.filterForm.value.stackAreaConfigId,
      stackConfigId: this.filterForm.value.stackConfigId,
      maximumRows: this.dataList.length > 0 ? this.dataList.length : +CommonPaginationConfig.defaultPageSize,
      occurrenceBookId: this.selectedRows?.occurrenceBookId,
    }
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_MONITORING_PHONER, data).subscribe();
  }

  hubConnectionInstanceForSalesAPI: HubConnection;
  hubConnectionOperatorStackLiveDataConfig() {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
      // real data after the each signal's action the from stack
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.PhonerStackViewChangesTrigger, data => {
        let receivingUser = data?.receivingUsers?.find(d => d.receivingUserId == this.loggedInUserData.userId.toLowerCase());
        if (receivingUser) {
          this.isStackLoading = receivingUser.isForcedStackDistribution;
          this.isStackAreaLoading = receivingUser.isForcedStackDistribution;
          if (data?.phonerStackViewChangedListItems) {
            data?.phonerStackViewChangedListItems?.forEach((phonerStackViewChangedItem) => {
              let filteredOBRowIndex = this.dataList.findIndex(dL => dL['occurrenceBookId'] == phonerStackViewChangedItem['occurrenceBookId']);
              if (filteredOBRowIndex > -1 && this.dataList.find(dL => dL['occurrenceBookId'] == phonerStackViewChangedItem['occurrenceBookId'])) {
                this.dataList.splice(filteredOBRowIndex, 1, phonerStackViewChangedItem);
              }
              else if (filteredOBRowIndex == -1) {
                this.dataList.push(phonerStackViewChangedItem);
              }
            });
          }
          // Prefetch the stack counts and config ID's from the signalR
          if (data?.signals) {
            this.countDetails = data?.signals;
            this.filterForm.get('stackConfigId').setValue(data?.signals.stackConfigId ? data?.signals.stackConfigId :
              this.queryParamsData?.stackConfigId);
            this.filterForm.get('stackAreaConfigId').setValue(data?.signals.stackAreaConfigId ? data?.signals.stackAreaConfigId :
              this.queryParamsData?.stackAreaConfigId);
          }
          // Remove the stack from the dashboard
          data?.phonerStackViewRemovedListItems?.forEach((occurrenceBookId: string, index) => {
            let filteredOBRowIndex = this.dataList.findIndex(dL => dL['occurrenceBookId'] == occurrenceBookId);
            if (filteredOBRowIndex > -1) { // only splice array when item is found
              this.dataList.splice(filteredOBRowIndex, 1);
            }
            // Expand the very next stack once after the last stack is removed from the list
            if (data.phonerStackViewRemovedListItems.length == index + 1 && filteredOBRowIndex > -1) {
              this.selectedRows = this.dataList[(filteredOBRowIndex == this.dataList.length) ? filteredOBRowIndex - 1 : filteredOBRowIndex];
              this.sessionService.setItem('selectedStack', JSON.stringify(this.selectedRows));
              this.sessionService.setItem('selectedRowIndex', filteredOBRowIndex);
              this.onRowClickExpand(this.selectedRows, filteredOBRowIndex);
            }
          });
          this.dataListSubject$.next({ listUpdated: true });
        }
      });
      this.hubConnectionInstanceForSalesAPI.onclose(() => {
      });
    })
      .catch(error => console.error(error, "Sales API Error from operator component..!!"));
  }

  sortTheStackByPriority() {
    // Sort by the priority field of the stack
    this.dataList = this.dataList?.sort((prevObj, nextObj) => {
      if (prevObj.priority === nextObj.priority) {
        // signalDateTime is only important when priorities are the same
        return new Date(nextObj.signalDateTime).valueOf() - new Date(prevObj.signalDateTime).valueOf();
      }
      return prevObj.priority > nextObj.priority ? 1 : -1;
    });
    let selectedStackObject = JSON.parse(this.sessionService.getItem('selectedStack') ?
      this.sessionService.getItem('selectedStack') : "{}");
    if (Object.keys(selectedStackObject).length > 0) {
      if (selectedStackObject) {
        let foundStackIndexAfterSort = this.dataList.findIndex(dL => dL['occurrenceBookId'] == selectedStackObject.occurrenceBookId);
        this.selectedRows = this.dataList[foundStackIndexAfterSort];
        this.onRowClickExpand(this.selectedRows, foundStackIndexAfterSort);
      }
      else {
        this.onRowClickExpand(this.dataList[0], 0);
      }
    }
    else {
      this.onRowClickExpand(this.dataList[0], 0);
    }
    this.scrollToRowExpandedPosition();
  }

  stackDashboardRedirectionLogics() {
    if (this.countDetails?.monitoringUserType == MonitoringUserTypes.STACK_OPERATOR) {
      this.router.navigateByUrl("/event-management/stack-operator");
    }
    else if (this.countDetails?.monitoringUserType == MonitoringUserTypes.STACK_DISPATCHER) {
      this.router.navigateByUrl("/event-management/stack-dispatcher");
    }
    this.filterForm.get('stackConfigId').setValue(this.countDetails?.stackConfigId ? this.countDetails.stackConfigId : this.filterForm.value.stackConfigId);
    this.filterForm.get('stackAreaConfigId').setValue(this.countDetails?.stackAreaConfigId ? this.countDetails.stackAreaConfigId : this.filterForm.value.stackAreaConfigId);
  }

  prepareShortcutObjects() {
    this.shortcuts = [{
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F1,
      label: "Shortcut Keys",
      description: "Acknowledgement",
      command: () => {
        this.get_F1_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F2,
      label: "Shortcut Keys",
      description: "Dispatch",
      command: () => {
        this.get_F2_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F4,
      label: "Shortcut Keys",
      description: "Contact",
      command: () => {
        this.get_F4_ShorcutActions();
      },
      preventDefault: true

    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F5,
      label: "Shortcut Keys",
      description: "On hold / Transfer",
      command: () => {
        this.get_F5_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F6,
      label: "Shortcut Keys",
      description: "Services",
      command: () => {
        this.get_F6_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F7,
      label: "Shortcut Keys",
      description: "Search",
      command: () => {
        this.get_F7_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F9,
      label: "Shortcut Keys",
      description: "Messages",
      command: () => {
        this.get_F9_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F10,
      label: "Shortcut Keys",
      description: "Memo",
      command: () => {
        this.get_F10_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F11,
      label: "Shortcut Keys",
      description: "Incident",
      command: () => {
        this.get_F11_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_F12,
      label: "Shortcut Keys",
      description: "Log away",
      command: () => {
        this.get_F12_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_ENTER,
      label: "Shortcut Keys",
      description: "Client History",
      command: () => {
        this.get_ENTER_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_V,
      preventDefault: true,
      label: "Shortcut Keys",
      description: "Verify Password",
      command: () => {
        this.get_CTRL_V_ShorcutActions();
      },
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_F,
      preventDefault: true,
      label: "Shortcut Keys",
      description: "Find Signal",
      command: () => {
        this.get_CTRL_F_ShorcutActions();
      },
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_HOME,
      label: "Shortcut Keys",
      description: "Top of Stack",
      command: () => {
        this.get_HOME_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_END,
      label: "Shortcut Keys",
      description: "Bottom of Stack",
      command: () => {
        this.get_END_ShorcutActions();
      },
      preventDefault: true
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_S,
      preventDefault: true,
      label: "Shortcut Keys",
      description: "Supervisor message",
      command: () => {
        this.get_CTRL_S_ShorcutActions();
      },
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_A,
      preventDefault: true,
      label: "Shortcut Keys",
      description: "Active Stack",
      command: () => {
        this.get_CTRL_A_ShorcutActions();
      },
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_O,
      preventDefault: true,
      label: "Shortcut Keys",
      description: "On Hold Stack",
      command: () => {
        this.get_CTRL_O_ShorcutActions();
      },
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_G,
      preventDefault: true,
      label: "Shortcut Keys",
      description: "Usefull Numbers",
      command: () => {
        this.get_CTRL_G_ShorcutActions();
      },
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_R,
      preventDefault: true,
      label: "Shortcut Keys",
      description: "Refresh",
      command: () => {
        this.get_CTRL_R_ShorcutActions();
      },
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_M,
      preventDefault: true,
      label: "Shortcut Keys",
      description: "Map",
      command: () => {
        this.get_CTRL_M_ShorcutActions();
      },
    }, {
      key: EventMgntModuleApiSuffixModels.SHORTCUT_ESC,
      label: "Shortcut Keys",
      description: "Exit",
      command: () => {
        this.shortcutKeyDialog = !this.shortcutKeyDialog
      },
      preventDefault: true
    }]
  }

  ngAfterViewInit() {
    // Whenever the table gets updated dynamically the below method calls so that we can scroll once data is loaded fully
    this.dataListSubject$?.subscribe(() => {
      this.sortTheStackByPriority();
    });
    this.prepareShortcutObjects();
    if (this.loggedUser?.roleName.toLowerCase() == 'supervisor' || this.loggedUser?.roleName.toLowerCase() == 'event management supervisor' || this.loggedUser?.roleName.toLowerCase() == 'super admin') {
      this.shortcuts.push(
        {
          key: EventMgntModuleApiSuffixModels.SHORTCUT_F8,
          label: "Shortcut Keys",
          description: "Print",
          command: () => {
            this.get_F8_ShorcutActions();
          },
          preventDefault: true
        },
        {
          key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_H,
          preventDefault: true,
          label: "Shortcut Keys",
          description: "History Stack",
          command: () => {
            this.get_CTRL_H_ShorcutActions();
          },
        },
        {
          key: EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_U,
          preventDefault: true,
          label: "Shortcut Keys",
          description: "Operator Traking",
          command: () => {
            this.get_CTRL_U_ShorcutActions();
          },
        });
    } else {
      this.shortcuts.push(
        {
          key: EventMgntModuleApiSuffixModels.SHORTCUT_F7,
          label: "Shortcut Keys",
          description: "Search",
          command: () => {
            this.get_F7_ShorcutActions();
          },
          preventDefault: true
        },
      );
    }
    this.cdr.detectChanges();
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  showSelectRequiredSignalMessage() {
    return this.snackbarService.openSnackbar(SELECT_REQUIRED_SIGNAL, ResponseMessageTypes.WARNING);
  }

  showPermissionRestricedMessage() {
    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }

  getAllDropdowns() {
    this.getStackAreaConfigDropdown()
    this.getStackConfigDropdown()
  }

  getStackAreaConfigDropdown() {
    this.isStackAreaLoading = true
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_AREA_CONFIG,
      prepareGetRequestHttpParams(null, null, {
        isDefaultLoaderDisabled: true
      }))
      .subscribe((resp: IApplicationResponse) => {
        this.isStackAreaLoading = false
        if (resp.isSuccess && resp.statusCode === 200) {
          this.areaDropDown = resp.resources;
          if (this.queryParamsData['stackAreaConfigId']) {
            this.rxjsService.setStackAreaConfigId(this.queryParamsData['stackAreaConfigId']);
            let areaNameData = this.areaDropDown.find(e => e.id == this.queryParamsData['stackAreaConfigId']);
            this.areaName = areaNameData?.displayName;
          }
        }
      });
  }

  getStackConfigDropdown() {
    this.isStackLoading = true
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_CONFIG,
      prepareGetRequestHttpParams(null, null, {
        isDefaultLoaderDisabled: true
      }))
      .subscribe((resp: IApplicationResponse) => {
        this.isStackLoading = false
        if (resp.isSuccess && resp.statusCode === 200) {
          this.stackDropDown = resp.resources;
          if (this.queryParamsData['stackAreaConfigId']) {
            this.rxjsService.setStackAreaConfigId(this.queryParamsData['stackAreaConfigId']);
            let stackNameData = this.stackDropDown.find(e => e.id == this.queryParamsData['stackConfigId']);
            this.stackName = stackNameData.displayName;
          }
        }
      });
  }

  isSelectedRow() {
    if (this.areaStackDialog == false) {
      if (!this.selectedRows || this.selectedRows.length == 0) {
        this.snackbarService.openSnackbar(SELECT_REQUIRED_SIGNAL, ResponseMessageTypes.WARNING);
        return false;
      } else {
        return true;
      }
    }
    else {
      return false;
    }
  }

  getEventPrintReport(occurrenceBookId) {
    this.rxjsService.setDialogOpenProperty(true);
    let otherParams = {};
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_SIGNAL_REPORT,
      occurrenceBookId,
      false)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess && response.resources) {
          this.eventPrintData = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_EVENT_MEMO_REPORT,
      occurrenceBookId,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess && response.resources) {
          this.eventPrintData.listData = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onBreadCrumbClick(breadCrumbItem: object) {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] });
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`);
    }
  }

  createMoveToStackForm() {
    this.moveToStackForm = this._fb.group({
      occurrenceBookId: [''],
      stackConfigId: [''],
      modifiedUserId: ['']
    });
  }
  createEventMemo() {
    this.memoForm = this.formBuilder.group({
      createdUserId: [Validators.required],
      occurrenceBookId: [Validators.required],
      memo: ['', Validators.required],
      isActives: [true]
    });
  }

  createFilterForm() {
    let filterFormModel = new FilterFormModel();
    this.filterForm = this._fb.group({
    });
    Object.keys(filterFormModel).forEach((key) => {
      this.filterForm.addControl(key, new FormControl(filterFormModel[key]));
    });
    this.filterForm = setRequiredValidator(this.filterForm, ["stackAreaConfigId", "stackConfigId"]);
  }

  submitFilter() {
    this.rxjsService.setDialogOpenProperty(false);
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.filterForm.invalid) {
      this.filterForm.markAllAsTouched();
      return;
    }
    let filteredData = Object.assign({},
      { stackAreaConfigId: this.filterForm.get('stackAreaConfigId').value ? this.filterForm.get('stackAreaConfigId').value : '' },
      { stackConfigId: this.filterForm.get('stackConfigId').value ? this.filterForm.get('stackConfigId').value : '' },
      { onHoldStack: this.filterForm.get('onHoldStack').value ? this.filterForm.get('onHoldStack').value : '' },
    );
    let areaNameData = this.areaDropDown.find(e => e.id == this.filterForm.get('stackAreaConfigId').value);
    this.areaName = areaNameData.displayName;
    let stackNameData = this.stackDropDown.find(e => e.id == this.filterForm.get('stackConfigId').value);
    this.stackName = stackNameData.displayName;
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.getRequiredListData(filterdNewData);
    this.showFilterForm = false;
    this.areaStackDialog = false
    this.queryParamsData['stackAreaConfigId'] = this.filterForm.get('stackAreaConfigId').value ? this.filterForm.get('stackAreaConfigId').value : ''
    this.queryParamsData['stackConfigId'] = this.filterForm.get('stackConfigId').value ? this.filterForm.get('stackConfigId').value : ''
    this.sessionService.setItem('stackAreaConfigId', this.queryParamsData['stackAreaConfigId'])
    this.sessionService.setItem('stackConfigId', this.queryParamsData['stackConfigId'])
  }

  resetForm() {
    this.filterForm.reset()
    this.dataList = [];
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].rowExpantable = false;
    this.primengTableConfigProperties;
    this.getRequiredListData(this.queryParamsData);
    this.showFilterForm = !this.showFilterForm;
  }

  getEventMemoList(occurrenceBookId) {
    this.eventMemoList = undefined;
    this.isLoading = true;
    let otherParams = {};
    otherParams['isDefaultLoaderDisabled'] = true;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_EVENT_MEMO_REPORT,
      occurrenceBookId,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess && response.resources) {
          this.eventMemoList = response.resources;
        } else {
          this.eventMemoList = [];
        }
        this.isLoading = false;
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.allPermissions = response[1][EVENT_MANAGEMENT_COMPONENT.DASHBOARD];
      if (this.allPermissions) {
        this.servicesPermissions = this.allPermissions.find(item => item.menuName == "Services")
        this.shortcutKeysPermissions = this.allPermissions.find(item => item.menuName == "Shortcut Keys")
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.allPermissions);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
          }
          this.scrollEnabled = false;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(otherParams?: object, refreshType = 'default', fromCalling = 'default') {
    if (!otherParams) {
      otherParams = {
        stackAreaConfigId: this.filterForm.get('stackAreaConfigId').value,
        stackConfigId: this.filterForm.get('stackConfigId').value,
        isDefaultLoaderDisabled: true
      };
    }
    else {
      this.loading = true;
    }
    otherParams['monitoringUserId'] = this.loggedUser.userId;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = fromCalling == 'from OB queryparams' ? EventMgntModuleApiSuffixModels.PHONE_STACK_VIEW : this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.scrollEnabled) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.timer$?.unsubscribe();
        this.refreshOperatorStackLiveDataEveryFiveMinutes();
        if (!this.scrollEnabled) {
          if (this.selectedRowIndexMessage) {
            this.dataList = [];
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].rowExpantable = true;
            this.dataList = data.resources;
            if (Object.keys(this.sessionService.getItem('selectedStack') ?
              this.sessionService.getItem('selectedStack') : {}).length > 0) {
              this.getSelectedStackAndIndexFromSessionAndExpand();
            }
            else if (refreshType == 'default' && fromCalling !== 'from OB queryparams') {
              this.onRowClickExpand(this.dataList[this.selectedRowIndexMessage], this.selectedRowIndexMessage);
            }
          }
          else {
            this.dataList = [];
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].rowExpantable = true;
            this.dataList = data.resources;
            if (Object.keys(this.sessionService.getItem('selectedStack') ?
              this.sessionService.getItem('selectedStack') : {}).length > 0) {
              this.getSelectedStackAndIndexFromSessionAndExpand();
            }
            else if (refreshType == 'default' && fromCalling !== 'from OB queryparams') {
              this.onRowClickExpand(this.dataList[0], 0);
            }
          }
        } else {
          data.resources.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
        this.selectedRows = null;
        this.selectedOccurrenceBookId = null;
      }
      if (fromCalling == 'from OB queryparams') {
        this.filterForm.get('stackAreaConfigId').setValue(this.dataList[0]?.stackAreaConfigId);
        this.filterForm.get('stackConfigId').setValue(this.dataList[0]?.stackConfigId);
        this.getCountDetails();
        let filteredOBRowIndex = this.dataList.findIndex(dL => dL['occurrenceBookId'] == otherParams?.['occurrenceBookId']);
        if (filteredOBRowIndex > -1) { // condition is satisfied when stack item is found
          this.selectedRows = this.dataList[filteredOBRowIndex];
          this.sessionService.setItem('selectedStack', JSON.stringify(this.selectedRows));
          this.sessionService.setItem('selectedRowIndex', filteredOBRowIndex);
          this.onRowClickExpand(this.selectedRows, filteredOBRowIndex, 'from ts', true);
        }
      }
      else {
        this.getCountDetails();
      }
      this.dataListSubject$.next({ listUpdated: true });
    }, error => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  refreshOperatorStackLiveDataEveryFiveMinutes() {
    this.timer$ = timer(0, 1000).pipe(scan(acc => --acc, 300),
      takeWhile(pre => pre >= 0))
      .subscribe((timeLeftInSeconds: number) => {
        if (timeLeftInSeconds == 0) {
          this.getRequiredListData();
          this.timer$.unsubscribe();
        }
      });
  }

  getSelectedStackAndIndexFromSessionAndExpand() {
    this.selectedRows = JSON.parse(this.sessionService.getItem('selectedStack'));
    this.selectedRowIndex = +this.sessionService.getItem('selectedRowIndex');
    this.onRowClickExpand(this.dataList[this.selectedRowIndex], this.selectedRowIndex);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string) {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(otherParams);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
      case CrudType.FILTER:
        let isAccessDenied = this.getAllPermissionByActionIconType(DASHBOARD.FILTER);
        if (isAccessDenied) {
          return this.showPermissionRestricedMessage();
        }
        this.showFilterForm = !this.showFilterForm;
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string) {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/call-workflow/call-workflow-list/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/stack-operator/call-workflow-list/list"], { queryParams: { id: editableObject['customerId'] } });
            break;
        }
    }
  }

  navigateToDispatcher() {
    this.rxjsService.setDialogOpenProperty(false);
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.filterForm.invalid) {
      this.filterForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setStackAreaConfigId(this.filterForm.value.stackAreaConfigId);
    this.sessionService.setItem('stackAreaConfigId', this.filterForm.value.stackAreaConfigId);
    this.sessionService.setItem('stackConfigId', this.filterForm.value.stackConfigId);
    this.router.navigate(["/event-management/stack-dispatcher"], {
      queryParams: {
        stackAreaConfigId: this.filterForm.value.stackAreaConfigId,
        stackConfigId: this.filterForm.value.stackConfigId,
        name: this.areaName,
        stackName: this.stackName
      }
    });
  }

  onClickNewTab(a, rawData) {
    let isAccessDenied = this.getAllPermissionByActionIconType(DASHBOARD.ACTION);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    let data = {
      acknowledgedBy: this.loggedInUserData.userId,
      occurrenceBookId: rawData.occurrenceBookId,
    }
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_ACKNOWLEDGEMENT_GENERAL, data).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        window.open(`event-management/stack-operator/call-workflow-list-dashboard/list?id=${rawData.occurrenceBookId}&customerId=${rawData.customerId}&customerAddressId=${rawData.customerAddressId}&partitionId=${rawData?.partitionId}&tab=1`, "_blank");
      }
    });
  }

  onClickThirdPartyLink(data) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(ThirdPartyGenerateUrlComponent, { width: '700px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  onTabChange(event) {
    this.tables.forEach(table => {
      table.rows = 20;
    });
    this.row = {};
    this.columnFilterForm = this._fb.group({});
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(['/event-management/signal-management'], { queryParams: { tab: this.selectedTabIndex } });
    this.getRequiredListData();
  }

  onChangeStatus(rowData, index) {
    this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
      this.primengTableConfigProperties, rowData)?.onClose?.subscribe((result) => {
        if (!result) {
          this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
        }
      });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows) ? [this.selectedRows] : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer, fileName: string) {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  openResponseInstructionDialog(row?: any) {
    let isAccessDenied = this.getAllPermissionByActionIconType(DASHBOARD.RESPONSE_INSTRUCTIONS);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.rxjsService.setDialogOpenProperty(true)
    if (this.selectedRows) {
      let data = {
        occurrenceBookId: this.selectedRows.occurrenceBookId,
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows.customerAddressId,
        responseInstructionDialog: true
      }
      this.dialogService.open(ResponseInstructionsDetailsComponent, {
        showHeader: true,
        header: 'Response Instructions', styleClass: "acknowledgeDialog",
        data: { ...data, }, width: '950px'
      });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  openHolidayInstructionDialog(row) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.HOLIDAY_TEMP_INSTRUCTION_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        OccurrenceBookId: row.occurrenceBookId
      }), 1).subscribe((res) => {
        if (res.resources && res.statusCode && res.isSuccess) {
          this.holidayInstructionDialog = true;
          this.holidayInstructionDetails = res.resources[0];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  escalationDispatch() {
    let data = {
      occurrenceBookId: this.selectedRows['occurrenceBookId'],
      modifiedUserId: this.loggedUser.userId
    }
    this.crudService.update(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.HOLIDAY_TEMP_INSTRUCTION_ESCALATION, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.holidayInstructionDialog = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openOperatorInstructionsDialog(rowData) {
    if (rowData) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SPECIAL_INSTRUCTION_OCCURANCE_BOOK, rowData.occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.resources && response.statusCode == 200) {
            this.operatorInstructionsDialog = true;
            this.openOperatorInstructionDetails = rowData;
            this.operatorInstructionDetails = response.resources;
          }
          else {
            this.operatorInstructionsDialog = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  findUDialogDetails(rowData) {
    if (rowData) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GET_FINDU_DETAILS, rowData.occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.resources && response.statusCode == 200) {
            this.findUDialog = true;
            this.findUDetails = response.resources;
          }
          else {
            this.findUDialog = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  PanicAppDialog(rowData) {
    if (rowData) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GET_PANIC_DETAILS, rowData.occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.panicAppDetails = response.resources;
          }
          this.panicAppDialog = true;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  openSignalHistoryDialog(rowData) {
    this.rxjsService.setDialogOpenProperty(true)
    this.signalHistoryDialog = true;
  }

  openPartitionDialog(row) {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.selectedRows) {
      let data = {
        occurrenceBookId: this.selectedRows.occurrenceBookId,
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows.customerAddressId,
      }
      const dialogReff = this.dialogService.open(StackDispachPartitionDetailsComponent, { showHeader: false, data, width: '750px' });
      dialogReff.onClose.subscribe(result => {
        if (result) {
          let data = this.dataList.findIndex(x => x.occurrenceBookId == result.occurrenceBookId);
          if (data !== -1) {
            this.dataListSubject$.next({ listUpdated: true });
            this.onRowClickExpand(this.dataList[data], data);
          }
          else {
            this.snackbarService.openSnackbar('Selected OB number is not in the list of stack..!!', ResponseMessageTypes.WARNING);
          }
        }
      });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  patrolDetails() {
    let isAccessDenied = this.getAllPermissionByActionIconType('Patrols');
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.rxjsService.setDialogOpenProperty(true)
    if (this.selectedRows) {
      let data = {
        occurrenceBookId: this.selectedRows.occurrenceBookId,
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows.customerAddressId,
      }
      const dialogReff = this.dialogService.open(StackDispachPatrolDetailsComponent, { showHeader: false, data: { ...data, }, width: '750px' });
      dialogReff.onClose.subscribe(result => {
        if (result) {
          let data = this.dataList.findIndex(x => x.occurrenceBookId == result.occurrenceBookId)
          this.onRowClickExpand(this.dataList[data], data)
        }
      });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  navigateToSignalHistory(rowData) {
    window.open(
      `event-management/stack-operator/signal-history?id=${rowData.occurrenceBookId}`, "_blank");
  }

  expandedRows = {}
  refreshStackOperator(refreshType = 'default') {
    let isAccessDenied = this.getAllPermissionByActionIconType(SHORTCUT_KEYS.REFRESH);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (refreshType == 'manual refresh') {
      this.isLoading = true;
    }
    this.eventMemoList = undefined;
    if (!this.queryParamsData?.stackAreaConfigId) {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(true);
      this.areaStackDialog = true
    } else {
      this.dataList = [];
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].rowExpantable = false;
      this.primengTableConfigProperties;
      if (this.filterForm.get('onHoldStack').value) {
        this.queryParamsData = { ...this.queryParamsData, onHoldStack: this.filterForm.get('onHoldStack').value };
        this.queryParamsData['isDefaultLoaderDisabled'] = true;
        this.getRequiredListData(this.queryParamsData, refreshType);
      }
      else if (this.onHoldStack) {
        this.getActiveOrOnHoldStack('onhold');
      }
      else {
        this.queryParamsData['isDefaultLoaderDisabled'] = true;
        this.getRequiredListData(this.queryParamsData, refreshType);
      }
      this.selectedTabChangeIndex = 0;
      this.isAlarmTabShow = false;
      this.isSignalInfoTabShow = false;
      this.isRoMessageTabShow = false;
      this.selectedRows = [];
      this.showEventMemo = true;
    }
  }

  openIncidentModal(data) {
    if (this.selectedRows) {
      this.rxjsService.setDialogOpenProperty(true);
      this.dialogService.open(ClassifyIncidentModalComponent, { showHeader: false, data: { data }, width: '650px' })
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  openPasswordModal() {
    let isAccessDenied = this.getAllPermissionByActionIconType(DASHBOARD.PASSWORD);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      let data = {
        LoggedInUserId: this.loggedUser.userId,
        occurrenceBookId: this.selectedRows.occurrenceBookId,
        customerId: this.selectedRows.customerId,
        addressId: this.selectedRows.customerAddressId,
        partitionId: this.selectedRows?.partitionId
      }
      const dialogReff = this.dialogService.open(PasswordPopupComponent, { showHeader: false, data: { ...data, isPasswordDialog: true }, width: '500px' });
      dialogReff.onClose.subscribe(result => {
        if (result) {
          let data = this.dataList.findIndex(x => x.occurrenceBookId == this.selectedRows.occurrenceBookId);
          this.onRowClickExpand(this.dataList[data], data)
          this.getEventMemoList(this.selectedRows.occurrenceBookId);
        }
      });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  onClickPasswordSelection(passwordList) {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    if (passwordList.fakeWord) {
      let data = {
        occurrenceBookId: this.occurrenceBookId,
        password: passwordList.fakeWord
      }
      this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS_PASSWORD_VALIDATE, data).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && !response.resources.valid) {
          this.selectionpasswordDialog = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          this.rxjsService.setDialogOpenProperty(false);
        }
        else {
          this.selectionpasswordDialog = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
    }
  }

  openMoveToStackModal() {
    let isAccessDenied = this.getAllPermissionByActionIconType(DASHBOARD.MOVE_TO_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      let occurrenceBookId = this.selectedRows['occurrenceBookId'];
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MOVE_TO_STACK, occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.isSuccess && response.resources) {
            this.moveToStackDialog = true;
            this.moveToStackDetails = response.resources;
            this.moveToStackForm.get('stackConfigId').setValue('');
            this.moveToStackForm.get('stackConfigId').setValidators([]);
            this.moveToStackForm.get('stackConfigId').updateValueAndValidity();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  getOccurrenceBookMemo() {
    if (this.selectedRows) {
      let occurrenceBookId = this.selectedRows['occurrenceBookId']
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEMO_ID, occurrenceBookId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.isSuccess && response.resources) {
            this.memoDialog = true;
            this.getMemoDetails = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  transferToStack() {
    this.moveToStackForm.get('stackConfigId').setValidators([Validators.required])
    this.moveToStackForm.get('stackConfigId').updateValueAndValidity()
    if (this.moveToStackForm.invalid) {
      return
    }
    this.rxjsService.setDialogOpenProperty(true)
    let data = {
      occurrenceBookId: this.moveToStackDetails.occurrenceBookId,
      modifiedUserId: this.loggedInUserData.userId,
      stackConfigId: this.moveToStackForm.value.stackConfigId,
    }
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_MOVE, data).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.moveToStackDialog = false;
        this.selectedRows = [];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  get_F1_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.ACKNOWLEDGE);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.onClickNewTab(null, this.selectedRows);
    }
  }

  get_F2_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.DISPATCH);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.router.navigate(['/event-management/stack-dispatcher'],
      {
        queryParams: {
          stackAreaConfigId: this.filterForm.value.stackAreaConfigId,
          stackConfigId: this.filterForm.value.stackConfigId,
          name: this.areaName,
          stackName: this.stackName
        }
      });
  }

  get_F4_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.CONTACT);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.contactNavigation();
    }
  }

  get_F5_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.ON_HOLD_TRANSFER);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.openMoveToStackModal();
    }
  }

  get_F6_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.SERVICES);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.shortcutKeyDialog = false;
    this.showServiceSubmenu = true;
  }

  get_F7_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.SEARCH);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.router.navigate(['customer/advanced-search']);
  }

  get_F8_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.PRINT);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.getOBbookColumns();
      this.getOBbookTemplates();
      this.getEventPrintReport(this.selectedRows.occurrenceBookId);
      this.printDialog = true;
      this.shortcutKeyDialog = false;
    }
  }

  get_F9_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.MESSAGES);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.openMessageModal(this.selectedRows, this.selectedRowIndex);
      this.shortcutKeyDialog = false;
    }
  }

  get_F10_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.MEMO);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.getOccurrenceBookMemo();
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  get_F11_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.INCIDENT);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.openIncidentModal(this.selectedRows);
      this.shortcutKeyDialog = false;
    }
  }

  get_F12_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.LOG_AWAY);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.logAway();
    }
  }

  get_ENTER_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.CLIENT_HISTORY);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.navigateToCustomerViewPage();
    }
  }

  get_CTRL_V_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.VERIFY_PASSWORD);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.openPasswordModal();
    }
  }

  get_CTRL_F_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.FIND_SIGNAL);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.router.navigateByUrl('event-management/find-signal');
  }

  get_HOME_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.TOP_OF_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-body')[0];
    scrollableBody.scrollTop = 0;
  }

  get_END_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.BOTTOM_OF_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    const scrollableBody = this.content.nativeElement.firstChild.getElementsByClassName('ui-scrollpanel-content')[0];
    scrollableBody.scrollTop = 500;
  }

  get_CTRL_S_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.SUPERVISOR_MESSAGE);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.supervisorMessageDialog = true;
      this.shortcutKeyDialog = false;
    }
  }

  get_CTRL_A_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.ACTIVE_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.shortcutKeyDialog = !this.shortcutKeyDialog;
    this.getActiveOrOnHoldStack('active');
  }

  get_CTRL_O_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.ON_HOLD_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.shortcutKeyDialog = !this.shortcutKeyDialog;
    this.getActiveOrOnHoldStack('onhold');
  }


  get_CTRL_H_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.HISTORY_STACK);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.getStackHistoryDetails();
    this.shortcutKeyDialog = false;
  }

  get_CTRL_G_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.USEFULL_NUMBERS);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.router.navigate(['event-management/useful-numbers-contact']);
  }

  get_CTRL_R_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.REFRESH);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.shortcutKeyDialog = false;
    this.refreshStackOperator();
  }

  get_CTRL_M_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.MAP);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.openMap(this.selectedRows);
    }
  }

  get_CTRL_U_ShorcutActions() {
    let isAccessDenied = this.getShortCutKeyPermissionByActionIconType(SHORTCUT_KEYS.OPERATOR_TRAKING);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.shortcutKeyDialog = false;
      this.openOperatorInstructionsDialog(this.selectedRows);
    }
    this.shortcutKeyDialog = false;
  }

  onClickShortCutkey(key) {
    if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F1) {
      this.get_F1_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F2) {
      this.get_F2_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F4) {
      this.get_F4_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F5) {
      this.get_F5_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F6) {
      this.get_F6_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F7) {
      this.get_F7_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F8) {
      this.get_F8_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F9) {
      this.get_F9_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F10) {
      this.get_F10_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F11) {
      this.get_F11_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_F12) {
      this.get_F12_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_ENTER) {
      this.get_ENTER_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_V) {
      this.get_CTRL_V_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_F) {
      this.get_CTRL_F_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_HOME) {
      this.get_HOME_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_END) {
      this.get_END_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_S) {
      this.get_CTRL_S_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_A) {
      this.get_CTRL_A_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_O) {
      this.get_CTRL_O_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_H) {
      this.get_CTRL_H_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_G) {
      this.get_CTRL_G_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_R) {
      this.get_CTRL_R_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_M) {
      this.get_CTRL_M_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_CTRL_U) {
      this.get_CTRL_U_ShorcutActions();
    }
    else if (key == EventMgntModuleApiSuffixModels.SHORTCUT_ESC) {
      this.shortcutKeyDialog = false;
    } else {
      this.shortcutKeyDialog = false;
    }
  }

  navigateToCustomerViewPage() {
    this.rxjsService.setViewCustomerData({
      customerId: this.selectedRows.customerId,
      addressId: this.selectedRows?.customerAddressId,
      customerTab: 4,
      monitoringTab: null,
    });
    this.rxjsService.navigateToViewCustomerPage();
  }

  scrollToRowExpandedPosition() {
    setTimeout(() => {
      let selectedStack = JSON.parse(this.sessionService.getItem('selectedStack'));
      const el = document.getElementById(selectedStack?.['occurrenceBookId']);
      el?.scrollIntoView({
        behavior: "smooth",
        block: "end"
      });
    });
  }

  contactNavigation() {
    let isAccessDenied = this.getAllPermissionByActionIconType(DASHBOARD.CONTACT_NUMBERS);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      window.open(
        `event-management/stack-operator/call-workflow-list/list?id=${this.selectedRows.occurrenceBookId}&customerId=${this.selectedRows.customerId}&customerAddressId=${this.selectedRows.customerAddressId}&contact=${this.rxjsService.setTabActinCallflow(2)}&tab=2`, "_blank");
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  moveToHoldOrUnHold(type: string) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.moveToStackForm.get('stackConfigId').setValidators([]);
    this.moveToStackForm.get('stackConfigId').updateValueAndValidity();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    let payload = {
      occurrenceBookId: this.moveToStackDetails.occurrenceBookId,
      modifiedUserId: this.loggedInUserData.userId
    }
    let apiSuffix = type == 'hold' ? EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_ONHOLD : EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_UNHOLD;
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, apiSuffix, payload).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.moveToStackDialog = false;
        this.selectedRows = [];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  logAway() {
    let isAccessDenied = this.getAllPermissionByActionIconType(DASHBOARD.LOGAWAY);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      let clientName = this.selectedRows['displayName'];
      let obNumber = this.selectedRows['occurrenceBookNumber'];
      let occurrenceBookId = this.selectedRows['occurrenceBookId']
      this.dialogService.open(LogawayConfirmPopupComponent, {
        showHeader: true,
        header: 'Log Away Confirmation',
        baseZIndex: 1000,
        width: '450px',
        data: { clientName: clientName, obNumber: obNumber, occurrenceBookId: occurrenceBookId }
      });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  onRowSelect(event) {
    if (event.data.occurrenceBookId) {
      this.selectedRowIndex = event.index;
      this.sessionService.setItem('selectedStack', JSON.stringify(this.dataList[this.selectedRowIndex]));
      this.sessionService.setItem('selectedRowIndex', this.selectedRowIndex);
      this.selectedOccurrenceBookId = event.data?.occurrenceBookId;
      this.isAlarmTabShow = true;
      this.isSignalInfoTabShow = true;
      this.isRoMessageTabShow = true;
      this.selectedTabChangeIndex = 0;
      this.getEventMemoList(event.data.occurrenceBookId);
      this.supervisorMessageForm.get("occurrenceBookId").setValue(event.data.occurrenceBookId);
      this.supervisorMessageForm.get("isSiteMessage").setValue("true");
      this.selectedClientData = event.data;
    }
  }

  onRowUnselect(event) {
    this.selectedRowIndex = null;
    this.eventMemoList = [];
    this.isAlarmTabShow = false;
    this.isSignalInfoTabShow = false;
    this.isRoMessageTabShow = false;
    this.supervisorMessageForm.get("occurrenceBookId").setValue("");
    this.supervisorMessageForm.get("isSiteMessage").setValue("false");
  }

  onRowClickExpand(rowData, rowIndex?, fromType = 'from ts', isFromQueryParams = false) {
    this.expandedRows[rowData?.['occurrenceBookId']] = true;
    if (fromType == 'from template') {
      // Store in session where it can be retrieved whenever the manual refresh or real data changes in the stack
      this.sessionService.setItem('selectedStack', JSON.stringify(rowData));
      this.sessionService.setItem('selectedRowIndex', rowIndex);
    }
    if (rowData?.occurrenceBookId) {
      this.selectedRowIndex = rowIndex;
      this.selectedRows = rowData;
      // Store in session where it can be retrieved whenever the manual refresh or real data changes in the stack
      this.sessionService.setItem('selectedStack', JSON.stringify(rowData));
      this.sessionService.setItem('selectedRowIndex', rowIndex);
      this.rxjsService.setCustomerDate(rowData);
      this.selectedOccurrenceBookId = rowData.occurrenceBookId;
      this.isAlarmTabShow = true;
      this.isSignalInfoTabShow = true;
      this.isRoMessageTabShow = true;
      this.selectedTabChangeIndex = 0;
      let selectedStack = JSON.parse(this.sessionService.getItem('selectedStack'));
      if (!selectedStack || (selectedStack && selectedStack?.occurrenceBookId !== rowData.occurrenceBookId) || this.eventMemoList?.length == 0 || !this.eventMemoList) {
        this.getEventMemoList(rowData.occurrenceBookId);
      }
      this.supervisorMessageForm.get("occurrenceBookId").setValue(rowData.occurrenceBookId);
      this.supervisorMessageForm.get("isSiteMessage").setValue("true");
      this.selectedClientData = rowData;
    } else {
      this.selectedRows = null;
      this.selectedOccurrenceBookId = null;
      this.eventMemoList = [];
      this.occurrenceBookId = null;
    }
  }

  onRowClickCollapse() {
    this.selectedRowIndex = null;
    this.eventMemoList = [];
    this.isAlarmTabShow = false;
    this.isSignalInfoTabShow = false;
    this.isRoMessageTabShow = false;
  }

  onTabChangeEvent(event) {
    this.selectedTabChangeIndex = event.index;
  }

  onRightClick(event) {
    event.preventDefault();
    if (this.dataList.length > 0) {
      this.shortcutKeyDialog = true;
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  getUpdateMessage() {
    if (this.onHoldStack) {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PHONE_STACK_VIEW,
        undefined, null, prepareGetRequestHttpParams(null, null,
          { monitoringUserId: this.loggedUser.userId, OccurrenceBookId: this.selectedOccurrenceBookId, onHoldStack: this.onHoldStack }))
        .subscribe(response => {
          this.updateMessage = response.resources;
          if (this.updateMessage) {
            this.dataList[this.selectedRowIndexMessage].message = this.updateMessage[0].message;
            this.dataList = [...this.dataList];
          }
          this.getEventMemoList(this.selectedRows?.occurrenceBookId);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PHONE_STACK_VIEW,
        undefined, null, prepareGetRequestHttpParams(null, null,
          { monitoringUserId: this.loggedUser.userId, OccurrenceBookId: this.selectedOccurrenceBookId }))
        .subscribe(response => {
          this.updateMessage = response.resources;
          if (this.updateMessage) {
            this.dataList[this.selectedRowIndexMessage].message = this.updateMessage[0].message;
            this.dataList = [...this.dataList];
          }
          this.getEventMemoList(this.selectedRows?.occurrenceBookId);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  openMessageModal(rowData, selectedRowIndex) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(AddMessageOperatorModalComponent, { width: '700px', disableClose: true, data: rowData.occurrenceBookId });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
        this.rxjsService.setSelctedRowIndex(selectedRowIndex);
        this.getUpdateMessage();
      }
    });
  }

  setMap(event) {
    this.map = event.map;
  }

  openMap(rowData) {
    this.options = {};
    this.overlays = {};
    this.openMapDialog = true;
    this.options = {
      center: { lat: Number(rowData?.currentLatitude), lng: Number(rowData?.currentLongitude) },
      zoom: 12
    };
    setTimeout(() => {
      this.map.setCenter({
        lat: Number(rowData?.currentLatitude),
        lng: Number(rowData?.currentLongitude)
      });
    }, 500);
    this.overlays = [
      new google.maps.Marker({ position: { lat: Number(rowData?.currentLatitude), lng: Number(rowData?.currentLongitude) }, icon: "assets/img/map-icon.png", title: rowData.displayName }),
      new google.maps.Marker({ position: { lat: Number(rowData?.latitude), lng: Number(rowData?.longitude) }, icon: "assets/img/map-icon.png", title: rowData.displayName })
    ];
  }

  getCountDetails() {
    this.stackConfigId = this.filterForm.value.stackConfigId;
    this.stackAreaConfigId = this.filterForm.value.stackAreaConfigId;
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PHONER_STACK_SIGNAL_COUNT,
      prepareGetRequestHttpParams(null, null, {
        StackAreaConfigId: this.filterForm.value.stackAreaConfigId,
        StackConfigId: this.filterForm.value.stackConfigId,
        isDefaultLoaderDisabled: true
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.countDetails = response.resources;
        }
      });
  }

  supervisorMessageForm: FormGroup;
  createSupervisorMessageForm(supervisorMessageModel?: SupervisorMessageModel) {
    let supervisorMessage = new SupervisorMessageModel(supervisorMessageModel);
    this.supervisorMessageForm = this._fb.group({});
    Object.keys(supervisorMessage).forEach((key) => {
      this.supervisorMessageForm.addControl(key, new FormControl(supervisorMessage[key]));
    });
    this.supervisorMessageForm = setRequiredValidator(this.supervisorMessageForm, ["operatorId", "isSiteMessage", "message"]);
    this.supervisorMessageForm.get("createdUserId").setValue(this.loggedUser.userId);
    this.supervisorMessageForm.get("operatorId").setValue(this.loggedUser.userId);
  }

  sendMessage() {
    let data = this.supervisorMessageForm.value;
    data.isSiteMessage = data.isSiteMessage == 'true' ? true : false;
    let _occurrenceBookId = this.selectedRows.occurrenceBookId;
    if (!data.isSiteMessage) _occurrenceBookId = "";
    this.supervisorMessageForm.get("occurrenceBookId").setValue(_occurrenceBookId);
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUPERVISOR_MESSAGE, data).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.supervisorMessageDialog = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.supervisorMessageForm.reset();
      }
    });
  }

  openArmDisarmDialog() {
    let isAccessDenied = this.getServicePermissionByActionIconType(SERVICES.RESCHEDULE_O_C);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.showServiceSubmenu = false;
      this.dialogService.open(ArmDisarmScheduleDialogComponent, {
        header: 'Arm Disarm Schedule',
        width: '950px',
        data: this.selectedRows.occurrenceBookId
      });
    }
  }

  getStackHistoryDetails() {
    this.rxjsService.setDialogOpenProperty(true);
    this.dialogService.open(StackHistoryDetailsComponent, {
      showHeader: true,
      header: 'Stack History',
      baseZIndex: 10000,
      width: '80%',
      height: '80%',
      data: { stackAreaConfigId: this.stackAreaConfigId, stackConfigId: this.stackConfigId, breadcrumFalse: false }
    });
  }

  openQuickAddServiceCall() {
    let isAccessDenied = this.getServicePermissionByActionIconType(SERVICES.ADD_CLIENT_SERVICE_CALL);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.isSelectedRow()) {
      this.showServiceSubmenu = false;
      this.rxjsService.setDialogOpenProperty(true);
      const dialogRef = this.dialog.open(QuickAddServiceCallDialogComponent, {
        width: '750px',
        data: {
          customerId: this.selectedRows.customerId,
          customerAddressId: this.selectedRows.customerAddressId,
          edit: true,
          userId: this.loggedInUserData?.userId,
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        this.rxjsService.setDialogOpenProperty(false);
      });
    }
  }

  onMemoSubmit() {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.memoForm.invalid) {
      return;
    }
    if (this.selectedRows) {
      let formValue = this.memoForm.value;
      formValue.createdUserId = this.loggedUser?.userId,
        formValue.occurrenceBookId = this.selectedRows['occurrenceBookId'];
      this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_MEMO_POST, formValue).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.memoDialog = false;
          this.memoForm.reset();
        }
      });
    }
  }

  copyToClipboard(element) {
    element.select();
    document.execCommand("copy");
  }

  async pasteToClipboard() {
    this.pastedText = await navigator.clipboard.readText();
  }

  memoDialogCancel() {
    this.memoDialog = false;
    this.memoForm.reset();
  }

  ngOnDestroy() {
    if (this.startMonitoring) {
      clearInterval(this.startMonitoring);
    }
    this.dialog.closeAll();
    this.shortcuts = [];
    this.sessionService.removeItem('selectedStack');
    this.sessionService.removeItem('selectedRowIndex');
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setPopupLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(false);
    this.hubConnectionInstanceForSalesAPI?.off(SignalRTriggers.PhonerStackViewChangesTrigger);
  }

  captureEmailAndCell() {
    let isAccessDenied = this.getServicePermissionByActionIconType(SERVICES.CAPTURE_EMAIL_AND_CELL);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    this.dialogService.open(SharedClientAssitanceComponent, {
      showHeader: true,
      header: 'Client Assitance',
      baseZIndex: 10000,
      width: '650px',
      data: { occurrenceBookId: this.selectedOccurrenceBookId }
    });
  }

  opensuspendSignalDialogModal() {
    let isAccessDenied = this.getServicePermissionByActionIconType(SERVICES.TEMP_SUSPEND_SIGNAL);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (!this.isSelectedRow()) return '';
    if (this.selectedRows) {
      this.rxjsService.setDialogOpenProperty(true);
      this.dialogService.open(EventSignalListComponent, { showHeader: false, data: { ...this.selectedRows, }, width: '750px' });
    } else {
      this.showSelectRequiredSignalMessage();
    }
  }

  openPrintStackModal() {
    this.printDialog = false;
    this.displayPrintStack = true;
    this.initOccurrenceBookListForm();
  }

  initOccurrenceBookListForm() {
    let printStackModel = new PrintStackModel();
    this.printStackForm = this._fb.group({});
    Object.keys(printStackModel).forEach((key) => {
      this.printStackForm.addControl(key, new FormControl(printStackModel[key]));
    });
    this.printStackForm = setRequiredValidator(this.printStackForm, ["occurrenceBookListReportTemplateName", "occurrenceBookListReportTemplateColumnsList"]);
    this.printStackForm.get("createdUserId").setValue(this.loggedUser.userId);
  }

  getOBbookColumns() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_BOOK_REPORT_DROPDOWN).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.OBbookColumns = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getOBbookTemplates() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_BOOK_REPORT_LIST).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.oBookReportTemplateList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  loadTemplate(event) {
    let lists = event.value.occurrenceBookListReportTemplateColumns.split(", ");
    let _selectedRow = [];
    this.occurrenceBookListReportTemplateId = event.value.occurrenceBookListReportTemplateId;
    this.printStackForm.get('occurrenceBookListReportTemplateName').setValue(event.value.occurrenceBookListReportTemplateName);
    this.printStackForm.get('occurrenceBookListReportTemplateId').setValue(event.value.occurrenceBookListReportTemplateId);
    this.printStackForm.get('occurrenceBookListReportTemplateColumnsList').setValue(event.value.occurrenceBookListReportTemplateColumnsList);
    for (const list of lists) {
      let data = this.OBbookColumns.find(item => item.displayName == list);
      _selectedRow.push(data);
    }
    this.selectedFieldsRow = _selectedRow;
    this.mapSelectedOBookColumn();
  }

  onRowSelectOBook() {
    this.mapSelectedOBookColumn();
  }

  onRowUnselectBook() {
    this.mapSelectedOBookColumn();
  }

  mapSelectedOBookColumn() {
    this.selectedFieldsOBook = {};
    this.selectedFieldsRow.map(item => {
      this.selectedFieldsOBook[item.displayName] = this._convertText(item.displayName);
    });
  }

  _convertText(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult;
  }

  resetLoadTemplate() {
    this.selectedFieldsRow = [];
    this.occurrenceBookListReportTemplateId = "";
    this.printStackForm.reset();
  }

  saveOBookTemplate() {
    let _ids = [];
    this.selectedFieldsRow.map(item => {
      _ids.push({ occurrenceBookListReportTemplateId: 0, occurrenceBookListReportStaticColumnId: item.id });
    })
    this.printStackForm.get("occurrenceBookListReportTemplateColumnsList").setValue(_ids);
    if (!this.printStackForm.valid) return '';
    let crudService: Observable<IApplicationResponse> = this.occurrenceBookListReportTemplateId ? this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_BOOK_REPORT_LIST, this.printStackForm.value)
      : this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURENCE_BOOK_REPORT_LIST, this.printStackForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getOBbookTemplates();
        this.printStackForm.reset();
        this.printStackForm.get('occurrenceBookListReportTemplateName').setErrors(null);
        this.selectedFieldsRow = [];
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  redirectToAddService() {
    let isAccessDenied = this.getAllPermissionByActionIconType(DASHBOARD.GUARD_REQUEST);
    if (isAccessDenied) {
      return this.showPermissionRestricedMessage();
    }
    if (this.selectedRows) {
      window.open(`billing/risk-watch-guard-service/add-service?customerId=${this.selectedRows.customerId}&addressId=${this.selectedRows.customerAddressId}`);
    }
    else {
      this.showSelectRequiredSignalMessage();
    }
  }

  getActiveOrOnHoldStack(stackType: string) {
    this.dataList = [];
    let payload = {
      monitoringUserId: this.loggedInUserData?.userId,
      stackAreaConfigId: this.queryParamsData.stackAreaConfigId,
      stackConfigId: this.queryParamsData.stackConfigId,
    }
    if (stackType == 'onhold') {
      payload['onHoldStack'] = true;
    }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PHONE_STACK_VIEW, null, false, prepareRequiredHttpParams(payload)).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.dataList = response.resources;
        this.onHoldStack = stackType == 'onhold' ? true : false;
        this.onRowClickExpand(this.dataList[0], 0);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getServicePermissionByActionIconType(actionTypeMenuName): boolean {
    return this.servicesPermissions['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName) ? false : true;
  }

  getAllPermissionByActionIconType(actionTypeMenuName): boolean {
    return this.allPermissions.find(fSC => fSC.menuName == actionTypeMenuName) ? false : true;
  }

  getShortCutKeyPermissionByActionIconType(actionTypeMenuName): boolean {
    return this.shortcutKeysPermissions['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName) ? false : true;
  }
}