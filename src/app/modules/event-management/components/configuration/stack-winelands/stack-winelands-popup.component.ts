import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { StackWinelandsModal } from '@modules/event-management/models/configurations/stack-winelands-modal-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'stack-winelands-popup-component',
  templateUrl: './stack-winelands-popup.component.html',
  styleUrls: ['./stack-winelands.component.scss'],
})
export class StackWinelandsPopupComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  @Output() outputAreaName = new EventEmitter<any>();
  @Output() outputStackName = new EventEmitter<any>();

  leadId: string;
  stackWinelandsModalForm: FormGroup;
  public formData = new FormData();
  userData: any;
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  submitted = false;
  customerId: any;
  areaDropDown: any = [];
  stackDropDown: any = [];
  areaName: any;
  stackName: any;


  @ViewChild('fileInput', null) myFileInputField: ElementRef;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private crudService: CrudService, private router: Router,
    private dialog: MatDialog,  private store: Store<AppState>) {
    this.leadId = this.activatedRoute.snapshot.queryParams.leadId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createStackWinelandsModalForm();
    this.getArea();
    this.getStack();
  }

  createStackWinelandsModalForm(): void {
    let stackWinelandsModal = new StackWinelandsModal();
    this.stackWinelandsModalForm = this.formBuilder.group({
    });

    Object.keys(stackWinelandsModal).forEach((key) => {
      this.stackWinelandsModalForm.addControl(key, new FormControl(stackWinelandsModal[key]));
    });
    this.stackWinelandsModalForm = setRequiredValidator(this.stackWinelandsModalForm, ["stackAreaConfigId", "stackConfigId"]);
  }

  getArea() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_AREA_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.areaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  getStack() {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_WINELANDS_STACK_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stackDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }
  //Validators.email, emailPattern

  onChangeArea(value) {
    value = this.stackWinelandsModalForm.value.stackAreaConfigId;
    let found = this.areaDropDown.find(e => e.id == value);
    this.areaName = found.displayName
  }

  onChangeStack(value) {
    value = this.stackWinelandsModalForm.value.stackConfigId;
    let found = this.stackDropDown.find(e => e.id == value);
    this.stackName = found.displayName
  }

  onSubmit(): void {

    if (this.stackWinelandsModalForm.invalid) {
      this.stackWinelandsModalForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setDialogOpenProperty(false);
    this.dialog.closeAll();
    this.outputData.emit(this.stackWinelandsModalForm.value);
    this.outputAreaName.emit(this.areaName);
    this.outputStackName.emit(this.stackName);
  }

  navigateToDispatcher() {
    if (this.stackWinelandsModalForm.invalid) {
      this.stackWinelandsModalForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setStackAreaConfigId(this.stackWinelandsModalForm.value.stackAreaConfigId);
    this.router.navigate(["/event-management/stack-dispatcher"], {
      queryParams: {
        stackAreaConfigId :this.stackWinelandsModalForm.value.stackAreaConfigId,
        stackConfigId :this.stackWinelandsModalForm.value.stackConfigId,
        name: this.areaName,
        stackName: this.stackName
      }
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
    this.dialog.closeAll();
  }

}
