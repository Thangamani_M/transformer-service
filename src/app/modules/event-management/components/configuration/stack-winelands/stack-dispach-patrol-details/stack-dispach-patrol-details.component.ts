import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { RxjsService, CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, IApplicationResponse, CrudType } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stack-dispach-patrol-details',
  templateUrl: './stack-dispach-patrol-details.component.html',
})
export class StackDispachPatrolDetailsComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,private router: Router,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: 'Supervisor Dashboard', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Operation', relativeRouterUrl: '/event-management/supervisor-dashboard', queryParams: { tab: 7 } }, { displayName: 'Signal History' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'stackConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'createdDate', header: 'Time Of Patrol', width: '150px' },
              { field: 'patrolTypeName', header: 'Patrol Type', width: '100px' },
              { field: 'vehicleRegistrationId', header: 'Vehicle', width: '100px' },
              { field: 'officer', header: 'Officer', width: '100px' },
              { field: 'slipNumber', header: 'Slip Number', width: '130px' },
              { field: 'occurrenceBookNumber', header: 'OBN Number', width: '130px' },
              { field: 'feedback', header: 'Feedback', width: '200px' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_PATROL_HISTORY,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableAction: true,

          }
        ]
      }
    }
  }

  ngOnInit(): void {
    if (this.config?.data?.occurrenceBookId) {
      let otherParams = {}
      this.getSignalHistoryList(null, null, otherParams);
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.config?.data?.occurrenceBookId) {
      let otherParams = {}
      this.getSignalHistoryList(null, null, otherParams);
    }
  }

  getSignalHistoryList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
      const params = { customerId: this.config?.data?.customerId, CustomerAddressId: this.config?.data?.addressId }
      otherParams = { ...otherParams, ...params }; 


    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.resources.length;
          this.isShowNoRecord = this.totalRecords > 0 ? false : true;
        } else {
          this.dataList = null;
          this.isShowNoRecord = true;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalHistoryList(row["pageIndex"], row["pageSize"], unknownVar)
        case CrudType.CREATE:
          this.ref.close()
          this.router.navigate(['customer/manage-customers/customer-verification'], { queryParams: { customerId: this.config?.data?.customerId, siteAddressId: this.config?.data?.addressId } });
          this.rxjsService.setTabIndexForCustomerComponent(4)
          this.rxjsService.setTabIndexForMonitoringAndResponseComponent(8)

          break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  btnCloseClick(){
    this.ref.close();
  }

}