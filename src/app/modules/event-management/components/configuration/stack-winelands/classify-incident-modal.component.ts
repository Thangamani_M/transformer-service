import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'classify-incident-modal-component',
  templateUrl: './classify-incident-modal.component.html',
})
export class ClassifyIncidentModalComponent extends PrimeNgTableVariablesModel implements OnInit {

  addIncidentForm: FormGroup;
  occurrenceBookId: any;
  leadId: string;
  classifyIncidentModalForm: FormGroup;
  userData: any;
  dispatcherData: any;
  ClassifyIncidentModelViewDetails: any;
  filterForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService,
    private crudService: CrudService,
    private momentService: MomentService,
    public dialogService: DialogService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,) {
    super();
    this.leadId = this.activatedRoute.snapshot.queryParams.leadId;
    this.primengTableConfigProperties = {
      tableCaption: "Incident",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'incidentTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'incidentTypeName', header: 'Incident' },
            { field: 'description', header: 'Description' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CLASSIFY_INCIDENT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            ebableFilterActionBtn: true,
            areCheckboxesRequired: true,
          },


        ]
      }
    }

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
    
    this.dispatcherData = this.config?.data?.data;
    this.occurrenceBookId = this.dispatcherData?.occurrenceBookId;
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.onShowValue(this.config?.data.data);
    this.createIncidentForm()
    this.getRequiredListData();
  }

  onShowValue(response?: any) {
    this.ClassifyIncidentModelViewDetails = [
      { name: 'OB Number', value: response?.occurrenceBookNumber },
      { name: 'Customer ID', value: response?.customerNumber },

    ]
  }

  onChangeSelecedRows(event) {
    this.selectedRows = event
  }

  createIncidentForm() {
    this.classifyIncidentModalForm = this.formBuilder.group({
      createdUserId: [this.userData.userId],
      occurrenceBookId: [this.occurrenceBookId ? this.occurrenceBookId : null, Validators.required],
      incidentTypeId: [null, Validators.required],
    })
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    const obj = { occurrenceBookId: this.occurrenceBookId ? this.occurrenceBookId : null }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj };
    } else {
      otherParams = obj;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = []
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;

      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onSubmit(): void {
    if (this.selectedRows.length == 0) {
      return this.snackbarService.openSnackbar('Please select atleast one Incident', ResponseMessageTypes.WARNING)
    }
    let incidentTypeIds: any = []
    this.selectedRows.forEach(element => {
      incidentTypeIds.push(element['incidentTypeId'])
    })
    this.classifyIncidentModalForm.get('incidentTypeId').setValue(incidentTypeIds)
    if (this.classifyIncidentModalForm.invalid) {
      this.classifyIncidentModalForm.markAllAsTouched();
      return;
    }
    let formValue = this.classifyIncidentModalForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_INCIDENT_TYPE, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.ref.close(true)
        }
      })
    this.rxjsService.setDialogOpenProperty(false);
  }

  btnCloseClick() {
    this.ref.close(false)
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
