import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-alarm-info-winelands-list',
  templateUrl: './alarm-info-winelands-list.component.html',
  styleUrls: ['./stack-winelands.component.scss'],
})
export class AlarmInfoWinelandsListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @Input() occurrenceBookId: string;
  isShowNoRecords: boolean = true;

  @Input() isDefaultLoaderDisabled = false;
  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'customerId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'signalInfo', header: 'Signal Info', width: '80px' },
            { field: 'actionDateTime', header: 'Time', width: '80px', isDate:true },
            { field: 'measureTime', header: 'Measure', width: '80px' },
            { field: 'displayName', header: 'User', width: '50px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_ALARM_INFO,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            ebableFilterActionBtn: false
          },
        ]
      }
    }
  }

  ngOnInit(): void {
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (this.occurrenceBookId) {
      this.getRequiredListData();
    }
  }


  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    if (!otherParams && this.isDefaultLoaderDisabled == true) {
      otherParams = {};
      otherParams['isDefaultLoaderDisabled'] = this.isDefaultLoaderDisabled;
    }
    else if (this.isDefaultLoaderDisabled == true && otherParams) {
      otherParams['isDefaultLoaderDisabled'] = this.isDefaultLoaderDisabled;
    }
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    let selectedTabIndex = this.selectedTabIndex ? this.selectedTabIndex : 0
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      this.occurrenceBookId ? this.occurrenceBookId : null,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.isShowNoRecords = false;
        this.totalRecords = 0     
      } else {
        this.dataList = null
        this.isShowNoRecords = false;
        this.totalRecords = 0;
      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }
}