import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { TimeOnSiteAddEditComponent } from './time-on-site-add-edit.component';
import { TimeOnSiteRoutingModule } from './time-on-site-routing.module';
import { TimeOnSiteViewComponent } from './time-on-site-view.component';



@NgModule({
  declarations: [TimeOnSiteAddEditComponent, TimeOnSiteViewComponent],
  imports: [
    CommonModule,
    TimeOnSiteRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TimeOnSiteModule { }
