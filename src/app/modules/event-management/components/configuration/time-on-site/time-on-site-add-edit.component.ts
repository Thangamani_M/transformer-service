import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { TimeOnSiteMainAreaListModel, TimeOnSiteModel } from '@modules/event-management/models/configurations/time-on-site.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-time-on-site-add-edit',
    templateUrl: './time-on-site-add-edit.component.html',
})
export class TimeOnSiteAddEditComponent implements OnInit {
    timeOnSiteId: any
    divisionDropDown: any = [];
    mainAreaDropDown: any = [];
    timeOnSiteForm: FormGroup;
    timeOnSiteMainAreaList: FormArray;
    loggedUser: any;
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    timeOnSiteDetails: any;
    isDuplicate: boolean;
    @ViewChildren('input') rows: QueryList<any>;
    totaltab = 0;
    selectedTab = 0;
    primengTableConfigProperties: any;
    title:string;

    constructor(private activatedRoute: ActivatedRoute,
        private snackbarService: SnackbarService,
        private router: Router,
        private httpCancelService: HttpCancelService,
        private store: Store<AppState>,
        private formBuilder: FormBuilder,
        private rxjsService: RxjsService,
        private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
        private crudService: CrudService) {
        this.timeOnSiteId = this.activatedRoute.snapshot.queryParams.id;
        this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
        this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.title =this.timeOnSiteId ? 'Update Time OnSite':'Add Time OnSite';
        this.primengTableConfigProperties = {
          tableCaption: this.title,
          selectedTabIndex: 0,
          breadCrumbItems: [{ displayName: 'RO Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
          { displayName: 'Time OnSite', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 12 } }],
          tableComponentConfigs: {
            tabsList: [
              {
                enableBreadCrumb: true,
                enableAction: false,
                enableEditActionBtn: false,
                enableClearfix: true,
              }
            ]
          }
        }
        if(this.timeOnSiteId){
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/ro-configuration/time-on-site/view' , queryParams: {id: this.timeOnSiteId,totalTabs:this.totaltab, selectedTab:this.selectedTab}});
        }
        else{
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
        }
    }

    ngOnInit() {
        this.createTimeOnSiteForm();
        this.getDivisionDropDownById();
        this.timeOnSiteForm.get("divisionId").valueChanges.subscribe(data => {
            this.getMainAreaDropDownById(data);
        })
        this.rxjsService.setGlobalLoaderProperty(false);
        if (this.timeOnSiteId) {
            this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
                let cmcSmsGroup = new TimeOnSiteModel(response.resources);
                this.timeOnSiteDetails = response.resources;
                this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.timeOnSiteDetails?.divisionName ? this.title +' - '+ this.timeOnSiteDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
                this.timeOnSiteForm.patchValue(cmcSmsGroup);
                this.timeOnSiteMainAreaList = this.getTimeOnSiteMainAreaListArray;
                response.resources.timeOnSiteMainAreaList.forEach((timeOnSiteMainAreaListModel) => {
                    timeOnSiteMainAreaListModel.timeOnSiteMainAreaId = timeOnSiteMainAreaListModel.timeOnSiteMainAreaId;
                    this.timeOnSiteMainAreaList.push(this.createTimeOnSiteMainAreaListModel(timeOnSiteMainAreaListModel));
                });
            })
        } else {
            this.timeOnSiteMainAreaList = this.getTimeOnSiteMainAreaListArray;
            this.timeOnSiteMainAreaList.push(this.createTimeOnSiteMainAreaListModel());
        }
    }

    createTimeOnSiteForm(): void {
        let timeOnSiteModel = new TimeOnSiteModel();
        this.timeOnSiteForm = this.formBuilder.group({
            timeOnSiteMainAreaList: this.formBuilder.array([])
        });
        Object.keys(timeOnSiteModel).forEach((key) => {
            this.timeOnSiteForm.addControl(key, new FormControl(timeOnSiteModel[key]));
        });
        this.timeOnSiteForm = setRequiredValidator(this.timeOnSiteForm, ["divisionId"]);
        this.timeOnSiteForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.timeOnSiteForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    }

    getDivisionDropDownById() {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    this.divisionDropDown = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    getMainAreaDropDownById(selectedDivisionId) {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    this.mainAreaDropDown = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    //Create FormArray
    get getTimeOnSiteMainAreaListArray(): FormArray {
        if (!this.timeOnSiteForm) return;
        return this.timeOnSiteForm.get("timeOnSiteMainAreaList") as FormArray;
    }

    //Create FormArray controls
    createTimeOnSiteMainAreaListModel(timeOnSiteMainAreaListModel?: TimeOnSiteMainAreaListModel): FormGroup {
        let timeOnSiteMainAreaListModelFormControlModel = new TimeOnSiteMainAreaListModel(timeOnSiteMainAreaListModel);
        let formControls = {};
        Object.keys(timeOnSiteMainAreaListModelFormControlModel).forEach((key) => {
            if (key === 'mainAreaId' || key === 'timeOnSite' || key === 'isSiteAverage') {
                formControls[key] = [{ value: timeOnSiteMainAreaListModelFormControlModel[key], disabled: false }, [Validators.required]];
                formControls['timeOnSite'] = [{ value: timeOnSiteMainAreaListModelFormControlModel['timeOnSite'], disabled: false }, [Validators.required, Validators.min(1), Validators.max(9999)]];

            } else if (this.timeOnSiteId) {
                formControls[key] = [{ value: timeOnSiteMainAreaListModelFormControlModel[key], disabled: false }]
            }
            else {
                formControls[key] = [{ value: timeOnSiteMainAreaListModelFormControlModel[key], disabled: false }]
            }
        });
        return this.formBuilder.group(formControls);
    }

    //Get Details 
    getCmcSmsDetailsById(): Observable<IApplicationResponse> {
        return this.crudService.get(
            ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            EventMgntModuleApiSuffixModels.TIME_ON_SITE,
            this.timeOnSiteId
        );
    }
    onSelectDivision() {
        this.isDuplicate = false;
        const arr = <FormArray>this.timeOnSiteForm.controls.timeOnSiteMainAreaList;
        arr.controls = [];
        this.timeOnSiteMainAreaList = this.getTimeOnSiteMainAreaListArray;
        this.timeOnSiteMainAreaList.push(this.createTimeOnSiteMainAreaListModel());
    }

    OnChange(index): boolean {
        if (this.getTimeOnSiteMainAreaListArray.length > 1) {
            this.isDuplicate = false;
            var cloneArray = this.getTimeOnSiteMainAreaListArray.value.slice();
            cloneArray.splice(index, 1);
            var findIndex = cloneArray.some(x => x.mainAreaId === this.getTimeOnSiteMainAreaListArray.value[index].mainAreaId);
            if (findIndex) {
                this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
                this.isDuplicate = true;
                return false;
            } else {
                this.isDuplicate = false;
            }

        }
    }


    focusInAndOutFormArrayFields(): void {
        this.rows.forEach((item) => {
            item.nativeElement.focus();
            item.nativeElement.blur();
        })
    }
    //Add Employee Details
    addCmcSmsGroupEmployee(): void {
        if (this.getTimeOnSiteMainAreaListArray.invalid) {
            this.focusInAndOutFormArrayFields();
            return;
        };
        if (this.isDuplicate) {
            this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
            return;
        }
        this.timeOnSiteMainAreaList = this.getTimeOnSiteMainAreaListArray;
        let timeOnSiteMainAreaListModel = new TimeOnSiteMainAreaListModel();
        this.timeOnSiteMainAreaList.insert(0, this.createTimeOnSiteMainAreaListModel(timeOnSiteMainAreaListModel));
        this.rxjsService.setFormChangeDetectionProperty(true);

    }

    removeCmcSmsEmployee(i: number): void {
        this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
            onClose?.subscribe(dialogResult => {
                if (dialogResult) {
                    if (this.getTimeOnSiteMainAreaListArray.length === 1) {
                        this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
                        return
                    }
                    if (this.getTimeOnSiteMainAreaListArray.controls[i].value.timeOnSiteMainAreaId && this.getTimeOnSiteMainAreaListArray.length > 1) {
                        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.TIME_ON_SITE_MAIN_AREA,
                            this.getTimeOnSiteMainAreaListArray.controls[i].value.timeOnSiteMainAreaId).subscribe((response: IApplicationResponse) => {
                                if (response.isSuccess) {
                                    this.getTimeOnSiteMainAreaListArray.removeAt(i);
                                }
                                if (this.getTimeOnSiteMainAreaListArray.length === 0) {
                                    this.addCmcSmsGroupEmployee();
                                };
                            });
                    }
                    else {
                        this.getTimeOnSiteMainAreaListArray.removeAt(i);
                        this.rxjsService.setFormChangeDetectionProperty(true);
                    }
                }
            });
    }


    onSubmit(): void {
        if (this.timeOnSiteForm.invalid) {
            return;
        }

        if (this.isDuplicate) {
            this.snackbarService.openSnackbar("Main Area already exist", ResponseMessageTypes.WARNING);
            return;
        }

        let formValue = this.timeOnSiteForm.value;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> = (!this.timeOnSiteId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.TIME_ON_SITE, formValue) :
            this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.TIME_ON_SITE, formValue)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
                this.router.navigateByUrl('/event-management/ro-configuration?tab=12');
            }
        })
    }

    onCRUDRequested(type){}

}

