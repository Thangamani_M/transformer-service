import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-time-on-site-view',
  templateUrl: './time-on-site-view.component.html',
})
export class TimeOnSiteViewComponent implements OnInit {

  timeOnSiteId: any
  timeOnSiteDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any
  timeOnSiteDetailsViewDetails: any
  safetyAlertConfigMainAreasList: any;
  divisionId: any;
  mainAreaDropDown: any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
      tableComponentConfigs: {
        tabsList: []
      }
    };

  constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.timeOnSiteId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View Time OnSite',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/ro-configuration' },
      { displayName: 'Time OnSite', relativeRouterUrl: '/event-management/ro-configuration', queryParams: { tab: 12 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData()
    if (this.timeOnSiteId) {
      this.gettimeOnSiteDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.divisionId = response.resources.divisionId
        if (this.divisionId) {
          this.getMainAreaDropDownById()
        }
        this.timeOnSiteDetails = response.resources;
        this.onShowValue(response.resources);
        this.safetyAlertConfigMainAreasList = response.resources.safetyAlertConfigMainAreasList
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + response.resources.divisionName;
      })
    }

    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.RO_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  gettimeOnSiteDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.TIME_ON_SITE,
      this.timeOnSiteId
    );
  }

  getMainAreaDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, this.divisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onShowValue(response?: any) {
    this.timeOnSiteDetailsViewDetails = [
      { name: 'Division', value: response?.divisionName },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Modified By', value: response?.modifiedUserName },
    ]
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['event-management/ro-configuration/time-on-site/add-edit'], { queryParams: { id: this.timeOnSiteId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
  }

}