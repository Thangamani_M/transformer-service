import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventConfigurationListComponent } from './event-configuration-list/event-configuration-list.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  {
    path: '', component: EventConfigurationListComponent, data: { title: 'Event Configuration List' },canActivate: [AuthGuard]
  },
  { path: 'stack-config', loadChildren: () => import('../configuration/stack-config/stack-config.module').then(m => m.StackConfigModule) },
  { path: 'stack-areas', loadChildren: () => import('../configuration/stack-areas/stack-areas.module').then(m => m.StackAreasModule) },
  { path: 'namedStack-config', loadChildren: () => import('../configuration/named-stack-config/named-stack-config.module').then(m => m.NamedStackConfigModule) },
  { path: 'ptt-registration', loadChildren: () => import('../configuration/ptt-registration/ptt-registration.module').then(m => m.PttRegistrationModule) },
  { path: 'pda-registration', loadChildren: () => import('../configuration/pda-registration/pda-registration.module').then(m => m.PdaRegistrationModule) },
  { path: 'vehicle-registration', loadChildren: () => import('../configuration/vehicle-registration/vehicle-registration.module').then(m => m.VehicleRegistrationModule) },
  { path: 'alarm-group', loadChildren: () => import('../configuration/alarm-group/alarm-group.module').then(m => m.AlarmGroupModule) },
  { path: 'boundary-limit', loadChildren: () => import('../configuration/boundary-limit/boundary-limit.module').then(m => m.BoundaryLimitModule) },
  { path: 'delay-to-duration', loadChildren: () => import('../configuration/delay-to-duration/delay-to-duration.module').then(m => m.DelayToDurationModule) },
  { path: 'safe-entry-limit', loadChildren: () => import('../configuration/safe-entry-limit/safe-entry-limit.module').then(m => m.SafeEntryLimitModule) },
  { path: 'ro-accept-time', loadChildren: () => import('../configuration/ro-accept-time/ro-accept-time.module').then(m => m.RoAcceptTimeModule) },
  { path: 'signal-automation', loadChildren: () => import('../configuration/signal-automation/signal-automation.module').then(m => m.SignalAutomationModule) },
  { path: 'cmc-sms-group', loadChildren: () => import('../configuration/cmc-sms-group/cmc-sms-group.module').then(m => m.CmcSmsGroupModule) },

  { path: 'cluster-config', loadChildren: () => import('../configuration/cluster-config/cluster-config.module').then(m => m.ClusterConfigModule) },
  { path: 'event-type', loadChildren: () => import('../configuration/event-type/event-type.module').then(m => m.EventTypeModule) },
  { path: 'bolo-distance', loadChildren: () => import('../configuration/bolo-distance/bolo-distance.module').then(m => m.BoloDistanceModule) },
  { path: 'shift-change-way-point', loadChildren: () => import('../configuration/shift-change-way-point/shift-change-way-point.module').then(m => m.ShiftChangeWayPointModule) },
  { path: 'suspend-delay', loadChildren: () => import('../configuration/suspend-delay/suspend-delay.module').then(m => m.SuspendDelayModule) },
 
  { path: 'third-party-details', loadChildren: () => import('../configuration/third-party-details/third-party-details.module').then(m => m.ThirdPartyDetailsModule) },
  { path: 'distribution-group', loadChildren: () => import('../configuration/distribution-group/distribution-group.module').then(m => m.DistributionGroupModule) },

  { path: 'named-stack-mapping', loadChildren: () => import('../configuration/named-stack-mapping/named-stack-mapping.module').then(m => m.NamedStackMappingModule) },
  { path: 'delay-dispatch', loadChildren: () => import('../configuration/delay-dispatch/delay-dispatch.module').then(m => m.DelayDispatchModule) },
 
  { path: 'community-patrol', loadChildren: () => import('../configuration/community-patrol/community-patrol.module').then(m => m.CommunityPatrolModule) },
  { path: 'stats-config', loadChildren: () => import('../configuration/stats-config/stats-config.module').then(m => m.StatsConfigModule) },
  { path: 'default-message-config', loadChildren: () => import('../configuration/default-message-config/default-message-config.module').then(m => m.DefaultMessageConfigModule) },
  { path: 'client-request-type', loadChildren: () => import('../configuration/client-request-type/client-request-type.module').then(m => m.ClientRequestTypeModule) },

  { path: 'decoder-account-config', loadChildren: () => import('../configuration/decoder-account-config/decoder-account-config.module').then(m => m.DecoderAccountConfigModule) },
  { path: 'fail-to-test', loadChildren: () => import('../configuration/fail-to-test/fail-to-test.module').then(m => m.FailToTestModule) },
  { path: 'patrol-config', loadChildren: () => import('../configuration/patrol-config/patrol-config.module').then(m => m.PatrolConfigModule) },
  { path: 'cell-panic-config', loadChildren: () => import('../configuration/cell-panic-config/cell-panic-config.module').then(m => m.CellPanicConfigModule) },
  { path: 'cmc-service-level-config', loadChildren: () => import('../configuration/cmc-service-level-config/cmc-service-level-config.module').then(m => m.CMCServiceLevelConfigModule) },
  { path: 'erb-exclusion-reason-config', loadChildren: () => import('../configuration/erb-ex-reason-config/erb-ex-reason-config.module').then(m => m.ERBExcReasonConfigModule) },
  { path: 'visibility-logging-config', loadChildren: () => import('../configuration/visibility-logging-config/visibility-logging-config.module').then(m => m.VisibilityLoggingConfigModule) },
  { path: 'signal-influx-classification', loadChildren: () => import('../configuration/signal-influx-classification/signal-influx-classification.module').then(m => m.SignalInfluxClassificationModule) },
  { path: 'operator-interactions', loadChildren: () => import('../configuration/instructor-instructions/instructor-instruction.module').then(m => m.InstructorInstructionModule) },
  { path: 'open-close-setup', loadChildren: () => import('../configuration/open-close-setup/open-close-setup.module').then(m => m.OpenCloseSetupModule) },
  { path: 'signal-pattern', loadChildren: () => import('../configuration/signal-pattern/signal-pattern.module').then(m => m.SignalPatternModule) },
  { path: 'spike-identificaton-config', loadChildren: () => import('../configuration/spike-identification-config/spike-identification-config.module').then(m => m.SpikeIdentificationConfigModule) },
  { path: 'unknown-signal-config', loadChildren: () => import('../configuration/unknown-signal-config/unknown-signal-config.module').then(m => m.UnknownSignalConfigModule) },
  { path: 'ptt-stack-area-config', loadChildren: () => import('../configuration/ptt-stack-area-config/ptt-stack-area-config.module').then(m => m.PttStackAreaConfigModule) },
  { path: 'vas-config', loadChildren: () => import('../configuration/vas-config/vas-config.module').then(m => m.VasConfigModule) },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class EventConfigurationRoutingModule { }
