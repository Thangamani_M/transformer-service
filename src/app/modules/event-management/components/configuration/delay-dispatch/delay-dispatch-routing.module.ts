import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DelayDispatchAddEditComponent } from "./delay-dispatch-add-edit.component";
import { DelayDispatchViewComponent } from "./delay-dispatch-view.component";
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: "", redirectTo: "add-edit", pathMatch: "full" },
  {
    path: "add-edit",
    component: DelayDispatchAddEditComponent,
    data: { title: "Delay Dispatch Add/Edit" },
  },
  {
    path: "view",
    component: DelayDispatchViewComponent,
    data: { title: "Delay Dispatch View" },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DelayDispatchRoutingModule {}
