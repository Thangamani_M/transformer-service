import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { DelayDispatchModel, PropertydelayDispatchTimingListModel } from '@modules/event-management/models/configurations/delay-dispatch.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-delay-dispatch-add-edit',
  templateUrl: './delay-dispatch-add-edit.component.html',
})
export class DelayDispatchAddEditComponent implements OnInit {

  delayDispatchId: any;
  delayDispatchDetails: any;
  divisionDropDown: any = [];
  mainAreaDropdown: any = [];
  loggedUser: any;
  delayDispatchForm: FormGroup;
  delayDispatchTimingList: FormArray;
  delayDispatchTimingId: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChildren('input') rows: QueryList<any>;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private snakBarService: SnackbarService, public momentService: MomentService,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    this.delayDispatchId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.delayDispatchId ? 'Update Delay Dispatch':'Add Delay Dispatch';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Delay Dispatch', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 11 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.delayDispatchId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/delay-dispatch/view' , queryParams: {id: this.delayDispatchId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createdelayDispatchForm();
    this.getDivisionDropDownById();
    this.delayDispatchForm.get("divisionId").valueChanges.subscribe(data => {
      if (data) {
        this.getMainAreaDropDown(data);
      }
    })
    if (this.delayDispatchId) {
      this.getdelayDispatchDetailsById(this.delayDispatchId);
      return
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createdelayDispatchForm(): void {
    let delayDispatchModel = new DelayDispatchModel();
    // create form controls dynamically from model class
    this.delayDispatchForm = this.formBuilder.group({
      delayDispatchTimingList: this.formBuilder.array([])
    });
    Object.keys(delayDispatchModel).forEach((key) => {
      this.delayDispatchForm.addControl(key, new FormControl(delayDispatchModel[key]));
    });
    this.delayDispatchForm = setRequiredValidator(this.delayDispatchForm, ["delayDispatchName", "delayDispatchDescription", "divisionId", "delayDispatchMainAreaList", "delayDispatchTimingList"]);
    this.delayDispatchForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.delayDispatchForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.delayDispatchId) {
      this.delayDispatchForm.removeControl('delayDispatchId');
      this.delayDispatchForm.removeControl('modifiedUserId');
      this.delayDispatchTimingList = this.getpropertydelayDispatchTimingListArray;
      this.delayDispatchTimingList.push(this.createpropertydelayDispatchTimingListModel());
    } else {
      this.delayDispatchForm.removeControl('createdUserId');

    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaDropDown(divisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, divisionId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onChangeDivition(val) {
    this.delayDispatchForm.get('delayDispatchMainAreaList').setValue(null)
  }

  getdelayDispatchDetailsById(delayDispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DELAY_DISPATCH, delayDispatchId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.delayDispatchDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.delayDispatchDetails?.delayDispatchName ? this.title +' - '+ this.delayDispatchDetails?.delayDispatchName : this.title + ' --', relativeRouterUrl: '' });
          this.delayDispatchDetails.delayDispatchMainAreaList.forEach(element => {
            element.id = element.mainAreaId
          });
          this.delayDispatchForm.patchValue(this.delayDispatchDetails);
          setTimeout(() => {
            this.delayDispatchForm.patchValue(this.delayDispatchDetails);         // To resolve form min value data binding validation issue
          }, 0);
          this.delayDispatchTimingList = this.getpropertydelayDispatchTimingListArray;
          response.resources.delayDispatchTimingList.forEach((propertydelayDispatchTimingListModel) => {
            propertydelayDispatchTimingListModel.timeRangeFrom = this.formateTime(propertydelayDispatchTimingListModel.timeRangeFrom)
            propertydelayDispatchTimingListModel.timeRangeTo = this.formateTime(propertydelayDispatchTimingListModel.timeRangeTo)
            this.delayDispatchTimingList.push(this.createpropertydelayDispatchTimingListModel(propertydelayDispatchTimingListModel));
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSelectedValue(event) {
    if (!event) return;
  }

  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date)
  }

  ifEmpty(val) {
    if (!val)
      this.snakBarService.openSnackbar('Select Start Time', ResponseMessageTypes.WARNING);
    return
  }
  todayEndTime(dateTime) {
    if (!dateTime) return
    let date = new Date(dateTime).setHours(23, 59)
    return new Date(date)
  }
  //Create FormArray
  get getpropertydelayDispatchTimingListArray(): FormArray {
    if (!this.delayDispatchForm) return;
    return this.delayDispatchForm.get("delayDispatchTimingList") as FormArray;
  }

  //Create FormArray controls
  createpropertydelayDispatchTimingListModel(propertydelayDispatchTimingListModel?: PropertydelayDispatchTimingListModel): FormGroup {
    let propertydelayDispatchTimingListFormControlModel = new PropertydelayDispatchTimingListModel(propertydelayDispatchTimingListModel);
    let formControls = {};
    Object.keys(propertydelayDispatchTimingListFormControlModel).forEach((key) => {
      if (key === 'timeRangeFrom' || key === 'timeRangeTo') {
        formControls[key] = [{ value: propertydelayDispatchTimingListFormControlModel[key], disabled: false }, [Validators.required]]
      }
      else {
        formControls[key] = [{ value: propertydelayDispatchTimingListFormControlModel[key], disabled: false }]
      }
    });
    let groupCongrolsGroup = this.formBuilder.group(formControls);
    groupCongrolsGroup.get('timeRangeFrom').valueChanges
      .subscribe(val => {
        if (val) {
          groupCongrolsGroup.get('timeRangeTo').setValue(null)
        }
      })
    groupCongrolsGroup.get('isSiteAverage').setValue(true)
    groupCongrolsGroup.get('isSiteAverage').valueChanges
      .subscribe(val => {
        if (val == false) {
          groupCongrolsGroup.get('delayTime').setValidators([Validators.required, Validators.min(10), Validators.max(9999)])
        }
        else if (val == true) {
          groupCongrolsGroup.get('delayTime').clearValidators()

        }
      })
    return groupCongrolsGroup
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (this.getpropertydelayDispatchTimingListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.delayDispatchTimingList = this.getpropertydelayDispatchTimingListArray;
    let propertydelayDispatchTimingListModel = new PropertydelayDispatchTimingListModel();
    this.delayDispatchTimingList.insert(0, this.createpropertydelayDispatchTimingListModel(propertydelayDispatchTimingListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.delayDispatchTimingList.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if(this.delayDispatchTimingList.value[i].delayDispatchTimingId){
              if (this.delayDispatchTimingList.length > 1) {
        
                this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DELAY_DISPATCH, this.delayDispatchTimingList.value[i].delayDispatchTimingId,
                  this.delayDispatchTimingList.controls[i].value.propertyAccessReasonListId).subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess && response.statusCode == 200) {
                      this.delayDispatchTimingList.removeAt(i);
                    }
                  });
              }
            }
          else {
            this.delayDispatchTimingList.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    if (this.delayDispatchForm.invalid) {
      return;
    }
    let formValue = this.delayDispatchForm.value;
    formValue.delayDispatchMainAreaList.forEach(element => {
      element.mainAreaId = element.id
    });
    formValue.delayDispatchTimingList.forEach(element => {
      element.timeRangeFrom = this.momentService.convertNormalToRailayTime(element.timeRangeFrom);
      element.timeRangeTo = this.momentService.convertNormalToRailayTime(element.timeRangeTo);
      element.delayTime = Number(element.delayTime)
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.delayDispatchId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DELAY_DISPATCH, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DELAY_DISPATCH, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=11');
      }
    })
  }

  onCRUDRequested(type){}

}
