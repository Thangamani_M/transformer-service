import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-delay-dispatch-view',
  templateUrl: './delay-dispatch-view.component.html',
})
export class DelayDispatchViewComponent implements OnInit {

  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  delayDispatchId: string;
  delayDispatchDetails: any;
  delayDispatchTimingList: any;
  primengTableConfigProperties:any;
  totaltab = 0
  selectedTab = 0
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService, private router: Router, public momentService: MomentService, private rxjsService: RxjsService, private crudService: CrudService,  private store: Store<AppState>) {
    this.delayDispatchId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40 ;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.primengTableConfigProperties = {
      tableCaption: this.delayDispatchId && 'View Delay Dispatch',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },{ displayName: 'Delay Dispatch', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 11} }, { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData()
    this.getDelayDispatchDetailsById(this.delayDispatchId);
  }
  
  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDelayDispatchDetailsById(delayDispatchId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DELAY_DISPATCH, delayDispatchId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.delayDispatchDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.delayDispatchDetails?.delayDispatchName;
          this.delayDispatchTimingList = response.resources.delayDispatchTimingList
          this.onShowValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.delayDispatchDetails = [
      { name: 'Delay Dispatch Name', value: response?.delayDispatchName },
      { name: 'Description', value: response?.delayDispatchDescription },
      { name: 'Division', value: response?.divisionName },
      { name: 'Main Area', value: response?.mainArea },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Modified By', value: response?.modifiedUserName },
      { name: 'Status', value: response.isActive == true ? 'Active' : 'In-Active', statusClass: response.isActive == true ? "status-label-green" : 'status-label-red' },
    ]
  }
  

  edit() {
    if (this.delayDispatchId) {
      if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['event-management/event-configuration/delay-dispatch/add-edit'], { queryParams: { id: this.delayDispatchId, totalTabs:this.totaltab, selectedTab:this.selectedTab } });
    }
  }
  
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit();
        break;
    }
  }
}
