import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule, MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { DelayDispatchAddEditComponent } from './delay-dispatch-add-edit.component';
import { DelayDispatchRoutingModule } from './delay-dispatch-routing.module';
import { DelayDispatchViewComponent } from './delay-dispatch-view.component';


@NgModule({
  declarations: [DelayDispatchAddEditComponent, DelayDispatchViewComponent],
  imports: [
    CommonModule,
    DelayDispatchRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule
  ]
})
export class DelayDispatchModule { }
