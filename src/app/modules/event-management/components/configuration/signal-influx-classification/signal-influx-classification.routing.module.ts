import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalInfluxClassificationAddEditComponent } from './signal-influx-classification-add-edit';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SignalInfluxClassificationAddEditComponent, data: { title: 'Signal Influx Classification Add/Edit' } },
  { path: 'view', component: SignalInfluxClassificationAddEditComponent, data: { title: 'Signal Influx Classification View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalInfluxClassificationRoutingModule { }
