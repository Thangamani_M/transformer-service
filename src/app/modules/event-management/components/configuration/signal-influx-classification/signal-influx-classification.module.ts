import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { SignalInfluxClassificationAddEditComponent } from './signal-influx-classification-add-edit';
import { SignalInfluxClassificationRoutingModule } from './signal-influx-classification.routing.module';



@NgModule({
  declarations: [SignalInfluxClassificationAddEditComponent],
  imports: [
    CommonModule,
    SignalInfluxClassificationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class SignalInfluxClassificationModule { }
