import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { SignalInfluxClassificationAddEditModel } from '@modules/event-management/models/configurations/signal-influx-classification.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'signal-influx-classification-add-edit',
  templateUrl: './signal-influx-classification-add-edit.component.html',
})
export class SignalInfluxClassificationAddEditComponent implements OnInit {

  signalinfluxClassificationAddEditForm: FormGroup;
  viewable: boolean;
  userData: UserLogin;
  primengTableConfigProperties: any;
  id: any;
  btnName: any;
  signalInfluxClassificationDetail: any;
  selectedTabIndex: any = 0;
  isSubmitted: boolean;
  totaltab = 0
  selectedTab = 0
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private router: Router,
    private activateRoute: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService,
    private crudService: CrudService,
    private snackbarService : SnackbarService) {
    this.activateRoute.queryParamMap.subscribe((params) => {
      this.id = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = (Object.keys(params['params']).length > 0) ? params['params']['totalTabs'] : '' || 40;
      this.totaltab = (Object.keys(params['params']).length > 0) ? params['params']['selectedTab'] : '';
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.primengTableConfigProperties = {
      tableCaption: this.id && !this.viewable ? 'Update Signal Influx Classification Type' : this.viewable ? 'View Signal Influx Classification Type' : 'Add Signal Influx Classification Type',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '' }, { displayName: 'Signal Influx Classification Type', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 33 } }, { displayName: 'Add Signal Influx Classification Type', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngOnInit(): void {
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    }
    this.combineLatestNgrxStoreData()
    this.onLoadValue();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    if (this.viewable) {
      this.signalInfluxClassificationDetail = [
        { name: 'Classification', value: '' },
        { name: 'Description', value: '' },
        { name: 'Created On', value: '' },
        { name: 'Modified On', value: '' },
        { name: 'Created By', value: '' },
        { name: 'isActive', value: '' }
      ]
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
      this.viewValue();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm(signalInfluxClassificationAddEditModel?: SignalInfluxClassificationAddEditModel) {
    let signalInfluxClassification = new SignalInfluxClassificationAddEditModel(signalInfluxClassificationAddEditModel);
    this.signalinfluxClassificationAddEditForm = this.formBuilder.group({});
    Object.keys(signalInfluxClassification).forEach((key) => {
      if (!this.viewable) {
        this.signalinfluxClassificationAddEditForm.addControl(key, new FormControl(signalInfluxClassification[key]));
      }
    });
    if (!this.viewable) {
      this.signalinfluxClassificationAddEditForm = setRequiredValidator(this.signalinfluxClassificationAddEditForm, ["signalInfluxClassificationTypeName", "description", "isActive"])
    }
    if (this.id && !this.viewable) {
      this.patchValue();
    }
  }

  viewValue() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_CLASSIFICATION_CONFIG, this.id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources) {
          if (this.viewable) {
            this.signalInfluxClassificationDetail = [
              { name: 'Classification', value: response.resources.signalInfluxClassificationTypeName },
              { name: 'Description', value: response.resources.description },
              { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
              { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
              { name: 'Created By', value: response.resources.createdUserName },
              { name: 'Modified By', value: response.resources.modifiedUserName },
            ];
          } else if (this.id && !this.viewable) {
            this.signalinfluxClassificationAddEditForm.patchValue({
              signalInfluxClassificationTypeName: response.resources.signalInfluxClassificationTypeName,
              description: response.resources.description,
              isActive: response.resources.isActive
            })
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.id && !this.viewable ? 'Update Signal Influx Classification Type' : this.viewable ? 'View Signal Influx Classification Type' : 'Add Signal Influx Classification Type';
    this.btnName = this.id ? 'Update' : 'Save';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.viewable ? 'View Signal Influx Classification Type' : this.id && !this.viewable ? 'Update Signal Influx Classification Type' : 'Add Signal Influx Classification Type';
  }

  patchValue() {
    this.viewValue();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/event-management/event-configuration/signal-influx-classification/add-edit'], { queryParams: { id: this.id,totalTabs:this.totaltab, selectedTab:this.selectedTab }, skipLocationChange: true })
  }

  onSubmit() {
    if (this.signalinfluxClassificationAddEditForm?.invalid) {
      this.signalinfluxClassificationAddEditForm.markAllAsTouched();
      return;
    }
    let reqObj = {
      signalInfluxClassificationTypeName: this.signalinfluxClassificationAddEditForm.get('signalInfluxClassificationTypeName').value,
      description: this.signalinfluxClassificationAddEditForm.get('description').value,
      isActive: this.signalinfluxClassificationAddEditForm.get('isActive').value,
    }
    this.isSubmitted = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let api = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_CLASSIFICATION_CONFIG, reqObj);
    if (this.id) {
      reqObj['modifiedUserId'] = this.userData?.userId;
      reqObj['signalInfluxClassificationTypeId'] = this.id;
      api = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.EVENT_CLASSIFICATION_CONFIG, reqObj);
    } else if (!this.id) {
      reqObj['createdUserId'] = this.userData?.userId;
    }
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess) {
        this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: 33 }, skipLocationChange: true })
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
}
