import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { StackAreasAddEditComponent } from './stack-areas-add-edit/stack-areas-add-edit.component';
import { StackAreasRoutingModule } from './stack-areas-routing.module';
import { StackAreasViewComponent } from './stack-areas-view/stack-areas-view.component';



@NgModule({
  declarations: [StackAreasAddEditComponent, StackAreasViewComponent],
  imports: [
    CommonModule,
    StackAreasRoutingModule,
    SharedModule, ReactiveFormsModule, FormsModule,
    MatSelectModule
  ]
})
export class StackAreasModule { }
