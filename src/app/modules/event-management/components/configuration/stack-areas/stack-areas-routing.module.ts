import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StackAreasAddEditComponent } from './stack-areas-add-edit/stack-areas-add-edit.component';
import { StackAreasViewComponent } from './stack-areas-view/stack-areas-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: StackAreasAddEditComponent, data: { title: 'Stack Areas Add/Edit' } },
  { path: 'view', component: StackAreasViewComponent, data: { title: 'Stack Areas View' },canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StackAreasRoutingModule { }
