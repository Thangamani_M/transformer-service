import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, HttpCancelService, IApplicationResponse, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import {
  ModulesBasedApiSuffix,
  setRequiredValidator
} from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { StackAreaConfigAddEditModel } from "../../../../models/configurations/stack-area-model";
import { EventMgntModuleApiSuffixModels } from '../../../../shared/enums/configurations.enum';
@Component({
  selector: 'app-stack-areas-add-edit',
  templateUrl: './stack-areas-add-edit.component.html',
})
export class StackAreasAddEditComponent implements OnInit {

  stackAreaConfigAddEditForm: FormGroup;
  divisionList = [];
  mainAreaList = [];
  createdUserId: any;
  stackAreaConfigId: any;
  stackAreaConfigViewModel = {};
  modifiedUserId: any;
  selectedDivisionId: any;
  loggedUser: any;
  divisionDropDown: any = [];
  stackAreaConfigDetails: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;
  @ViewChild('allSelectedMainArea', { static: false }) private allSelectedMainArea: MatOption;
  totaltab = 0;
  selectedTab = 0;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private router: Router, private httpCancelService: HttpCancelService) {
    this.stackAreaConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
    this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.stackAreaConfigId ? 'Update Stack Area Configuration':'Add Stack Area Configuration';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Stack Area Configuration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 1 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.stackAreaConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/event-management/event-configuration/stack-areas/view' , queryParams: {id: this.stackAreaConfigId,totalTabs:this.totaltab,selectedTab:this.selectedTab}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnInit(): void {
    this.createStackAreaConfigForm();
    this.getDivisionDropDown();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.stackAreaConfigId) {
      this.getStackAreaConfigDetailsById(this.stackAreaConfigId);
      return
    }

  }

  createStackAreaConfigForm(): void {
    let stackAreaConfigAddEditModel = new StackAreaConfigAddEditModel();
    // create form controls dynamically from model class
    this.stackAreaConfigAddEditForm = this.formBuilder.group({});
    Object.keys(stackAreaConfigAddEditModel).forEach((key) => {
      this.stackAreaConfigAddEditForm.addControl(key, new FormControl(stackAreaConfigAddEditModel[key]));
    });
    this.stackAreaConfigAddEditForm = setRequiredValidator(this.stackAreaConfigAddEditForm, ["areaName", "stackAreaConfigDivisionList", "stackAreaConfigMainAreaList"]);
    this.stackAreaConfigAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.stackAreaConfigAddEditForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.stackAreaConfigId) {
      this.stackAreaConfigAddEditForm.removeControl('stackAreaConfigId');
      this.stackAreaConfigAddEditForm.removeControl('modifiedUserId');
    } else {
      this.stackAreaConfigAddEditForm.removeControl('createdUserId');
    }

    this.stackAreaConfigAddEditForm.get('stackAreaConfigDivisionList').valueChanges
      .pipe(debounceTime(1500), distinctUntilChanged())
      .subscribe((res: any) => {
        this.mainAreaList = [];
        if (res.length == 0) return
        var filtered = res.filter(function (el) { return el; });
        this.getMainAreaList({
          'divisionId': filtered
        });
      })
  }

  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList(selectedDivisionId) {
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getStackAreaConfigDetailsById(stackAreaConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_AREA_CONFIG, stackAreaConfigId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stackAreaConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.stackAreaConfigDetails?.areaName ? this.title +' - '+ this.stackAreaConfigDetails?.areaName : this.title + ' --', relativeRouterUrl: '' });
          this.stackAreaConfigAddEditForm.patchValue(response.resources);
          var stackAreaConfigDivision = [];
          var stackAreaConfigMainArea = [];
          response.resources.stackAreaConfigDivisionList.forEach(element => {
            stackAreaConfigDivision.push(element.divisionId);
          });
          response.resources.stackAreaConfigMainAreaList.forEach(element => {
            stackAreaConfigMainArea.push(element.mainAreaId);
          });
          this.stackAreaConfigAddEditForm.get('stackAreaConfigDivisionList').setValue(stackAreaConfigDivision);
          this.stackAreaConfigAddEditForm.get('stackAreaConfigMainAreaList').setValue(stackAreaConfigMainArea);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleDivisionOne() {
    if (this.stackAreaConfigAddEditForm.controls.stackAreaConfigDivisionList.value.length === 0) {
      this.mainAreaList = [];
    } 
    if (this.allSelectedDivision.selected) {
      this.allSelectedDivision.deselect();
      return false;
    }
    if (this.stackAreaConfigAddEditForm.controls.stackAreaConfigDivisionList.value.length == this.divisionDropDown.length)
      this.allSelectedDivision.select();
  }

  toggleAllDivisions() {

    if (this.allSelectedDivision.selected) {
      this.stackAreaConfigAddEditForm.controls.stackAreaConfigDivisionList
        .patchValue([...this.divisionDropDown.map(item => item.id), '']);
      this.rxjsService.setGlobalLoaderProperty(false);

    } else {
      this.stackAreaConfigAddEditForm.controls.stackAreaConfigDivisionList.patchValue([]);
      this.mainAreaList = [];
    }
  }

  toggleMainAreaOne() {
    if (this.allSelectedMainArea.selected) {
      this.allSelectedMainArea.deselect();
      return false;
    }
    if (this.stackAreaConfigAddEditForm.controls.stackAreaConfigMainAreaList.value.length == this.mainAreaList.length)
      this.allSelectedMainArea.select();
  }

  toggleAllMainAreas() {

    if (this.allSelectedMainArea.selected) {
      this.stackAreaConfigAddEditForm.controls.stackAreaConfigMainAreaList
        .patchValue([...this.mainAreaList.map(item => item.id), '']);
    } else {
      this.stackAreaConfigAddEditForm.controls.stackAreaConfigMainAreaList.patchValue([]);
    }
  }

  onSubmit(): void {
    if (this.stackAreaConfigAddEditForm.invalid) {
      return;
    }
    let formValue = this.stackAreaConfigAddEditForm.value;
    if (formValue.stackAreaConfigDivisionList[0].hasOwnProperty('divisionId')) {
      formValue = formValue;
    } else {
      formValue.stackAreaConfigDivisionList = formValue.stackAreaConfigDivisionList.filter(item => item != '')
      formValue.stackAreaConfigDivisionList = formValue.stackAreaConfigDivisionList.map(divisionId => ({ divisionId }))
    }
    if (formValue.stackAreaConfigMainAreaList[0].hasOwnProperty('mainAreaId')) {
      formValue = formValue;
    } else {
      formValue.stackAreaConfigMainAreaList = formValue.stackAreaConfigMainAreaList.filter(item => item != '')
      formValue.stackAreaConfigMainAreaList = formValue.stackAreaConfigMainAreaList.map(mainAreaId => ({ mainAreaId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.stackAreaConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_AREA_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.STACK_AREA_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/event-management/event-configuration?tab=1');
      }
    })
  }

  onCRUDRequested(type){}

}
