import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { EVENT_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/event-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../../shared/enums/configurations.enum';

@Component({
  selector: 'app-stack-areas-view',
  templateUrl: './stack-areas-view.component.html',
})
export class StackAreasViewComponent implements OnInit {

  stackAreaConfigId: any
  stacAreaDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any;
  totaltab = 0;
  selectedTab = 0;
  pageLevelProperties: any = {
    tableComponentConfigs: {
      tabsList: []
    }
  }

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.stackAreaConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.totaltab = +this.activatedRoute.snapshot.queryParams.totalTabs || 40;
      this.selectedTab = this.activatedRoute.snapshot.queryParams.selectedTab;
    });
    this.primengTableConfigProperties = {
      tableCaption: this.stackAreaConfigId && 'View Stack Area Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Configuration', relativeRouterUrl: '/event-management/event-configuration' },
      { displayName: 'Stack Area Configuration', relativeRouterUrl: '/event-management/event-configuration', queryParams: { tab: 1 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    for (let index = 0; index < this.totaltab; index++) {
      this.pageLevelProperties.tableComponentConfigs.tabsList.push({});
    };
    this.combineLatestNgrxStoreData()
    if (this.stackAreaConfigId) {
      this.getEmailDistributionDetailsById().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.stacAreaDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + this.stacAreaDetails.areaName;
        this.stacAreaDetails = [
          { name: 'Area Name', value: response.resources.areaName },
          { name: 'Division', value: response.resources.stackAreaConfigDivisions },
          { name: 'Main Area', value: response.resources.stackAreaConfigMainAreas },
          { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
          { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
          { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
          { name: 'Modified By', value: response.resources.modifiedUserName },
          { name: 'Created By', value: response.resources.createdUserName },


        ];
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }


  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      let permission = response[0][EVENT_MANAGEMENT_COMPONENT.EVENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.pageLevelProperties, permission);
        this.pageLevelProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.pageLevelProperties.tableComponentConfigs.tabsList[this.selectedTab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/event-management/event-configuration/stack-areas/add-edit"], { queryParams: { id: this.stackAreaConfigId,totalTabs:this.totaltab, selectedTab:this.selectedTab } });
  }

  getEmailDistributionDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.STACK_AREAS,
      this.stackAreaConfigId
    );
  }

}
