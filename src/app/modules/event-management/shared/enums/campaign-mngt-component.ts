export enum CAMPAIGN_MANAGEMENT_COMPONENT {
  MARKETING_DATA_SYNC_CONFIG = 'Marketing Data Sync Config',
  MARKETING_DATA_SYNC_REPORT = 'Marketing Data Sync Report',
  SMS_CAMPAIGN = 'SMS Campaign',
  CAMPAING = 'Campaign',
}

