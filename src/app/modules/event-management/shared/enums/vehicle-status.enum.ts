export enum VehicleStatus {
    AVAILABLE = 'Available',
    ARRIVED = 'Arrived',
    DECLINED = 'Declined',
    DISPATCHED = 'Dispatched',
    REQUEST_DISPATCHED = 'Request Dispatch',
    REQUEST_CANCELLED = 'Request Cancel',
    ALLOCATED = 'Allocated',
    LONGONSITE = 'Long on Site',
    PANIC = 'Panic',
    SHIFT_CHANGE_REQUEST = 'Shift Change Request',
    BREAK_TAKEN_MORE_TIME = 'Break Taken More Time',
    CANCELLED = 'Cancelled',
    BREAK = 'Break',
    CLOSED = 'Closed',
    LOGOUT = 'Logout'
}
