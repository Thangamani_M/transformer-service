export enum SIGNAL_MANAGEMENT_COMPONENT {
  ERB = 'ERB',
  ERB_CONFIG = 'ERB Config',
  NKA = 'NKA',
  QUERY_ANALYST = 'Query Analyst',
  QUERY_BUILDER_CONFIGFIGURATION = 'Query Builder Configuration',
  SIGNAL_CONFIGFIGURATION = 'Signal Configuration',
  WORK_LIST = 'Work List',
  MY_TICKET = 'My Ticket',
}

export enum ACTION {
  RESCHEDULE_O_C = "View",
  ADDRESS_SEARCH_BY_DETAILS = "Address Search By Details",
  ADDESS_SEARCH_BY_DISPATCHER_AREA = "Address Search By Dispatcher Area",
  ADDESS_SEARCH_BY_LAYERS = "Address Search by Layers",
  SCHEDULE_CALL_BACK = "Schedule Call Back",
}
