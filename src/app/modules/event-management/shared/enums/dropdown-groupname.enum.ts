export enum DropdownGroupName {
    CALL_CATEGORY = 'Call Category',
    RESOLUTION_CATEGORY = 'Resolution Category',
    FOLLOWUP_REASON_CATEGORY = 'Follow Up Reason'
}