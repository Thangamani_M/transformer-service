import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { defaultLeafletLatitude, defaultLeafletLongitude, defaultLeafLetMapZoomValue, LoggedInUserModel, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { IpolyLayersObject } from "@modules/event-management/components/dispatcher-map-view/dispatcher-map-view.component";
import { Control } from 'leaflet';
import "leaflet-draw";
import 'leaflet-routing-machine';
import 'leaflet-routing-machine/dist/leaflet-routing-machine.css';
import 'leaflet.markercluster';
declare var L: any;

const PREFIX_IMAGE_RELATIVE_PATH = '../../../../assets/img/dispatcher-map-images/';

enum AlarmCaptionPriority {
	RED = 1, //<Blink>
	BLUE = 2,  // <No blink>
	YELLOW = 3, //<No blink>
	BLACK = 4    // Default value <No blink>
}
@Component({
	selector: "app-event-mgmt-leaf-let",
	templateUrl: "./event-mgmt-leaf-let.component.html",
	styleUrls: ["./event-mgmt-leaf-let.component.scss"],
})
export class EventsMgmtSharedLeafLetComponent implements OnInit {
	@Input() polyLayersObject: IpolyLayersObject;
	@Input() id = "dispatcher_map_view";
	@Input() caption = "";
	@Input() shouldShowLegend = true;
	@Input() boundaryRequestDetails;
	@Input() zoomInOptions = { latitude: defaultLeafletLatitude, longitude: defaultLeafletLongitude };
	@Input() zoomInOptionsForDispatcherArea = { southWestLatitude: 0, southWestLongitude: 0, northEastLatitude: 0, northEastLongitude: 0 };
	@Input() targetLatLong;
	@Output() onSelection = new EventEmitter();
	@Output() onOpenPopup = new EventEmitter();
	@Output() afterDataRenderedInMapAction = new EventEmitter();
	container;
	map: L.Map;
	drawControl: Control;
	openedPopup: L.Popup;
	selectedLayer;
	loggedInUserData: LoggedInUserModel;
	encodedToken = "";
	editablePolygonLayers = new L.FeatureGroup();
	editableMarkerVehicleLayers = new L.FeatureGroup();
	editableMarkerSiteLayers = new L.FeatureGroup();
	currentLocationAndDestinaitonMarkerSiteLayers = new L.FeatureGroup();
	editablePolyLineLayers = new L.FeatureGroup();
	routingPathControl;
	sourceLatLong: L.LatLng;

	constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService) {
		this.rxjsService.setGlobalLoaderProperty(true);
	}

	ngOnInit(): void {
		setTimeout(() => {
			// Check if map already exists then remove after that recreate the map
			if (this.map) {
				this.map.remove();
			}
			// Create a container using element's id
			this.container = document.getElementById(this.id);
			// create a map using created element's container using default options like zoom and lat,long values
			this.map = L.map(this.container).setView(
				[this.zoomInOptions.latitude, this.zoomInOptions.longitude],
				defaultLeafLetMapZoomValue
			);
			// normal view layer
			let customPaidTileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').
				addTo(this.map);
			// Satellite view layer based on google's open API
			let googleSatelliteViewTileLayer = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
				maxZoom: 20,
				subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
			});
			let baseMaps: L.Control.LayersObject = {
				"Normal View": customPaidTileLayer,
				"Satellite View": googleSatelliteViewTileLayer
			};
			L.control.layers(baseMaps, null, { position: 'bottomleft' }).addTo(this.map);
			if (this.caption == 'Third Party Map View') {
				this.trackTheCurrentSourceMovingLocation();
				this.map.addLayer(this.currentLocationAndDestinaitonMarkerSiteLayers);
			}
			// Select default view as custom paid layer
			customPaidTileLayer.addTo(this.map);
			this.map.addLayer(this.editableMarkerVehicleLayers);
			this.map.addLayer(this.editableMarkerSiteLayers);
			this.map.addLayer(this.editablePolygonLayers);
			this.map.addLayer(this.editablePolyLineLayers);
			// Create and Add draw control to leaflet map
			this.drawControl = new L.Control.Draw({
				draw: {
					circle: false,
					circlemarker: false,
					marker: false,
					polygon: false,
					polyline: false,
					rectangle: false
				}
			});
			this.map.addControl(this.drawControl);
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		for (let prop in changes) {
			switch (prop) {
				case 'polyLayersObject':
					if (this.map) {
						if (this.polyLayersObject) {
							if (this.polyLayersObject.isUpdatedLayers == true) {
								this.editablePolygonLayers.clearLayers();
							}
							if (this.polyLayersObject.isUpdatedResponseOfficers == true) {
								this.editableMarkerVehicleLayers.clearLayers();
							}
							if (this.polyLayersObject.isUpdatedCustomerAddress == true) {
								this.editableMarkerSiteLayers.clearLayers();
							}
							if (this.polyLayersObject.isUpdatedRoots == true) {
								this.editablePolyLineLayers.eachLayer((layer) => {
										this.editablePolyLineLayers.removeLayer(layer);
								});
							}
							this.prepareDrawableShapes();
						}
					}
					break;
				case "zoomInOptions":
					// zoom in leaf let map based on lat,long filter
					if (this.map) {
						var southWest = new L.LatLng(this.zoomInOptions.latitude, this.zoomInOptions.longitude),
							northEast = new L.LatLng(this.zoomInOptions.latitude, this.zoomInOptions.longitude),
							bounds = new L.LatLngBounds(southWest, northEast);
						this.map.fitBounds(bounds);
					}
					break;
				case "zoomInOptionsForDispatcherArea":
					if (this.map && this.zoomInOptionsForDispatcherArea.southWestLatitude && this.zoomInOptionsForDispatcherArea.southWestLatitude !== 0) {
						var southWest = new L.LatLng(this.zoomInOptionsForDispatcherArea.southWestLatitude, this.zoomInOptionsForDispatcherArea.southWestLongitude),
							northEast = new L.LatLng(this.zoomInOptionsForDispatcherArea.northEastLatitude, this.zoomInOptionsForDispatcherArea.northEastLongitude),
							bounds = new L.LatLngBounds(southWest, northEast);
						this.map.fitBounds(bounds);
					}
					break;
				case "targetLatLong":
					if (this.caption == 'Third Party Map View') {
						this.currentLocationAndDestinaitonMarkerSiteLayers.clearLayers();
						if (this.targetLatLong.latitude && this.targetLatLong.longitude) {
							this.trackTheCurrentSourceMovingLocation();
						}
					}
					break;
			}
		}
	}

	trackTheCurrentSourceMovingLocation() {
		this.map.locate().on('locationfound', (e) => {
			console.log('Found Location using leaflet is :', e?.latlng);
			this.sourceLatLong = e.latlng;
			this.trackTheTargetMovingLocation();
		}).on('locationerror', (e) => {
			this.snackbarService.openSnackbar("Location access is denied", ResponseMessageTypes.WARNING);
		});
	}

	trackTheTargetMovingLocation() {
		//source marker
		let sourceMarker = L.marker(this.sourceLatLong,
			{ icon: new L.Icon({ iconUrl: `${PREFIX_IMAGE_RELATIVE_PATH}` + "vehicle-dispatched.png" }) });
		this.currentLocationAndDestinaitonMarkerSiteLayers.addLayer(sourceMarker);
		if (this.targetLatLong.targetInitialLatitude && this.targetLatLong.targetInitialLongitude) {
			// initial target marker
			let targetInitialMarker = L.marker([this.targetLatLong.targetInitialLatitude, this.targetLatLong.targetInitialLongitude],
				{ icon: new L.Icon({ iconUrl: `${PREFIX_IMAGE_RELATIVE_PATH}` + "site-panic-initial.png" }) }).bindTooltip("Panic signal where triggered initially..!!");
			this.currentLocationAndDestinaitonMarkerSiteLayers.addLayer(targetInitialMarker);
			// current target marker
			let currentTargetMarker = L.marker([this.targetLatLong.latitude, this.targetLatLong.longitude],
				{ icon: new L.Icon({ iconUrl: `${PREFIX_IMAGE_RELATIVE_PATH}` + "site-panic-follow.png" }) });
			this.currentLocationAndDestinaitonMarkerSiteLayers.addLayer(currentTargetMarker);
			let endingPoint = new L.LatLng(this.targetLatLong.latitude, this.targetLatLong.longitude);
			let latlngs = [this.sourceLatLong, endingPoint];
			let polyLine = new L.Polyline(latlngs, {
				color: '#115943',
				weight: 3
			});
			this.currentLocationAndDestinaitonMarkerSiteLayers.addLayer(polyLine);
			this.afterDataRenderedInMapAction.emit(true);
		}
	}

	createRoutingPathBetweenSourceAndTarget() {
		if (this.map && this.targetLatLong.latitude && this.targetLatLong.longitude) {
			let waypoints = [this.sourceLatLong, [this.targetLatLong.latitude, this.targetLatLong.longitude]];
			let routingControlOptions = {
				createMarker: (i: number, waypoint: any, n: number) => {
					let iconUrl = i == 0 ? `${PREFIX_IMAGE_RELATIVE_PATH}` + "vehicle-dispatched.png" : `${PREFIX_IMAGE_RELATIVE_PATH}` + "site-panic-follow.png";
					const marker = L.marker(waypoint.latLng, {
						icon: L.icon({ iconUrl })
					});
					return marker;
				},
				routeWhileDragging: false, draggableWaypoints: false, reverseWaypoints: false,
				addWaypoints: false, waypoints,
				lineOptions: { styles: [{ color: '#20c997', weight: 9 }] }
			};
			if (this.routingPathControl) {
				var targetLatLong = this.routingPathControl.getWaypoints()[1].latLng;
				var sourceLatLong = this.routingPathControl.getWaypoints()[0].latLng;
				var sourceLat = sourceLatLong.lat + 0.01;
				var sourceLong = sourceLatLong.lng + 0.01;
				var targetLat = targetLatLong.lat + 0.01;
				var targetLong = targetLatLong.lng + 0.01;
				// re create route path if already exists
				this.routingPathControl.setWaypoints([[sourceLat, sourceLong], [this.targetLatLong.latitude, this.targetLatLong.longitude]]);
			}
			else {
				this.routingPathControl = L.Routing.control(routingControlOptions).addTo(this.map);
			}
			this.routingPathControl.on('routesfound', (e) => {
				let totalDistance = e.routes[0].summary.totalDistance;
				this.rxjsService.setAnyPropertyValue({ totalDistance });
				this.afterDataRenderedInMapAction.emit(true);
			});
			// remove router container
			this.routingPathControl._container.style.display = "None";
		}
	}

	// draw shapes like polygons, points( such as vehicles, sites ), polylines etc
	prepareDrawableShapes() {
		if (this.polyLayersObject.polyLayers == null || this.polyLayersObject.polyLayers.length == 0) {
			return;
		}
		for (let layers of this.polyLayersObject.polyLayers) {
			let layer = layers,
				feature = (layer.feature = layer.feature || {});
			feature.type = feature.type || "Feature";
			feature.properties = layer.options;
			// fill color based on Batch Ids to group the layers
			let shapeType = feature.properties.shapeType;
			if (shapeType == 'Point') {
				let vehicleOrSiteMarker = L.marker(feature.properties.coordinates, { icon: new L.Icon({ iconUrl: `${PREFIX_IMAGE_RELATIVE_PATH}${feature.properties.iconName}` }) });
				if (feature.properties.legentType == 'Vehicle' || feature.properties.legentType == 'Site') {
					// show white background label like display name at the right side of the vehicle
					if (feature.properties?.displayText) {
						let className = "";
						switch (feature.properties?.alarmCaptionPriority) {
							case AlarmCaptionPriority.RED:
								className = "toolTipBackgroundWhiteWithDisplayColor";
								break;
							case AlarmCaptionPriority.BLUE:
								className = "toolTipBackgroundWhiteWithDisplayColor";
								break;
							case AlarmCaptionPriority.YELLOW:
								className = "toolTipBackgroundWhiteWithDisplayColor";
								break;
							default:
								className = "toolTipBackgroundWhiteWithoutDisplayColor";
								break;
						}
						vehicleOrSiteMarker.bindTooltip(`${feature.properties.displayText}`, {
							permanent: true, direction: "right", zoomAnimation: true,
							interactive: false,
							className
						}).openTooltip();
					}
				}
				vehicleOrSiteMarker.on('click', (e) => {
					this.onOpenPopup.emit(layer.feature.properties);
				});
				vehicleOrSiteMarker.on('mouseover', (e) => {
					let props = layer.feature.properties;
					if (props.legentType == 'Site') {
						this.openedPopup = L.popup()
							.setLatLng(e['latlng'])
							.setContent(`<strong>Customer Name : ${props.displayName}</strong> <br>
														<strong>Address : ${props.fullAddress ? props.fullAddress : '-'}</strong><br>
														<strong>Mobile Number : ${props.mobileNumber ? props.mobileNumber : '-'}</strong>`)
							.openOn(this.map);
					}
					else if (props.legentType == 'Vehicle') {
						this.openedPopup = L.popup()
							.setLatLng(e['latlng'])
							.setContent(`<strong>Response Officer Name : ${props.displayName}</strong> <br>
														<strong>Sub Area : ${props.subArea ? props.subArea : '-'}</strong><br>
														<strong>Vehicle Reg Number : ${props.vehicleRegistrationNumber ? props.vehicleRegistrationNumber : '-'}</strong>`)
							.openOn(this.map);
					}
				});
				vehicleOrSiteMarker.on('mouseout', (e) => {
					setTimeout(() => {
						this.map.closePopup(this.openedPopup);
					});
				});
				if (feature.properties.legentType == 'Vehicle') {
					this.editableMarkerVehicleLayers.addLayer(vehicleOrSiteMarker);
				}
				else if (feature.properties.legentType == 'Site') {
					this.editableMarkerSiteLayers.addLayer(vehicleOrSiteMarker);
				}
			}
			layer.on('add', (e) => {
				if (shapeType == "Polygon") {
					let mouseOverPolyLineOptions: L.PolylineOptions = {
						color: feature.properties.color,
						fillColor: feature.properties.fillColor, fillOpacity: 0.3, weight: 6
					};
					let mouseOutPolyLineOptions: L.PolylineOptions = {
						color: feature.properties.color,
						fillColor: feature.properties.fillColor, weight: 3
					}
					if (this.polyLayersObject.isShowLayerLabelName && this.polyLayersObject.isShowLayerLabelName == true) {
						layer.bindTooltip(`${feature.properties.displayName}`, {
							permanent: true, direction: "center", zoomAnimation: true,
							className: 'toolTipTransparentColor'
						}).openTooltip();
					}
					layer.setStyle({
						color: feature.properties.color,
						fillColor: feature.properties.fillColor, fillOpacity: feature.properties.fillOpacity,
						weight: feature.properties.weight
					});
					layer.on('mouseover', (e) => {
						e.target.setStyle(mouseOverPolyLineOptions);
					});
					layer.on('mouseout', (e) => {
						e.target.setStyle(mouseOutPolyLineOptions);
						setTimeout(() => {
							this.map.closePopup(this.openedPopup);
						});
					});
					layer.on('click', (e) => {
						let props = e.target.options;
						this.openedPopup = L.popup()
							.setLatLng(e['latlng'])
							.setContent(`<strong>Area Name : ${props.displayName}</strong> <br>`)
							.openOn(this.map);
					});
				}
				else if (shapeType == "LineString") {
					var startingPoint = new L.LatLng(feature.properties.coordinates[0][0], feature.properties.coordinates[0][1]);
					var endingPoint = new L.LatLng(feature.properties.coordinates[1][0], feature.properties.coordinates[1][1]);
					var latlngs = [startingPoint, endingPoint];
					var polyLine = new L.Polyline(latlngs, {
						color: feature.properties.color ? feature.properties.color : 'orange',
						weight: 3
					});
					// show white background label like display name at the right side of the vehicle
					if (feature.properties?.displayText) {
					}
					polyLine.on('mouseover', (e) => {
						e.target.setStyle({ weight: 6 });
					});
					polyLine.on('mouseout', (e) => {
						e.target.setStyle({ weight: 3 });
					});
					this.editablePolyLineLayers.addLayer(polyLine);
				}
			});
			if (shapeType == "Polygon") {
				this.editablePolygonLayers.addLayer(layer);
			}
			else if (shapeType == "LineString") {
				this.editablePolyLineLayers.addLayer(layer);
			}
		}
		this.afterDataRenderedInMapAction.emit(true);
	}
}
