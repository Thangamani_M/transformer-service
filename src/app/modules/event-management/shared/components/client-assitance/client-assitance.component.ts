import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, defaultPopupType, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { ClientAssitanceModel } from '@app/shared/models/client-assitance.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'shared-client-assitance',
  templateUrl: './client-assitance.component.html',
})
export class SharedClientAssitanceComponent implements OnInit {
  clientAssitanceForm: FormGroup;
  userData: UserLogin;
  requestTypeList = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  defaultPopupType=defaultPopupType.P_DIALOG;

  constructor(private store: Store<AppState>,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.initForm();
    this.getRequestTypeList();
  }

  initForm(clientAssitanceModel?: ClientAssitanceModel) {
    let clientAssitanceModelData = new ClientAssitanceModel(clientAssitanceModel);
    this.clientAssitanceForm = this.formBuilder.group({});
    Object.keys(clientAssitanceModelData).forEach((key) => {
      this.clientAssitanceForm.addControl(key, new FormControl(clientAssitanceModelData[key]));
    });
    this.clientAssitanceForm = setRequiredValidator(this.clientAssitanceForm, ["occurrenceBookId", "clientRequestTypeId", "comments"])
    this.clientAssitanceForm.get("createdUserId").setValue(this.userData?.userId);
    this.clientAssitanceForm.get("occurrenceBookId").setValue(this.config.data.occurrenceBookId);
    this.clientAssitanceForm.get("supervisorRequestedById").setValue(this.userData?.userId);
  }

  onSubmit() {
    if (this.clientAssitanceForm?.invalid) {
      this.clientAssitanceForm.markAllAsTouched();
      return;
    }
    this.dialogClose();
    let api = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SUPERVISOR_REQUEST, this.clientAssitanceForm.value);
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess) {
        this.dialogClose();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getRequestTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CLIENT_REQUEST_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.requestTypeList = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
