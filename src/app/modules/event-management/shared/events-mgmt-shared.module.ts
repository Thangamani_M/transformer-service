import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ArmDisarmScheduleDialogComponent } from '../components/configuration/call-workflow/arm-disarm-schedule-dialog/arm-disarm-schedule-dialog.component';
import { EventSignalListComponent } from '../components/configuration/call-workflow/event-signal-list/event-signal-list.component';
import { PasswordPopupComponent } from '../components/configuration/call-workflow/password-popup/password-popup.component';
import { RescheduleDialogComponent } from '../components/configuration/call-workflow/reschedule-dialog/reschedule-dialog.component';
import { ResponseInstructionsViewDetailsModule } from '../components/configuration/call-workflow/response-instructions-view-details/response-instructions-view-details.module';
import { AlarmInfoWinelandsListComponent } from '../components/configuration/stack-winelands/alarm-info-winelands-list.component ';
import { RoMessageListComponent } from '../components/configuration/stack-winelands/ro-message-list.component';
import { SignalInfoComponent } from '../components/configuration/stack-winelands/signal-info.component';
import { StackDispachPartitionDetailsComponent } from '../components/configuration/stack-winelands/stack-dispach-partition-details/stack-dispach-partition-details.component';
import { StackDispachPatrolDetailsComponent } from '../components/configuration/stack-winelands/stack-dispach-patrol-details/stack-dispach-patrol-details.component';
import { OperatorInteractionAreaDistrubutioncomponentComponent } from '../components/event-supervisor-dashboard/operator-interaction-area-distrubutioncomponent/operator-interaction-area-distrubutioncomponent.component';
import { SharedClientAssitanceComponent } from './components';
import { EventsMgmtSharedLeafLetComponent } from './components/dispatcher-leaflet-map/event-mgmt-leaf-let.component';

@NgModule({
    declarations: [EventsMgmtSharedLeafLetComponent, StackDispachPatrolDetailsComponent,RescheduleDialogComponent,StackDispachPartitionDetailsComponent, SharedClientAssitanceComponent, ArmDisarmScheduleDialogComponent, EventSignalListComponent,
        PasswordPopupComponent,AlarmInfoWinelandsListComponent,RoMessageListComponent,SignalInfoComponent,
        OperatorInteractionAreaDistrubutioncomponentComponent],
    imports: [
        CommonModule, ReactiveFormsModule, FormsModule,
        SharedModule,
        MaterialModule, LayoutModule,
        ResponseInstructionsViewDetailsModule],
    exports: [EventsMgmtSharedLeafLetComponent, SharedClientAssitanceComponent,AlarmInfoWinelandsListComponent,
        RoMessageListComponent,SignalInfoComponent],
    entryComponents: [SharedClientAssitanceComponent,StackDispachPatrolDetailsComponent,StackDispachPartitionDetailsComponent,RescheduleDialogComponent, ArmDisarmScheduleDialogComponent, EventSignalListComponent,
        PasswordPopupComponent, EventSignalListComponent,
        OperatorInteractionAreaDistrubutioncomponentComponent]
})
export class EventsMgmtSharedModule { }
