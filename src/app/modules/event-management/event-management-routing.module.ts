import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DispatcherMapViewComponent } from './components/dispatcher-map-view/dispatcher-map-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  {
    path: 'dispatcher-map-view', component: DispatcherMapViewComponent, data: { title: 'Dispatch Map View' }, canActivate: [AuthGuard]
  },
  { path: 'event-configuration', loadChildren: () => import('../event-management/components/configuration/event-configuration.module').then(m => m.EventConfigurationModule), data: { preload: true } },
  { path: 'ro-configuration', loadChildren: () => import('../event-management/components/configuration/ro-configuration.module').then(m => m.RoConfigurationModule), data: { preload: true } },
  { path: 'stack-operator', loadChildren: () => import('../event-management/components/configuration/stack-winelands.module').then(m => m.StackWinelandsModule), data: { preload: true } },
  { path: 'stack-history', loadChildren: () => import('../event-management/components/configuration/stack-history.module').then(m => m.StackHistoryModule), data: { preload: true } },
  { path: 'stack-dispatcher', loadChildren: () => import('../event-management/components/configuration/dispatcher-list.module').then(m => m.DispatcherListModule), data: { preload: true } },
  { path: 'find-signal', loadChildren: () => import('./components/configuration/find-signal.module').then(m => m.FindSignalModule),canActivate: [AuthGuard], data: { preload: true } },
  { path: 'useful-numbers-contact', loadChildren: () => import('./components/configuration/useful-numbers-contact.module').then(m => m.UsefulNumberContactModule),canActivate: [AuthGuard], data: { preload: true } },
  { path: 'aa-phone-in', loadChildren: () => import('./components/phone-in/aa-phone-in/aa-phone-in.module').then(m => m.AaPhoneInModule), data: { preload: true } },
  { path: 'create-phone-in', loadChildren: () => import('./components/phone-in/create-phone-in/create-phone-in.module').then(m => m.CreatePhoneInModule), data: { preload: true } },
  { path: 'supervisor-dashboard', loadChildren: () => import('./components/event-supervisor-dashboard/event-supervisor-dashboard.module').then(m => m.EventSupervisorDashboardModule), data: { preload: true } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class EventManagementRoutingModule { }
