import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';

export class Alert {
    response: IApplicationResponse;
    keepAfterRouteChange: boolean;

    constructor(init?: Partial<Alert>) {
        Object.assign(this, init);
    }
}

export enum AlertType {
    Success,
    Error,
    Info,
    Warning
}

export enum StatusCode {
    OK = 200,
    NoContent = 204,
    Conflict = 409,
    InternalServerError = 500,
    BadRequest = 400,
    Unauthorized = 401,
    PaymentRequired = 402,
    Forbidden = 403,
    NotFound = 404,
    MethodNotAllowed = 405
}