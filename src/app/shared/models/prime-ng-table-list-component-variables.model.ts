import { CommonPaginationConfig } from "../utils/enums.utils";
import { LoggedInUserModel } from "../utils/models/logged-in-user.model";

export abstract class PrimeNgTableVariablesModel {
    selectedTabIndex?: any = 0;
    totalRecords: number;
    selectedRow?: string;
    loggedInUserData: LoggedInUserModel;
    status?: any = [{ label: 'Active', value: true }, { label: 'In-Active', value: false }];
    dataList = [];
    selectedColumns = [];
    selectedRows?= [];
    loading?= false;
    pageLimit = [10, 25, 50, 75, 100];
    primengTableConfigProperties: any;
    row?: any = {};
    first?: number = 0;
    reset?: boolean;
    isShowNoRecord?: boolean;
    pageSize=+CommonPaginationConfig.defaultPageSize;
    // start logic table fields search persistant
    tableFilteredFields: any = {};
    areColumnFiltersEdited = false;
    // end logic table fields search persistant
}