export class ClientAssitanceModel {
    constructor(clientAssitanceModel?: ClientAssitanceModel) {
      this.occurrenceBookId = clientAssitanceModel == undefined ? null : clientAssitanceModel.occurrenceBookId == undefined ? null : clientAssitanceModel.occurrenceBookId;
      this.clientRequestTypeId = clientAssitanceModel == undefined ? "" : clientAssitanceModel.clientRequestTypeId == undefined ? "" : clientAssitanceModel.clientRequestTypeId;
      this.comments = clientAssitanceModel == undefined ? "" : clientAssitanceModel.comments == undefined ? "" : clientAssitanceModel.comments;
      this.createdUserId = clientAssitanceModel == undefined ? "" : clientAssitanceModel.createdUserId == undefined ? "" : clientAssitanceModel.createdUserId;
      this.supervisorRequestedById = clientAssitanceModel == undefined ? "" : clientAssitanceModel.supervisorRequestedById == undefined ? "" : clientAssitanceModel.supervisorRequestedById;
      // this.supervisorRequestedByRoleId = clientAssitanceModel == undefined ? "" : clientAssitanceModel.supervisorRequestedByRoleId == undefined ? "" : clientAssitanceModel.supervisorRequestedByRoleId;
      // this.supervisorRequestedDateTime = clientAssitanceModel == undefined ? "" : clientAssitanceModel.supervisorRequestedDateTime == undefined ? "" : clientAssitanceModel.supervisorRequestedDateTime;
      //this.supervisorRequestStatusId = clientAssitanceModel == undefined ? "" : clientAssitanceModel.supervisorRequestStatusId == undefined ? "" : clientAssitanceModel.supervisorRequestStatusId;
      //this.supervisorRequestStatusId = clientAssitanceModel ? clientAssitanceModel.supervisorRequestStatusId == undefined ? null : clientAssitanceModel.supervisorRequestStatusId : null;
    }
    occurrenceBookId: number;
    clientRequestTypeId: string;
    comments: string;
    createdUserId: string;
    supervisorRequestedById:string;
    // supervisorRequestedByRoleId:string;
    // supervisorRequestedDateTime:string;
    // supervisorRequestApprovedDateTime:string;
    // supervisorRequestStatusId:number;
  }
