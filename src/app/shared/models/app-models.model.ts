import { MatTableDataSource } from '@angular/material';
import { BillingModuleApiSuffixModels, CustomerModuleApiSuffixModels, InventoryModuleApiSuffixModels, SalesModuleApiSuffixModels, UserModuleApiSuffixModels } from '@app/modules';
import { BreadCrumbItem, CommonPaginationConfig, DropdownApiSuffixModel, ModulesBasedApiSuffix } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator';
class ComponentProperties {
    selectedTabIndex?= 0;
    tableCaption?: string;
    tableSubCaption?: string;
    dependentDropdownApiSuffixModel?: DropdownApiSuffixModel[];
    apiSuffixModel?: UserModuleApiSuffixModels | InventoryModuleApiSuffixModels | SalesModuleApiSuffixModels | CustomerModuleApiSuffixModels;
    shouldShowSearchField?= true;
    shouldShowDeleteActionBtn?= true;
    shouldShowCreateActionBtn?= true;
    shouldShowBreadCrumb?= true;
    breadCrumbItems?: BreadCrumbItem[];
    tableComponentConfigs?: TableComponentConfigs;
}
class TableComponentConfigs {
    pageLimit?: string = CommonPaginationConfig.defaultPageSize;
    isDateWithTimeRequired?= true;
    tabsList?: TabsList[];
}
class CustomDirectiveConfig {
    constructor(customDirectiveConfig?: CustomDirectiveConfig) {
        this.shouldCutKeyboardEventBeRestricted = customDirectiveConfig ? customDirectiveConfig.shouldCutKeyboardEventBeRestricted == undefined ? false : customDirectiveConfig.shouldCutKeyboardEventBeRestricted : false;
        this.shouldPasteKeyboardEventBeRestricted = customDirectiveConfig ? customDirectiveConfig.shouldPasteKeyboardEventBeRestricted == undefined ? false : customDirectiveConfig.shouldPasteKeyboardEventBeRestricted : false;
        this.isANumberOnly = customDirectiveConfig ? customDirectiveConfig.isANumberOnly == undefined ? false : customDirectiveConfig.isANumberOnly : false;
        this.isAnSAIDNumberOnly = customDirectiveConfig ? customDirectiveConfig.isAnSAIDNumberOnly == undefined ? false : customDirectiveConfig.isAnSAIDNumberOnly : false;
        this.isANumberWithZero = customDirectiveConfig ? customDirectiveConfig.isANumberWithZero == undefined ? false : customDirectiveConfig.isANumberWithZero : false;
        this.isANumberWithoutZeroStartWithMaxTwoNumbers = customDirectiveConfig ? customDirectiveConfig.isANumberWithoutZeroStartWithMaxTwoNumbers == undefined ? false : customDirectiveConfig.isANumberWithoutZeroStartWithMaxTwoNumbers : false;
        this.isANumberWithoutZeroStartWithMaxThreeNumbers = customDirectiveConfig ? customDirectiveConfig.isANumberWithoutZeroStartWithMaxThreeNumbers == undefined ? false : customDirectiveConfig.isANumberWithoutZeroStartWithMaxThreeNumbers : false;
        this.isANumberWithoutZeroStartWithMaxTwoNumbersDecimal = customDirectiveConfig ? customDirectiveConfig.isANumberWithoutZeroStartWithMaxTwoNumbersDecimal == undefined ? false : customDirectiveConfig.isANumberWithoutZeroStartWithMaxTwoNumbersDecimal : false;
        this.isAStringOnly = customDirectiveConfig ? customDirectiveConfig.isAStringOnly == undefined ? false : customDirectiveConfig.isAStringOnly : false;
        this.isAStringOnlyNoSpace = customDirectiveConfig ? customDirectiveConfig.isAStringOnlyNoSpace == undefined ? false : customDirectiveConfig.isAStringOnlyNoSpace : false;
        this.isAStringWithHyphenDash = customDirectiveConfig ? customDirectiveConfig.isAStringWithHyphenDash == undefined ? false : customDirectiveConfig.isAStringWithHyphenDash : false;
        this.isHypenOnly = customDirectiveConfig ? customDirectiveConfig.isHypenOnly == undefined ? false : customDirectiveConfig.isHypenOnly : false;
        this.isMltiDotsRestrict = customDirectiveConfig ? customDirectiveConfig.isMltiDotsRestrict == undefined ? false : customDirectiveConfig.isMltiDotsRestrict : false;
        this.isAnAlphaNumericOnly = customDirectiveConfig ? customDirectiveConfig.isAnAlphaNumericOnly == undefined ? false : customDirectiveConfig.isAnAlphaNumericOnly : false;
        this.isAnAlphaNumericWithSlash = customDirectiveConfig ? customDirectiveConfig.isAnAlphaNumericWithSlash == undefined ? false : customDirectiveConfig.isAnAlphaNumericWithSlash : false;
        this.isADecimalOnly = customDirectiveConfig ? customDirectiveConfig.isADecimalOnly == undefined ? false : customDirectiveConfig.isADecimalOnly : false;
        this.isADecimalFourOnly = customDirectiveConfig ? customDirectiveConfig.isADecimalFourOnly == undefined ? false : customDirectiveConfig.isADecimalFourOnly : false;
        this.isAddressOnly = customDirectiveConfig ? customDirectiveConfig.isAddressOnly == undefined ? false : customDirectiveConfig.isAddressOnly : false;
        this.isAccountNumber = customDirectiveConfig ? customDirectiveConfig.isAccountNumber == undefined ? false : customDirectiveConfig.isAccountNumber : false;
        this.isAValidPhoneNumberOnly = customDirectiveConfig ? customDirectiveConfig.isAValidPhoneNumberOnly == undefined ? false : customDirectiveConfig.isAValidPhoneNumberOnly : false;
        this.isAlphaSpecialCharterOnly = customDirectiveConfig ? customDirectiveConfig.isAlphaSpecialCharterOnly == undefined ? false : customDirectiveConfig.isAlphaSpecialCharterOnly : false;
        this.isAlphaSpecialCharterOnlyRestrictCommetntSymbol = customDirectiveConfig ? customDirectiveConfig.isAlphaSpecialCharterOnlyRestrictCommetntSymbol == undefined ? false : customDirectiveConfig.isAlphaSpecialCharterOnlyRestrictCommetntSymbol : false;
        this.isAlphaSomeSpecialCharterOnly = customDirectiveConfig ? customDirectiveConfig.isAlphaSomeSpecialCharterOnly == undefined ? false : customDirectiveConfig.isAlphaSomeSpecialCharterOnly : false;
        this.isAlphaNumericSomeSpecialCharterOnly = customDirectiveConfig ? customDirectiveConfig.isAlphaNumericSomeSpecialCharterOnly == undefined ? false : customDirectiveConfig.isAlphaNumericSomeSpecialCharterOnly : false;
        this.isANumberWithHash = customDirectiveConfig ? customDirectiveConfig.isANumberWithHash == undefined ? false : customDirectiveConfig.isANumberWithHash : false;
        this.isADecimalWithConfig = customDirectiveConfig ? customDirectiveConfig.isADecimalWithConfig == undefined ? new DecimalInputConfig() : customDirectiveConfig.isADecimalWithConfig : new DecimalInputConfig();
        this.isANumberWithColon = customDirectiveConfig ? customDirectiveConfig.isANumberWithColon == undefined ? false : customDirectiveConfig.isANumberWithColon : false;
        this.isANumberWithHypen = customDirectiveConfig ? customDirectiveConfig.isANumberWithHypen == undefined ? false : customDirectiveConfig.isANumberWithHypen : false;
        this.isAlphaNumericWithPasteOnly = customDirectiveConfig ? customDirectiveConfig.isAlphaNumericWithPasteOnly == undefined ? false : customDirectiveConfig.isAlphaNumericWithPasteOnly : false;
    }
    shouldCutKeyboardEventBeRestricted?: boolean;
    isANumberWithHash?: boolean;
    isANumberWithColon?: boolean;
    isANumberWithHypen?: boolean;
    shouldCopyKeyboardEventBeRestricted?: boolean;
    shouldPasteKeyboardEventBeRestricted?: boolean;
    isANumberOnly?: boolean;
    isANumberWithZero?: boolean;
    isANumberWithoutZeroStartWithMaxTwoNumbers?: boolean;
    isANumberWithoutZeroStartWithMaxTwoNumbersDecimal?: boolean;
    isANumberWithoutZeroStartWithMaxThreeNumbers?: boolean;
    isAStringOnly?: boolean;
    isAStringWithHyphenDash?: boolean;
    isAStringOnlyNoSpace?: boolean;
    isHypenOnly?: boolean;
    isMltiDotsRestrict?: boolean;
    isAnAlphaNumericOnly?: boolean;
    isAnAlphaNumericWithSlash?: boolean;
    isAValidPhoneNumberOnly?: boolean;
    isADecimalOnly?: boolean;
    isADecimalFourOnly?: boolean;
    isADecimalWithConfig?: DecimalInputConfig;
    isAddressOnly?: boolean;
    isAccountNumber?: boolean;
    isAlphaSpecialCharterOnly?: boolean;
    isAlphaSpecialCharterOnlyRestrictCommetntSymbol?: boolean;
    isAlphaSomeSpecialCharterOnly?: boolean;
    isAlphaNumericSomeSpecialCharterOnly?: boolean;
    isAlphaNumericWithPasteOnly?: boolean;
    isAnSAIDNumberOnly?: boolean;
}
class DecimalInputConfig {
    trailingDotDigitsCount: number;
}
class TabsList {
    constructor(tabslist?: TabsList) {
        this.breadCrumbItems = tabslist == undefined ? [] : tabslist.breadCrumbItems == undefined ? [] : tabslist.breadCrumbItems;
        this.cursorLinkIndex = tabslist.cursorLinkIndex == undefined ? 0 : tabslist.cursorLinkIndex;
        this.secondHyperlinkIndex = tabslist.secondHyperlinkIndex == undefined ? null : tabslist.secondHyperlinkIndex;
        this.caption = tabslist.caption == undefined ? '' : tabslist.caption;
        this.rolesAndPermissionsObj = tabslist.rolesAndPermissionsObj == undefined ? new RolesAndPermissionsObj() : tabslist.rolesAndPermissionsObj;
        this.dataSource = tabslist.dataSource == undefined ? new MatTableDataSource<any>() : tabslist.dataSource;
        this.displayedColumns = tabslist.displayedColumns == undefined ? [] : tabslist.displayedColumns;
        this.apiColumns = tabslist.apiColumns == undefined ? [] : tabslist.apiColumns;
        this.model = tabslist.model == undefined ? '' : tabslist.model;
        this.moduleName = tabslist.moduleName == undefined ? ModulesBasedApiSuffix.INVENTORY : tabslist.moduleName;
        this.pageLimit = tabslist.pageLimit == undefined ? CommonPaginationConfig.defaultPageIndex : tabslist.pageLimit;
        this.isDateWithTimeRequired = tabslist.isDateWithTimeRequired == undefined ? false : tabslist.isDateWithTimeRequired;
        this.shouldShowBreadCrumb = tabslist.shouldShowBreadCrumb == undefined ? true : tabslist.shouldShowBreadCrumb;
        this.shouldShowCreateActionBtn = tabslist.shouldShowCreateActionBtn == undefined ? true : tabslist.shouldShowCreateActionBtn;
        this.shouldShowDeleteActionBtn = tabslist.shouldShowDeleteActionBtn == undefined ? true : tabslist.shouldShowDeleteActionBtn;
        this.shouldShowSearchField = tabslist.shouldShowSearchField == undefined ? true : tabslist.shouldShowSearchField;
        this.allTableColumnDetails = tabslist.allTableColumnDetails == undefined ? [] : tabslist.allTableColumnDetails;
        this.areCheckboxesRequired = tabslist.areCheckboxesRequired == undefined ? false : tabslist.areCheckboxesRequired;
        this.shouldShowFilter = tabslist.shouldShowFilter == undefined ? false : tabslist.shouldShowFilter;
        this.shouldShowPrintActionBtn = tabslist.shouldShowPrintActionBtn == undefined ? false : tabslist.shouldShowPrintActionBtn;
    }
    cursorLinkIndex?= 0;
    breadCrumbItems?: BreadCrumbItem[];
    caption: string;
    rolesAndPermissionsObj?: RolesAndPermissionsObj;
    dataSource: MatTableDataSource<any>;
    displayedColumns?: Array<string | DisplayedColumns>;
    apiSuffixModel?: UserModuleApiSuffixModels | SalesModuleApiSuffixModels | InventoryModuleApiSuffixModels | CustomerModuleApiSuffixModels | BillingModuleApiSuffixModels | TranslatorModuleApiSuffixModels | EventMgntModuleApiSuffixModels;
    apiColumns?: Array<string>;
    model?: any;
    moduleName?: ModulesBasedApiSuffix;
    pageLimit?: string = CommonPaginationConfig.defaultPageSize;
    isDateWithTimeRequired?= false;
    isDateFilter?: string;
    shouldShowSearchField?= true;
    shouldShowDeleteActionBtn?= true;
    shouldShowCreateActionBtn?= true;
    shouldShowBreadCrumb?= true;
    secondHyperlinkIndex?: number;
    allTableColumnDetails?: AllTableColumnDetails[];
    areCheckboxesRequired?: boolean;
    shouldShowFilter?: boolean;
    shouldShowPrintActionBtn?: boolean;
};
class DisplayedColumns {
    columnName: string;
    width?: string;
    truncateCharacters?: string;
    shouldShow?: boolean
}
class AllTableColumnDetails {
    constructor(allTableColumnDetails?: AllTableColumnDetails) {
        this.displayedColumnName = allTableColumnDetails == undefined ? "" : allTableColumnDetails.displayedColumnName == undefined ? "" : allTableColumnDetails.displayedColumnName;
        this.apiColumnName = allTableColumnDetails == undefined ? "" : allTableColumnDetails.apiColumnName == undefined ? "" : allTableColumnDetails.apiColumnName;
        this.truncateCharacters = allTableColumnDetails == undefined ? "" : allTableColumnDetails.truncateCharacters == undefined ? "" : allTableColumnDetails.truncateCharacters;
        this.width = allTableColumnDetails == undefined ? "" : allTableColumnDetails.width == undefined ? "" : allTableColumnDetails.width;
        this.shouldShow = allTableColumnDetails == undefined ? false : allTableColumnDetails.shouldShow == undefined ? false : allTableColumnDetails.shouldShow;
    }
    displayedColumnName: string;
    apiColumnName: string;
    truncateCharacters?: string;
    width?: string;
    shouldShow?: boolean;
}
class RolesAndPermissionsObj {
    constructor(rolesAndPermissions?: RolesAndPermissionsObj) {
        this.view = rolesAndPermissions ? rolesAndPermissions.view == undefined ? true : rolesAndPermissions.view : true;
        this.create = rolesAndPermissions ? rolesAndPermissions.create == undefined ? true : rolesAndPermissions.create : true;
        this.edit = rolesAndPermissions ? rolesAndPermissions.edit == undefined ? true : rolesAndPermissions.edit : true;
        this.delete = rolesAndPermissions ? rolesAndPermissions.delete == undefined ? true : rolesAndPermissions.delete : true;
    }
    view?= true;
    create?= true;
    edit?= true;
    delete?= true;
}
interface AddNewFormControlObj {
    controlName: string;
    defaultValue?: string;
    isDisabled?: boolean;
}

export {
    ComponentProperties, TableComponentConfigs, CustomDirectiveConfig, TabsList,
    RolesAndPermissionsObj, AddNewFormControlObj, AllTableColumnDetails, DisplayedColumns
};

