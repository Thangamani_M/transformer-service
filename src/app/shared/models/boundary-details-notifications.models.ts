class BoundaryDetailNotificationModel {
    constructor(notificationModel?: BoundaryDetailNotificationModel) {
        this.subject = notificationModel == undefined ? "" : notificationModel.subject == undefined ? '' : notificationModel.subject;
        this.description = notificationModel == undefined ? "" : notificationModel.description == undefined ? '' : notificationModel.description;
        this.toIds = notificationModel == undefined ? "" : notificationModel.toIds == undefined ? '' : notificationModel.toIds;
        this.ccIds = notificationModel == undefined ? "" : notificationModel.ccIds == undefined ? '' : notificationModel.ccIds;
        this.goLiveDate = notificationModel == undefined ? "" : notificationModel.goLiveDate == undefined ? '' : notificationModel.goLiveDate;
    }
    subject?: string;
    description?: string;
    toIds?: string;
    ccIds?: string;
    goLiveDate:string;
}

export { BoundaryDetailNotificationModel };
