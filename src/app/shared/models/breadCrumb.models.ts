class BreadCrumbModel {
    constructor(breadCrumbModel?: BreadCrumbModel) {
        this.pageTitle = breadCrumbModel == undefined ? { key: '', value: '' } : breadCrumbModel.pageTitle == undefined ? 
        { key: '', value: '' } : this.pageTitle;
        this.items = breadCrumbModel == undefined ? [{ key: '', routeUrl: '' }] :
            breadCrumbModel.items == undefined ? [{ key: '', routeUrl: '' }] : this.items;
    }
    pageTitle?: { key?:string, value?:string };
    items: { key:string, routeUrl?:string }[]
}

export { BreadCrumbModel }