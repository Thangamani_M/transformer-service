export class EnableDisable {
    public ids: string;
    public isActive: boolean;
    public modifiedUserId: string;
}