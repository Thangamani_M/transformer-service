enum ResponseMessageTypes {
    SUCCESS = 'success',
    WARNING = 'warning',
    ERROR = 'error',
    INFO = 'info'
}

enum KeyboardEvents {
    CUT = 'cut',
    COPY = 'copy',
    PASTE = 'paste',
    KEY_DOWN = 'keyDown',
    KEY_UP = 'keyUp',
    KEY_PRESS = 'keyPress'
}
enum VarianceInvestigationStatus {
    OPENED = 'Opened',
    CLOSED = 'Closed',
    Investigation = 'Investigation',
    DISCIPLINARY = "Disciplinary"
}

enum RestfulMethods {
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    GET = 'GET'
}
enum ApprovalMode {
    PHONE = 'PHONE',
    DOCUMENT = 'DOCUMENT',
    MAIL = 'MAIL'

}
enum QuotationStatus {
    OPEN = 'OPEN',
    COMPLETED = 'COMPLETED',
    CANCELLED = 'CANCELLED',
    APPROVED = 'Approved',
    INPROGRESS = 'INPROGRESS',

}
enum TestRunPeroid {
    //commented on 28-07-2020 for client issues, as they wanted to be renamed for daily and weekly..
    DAILY = 'Daily',
    WEEKLY = 'Weekly',
    // DAILY = 'SameDay',
    // WEEKLY = 'Dated',
    MONTHLY = 'Monthly',
    YEARLY = 'Yearly'
}
enum StockTakeStatus {
    PENDING = 'Pending',
    INITIATE = 'Initiate',
    CANCEL = 'Cancel'
}
enum RawLeadStatus {
    New = "1",
    Closed = "2"
}

enum NoSales {
    No_Sale= 2,
    Decline = 4
}

enum ModuleName {
    CUSTOMER = 'customer',
    LEAD = 'lead'
}

enum BoundaryTypes {
    OPERATIONS = 1,
    TECHNICAL = 2,
    LSS = 3,
    SALES = 4,
    OTHERS=5
}


export {
     ResponseMessageTypes, ModuleName, KeyboardEvents, VarianceInvestigationStatus, RestfulMethods, ApprovalMode, QuotationStatus, TestRunPeroid, StockTakeStatus,RawLeadStatus, NoSales, BoundaryTypes
}
