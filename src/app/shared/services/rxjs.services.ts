import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { RolesAndPermissionsObj } from '@app/shared';
import { dealerCustomerServiceAgreementDataState$ } from '@modules/dealer/components/dealer-contract/dealer-agreement-summary';
import { DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { selectLeadHeaderDataState$ } from '@modules/sales/components/task-management/lead-creation-ngrx-files/lead-creation.selectors';
import { LeadHeaderData } from '@modules/sales/models/lead-creation-stepper.model';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Injectable({ providedIn: 'root' })

export class RxjsService {
    // const variables
    leadServiceAgreementAddNewButtonName = "Add More";
    leadHeaderData: LeadHeaderData;
    viewCustomerData: any = {};
    dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;

    constructor(private store: Store<AppState>, private router: Router) {
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(selectLeadHeaderDataState$),
        ])
            .pipe(take(1))
            .subscribe((response) => {
                this.leadHeaderData = response[0];
            });
    }

    combineLatestNgrxStoreDataForDealerCustomerAgreementSummary() {
        combineLatest([
            this.store.select(dealerCustomerServiceAgreementDataState$)]
        ).subscribe((response) => {
            this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[0]);
        });
    }

    private globalLoaderPropertySubject = new BehaviorSubject(false);
    private globalLoaderUntilPropertySubject = new BehaviorSubject(false);
    private popupLoaderPropertySubject = new BehaviorSubject(false);
    private expandOpenScape = new BehaviorSubject(false);
    private isDialogOpened = new BehaviorSubject(false);
    private showNotification = new BehaviorSubject(true);
    private isInboundVisited = new BehaviorSubject(false);
    private isDailedCall = new BehaviorSubject(false);
    private showBusinessNotification = new BehaviorSubject(false);
    private isUpSellingQuote = new BehaviorSubject(false);
    private isInbpundCallCame = new BehaviorSubject(false);
    private rolesAndPermissionsSubject = new BehaviorSubject({});
    private expirationTimerBySeconds = new BehaviorSubject(0);
    private tabIndexPersistance = new BehaviorSubject(0);
    private tabMonitoringIndexPersistance = new BehaviorSubject(0);
    private actinTaIndexCallflow = new BehaviorSubject(0);
    private quickActionSideBar = new BehaviorSubject(null);
    private customerData = new BehaviorSubject(null);
    private customerProfile = new BehaviorSubject(null);
    private validPassword = new BehaviorSubject(false);
    private selctedRowIndex = new BehaviorSubject(false);
    private callInitiationData = new BehaviorSubject(null);
    private customerAddressId = new BehaviorSubject(null);
    private customerPartitionId = new BehaviorSubject(null);
    private customerId = new BehaviorSubject(null);
    private stackAreaConfigId = new BehaviorSubject(null);
    private boundaryData = new BehaviorSubject(null);
    private relocationData = new BehaviorSubject(false);
    private updatedContctData = new BehaviorSubject(false);
    private isFromupsellData = new BehaviorSubject(false);
    private customerContactData = new BehaviorSubject(false);
    private uniqueCallId = new BehaviorSubject(null);
    private inboundCallDetails = new BehaviorSubject(null);
    private requisitionData = new BehaviorSubject({});
    private scheduledUpdateStatus = new BehaviorSubject(false);
    // cycle count add scanned items subject
    private cycleCountItemSubject = new BehaviorSubject({});
    private formChangeSubject = new BehaviorSubject(false);
    private formChangeDetectionSubject = new BehaviorSubject(false);
    private scheduledCallbackData = new BehaviorSubject(null);
    private extentionNumber = new BehaviorSubject(null);
    private counterSaleData = new BehaviorSubject(null);
    private propertySubject = new BehaviorSubject(0);
    private anyPropertySubject = new BehaviorSubject({});
    private crmLocationWarehouseSubject = new BehaviorSubject(null);
    private fromUrlSubject = new BehaviorSubject<string>("");
    private formChangesDetectionPropertyForPageReload = new BehaviorSubject<boolean>(false);
    private isInstallationDebtorRoute = new BehaviorSubject<boolean>(false);
    private formSubmittedSuccessfullySubject = new BehaviorSubject<boolean>(false);
    private notificationsSubject = new BehaviorSubject<any>({});
    private isSpecialProjectCreationClicked = new BehaviorSubject<boolean>(false);
    private isSpecialProjectTechnicianClicked = new BehaviorSubject<boolean>(false);
    private isSpecialProjectCoordinatorClicked = new BehaviorSubject<boolean>(false);
    private contractReviewQualityCheck = new BehaviorSubject<boolean>(false);
    private loadCustomerCategoryList = new BehaviorSubject<boolean>(false);
    private troubleShootAlarm = new BehaviorSubject({});
    private customerContactInfo = new BehaviorSubject({});
    private authenticationHeaderSubject = new BehaviorSubject(true);
    private goToTroubleShoot = new BehaviorSubject<boolean>(false);
    private isLoadTechnician = new BehaviorSubject<boolean>(false)
    private isRefershRiskwatchRequests = new BehaviorSubject<boolean>(false)
    private isIncentiveBtnClick = new BehaviorSubject<boolean>(false);

    setGlobalLoaderProperty(isLoading: boolean) {
        this.globalLoaderPropertySubject.next(isLoading);
    }

    getGlobalLoaderProperty(): Observable<boolean> {
        return this.globalLoaderPropertySubject.asObservable();
    }

    setCounterSaleProperty(data: string) {
        this.counterSaleData.next(data);
    }

    getCounterSaleProperty(): Observable<string> {
        return this.counterSaleData.asObservable();
    }


    setGlobalLoaderPropertyUntilChanged(isLoading: boolean) {
        this.globalLoaderUntilPropertySubject.next(isLoading);
    }

    getGlobalLoaderPropertyUntilChanged(): Observable<boolean> {
        return this.globalLoaderUntilPropertySubject.asObservable();
    }

    setPopupLoaderProperty(isLoading: boolean) {
        this.popupLoaderPropertySubject.next(isLoading);
    }

    getPopupLoaderProperty(): Observable<boolean> {
        return this.popupLoaderPropertySubject.asObservable();
    }

    setDialogOpenProperty(isLoading: boolean) {
        this.isDialogOpened.next(isLoading);
    }

    getDialogOpenProperty(): Observable<boolean> {
        return this.isDialogOpened.asObservable();
    }

    setNotificationProperty(showNotification: boolean = true) {
        this.showNotification.next(showNotification);
    }

    getNotificationProperty(): Observable<boolean> {
        return this.showNotification.asObservable();
    }

    setBusinessNotificationProperty(showNotification: boolean = false) {
        this.showBusinessNotification.next(showNotification);
    }

    getBusinessNotificationProperty(): Observable<boolean> {
        return this.showBusinessNotification.asObservable();
    }

    getUpsellingQuoteProperty(): Observable<boolean> {
        return this.isUpSellingQuote.asObservable();
    }

    setUpsellingQuoteProperty(isUpSellingQuote: boolean = false) {
        this.isUpSellingQuote.next(isUpSellingQuote);
    }

    getInboundCallFlag(): Observable<boolean> {
        return this.isInbpundCallCame.asObservable();
    }

    setInboundCallFlag(isInbpundCallCame: boolean = false) {
        this.isInbpundCallCame.next(isInbpundCallCame);
    }

    setRolesAndPermissionsProperty(rolesAndPermissionsObj: RolesAndPermissionsObj) {
        this.rolesAndPermissionsSubject.next(rolesAndPermissionsObj);
    }

    getRolesAndPermissionsProperty(): Observable<RolesAndPermissionsObj> {
        return this.rolesAndPermissionsSubject.asObservable();
    }

    setExpirationTimerBySeconds(seconds: number) {
        this.expirationTimerBySeconds.next(seconds);
    }

    getExpirationTimerBySeconds(): Observable<number> {
        return this.expirationTimerBySeconds.asObservable();
    }

    setTabIndexForCustomerComponent(tabIndex: number) {
        this.tabIndexPersistance.next(tabIndex);
    }

    getTabIndexForCustomerComponent(): Observable<number> {
        return this.tabIndexPersistance.asObservable();
    }

    setTabIndexForMonitoringAndResponseComponent(monitoringTabIndex: number) {
        this.tabMonitoringIndexPersistance.next(monitoringTabIndex);
    }

    setTabActinCallflow(actionTabIndex: number) {
        this.actinTaIndexCallflow.next(actionTabIndex);
    }

    getTabIndexForMonitoringAndResponseComponent(): Observable<number> {
        return this.tabMonitoringIndexPersistance.asObservable();
    }

    getTabActinCallflow(): Observable<number> {
        return this.actinTaIndexCallflow.asObservable();
    }


    setQuickActionComponent(moduleName: string) {
        this.quickActionSideBar.next(moduleName);
    }

    getQuickActionComponent(): Observable<string> {
        return this.quickActionSideBar.asObservable();
    }


    setCustomerDate(customer: any) {
        this.customerData.next(customer);
    }
    setCustomerProfile(customer: any) {
        this.customerProfile.next(customer);
    }

    setValidPassword(data: boolean) {
        this.validPassword.next(data)
    }
    setSelctedRowIndex(data: boolean) {
        this.selctedRowIndex.next(data)
    }

    setCallInitiationData(data: any) {
        this.callInitiationData.next(data);
    }

    getCallInitiationData(): Observable<any> {
        return this.callInitiationData.asObservable();
    }

    setRelocation(relocation: any) {
        this.relocationData.next(relocation);
    }

    getRelocation(): Observable<any> {
        return this.relocationData.asObservable();
    }

    setContactUpdateDetails(contact: any) {
        this.updatedContctData.next(contact);
    }

    getContactUpdateDetails(): Observable<any> {
        return this.updatedContctData.asObservable();
    }

    setisFromupsellData(upsell: any) {
        this.isFromupsellData.next(upsell);
    }

    getisFromupsellData(): Observable<any> {
        return this.isFromupsellData.asObservable();
    }


    getCustomerDate(): Observable<any> {
        return this.customerData.asObservable();
    }

    getCustomerProfile(): Observable<any> {
        return this.customerProfile.asObservable();
    }
    getValidPassword(): Observable<any> {
        return this.validPassword.asObservable();
    }
    getSelctedRowIndex(): Observable<any> {
        return this.selctedRowIndex.asObservable();
    }

    setBoundaryData(data: any) {
        this.boundaryData.next(data);
    }

    getBoundaryData(): Observable<any> {
        return this.boundaryData.asObservable();
    }

    setLeadNotesUpdateStatus(status: boolean = true) {
        this.scheduledUpdateStatus.next(status);
    }

    getLeadNotesUpdateStatus(): Observable<boolean> {
        return this.scheduledUpdateStatus.asObservable();
    }

    // cycle count add scanned items subject
    getCycleCountItemProperty(): Observable<{}> { // <RolesAndPermissionsObj>
        return this.cycleCountItemSubject.asObservable();
    }

    setCycleCountItemProperty(cycleCountItemObject) { // : RolesAndPermissionsObj
        this.cycleCountItemSubject.next(cycleCountItemObject);
    }

    setFormChangeDetectionProperty(isFormChangeDetected: boolean) {
        this.formChangeDetectionSubject.next(isFormChangeDetected);
    }

    getFormChangeDetectionProperty(): Observable<boolean> {
        return this.formChangeDetectionSubject.asObservable();
    }

    setFormChangeProperty(isFormChangeDetected: boolean) {
        this.formChangeSubject.next(isFormChangeDetected);
    }

    getFormChangeProperty(): Observable<boolean> {
        return this.formChangeSubject.asObservable();
    }

    setCustomerContactNumber(number: any) {
        this.customerContactData.next(number);
    }

    getCustomerContactNumber(): Observable<any> {
        return this.customerContactData.asObservable();
    }

    setUniqueCallId(id: any) {
        this.uniqueCallId.next(id);
    }

    getUniqueCallId(): Observable<any> {
        return this.uniqueCallId.asObservable();
    }

    setInboundCallDetails(data: any) {
        this.inboundCallDetails.next(data);
    }

    getInboundCallDetails(): Observable<any> {
        return this.inboundCallDetails.asObservable();
    }

    setInboundVisited(data: any) {
        this.isInboundVisited.next(data);
    }

    getInboundVisited(): Observable<any> {
        return this.isInboundVisited.asObservable();
    }

    setDailCall(data: any) {
        this.isDailedCall.next(data);
    }

    getDailCall(): Observable<any> {
        return this.isDailedCall.asObservable();
    }
    setRequisitionData(requisitionData: any) {
        this.requisitionData.next(requisitionData);
    }

    getRequisitionData(): Observable<any> {
        return this.requisitionData.asObservable();
    }

    setscheduleCallbackDetails(data: any) {
        this.scheduledCallbackData.next(data);
    }

    getscheduleCallbackDetails(): Observable<any> {
        return this.scheduledCallbackData.asObservable();
    }

    setExtensionNumber(data: any) {
        this.extentionNumber.next(data);
    }

    getExtensionNumber(): Observable<any> {
        return this.extentionNumber.asObservable();
    }

    setExpandOpenScape(expandOpenScape: boolean) {
        this.expandOpenScape.next(expandOpenScape);
    }

    getExpandOpenScape(): Observable<boolean> {
        return this.expandOpenScape.asObservable();
    }

    setPropertyValue(value: number) {
        this.propertySubject.next(value);
    }

    getPropertyValue(): Observable<number> {
        return this.propertySubject.asObservable();
    }

    setAnyPropertyValue(value: any) {
        this.anyPropertySubject.next(value);
    }

    getAnyPropertyValue(): Observable<any> {
        return this.anyPropertySubject.asObservable();
    }

    setCrmLocationWarehouse(data) {
        this.crmLocationWarehouseSubject.next(data);
    }

    getCrmLocationWarehouse(): Observable<boolean> {
        return this.crmLocationWarehouseSubject.asObservable();
    }

    getFromUrl(): Observable<string> {
        return this.fromUrlSubject.asObservable();
    }

    setFromUrl(fromUrl: string) {
        this.fromUrlSubject.next(fromUrl);
    }

    getFormChangesDetectionPropertyForPageReload(): Observable<boolean> {
        return this.formChangesDetectionPropertyForPageReload.asObservable();
    }

    setFormChangesDetectionPropertyForPageReload(isFormChangesDetected: boolean) {
        this.formChangesDetectionPropertyForPageReload.next(isFormChangesDetected);
    }

    getIsInstallationDebtor(): Observable<boolean> {
        return this.isInstallationDebtorRoute.asObservable();
    }

    setIsInstallationDebtor(IsInstallationDebtor: boolean) {
        this.isInstallationDebtorRoute.next(IsInstallationDebtor);
    }

    setFormSubmittedSuccessfullyProperty(isFormSubmittedSuccessfully: boolean) {
        this.formSubmittedSuccessfullySubject.next(isFormSubmittedSuccessfully);
    }

    getFormSubmittedSuccessfullyProperty(): Observable<boolean> {
        return this.formSubmittedSuccessfullySubject.asObservable();
    }

    setNotifications(notifications) {
        this.notificationsSubject.next(notifications);
    }

    getNotifications() {
        return this.notificationsSubject.asObservable();
    }

    setTroubleShootAlarm(notifications) {
        this.troubleShootAlarm.next(notifications);
    }

    getTroubleShootAlarm() {
        return this.troubleShootAlarm.asObservable();
    }

    setCustomerAddresId(data: any) {
        this.customerAddressId.next(data);
    }

    setCustomerPartitionId(data: any) {
        this.customerPartitionId.next(data);
    }
    getCustomerAddresId(): Observable<any> {
        return this.customerAddressId.asObservable();
    }
    getCustomerPartitionId(): Observable<any> {
        return this.customerPartitionId.asObservable();
    }

    setCustomerId(data: any) {
        this.customerId.next(data);
    }
    getCustomerId(): Observable<any> {
        return this.customerId.asObservable();
    }

    setStackAreaConfigId(data: any) {
        this.stackAreaConfigId.next(data);
    }
    getStackAreaConfigId(): Observable<any> {
        return this.stackAreaConfigId.asObservable();
    }

    getIsSpecialProjectCreationClicked(): Observable<boolean> {
        return this.isSpecialProjectCreationClicked.asObservable();
    }

    setIsSpecialProjectCreationClicked(isClicked: boolean) {
        this.isSpecialProjectCreationClicked.next(isClicked);
    }

    getIsSpecialProjectTechnicianClicked(): Observable<boolean> {
        return this.isSpecialProjectTechnicianClicked.asObservable();
    }

    setIsSpecialProjectTechnicianClicked(isClicked: boolean) {
        this.isSpecialProjectTechnicianClicked.next(isClicked);
    }

    getIsSpecialProjectCoordinatorClicked(): Observable<boolean> {
        return this.isSpecialProjectCoordinatorClicked.asObservable();
    }

    setIsSpecialProjectCoordinatorClicked(isClicked: boolean) {
        this.isSpecialProjectCoordinatorClicked.next(isClicked);
    }

    setContractReviewProperty(contractReviewQualityCheck: boolean) {
        this.contractReviewQualityCheck.next(contractReviewQualityCheck);
    }

    getContractReviewProperty(): Observable<boolean> {
        return this.contractReviewQualityCheck.asObservable();
    }

    setLoadCustomerCategoryList(loadCustomerCategoryList: boolean) {
        this.loadCustomerCategoryList.next(loadCustomerCategoryList);
    }

    getLoadCustomerCategoryList(): Observable<boolean> {
        return this.loadCustomerCategoryList.asObservable();
    }

    getCustomerContactInfo(): Observable<any> {
        return this.customerContactInfo.asObservable();
    }

    setAuthenticationHeaderProperty(setHeaderProperty: boolean) {
        this.authenticationHeaderSubject.next(setHeaderProperty);
    }

    getAuthenticationHeaderProperty(): Observable<boolean> {
        return this.authenticationHeaderSubject.asObservable();
    }

    setGoToTroubleShootProperty(setHeaderProperty: boolean) {
        this.goToTroubleShoot.next(setHeaderProperty);
    }

    getGoToTroubleShootProperty(): Observable<boolean> {
        return this.goToTroubleShoot.asObservable();
    }

    setLoadTechnicianProperty(isLoadTechnician: boolean) {
        this.isLoadTechnician.next(isLoadTechnician);
    }

    getLoadTechnicianProperty(): Observable<boolean> {
        return this.isLoadTechnician.asObservable();
    }
    setIsRefershRiskwatchRequestsProperty(boolean: boolean) {
        this.isRefershRiskwatchRequests.next(boolean);
    }

    getIsRefershRiskwatchRequestsProperty(): Observable<boolean> {
        return this.isRefershRiskwatchRequests.asObservable();
    }

    setIncentiveBtnElement(flag: boolean) {
        this.isIncentiveBtnClick.next(flag);
    }

    getIncentiveBtnElement(): Observable<boolean> {
        return this.isIncentiveBtnClick.asObservable();
    }

    // setCustomerVerficationProperty(setcustomerverfication: boolean) {
    //     this.customerverficationData.customerverfication = setcustomerverfication
    // }

    // getCustomerVerficationProperty():boolean {
    //     return this.customerverficationData.customerverfication;
    // }

    setCustomerContactInfo(data: any) {
        this.customerContactInfo.next(data);
    }

    navigateToLeadListPage() {
        this.combineLatestNgrxStoreData();
        if (this.leadHeaderData.fromUrl == 'Leads List') {
            this.router.navigateByUrl('/sales/task-management/leads');
        }
        else {
            this.router.navigate(['my-tasks/my-sales'],
                { queryParams: { leadId: this.leadHeaderData.leadId, tab: 1 } })
        }
    }

    navigateToProcessQuotationPage() {
        this.combineLatestNgrxStoreData();
        if (this.leadHeaderData.fromUrl == 'Leads List') {
            this.router.navigate(['/sales/task-management/leads/quotation/view'], { queryParams: { leadId: this.leadHeaderData.leadId } });
        }
        else {
            this.router.navigate(['/my-tasks/my-sales/my-leads/quotation/view'], { queryParams: { leadId: this.leadHeaderData.leadId } });
        }
    }

    navigateToAgreementSummaryPage() {
        this.combineLatestNgrxStoreDataForDealerCustomerAgreementSummary();
        this.router.navigate(['/dealer/dealer-contract/dealer-agreement-summary'],
            { queryParams: { customerId: this.dealerCustomerServiceAgreementParamsDataModel.customerId, addressId: this.dealerCustomerServiceAgreementParamsDataModel.addressId } });
    }

    navigateToLeadViewPage() {
        this.combineLatestNgrxStoreData();
        if (this.leadHeaderData.fromUrl == 'Leads List') {
            this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: this.leadHeaderData.leadId } });
        }
        else {
            this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/view'], { queryParams: { leadId: this.leadHeaderData.leadId } });
        }
    }

    setViewCustomerData(data) {
        this.viewCustomerData = data;
    }

    navigateToViewCustomerPage() {
        const navigateTab = this.viewCustomerData['customerTab']?.toString() ? this.viewCustomerData['customerTab'] : 0;
        const monitoringTab = this.viewCustomerData['monitoringTab']?.toString() ? this.viewCustomerData['monitoringTab'] : 0;
        let queryParams = { addressId: this.viewCustomerData['addressId'], navigateTab: navigateTab, monitoringTab: monitoringTab };
        if (this.viewCustomerData['feature_name'] && this.viewCustomerData['featureIndex']?.toString()) {
            queryParams['feature_name'] = this.viewCustomerData['feature_name'];
            queryParams['featureIndex'] = this.viewCustomerData['featureIndex'];
        }
        if (this.viewCustomerData['isRadio'] && this.viewCustomerData['radioRemovalWorkListId']) {
            queryParams['isRadio'] = this.viewCustomerData['isRadio'];
            queryParams['radioRemovalWorkListId'] = this.viewCustomerData['radioRemovalWorkListId'];
        }
        this.router.navigate([`/customer/manage-customers/view`, this.viewCustomerData['customerId']], { queryParams: queryParams, skipLocationChange: true });
    }
}
