import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { API_SUFFIX_MODELS, formatMobileNumbersBeforeSubmit, ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { environment } from '@environments/environment';
import { Observable } from "rxjs";
@Injectable({ providedIn: "root" })
export class CrudService {
  apiPrefixEnvironmentUrl: string;
  constructor(private http: HttpClient) { }

  prepareApiPrefixBasedOnLoadedModule(
    moduleName: ModulesBasedApiSuffix,
    apiVersion?: Number
  ): string {
    let moduleApi: string;
    switch (moduleName) {
      case ModulesBasedApiSuffix.SALES:
        if (environment.API_VERSION == apiVersion) {
          moduleApi =
            environment.SALES_API;
        } else {
          moduleApi =
            environment.salesApi;
        }
        break;
      case ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT:
        moduleApi =
          environment.customerManagementApi;
        break;
      case ModulesBasedApiSuffix.INVENTORY:
        moduleApi =
          environment.inventoryApi;
        break;
      case ModulesBasedApiSuffix.IT_MANAGEMENT:
        moduleApi =
          environment.ITManagementApi;
        break;
      case ModulesBasedApiSuffix.AUTHENTICATION:
        moduleApi =
          environment.authenticationApi;
        break;
      case ModulesBasedApiSuffix.CALL_CENTER:
        moduleApi =
          environment.callCenterApi;
        break;
      case ModulesBasedApiSuffix.TELEPHONY:
        moduleApi =
          environment.telephonyApi;
        break;
      case ModulesBasedApiSuffix.BILLING:
        moduleApi =
          environment.billingApi;
        break;
      case ModulesBasedApiSuffix.TRANSLATOR:
        moduleApi =
          environment.TRANSLATOR_CONFIG_API;
        break;
      case ModulesBasedApiSuffix.TRANSLATOR_CONFIG:
        moduleApi = environment.TRANSLATOR_CONFIG_API;
        break;
      case ModulesBasedApiSuffix.EVENT_MANAGEMENT:
        moduleApi = environment.EVENT_MANAGEMENT_API;
        break;
      case ModulesBasedApiSuffix.ROUTING_STATE:
        moduleApi = environment.telephonyApi;
        break;
      case ModulesBasedApiSuffix.COMMON_API:
        moduleApi = environment.COMMON_API;
        break;
      case ModulesBasedApiSuffix.ROUTING_STATE:
        moduleApi =
          environment.setRoutingApi;
        break;
      case ModulesBasedApiSuffix.SHARED:
        moduleApi = environment.sharedApi;
        break;
      case ModulesBasedApiSuffix.TECHNICIAN:
        moduleApi = environment.technicianAPI;
        break;

      case ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT:
        moduleApi = environment.ITManagementApi;
        break;
      case ModulesBasedApiSuffix.FILE_PROCESS:
        moduleApi = environment.FILE_PROCESS_API;
        break;
      case ModulesBasedApiSuffix.DEALER:
        moduleApi = environment.DEALER_API;
        break;
      case ModulesBasedApiSuffix.COLLECTIONS:
        moduleApi = environment.COLLECTIONS_API;
        break;
      case ModulesBasedApiSuffix.OPEN_SCAP_EMAIL:
        moduleApi = environment.OPEN_SCAP_EMAIL_API;
        break;
      case ModulesBasedApiSuffix.PANIC_APP:
        moduleApi = environment.PANIC_APP_API;
        break;
      case ModulesBasedApiSuffix.PDF:
          moduleApi = environment.pdfUrl;
          break;
    }
    return moduleApi;
  }

  create(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    body?: object | Array<object>,
    apiVersion = 0
  ): Observable<IApplicationResponse> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName, apiVersion
    );
    //body=formatMobileNumbersBeforeSubmit(body);
    return this.http.post<IApplicationResponse>(
      apiPrefixEnvironmentUrl + apiSuffix,
      body
    );
  }

  fileUpload(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    body: object | Array<object>,
    apiVersion = 0
  ): Observable<IApplicationResponse> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName, apiVersion
    );
    return this.http.post<IApplicationResponse>(
      apiPrefixEnvironmentUrl + apiSuffix,
      body
    );
  }

  // get(
  //   moduleName: ModulesBasedApiSuffix,
  //   apiSuffix: UserModuleApiSuffixModels | InventoryModuleApiSuffixModels | CustomerModuleApiSuffixModels | TechnicalMgntModuleApiSuffixModels | OutOfOfficeModuleApiSuffixModels |
  //     SalesModuleApiSuffixModels | AuthenticationModuleApiSuffixModels | CallCenterModuleApiSuffixModels | TelephonyApiSuffixModules |
  //     CommonModuleApiSuffixModels | BillingModuleApiSuffixModels | TranslatorModuleApiSuffixModels | EventMgntModuleApiSuffixModels | CollectionModuleApiSuffixModels |
  //     ChatRoomModuleApiSuffixModels | SharedModuleApiSuffixModels | ItManagementApiSuffixModels | FAQModuleApiSuffixModels | DealerModuleApiSuffixModels | OpenScapModuleApiSuffixModels | ReportsModuleApiSuffixModels | PanicAppApiSuffixModels | CampaignModuleApiSuffixModels,
  //   retrievableId?: string | number | undefined | null,
  //   areAllDataRequired?: boolean,
  //   params?: HttpParams,
  //   apiVersion = 0
  // ): Observable<IApplicationResponse> {

  //   let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
  //     moduleName, apiVersion
  //   );
  //   if (retrievableId) {
  //     if(params) {
  //       return this.http.get<IApplicationResponse>(
  //         apiPrefixEnvironmentUrl + apiSuffix + "/" + retrievableId, params ? { params } : { params: {} }
  //       );
  //     }else {
  //       return this.http.get<IApplicationResponse>(
  //         apiPrefixEnvironmentUrl + apiSuffix + "/" + retrievableId
  //       );
  //     }

  //   }
  //   else {
  //     if (areAllDataRequired) {
  //       return this.http.get<IApplicationResponse>(
  //         apiPrefixEnvironmentUrl + apiSuffix,
  //         params ? { params } : { params: {} }
  //       );
  //     } else {
  //       return this.http.get<IApplicationResponse>(
  //         apiPrefixEnvironmentUrl + apiSuffix,
  //         { params }
  //       );
  //     }
  //   }
  // }

  get(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    retrievableId?: string | number | undefined | null,
    areAllDataRequired?: boolean,
    params?: HttpParams,
    apiVersion = 0
  ): Observable<IApplicationResponse> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName, apiVersion
    );
    if (retrievableId) {
      let finalParams: string;
      let URL: string;
      if (params) {
        // finalParams = params ? params.toString().replace('%20', ' ') : null;
        // finalParams = params ? params.toString().replace('%2520', ' ') : null;
        URL = apiPrefixEnvironmentUrl + apiSuffix + "/" + retrievableId + "?" + params;
      }
      else {
        URL = apiPrefixEnvironmentUrl + apiSuffix + "/" + retrievableId;
      }
      if (params) {
        return this.http.get<IApplicationResponse>(
          URL
        );
      } else {
        return this.http.get<IApplicationResponse>(
          apiPrefixEnvironmentUrl + apiSuffix + "/" + retrievableId
        );
      }
    }
    else {
      let finalParams: string;
      let URL: string;
      if (params) {
        // finalParams = params ? params.toString().replace('%20', ' ') : null;
        // finalParams = params ? params.toString().replace('%2520', ' ') : null;
        URL = apiPrefixEnvironmentUrl + apiSuffix + "?" + params;
      }
      else {
        URL = apiPrefixEnvironmentUrl + apiSuffix;
      }
      if (areAllDataRequired) {
        return this.http.get<IApplicationResponse>(
          URL
        );
      } else {
        return this.http.get<IApplicationResponse>(
          URL
        );
      }
    }
  }

  dropdown(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    params?: HttpParams,
    apiVersion = 0
  ): Observable<IApplicationResponse> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName, apiVersion
    );
    return this.http.get<IApplicationResponse>(
      apiPrefixEnvironmentUrl + apiSuffix,
      params ? { params } : { params: {} }
    );
  }

  update(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    body: object | Array<object>,
    apiVersion = 0
  ): Observable<IApplicationResponse> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName, apiVersion
    );
    //body=formatMobileNumbersBeforeSubmit(body);
    return this.http.put<IApplicationResponse>(
      apiPrefixEnvironmentUrl + apiSuffix,
      body
    );
  }

  enableDisable(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    body: object | Array<object>,
    apiVersion: Number = 0

  ): Observable<IApplicationResponse> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName, apiVersion
    );
    return this.http.put<IApplicationResponse>(
      apiPrefixEnvironmentUrl + apiSuffix + '/enable-disable',
      body
    );
  }

  delete(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    selectedIds: string,
    params?: HttpParams,
    apiVersion = 0
  ): Observable<IApplicationResponse> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName, apiVersion
    );
    return this.http.delete<IApplicationResponse>(
      apiPrefixEnvironmentUrl + apiSuffix,
      {
        params: selectedIds ? { ids: selectedIds } : params
      }
    );
  }

  deleteByParams(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    body: object,
    apiVersion = 0
  ): Observable<IApplicationResponse> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName,
      apiVersion
    );
    return this.http.delete<IApplicationResponse>(
      apiPrefixEnvironmentUrl + apiSuffix,
      body
    );
  }


  downloadFile(
    moduleName: ModulesBasedApiSuffix,
    apiSuffix: typeof API_SUFFIX_MODELS,
    body?: object,
    apiVersion = 0,
    params?: HttpParams | string,
  ): Observable<any> {
    let apiPrefixEnvironmentUrl = this.prepareApiPrefixBasedOnLoadedModule(
      moduleName,
      apiVersion
    );
    if(params)
    return this.http.get(apiPrefixEnvironmentUrl + apiSuffix + params, {
      responseType: 'blob',
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/octet-stream'
      })
    })
    else
    return this.http.get(apiPrefixEnvironmentUrl + apiSuffix, {
      responseType: 'blob',
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/octet-stream'
      })
    })
  }
}
