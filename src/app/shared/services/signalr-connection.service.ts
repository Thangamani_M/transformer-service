import { Injectable } from '@angular/core';
import { AppState } from '@app/reducers';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import { environment } from '@environments/environment';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { LoggedInUserModel } from '../utils';
import * as CryptoJS from "crypto-js";
@Injectable({
  providedIn: 'root'
})
export class SignalrConnectionService {
  connection: HubConnection;
  private signalEventReceiveSubject = new BehaviorSubject(null);
  hubConnectionForSalesAPI: HubConnection;
  hubConnectionForTelephonyAPI: HubConnection;
  hubConnectionForChatAPI: HubConnection;
  loggedInUserData: LoggedInUserModel;

  constructor(private store: Store<AppState>) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)
    ]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  hubConnection(): void {
    this.connection = new HubConnectionBuilder()
      .withUrl(environment.SALES_API, {
      }).build();
    this.connection.start()
      .then(() => {
        this.triggerEvent()
      })
      .catch((error) => {
        console.log('Connection Failed : ', error);
      });
    this.connection.onclose(() => {
      // this.hubConnection();
    });
  }

  hubConnectionForTelephonyAPIPromise: Promise<void>;
  hubConnectionInitializationForTelephonyAPIPromise() {
    // Reuse the hub instance
    if (this.hubConnectionForTelephonyAPIPromise) {
      return this.hubConnectionForTelephonyAPIPromise;
    }
    else {
      this.buildHubConnection('telephony');
      this.hubConnectionForTelephonyAPIPromise = this.hubConnectionForTelephonyAPI.start();
      this.hubConnectionForTelephonyAPIPromise.
        then(() => console.log('Telephony API Hub Connection is started..!!'))
        .catch((error) => {
          console.log('Telephony API Hub Connection is Failed because of : ', error);
        });
      this.hubConnectionForTelephonyAPI.onclose(() => {
        console.log('Telephony API Hub Connection is closed..!!');
        if (this.loggedInUserData?.userId) {
          this.hubConnectionForTelephonyAPIPromise = undefined;
          this.hubConnectionForTelephonyAPI = undefined;
          this.hubConnectionInitializationForTelephonyAPIPromise();
        }
      });
      return this.hubConnectionForTelephonyAPIPromise;
    }
  }

  salesAPIHubConnectionBuiltInstance() {
    if (this.hubConnectionForSalesAPI) {
      return this.hubConnectionForSalesAPI;
    }
    else {
      this.buildHubConnection('sales');
    }
  }

  hubConnectionForSalesAPIPromise: Promise<void>;
  hubConnectionInitializationForSalesAPIPromise() {
    // Reuse the hub instance
    if (this.hubConnectionForSalesAPIPromise) {
      return this.hubConnectionForSalesAPIPromise;
    }
    else {
      this.buildHubConnection('sales');
      this.hubConnectionForSalesAPIPromise = this.hubConnectionForSalesAPI.start();
      this.hubConnectionForSalesAPIPromise.
        then(() => console.log('Sales API Hub Connection is started..!!'))
        .catch((error) => {
          //console.log('Sales API Hub Connection is Failed because of : ', error);
        });
      this.hubConnectionForSalesAPI.onclose(() => {
        console.log('Sales API Hub Connection is closed..!!');
        if (this.loggedInUserData?.userId) {
          this.hubConnectionForSalesAPI = undefined;
          this.hubConnectionForSalesAPIPromise = undefined;
          this.hubConnectionInitializationForSalesAPIPromise();
        }
      });
      return this.hubConnectionForSalesAPIPromise;
    }
  }

  telephonyAPIHubConnectionBuiltInstance() {
    if (this.hubConnectionForTelephonyAPI) {
      return this.hubConnectionForTelephonyAPI;
    }
    else {
      this.buildHubConnection('telephony');
    }
  }

  buildHubConnection(type: string) {
    if (type == 'sales') {
      this.hubConnectionForSalesAPI = new HubConnectionBuilder()
        .configureLogging(LogLevel.Critical)
        .withUrl(environment.SALES_API)
        .build();
      this.hubConnectionForSalesAPI.keepAliveIntervalInMilliseconds = 30000000;
    }
    else if (type == 'telephony') {
      this.hubConnectionForTelephonyAPI = new HubConnectionBuilder()
        .configureLogging(LogLevel.Critical)
        .withUrl(environment.telephonyApi)
        .build();
    }
    else if (type == 'chat') {
      this.hubConnectionForChatAPI = new HubConnectionBuilder()
        // .configureLogging(LogLevel.Critical)
        .withUrl(environment.COMMON_API, {
          accessTokenFactory: () => {
            return this.generateAccessToken(this.loggedInUserData?.userId);
          }
        }).build();
    }
  }

  // for chat start connection
  generateAccessToken(userName) {
    var header = {
      "alg": "HS256",
      "typ": "JWT"
    };
    var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
    var encodedHeader = this.base64url(stringifiedHeader);
    // customize your JWT token payload here
    var data = {
      "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier": userName,
      "exp": 1699819025,
      'admin': 0
    };
    var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
    var encodedData = this.base64url(stringifiedData);
    var token = encodedHeader + "." + encodedData;
    var secret = "myfunctionauthtest"; // do not expose your secret here
    var signature = CryptoJS.HmacSHA256(token, secret);
    signature = this.base64url(signature);
    var signedToken = token + "." + signature;
    return signedToken;
  }

  base64url(source) {
    // Encode in classical base64
    let encodedSource = CryptoJS.enc.Base64.stringify(source);
    // Remove padding equal characters
    encodedSource = encodedSource.replace(/=+$/, '');
    // Replace characters according to base64url specifications
    encodedSource = encodedSource.replace(/\+/g, '-');
    encodedSource = encodedSource.replace(/\//g, '_');
    return encodedSource;
  }

  chatAPIHubConnectionBuiltInstance() {
    if (this.hubConnectionForChatAPI) {
      return this.hubConnectionForChatAPI;
    }
    else {
      this.buildHubConnection('chat');
    }
  }

  hubConnectionForChatAPIPromise: Promise<void>;
  hubConnectionInitializationForChatAPIPromise() {
    // Reuse the hub instance
    if (this.hubConnectionForChatAPIPromise) {
      return this.hubConnectionForChatAPIPromise;
    }
    else {
      this.buildHubConnection('chat');
      this.hubConnectionForChatAPIPromise = this.hubConnectionForChatAPI.start();
      this.hubConnectionForChatAPIPromise.
        then(() => console.log('Chat API Hub Connection is started..!!'))
        .catch((error) => {
          //console.log('Chat API Hub Connection is Failed because of : ', error);
        });
      this.hubConnectionForChatAPI.onclose(() => {
        console.log('Chat API Hub Connection is closed..!!');
        if (this.loggedInUserData?.userId) {
          this.hubConnectionForChatAPI = undefined;
          this.hubConnectionForChatAPIPromise = undefined;
          this.hubConnectionInitializationForChatAPIPromise();
        }
      });
      return this.hubConnectionForChatAPIPromise;
    }
  }
  // for chat end connection

  closeAllHubConnections() {
    this.hubConnectionForSalesAPI?.stop();
    this.hubConnectionForTelephonyAPI?.stop();
    this.hubConnectionForChatAPI?.stop();
    this.hubConnectionForSalesAPI = undefined;
    this.hubConnectionForTelephonyAPI = undefined;
    this.hubConnectionForChatAPI = undefined;
    this.hubConnectionForSalesAPIPromise = undefined;
    this.hubConnectionForTelephonyAPIPromise = undefined;
    this.hubConnectionForChatAPIPromise = undefined;
  }

  closeTelephonyAPIHubConnection() {
    this.hubConnectionForTelephonyAPI?.stop();
    this.hubConnectionForTelephonyAPI = undefined;
    this.hubConnectionForTelephonyAPIPromise = undefined;
  }

  closeChatAPIHubConnection() {
    this.hubConnectionForChatAPI?.stop();
    this.hubConnectionForChatAPI = undefined;
    this.hubConnectionForChatAPIPromise = undefined;
  }

  disconnectHub() {
    this.connection?.stop();
  }

  getObserveEvent(): Observable<boolean> {
    return this.signalEventReceiveSubject.asObservable();
  }

  unObserveEvent() {
    if (this.signalEventReceiveSubject.observers) {
      // return this.signalEventReceiveSubject.unsubscribe();
    }
  }

  triggerEvent() {
    this.connection.on("notifyEvent", data => {
      if (this.signalEventReceiveSubject.observers) {
        this.signalEventReceiveSubject.next({ event: 'notifyEvent', result: data });
      }
    });
    this.connection.on("ReceiveEvents", data => {
      if (this.signalEventReceiveSubject.observers) {
        this.signalEventReceiveSubject.next({ event: 'ReceiveEvents', result: data });
      }
    });
    this.connection.on("DispatchingAlertCallBackTrigger", data => {
      if (this.signalEventReceiveSubject.observers) {
        this.signalEventReceiveSubject.next({ event: 'DispatchingAlertCallBackTrigger', result: data });
      }
    });
    this.connection.on("OSCCEmailNotification", data => {
      if (this.signalEventReceiveSubject.observers) {
        this.signalEventReceiveSubject.next({ event: 'OSCCEmailNotification', result: data });
      }
    });
    this.connection.on("eventName", data => {
      if (this.signalEventReceiveSubject.observers) {
        this.signalEventReceiveSubject.next({ event: 'eventName', result: data });
      }
    });
  }
}















// import { Injectable } from '@angular/core';
// import { AppState } from '@app/reducers';
// import { HubConnection, HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
// import { environment } from '@environments/environment';
// import { loggedInUserData } from '@modules/others/auth.selectors';
// import { Store } from '@ngrx/store';
// import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
// import { LoggedInUserModel } from '../utils';
// @Injectable({
//   providedIn: 'root'
// })
// export class SignalrConnectionService {
//   connection: HubConnection;
//   private signalEventReceiveSubject = new BehaviorSubject(null);
//   hubConnectionForSalesAPI: HubConnection;
//   hubConnectionForTelephonyAPI: HubConnection;
//   loggedInUserData: LoggedInUserModel;

//   constructor(private store: Store<AppState>) {
//     this.combineLatestNgrxStoreData();
//   }

//   combineLatestNgrxStoreData() {
//     combineLatest([
//       this.store.select(loggedInUserData)
//     ]).subscribe((response) => {
//       this.loggedInUserData = new LoggedInUserModel(response[0]);
//     });
//   }

//   hubConnection(): void {
//     this.connection = new HubConnectionBuilder()
//       .withUrl(environment.SALES_API, {
//       }).build();
//     this.connection.start()
//       .then(() => {
//         this.triggerEvent()
//       })
//       .catch((error) => {
//         console.log('Connection Failed : ', error);
//       });
//     this.connection.onclose(() => {
//       // this.hubConnection();
//     });
//   }

//   hubConnectionForTelephonyAPIPromise: Promise<void>;
//   hubConnectionInitializationForTelephonyAPIPromise() {
//     // Reuse the hub instance
//     if (this.hubConnectionForTelephonyAPIPromise) {
//       return this.hubConnectionForTelephonyAPIPromise;
//     }
//     else {
//       this.buildHubConnection('telephony');
//       this.hubConnectionForTelephonyAPIPromise = this.hubConnectionForTelephonyAPI.start();
//       this.hubConnectionForTelephonyAPIPromise.
//         then(() => console.log('Telephony API Hub Connection is started..!!'))
//         .catch((error) => {
//           console.log('Telephony API Hub Connection is Failed because of : ', error);
//         });
//       this.hubConnectionForTelephonyAPI.onclose(() => {
//         console.log('Telephony API Hub Connection is closed..!!');
//         if (this.loggedInUserData?.userId) {
//           this.hubConnectionForTelephonyAPIPromise = undefined;
//           this.hubConnectionForTelephonyAPI = undefined;
//           this.hubConnectionInitializationForTelephonyAPIPromise();
//         }
//       });
//       return this.hubConnectionForTelephonyAPIPromise;
//     }
//   }

//   salesAPIHubConnectionBuiltInstance() {
//       return this.buildHubConnection('sales');
//   }

//   hubConnectionForSalesAPIPromise: Promise<void>;
//   hubConnectionInitializationForSalesAPIPromise() {
//     // Reuse the hub instance
//     if (this.hubConnectionForSalesAPIPromise) {
//       return this.hubConnectionForSalesAPIPromise;
//     }
//     else {
//       this.buildHubConnection('sales');
//       this.hubConnectionForSalesAPIPromise = this.hubConnectionForSalesAPI.start();
//       this.hubConnectionForSalesAPIPromise.
//         then(() => console.log('Sales API Hub Connection is started..!!'))
//         .catch((error) => {
//           //console.log('Sales API Hub Connection is Failed because of : ', error);
//         });
//       this.hubConnectionForSalesAPI.onclose(() => {
//         console.log('Sales API Hub Connection is closed..!!');
//         if (this.loggedInUserData?.userId) {
//           this.hubConnectionForSalesAPI = undefined;
//           this.hubConnectionForSalesAPIPromise = undefined;
//           this.hubConnectionInitializationForSalesAPIPromise();
//         }
//       });
//       return this.hubConnectionForSalesAPIPromise;
//     }
//   }

//   telephonyAPIHubConnectionBuiltInstance() {
//       return this.buildHubConnection('telephony');
//   }

//   buildHubConnection(type: string):HubConnection {
//     if (type == 'sales') {
//       if(!this.hubConnectionForSalesAPI){
//         this.hubConnectionForSalesAPI = new HubConnectionBuilder()
//         .configureLogging(LogLevel.Critical)
//         .withUrl(environment.SALES_API)
//         .build();
//       }
//       return this.hubConnectionForSalesAPI;
//     }
//     else if (type == 'telephony') {
//       if(!this.hubConnectionForTelephonyAPI){
//         this.hubConnectionForTelephonyAPI = new HubConnectionBuilder()
//         .configureLogging(LogLevel.Critical)
//         .withUrl(environment.telephonyApi)
//         .build();
//       }
//       return this.hubConnectionForTelephonyAPI;
//     }
//   }

//   closeAllHubConnections() {
//     this.hubConnectionForSalesAPI?.stop();
//     this.hubConnectionForTelephonyAPI?.stop();
//     this.hubConnectionForSalesAPI = undefined;
//     this.hubConnectionForTelephonyAPI = undefined;
//     this.hubConnectionForSalesAPIPromise = undefined;
//     this.hubConnectionForTelephonyAPIPromise = undefined;
//   }

//   closeTelephonyAPIHubConnection() {
//     this.hubConnectionForTelephonyAPI?.stop();
//     this.hubConnectionForTelephonyAPI = undefined;
//     this.hubConnectionForTelephonyAPIPromise = undefined;
//   }

//   disconnectHub() {
//     this.connection?.stop();
//   }

//   getObserveEvent(): Observable<boolean> {
//     return this.signalEventReceiveSubject.asObservable();
//   }

//   unObserveEvent() {
//     if (this.signalEventReceiveSubject.observers) {
//       // return this.signalEventReceiveSubject.unsubscribe();
//     }
//   }

//   triggerEvent() {
//     this.connection.on("notifyEvent", data => {
//       if (this.signalEventReceiveSubject.observers) {
//         this.signalEventReceiveSubject.next({ event: 'notifyEvent', result: data });
//       }
//     });
//     this.connection.on("ReceiveEvents", data => {
//       if (this.signalEventReceiveSubject.observers) {
//         this.signalEventReceiveSubject.next({ event: 'ReceiveEvents', result: data });
//       }
//     });
//     this.connection.on("DispatchingAlertCallBackTrigger", data => {
//       if (this.signalEventReceiveSubject.observers) {
//         this.signalEventReceiveSubject.next({ event: 'DispatchingAlertCallBackTrigger', result: data });
//       }
//     });
//     this.connection.on("OSCCEmailNotification", data => {
//       if (this.signalEventReceiveSubject.observers) {
//         this.signalEventReceiveSubject.next({ event: 'OSCCEmailNotification', result: data });
//       }
//     });
//     this.connection.on("eventName", data => {
//       if (this.signalEventReceiveSubject.observers) {
//         this.signalEventReceiveSubject.next({ event: 'eventName', result: data });
//       }
//     });
//   }
// }