import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';

@Injectable({ providedIn: 'root' })
export class AlertService {
    private subject = new Subject<IApplicationResponse>();
    private keepAfterRouteChange = false;

    constructor(private router: Router) {
        // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterRouteChange) {
                    // only keep for a single route change
                    this.keepAfterRouteChange = false;
                } else {
                    // clear alert message
                    this.clear();
                }
            }
        });
    }

    getAlert(): Observable<IApplicationResponse> {
        return this.subject.asObservable();
    }

    processAlert(response: IApplicationResponse, keepAfterRouteChange = false) {
        this.keepAfterRouteChange = keepAfterRouteChange;
        this.subject.next(response);
    }

    clear() {
        // clear by calling subject.next() without parameters
        this.subject.next();
    }
}