import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class MomentService {

  constructor() { }


  convertNormalToRailayTime(date) {
    return moment(date, ["h:mm A"]).format("HH:mm:ss");
  }

  convertRailayToNormalTime(date) {
    return moment(date, ["HH.mm"]).format("hh:mm a");
  }

  convertNormalToRailayTimeOnly(time) {
    return moment(time, '"hh:mm A"').format("HH:mm")

  }

  convertRailayToNormalTimeOnly(time) {
    return moment(time, '"hh:mm"').format("hh:mm a")

  }

  addSeconds(time, minutes) { // 1 minute added
    return moment(time, 'h:mm A').add(minutes, 'minutes').format("hh:mm a");
  }

  removeSeconds(date) {
    return moment(date, ["HH:mm"]).format("HH:mm");
  }

  addMinits(day, minutes) { //  minute added
    return moment(day).add(minutes, 'minutes').format();
  }

  addHoursMinits(day, hours, minutes) { //  minute added
    return moment(day).add(hours, 'hours').add(minutes, 'minutes').format();
  }

  removeMinits(day, minutes) { //  minute added
    return moment(day).subtract(minutes, 'minutes').format();
  }

  removeHoursMinits(day, hours, minutes) { //  minute added
    return moment(day).subtract(hours, 'hours').add(minutes, 'minutes').format();
  }

  localToUTC(date) {
    // return moment(date).utc().format();
    return moment(date).format('YYYY-MM-DD');
  }

  localToUTCDateTime(date) {
    return moment(date).format('YYYY-MM-DD');
    // return moment(date).format('YYYY-MM-DD h:mm:ss a');
  }

  toUTCDateTime(date) {
    return moment(date).utc();
    // return moment(date).format('YYYY-MM-DD h:mm:ss a');
  }

  convertToUTC(date) {

    return moment.utc(moment(date)).format()  // pass to utc
  }

  toDateTime(date) {
    return moment(date).format('YYYY-MM-DD h:mm:ss a');
  }

  toConvertDate(date) {
    return moment(date).format('YYYY-MM-DD h:mm:ss');
  }

  toDateTimeSeconds(date) {
    return moment(date).format('YYYY-MM-DD h:mm');
  }

  toFormateType(date, type) {
    return moment(date).format(type);

  }

  toFormat(date) {
    return moment(date).format()
  }

  toHoursMints12(date) {
    return moment(date).format('h:mm a');
  }

  toHoursMints24(date) {
    return moment(date).format('HH:mm');
  }

  timeDiff(fromTime, toTime) {
    let fromTime1 = moment(fromTime, "HH:mm:ss a")
    let toTime1 = moment(toTime, "HH:mm:ss a")
    // return toTime1.diff(fromTime1, 'hours')
    let diff = moment.duration(fromTime1.diff(toTime1));
    // let ck = [diff.asHours(), diff.minutes(), diff.seconds()].join(':')
    if (diff.asHours() < 0 || diff.minutes() < 0) {
      return false
    } else {
      return true
    }
    // return moment(fromTime).isAfter(toTime);
  }

  timeDuration(fromTime, toTime) { //'13:00:10'
    var startTime = moment(fromTime, 'hh:mm:ss a');
    var endTime = moment(toTime, 'hh:mm:ss a');
    var totalHours = (endTime.diff(startTime, 'hours'));
    var totalMinutes = endTime.diff(startTime, 'minutes');
    var clearMinutes = totalMinutes % 60;
    if (totalHours < 0 || clearMinutes < 0) {
      return false
    } else {
      return true
    }
  }

  getTimeDiff(fromTime, toTime) {
    let fromTime1 = moment(fromTime, "HH:mm:ss a")
    let toTime1 = moment(toTime, "HH:mm:ss a")
    // return toTime1.diff(fromTime1, 'hours')
    let diff = moment.duration(toTime1.diff(fromTime1));
    // let ck = [diff.asHours(), diff.minutes(), diff.seconds()].join(':')
    let hours = String(Math.round(diff.hours())).padStart(2, '0');
    let minutes = String(diff.minutes()).padStart(2, '0');
    return hours + ":" + minutes + ":00"

  }

  asMinutes(time) { //HH:mm
    moment.duration(time).asMinutes()

  }

  toMoment(date) {
    return moment(date);
  }

  getLongMonthsList() {
    let months = moment.months().map(function (e, index) {
      let i = index + 1
      return { id: i, displayName: e };
    });
    return months
  }

  getShortMonthsList() {
    let months = moment.monthsShort().map(function (e, index) {
      let i = index + 1
      return { id: i, displayName: e };
    });
    return months
  }

  getYesterday() {
    return moment().subtract(1, 'days').format()
  }

  isDateValid(date) {
    return moment(date).isValid()
  }

  setTime(time) {
    let spltime = time.split(':')

    // return moment().set({"hour": spltime[0], "minute": spltime[1]}).format()
    return new Date(moment().set({ "hour": spltime[0], "minute": spltime[1] }).format())
  }

  setTimetoRequieredDate(date, time) {
    let spltime = time.split(':')

    // return moment().set({"hour": spltime[0], "minute": spltime[1]}).format()
    return new Date(moment(date).set({ "hour": spltime[0], "minute": spltime[1] }).format())
  }

  convertTwelveToTwentyFourTime(d) {
    var dt = moment(d, ["h:mm A"]).format("HH:mm");
    return dt
  }


  addSevenDaysInCurrentDate(event, dayName) {
    var nxtDay = moment(event, "DD-MM-YYYY").add(7, 'days').day(dayName);
    var getDate = new Date(nxtDay.format());
    return getDate
  }

  isSameDay(fromDate, toDate) {
    return moment(fromDate).isSame(toDate, 'day');
  }

  isSameDayTime(fromDate, toDate) {
    return moment(fromDate).isSame(toDate);
  }

  betWeenTime(startTime, endTime, selectedTime) {
    // var time = moment() gives you current time. no format required.
    var time = moment(selectedTime, 'hh:mm:ss'),
      beforeTime = moment(startTime, 'hh:mm:ss'),
      afterTime = moment(endTime, 'hh:mm:ss');

    if (time.isBetween(beforeTime, afterTime)) {

      return true
    } else {

      return false
    }
  }

  betWeenDateTime(startTime, endTime, selectedTime) {
    // var time = moment() gives you current time. no format required.
    let time1 = moment(selectedTime).format('YYYY-MM-DD'),
      beforeTime1 = moment(startTime).format('YYYY-MM-DD'),
      afterTime1 = moment(endTime).format('YYYY-MM-DD');
    var time = moment(selectedTime, 'hh:mm'),
      beforeTime = moment(startTime, 'hh:mm'),
      afterTime = moment(endTime, 'hh:mm');

    // if (moment(selectedTime).format('YYYY-MM-DD').isBetween(beforeTime1, afterTime1)) {
    //   return true
    // } else {
    //   return false
    // }
    if (time.isBetween(beforeTime, afterTime)) {
      return true
    } else {

      return false
    }
  }

  isBefore(selectedDate) {
    return moment(selectedDate).isAfter(new Date());
  }

  isAfter(selectedDate) {
    return moment(selectedDate).isAfter(new Date());
  }

  addDays(selectedDay, numberOfDays) {
    return moment(selectedDay, "DD-MM-YYYY").add(numberOfDays, 'days');
  }

  addMinutes(selectedDay, numberOdMinutes) {
    return moment(selectedDay).add(numberOdMinutes, "minutes").format()
  }

  reduceMinutes(selectedDay, numberOdMinutes) {
    return moment(selectedDay).subtract(numberOdMinutes, "minutes").format()
  }

  addDaysWithFormate(selectedDay, numberOfDays, formate) {
    return moment(selectedDay, formate).add(numberOfDays, 'days');
  }

  disableParticularDayonEveryweek(date?: any) {
    let days = date ? moment(date).day() : moment().day();
    let array = [0, 1, 2, 3, 4, 5, 6];
    let index = array.findIndex(x => x == days);
    if (index >= 0) {
      array.splice(index, 1);
    }
    return array;
  }

  stringToDateFormat(date, format) {
    return moment(date, format);
  }

  scrollCalenderByTime(time) {
    let times = time ? time : '09:00:00'
    let timeNumber = Number(times.split(':')[0])
    return timeNumber * 60 // 60 is not time , its appromimate height of the time slap
  }

  endOfDayTime(day) { 
    return moment(day).endOf('day').format()
  }

  startOfDayTime(day) { 
    return moment(day).startOf('day').format()
  }

}
