import { Injectable } from "@angular/core";
import * as CryptoJS from "crypto-js";

const encodedUrlParamsToken = "encodedUrlParamsToken";

@Injectable({ providedIn: "root" })
export class UrlParamsService {
  private cryptoSecretKey = "hjsjhb87asjhajscjhb";
  /**
   * Sets url router params id or object.
   * @param urlRouterParams which is the urlRouterParams pass from one router to another.
   */
  set encodedUrlRouterParams(urlRouterParams) {
    if (urlRouterParams) {
      //  localStorage.setItem(encodedUrlParamsToken, urlRouterParams);
      this.encryptUrlRouterParamsData(urlRouterParams);
    } else {
      localStorage.removeItem(encodedUrlParamsToken);
    }
  }

  get encodedUrlRouterParams() {
    return this.decryptUrlRouterParamsData();
  }

  encryptUrlRouterParamsData(urlRouterParamsData): void {
    const cipherTextUrlRouterParamsData = CryptoJS.AES.encrypt(
      JSON.stringify(urlRouterParamsData),
      this.cryptoSecretKey
    ).toString();
    localStorage.setItem(encodedUrlParamsToken, cipherTextUrlRouterParamsData);
  }

  decryptUrlRouterParamsData() {
    const bytes = CryptoJS.AES.decrypt(
      localStorage.getItem(encodedUrlParamsToken),
      this.cryptoSecretKey
    );
    if (bytes) var originalText = bytes.toString(CryptoJS.enc.Utf8);
    return JSON.parse(originalText);
  }
}
