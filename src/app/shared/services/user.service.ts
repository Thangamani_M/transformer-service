import { HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { AuthenticationModuleApiSuffixModels, decryptData, encryptData, LoggedInUserModel, ModulesBasedApiSuffix, RoutingStates, SharedTableFilteredDataRemoved } from '@app/shared';
import { JwtHelperService } from "@auth0/angular-jwt";
import { encodedTokenSelector, loggedInUserData } from '@modules/others';
import { LeadCreationStepperParamsRemoveAction, LeadCreationUserDataRemoveAction, LeadHeaderDataRemoveAction } from '../../modules/sales/components/task-management/lead-creation-ngrx-files/lead-creation.actions';
import { select, Store } from '@ngrx/store';
import { Logout } from '../../modules/others/auth.actions';
import { StaticEagerLoadingRemove, DynamicEagerLoadingRemove } from '../../modules/others/eager-loading.actions';
import { MatDialog } from '@angular/material';
import { RxjsService } from './rxjs.services';
import { PageBasedPermissionsRemoved, SidebarDataRemoved } from '../../shared/layouts/layout.actions';
import { AgentLogoutDataRemoved } from '../../shared/layouts/app-header/state/agent-login.actions';
import { SignalrConnectionService } from './signalr-connection.service';
import { MsalService } from '@azure/msal-angular';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { CrudService } from './crud.service';

const encodedJWTTokenKey = 'encodedJWTToken';
const loggedUserDataKey = 'loggedUserData';
const agentLoginDataKey = 'agentLoginData';
const agentLoginMediaTypesKey = 'agentLoginMediaTypes';
const agentStatusKey = 'agentStatus';
const sidebarMenuKey = 'sidebarMenuKey';
const tokenExpirationTimeKey = 'tokenExpirationTime';
const defaultRedirectRouteUrlKey = 'defaultRedirectRouteUrl';
const expiresTimeInFullDateKey = 'expiresTimeInFullDate';
const refreshTokenKey = 'refreshToken';
const currentComponentPageBasedPermissionsKey = "";
const moduleBasedComponentPermissionsLey = "";
const tableFilteredFieldsKey = 'tableFilteredFields';
const openScapePabxIdKey = 'openScapePabxIdKey';
const agentKey = 'agentKey';
/**
 * Provides storage for authentication jwt token.
 */
@Injectable({ providedIn: 'root' })
export class UserService {
  encodedToken = '';
  private token: string | null = null;
  loggedInUserDataModel: LoggedInUserModel;

  constructor(private store: Store<AppState>, private router: Router, private dialog: MatDialog, private rxjsService: RxjsService,
    private signalrConnectionService: SignalrConnectionService, private msalService: MsalService, private crudService: CrudService) {
    this.store.pipe(select(encodedTokenSelector)).subscribe((encodedToken: string) => {
      this.encodedToken = encodedToken;
    });
    this.store.pipe(select(loggedInUserData)).subscribe((loggedInUserData: LoggedInUserModel) => {
      this.loggedInUserDataModel = new LoggedInUserModel(loggedInUserData);
    });
  }

  /**
   * Checks if the token is validated.
   * @return True  if the token is validated.
   */
  isTokenValid(): boolean {
    return this.token !== null ? true : false;
  }

  /**
    * Gets logged in decoded user data.
    * @param encodedToken which is extracted from the object when the user logs in.
    */
  getDecodedLoggedInData(encodedToken: string): object | null {
    const helper = new JwtHelperService();
    const isExpired = helper.isTokenExpired(encodedToken);
    if (!isExpired) {
      return helper.decodeToken(encodedToken);
    }
    return null;
  }

  /**
  * Sets logged in user data.
  * @param userData which is in object where the object is sent from the server whenever the user logged In.
  */
  set loggedInUserData(userData: LoggedInUserModel | null) {
    if (userData) {
      encryptData(userData, loggedUserDataKey);
    }
    else {
      localStorage.removeItem(loggedUserDataKey);
    }
  }

  /**
   * Gets the user data.
   * @return The object which contains user data.
   */
  get loggedInUserData(): LoggedInUserModel | null {
    if (localStorage.getItem(loggedUserDataKey)) {
      return decryptData(loggedUserDataKey);
    }
  }

  /**
  * Sets logged in user data.
  * @param encodedToken which is the encoded token given from server response when user logs in.
  */
  set encodedJWTToken(encodedToken: string | null) {
    if (encodedToken) {
      encryptData(encodedToken, encodedJWTTokenKey);
    }
    else {
      localStorage.removeItem(loggedUserDataKey);
    }
  }

  get encodedJWTToken() {
    if (localStorage.getItem(encodedJWTTokenKey)) {
      return decryptData(encodedJWTTokenKey);
    }
  }

  set sidebarMenuData(sidebarMenuList: Array<object> | null) {
    if (sidebarMenuList) {
      encryptData(sidebarMenuList, sidebarMenuKey);
    } else {
      localStorage.removeItem(sidebarMenuKey);
    }
  }

  get sidebarMenuData() {
    if (localStorage.getItem(sidebarMenuKey)) {
      return decryptData(sidebarMenuKey);
    }
  }

  set moduleBasedComponentPermissions(moduleBasedComponentPermissions: Array<object> | null) {
    if (moduleBasedComponentPermissions) {
      encryptData(moduleBasedComponentPermissions, moduleBasedComponentPermissionsLey);
    } else {
      localStorage.removeItem(moduleBasedComponentPermissionsLey);
    }
  }

  get moduleBasedComponentPermissions() {
    if (localStorage.getItem(moduleBasedComponentPermissionsLey)) {
      return decryptData(moduleBasedComponentPermissionsLey);
    }
  }

  set currentComponentPageBasedPermissions(currentComponentPageBasedPermissions: Array<object> | null) {
    if (currentComponentPageBasedPermissions) {
      encryptData(currentComponentPageBasedPermissions, currentComponentPageBasedPermissionsKey);
    } else {
      localStorage.removeItem(currentComponentPageBasedPermissionsKey);
    }
  }

  get currentComponentPageBasedPermissions() {
    if (localStorage.getItem(currentComponentPageBasedPermissionsKey)) {
      return decryptData(currentComponentPageBasedPermissionsKey);
    }
  }

  set agentLoginData(agentExtensionNo: string | null) {
    if (agentExtensionNo) {
      encryptData(agentExtensionNo, agentLoginDataKey);
    } else {
      localStorage.removeItem(agentLoginDataKey);
    }
  }

  get agentLoginData() {
    if (localStorage.getItem(agentLoginDataKey)) {
      return decryptData(agentLoginDataKey);
    }
  }

  get agentLoginMediaTypes() {
    if (localStorage.getItem(agentLoginMediaTypesKey)) {
      return decryptData(agentLoginMediaTypesKey);
    }
  }

  set agentLoginMediaTypes(mediaTypes: string[]) {
    if (mediaTypes && mediaTypes.length > 0) {
      encryptData(mediaTypes, agentLoginMediaTypesKey);
    } else {
      localStorage.removeItem(agentLoginMediaTypesKey);
    }
  }

  get agentStatus() {
    if (localStorage.getItem(agentStatusKey)) {
      return decryptData(agentStatusKey);
    }
  }

  set agentStatus(agentStatus: RoutingStates) {
    if (agentStatus) {
      encryptData(agentStatus, agentStatusKey);
    } else {
      localStorage.removeItem(agentStatusKey);
    }
  }

  get agentKey() {
    if (localStorage.getItem(agentKey)) {
      return decryptData(agentKey);
    }
  }

  set agentKey(agentKeyValue: string | null) {
    if (agentKeyValue) {
      encryptData(agentKeyValue, agentKey);
    } else {
      localStorage.removeItem(agentKey);
    }
  }

  /**
 * Sets logged in user's token expiration time.
 * @param time which is the time ( In Seconds ) given from server response when user logs in.
 */
  set tokenExpirationTimeInSeconds(tokenExpirationTime: number | null) {
    if (tokenExpirationTime) {
      encryptData(tokenExpirationTime, tokenExpirationTimeKey);
    }
    else {
      localStorage.removeItem(tokenExpirationTimeKey);
    }
  }

  get tokenExpirationTimeInSeconds() {
    if (localStorage.getItem(tokenExpirationTimeKey)) {
      return decryptData(tokenExpirationTimeKey);
    }
  }

  set defaultRedirectRouteUrl(routeUrl: string | null) {
    if (routeUrl) {
      encryptData(routeUrl, defaultRedirectRouteUrlKey);
    }
    else {
      localStorage.removeItem(defaultRedirectRouteUrlKey);
    }
  }

  get defaultRedirectRouteUrl() {
    if (localStorage.getItem(defaultRedirectRouteUrlKey)) {
      return decryptData(defaultRedirectRouteUrlKey);
    }
  }

  get expiresTimeInFullDate() {
    if (localStorage.getItem(expiresTimeInFullDateKey)) {
      return decryptData(expiresTimeInFullDateKey);
    }
  }

  set expiresTimeInFullDate(expiresTimeInFullDate: Date | null) {
    if (expiresTimeInFullDate) {
      encryptData(expiresTimeInFullDate, expiresTimeInFullDateKey);
    }
    else {
      localStorage.removeItem(expiresTimeInFullDateKey);
    }
  }

  get refreshToken() {
    if (localStorage.getItem(refreshTokenKey)) {
      return decryptData(refreshTokenKey);
    }
  }

  set refreshToken(refreshToken: string | null) {
    if (refreshToken) {
      encryptData(refreshToken, refreshTokenKey);
    }
    else {
      localStorage.removeItem(refreshTokenKey);
    }
  }

  // // uncomment the below methos for testing refresh token feature
  // count = 0;
  // setTokenHeader(request: HttpRequest<any>, encodedToken?: string): HttpRequest<any> {
  //   let headers;
  //   if (request.url.includes('lss') && request.method == 'GET' && this.count == 0) {
  //     this.count += 1;
  //     headers = new HttpHeaders({
  //       "Authorization": `Bearer ${encodedToken ? encodedToken : this.encodedToken}` + 'fgh'
  //     });
  //   }
  //   else {
  //     headers = new HttpHeaders({
  //       "Authorization": `Bearer ${encodedToken ? encodedToken : this.encodedToken}`
  //     });
  //   }
  //   request = request.clone({
  //     headers
  //   });
  //   return request;
  // }

  setRequestFromHeader (request:HttpRequest<any>): HttpRequest<any> {
    let headers = new HttpHeaders({
      "RequestedFrom":"CRM"
    });
    request = request.clone({
      headers
    });
    return request;
  }
  setTokenHeader(request: HttpRequest<any>, encodedToken?: string): HttpRequest<any> {
    let headers = new HttpHeaders({
      "Authorization": `Bearer ${encodedToken ? encodedToken : this.encodedToken}`,
      "RequestedFrom":"CRM"
    });
    request = request.clone({
      headers
    });
    return request;
  }

  onLogout(returnType = 'void') {
    // If the user is internal we have adReferenceId then need to go into the azure AD logout process
    if (this.loggedInUserDataModel.adReferenceId) {
      this.msalService?.logout();
    }
    else {
      if (returnType == 'void') {
        this.onAppLogout();
      }
      else if (returnType == 'observable') {
        return this.crudService.create(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.LOGOUT_V2, {
          userId: this.loggedInUserData.userId
        });
      }
    }
  }

  onAppLogout() {
    if (!this.loggedInUserData?.userId) return;
    this.crudService.create(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.LOGOUT_V2, {
      userId: this.loggedInUserData.userId
    }).pipe(map(result => result), catchError(error => of(error)))
      .subscribe((response) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.removeAllStorageDataAndAppData();
        }
      });
  }

  removeAllStorageDataAndAppData() {
    this.router.navigateByUrl('login');
    this.store.dispatch(new Logout());
    this.store.dispatch(new SidebarDataRemoved());
    this.store.dispatch(new AgentLogoutDataRemoved());
    this.store.dispatch(new StaticEagerLoadingRemove());
    this.store.dispatch(new DynamicEagerLoadingRemove());
    this.store.dispatch(new LeadCreationUserDataRemoveAction());
    this.store.dispatch(new LeadCreationStepperParamsRemoveAction());
    this.store.dispatch(new LeadHeaderDataRemoveAction());
    this.store.dispatch(new PageBasedPermissionsRemoved());
    this.store.dispatch(new SharedTableFilteredDataRemoved());
    this.rxjsService.setUniqueCallId(null);
    this.rxjsService.setCustomerContactNumber(null);
    this.rxjsService.setscheduleCallbackDetails(null);
    this.rxjsService.setAnyPropertyValue(null);
    this.signalrConnectionService.closeAllHubConnections();
    localStorage.clear();
    sessionStorage.clear();
    // close all opened angular material popups thoughout the entire application
    this.dialog.closeAll();
  }

  /**
  * Sets the table's filtered fields.
  * @param tableFilteredFields which is in object where the user filtered fields in the desired table.
  */
  set tableFilteredFields(tableFilteredFields) {
    if (tableFilteredFields) {
      encryptData(tableFilteredFields, tableFilteredFieldsKey);
    }
    else {
      localStorage.removeItem(tableFilteredFieldsKey);
    }
  }

  /**
   * Gets table's filtered fields.
   * @return the user filtered fields in the desired table.
   */
  get tableFilteredFields() {
    if (localStorage.getItem(tableFilteredFieldsKey)) {
      return decryptData(tableFilteredFieldsKey);
    }
  }

  get openScapePabxId() {
    if (localStorage.getItem(openScapePabxIdKey)) {
      return decryptData(openScapePabxIdKey);
    }
  }

  set openScapePabxId(openScapePabxId: string) {
    if (openScapePabxId) {
      encryptData(openScapePabxId, openScapePabxIdKey);
    }
    else {
      localStorage.removeItem(openScapePabxIdKey);
    }
  }
}
