import { Injectable } from "@angular/core";
import { FormBuilder } from "@angular/forms";


@Injectable({ providedIn: 'root' })
export class TableFilterFormService {

     constructor(private _fb: FormBuilder) {}
    //create form group
    createFormGroup(column) {
        let columns = [];
        let formControls = {};
        column.forEach(element => {
        columns.push(element.field)
        });
        let columsObj = Object.assign({}, columns)
        Object.values(columsObj).forEach((value) => {
        formControls[value] = [{ value: columsObj[value], disabled: false }]
        });
        return this._fb.group(formControls);
    }
}