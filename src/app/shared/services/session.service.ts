import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }
 

  setItem(item: string, value: any) {
    return sessionStorage.setItem(item, value)
  }

  getItem(item) {
    return sessionStorage.getItem(item)
  }

  removeItem(item) {
    sessionStorage.removeItem(item)
  }

  removeAllItem() {
    sessionStorage.clear()
  }

}
