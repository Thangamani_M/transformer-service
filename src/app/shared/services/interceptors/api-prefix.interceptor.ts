import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, UserService } from '@app/shared/services';
import { AuthenticationModuleApiSuffixModels, SharedModuleApiSuffixModels } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { encodedTokenSelector } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { retry, takeUntil } from 'rxjs/operators';
import { RxjsService } from '../rxjs.services';
@Injectable({ providedIn: 'root' })
export class ApiPrefixInterceptor implements HttpInterceptor {
  encodedToken: string;
  shouldAppendHeaderProperty = true;
  // cancel pending http requests while navigating among components
  constructor(private rxjsService: RxjsService, private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private userService: UserService) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.rxjsService.setGlobalLoaderProperty(true);
      }
    });
    this.store.pipe(select(encodedTokenSelector)).subscribe((encodedToken: string) => {
      this.encodedToken = encodedToken;
    });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.rxjsService.getAuthenticationHeaderProperty().subscribe((shouldAppendHeaderProperty: boolean) => {
      this.shouldAppendHeaderProperty = shouldAppendHeaderProperty;
    });
    if (request.url.includes(SharedModuleApiSuffixModels.NOTIFICATIONS) || request.url.includes(AuthenticationModuleApiSuffixModels.REFRESH_TOKEN_V2) ||
      request.url.includes(SalesModuleApiSuffixModels.SALES_API_UX_DYNAMIC) || request.url.includes(SharedModuleApiSuffixModels.MICROSOFT_ENDPOINT)) {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setPopupLoaderProperty(false);
    }
    request = this.userService.setRequestFromHeader(request);

    // check whether to append bearer token in authorization token header based on requests
    if (!request.urlWithParams.includes(AuthenticationModuleApiSuffixModels.LOGIN_V2) &&
      !request.urlWithParams.includes(AuthenticationModuleApiSuffixModels.SIGNUP) &&
      !request.urlWithParams.includes(SalesModuleApiSuffixModels.DEBTOR_ACCEPTANCE_DETAILS) &&
      !request.urlWithParams.includes(EventMgntModuleApiSuffixModels.WELCOME_PATROL) &&
      !request.urlWithParams.includes(AuthenticationModuleApiSuffixModels.GENERATE_PASSWORD)) {
      // append bearer token for every server request
      request = this.userService.setTokenHeader(request);
    }

    // retry http request again when the server request gets error
    let retryNoOfTimes = true ? 0 : 1;
    if (request.url.includes('telephony') && request.method == 'POST') {
      return next.handle(request)
    }
    else {
      let notifier1 = (request.url.includes('pageIndex') || request.method !== 'GET') ? this.httpCancelService.onCancelPendingRequestsOnFormSubmission() : null;
      if (notifier1) {
        return next.handle(request).pipe(
          takeUntil(this.httpCancelService.onCancelPendingRequestsOnNavigationChanged()),
          takeUntil(notifier1),
          retry(retryNoOfTimes)
        );
      }
      else {
        return next.handle(request).pipe(
          takeUntil(this.httpCancelService.onCancelPendingRequestsOnNavigationChanged()),
          retry(retryNoOfTimes)
        );
      }
    }
  }
}
