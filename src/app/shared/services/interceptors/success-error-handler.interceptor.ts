import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes, RestfulMethods } from '@app/shared/enums';
import { environment } from '@environments/environment';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { Login, LoginUpdate } from '../../../../app/modules/others/auth.actions';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, Subject, throwError } from 'rxjs';
import { catchError, filter, map, switchMap, finalize, take } from 'rxjs/operators';
import { RxjsService } from '../rxjs.services';
import { SnackbarService } from '../snackbar.service';
import { UserService } from '../user.service';
import { AuthenticationModuleApiSuffixModels, CrudService, LoggedInUserModel, ModulesBasedApiSuffix } from '@app/shared';
import { loggedInUserData, refreshToken } from '@modules/others';
@Injectable({ providedIn: 'root' })
export class SuccessErrorHandlerInterceptor implements HttpInterceptor {
  isDialogOpened = false;
  showNotification = false;
  restRequestMethod;
  request: HttpRequest<any>;
  loggedInUserData: LoggedInUserModel;
  refreshToken = '';
  broadcastChannel: BroadcastChannel;

  constructor(private rxjsService: RxjsService, private userService: UserService,
    private snackbarService: SnackbarService, private store: Store<AppState>, private crudService: CrudService) {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(refreshToken)
    ]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.refreshToken = response[1];
    });
    // Helps to share the encoded token after refresh token across the angular opened tabs
    this.broadcastChannel = new BroadcastChannel("Refresh Token Sharing Between Tabs");
    this.broadcastChannel.onmessage = (e) =>
      this.store.dispatch(new LoginUpdate({
        encodedToken: e.data.encodedToken,
        refreshToken: e.data.refreshToken
      }));
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.request = request;
    this.restRequestMethod = request.method;
    this.rxjsService.getDialogOpenProperty().subscribe((isDialogOpened: boolean) => {
      this.isDialogOpened = isDialogOpened;
    });
    if (this.isDialogOpened) {
      if ((request.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_PHONER) || request.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_DISPATCHER)) && request.method === RestfulMethods.POST ||
        request.url.includes(EventMgntModuleApiSuffixModels.MAP_MONITORING_CUSTOM)) {
        this.rxjsService.setPopupLoaderProperty(false);
      } else {
        this.rxjsService.setPopupLoaderProperty(true);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    else {
      if ((request.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_PHONER) || request.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_DISPATCHER)) && request.method === RestfulMethods.POST ||
        request.url.includes(EventMgntModuleApiSuffixModels.MAP_MONITORING_CUSTOM)) {
        this.rxjsService.setGlobalLoaderProperty(false);
        // this.rxjsService.setPopupLoaderProperty(false);
      } else {
        if (request.urlWithParams.includes('isDefaultLoaderDisabled') ||
          request?.body?.isDefaultLoaderDisabled) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.rxjsService.setPopupLoaderProperty(false);
        } else {
          this.rxjsService.setGlobalLoaderProperty(true);
          this.rxjsService.setPopupLoaderProperty(false);
        }
      }
    }
    return next.handle(request)
      .pipe(
        map(response => this.successHandler(response)),
        catchError(error => {
          if (error.status == 401) {
            return this.handleUnauthorizedRequest(request, next);
          }
          else {
            return this.errorHandler(error)
          }
        })
      );
  }

  private successHandler(response: HttpEvent<any>): HttpEvent<any> {
    if (response instanceof HttpResponse) {
      this.rxjsService.setAuthenticationHeaderProperty(true);
      if (this.restRequestMethod !== RestfulMethods.GET) {
        if (response.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_PHONER) || response.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_DISPATCHER)) {
        } else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      }
      if (response.body.statusCode == 404 || response.body.statusCode == 500 || response.body.statusCode == 0) {
        if (response.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_PHONER) || response.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_DISPATCHER)) {
        } else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      }
      if (this.restRequestMethod == RestfulMethods.POST || this.restRequestMethod == RestfulMethods.PUT) {
        this.rxjsService.setFormSubmittedSuccessfullyProperty(true);
      }
      //Validate enable of disable notification
      this.GetNotificationProperty();
      let { body: { message, statusCode, exceptionMessage } } = response;
      let type: ResponseMessageTypes;
      switch (statusCode) {
        case 200:
          type = ResponseMessageTypes.SUCCESS;
          break;
        case 400:
          type = ResponseMessageTypes.WARNING;
          break;
        case 401:
          break;
        case 409:
          type = ResponseMessageTypes.WARNING;
          break;
        default:
          type = ResponseMessageTypes.ERROR;
          break;
      }
      if (message && this.showNotification == true) {
        if (message !== 'Record not found' && !message.includes('Lead This address exists in Fidelity')) {
          if (message.includes("Required services/items are not added. Please check stage 2 & 3 to add the required services/items.")) {
            if (this.request.url == `${environment.SALES_API}lead-items`) {
              this.snackbarService.openSnackbar(message, type);
            }
          }
          else if (message === 'Invalid username or password' || this.request.url.includes('enable') || message !== 'Record not found') {
            if (message.toLowerCase().includes('boundary request downloaded successfully')) {
              setTimeout(() => {
                this.snackbarService.openSnackbar(message, type);
              }, 4000)
            }
            else {
              if (message !== 'Invalid token') {
                this.snackbarService.openSnackbar(message, type);
              }
            }
          }
          else {
            setTimeout(() => {
              this.snackbarService.openSnackbar(message, type);
            }, 800);
          }
        }
      }
      if ((exceptionMessage && !message && statusCode == 500) || (!exceptionMessage && !message && statusCode == 500)) {
        this.snackbarService.openSnackbar("Some Server Exception Messages Occurs", type);
      }
      else if (statusCode == 400) {
        this.snackbarService.openSnackbar("Bad Request", type);
      }
      setTimeout(() => {
        this.rxjsService.setPopupLoaderProperty(false);
        this.rxjsService.setNotificationProperty(true);
      });
      return response;
    }
  }

  private GetNotificationProperty() {
    this.rxjsService.getNotificationProperty().subscribe((showNotification: boolean) => {
      this.showNotification = showNotification;
    });
  }

  private errorHandler(error: HttpEvent<any>): Observable<HttpEvent<any>> {
    if (error instanceof HttpErrorResponse) {
      this.rxjsService.setAuthenticationHeaderProperty(true);
      switch (error.status) {
        case 0:
          this.snackbarService.openSnackbar("Service is unavailable", ResponseMessageTypes.ERROR);
          break;
        case 500:
          this.snackbarService.openSnackbar(error.statusText, ResponseMessageTypes.ERROR);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setPopupLoaderProperty(false);
      this.rxjsService.setNotificationProperty(true);
      return throwError(error);
    }
  }

  isRefreshingToken = false;
  tokenSubject = new Subject<string>();
  handleUnauthorizedRequest(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (!this.isRefreshingToken) {
      if (this.userService.loggedInUserData?.userId) { // if user loggedin
        this.isRefreshingToken = true;
        // Reset to null so that the following requests wait until the token comes back from the refreshToken call
        this.tokenSubject.next(null);
        return this.getRefreshToken()
          .pipe(switchMap((response: any) => {
            if (response.resources && response.resources['accessToken'] && response.resources['refreshToken'] && response.resources['expiresIn']) {
              const newToken = response.resources['accessToken'];
              const decodedUserData = this.userService.getDecodedLoggedInData(response.resources['accessToken']);
              if (decodedUserData) {
                const redirectRouteUrl = "common-dashboard";
                const parsedUserData = JSON.parse(decodedUserData['userData']);
                const expiresTimeInFullDate = new Date(decodedUserData['exp'] * 1000);
                this.broadcastChannel.postMessage({
                  "encodedToken": response.resources['accessToken'],
                  "refreshToken": response.resources['refreshToken']
                });
                // Fetch the stored regionId from loca storage when refreshtoken is called
                parsedUserData.openScapePabxId = this.userService.openScapePabxId;
                this.store.dispatch(new Login({
                  user: parsedUserData, encodedToken: response.resources['accessToken'], expiresTimeInFullDate,
                  tokenExpirationSeconds: response.resources['expiresIn'], defaultRedirectRouteUrl: redirectRouteUrl,
                  refreshToken: response.resources['refreshToken']
                }));
              }
              // After gotten new token then retry previous request
              if (newToken) {
                this.tokenSubject.next(newToken);
                if ((req.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_PHONER) || req.url.includes(EventMgntModuleApiSuffixModels.STACK_MONITORING_DISPATCHER)) && req.method === RestfulMethods.POST) {
                  this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                  this.rxjsService.setGlobalLoaderProperty(true);
                }
                // return next.handle(this.userService.setTokenHeader(req));
                return next.handle(this.userService.setTokenHeader(req)).pipe(
                  map(response => this.successHandler(response)),
                  catchError(error => {
                    if (error.status == 401) {
                      return this.handleUnauthorizedRequest(req, next);
                    }
                    else {
                      return this.errorHandler(error)
                    }
                  })
                );
              }
            }
            // If no token then logout
            this.userService.onLogout();
            return throwError("Unauthorized..!!");
          }), catchError(error => {
            // If there is an exception calling 'refreshToken', bad news so logout.
            this.userService.onLogout();
            return throwError(error);
          }), finalize(() => {
            this.isRefreshingToken = false;
          })
          );
      } else {
        this.userService.onLogout();
      }
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      return this.tokenSubject
        .pipe(
          filter(token => token != null)
          , take(1)
          , switchMap(token => {
            return next.handle(this.userService.setTokenHeader(req));
          })
        );
    }
  }
  // get new refresh token
  getRefreshToken() {
    return this.crudService.create(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.REFRESH_TOKEN_V2, {
      userId: this.loggedInUserData?.userId,
      email: this.loggedInUserData?.email,
      refreshToken: this.refreshToken
    })
  }
}