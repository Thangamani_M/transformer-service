import { ComponentType } from '@angular/cdk/portal';
import { Injectable, TemplateRef, Type } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, PrimengMessageConfirmDialogComponent, PrimengStatusConfirmDialogComponent, ResponseMessageTypes } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimengDeleteConfirmDialogComponent } from '../components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { SnackbarService } from '../services/snackbar.service';
import { API_SUFFIX_MODELS, LoggedInUserModel } from '../utils';
interface IDynamicDeleteDialogData {
    deletableIds: string;
    modifiedUserId: string;
    moduleName: ModulesBasedApiSuffix,
    apiSuffixModel: typeof API_SUFFIX_MODELS,
    isDeleted?: boolean
}
interface IDynamicChangeStatusDialogData {
    ids: string;
    isActive: boolean;
    modifiedUserId: string;
    moduleName: ModulesBasedApiSuffix,
    apiSuffixModel: typeof API_SUFFIX_MODELS,
    customData?:object
}
interface IDynamicConfirmByMessageDialogData {
    message?: string;
    confirmationType?: DynamicConfirmByMessageConfirmationType;
}

export enum DynamicConfirmByMessageConfirmationType {
    WARNING = 'WARNING',
    DANGER = 'DANGER'
}
@Injectable({ providedIn: 'root' })
export class ReusablePrimeNGTableFeatureService {
    loggedInUserData: LoggedInUserModel;

    constructor(private store: Store<AppState>, private snackbarService: SnackbarService,
        private dialogService: DialogService, private matDialog: MatDialog) {
        this.combineLatestNgrxStoreData();
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData)]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    openDynamicDeleteDialog(selectedTabIndex = 0, checkboxSelectedRows, primengTableConfigProperties,
        selectedRow?: object, dynamicChangeStatusDialogConfiguration?: DynamicDialogConfig): DynamicDialogRef {
        let dataKey = primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].dataKey;
        if (!selectedRow && checkboxSelectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
            return;
        }
        else {
            var deletableIds;
            if (selectedRow) {
                deletableIds = selectedRow[dataKey];
            }
            else if (checkboxSelectedRows.length > 0) {
                deletableIds = [];
                checkboxSelectedRows.forEach((element: any) => {
                    deletableIds.push(element[dataKey]);
                });
            }
            let data: IDynamicDeleteDialogData = {
                deletableIds: selectedRow ? deletableIds : checkboxSelectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
                modifiedUserId: this.loggedInUserData.userId,
                moduleName: primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].moduleName,
                apiSuffixModel: primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].apiSuffixModel,
            }
            let dynamicDialogConfig: DynamicDialogConfig;
            if (dynamicChangeStatusDialogConfiguration) {
                dynamicChangeStatusDialogConfiguration.data = dynamicChangeStatusDialogConfiguration?.data?dynamicChangeStatusDialogConfiguration.data:data;

                dynamicDialogConfig = dynamicChangeStatusDialogConfiguration;
            }
            else {
                dynamicDialogConfig = {
                    showHeader: false,
                    baseZIndex: 10000,
                    width: '400px',

                    data,
                };
            }
            return this.dialogService.open(PrimengDeleteConfirmDialogComponent, dynamicDialogConfig);
        }
    }

    openDynamicDeleteDialogByParams(selectedTabIndex = 0, checkboxSelectedRows, primengTableConfigProperties,
        selectedRow?: object, dynamicChangeStatusDialogConfiguration?: DynamicDialogConfig): DynamicDialogRef {
        let dataKey = primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].dataKey;
        if (!selectedRow && checkboxSelectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
            return;
        }
        else {
            var deletableIds;
            if (selectedRow) {
                deletableIds = selectedRow[dataKey];
            }
            else if (checkboxSelectedRows.length > 0) {
                deletableIds = [];
                checkboxSelectedRows.forEach((element: any) => {
                    deletableIds.push(element[dataKey]);
                });
            }
            let data: IDynamicDeleteDialogData = {
                deletableIds: selectedRow ? deletableIds : checkboxSelectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
                modifiedUserId: this.loggedInUserData.userId,
                moduleName: primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].moduleName,
                apiSuffixModel: primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].apiSuffixModel,
                isDeleted: true
            }
            let dynamicDialogConfig: DynamicDialogConfig;
            if (dynamicChangeStatusDialogConfiguration) {
                dynamicChangeStatusDialogConfiguration.data = data;
                dynamicDialogConfig = dynamicChangeStatusDialogConfiguration;
            }
            else {
                dynamicDialogConfig = {
                    showHeader: false,
                    baseZIndex: 10000,
                    width: '400px',
                    data,
                };
            }
            return this.dialogService.open(PrimengDeleteConfirmDialogComponent, dynamicDialogConfig);
        }
    }

    openDynamicChangeStatusDialog(selectedTabIndex = 0, primengTableConfigProperties,
        selectedRow, dynamicChangeStatusDialogConfiguration?: DynamicDialogConfig,customData:any = {}): DynamicDialogRef {
        let data: IDynamicChangeStatusDialogData = {
            ids: selectedRow[primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].dataKey],
            modifiedUserId: this.loggedInUserData.userId,
            isActive: selectedRow.isActive,
            moduleName: primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].moduleName,
            apiSuffixModel: primengTableConfigProperties.tableComponentConfigs.tabsList[selectedTabIndex].apiSuffixModel,
            customData:customData
        }
        let dynamicChangeStatusDialogConfig: DynamicDialogConfig;
        if (dynamicChangeStatusDialogConfiguration) {
            dynamicChangeStatusDialogConfiguration.data = data;
            dynamicChangeStatusDialogConfig = dynamicChangeStatusDialogConfiguration;
        }
        else {
            dynamicChangeStatusDialogConfig = {
                showHeader: false,
                baseZIndex: 10000,
                width: '400px',
                data,
            };
        }
        return this.dialogService.open(PrimengStatusConfirmDialogComponent, dynamicChangeStatusDialogConfig);
    }

    openDynamicConfirmByMessageDialog(message: string, dynamicConfirmByMessageDialogConfiguration?: DynamicDialogConfig,
        confirmationType: DynamicConfirmByMessageConfirmationType = DynamicConfirmByMessageConfirmationType.WARNING): DynamicDialogRef {
        let dynamicConfirmByMessageDialogConfig: DynamicDialogConfig;
        let data: IDynamicConfirmByMessageDialogData = { message };
        data.confirmationType = confirmationType;
        if (dynamicConfirmByMessageDialogConfiguration) {
            dynamicConfirmByMessageDialogConfiguration.data = data;
            dynamicConfirmByMessageDialogConfig = dynamicConfirmByMessageDialogConfiguration;
        }
        else {
            dynamicConfirmByMessageDialogConfig = {
                showHeader: false,
                baseZIndex: 10000,
                width: '400px',
                data,
            };
        }
        return this.dialogService.open(PrimengMessageConfirmDialogComponent, dynamicConfirmByMessageDialogConfig);
    }

    openComponentDialog<T, R>(componentOrTemplateRef: ComponentType<T> | TemplateRef<T>,
        matDialogConfig?: MatDialogConfig): MatDialogRef<T, R> {
        let matDialogConfiguration: MatDialogConfig= {disableClose:false,width: '400px',data: {componentOrTemplateRef:componentOrTemplateRef}};
        if (matDialogConfig) {
            matDialogConfiguration = {...matDialogConfiguration,...matDialogConfig};
        }
        return this.matDialog.open(componentOrTemplateRef, matDialogConfiguration);
    }
}
