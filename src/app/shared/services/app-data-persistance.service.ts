import { Injectable } from '@angular/core';
import { LeadCreationUserDataModel, ServiceAgreementStepsParams, StepperParams } from '@modules/sales';
import { decryptData, encryptData } from '../utils/functions.utils';

const serviceAgreementParamsKey = 'serviceAgreementParamsKey';
const leadCreationStepperKey = 'leadCreationStepperKey';
const leadCreationUserDataKey = 'leadCreationUserDataKey';
const staticEagerLoadingDataKey = 'staticEagerLoadingDataKey';
const dynamicEagerLoadingDataKey = 'dynamicEagerLoadingDataKey';
const leadHeaderDataKey = 'leadHeaderDataKey';
const fromUrlOfLeadLifecycleKey = 'fromUrlOfLeadLifecycleKey';
const dealerCustomerCreationServiceAgreementDataKey = 'dealerCustomerCreationServiceAgreementDataKey';
const openedTabsCountKey="openedTabsCountKey";
@Injectable({ providedIn: 'root' })
export class AppDataService {
  set leadCreationUserData(appData: LeadCreationUserDataModel | null) {
    if (appData) {
      encryptData(appData, leadCreationUserDataKey);
    }
    else {
      localStorage.removeItem(leadCreationUserDataKey);
    }
  }

  get leadCreationUserData(): LeadCreationUserDataModel | null {
    if (localStorage.getItem(leadCreationUserDataKey)) {
      return decryptData(leadCreationUserDataKey);
    }
  }

  set leadCreationStepperParams(appData: StepperParams | null) {
    if (appData) {
      encryptData(appData, leadCreationStepperKey);
    }
    else {
      localStorage.removeItem(leadCreationStepperKey);
    }
  }

  get leadCreationStepperParams(): StepperParams | null {
    if (localStorage.getItem(leadCreationStepperKey)) {
      return decryptData(leadCreationStepperKey);
    }
  }

  set ServiceAgreementUserData(appData: ServiceAgreementStepsParams | null) {
    if (appData) {
      encryptData(appData, serviceAgreementParamsKey);
    }
    else {
      localStorage.removeItem(serviceAgreementParamsKey);
    }
  }

  get ServiceAgreementUserData(): ServiceAgreementStepsParams | null {
    if (localStorage.getItem(serviceAgreementParamsKey)) {
      return decryptData(serviceAgreementParamsKey);
    }
  }

  get staticEagerLoadingData() {
    if (localStorage.getItem(staticEagerLoadingDataKey)) {
      return decryptData(staticEagerLoadingDataKey);
    }
  }

  set staticEagerLoadingData(staticEagerLoadingData) {
    if (staticEagerLoadingData) {
      encryptData(staticEagerLoadingData, staticEagerLoadingDataKey);
    }
    else {
      localStorage.removeItem(staticEagerLoadingDataKey);
    }
  }

  get dynamicEagerLoadingData() {
    if (localStorage.getItem(dynamicEagerLoadingDataKey)) {
      return decryptData(dynamicEagerLoadingDataKey);
    }
  }

  set dynamicEagerLoadingData(dynamicEagerLoadingData) {
    if (dynamicEagerLoadingData) {
      encryptData(dynamicEagerLoadingData, dynamicEagerLoadingDataKey);
    }
    else {
      localStorage.removeItem(dynamicEagerLoadingDataKey);
    }
  }

  get leadHeaderData() {
    if (localStorage.getItem(leadHeaderDataKey)) {
      return decryptData(leadHeaderDataKey);
    }
  }

  set leadHeaderData(leadHeaderData) {
    if (leadHeaderData) {
      encryptData(leadHeaderData, leadHeaderDataKey);
    }
    else {
      localStorage.removeItem(leadHeaderDataKey);
    }
  }

  get fromUrlOfLeadLifecycle() {
    if (localStorage.getItem(fromUrlOfLeadLifecycleKey)) {
      return decryptData(fromUrlOfLeadLifecycleKey);
    }
  }

  set fromUrlOfLeadLifecycle(fromUrlOfLeadLifecycleData: string) {
    if (fromUrlOfLeadLifecycleData) {
      encryptData(fromUrlOfLeadLifecycleData, fromUrlOfLeadLifecycleKey);
    }
    else {
      localStorage.removeItem(fromUrlOfLeadLifecycleKey);
    }
  }

  get dealerCustomerCreationServiceAgreementData() {
    if (localStorage.getItem(dealerCustomerCreationServiceAgreementDataKey)) {
      return decryptData(dealerCustomerCreationServiceAgreementDataKey);
    }
  }

  set dealerCustomerCreationServiceAgreementData(data) {
    if (data) {
      encryptData(data, dealerCustomerCreationServiceAgreementDataKey);
    }
    else {
      localStorage.removeItem(dealerCustomerCreationServiceAgreementDataKey);
    }
  }

  set openedTabsCount(openedTabsCount:number){
    localStorage.setItem(openedTabsCountKey,openedTabsCount.toString());
  }

  get openedTabsCount(){
    return parseInt(localStorage.getItem(openedTabsCountKey));
  }
}

