import {ComponentCanDeactivate} from './component-can-deactivate';
import {NgForm} from "@angular/forms";

export abstract class FormCanDeactivate extends ComponentCanDeactivate{

 abstract get isItemChangeDetected():boolean;
 
 canDeactivate():boolean{
   return this.isItemChangeDetected;  
  }
}