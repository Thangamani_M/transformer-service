import { HostListener } from "@angular/core";
import { RxjsService } from "@app/shared";
export  class ComponentCanDeactivate {
    isFormChangeDetected: boolean;
    constructor( rxjsService?:RxjsService) {
        rxjsService.getFormChangesDetectionPropertyForPageReload().subscribe((isFormChangeDetected: boolean) => {
            this.isFormChangeDetected = isFormChangeDetected;
        })
    }

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification() {
        if (this.isFormChangeDetected) {
            return confirm("You have unsaved changes! If you leave, your changes will be lost.");
        }
    }
}