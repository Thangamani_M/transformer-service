import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { AppState } from "@app/reducers";
import {
  MenuCategoriesForPermissions,
  PERMISSION_RESTRICTION_ERROR,
  ResponseMessageTypes,
} from "@app/shared";
import {
  currentComponentPageBasedPermissionsSelector$,
  PageBasedPermissionsLoaded,
  sidebarDataSelector$,
} from "@app/shared/layouts";
import { OtherService } from "@app/shared/services/other-services.service";
import { SnackbarService } from "@app/shared/services/snackbar.service";
import { isLoggedIn } from "@modules/others";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { Observable } from "rxjs/internal/Observable";
import { map, take } from "rxjs/operators";
@Injectable({ providedIn: "root" })
export class AuthenticationGuard implements CanActivate {
  moduleBasedMenus = [];
  constructor(
    private router: Router,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private otherService: OtherService
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    // combine two long lived observables
    return combineLatest([
      this.store.select(isLoggedIn),
      this.store.select(sidebarDataSelector$),
    ]).pipe(
      map((combinedData) => {
        if (combinedData[0]) {
          this.filterCurrentModule(state, combinedData[1]?.["menuAccess"]);
          this.prepareComponentBasedRolesAndPermissions(state);
          // logic to identify whether the typed url has the access based on role
          let selectedModule: any = this.findSelectedModule(state);
          // if(!selectedModule&&state.url !== '/common-dashboard'&&this.router.navigated==false){
          //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR,ResponseMessageTypes.WARNING);
          //   this.router.navigateByUrl('/common-dashboard');
          //   return true;
          // }
          // end of logic to identify whether the typed url has the access based on role
          if (state.url === "/login") {
            // if (state.url === '/' || state.url === '/login') {
            let previousURL =
              this.otherService.previousUrl !== ""
                ? this.otherService.previousUrl
                : this.otherService.previousUrl;
            this.router.navigateByUrl(previousURL);
            return true;
          }
          return true;
        }
        this.router.navigateByUrl("/login");
        return false;
      })
    );
  }

  filterCurrentModule(state: RouterStateSnapshot, menuAccess: []) {
    if (!menuAccess) return;
    const extractModuleNameFromRoute = state.url.split("/")[1];
    const _extractModuleNameFromRoute = state.url.split("/")[2];
    let conditionBasedModuleName =
      extractModuleNameFromRoute === "inventory" ? "Inventory Management"
        : extractModuleNameFromRoute === "customer"
        ? "Customer Management"
        : extractModuleNameFromRoute === "user"
        ? "IT Management"
        : extractModuleNameFromRoute === "sales"
        ? "Sales"
        : extractModuleNameFromRoute === "event-management"
        ? "Event Management"
        : extractModuleNameFromRoute === "technical-management"
        ? "Technical Management"
        : extractModuleNameFromRoute === "billing"
        ? "Billing"
        : extractModuleNameFromRoute == "configuration"
        ? "Configuration"
        : extractModuleNameFromRoute == "lss"
        ? "LSS"
        : extractModuleNameFromRoute == "my-tasks"
        ? "My Task"
        : extractModuleNameFromRoute == "calls"
        ? "Calls"
        : extractModuleNameFromRoute == "boundary-management"
        ? "Boundary Management"
        : extractModuleNameFromRoute == "signal-management"
        ? "Signal Management"
        : extractModuleNameFromRoute == "chat-room"
        ? "Chat Room"
        : extractModuleNameFromRoute == "data-maintenance"
        ? "Data Maintenance"
        : extractModuleNameFromRoute == "dealer"
        ? "Dealer Management"
        : extractModuleNameFromRoute == "collection"
        ? "Credit Control"
        : extractModuleNameFromRoute == "panic-app"
        ? "Panic App"
        : extractModuleNameFromRoute == "reports"
        ? "Reports"
        : extractModuleNameFromRoute == "faq"
        ? "FAQ"
        : "";
    extractModuleNameFromRoute == "signal-translator"
      ? "Signal Translator"
      :extractModuleNameFromRoute == "campaign-management"
      ? "Campaign Management"
      : extractModuleNameFromRoute == "reports"
      ? "Reports"
      : extractModuleNameFromRoute == "risk-watch"
      ? "Risk Watch"
      : extractModuleNameFromRoute == "panic-app"
      ? "Panic App"
      : "";
    // TEMP Condition for non related menus path

    if (
      _extractModuleNameFromRoute == "transaction-configuration" ||
      _extractModuleNameFromRoute == "pricing-run-date-configuration" ||
      _extractModuleNameFromRoute == "price-increase-configuration" ||
      _extractModuleNameFromRoute == "statement-configuration" ||
      _extractModuleNameFromRoute == "approval-configuration" ||
      _extractModuleNameFromRoute == "leave-user" ||
      _extractModuleNameFromRoute == "calendar-event" ||
      _extractModuleNameFromRoute == "bdi-code-config" ||
      _extractModuleNameFromRoute == "fual-price-increase-letter-config" ||
      _extractModuleNameFromRoute == "requisition-initiator-configuration" ||
      _extractModuleNameFromRoute == "it-management"
    ) {
      conditionBasedModuleName = "Configuration";
    }
    if (
      _extractModuleNameFromRoute == "statement-fee-discount" ||
      _extractModuleNameFromRoute == "recurring-billing" ||
      _extractModuleNameFromRoute == "credit-card-maintenance" ||
      _extractModuleNameFromRoute == "statement-fee-discount" ||
      _extractModuleNameFromRoute == "license-bill-mode"
    ) {
      conditionBasedModuleName = "Billing";
    }
    if (extractModuleNameFromRoute == "out-of-office") {
      conditionBasedModuleName = "Configuration";
    }

    if (extractModuleNameFromRoute == "it-management") {
      conditionBasedModuleName = "IT Management";
    }
    if (_extractModuleNameFromRoute == "useful-numbers-contact") {
      conditionBasedModuleName = "Useful Number";
    }
    if (
      _extractModuleNameFromRoute == "market-type-config" ||
      _extractModuleNameFromRoute == "customer-app-banner-config"
    ) {
      conditionBasedModuleName = "Marketing";
    }
    if (_extractModuleNameFromRoute == "office-address") {
      conditionBasedModuleName = "Office Address";
    }
    if (_extractModuleNameFromRoute == "dealer-stock-selling-price") {
      conditionBasedModuleName = "Inventory Management";
    }
    if (
      _extractModuleNameFromRoute == "dealer-worklist" ||
      _extractModuleNameFromRoute == "credit-control-task-list"
    ) {
      conditionBasedModuleName = "My Task";
    }
    if (
      _extractModuleNameFromRoute == "stack-operator" ||
      _extractModuleNameFromRoute == "stack-dispatcher" ||
      _extractModuleNameFromRoute == "supervisor-dashboard" ||
      _extractModuleNameFromRoute == "dispatcher-map-view"
    ) {
      conditionBasedModuleName = "Dashboard";
    }
    if (extractModuleNameFromRoute == "signal-translator") {
      conditionBasedModuleName = "Signal Translator";
    }
    if (extractModuleNameFromRoute == "risk-watch") {
      conditionBasedModuleName = "Risk Watch";
    }
    if (extractModuleNameFromRoute == "campaign-management") {
      conditionBasedModuleName = "Campaign Management";
    }
    if (_extractModuleNameFromRoute == "find-signal") {
      conditionBasedModuleName = "Find Signal";
    }
    if (extractModuleNameFromRoute == "radio-removal") {
      conditionBasedModuleName = "Radio Removal";
    }
    if (_extractModuleNameFromRoute == "audit-log-configuration" || _extractModuleNameFromRoute == "audit-logs" ) {
      conditionBasedModuleName = "Audit Log";
    }


    switch (true) {
      case extractModuleNameFromRoute == "common-dashboard":
        break;
      default:
        this.moduleBasedMenus = menuAccess
          .filter((p) => p["menuName"] === conditionBasedModuleName)
          .map((mA) => mA["subMenu"])[0];
        break;
    }
    console.log("moduleBasedMenus",this.moduleBasedMenus)
    console.log("conditionBasedModuleName",conditionBasedModuleName)
  }

  checkAccessPermissionForTheRequestedRoute(
    permittedModules: Array<object>,
    state: RouterStateSnapshot
  ): object {
    const extractModuleNameFromRoute = state.url.split("/")[1];
    const conditionBasedModuleName =
      extractModuleNameFromRoute === "inventory"
        ? "Inventory Management"
        : extractModuleNameFromRoute === "customer"
        ? "Customer Management"
        : extractModuleNameFromRoute === "user"
        ? "IT Management"
        : extractModuleNameFromRoute === "sales"
        ? "Sales"
        : extractModuleNameFromRoute === "event-management"
        ? "Event Management"
        : extractModuleNameFromRoute === "technical-management"
        ? "Technical Management"
        : extractModuleNameFromRoute === "billing"
        ? "Billing"
        : extractModuleNameFromRoute == "configuration"
        ? "Configuration"
        : extractModuleNameFromRoute == "lss"
        ? "LSS"
        : extractModuleNameFromRoute == "my-tasks"
        ? "My Task"
        : extractModuleNameFromRoute == "calls"
        ? "Calls"
        : extractModuleNameFromRoute == "boundary-management"
        ? "Boundary Management"
        : extractModuleNameFromRoute == "signal-management"
        ? "Signal Management"
        : extractModuleNameFromRoute == "chat-room"
        ? "Chat Room"
        : extractModuleNameFromRoute == "dealer"
        ? "Dealer Management"
        : extractModuleNameFromRoute == "collection"
        ? "Credit Control"
        : extractModuleNameFromRoute == "reports"
        ? "Reports"
        : "";
    const filteredObject = permittedModules.find(
      (p) => p["moduleName"] === conditionBasedModuleName
    );
    switch (true) {
      case extractModuleNameFromRoute == "common-dashboard":
        return {};
      case !filteredObject:
        return null;
      case filteredObject["moduleName"] == "Internal Settings":
        return {};
      default:
        let returnObject = filteredObject["navigations"].find(
          (navigationObj) => {
            if (
              navigationObj.navigations.length == 0 &&
              navigationObj.navigationUrl == state.url
            ) {
              return navigationObj;
            } else if (navigationObj.navigations.length > 0) {
              return navigationObj.navigations.some((nestedNavigationObj) => {
                if (
                  nestedNavigationObj.navigations.length == 0 &&
                  nestedNavigationObj.navigationUrl == state.url
                ) {
                  return nestedNavigationObj;
                } else if (nestedNavigationObj.navigations.length > 0) {
                  return nestedNavigationObj.navigations.some(
                    (secondNestedNavigationObj) =>
                      secondNestedNavigationObj.navigationUrl === state.url
                  );
                } else if (
                  nestedNavigationObj.navigations.length == 0 &&
                  nestedNavigationObj.navigationUrl !== state.url
                ) {
                  return {};
                }
              });
            } else if (
              navigationObj.navigations.length == 0 &&
              navigationObj.navigationUrl !== state.url
            ) {
              return {};
            }
          }
        );
        return returnObject;
    }
  }

  async prepareComponentBasedRolesAndPermissions(state: RouterStateSnapshot) {
    let selectedModule: any = this.findSelectedModule(state);
    if (!selectedModule) return;
    var mName = selectedModule["menuName"]
      ? selectedModule["menuName"]
      : selectedModule["moduleName"];
    const response = await this.store
      .pipe(select(currentComponentPageBasedPermissionsSelector$), take(1))
      .toPromise();
    if (!response[mName] && mName) {
      let _data = {};
      _data[mName] = [];
      _data = _data = { ...response, ..._data };
      let currentComponentPageBasedPermissions,
        filteredCurrentComponentPageBasedPermissionsObj;
      filteredCurrentComponentPageBasedPermissionsObj =
        this.prepareCurrentComponentBasedPermissions(
          this.moduleBasedMenus,
          selectedModule.menuParentId
        );
      currentComponentPageBasedPermissions =
        filteredCurrentComponentPageBasedPermissionsObj?.["subMenu"]
          .filter((f) => f["menuId"] == selectedModule.menuId)
          .map((res) => res.subMenu);
      if (!filteredCurrentComponentPageBasedPermissionsObj) {
        currentComponentPageBasedPermissions = this.moduleBasedMenus
          .filter((pM) => pM["menuId"] == selectedModule.menuId)
          .map((res) => res.subMenu);
      } else if (
        filteredCurrentComponentPageBasedPermissionsObj.subMenu.length > 0
      ) {
        if (
          currentComponentPageBasedPermissions[0].find(
            (sM) => sM["menuCategoryId"] == MenuCategoriesForPermissions.MENU
          )
        ) {
          debugger;
        }
      }

      _data[`${mName}`] = currentComponentPageBasedPermissions[0];
      this.store.dispatch(
        new PageBasedPermissionsLoaded({
          moduleBasedComponentPermissions: this.moduleBasedMenus,
          currentComponentPageBasedPermissions: _data,
        })
      );
    }
  }

  findSelectedModule(state: RouterStateSnapshot) {
    if (this.moduleBasedMenus && this.moduleBasedMenus.length > 0) {
      return this.findItemNested(this.moduleBasedMenus, state.url);
    }
  }

  findItemNested = (arr, url, nestingKey = "subMenu") =>
    arr.reduce((a, item) => {
      if (a) return a;
      if (item.url === url) return item;
      else if (
        (url?.indexOf("/customer/manage-customers") != -1 || url?.indexOf("/customer/upsell-quote") != -1) &&
        item?.url == "/customer/manage-customers" &&
        url?.indexOf("/customer/manage-customers/ticket") == -1
      ) {
        return item;
      } else if (
        url?.indexOf("/customer/manage-customers/ticket") != -1 &&
        item?.url == "/customer/all-tickets"
      ) {
        return item;
      } else if (
        url?.indexOf("/inventory/requisitions/stock-orders") != -1 &&
        item?.url == "/inventory/requisitions/stock-orders"
      ) {
        return item;
      } else if (
        url?.indexOf("/inventory/inter-branch/inter-branch-transfer") != -1 &&
        item?.url == "/inventory/inter-branch/inter-branch-transfer"
      ) {
        return item;
      } else if (
        url?.indexOf("/technical-management/tech-stock-order/approval") != -1 &&
        item?.url == "/technical-management/tech-stock-order/approval"
      ) {
        return item;
      } else if (
        url?.indexOf("/inventory/suppliers/return") != -1 &&
        item?.url == "/inventory/suppliers/return"
      ) {
        return item;
      }else if (
        url?.indexOf("/inventory/stock-maintenance") != -1 &&
        item?.url == "/inventory/stock-maintenance"
      ) {
        return item;
      } else if (
        url?.indexOf("/out-of-office/request-approval") != -1 &&
        item?.url == "/out-of-office/request-approval/list"
      ) {
        return item;
      }
      // if (item.url === url || url.includes(item.url)) {
      //   return item;
      // }
      if (item[nestingKey]) return this.findItemNested(item[nestingKey], url);
    }, null);

  // nested array using recursion
  prepareCurrentComponentBasedPermissions(
    parentBasedMenus,
    menuParentId,
    nestingKey = "subMenu"
  ): [] {
    return parentBasedMenus.reduce((parentBasedMenu, item) => {
      if (parentBasedMenu) {
        return parentBasedMenu;
      }
      if (item.menuId === menuParentId) return item;
      if (item[nestingKey]) {
        return this.prepareCurrentComponentBasedPermissions(
          item[nestingKey],
          menuParentId
        );
      }
    }, null);
  }
}
