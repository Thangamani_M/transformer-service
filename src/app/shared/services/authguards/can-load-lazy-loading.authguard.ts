import { CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { isLoggedIn } from '@modules/others';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CanLoadAuthenticationGuard implements CanLoad {
    constructor(private router: Router, private store: Store<AppState>) { }
    canLoad(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
        return this.store
            .pipe(
                select(isLoggedIn),
                tap(loggedIn => {
                    if (!loggedIn) {
                        this.router.navigateByUrl('/login');
                    }
                })
            );
    }
}