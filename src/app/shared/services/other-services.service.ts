import { DecimalPipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { FormArray } from '@angular/forms';

const titleKey = 'titleKey';
const previousUrlKey = 'previousUrlKey';
const currentUrlKey = "currentUrlKey";
@Injectable({ providedIn: 'root' })
export class OtherService {

  constructor(private decimalPipe: DecimalPipe,) { }

  set title(titleName: string | null) {
    if (titleName) {
      sessionStorage.setItem('title', JSON.stringify(titleName));
    }
    else {
      sessionStorage.removeItem('title');
    }
  }

  get title() {
    return JSON.parse(sessionStorage.getItem('title'));
  }

  set previousUrl(prevUrl: string | null) {
    if (prevUrl) {
      sessionStorage.setItem(previousUrlKey, JSON.stringify(prevUrl));
    }
    else {
      sessionStorage.removeItem(previousUrlKey);
    }
  }

  get previousUrl() {
    return JSON.parse(sessionStorage.getItem(previousUrlKey));
  }

  set currentUrl(currentUrl: string | null) {
    if (currentUrl) {
      sessionStorage.setItem(currentUrlKey, JSON.stringify(currentUrl));
    }
    else {
      sessionStorage.removeItem(currentUrlKey);
    }
  }

  get currentUrl() {
    return JSON.parse(sessionStorage.getItem(currentUrlKey));
  }

  //don't remove this currency format
  transformDecimal(num: number | any) {
    if(typeof num == 'string' && num?.toString()?.indexOf('R') !== -1) {
      return num;
    }
    if (num?.toString()?.indexOf('.') !== -1 && num) {
      return 'R '+this.decimalPipe.transform(num, '.2-2', 'en-GB').toString().replace(/,/g, " ");
    }
    else if(num?.toString() && num) {
      return 'R '+this.decimalPipe.transform(num, '.2-2', 'en-GB').toString().replace(/,/g, " ");
    } else if(num == 0) {
      return `R ${num}.00`;
    } else {
      return num;
    }
  }

  transformCurrToNum(num: any) {
    if(typeof num == 'number') {
      return num;
    }
    if(num != null && num != '') {
      const number =  num?.indexOf('R') != -1 ? num?.replace('R ', '') : num ? num : '';
      if(number) {
        return +(number?.replace(/ /g, ""));
      }
    } else if(num?.toString()) {
      return num;
    } else if(num == 0) {
      return num;
    }
    return num;
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
    return formArray;
  };

}
