import {createSelector, createFeatureSelector} from '@ngrx/store';
import { LayoutState, PagePermissionsState } from './layout.reducer';

export const selectLayoutState = createFeatureSelector<LayoutState>("layout");
export const selectPagePermissionsState = createFeatureSelector<PagePermissionsState>("pageBasedPermissions");

export const sidebarDataSelector$ = createSelector(
    selectLayoutState,
  layoutState => layoutState.sidebarData
);

export const moduleBasedComponentPermissionsSelector$ = createSelector(
  selectPagePermissionsState,
layoutState => layoutState.moduleBasedComponentPermissions
);

export const currentComponentPageBasedPermissionsSelector$ = createSelector(
  selectPagePermissionsState,
layoutState => layoutState.currentComponentPageBasedPermissions
);