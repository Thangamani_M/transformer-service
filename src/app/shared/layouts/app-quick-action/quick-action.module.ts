import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { LayoutModule, QuickActionComponent } from "@app/shared";
import { MaterialModule } from "@app/shared/material.module";
import { SharedModule } from "@app/shared/shared.module";
import { CustomerTicketAddEditModule } from "./../../../modules//customer/components/customer/customer-management/customer-ticket/customer-ticket-add-edit.module";
import { LoadRequestModelModule } from "@modules/customer/components/customer/customer-management/load-request-model/load-request-model.module";
import { QuickAddServiceCallDialogModule } from "@modules/customer/components/customer/customer-management/quick-add-service-call-dialog/quick-add-service-call-dialog.module";
import { MarkettingPreferenceDialogComponent } from "./marketting-preference-dialog/marketting-preference-dialog.component";

@NgModule({
    declarations: [QuickActionComponent,MarkettingPreferenceDialogComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        QuickAddServiceCallDialogModule,
        LoadRequestModelModule,
        CustomerTicketAddEditModule
    ],
    providers: [],
    exports: [QuickActionComponent],
    entryComponents: [MarkettingPreferenceDialogComponent]
  })
  export class QuickActionModule { }
