import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared/utils';
import { CustomerTicketAddEditComponent } from '@modules/customer/components/customer/customer-management/customer-ticket/customer-ticket-add-edit.component';
import { LoadRequestModelComponent } from '@modules/customer/components/customer/customer-management/load-request-model/load-request-model.component';
import { VisibilityPatrolInfoComponent } from '@modules/customer/components/customer/customer-management/patrol-history/visibility-patrol-info/visibility-patrol-info.component';
import { QuickAddServiceCallDialogComponent } from '@modules/customer/components/customer/customer-management/quick-add-service-call-dialog/quick-add-service-call-dialog.component';
import { CustomerVerificationRedirect, CustomerVerificationType } from '@modules/customer/shared/enum/customer-verification.enum';
import { CustomerModuleApiSuffixModels, CUSTOMER_COMPONENT } from '@modules/customer/shared/utils/customer-module.enums';
import { loggedInUserData } from '@modules/others';
import { CustomerContactAddEditModalComponent } from '@modules/sales/components/lead-notes/customer-contact-edit-modal.component';
import { CustomerUpgradeModalComponent } from '@modules/sales/components/lead-notes/customer-upgrade-modal.component';
import { ScheduleCallbackAddEditModalComponent } from '@modules/sales/components/lead-notes/schedule-callback-add-edit-modal.component';
import { ScheduleCallBackAddEditModel } from '@modules/sales/models/lead-sms-notification.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { currentComponentPageBasedPermissionsSelector$, moduleBasedComponentPermissionsSelector$ } from '../layout.selectors';
import { MarkettingPreferenceDialogComponent } from './marketting-preference-dialog/marketting-preference-dialog.component';

declare var $: any
@Component({
  selector: 'app-quick-action',
  templateUrl: './quick-action.component.html',
  styleUrls: ['./app-quick-action.scss']
})
export class QuickActionComponent implements OnInit {
  LeadId;
  customerId;
  moduleName: string = null
  quickAddServiceCallDialog: boolean = false
  customerData;
  loggedInUserData;
  siteAddressId;
  counterSaleData: string;
  customerAddressId;
  addressId: string;
  uniqueCallId;
  customerProfile: any;
  feature: string;
  featureIndex: number;
  @Output() redirectToCustomerView = new EventEmitter<any>();
  customerQuickAction: any = []
  allPermissions: any
  menuAccess: any
  constructor(private dialog: MatDialog, private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>, public dialogService: DialogService,
    private snackbarService: SnackbarService) {
    this.LeadId = this.activatedRoute.snapshot.queryParams.leadId;
    this.activatedRoute.queryParams.subscribe(
      params => {
        this.addressId = params['addressId']
        this.feature = params['feature_name']
        this.featureIndex = params['featureIndex']
      })
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreDataOne()
    this.combineLatestNgrxStoreData();
    this.rxjsService.getQuickActionComponent()
      .subscribe(moduleName => {
        this.moduleName = moduleName
      });
    this.rxjsService.getCounterSaleProperty().subscribe((data: string) => {
      this.counterSaleData = data;
    });
    this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerData = data;  // Customer Id Only
      });
    this.rxjsService.getCustomerProfile()
      .subscribe(data => {
        this.customerProfile = data;  // Customer profile Only

      });
  }


  combineLatestNgrxStoreDataOne() {
    let key = CUSTOMER_COMPONENT.CUSTOMER
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
      this.store.select(moduleBasedComponentPermissionsSelector$)]
    ).subscribe((response: any) => {
      let permission = response[0]?.Customers
      this.allPermissions = response[0]
      this.menuAccess = response[1]
      if (permission) {
        if (key == CUSTOMER_COMPONENT.CUSTOMER) {
          this.customerQuickAction = permission.find(item => item.menuName == "Quick Action");
        }
      }
    });
  }


  onPhoneIn(val: String = 'aa') {
    let isAccessDeined;
    let tab = CustomerVerificationType.AA_PHONE_IN;
    if (val == 'create') {
      isAccessDeined = this.getPermissionByActionIconType("Create Phone In");
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      tab = CustomerVerificationType.CREATE_PHONE_IN;
      this.onAfterCreatePhoneIn();
    } else if (val == 'aa') {
      isAccessDeined = this.getPermissionByActionIconType("AA Phone In");
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.onAfterAAPhoneIn();
    }
    // this.onCustomerVerficationCheck(tab);
  }

  onAfterAAPhoneIn() {
    if (this.customerProfile == 'Hub Profile') {
      let queryParams = { customerId: this.customerData?.customerId, addressId: this.addressId, };
      if (this.feature && this.featureIndex) {
        queryParams['feature_name'] = this.feature;
        queryParams['featureIndex'] = this.featureIndex;
      }
      this.router.navigate(['/event-management/aa-phone-in'], { queryParams: queryParams })
    }
  }

  onAfterCreatePhoneIn() {
    if (this.customerProfile === 'Repeater Site' || this.customerProfile === 'Standard Customer' || this.customerProfile === 'ExecuGuard'
      || this.customerProfile === 'Test Profile' || this.customerProfile === 'Casual Guarding' || this.customerProfile === 'Adt Site' || this.customerProfile === 'NKA Customers') {
      let queryParams = { customerId: this.customerData?.customerId, addressId: this.addressId, tab: 0, childtab: 0 };
      if (this.feature && this.featureIndex) {
        queryParams['feature_name'] = this.feature;
        queryParams['featureIndex'] = this.featureIndex;
      }
      this.router.navigate(['/event-management/create-phone-in'], { queryParams: queryParams })
    }

    if (this.customerProfile === 'Hub Profile' || this.customerProfile === 'No Client' || this.customerProfile === 'Whatsapp Profile'
      || this.customerProfile === 'Community Profile') {
      let queryParams = { customerId: this.customerData?.customerId, addressId: this.addressId, tab: 1, childtab: 0 };
      if (this.feature && this.featureIndex) {
        queryParams['feature_name'] = this.feature;
        queryParams['featureIndex'] = this.featureIndex;
      }
      this.router.navigate(['/event-management/create-phone-in'], { queryParams: queryParams })
    }
    // if(this.customerProfile === 'Standard Customer'){
    //   let queryParams = { customerId: this.customerData?.customerId, addressId: this.addressId, tab: 2, childtab: 0};
    // if(this.feature && this.featureIndex) {
    //   queryParams['feature_name'] = this.feature;
    //   queryParams['featureIndex'] = this.featureIndex;
    // }
    //   this.router.navigate(['/event-management/create-phone-in'], { queryParams: queryParams })
    //  }
    if (this.customerProfile === 'CCTV-Camera Profile') {
      let queryParams = { customerId: this.customerData?.customerId, addressId: this.addressId, tab: 3, childtab: 0 };
      if (this.feature && this.featureIndex) {
        queryParams['feature_name'] = this.feature;
        queryParams['featureIndex'] = this.featureIndex;
      }
      this.router.navigate(['/event-management/create-phone-in'], { queryParams: queryParams })
    }
  }

  toggleQuickAction() {
    this.rxjsService.setGlobalLoaderProperty(false);
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
  }

  openCounterSale() {
    let queryParams = { id: this.customerData.customerId, };
    if (this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }
    this.router.navigate(['/customer/counter-sales/item-info'], { queryParams: queryParams });
  }

  onOptionselected(type: string) {
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    let modalComponent;
    let data: ScheduleCallBackAddEditModel = {};
    let isPrimeNg = false;
    let isAccessDeined: any
    switch (type) {
      case "load request":
        isAccessDeined = this.getPermissionByActionIconType("Load Request");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        modalComponent = LoadRequestModelComponent;
        break;
      case "create ticket":

      case "create new lead":
        isAccessDeined = this.getPermissionByActionIconType("Create New Lead");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let queryParamsCreateNewLead = { customerId: this.customerData.customerId, setindex: 1, addressId: this.addressId, fromModule: 'Customer' };
        if (this.feature && this.featureIndex) {
          queryParamsCreateNewLead['feature_name'] = this.feature;
          queryParamsCreateNewLead['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(['my-tasks/my-sales/my-leads/lead-info/add-edit'], { queryParams: queryParamsCreateNewLead })
        break;
      case "schedule callback":
        isAccessDeined = this.getPermissionByActionIconType("Schedule Callback View");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        modalComponent = ScheduleCallbackAddEditModalComponent;
        data = new ScheduleCallBackAddEditModel(this.customerData);
        data.isCustomer = true;
        data.contactNumberCountryCode = this.customerData.contactNumber.slice(0, 3);
        data.contactNumber = this.customerData.contactNumber.slice(4);
        break;
      case "risk watch guard service":
        isAccessDeined = this.getPermissionByActionIconType("Risk Watch Service");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onCustomerVerficationCheck(CustomerVerificationType.RISK_WATCH_SERVICE);
        break;
      case "visibility request":
        isAccessDeined = this.getPermissionByActionIconType("Visibility Request");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        modalComponent = VisibilityPatrolInfoComponent
        data = { customerId: this.customerId }
        isPrimeNg = true;
        break;
      case "view notes":
        isAccessDeined = this.getPermissionByActionIconType("View Notes");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let queryParamsViewNotes = { customerId: this.customerData.customerId, customerAddressId: this.addressId };
        if (this.feature && this.featureIndex) {
          queryParamsViewNotes['feature_name'] = this.feature;
          queryParamsViewNotes['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(['customer/manage-customers/customer-view-notes'], { queryParams: queryParamsViewNotes })
        break;
      case "update address":
        this.onAfterCustomerVerificationCheck(CustomerVerificationType.UPDATE_SITE_ADDRESS)
        break;
    }

    if (!isPrimeNg && type !== 'create new lead' && type !== 'risk watch guard service' && type != 'update address' && type != 'view notes') {
      const dialogReff = this.dialog.open(modalComponent, { data, width: '750px', disableClose: true });
      dialogReff.afterClosed().subscribe(result => {
        if (type == "load request") {
          const monitoringTab = result == '1' || result == '2' ? 0 : 8;
          this.redirectToCustomerView.emit({ navigateTab: 4, monitoringTab: monitoringTab })
        }
      });
    } else if (isPrimeNg) {
      const ref = this.dialogService.open(modalComponent, {

        showHeader: false,
        // baseZIndex: 10000,
        baseZIndex: 1000,
        width: "560px",
        data: { ...data, header: `Visibility Request`, },
      });
      ref.onClose.subscribe((resp) => {
        if (type == "load request") {
          const monitoringTab = resp == '1' || resp == '2' ? 0 : 8;
          this.redirectToCustomerView.emit({ navigateTab: 4, monitoringTab: monitoringTab })
        }
      })
    }
  }

  onAfterRiskWatch() {
    window.open(
      `billing/risk-watch-guard-service/add-service?customerId=${this.customerData.customerId}&addressId=${this.addressId}`);
  }

  onAfterUpdateAddress() {
    let queryParams = { customerId: this.customerData.customerId, id: this.addressId, fromComponent: 'Customer' };
    if (this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }
    this.router.navigate([`data-maintenance/customer-address/add-edit`], { queryParams });
  }

  openCustomerPreference() {
    let isAccessDeined = this.getPermissionByActionIconType("Customer Preference");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    this.redirectToCustomerView.emit({ navigateTab: 4, monitoringTab: 4 });
  }

  updateCustomer() {
    let isAccessDeined = this.getPermissionByActionIconType("Update Customer Contact Details");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    this.onAfterUpdateCustomer(CustomerVerificationType.UPDATE_CUSTOMER_CONTACT_DETAIL);
  }

  onAfterUpdateCustomer(tab) {
    let permissions = this.customerQuickAction['subMenu'].find(fSC => fSC.menuName == "Update Customer Contact Details");

    const dialogReff = this.dialog.open(CustomerContactAddEditModalComponent, {
      width: '700px', data:
      {
        leadId: this.LeadId, addressId: this.addressId, uniqueCallId: this.uniqueCallId, tab: tab,
        permissions: permissions,
        api: CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
        return;
      }
      this.rxjsService.setContactUpdateDetails(true);
    });
  }

  onCustomerVerficationCheck(tab: number) {
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.onAfterCustomerVerificationCheck(tab);
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.customerData?.customerId, siteAddressId: this.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onAfterCustomerVerificationCheck(tab);
          } else {
            $('.quick-action-sidebar').addClass('hide-quick-action-menu');
            let queryParams: any = { customerId: this.customerData?.customerId, siteAddressId: this.addressId, tab: tab };
            if (CustomerVerificationType.RISK_WATCH_SERVICE == tab) {
              queryParams.redirectUrl = CustomerVerificationRedirect.RISK_WATCH_SERVICE
            }
            if (this.feature && this.featureIndex) {
              queryParams['feature_name'] = this.feature;
              queryParams['featureIndex'] = this.featureIndex;
            }
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: queryParams })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  onAfterCustomerVerificationCheck(tab: number) {
    this.toggleQuickAction();
    switch (tab) {
      case 12:
        this.onAfterAAPhoneIn();
        break;
      case 13:
        this.onAfterCreatePhoneIn();
        break;
      case 14:
        this.onAfterRiskWatch();
        break;
      case 15:
        this.onAfterUpdateAddress();
        break;
    }
  }

  relocationRedirect() {
    let isAccessDeined = this.getPermissionByActionIconType("Relocation");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerId = data.customerId;
      })
    this.rxjsService.setRelocation(true);
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      feature_name: this.feature,
      featureIndex: this.featureIndex,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  upgradeRedirect() {
    let isAccessDeined = this.getPermissionByActionIconType("Upgrade");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    let data = this.customerData;
    data['addressId'] = this.addressId;
    data.fromComponent = 'customer detail';
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    this.rxjsService.setDialogOpenProperty(true);
    this.dialog.open(CustomerUpgradeModalComponent, { width: '850px', disableClose: true, data });
  }

  openStandbyRoster() {
    let isAccessDeined = this.getPermissionByActionIconType("Standby Roster");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    let queryParams = { customerId: this.customerData.customerId, addressId: this.addressId, quickActionType: 'customer' };
    if (this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }
    this.router.navigate(['/customer/stand-by-roster'], { queryParams: queryParams })
  }

  openQuickAddServiceCall() {
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    this.openAfterQuickAddServiceCall();
  }

  openAfterQuickAddServiceCall() {
    let isAccessDeined = this.getPermissionByActionIconType("Quick Add Service Call");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerId = data.customerId;
      })
    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data;
      })
    if (!this.customerId && this.customerAddressId) {
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.QUICK_ADD_SERVICE,
      undefined, null, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.customerId, AddressId: this.customerAddressId, uniqueCallId: this.uniqueCallId }))
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        const dialogRef = this.dialog.open(QuickAddServiceCallDialogComponent, {
          width: '750px',
          data: {
            customerId: this.customerId,
            customerAddressId: this.customerAddressId,
            isProceed: response.resources?.debtorId ? true : false,
            warningMsg: response.resources?.warningMessage ? response.resources?.warningMessage : response.resources?.debtorId ? null : 'No Debtor created for the customer. Unable to create a service call',
            edit: true,
            userId: this.loggedInUserData?.userId,
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          this.rxjsService.setDialogOpenProperty(false);
        });
      });
  }

  openCustomerVerification() {
    let queryParams = { customerId: this.customerData.customerId, siteAddressId: this.addressId };
    if (this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }
    this.router.navigate(['customer/manage-customers/customer-verification'], { queryParams: queryParams });
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
  }
  getPermissionByActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.customerQuickAction['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  openMarkettingPreference() {
    let isAccessDeined = this.getPermissionByActionIconType("Marketting Preference");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    const ref = this.dialogService.open(MarkettingPreferenceDialogComponent, {
      showHeader: false,
      baseZIndex: 1000,
      width: "67vw",
      data: { customerId: this.customerData?.customerId, addressId: this.addressId },
    });
    ref.onClose.subscribe((resp) => {

    })
  }

  openCreateAticket() {
    let isAccessDeined = this.getPermissionByActionIconType("Create Ticket");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    const ref = this.dialogService.open(CustomerTicketAddEditComponent, {
      showHeader: false,
      baseZIndex: 1000,
      width: "45vw",
      data: { custId: this.customerData?.customerId, addressId: this.addressId },
    });
    ref.onClose.subscribe((resp) => {

    })
  }

}
