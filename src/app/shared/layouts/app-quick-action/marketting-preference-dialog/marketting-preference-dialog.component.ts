import { Component, Inject, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { addTrueOrFalseDefaultFormControlValue, countryCodes, CrudService, defaultCountryCode, disableFormControls, disableWithDefaultFormControlValues, enableFormControls, enableWithDefaultFormControlValues, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, RxjsService, setRequiredValidator } from '@app/shared';
import { select, Store } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { MarketingPreferenceModel } from "@modules/sales/components/task-management/latest-lead-stage-flows/models/marketing-preference.model";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
import { CustomerModuleApiSuffixModels } from "@modules/customer/shared/utils/customer-module.enums";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { combineLatest } from "rxjs";
import { take } from "rxjs/operators";
@Component({
  selector: 'app-marketting-preference-dialog',
  templateUrl: './marketting-preference-dialog.component.html',
  styleUrls: ['./marketting-preference-dialog.component.scss']
})
export class MarkettingPreferenceDialogComponent implements OnInit {
  preferenceForm: FormGroup;
  countryCodes = countryCodes;
  isSubmitted = false;
  isButtonDisabled = false;
  loggedInUserData: LoggedInUserModel;
  isFormChangeDetected = false;
  mobileNumbers = [];
  landLineNumbers = [];
  mobileNumbersCopy = [];
  landLineNumbersCopy = [];
  isFormSubmitted = false;
  marketingPreferenceDetail;
  mobileNumberLength = 10;
  smsNumberLength = 10;
  lendlineNumberLength = 10;
  savedMarketingDataCopy = {
    email: '', smsNumber: '', smsNumberCountryCode: defaultCountryCode, mobileNumberCountryCode: defaultCountryCode,
    landLineNumberCountryCode: defaultCountryCode, mobileNumber: '', landLineNumber: '', address1: '', address2: '', address3: '', address4: '',
    postalCode: ''
  };
  constructor( private formBuilder: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
     private store: Store<AppState>,
      private rxjsService: RxjsService,
      public _fb: FormBuilder,
       private crudService: CrudService
  ) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createMarketingPreferenceForm();
    this.getMarketingPreferenceForCustomer()
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createMarketingPreferenceForm(): void {
    let marketingPreferenceModel = new MarketingPreferenceModel();
    this.preferenceForm = this.formBuilder.group({});
    Object.keys(marketingPreferenceModel).forEach((key) => {
      this.preferenceForm.addControl(key, new FormControl(marketingPreferenceModel[key]));
    });
  }

  onFormControlChanges() {
    this.preferenceForm.get('isNoMarketing').valueChanges.subscribe((isNoMarketing: boolean) => {
      if (isNoMarketing) {
        this.preferenceForm = addTrueOrFalseDefaultFormControlValue(this.preferenceForm, [{ controlName: 'isEmail', defaultValue: false, isDisabled: true },
        { controlName: 'isSMS', defaultValue: false, isDisabled: true }, { controlName: 'isMobileNumber', defaultValue: false, isDisabled: true }, { controlName: 'isLandLineNumber', defaultValue: false, isDisabled: true },
        { controlName: 'isPostal', defaultValue: false, isDisabled: true }, { controlName: 'isMorningBefore12', defaultValue: false, isDisabled: true }, { controlName: 'isAfterNoonAfter12', defaultValue: false, isDisabled: true },
        { controlName: 'isLaterAfterNoonAfter16', defaultValue: false, isDisabled: true }, { controlName: 'isSameAsSiteAddress', defaultValue: false, isDisabled: true }])
        this.preferenceForm = disableFormControls(this.preferenceForm, ['email', 'smsNumber', 'mobileNumber', 'landLineNumber', 'isPostal',
          'postalCode', 'address1', 'address2', 'address3', 'address4', 'smsNumberCountryCode', 'mobileNumberCountryCode', 'landLineNumberCountryCode']);
      }
      else {
        this.preferenceForm = enableFormControls(this.preferenceForm, ['isEmail', 'isSMS', 'isMobileNumber', 'isLandLineNumber', 'isPostal', 'isMorningBefore12', 'isAfterNoonAfter12',
          'isLaterAfterNoonAfter16', 'isSameAsSiteAddress']);
      }
    });
    this.preferenceForm.get("isEmail").valueChanges.subscribe((isEmail: boolean) => {
      if (isEmail) {
        this.preferenceForm = enableFormControls(this.preferenceForm, ['email']);
        this.preferenceForm = setRequiredValidator(this.preferenceForm, ['email']);
        this.preferenceForm.get('email').setValue(this.savedMarketingDataCopy.email);
      }
      else {
        this.preferenceForm = removeFormControlError(this.preferenceForm, 'invalid');
        this.preferenceForm = removeFormControlError(this.preferenceForm, 'required');
        this.preferenceForm = disableFormControls(this.preferenceForm, ['email']);
        this.preferenceForm.get('email').setValue("");
      }
    });
    this.preferenceForm.get("isSMS").valueChanges.subscribe((isSMS: boolean) => {
      if (isSMS) {
        this.preferenceForm = enableWithDefaultFormControlValues(this.preferenceForm, [{ formControlName: 'smsNumberCountryCode', value: this.savedMarketingDataCopy.smsNumberCountryCode ? this.savedMarketingDataCopy.smsNumberCountryCode : defaultCountryCode }, {
          formControlName: 'smsNumber', isRequired: true, value: this.savedMarketingDataCopy.smsNumber
        }]);
      }
      else {
        this.preferenceForm.get('smsNumber').setValue("");
        this.preferenceForm = disableFormControls(this.preferenceForm, ['smsNumberCountryCode', 'smsNumber']);
      }
    });
    this.preferenceForm.get("isMobileNumber").valueChanges.subscribe((isMobileNumber: boolean) => {
      if (isMobileNumber) {
        this.preferenceForm = enableWithDefaultFormControlValues(this.preferenceForm, [{ formControlName: 'mobileNumberCountryCode', value: this.savedMarketingDataCopy.mobileNumberCountryCode ? this.savedMarketingDataCopy.mobileNumberCountryCode : defaultCountryCode }, {
          formControlName: 'mobileNumber', isRequired: true, value: this.savedMarketingDataCopy.mobileNumber
        }]);
        this.preferenceForm.get('smsNumber').markAllAsTouched();
        this.preferenceForm.get('mobileNumber').markAllAsTouched();
        this.preferenceForm.get('landLineNumber').markAllAsTouched();
      }
      else {
        this.preferenceForm.get('mobileNumber').setValue("");
        this.preferenceForm = disableFormControls(this.preferenceForm, ['mobileNumberCountryCode', 'mobileNumber']);
      }
    });
    this.preferenceForm.get("isLandLineNumber").valueChanges.subscribe((isLandLineNumber: boolean) => {
      if (isLandLineNumber) {
        this.preferenceForm = enableWithDefaultFormControlValues(this.preferenceForm, [{ formControlName: 'landLineNumberCountryCode', value: this.savedMarketingDataCopy.landLineNumberCountryCode ? this.savedMarketingDataCopy.landLineNumberCountryCode : defaultCountryCode }, {
          formControlName: 'landLineNumber', isRequired: true, value: this.savedMarketingDataCopy.landLineNumber
        }]);
      }
      else {
        this.preferenceForm.get('landLineNumber').setValue("");
        this.preferenceForm = disableFormControls(this.preferenceForm, ['landLineNumberCountryCode', 'landLineNumber']);
      }
    });
    this.preferenceForm.get("isPostal").valueChanges.subscribe((isPostal: boolean) => {
      if (isPostal) {
        this.preferenceForm = enableWithDefaultFormControlValues(this.preferenceForm, [{ formControlName: 'address1', isRequired: true, value: this.savedMarketingDataCopy.address1 },
        { formControlName: 'address2', isRequired: true, value: this.savedMarketingDataCopy.address2 },
        { formControlName: 'address3', isRequired: true, value: this.savedMarketingDataCopy.address3 },
        { formControlName: 'address4', isRequired: true, value: this.savedMarketingDataCopy.address4 },
        { formControlName: 'postalCode', isRequired: true, value: this.savedMarketingDataCopy.postalCode }]);
      }
      else {
        this.preferenceForm = disableWithDefaultFormControlValues(this.preferenceForm, [{ formControlName: 'address1', value: '' },
        { formControlName: 'address2', value: '' }, { formControlName: 'address3', value: '' },
        { formControlName: 'address4', value: '' }, { formControlName: 'postalCode', value: '' }]);
      }
    });
    this.preferenceForm.get("isSameAsSiteAddress").valueChanges.subscribe((isSameAsSiteAddress: boolean) => {
      if (isSameAsSiteAddress == true) {
        console.log("isSameAsSiteAddress",isSameAsSiteAddress)
        this.preferenceForm = disableWithDefaultFormControlValues(this.preferenceForm, [{ formControlName: 'address1', value: this.marketingPreferenceDetail.leadAddress.addressLine1 },
        { formControlName: 'address2', value: this.marketingPreferenceDetail.leadAddress.addressLine2 },
        { formControlName: 'address3', value: this.marketingPreferenceDetail.leadAddress.addressLine3 },
        { formControlName: 'address4', value: this.marketingPreferenceDetail.leadAddress.addressLine4 },
        { formControlName: 'postalCode', value: this.marketingPreferenceDetail.leadAddress.postalCode }]);
        this.preferenceForm.get("isPostal").setValue(true, { emitEvent: false, onlySelf: true });
        this.preferenceForm.get("isPostal").disable({ emitEvent: false, onlySelf: true });
      }
      else if (isSameAsSiteAddress == false) {
        this.preferenceForm.patchValue({
          address1: '', address2: '', address3: '', address4: '', postalCode: '', isPostal: false
        });
        this.preferenceForm = enableFormControls(this.preferenceForm, ['isPostal']);
      }
    });
    this.filterContactNumbersByAutoCompleteLocally();
  }

  filterContactNumbersByAutoCompleteLocally(): void {
    this.preferenceForm.get("smsNumber").valueChanges.subscribe((searchSmsNumber: string) => {
      this.prepareAutocompleteContactNumbers('smsNumber', searchSmsNumber);
      this.preferenceForm.get('smsNumber').markAllAsTouched();
      this.smsNumberLength =  this.preferenceForm.get("smsNumberCountryCode").value == "+27" ? 11: 12

    });
    this.preferenceForm.get("mobileNumber").valueChanges.subscribe((searchMobileNumber: string) => {
      this.prepareAutocompleteContactNumbers('mobileNumber', searchMobileNumber);
      this.mobileNumberLength =  this.preferenceForm.get("mobileNumberCountryCode").value == "+27" ? 11  : 12
      this.preferenceForm.get('mobileNumber').markAllAsTouched();
    });
    this.preferenceForm.get("landLineNumber").valueChanges.subscribe((searchLandLineNumber: string) => {
      this.prepareAutocompleteContactNumbers('landLineNumber', searchLandLineNumber);
      this.lendlineNumberLength =  this.preferenceForm.get("landLineNumberCountryCode").value == "+27" ? 11 : 12
      this.preferenceForm.get('landLineNumber').markAllAsTouched();

    });
  }

  prepareAutocompleteContactNumbers(formControlName = 'smsNumber', searchNumber: string) {
    searchNumber = searchNumber && searchNumber.toString();
    let filteredCountryCode = formControlName == 'smsNumber' ? this.preferenceForm.get('smsNumberCountryCode').value :
      formControlName == 'mobileNumber' ? this.preferenceForm.get('mobileNumberCountryCode').value :
        this.preferenceForm.get('landLineNumberCountryCode').value;
    if (formControlName == 'smsNumber' || formControlName == 'mobileNumber') {
      this.mobileNumbers = this.mobileNumbersCopy.filter(mN => mN['countryCode'] == filteredCountryCode && mN['phoneNo'].toString().includes(searchNumber.toString().replace(/\s/g, "")));
    }
    else {
      this.landLineNumbers = this.landLineNumbersCopy.filter(lN => lN['countryCode'] == filteredCountryCode && lN['phoneNo'].toString().includes(searchNumber.toString().replace(/\s/g, "")));
    }
    switch (formControlName) {
      case 'smsNumber':
        if (searchNumber === "" && this.preferenceForm.get('isSMS').value) {
          this.preferenceForm.get(formControlName).setErrors({ required: true });
        }
        else {
          this.preferenceForm.get(formControlName).setErrors({ required: false });
        }
        break;
      case 'mobileNumber':
        if (searchNumber === "" && this.preferenceForm.get('isMobileNumber').value) {
          this.preferenceForm.get(formControlName).setErrors({ required: true });
        }
        break;
      case 'landLineNumber':
        if (searchNumber === "" && this.preferenceForm.get('isLandLineNumber').value) {
          this.preferenceForm.get(formControlName).setErrors({ required: true });
        }
        break;
    }
  }

  setPhoneNumberLengthByCountryCode(formControlName: string, countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.preferenceForm.get(formControlName).setErrors({
          minlength: { actualLength: formConfigs.nineDigits, requiredLength: formConfigs.nineDigits },
          maxlength: { actualLength: formConfigs.nineDigits, requiredLength: formConfigs.nineDigits }
        });
        break;
      default:
        this.preferenceForm.get(formControlName).setErrors({
          minlength: { actualLength: formConfigs.ten, requiredLength: formConfigs.ten },
          maxlength: { actualLength: formConfigs.ten, requiredLength: formConfigs.ten }
        });
        break;
    }
  }

  getMarketingPreferenceForCustomer() {
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.MARKETTING_PREFERENCES, null,
      false, prepareRequiredHttpParams({
        customerId: this.config.data.customerId,
        addressId: this.config.data.addressId,
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.marketingPreferenceDetail = response.resources;
          this.mobileNumbers = response.resources.mobileNumbers;
          this.mobileNumbersCopy = response.resources.mobileNumbers;
          this.landLineNumbers = response.resources.landLineNumbers;
          this.landLineNumbersCopy = response.resources.landLineNumbers;
          this.savedMarketingDataCopy = {
            email: response.resources?.email, smsNumber: response.resources?.smsNumber, mobileNumber: response.resources?.mobileNumber,
            landLineNumber: response.resources?.landLineNumber, smsNumberCountryCode: response.resources?.smsNumberCountryCode,
            mobileNumberCountryCode: response.resources?.mobileNumberCountryCode, landLineNumberCountryCode: response.resources?.landLineNumberCountryCode,
            address1: response.resources?.address1, address2: response.resources?.address2, address3: response.resources?.address3, address4: response.resources?.address4,
            postalCode: response.resources?.postalCode
          };
          response.resources.customerId =  this.config.data.customerId
          this.preferenceForm.patchValue(new MarketingPreferenceModel(response.resources));

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit() {
    this.preferenceForm.get('smsNumber').markAllAsTouched();
    this.preferenceForm.get('mobileNumber').markAllAsTouched();
    this.preferenceForm.get('landLineNumber').markAllAsTouched();
    this.isFormSubmitted = true;
    if ((!this.preferenceForm.get('isNoMarketing').value && !this.preferenceForm.get('isEmail').value &&
      !this.preferenceForm.get('isSMS').value && !this.preferenceForm.get('isMobileNumber').value &&
      !this.preferenceForm.get('isLandLineNumber').value && !this.preferenceForm.get('isPostal').value && this.isFormSubmitted) ||
      (!this.preferenceForm.get('isNoMarketing').value && !this.preferenceForm.get('isMorningBefore12').value &&
        !this.preferenceForm.get('isAfterNoonAfter12').value && !this.preferenceForm.get('isLaterAfterNoonAfter16').value && this.isFormSubmitted)) {
      this.rxjsService.setFormChangeDetectionProperty(true);
    }
    let payload = this.preferenceForm.getRawValue();
    if (!payload.smsNumber || !payload.isSMS) {
      delete payload.smsNumberCountryCode;
      delete payload.smsNumber;
    }
    if (payload.isSMS && payload.smsNumber) {
      payload.smsNumber = payload.smsNumber.toString().replace(/\s/g, "");
    }
    if (((payload.smsNumberCountryCode == defaultCountryCode && payload.smsNumber.length !== 9) ||
      ((payload.smsNumberCountryCode == '+91' || payload.smsNumberCountryCode == '+45') && payload.smsNumber.length !== 10)) &&
      this.preferenceForm.get('isSMS').value && this.preferenceForm.get('smsNumber').value !== "") {
      this.setPhoneNumberLengthByCountryCode('smsNumber', this.preferenceForm.get("smsNumberCountryCode").value);
    }
    if (!payload.mobileNumber || !payload.isMobileNumber) {
      delete payload.mobileNumberCountryCode;
      delete payload.mobileNumber;
    }
    if (payload.isMobileNumber && payload.mobileNumber) {
      payload.mobileNumber = payload.mobileNumber.toString().replace(/\s/g, "");
    }
    if (((payload.mobileNumberCountryCode == defaultCountryCode && payload.mobileNumber.length !== 9) ||
      ((payload.mobileNumberCountryCode == '+91' || payload.mobileNumberCountryCode == '+45') && payload.mobileNumber.length !== 10)) &&
      this.preferenceForm.get('isMobileNumber').value && this.preferenceForm.get('mobileNumber').value) {
      this.setPhoneNumberLengthByCountryCode('mobileNumber', this.preferenceForm.get("mobileNumberCountryCode").value);
    }
    if (!payload.landLineNumber || !payload.isLandLineNumber) {
      delete payload.landLineNumberCountryCode;
      delete payload.landLineNumber;
    }
    if (payload.isLandLineNumber && payload.landLineNumber) {
      payload.landLineNumber = payload.landLineNumber.toString().replace(/\s/g, "");
    }
    if (((payload.landLineNumberCountryCode == defaultCountryCode && payload.landLineNumber.length !== 9) ||
      ((payload.landLineNumberCountryCode == '+91' || payload.landLineNumberCountryCode == '+45') &&
        payload.landLineNumber.length !== 10)) && this.preferenceForm.get('isLandLineNumber').value && this.preferenceForm.get('landLineNumber').value) {
      this.setPhoneNumberLengthByCountryCode('landLineNumber', this.preferenceForm.get("landLineNumberCountryCode").value);
    }
    payload.customerId = this.config.data.customerId;
    payload.createdUserId = this.loggedInUserData.userId
    console.log("this.loggedInUserData.userId",this.loggedInUserData.userId)
    if (this.preferenceForm.invalid || (!this.preferenceForm.get('isNoMarketing').value &&
      !this.preferenceForm.get('isMorningBefore12').value &&
      !this.preferenceForm.get('isAfterNoonAfter12').value && !this.preferenceForm.get('isLaterAfterNoonAfter16').value) ||
      (!this.preferenceForm.get('isNoMarketing').value && !this.preferenceForm.get('isEmail').value &&
        !this.preferenceForm.get('isSMS').value && !this.preferenceForm.get('isMobileNumber').value &&
        !this.preferenceForm.get('isLandLineNumber').value && !this.preferenceForm.get('isPostal').value)) {
      return;
    }
    this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.MARKETTING_PREFERENCES, payload).subscribe((response) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        this.close();
      }
      this.isFormSubmitted = false;
    });
  }

  close() {
    this.ref.close()
  }
}
