import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { LayoutEffectEffects } from './layout-effect.effects';

describe('LayoutEffectEffects', () => {
  let actions$: Observable<any>;
  let effects: LayoutEffectEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LayoutEffectEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<LayoutEffectEffects>(LayoutEffectEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
