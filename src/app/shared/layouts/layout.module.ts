import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatMenuModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { CustomerUpgradeModalComponent } from '@app/modules/sales/components/lead-notes/customer-upgrade-modal.component';
import {
	AppBanner, AppFooter, AppHeader, AppLoaderComponent, AppMoreAdvancedSearchComponent, AppSidebar, ExtensionModalComponent,
	LayoutComponent, LayoutEffectEffects, layoutReducer, pageBasedPermissionReducer, QuickChatComponent
} from '@app/shared/layouts';
import { environment } from '@environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { SidebarModule } from 'primeng/sidebar';
import { ToastModule } from 'primeng/toast';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared.module';
import { AgentLoginReducer } from './app-header';
import { AgentLoginEffects } from './app-header/state/agent-login.effects';
import { PageSpinnerComponent } from './page-spinner/page-spinner.component';
import {OverlayPanelModule} from 'primeng/overlaypanel';

@NgModule({
	declarations: [
		LayoutComponent,
		AppHeader,
		AppSidebar,
		AppFooter,
		AppBanner,
		AppLoaderComponent,
		ExtensionModalComponent,
		AppMoreAdvancedSearchComponent,
		CustomerUpgradeModalComponent,
		PageSpinnerComponent,
		QuickChatComponent,
	],
	imports: [
		ReactiveFormsModule,
		FormsModule,
		CommonModule,
		RouterModule,
		MaterialModule,
		SidebarModule,
		SharedModule,
		OverlayPanelModule,
		MatMenuModule,
		ToastModule,
		VirtualScrollerModule,
		StoreModule.forFeature('layout',layoutReducer),
		StoreModule.forFeature('pageBasedPermissions',pageBasedPermissionReducer),
		StoreModule.forFeature('agentExtension',AgentLoginReducer),
		!environment.production ? StoreDevtoolsModule.instrument() : [],
		EffectsModule.forFeature([LayoutEffectEffects, AgentLoginEffects])
	],
	exports: [LayoutComponent,
		AppHeader,
		AppSidebar,
		AppFooter,
		AppBanner,
		AppLoaderComponent,
		AppMoreAdvancedSearchComponent,
		CustomerUpgradeModalComponent,
		PageSpinnerComponent,
		QuickChatComponent,
	],
	entryComponents: [ExtensionModalComponent,
		CustomerUpgradeModalComponent]
})
export class LayoutModule {
}
