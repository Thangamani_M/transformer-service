import { Component, Input, OnInit } from '@angular/core';
import { RxjsService } from '@app/shared/services';
@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class AppLoaderComponent implements OnInit {
  @Input()isLoading = false;
  color = 'primary';
  mode = 'indeterminate';
  constructor(private rxjsService: RxjsService) {
  }
  
  ngOnInit() {
    this.rxjsService.getGlobalLoaderProperty().subscribe((isLoading) => {
      setTimeout(() => {
        this.isLoading = isLoading;
      }, 50);
    });
  }
}