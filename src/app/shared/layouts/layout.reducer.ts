import { LayoutActions, LayoutActionTypes } from '@app/shared/layouts';
import { IApplicationResponse } from '../utils';
export interface LayoutState {
  sidebarData: IApplicationResponse[]
}

export const initialLayoutState: LayoutState = {
  sidebarData: undefined
};
export interface PagePermissionsState {
  moduleBasedComponentPermissions:[];
  currentComponentPageBasedPermissions: {};
}

export const initialPagePermissionsState: PagePermissionsState = {
  moduleBasedComponentPermissions:[],
  currentComponentPageBasedPermissions: []
};

export function layoutReducer(state = initialLayoutState,
  action: LayoutActions): LayoutState {
  switch (action.type) {
    case LayoutActionTypes.SidebarDataLoadedAction:
      return {
        sidebarData: action.payload.response
      };
      case LayoutActionTypes.SidebarDataRemovedAction:
        return{
          sidebarData:undefined
        }
    default:
      return state;
  }
}

export function pageBasedPermissionReducer(state = initialPagePermissionsState,
  action: LayoutActions): PagePermissionsState {
  switch (action.type) {
    case LayoutActionTypes.PageBasedPermissionsCreateAction:
      return {
        moduleBasedComponentPermissions:action.payload.moduleBasedComponentPermissions,
        currentComponentPageBasedPermissions: action.payload.currentComponentPageBasedPermissions
      };
      case LayoutActionTypes.PageBasedPermissionsDeleteAction:
        return{
          moduleBasedComponentPermissions:[],
          currentComponentPageBasedPermissions: []
        }
    default:
      return state;
  }
}
