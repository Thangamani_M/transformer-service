import { Component, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes, UserService } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { SignalrConnectionService } from '@app/shared/services/signalr-connection.service';
import { AgentMediaTypes, AuthenticationModuleApiSuffixModels, convertStringsToQueryParams, debounceTimeForSearchkeyword, prepareGetRequestHttpParams, prepareRequiredHttpParams, regionId, SharedModuleApiSuffixModels } from '@app/shared/utils';
import { MediaTypes } from '@modules/my-tasks/components/email-dashboard/media-type.enums';
import { loggedInUserData, LoginUpdate } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { TelephonyApiSuffixModules } from '@sales/shared';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { ExtensionModalComponent } from './extension-modal.component';
import { AgentLoginDataLoaded, agentLoginDataSelector, agentLoginMediaTypesSelector, AgentLogoutDataRemoved, agentStatus } from './state';

declare var $: any;

export enum RoutingStates {
  AVAILABLE = 1,
  UNAVAILABLE = 2,
  WORK = 3,
  LOGGEDOFF = 4,
  UNKNOWN = 0
}
@Component({
  selector: '[app-header]',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
})
export class AppHeader {
  search: FormControl = new FormControl();
  customersList;
  UserStatusList = [];
  isRequestInProgress = true;
  loggedInUserData: LoggedInUserModel = new LoggedInUserModel();
  selectedUserStatusCssClass;
  userStatusId;
  agentExtensionNo = "";
  newState: any;
  notifications = [];
  notificationsCount = 0;
  isShowSubMenu = false;
  mediaList = [
    {
      mediaName: 'Voice',
      status: false,
      statusColor: 'status-label-pink',
      tickColor: '',
      mediaId: 1
    },
    {
      mediaName: 'Email',
      status: false,
      statusColor: 'status-label-pink',
      tickColor: '',
      mediaId: 3
    },
    {
      mediaName: 'Callback',
      status: false,
      statusColor: 'status-label-pink',
      tickColor: '',
      mediaId: 2,
    }
  ];
  items = [];
  screenHeight: number;
  screenWidth: number;
  setIntervalToCheckUserStatus: NodeJS.Timer;
  agentLoginMediaTypes: string[];
  agentStatus: RoutingStates;

  constructor(private crudService: CrudService, private route: ActivatedRoute,
    private router: Router, private store: Store<AppState>, private rxjsService: RxjsService,
    private dialog: MatDialog, private snackbarService: SnackbarService,
    private userService: UserService, private signalrConnectionService: SignalrConnectionService
  ) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.store.select(agentLoginDataSelector),
    this.store.select(agentLoginMediaTypesSelector), this.store.select(agentStatus)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.agentExtensionNo = response[1];
        // Retrieve agent logged in media types from ngrx store
        if (response[2]) {
          this.agentLoginMediaTypes = response[2];
          if (this.agentLoginMediaTypes?.length > 0) {
            if (this.setIntervalToCheckUserStatus) {
              clearInterval(this.setIntervalToCheckUserStatus);
            }
            this.frequentServerCallToCheckUserAvailableStatus();
          }
          else if (this.agentStatus !== RoutingStates.AVAILABLE && this.setIntervalToCheckUserStatus) {
            clearInterval(this.setIntervalToCheckUserStatus);
          }
        }
        // Retrieve agent logged in media types from ngrx store
        if (response[3]) {
          this.agentStatus = response[3];
          if (this.agentStatus) {
            if (this.setIntervalToCheckUserStatus) {
              clearInterval(this.setIntervalToCheckUserStatus);
            }
            this.frequentServerCallToCheckUserAvailableStatus();
          }
          else if (this.agentLoginMediaTypes?.length == 0 && this.setIntervalToCheckUserStatus) {
            clearInterval(this.setIntervalToCheckUserStatus);
          }
        }
      });
  }

  combineLatestNotificationObservables() {
    this.rxjsService.getNotifications().subscribe((response) => {
      if (response) {
        this.notifications = response.notifications ? response.notifications : [];
        this.notificationsCount = response.count ? response.count : 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngOnInit(): void {
    this.gotoTop();
    this.getScreenSize();
    this.combineLatestNotificationObservables();
    this.combineLatestNgrxStoreData();
    this.getUserStatusList();
    this.search.valueChanges
      .pipe(tap(() => {
        this.isRequestInProgress = true
      }),
        distinctUntilChanged(),
        debounceTime(debounceTimeForSearchkeyword),
        switchMap(search => {
          this.customersList = [];
          if (search == '' || !search) {
            return of();
          }
          return this.getCustomersListByKeyword(search);
        })).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.customersList = response.resources;
          }
          else {
            this.customersList = [];
          }
          this.isRequestInProgress = false;
        });
    if (this.agentExtensionNo) {
      this.getActiveExtension();
    }
  }

  frequentServerCallToCheckUserAvailableStatus() {
    // 30 seconds Interval based server call to make sure that user is in available state in order to get inbound queue calls any time
    if (this.agentExtensionNo && this.selectdStatus.toLowerCase() == 'available' &&
      (this.agentLoginMediaTypes?.includes(AgentMediaTypes.VOICE) || this.agentLoginMediaTypes?.includes(AgentMediaTypes.CALLBACK))) {
      let userStatusId = '';
      if (this.agentStatus) {
        userStatusId = this.UserStatusList?.find(us => us.id === this.agentStatus)?.id;
      }
      else {
        userStatusId = this.UserStatusList?.find(us => us.displayName.toLowerCase() === this.selectdStatus.toLowerCase())?.id;
      }
      this.setIntervalToCheckUserStatus = setInterval(() => {
        this.rxjsService.setNotificationProperty(false);
        this.crudService.update(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.UX_USER_CHANGE_STATUS, {
          userId: this.loggedInUserData.userId,
          userStatusId,
          isDefaultLoaderDisabled: true
        }).subscribe();
      }, 30000);
    }
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }

  gotoTop(): void {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  getNotificationCount() {
    if (this.loggedInUserData?.userId) {
      this.crudService.get(ModulesBasedApiSuffix.COMMON_API, SharedModuleApiSuffixModels.NOTIFICATIONS, this.loggedInUserData.userId)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200 && response.resources) {
            this.rxjsService.setNotifications({ notifications: response.resources.notifications, count: response.resources.notificationCount });
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onNotificationClicked(notification) {
    this.rxjsService.setNotificationProperty(false);
    if (notification.type == 'Technician Stock Order Collection') {
      this.rxjsService.setGlobalLoaderProperty(false);
      return
    }
    if (notification.type != "Technician Stock Order Collection" && !notification.url) {
      this.snackbarService.openSnackbar("Navigation URL is not mapped with this notification", ResponseMessageTypes.WARNING);
      this.rxjsService.setGlobalLoaderProperty(false);
      return
    }
    if (notification.type == 'Business Notification') {
      this.rxjsService.setBusinessNotificationProperty(true);
    } else {
      this.rxjsService.setBusinessNotificationProperty(false);
    }
    this.crudService.update(ModulesBasedApiSuffix.COMMON_API, SharedModuleApiSuffixModels.NOTIFICATIONS, {
      notificationId: notification.notificationId,
      modifiedUserId: this.loggedInUserData.userId,
      notificationTo: notification.notificationTo
    }).subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.isSuccess) {
        this.crudService.get(ModulesBasedApiSuffix.COMMON_API, SharedModuleApiSuffixModels.NOTIFICATIONS, this.loggedInUserData.userId)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200 && response.resources) {
              this.rxjsService.setNotifications({ notifications: response.resources.notifications, count: response.resources.notificationCount });
            }
          });
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    if (notification.url.indexOf("?") == -1) {
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([notification.url], { relativeTo: this.route });
    } else {
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([notification.url.slice(0, notification.url.indexOf("?"))], { relativeTo: this.route, queryParams: convertStringsToQueryParams(notification.url) });
      setTimeout(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }, 500);
    }
  }

  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    const addressId = this.customersList?.find(cL => cL['customerId'] == event.option.value)?.addressId
    this.rxjsService.setViewCustomerData({
      customerId: event.option.value,
      addressId: addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
    this.search.setValue("");
  }

  getUserStatusList() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USER_STATUS, this.loggedInUserData.userId).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.UserStatusList = response.resources;
        this.UserStatusList.map(item => {
          if (item.isSelected) {
            this.selectedUserStatusCssClass = item.cssClass;
            this.selectdStatus = item.displayName;
          }
        });
      }
    });
  }

  getCustomersListByKeyword(search: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.GLOBAL_SEARCH, undefined,
      false, prepareGetRequestHttpParams(undefined, undefined, { search }));
  }

  selectdStatus = "Select Status";
  selectedParent(parent) {
    this.unSelectStatus();
    parent.keyName = parent.displayName;
    this.selectdStatus = parent.displayName;
    parent.isSelected = true;
    this.onUserStatusChanged(parent);
  }

  selectedChild(parent, child) {
    this.unSelectStatus();
    this.selectdStatus = parent.displayName;
    child.keyName = parent.displayName;
    parent.isSelected = true;
    child.isSelected = true;
    this.onUserStatusChanged(child);
  }

  unSelectStatus() {
    for (const status of this.UserStatusList) {
      status.isSelected = false;
      for (const child of status.subStatus) {
        child.isSelected = false;
      }
    }
  }

  onUserStatusChanged(selectedUserStatusObj): void {
    this.isShowSubMenu = false;
    let selUserStatusObj = this.UserStatusList?.find(u => u.id === selectedUserStatusObj.id);
    this.rxjsService.setNotificationProperty(false);
    this.crudService.update(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.UX_USER_CHANGE_STATUS, {
      userId: this.loggedInUserData.userId,
      userStatusId: selectedUserStatusObj.id
    }).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.selectedUserStatusCssClass = selUserStatusObj['cssClass'];
        // After a successful media switching we want to update the media type in the store
        this.store.dispatch(new AgentLoginDataLoaded({
          extensionNo: this.agentExtensionNo ? this.agentExtensionNo : null,
          agentLoginMediaTypes: this.agentLoginMediaTypes,
          agentStatus: selectedUserStatusObj.id
        }));
      }
    });
    if (this.agentExtensionNo) {
      if (selectedUserStatusObj.keyName == 'Available') {
        this.newState = RoutingStates.AVAILABLE;
      } else if (selectedUserStatusObj.keyName == 'Unavailable') {
        this.newState = RoutingStates.UNAVAILABLE;
      } else if (selectedUserStatusObj.keyName == "Work") {
        this.newState = RoutingStates.WORK;
      } else {
        this.newState = null;
      }
      if (this.newState) {
        this.checkIfEmailLoggedInAndSetRoutingForEmail({ newStateId: this.newState, reason: selectedUserStatusObj.reasonKey })
        this.crudService.create(ModulesBasedApiSuffix.ROUTING_STATE, AuthenticationModuleApiSuffixModels.SET_ROUTING_STATE_NEW, {
          extension: this.agentExtensionNo,
          newStateId: this.newState,
          pabxId: this.loggedInUserData.openScapePabxId,
          userId: this.loggedInUserData?.userId,
          reason: selectedUserStatusObj.reasonKey
        }).pipe(tap(() => {
          this.rxjsService.setNotificationProperty(true);
          this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe(() => {
        });
      }
      this.selectedUserStatusCssClass = selUserStatusObj['cssClass'];
    }
  }

  onApplicationLogout() {
    this.userService.onLogout();
  }

  sidebarToggle() {
    $('body').toggleClass('sidebar-mini');
  }

  stopEvent(e) {
    this.isShowSubMenu = !this.isShowSubMenu;
    e.stopPropagation();
  }

  public OpenDialogWindow() {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      this.agentExtensionNo = ele;
      this.getActiveExtension();
    });
  }

  checkIfEmailLoggedInAndSetRoutingForEmail(obj) {
    let isEmailLoggedIn = false;
    for (const media of this.mediaList) {
      if (media.mediaId == MediaTypes.EMAIL && media.status == true) {
        isEmailLoggedIn = true;
      }
    }
    if (isEmailLoggedIn) {
      this.crudService.create(ModulesBasedApiSuffix.ROUTING_STATE, AuthenticationModuleApiSuffixModels.SET_ROUTING_STATE_EMAIL, obj).pipe(tap(() => {
        this.rxjsService.setNotificationProperty(true);
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe(() => {
      });
    }
  }

  toggleMedia(media) {
    this.rxjsService.setGlobalLoaderProperty(true);
    let API: TelephonyApiSuffixModules
    API = media.status ? TelephonyApiSuffixModules.AGENT_LOGOFF : TelephonyApiSuffixModules.AGENTLOGIN
    let payload: any = {
      extension: this.agentExtensionNo,
      userId: this.loggedInUserData?.userId,
      regionId,
      password: !media.status ? atob(sessionStorage.getItem('agentPwd')) : "" || null
    }
    if (media.status) {
      payload.mediaTypeId = media.mediaId;
    } else {
      payload.mediaTypes = [media.mediaId];
    }
    // TODO:missing use case to handle password at the backend side for seperate media toggle login
    this.crudService.create(ModulesBasedApiSuffix.TELEPHONY, API, payload).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        // After a successful media switching we want to update the media type in the store
        this.store.dispatch(new AgentLoginDataLoaded({
          extensionNo: this.agentExtensionNo,
          agentLoginMediaTypes: media.mediaId,
          agentStatus: this.agentStatus
        }));
        this.getActiveExtension();
      }
    });
  }

  loggedMedia=[];
  getActiveExtension() {
    this.crudService.get(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.LOGGEDIN_MEDIA, null, false, prepareRequiredHttpParams({ userId: this.loggedInUserData.userId, extension: this.agentExtensionNo })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.loggedMedia = response.resources;
        for (const media of this.loggedMedia) {
          if (media.mediaTypeId == MediaTypes.VOICE) {
            this.mediaList[0].status = !media.isLoggedOff;
            this.mediaList[0].statusColor = this.mediaList[0].status ? 'status-label-green' : 'status-label-red';
            this.mediaList[0].tickColor = this.mediaList[0].status ? 'tick-green' : 'disabled-tick';
          }
          if (media.mediaTypeId == MediaTypes.CALLBACK) {
            this.mediaList[2].status = !media.isLoggedOff;
            this.mediaList[2].statusColor = this.mediaList[2].status ? 'status-label-green' : 'status-label-red';
            this.mediaList[2].tickColor = this.mediaList[2].status ? 'tick-green' : 'disabled-tick';
          }
          if (media.mediaTypeId == MediaTypes.EMAIL) {
            this.mediaList[1].status = !media.isLoggedOff;
            this.mediaList[1].statusColor = this.mediaList[1].status ? 'status-label-green' : 'status-label-red';
            this.mediaList[1].tickColor = this.mediaList[1].status ? 'tick-green' : 'disabled-tick';
          }
        }
        if (this.loggedMedia?.length == 0 || this.mediaList?.filter(mL => !mL['status'])?.length == this.mediaList.length) {
          this.closeAllOpenscapeConfigs();
        }
        if (this.getNonActiveMediaCount() == this.mediaList.length) {
          this.closeAllOpenscapeConfigs();
        }
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  closeAllOpenscapeConfigs() {
    // Close the telephony hub connection if all the media type logins are logged out
    this.signalrConnectionService.closeTelephonyAPIHubConnection();
    // If if all the media type logins are logged out remove the pabxID from ngrx store and update
    this.loggedInUserData.openScapePabxId = null;
    this.loggedInUserData.mediaTypeIds = null;
    this.store.dispatch(new LoginUpdate({
      user: this.loggedInUserData
    }));
    this.store.dispatch(new AgentLogoutDataRemoved());
  }

  getNonActiveMediaCount(): number {
    let count = 0
    for (const iterator of this.mediaList) {
      if (iterator.status === false) {
        count++;
      }
    }
    return count;
  }

  navigateToNotificationList() {
    this.router.navigate(['/notifications']);
  }
}
