import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { AppState } from '@app/reducers';
import {
  AgentLoginDataLoaded, agentStatus, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  OnDemandPreloadService, regionId, RoutingStates, UserService
} from '@app/shared';
import { AgentLoginModel } from '@app/shared/models/agent-login.model';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData, LoginUpdate } from '@modules/others';
import { select, Store } from '@ngrx/store';
import { TelephonyApiSuffixModules } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { UserModuleApiSuffixModels } from '../../../modules/user/shared/utils/user-module.enums';

const HOST = window.location.toString();
@Component({
  selector: 'app-extension-config',
  templateUrl: './extension-modal.component.html',
  styleUrls: ['./extension-modal.component.scss'],
})
export class ExtensionModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  agentExtensionForm: FormGroup;
  agentLogin;
  loggedInUserData: LoggedInUserModel;
  selectedMedia = [];
  isMediaNotSelected = false;
  isSubmitted = false;
  agentStatus: RoutingStates = null;
  regions = [];

  constructor(
    public dialogRef: MatDialogRef<ExtensionModalComponent>,
    private userService: UserService,
    private crudService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private preloadOnDemandService: OnDemandPreloadService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setDialogOpenProperty(true);
    this.agentLogin = new AgentLoginModel();
    this.agentExtensionForm = this.formBuilder.group({
      regionId: ['', [Validators.required]],
      extension: [HOST.includes('localhost') ? '4028' : '' || this.agentLogin.extension, [Validators.required]],
      password: [HOST.includes('localhost') ? 'agent@4028' : '' || this.agentLogin.password, [Validators.required]],
    });
    this.getRegions();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.pipe(select(agentStatus))]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      // Retrieve agent logged in media types from ngrx store
      this.agentStatus = response[1];
    });
  }

  getRegions() {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.UX_CALL_CENTER_REGIONS, undefined, false)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.regions = response.resources;
          if (response.resources.length == 1) {
            this.agentExtensionForm.get('regionId').setValue(response.resources[0]?.id);
          }
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onSubmit(): void {
    this.isSubmitted = true;
    if (this.selectedMedia.length == 0) {
      this.isMediaNotSelected = true;
      this.agentExtensionForm.setErrors({ invalid: true });
      return;
    }
    else if (this.agentExtensionForm.value.extension &&
      this.agentExtensionForm.value.password && this.selectedMedia.length > 0) {
      this.agentExtensionForm.setErrors(null);
    }
    this.isMediaNotSelected = false;
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.agentExtensionForm.invalid) return;
    setTimeout(() => {
      // preload customer's manage customers lazy load module
      this.preloadOnDemandService.startPreload('customer');
    });
    let payload = { ...this.agentLogin, ...this.agentExtensionForm.value }
    payload.userId = this.loggedInUserData.userId;
    payload.regionId = regionId;
    payload.mediaTypes = this.selectedMedia;
    this.crudService.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.AGENTLOGIN, payload).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        // take regionId as openscape pabxID and add it in ngrx store
        this.loggedInUserData.openScapePabxId = this.agentExtensionForm.get('regionId').value;
        this.loggedInUserData.mediaTypeIds = payload.mediaTypes;
        this.userService.openScapePabxId = this.agentExtensionForm.get('regionId').value;
        this.store.dispatch(new LoginUpdate({
          user: this.loggedInUserData
        }));
        this.outputData.emit(payload.extension);
        sessionStorage.setItem("agentPwd", btoa(this.agentExtensionForm.value?.password))
        this.rxjsService.setExtensionNumber(payload.extension);
        // open a openscape dialup component
        this.store.dispatch(new AgentLoginDataLoaded({
          agentKey: response.resources.agentKey,
          extensionNo: payload.extension,
          agentLoginMediaTypes: this.selectedMedia,
          agentStatus: this.agentStatus
        }));
        this.onSaveComplete(response, payload.extension);
        let data = null;
        this.rxjsService.setInboundCallDetails(data);
      }
      this.isSubmitted = false;
    });
  }

  onSaveComplete(response: IApplicationResponse, extension): void {
    if (response.statusCode == 200) {
      this.dialogRef.close(true);
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
