import { AgentLoginActions, AgentLoginActionTypes, RoutingStates } from '@app/shared/layouts';

export interface AgentLoginState {
  extensionNo: string,
  agentLoginMediaTypes: string[],
  agentStatus: RoutingStates,
  agentKey: string
}

export const initialAgentLoginState: AgentLoginState = {
  extensionNo: undefined,
  agentLoginMediaTypes: [],
  agentStatus: null, agentKey: null
};

export function AgentLoginReducer(state = initialAgentLoginState,
  action: AgentLoginActions): AgentLoginState {
  switch (action.type) {
    case AgentLoginActionTypes.AGENT_LOGIN_ACTION:
      return {
        extensionNo: action.payload.extensionNo,
        agentLoginMediaTypes: action.payload.agentLoginMediaTypes,
        agentStatus: action.payload.agentStatus,
        agentKey: action.payload.agentKey
      };
    case AgentLoginActionTypes.AGENT_LOGIN_UPDATE:
      return {
        ...state, ...action.payload
      };
    case AgentLoginActionTypes.AGENT_LOGOUT_ACTION:
      return {
        extensionNo: undefined,
        agentLoginMediaTypes: [],
        agentStatus: null, agentKey: null
      }
    default:
      return state;
  }
}
