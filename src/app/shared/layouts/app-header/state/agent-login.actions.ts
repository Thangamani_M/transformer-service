import { RoutingStates } from '@app/shared';
import {Action} from '@ngrx/store';
export enum AgentLoginActionTypes {
  AGENT_LOGIN_ACTION = '[AgentLogin] Data Loaded',
  AGENT_LOGIN_UPDATE = '[AgentLogin] Data Updated',
  AGENT_LOGOUT_ACTION = '[AgentLogout] Data Removed',
}
export class AgentLoginDataLoaded implements Action {
  readonly type = AgentLoginActionTypes.AGENT_LOGIN_ACTION;
  constructor(public payload:{extensionNo: any,agentLoginMediaTypes:string[],agentStatus:RoutingStates,agentKey?:string}) {}
}

export class AgentLoginDataUpdated implements Action {
  readonly type = AgentLoginActionTypes.AGENT_LOGIN_UPDATE;
  constructor(public payload: {
    extensionNo: any,agentLoginMediaTypes:string[],agentStatus:RoutingStates,agentKey?:string
  }) {
  }
}
export class AgentLogoutDataRemoved implements Action {
  readonly type = AgentLoginActionTypes.AGENT_LOGOUT_ACTION;
}

export type AgentLoginActions = AgentLoginDataLoaded | AgentLoginDataUpdated|AgentLogoutDataRemoved;
  



