import {createSelector, createFeatureSelector} from '@ngrx/store';
import { AgentLoginState } from '@app/shared';

export const selectAgentExtensionState = createFeatureSelector<AgentLoginState>("agentExtension");

export const agentLoginDataSelector = createSelector(
    selectAgentExtensionState,
  agentLoginData => agentLoginData.extensionNo
);

export const agentLoginMediaTypesSelector = createSelector(
  selectAgentExtensionState,
  agentLoginData => agentLoginData.agentLoginMediaTypes
);

export const agentStatus = createSelector(
  selectAgentExtensionState,
  agentLoginData => agentLoginData.agentStatus
);

export const agentKey$ = createSelector(
  selectAgentExtensionState,
  agentLoginData => agentLoginData.agentKey
);
