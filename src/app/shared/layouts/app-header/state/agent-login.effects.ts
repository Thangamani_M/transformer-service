import { Injectable } from '@angular/core';
import { AgentLoginActionTypes, AgentLoginDataLoaded, AgentLoginDataUpdated, AgentLogoutDataRemoved } from '@app/shared/layouts';
import { UserService } from '@app/shared/services';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { defer, of } from 'rxjs';
import { tap } from 'rxjs/operators';
@Injectable()
export class AgentLoginEffects {

  @Effect({ dispatch: false })
  AgeintLoginDataLoaded$ = this.actions$.pipe(
    ofType<AgentLoginDataLoaded>(AgentLoginActionTypes.AGENT_LOGIN_ACTION),
    tap(action => {
      this.userService.agentLoginData = action.payload.extensionNo;
      this.userService.agentLoginMediaTypes = action.payload.agentLoginMediaTypes;
      this.userService.agentStatus = action.payload.agentStatus;
      this.userService.agentKey = action.payload.agentKey;
    })
  );

  @Effect({ dispatch: false })
  AgeintLoginDataUpdated$ = this.actions$.pipe(
    ofType<AgentLoginDataUpdated>(AgentLoginActionTypes.AGENT_LOGIN_UPDATE),
    tap(action => {
      if (action.payload.extensionNo) {
        this.userService.agentLoginData = action.payload.extensionNo;
      }
      if (action.payload.agentLoginMediaTypes) {
        this.userService.agentLoginMediaTypes = action.payload.agentLoginMediaTypes;
      }
      if (action.payload.agentStatus) {
        this.userService.agentStatus = action.payload.agentStatus;
      }
      if (action.payload.agentKey) {
        this.userService.agentKey = action.payload.agentKey;
      }
    })
  );

  @Effect({ dispatch: false })
  LoadPageBasedPermissionsData$ = this.actions$.pipe(
    ofType<AgentLogoutDataRemoved>(AgentLoginActionTypes.AGENT_LOGOUT_ACTION),
    tap(action => {
      this.userService.agentLoginData = null;
      this.userService.agentLoginMediaTypes = null;
      this.userService.agentStatus = null;
      this.userService.agentKey = null
    })
  );

  @Effect()
  init$ = defer(() => {
    const extensionNo = this.userService.agentLoginData;
    const agentLoginMediaTypes = this.userService.agentLoginMediaTypes;
    const agentStatus = this.userService.agentStatus;
    const agentKey = this.userService.agentKey;
    if (extensionNo) {
      return of(new AgentLoginDataLoaded({ extensionNo, agentLoginMediaTypes, agentStatus, agentKey }));
    }
    else {
      return <any>of(new AgentLogoutDataRemoved());
    }
  });

  constructor(private actions$: Actions, private userService: UserService) {

  }
}
