import { Route, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services';
import { LayoutComponent } from "./layout.component";

export class LayoutsService {
  static childRoutes(routes: Routes): Route {
    return {
      path: '',
      component: LayoutComponent,
      children: routes,
      canActivate: [AuthGuard],
      //canDeactivate:LayoutsService.prepareChildRoutesRoutingConfigs(routes),
      // Reuse LayoutComponent instance when navigating between child views
      data: { reuse: true }
    };
  }

  static prepareChildRoutesRoutingConfigs(routes: Routes) {
    routes.forEach((route: Route) => {
      if (route.path == 'leads') {
        route.children.forEach((childRoute: Route) => {
          if (childRoute.children) {
            this.prepareNestedChildRoutesRoutingConfigs(childRoute.children);
          }
          else {
            childRoute.canDeactivate = [CanDeactivateGuard]
          }
        });
      }
    })
    return routes;
  }

  static prepareNestedChildRoutesRoutingConfigs(routes: Routes) {
    routes.forEach((childRoute: Route) => {
      if (childRoute.children) {

      }
      else {
        childRoute.canDeactivate = [CanDeactivateGuard]
      }
    })
    return routes;
  }
}

