import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, moduleBasedComponentPermissionsSelector$, ModulesBasedApiSuffix, OnDemandPreloadService, PageBasedPermissionsLoaded, RxjsService, sidebarDataSelector$ } from "@app/shared";
import { LoggedInUserModel, prepareRequiredHttpParams } from "@app/shared/utils";
import { IApplicationResponse } from "@app/shared/utils/interfaces.utils";
import { loggedInUserData } from "@modules/others";
import { UserModuleApiSuffixModels } from "@modules/user/shared/utils/user-module.enums";
import { select, Store } from "@ngrx/store";
import { combineLatest, Observable } from "rxjs";
import { map } from "rxjs/operators";
@Component({
  selector: "[app-sidebar]",
  templateUrl: "./app-sidebar.component.html"
})
export class AppSidebar {
  applicationResponse: IApplicationResponse;
  loggedInUserData: LoggedInUserModel;
  childMenus = [];
  parentBasedMenus = [];
  //applicationResponse: IApplicationResponse;
  accessibleRoutes$: Observable<any>;

  constructor(private store: Store<AppState>, private preloadOnDemandService: OnDemandPreloadService, private router: Router,
    private crudService: CrudService, private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(moduleBasedComponentPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.parentBasedMenus = response[1];
    });
  }

  ngOnInit() {
    this.accessibleRoutes$ = this.store.pipe(select(sidebarDataSelector$)).pipe(map((sidebarData)=>sidebarData?.['modules']));
  }

  async onSidebarModuleSectionClicked(selectedModuleObj, selectedSidebarType: string) {
     let selectedModule = { ...selectedModuleObj };
  //   var mName = selectedModuleObj['navigationName']?selectedModuleObj['navigationName']:selectedModuleObj['moduleName'];
  //   const response = await this.store.pipe(select(currentComponentPageBasedPermissionsSelector$),
  //   take(1)).toPromise();
  // //   // return;
  // //  this.store.select(currentComponentPageBasedPermissionsSelector$).subscribe((res :any)=>{
  //   if(!response[mName] && mName){
  //     let _data = {};
  //     _data[mName] = []
  //     _data = _data = {...response,..._data }

    // preload lazy loaded modules based on sidebar clicks
    switch (selectedSidebarType) {
      case "firstlevelHeader":
        //this.getChildrenPageLevelPermissionsByTopLevelParentId(selectedModule);
        let moduleName = selectedModule?.moduleName.toLowerCase();
        let preloadTopLevelModuleName;
        switch (true) {
          case moduleName.includes('customer'):
            break;
          case moduleName.includes('technical management'):
            preloadTopLevelModuleName = 'technical-management';
            break;
          case moduleName.includes('it management'):
            preloadTopLevelModuleName = 'user';
            break;
        }
        this.preloadOnDemandService.startPreload(preloadTopLevelModuleName);
        setTimeout(() => {
          selectedModule.navigations.forEach((childNavigationObj) => {
            if (childNavigationObj?.navigationUrl && childNavigationObj?.navigations?.length == 0) {
              let lazyLoadUrl = childNavigationObj.navigationUrl.split('/')[2];
              this.preloadOnDemandService.startPreload(lazyLoadUrl);
            }
          });
        }, 200);
        break;
      case "firstlevelChild":
        break;
      case "secondlevelHeader":
        selectedModule.navigations.forEach((nestedChildNavigationObj) => {
          if (nestedChildNavigationObj?.navigationUrl && (!nestedChildNavigationObj.navigations || nestedChildNavigationObj?.navigations?.length == 0)) {
            let lazyLoadUrl = nestedChildNavigationObj.navigationUrl.split('/')[2];
            this.preloadOnDemandService.startPreload(lazyLoadUrl);
          }
        });
        break;
      case "secondlevelChild":
        break;
    }
    // if (selectedModule.navigationName == "Sales" || selectedModule.navigationName == "Technical") {
    //   selectedModule.parentNavigationId = "62a2c7ce-2719-4f10-98a1-3ce8afcebe80";
    // }
    // else if (selectedModule.navigationName == "City") {
    //   selectedModule.parentNavigationId = "50e22420-e7d4-4047-ab33-eb703f23dbb0";
    // }
  //   if (selectedSidebarType.toLowerCase().includes('child')) {
  //     let currentComponentPageBasedPermissions, filteredCurrentComponentPageBasedPermissionsObj;
  //     filteredCurrentComponentPageBasedPermissionsObj = this.prepareCurrentComponentBasedPermissions(this.parentBasedMenus, selectedModule.parentNavigationId, 'subMenu');
  //     currentComponentPageBasedPermissions = filteredCurrentComponentPageBasedPermissionsObj?.['subMenu'].
  //       filter(f => f['menuId'] == selectedModule.navigationId).map(res => res.subMenu);
  //     if (!filteredCurrentComponentPageBasedPermissionsObj) {
  //       currentComponentPageBasedPermissions = this.parentBasedMenus.filter(pM => pM['menuId'] == selectedModule.navigationId).map(res => res.subMenu);
  //     }
  //     else if (filteredCurrentComponentPageBasedPermissionsObj.subMenu.length > 0) {
  //       if (currentComponentPageBasedPermissions[0].find(sM => sM['menuCategoryId'] == MenuCategoriesForPermissions.MENU)) {
  //         debugger
  //       }
  //     }
  //     _data[`${mName}`]= currentComponentPageBasedPermissions[0];
  //     this.store.dispatch(new PageBasedPermissionsLoaded({
  //       moduleBasedComponentPermissions: this.parentBasedMenus,
  //       currentComponentPageBasedPermissions: _data
  //     }));
  //   }
  // }else{
  // }
  // })
  }

  getChildrenPageLevelPermissionsByTopLevelParentId(selectedModule) {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.MENU_CHILD, undefined, false,
      prepareRequiredHttpParams({
        menuId: selectedModule.moduleId ? selectedModule.moduleId : selectedModule.navigationId,
        roleId: this.loggedInUserData.roleId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.parentBasedMenus = response.resources;
          this.store.dispatch(new PageBasedPermissionsLoaded({
            moduleBasedComponentPermissions: response.resources,
            currentComponentPageBasedPermissions: []
          }));
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  // nested array using recursion
  prepareCurrentComponentBasedPermissions(parentBasedMenus, parentNavigationId, nestingKey): [] {
    return parentBasedMenus.reduce((parentBasedMenu, item) => {
      if (parentBasedMenu) {
        return parentBasedMenu;
      };
      if (item.menuId === parentNavigationId) return item;
      if (item[nestingKey]) {
        return this.prepareCurrentComponentBasedPermissions(item[nestingKey], parentNavigationId, nestingKey)
      }
    }, null);
  }
}
