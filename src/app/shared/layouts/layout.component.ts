import { Component, OnInit } from '@angular/core';
import { Helpers } from '../../helpers';
import { MatDialog } from '@angular/material';
import { Store, select } from '@ngrx/store';
import { AppState } from '@app/reducers';
import {RxjsService} from '@app/shared/services';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, agentLoginDataSelector, } from '@app/shared';

declare var $: any

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  setindex: number = 0;
  agentExtensionNo: any;
  openScapeOpen = false;
  openChat = false;

  constructor(private dialog: MatDialog,
    private store: Store<AppState>,
    private rxjs: RxjsService
    ) {
  }

  ngOnInit() {
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });

    // this.rxjs.getExpandOpenScape().subscribe((open:boolean)=>{
    //   this.openScapeOpen=open;
    // })

    //  this.scheduledCallback();
  }

  ngAfterViewInit() {

    // initialize layout: handlers, menu ...
    Helpers.initLayout();


  }

  toggleQuickAction() {

    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
  }

  gotoTop() {}

  onClickChatNotification() {
    this.openChat = true;
  }

}
