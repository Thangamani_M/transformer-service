import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { LayoutActionTypes, SidebarDataLoaded } from '@app/shared/layouts';
import { tap } from 'rxjs/operators';
import { PageBasedPermissionsLoaded, PageBasedPermissionsRemoved, SidebarDataRemoved } from './layout.actions';
import { defer, of } from 'rxjs';
import { UserService } from '@app/shared/services';

@Injectable()
export class LayoutEffectEffects {

  @Effect({ dispatch: false })
  sidebarDataLoaded$ = this.actions$.pipe(
    ofType<SidebarDataLoaded>(LayoutActionTypes.SidebarDataLoadedAction),
    tap(action =>
      this.userService.sidebarMenuData = action.payload.response)
  );

  @Effect({ dispatch: false })
  removeSidebarData$ = this.actions$.pipe(
    ofType<SidebarDataRemoved>(LayoutActionTypes.SidebarDataRemovedAction),
    tap(() =>
      this.userService.sidebarMenuData = null
    )
  );

  @Effect()
  init$ = defer(() => {

    const sidebarData = this.userService.sidebarMenuData;

    if (sidebarData) {
      return of(new SidebarDataLoaded({ response: sidebarData }));
    }
    else {
      return <any>of(new SidebarDataRemoved());
    }

  });
  @Effect({ dispatch: false })
  LoadPageBasedPermissionsData$ = this.actions$.pipe(
    ofType<PageBasedPermissionsLoaded>(LayoutActionTypes.PageBasedPermissionsCreateAction),
      tap(action => {
        this.userService.moduleBasedComponentPermissions = action.payload.moduleBasedComponentPermissions;
        this.userService.currentComponentPageBasedPermissions = action.payload.currentComponentPageBasedPermissions;
      })
  );

  @Effect({ dispatch: false })
  RemovePageBasedPermissionsData$ = this.actions$.pipe(
    ofType<PageBasedPermissionsRemoved>(LayoutActionTypes.PageBasedPermissionsDeleteAction),
    tap(() => {
      this.userService.moduleBasedComponentPermissions = [];
      this.userService.currentComponentPageBasedPermissions = [];
    })
  );

  @Effect()
  initPageBasedPermissions$ = defer(() => {
    const moduleBasedComponentPermissions = this.userService.moduleBasedComponentPermissions;
    const currentComponentPageBasedPermissions = this.userService.currentComponentPageBasedPermissions;

    if (moduleBasedComponentPermissions) {
      return of(new PageBasedPermissionsLoaded({ moduleBasedComponentPermissions,currentComponentPageBasedPermissions }));
    }
    else {
      return <any>of(new PageBasedPermissionsRemoved());
    }
  });

  constructor(private actions$: Actions, private userService: UserService) {}
}
