import { Component, Input, OnInit } from '@angular/core';
import { RxjsService } from '@app/shared/services/rxjs.services';

@Component({
  selector: 'app-page-spinner',
  templateUrl: './page-spinner.component.html',
  styleUrls: ['./page-spinner.component.scss']
})
export class PageSpinnerComponent implements OnInit {

  @Input()isLoading = false;

  constructor(private rxjsService: RxjsService) {
  }

  ngOnInit(): void {
    this.rxjsService.getGlobalLoaderProperty().subscribe((isLoading) => {
      setTimeout(() => {
        this.isLoading = isLoading;
      }, 50)
    })
  }

}
