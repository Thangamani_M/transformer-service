import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-app-more-advanced-search',
  templateUrl: './app-more-advanced-search.component.html',
  styleUrls: ['./app-more-advanced-search.component.scss']
})
export class AppMoreAdvancedSearchComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  toggleSearchAction() {
    $('.search-action-sidebar').addClass('hide-search-action-menu');
  }
}
