import { Action } from '@ngrx/store';

export enum LayoutActionTypes {
  SidebarDataRequestedAction = '[Layout Sidebar] Data Requested',
  SidebarDataLoadedAction = '[Layout Sidebar] Data Loaded',
  SidebarDataRemovedAction = '[Layout Sidebar] Data Removed',
  PageBasedPermissionsCreateAction = '[Layout Page Based Permission] Data Creation',
  PageBasedPermissionsDeleteAction = '[Layout Page Based Permission] Data Deletion',
}
export class SidebarDataRequested implements Action {
  readonly type = LayoutActionTypes.SidebarDataRequestedAction;
  constructor(public payload: {}) { }
}
export class SidebarDataLoaded implements Action {
  readonly type = LayoutActionTypes.SidebarDataLoadedAction;
  constructor(public payload: { response: any }) { }
}
export class SidebarDataRemoved implements Action {
  readonly type = LayoutActionTypes.SidebarDataRemovedAction;
}
export class PageBasedPermissionsLoaded implements Action {
  readonly type = LayoutActionTypes.PageBasedPermissionsCreateAction;
  constructor(public payload: { moduleBasedComponentPermissions, currentComponentPageBasedPermissions }) {
  }
}
export class PageBasedPermissionsRemoved implements Action {
  readonly type = LayoutActionTypes.PageBasedPermissionsDeleteAction;
}

export type LayoutActions = SidebarDataRequested | SidebarDataLoaded | SidebarDataRemoved |
  PageBasedPermissionsLoaded | PageBasedPermissionsRemoved;

