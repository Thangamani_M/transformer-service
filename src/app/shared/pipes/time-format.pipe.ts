import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  // transform(value: unknown, ...args: unknown[]): unknown {
  //   return null;
  // }

  transform(val: string, format?: any | string): unknown {
    if(val && !format) {
      return moment(val, "HH:mm:ss").format("hh:mm A");
    } else if(val && format) {
      return moment(val, "HH:mm:ss").format(format);
    }
    return val;
  }

}
