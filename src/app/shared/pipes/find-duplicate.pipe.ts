import { Pipe, PipeTransform } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {
  defaultCountryCode
} from '@app/shared';

@Pipe({ name: 'findDuplicate' })
export class FindDuplicatePipe implements PipeTransform {
  transform(formControlValue: string, formGroup: FormGroup, formControlName: string, countryCode = defaultCountryCode): boolean {
    if (!formControlValue) return;
    formControlValue = formControlValue.toString();
    // formControlValue=formControlValue.split(" ").join("")
    if ((countryCode == '+27' && formControlValue.length === 11) || (countryCode !== '+27' && formControlValue.length === 12)) {
      switch (formControlName) {
        case "mobile1":
          if (formGroup.get('mobile2CountryCode')) {
            return (formControlValue !== '' && ((formGroup.get('mobile2CountryCode').value == '+27' &&
              formControlValue == formGroup.get('mobile2').value) || formControlValue == formGroup.get('officeNo').value ||
              formControlValue == formGroup.get('premisesNo').value));
          }
          else {
            return (formControlValue !== '' && ((formGroup.get('officeNoCountryCode').value == '+27' &&
              formControlValue == formGroup.get('officeNo').value)));
          }
        case "mobile2":
          return (formControlValue !== '' && (formControlValue == formGroup.get('mobile1').value || formControlValue == formGroup.get('officeNo').value ||
            formControlValue == formGroup.get('premisesNo').value));
        case "officeNo":
          if (formGroup.get('mobile2CountryCode')) {
            return (formControlValue !== '' && ((formGroup.get('mobile2CountryCode').value == '+27' &&
              formControlValue == formGroup.get('mobile2').value) || formControlValue == formGroup.get('mobile1').value ||
              formControlValue == formGroup.get('premisesNo').value));
          }
          else {
            return (formControlValue !== '' && ((formGroup.get('officeNoCountryCode').value == '+27' &&
              formControlValue == formGroup.get('mobile1').value)));
          }
        case "premisesNo":
          if (formGroup.get('mobile2CountryCode')) {
            return (formControlValue !== '' && ((formGroup.get('mobile2CountryCode').value == '+27' &&
              formControlValue == formGroup.get('mobile2').value) || formControlValue == formGroup.get('mobile1').value ||
              formControlValue == formGroup.get('officeNo').value));
          }
          else {
            return (formControlValue !== '' && ((formGroup.get('officeNoCountryCode').value == '+27' &&
              formControlValue == formGroup.get('mobile1').value)));
          }
        case "mobileNo":
            if (formGroup.get('mobileNoCountryCode')) {
              return (formControlValue !== '' && ((formGroup.get('mobileNoCountryCode').value == '+27' &&
               formControlValue == formGroup.get('contactNumber').value)));
            }
            else {
              return (formControlValue !== '' && ((formGroup.get('mobileNoCountryCode').value == '+27' &&
                formControlValue == formGroup.get('mobileNo').value)));
            }
        case "contactNumber":
              if (formGroup.get('mobileNoCountryCode')) {
                return (formControlValue !== '' && ((formGroup.get('contactNumberCountryCode').value == '+27' &&
                 formControlValue == formGroup.get('mobileNo').value)));
              }
              else {
                return (formControlValue !== '' && ((formGroup.get('contactNumberCountryCode').value == '+27' &&
                  formControlValue == formGroup.get('contactNumber').value)));
              }
      }
    }
    return false;
  }
}
