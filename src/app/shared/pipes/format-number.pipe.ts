import { DecimalPipe } from '@angular/common';

import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'formatNumber' })
export class FormatNumberPipe implements PipeTransform {

    constructor(private decimalPipe: DecimalPipe) {}

    transform(num: any) {
        // if(!num) return 'R '+this.decimalPipe.transform(num, '.2', 'en-GB').toString().replace(/,/g, " ");
        // num = +num;
        if (num?.toString()?.indexOf('.') !== -1 && num && num?.toString()?.indexOf('R') == -1) {
            return 'R '+this.decimalPipe.transform(num, '.2', 'en-GB').toString().replace(/,/g, " ");
        }
        else if(num?.toString() && num?.toString()?.indexOf('R') == -1) {
            return 'R '+this.decimalPipe.transform(num, '.2', 'en-GB').toString().replace(/,/g, " ");
        } else {
            return num;
        }
    }
}