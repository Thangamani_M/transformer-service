import { DatePipe } from "@angular/common";
import { Inject, LOCALE_ID, Pipe, PipeTransform } from "@angular/core";

export enum DATE_FORMAT_TYPES {
    IS_DATE = 'isDate',
    IS_DATE_AND_TIME = 'isDateTime',
    IS_TIME = 'isTime',
    IS_RAILWAYS_DATE_TIME = 'isDateRailwayTime',
    IS_DATE_WITH_TWELVE_HOURS_FORMAT = 'isDateWithTwelveHoursFormat',
    IS_TIME_WITH_TWELVE_HOURS_FORMAT = 'isTimeWithTwelveHoursFormat',
}
@Pipe({ name: 'date', pure: true })
export class CustomDatePipe extends DatePipe implements PipeTransform {
    constructor(@Inject(LOCALE_ID) locale: string) {
        super(locale);
    }

    transform(
        value: Date | string | number | null | undefined, datePipeArgs?): string | null {
        if (!value) return;
        // If the datePipe transform is used in ts files to transform property value then create a server request ( only for format changing in the actual value )
        if (value instanceof Date) {
            return super.transform(value, datePipeArgs);
        }
        // This else part is allowed to show the date format as mentioned in the global date configuration ( only for showing purposes )
        else {
            return this.prepareDateAndOrTimeFormatToBeShown(value, datePipeArgs);
        }
    }

    prepareDateAndOrTimeFormatToBeShown(value, datePipeArgs: string): string {
        switch (datePipeArgs) {
            case DATE_FORMAT_TYPES.IS_DATE:
                return super.transform(value, this.adjustDateFormatAsPerPCalendar());
            case DATE_FORMAT_TYPES.IS_DATE_AND_TIME:
                return super.transform(value, this.adjustDateFormatAsPerPCalendar() + ' HH:mm:ss');
            case DATE_FORMAT_TYPES.IS_TIME:
                return super.transform(value, 'HH:mm:ss');
            case DATE_FORMAT_TYPES.IS_RAILWAYS_DATE_TIME:
                break;
            case DATE_FORMAT_TYPES.IS_DATE_WITH_TWELVE_HOURS_FORMAT:
                return super.transform(value, this.adjustDateFormatAsPerPCalendar() + ' h:mm:ss a');
            case DATE_FORMAT_TYPES.IS_TIME_WITH_TWELVE_HOURS_FORMAT:
                return super.transform(value, 'h:mm:ss a');
            default:
                if (datePipeArgs == 'HH:mm:ss') {
                    return super.transform(value, datePipeArgs);
                }
                // Split up the time from the iso date string format to make sure to show whether date only or both the date and time
                let index = value.toString().indexOf('T') + 1;
                let subString = value.toString().substring(index);
                if (subString !== '00:00:00') {
                    return super.transform(value, this.adjustDateFormatAsPerPCalendar() + ' HH:mm:ss');
                }
                else if (subString === '00:00:00') {
                    return super.transform(value, this.adjustDateFormatAsPerPCalendar());
                }
        }
    }

    adjustDateFormatAsPerPCalendar(): string {
        return localStorage.getItem('globalDateFormat');
    }
}