import { Pipe, PipeTransform } from '@angular/core';
import { phoneMask } from '..';
@Pipe({ name: 'phoneMask' })
export class PhoneMaskPipe implements PipeTransform {
  transform(value: string, prefix?: number): string {
    if (!value) return;
    value=value.toString();
    let splittedValueArray = value.split(' ');
    if (splittedValueArray.length == 2) {
      value = splittedValueArray[1];
      return splittedValueArray[0]+" "+phoneMask(value, prefix);
    }
    else if (splittedValueArray.length == 3) {
      value = splittedValueArray[2];
      return splittedValueArray[1]+" "+phoneMask(value, prefix);
    }
    else{
      return phoneMask(value, prefix);
    }
  }
}
