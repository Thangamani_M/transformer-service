import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Optional, Output, QueryList, Self, ViewChild, ViewChildren } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormControl, NgControl, Validators } from '@angular/forms';
import { CustomDirectiveConfig } from '@app/shared/models';
import { convertCamelCasesToSpaces, formConfigs, saidPattern } from '@app/shared/utils';

const defaultConfig = {
  classNames: 'form-control fidelity-form-input',
  areMinLengthRequired: true,
  mask: null,
  index: '',
  type: 'text',
  shouldRequireFocus: false,
  isFocus: false,
  autocomplete: 'off',
  accessKey: ""
}
@Component({
  selector: 'input-custom-control',
  templateUrl: './input-custom-control.component.html',
  styleUrls: ['./input-custom-control.component.scss']
})

export class InputCustomControlComponent implements ControlValueAccessor {
  @ViewChild('input', { static: true }) inputSelector: ElementRef<any>;
  ngControlName: string;
  convertCamelCasesToSpaces = convertCamelCasesToSpaces;
  @Input() validateInputConfig;
  @Input() placeholder;
  @Input() autocomplete;
  @Input() minlength;
  @Input() maxlength;
  @Input() areMinLengthRequired = defaultConfig.areMinLengthRequired;
  @Input() name;
  @Input() value;
  @Input() index = defaultConfig.index;
  @Input() mask = defaultConfig.mask;
  @Input() classNames = defaultConfig.classNames;
  @Input() extraClassNames;
  @Input() type = defaultConfig.type;
  @Input() isFocus = defaultConfig.isFocus;
  @Input() shouldRequireFocus = defaultConfig.shouldRequireFocus;
  @Input() customErrorMessageForMinLength = defaultConfig.accessKey;
  @Output() blur: EventEmitter<any> = new EventEmitter();
  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() input: EventEmitter<any> = new EventEmitter();
  @Output() keyup: EventEmitter<any> = new EventEmitter();
  @Output() keypress: EventEmitter<any> = new EventEmitter();
  @Output() keydown: EventEmitter<any> = new EventEmitter();
  @ViewChildren('inputField') rows: QueryList<any>;

  tempNgControl: AbstractControl;
  tempNgControlName: string;

  onRegisterChange = () => { };
  onRegisterTouched = () => { };

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    private cdr: ChangeDetectorRef
  ) {
    if (ngControl != null) {
      ngControl.valueAccessor = this;
    }
    else {
      this.tempNgControl = new FormControl('');
      this.tempNgControlName = "randomName-" + Math.floor(Math.random() * 22);
      this.validateInputConfig = new CustomDirectiveConfig();
    }
  }

  ngOnInit() {
    if (!this.ngControl) return;
    if (this.extraClassNames && this.extraClassNames == 'disabled') {
      this.minlength = 0;
      this.maxlength = formConfigs.maxLengthFiveHundred;
    }
    else {
      this.getInputConfigs();
    }
    this.autocomplete = this.autocomplete ? this.autocomplete : defaultConfig.autocomplete;
    this.prepareErrorMessageAttributeBasedOnInputFields();
  }

  prepareErrorMessageAttributeBasedOnInputFields() {
    const controlName = this.ngControl.name.toLowerCase();
    switch (true) {
      case (controlName.includes('postal') || controlName.includes('zip') || controlName.includes('said')):
        this.customErrorMessageForMinLength = 'minimumLengthIsRequiredLength';
        break;

    }
  }

  ngOnChanges() {
    if (!this.ngControl) {
      this.tempNgControl = new FormControl(this.value);
      return;
    };
    // if (this.shouldRequireFocus) {
    //   this.inputSelector.nativeElement.focus();
    //   this.inputSelector.nativeElement.blur();
    // }
    this.getInputConfigs();
    if (this.isFocus) {
      this.focusInAndOutFormArrayFields();
    }
  }

  focusInAndOutFormArrayFields() {
    if (!this.rows) return;
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  prepareConfigForCompanyRegNumberBasedInputFields() {
    this.validateInputConfig = new CustomDirectiveConfig();
    if (this.areMinLengthRequired) {
      this.minlength = this.minlength ? this.minlength : formConfigs.two;
    }
    this.maxlength = this.maxlength ? this.maxlength : formConfigs.sixteenDigits;
  }

  getInputConfigs() {
    const formControlName = this.ngControl.name.toLowerCase();
    switch (true) {
      case (formControlName.includes('contactnumber') || formControlName.includes('telephonenumber') ||
        formControlName.includes('phonenumber') || formControlName.includes('mobilenumber') || formControlName.includes('officenumber') ||
        formControlName.includes('homenumber') || formControlName.includes('worknumber') || formControlName.includes('said') ||
        formControlName.includes('accountnumber') || formControlName.includes('vat') || formControlName.includes('vatnumber') ||
        formControlName.includes('buildingnumber') || formControlName.includes('streetnumber') || formControlName.includes('noof') ||
        formControlName.includes('smsnumber') || formControlName.includes('companyregno') || formControlName.includes('no') ||
        formControlName.includes('level') || formControlName.includes('escalationmin')):
        this.prepareConfigForNumberBasedInputFields(formControlName);
        break;
      case (formControlName.includes('name') || formControlName.includes('person')):
        this.prepareConfigForNameBasedInputFields(formControlName);
        break;
      case (formControlName.includes('password') || formControlName.includes('distressword')):
        this.prepareConfigForPasswordBasedInputFields(formControlName);
        break;
      default:
        this.prepareConfigForOtherInputFields(formControlName);
        break;
    }
  }

  prepareConfigForNameBasedInputFields(formControlName: string) {
    this.validateInputConfig = this.validateInputConfig ? this.validateInputConfig : new CustomDirectiveConfig();
    if (this.areMinLengthRequired) {
      this.maxlength = this.maxlength ? this.maxlength : formConfigs.generalNameLengthFiftyLength;
    }
    switch (true) {
      case formControlName.includes('buildingname'):
        this.validateInputConfig = new CustomDirectiveConfig();
        break;
      default:
        break;
    }
  }

  prepareConfigForPasswordBasedInputFields(formControlName: string) {
    this.validateInputConfig = this.validateInputConfig ? this.validateInputConfig : new CustomDirectiveConfig();
    if (this.areMinLengthRequired) {
      this.minlength = this.minlength ? this.minlength : formConfigs.four;
    }
    this.maxlength = this.maxlength ? this.maxlength : formConfigs.maxLengthFifteenForPassword;
  }

  prepareConfigForSaidNumberBasedInputFields() {
    this.validateInputConfig = this.validateInputConfig ? this.validateInputConfig : new CustomDirectiveConfig({ isANumberOnly: true });
    this.ngControl.control.setValidators(this.ngControl.control?.errors?.required ? [Validators.required, saidPattern] : [saidPattern]);
    if (this.areMinLengthRequired) {
      this.minlength = this.minlength ? this.minlength : formConfigs.thirteenLength;
    }
    this.maxlength = this.maxlength ? this.maxlength : formConfigs.thirteenLength;
  }

  prepareConfigForVATNumberBasedInputFields() {
    if (this.areMinLengthRequired) {
      this.minlength = this.minlength ? this.minlength : formConfigs.eleven;
    }
    else {
      this.maxlength = this.maxlength ? this.maxlength : formConfigs.eleven;
    }
  }

  prepareConfigForAccountNumberBasedInputFields() {
    if (this.areMinLengthRequired) {
      this.minlength = this.minlength ? this.minlength : formConfigs.eight;
    }
    this.maxlength = this.maxlength ? this.maxlength : formConfigs.sixteenDigits;
  }

  prepareConfigForNumberBasedInputFields(controlName: string) {
    this.validateInputConfig = this.validateInputConfig ? this.validateInputConfig : new CustomDirectiveConfig({ isANumberOnly: true });
    switch (true) {
      case (controlName.includes('buildingnumber') || controlName.includes('streetnumber') || controlName.includes('buildingno') ||
        controlName.includes('streetno')):
        this.minlength = this.minlength ? this.minlength : formConfigs.one;
        this.maxlength = this.maxlength ? this.maxlength : formConfigs.ten;
        this.validateInputConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
        break;
      case controlName.includes('passport'):
        this.minlength = this.minlength ? this.minlength : formConfigs.one;
        this.maxlength = this.maxlength ? this.maxlength : formConfigs.maxLengthTwenty;
        this.validateInputConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
        break;
      case controlName.includes('companyregno'):
        this.prepareConfigForCompanyRegNumberBasedInputFields();
        break;
      case controlName.includes('customerrefno'):
        this.validateInputConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
        break;
      case controlName.includes('said'):
        this.prepareConfigForSaidNumberBasedInputFields();
        break;
      case (controlName.includes('accountnumber') || controlName.includes('accountno')):
        this.prepareConfigForAccountNumberBasedInputFields();
        break;
      case (controlName.includes('no')):
        if (controlName.includes('ref')) {
          this.validateInputConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
          this.minlength = this.minlength ? this.minlength : formConfigs.one;
          this.maxlength = this.maxlength ? this.maxlength : formConfigs.maxLengthTwenty;
        }
        else {
          this.minlength = this.minlength ? this.minlength : formConfigs.one;
          this.maxlength = this.maxlength ? this.maxlength : formConfigs.ten;
        }
        break;
      case (controlName.includes('vat') || controlName.includes('vatnumber')):
        this.prepareConfigForVATNumberBasedInputFields();
        break;
      case controlName.includes('noof'):
        if (this.areMinLengthRequired) {
          this.maxlength = this.maxlength ? this.maxlength : formConfigs.four;
        }
        break;
      case (controlName.includes('level') || controlName.includes('escalationmin')):
        this.minlength = this.minlength ? this.minlength : formConfigs.one;
        this.maxlength = this.maxlength ? this.maxlength : formConfigs.three;
        break;
      default:
        this.validateInputConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
        if (this.areMinLengthRequired) {
          this.minlength = this.minlength ? this.minlength : formConfigs.contactnumberMinLength;
        }
        this.maxlength = this.maxlength ? this.maxlength : formConfigs.contactnumberMaxLength;
        break;
    }
  }

  prepareConfigForOtherInputFields(controlName: string) {
    switch (true) {
      case (controlName.includes('postal') || controlName.includes('zip')):
        this.validateInputConfig = this.validateInputConfig ? this.validateInputConfig : new CustomDirectiveConfig({ isANumberOnly: true });
        if (this.areMinLengthRequired) {
          this.minlength = this.minlength ? this.minlength : formConfigs.postalCodeMaxLength;
        }
        this.maxlength = this.maxlength ? this.maxlength : formConfigs.postalCodeMaxLength;
        break;
      case controlName.includes('sortorder'):
        this.validateInputConfig = this.validateInputConfig ? this.validateInputConfig : new CustomDirectiveConfig({ isANumberOnly: true });
        break;
      case (controlName.includes('email') || controlName.includes('username')):
        break;
      default:
        this.validateInputConfig = this.validateInputConfig ? this.validateInputConfig : new CustomDirectiveConfig();
        break;
    }
  }

  writeValue(obj: any) {
  }

  registerOnChange(fn: any) {
    this.onRegisterChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onRegisterTouched = fn;
  }

  setDisabledState?(isDisabled: boolean) {
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }
}