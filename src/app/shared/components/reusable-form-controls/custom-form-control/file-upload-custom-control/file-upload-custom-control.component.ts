import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Optional, Output, Self, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NgControl } from '@angular/forms';
import { ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS, DynamicConfirmByMessageConfirmationType, MAX_FILES_UPLOAD_COUNT, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { ReusablePrimeNGTableFeatureService } from '../../../../../shared/services/reusable-primeng-table-features.service';

const defaultConfig = {
  uploadedFiles: [],
  isASingleFileUpload: true,
  label: 'Upload File',
  firstDivClass: 'col-12',
  extraFirstDivClassNames: '',
  secondDivClass: 'pb-2',
  extraSecondDivClassNames: '',
  placeholder: 'Upload file',
  name: 'File Upload',
  shouldDownloadFileOnClick: false,
  downloadablePropertyName: ''
}
@Component({
  selector: 'file-upload-custom-control',
  templateUrl: './file-upload-custom-control.component.html'
})

export class FileUploadCustomControlCustomControlComponent implements ControlValueAccessor {
  @Input() isASingleFileUpload = defaultConfig.isASingleFileUpload;
  @Input() acceptableFileExtensions;
  @Input() label = defaultConfig.label;
  @Input() placeholder = defaultConfig.placeholder;
  @Input() name = defaultConfig.name;
  @Input() shouldDownloadFileOnClick = defaultConfig.shouldDownloadFileOnClick;
  @Input() downloadablePropertyName = defaultConfig.downloadablePropertyName;
  @Input() uploadedFiles = defaultConfig.uploadedFiles;
  @Input() firstDivClass = defaultConfig.firstDivClass;
  @Input() extraFirstDivClassNames = defaultConfig.extraFirstDivClassNames;
  @Input() secondDivClass = defaultConfig.secondDivClass;
  @Input() extraSecondDivClassNames = defaultConfig.extraSecondDivClassNames;
  @Input() formGroup: FormGroup;
  @Output() emitUploadedFiles = new EventEmitter();

  selectedFiles = [];
  tempNgControl;
  pscrollbarHeightStyle = { height: '7vh' };
  @ViewChild('fileUploadInput', null) fileUploadInput: ElementRef;


  constructor(
    @Self() @Optional() public ngControl: NgControl,
    private cdr: ChangeDetectorRef,
    private snackbarService: SnackbarService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService
  ) {
    if (ngControl != null) {
      ngControl.valueAccessor = this;
    }
    else {
      this.tempNgControl = new FormControl('');
    }
  }

  ngOnInit() {
    this.uploadedFiles=[];
    this.acceptableFileExtensions = this.acceptableFileExtensions || ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS;
    // this.formGroup?.get(this.ngControl.name).valueChanges.subscribe((uploadedFile) => {
    //   if (!uploadedFile) {
    //     this.ngControl.control.setValue("",{emitEvent:false,onlySelf:true});
    //     this.ngControl.control.updateValueAndValidity({ emitEvent: false, onlySelf: true });
    //     this.formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
    //     this.fileUploadInput.nativeElement.value = "";
    //   }
    // });
  }

  uploadFile(uploadedFile) {
    if (uploadedFile && uploadedFile.length == 0) {
      return;
    }
    const path = uploadedFile[0].name.split('.');
    const extension = "." + path[path.length - 1];
    const duplicatedFile = this.uploadedFiles.find(name => name == uploadedFile[0].name);
    if (uploadedFile && this.isASingleFileUpload == false) {
      if (!this.acceptableFileExtensions.includes(extension)) {
        this.snackbarService.openSnackbar(`Allow to upload the following file formats only - ${this.acceptableFileExtensions}`, ResponseMessageTypes.WARNING);
        return;
      }
      else if (duplicatedFile) {
        this.snackbarService.openSnackbar("File name already exists.Please upload another file.", ResponseMessageTypes.WARNING);
        return;
      }
      else if (uploadedFile.length > MAX_FILES_UPLOAD_COUNT || this.uploadedFiles.length >= MAX_FILES_UPLOAD_COUNT) {
        this.snackbarService.openSnackbar(`You can upload maximum ${MAX_FILES_UPLOAD_COUNT} files`, ResponseMessageTypes.WARNING);
        return;
      }
      else if (this.acceptableFileExtensions.includes(extension)) {
        this.selectedFiles.push(uploadedFile.item(0));
        this.uploadedFiles.push(uploadedFile[0].name);
        if (this.uploadedFiles.length > 2) {
          this.pscrollbarHeightStyle = { height: '15vh' };
        }
      }
      this.placeholder = this.uploadedFiles.join(', ');
    }
    else if (this.isASingleFileUpload) {
      if (!this.acceptableFileExtensions.includes(extension)) {
        this.snackbarService.openSnackbar(`Allow to upload the following file formats only - ${this.acceptableFileExtensions}`, ResponseMessageTypes.WARNING);
        return;
      }
      this.placeholder = uploadedFile[0].name;
      this.emitUploadedFiles.emit(uploadedFile[0]);
    }
    this.emitUploadedFiles.emit(this.selectedFiles);
  }

  removeUploadedFile(index: number) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this file?`, undefined,
      DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          this.uploadedFiles.splice(index, 1);
          this.selectedFiles.splice(index, 1);
          if (this.uploadedFiles.length > 0) {
            this.placeholder = this.uploadedFiles.join(', ');
          }
          else {
            this.placeholder = defaultConfig.placeholder;
          }
          this.emitUploadedFiles.emit(this.selectedFiles);
        }
      });
  }

  downloadFile(uploadedFile?) {
    if (this.shouldDownloadFileOnClick) {
      if (this.ngControl.value.includes('https')) {
        window.open(this.ngControl.value, '_blank');
      }
      else {
        window.open(uploadedFile[this.downloadablePropertyName], '_blank');
      }
    }
  }

  writeValue(obj: any): void {
  }

  registerOnChange(fn: any): void {

  }

  registerOnTouched(fn: any): void {

  }

  setDisabledState?(isDisabled: boolean): void {

  }

  ngAfterContentInit() {
    this.cdr.detectChanges();
  }
}