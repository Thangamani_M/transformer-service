import { Component, Input, Self, ChangeDetectorRef, Optional, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NgControl, FormControl, AbstractControl } from '@angular/forms';
import { formConfigs, convertCamelCasesToSpaces } from '@app/shared/utils';

const defaultConfig = {
  classNames: 'form-control fidelity-form-input',
  index:'',
  rows: 3
}

@Component({
  selector: 'text-area-custom-control',
  templateUrl: './text-area-custom-control.component.html',
  styleUrls: ['./text-area-custom-control.component.scss']
})

export class TextAreaCustomControlComponent implements ControlValueAccessor {
  ngControlName: string;
  convertCamelCasesToSpaces = convertCamelCasesToSpaces;
  @Input() ngModel: string;
  @Output() ngModelChange = new EventEmitter<string>();
  @Input() placeholder;
  @Input() minlength;
  @Input() maxlength;
  @Input() name;
  @Input() value;
  @Input() rows = defaultConfig.rows;
  @Input() classNames = defaultConfig.classNames;
  @Input() extraClassNames;
  @Input() index=defaultConfig.index;
  tempNgControl: AbstractControl;
  tempNgControlName: string;
  @Output() blur: EventEmitter<any> = new EventEmitter();
  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() input: EventEmitter<any> = new EventEmitter();
  @Output() keyup: EventEmitter<any> = new EventEmitter();
  @Output() keypress: EventEmitter<any> = new EventEmitter();
  @Output() keydown: EventEmitter<any> = new EventEmitter();

  onRegisterChange = () => { };
  onRegisterTouched = () => { };

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    private cdr: ChangeDetectorRef
  ) {
    if (ngControl != null) {
      ngControl.valueAccessor = this;
    }
    else {
      this.tempNgControl = new FormControl('');
      this.tempNgControlName = "randomName-" + Math.floor(Math.random() * 22);
    }
  }

  ngOnInit() {
    if (!this.ngControl) return;
    this.getInputConfigs();
  }

  ngOnChanges() {
    if (!this.ngControl) {
      this.tempNgControl = new FormControl(this.value);
      return;
    };
    this.getInputConfigs();
  }

  getInputConfigs(): void {
    this.minlength = this.minlength ? this.minlength : formConfigs.two;
    this.maxlength = this.maxlength ? this.maxlength : formConfigs.twoFifty;
  }

  writeValue(obj: any): void {
  }

  registerOnChange(fn: any): void {
    this.onRegisterChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onRegisterTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {

  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

}