import { ChangeDetectorRef, Component, EventEmitter, Input, Optional, Output, Self } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormControl, FormGroup, NgControl } from '@angular/forms';
import { convertCamelCasesToSpaces, debounceTimeForSearchkeyword, IApplicationResponse, RxjsService } from '@app/shared';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { isUndefined } from 'util';

const defaultConfig = {
  matSelectClassNames: '',
  inputClassNames: 'form-control fidelity-form-input',
  matOptionValueProperty: 'displayName',
  matOptionViewProperty: 'displayName',
  selectedOptionSearchkeywordProperty: 'fullName',
  shouldShowSearchBtn: true,
  minLength: null,
  maxLength: null,
  customInputGroupStyle: { 'margin-top': '26px' },
  customButtonHeightStyle: { 'height': '41px' },
  shouldShowPlaceHolder: true,
  shouldShowErrors: true
}
@Component({
  selector: 'mat-select-autocomplete-custom-control',
  templateUrl: './mat-search-select-custom-control.component.html',
  styleUrls: ['./mat-search-select-custom-control.component.scss']
})

export class MatSelectCustomControlComponent implements ControlValueAccessor {
  @Input() matSelectClassNames = defaultConfig.matSelectClassNames;
  @Input() extraMatSelectClassNames;
  @Input() inputClassNames = defaultConfig.inputClassNames;
  @Input() extraInputClassNames;
  @Input() shouldShowSearchBtn = defaultConfig.shouldShowSearchBtn;
  @Input() name;
  @Input() placeholder;
  @Input() matOptionValueProperty = defaultConfig.matOptionValueProperty;
  @Input() matOptionViewProperty = defaultConfig.matOptionViewProperty;
  @Input() selectedOptionSearchkeywordProperty = defaultConfig.selectedOptionSearchkeywordProperty;
  @Input() formGroup: FormGroup;
  @Input() minLength = defaultConfig.minLength;
  @Input() maxLength = defaultConfig.maxLength;
  @Input() customInputGroupStyle = defaultConfig.customInputGroupStyle;
  @Input() customButtonHeightStyle = defaultConfig.customButtonHeightStyle;
  @Input() getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  @Input() shouldShowPlaceHolder = defaultConfig.shouldShowPlaceHolder;
  @Input() shouldShowErrors = defaultConfig.shouldShowErrors;
  @Output() emitSelectedOption = new EventEmitter();
  @Output() clearDependentFormControlValues = new EventEmitter();

  tempNgControl;
  loopableArray = [];
  loopableArrayCopy = [];
  convertCamelCasesToSpaces = convertCamelCasesToSpaces;
  selectedOption;
  isLoading: boolean
  constructor(
    @Self() @Optional() public ngControl: NgControl,
    private cdr: ChangeDetectorRef, private rxjsService: RxjsService
  ) {
    if (ngControl != null) {
      ngControl.valueAccessor = this;
    }
    else {
      this.tempNgControl = new FormControl('');
    }
  }

  onSelectedItemOption(isSelected: boolean, loopableObj: object): void {
    if (isSelected) {
      setTimeout(() => {
        const filteredArray = this.loopableArray.filter(la => la[this.matOptionValueProperty] === loopableObj[this.matOptionValueProperty]);
        if (filteredArray.length > 0) {
          this.selectedOption = filteredArray[0];
          this.ngControl.control.setValue(this.selectedOption[this.selectedOptionSearchkeywordProperty]);
          this.emitSelectedOption.emit(this.selectedOption);
        }
      });
    }
  }setE

  ngOnInit() {
    let ngFormControl: AbstractControl = this.ngControl.control;
    var searchText: string;
    this.ngControl.valueChanges.pipe(
      debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(),
      switchMap((searchKeyword) => {
        searchText = searchKeyword;
        this.loopableArray = [];
        let isSearchKeyContainsInLoopableArray = this.loopableArrayCopy.find(lA => lA[this.selectedOptionSearchkeywordProperty] == searchKeyword) ? true : false;
        if (!searchKeyword || isSearchKeyContainsInLoopableArray) {
          return of();
        }
        else if (searchKeyword.length < this.minLength) {
          this.formGroup.controls[this.ngControl.name].setErrors({
            minlength: {
              actualLength: searchKeyword.length,
              requiredLength: this.minLength
            }
          });
          this.clearDependentFormControlValues.emit();
          return of();
        }
        else if (typeof searchKeyword === "object") {
          return (this.loopableArray = []);
        } else {
          return this.getLoopableObjectRequestObservable;
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.loopableArrayCopy = response.resources;
          this.loopableArray = response.resources;
        }
        if (this.formGroup && this.shouldShowErrors) {
          if (searchText !== "" && this.loopableArray.length == 0) {
            ngFormControl.setErrors({ invalid: true });
          }
          if (searchText === "" && this.formGroup.controls[this.ngControl.name]?.errors?.hasOwnProperty('required')) {
            ngFormControl.setErrors({ required: true });
            this.clearDependentFormControlValues.emit();
          }
          else if (isUndefined(this.selectedOption) && ngFormControl.touched && searchText !== "") {
            this.selectedOption = undefined;
            ngFormControl.setErrors({ invalid: true });
            this.clearDependentFormControlValues.emit();
          }
          else if (this.selectedOptionSearchkeywordProperty && this.selectedOption && this.selectedOption?.[this.selectedOptionSearchkeywordProperty] !== searchText && searchText !== "") {
            this.selectedOption = undefined;
            ngFormControl.setErrors({ invalid: true });
            this.clearDependentFormControlValues.emit();
          }
          this.emitSelectedOption.emit(this.selectedOption);
        }
        else if (searchText === "" && this.formGroup.controls[this.ngControl.name]?.errors?.hasOwnProperty('required') && this.shouldShowErrors) {
          ngFormControl.setErrors({ required: true });
          this.clearDependentFormControlValues.emit();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  writeValue(obj: any): void {
  }

  registerOnChange(fn: any): void {

  }

  registerOnTouched(fn: any): void {

  }

  setDisabledState?(isDisabled: boolean): void {

  }

  ngAfterContentInit() {
    this.cdr.detectChanges();
  }
}