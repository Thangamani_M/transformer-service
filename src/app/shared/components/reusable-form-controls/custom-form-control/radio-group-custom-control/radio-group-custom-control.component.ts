import { Component, Input, Self, ChangeDetectorRef, Optional, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NgControl, FormControl } from '@angular/forms';

const defaultConfig = {
  classNames: 'fidelity-radio-btn',
  inputType:'boolean'
}
@Component({
  selector: 'radio-group-custom-control',
  templateUrl: './radio-group-custom-control.component.html',
  styleUrls: ['./radio-group-custom-control.component.scss'],
})

export class RadioGroupCustomControlComponent implements ControlValueAccessor {

  @Input() loopableArray;
  @Input() classNames = defaultConfig.classNames;
  @Input() extraClassNames;
  @Input() inputType=defaultConfig.inputType;
  tempNgControl;

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    private cdr: ChangeDetectorRef
  ) {
    if (ngControl != null) {
      ngControl.valueAccessor = this;
    }
    else{
      this.tempNgControl=new FormControl('');
    }
  }

  ngOnInit() {
  }

  writeValue(obj: any): void {
  }

  registerOnChange(fn: any): void {

  }

  registerOnTouched(fn: any): void {

  }

  setDisabledState?(isDisabled: boolean): void {

  }

  ngAfterContentInit() {
    this.cdr.detectChanges();
  }

}