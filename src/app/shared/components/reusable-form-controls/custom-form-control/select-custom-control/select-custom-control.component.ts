import { Component, Input, Self, ChangeDetectorRef, Optional, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NgControl, FormControl, AbstractControl } from '@angular/forms';
import { convertCamelCasesToSpaces } from '@app/shared/utils';

const defaultConfig = {
  classNames: 'form-control fidelity-form-input',
  defaultOptionValue: "",
  index: '',
  shouldShowCaption: true,
  shouldDisablePlaceholder:true
}
@Component({
  selector: 'select-custom-control',
  templateUrl: './select-custom-control.component.html',
  styleUrls: ['./select-custom-control.component.scss']
})

export class SelectCustomControlComponent implements ControlValueAccessor {
  convertCamelCasesToSpaces = convertCamelCasesToSpaces;
  @Input() classNames = defaultConfig.classNames;
  @Input() name;
  @Input() placeholder;
  @Input() index = defaultConfig.index;
  @Input() defaultOptionValue = defaultConfig.defaultOptionValue;
  @Input() loopableArray;
  @Input() optionsCaption;
  @Input() extraClassNames;
  @Input() shouldShowCaption = defaultConfig.shouldShowCaption;
  @Input() shouldDisablePlaceholder=defaultConfig.shouldDisablePlaceholder;

  @Output() change: EventEmitter<any> = new EventEmitter()

  tempNgControl: AbstractControl;
  tempNgControlName: string;

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    private cdr: ChangeDetectorRef
  ) {
    if (ngControl != null) {
      ngControl.valueAccessor = this;

    }
    else {
      this.tempNgControl = new FormControl('');
      this.tempNgControlName = "randomName-" + Math.floor(Math.random() * 22);
    }
  }

  ngOnInit() {
  }

  writeValue(obj: any): void {
  }

  registerOnChange(fn: any): void {

  }

  registerOnTouched(fn: any): void {

  }

  setDisabledState?(isDisabled: boolean): void {

  }

  ngAfterContentInit() {
    this.cdr.detectChanges();
    // if the controlname is country code drop down then caption should be removed from the drop down 
    if (this.ngControl.name.toLowerCase().includes('countrycode')) {
      this.shouldShowCaption = false;
    }
  }

}