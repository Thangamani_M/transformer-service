import { Component, EventEmitter, Input, OnInit, Output, QueryList, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CommonPaginationConfig, createOrUpdateFilteredFieldKeyValues, CrudType, CustomDirectiveConfig, DATE_FORMAT_TYPES, debounceTimeForDeepNestedObjectSearchkeyword, HttpCancelService, IPatchTableFilteredFields, NO_DATA_FOUND_MESSAGE, OtherService, patchPersistedNgrxFieldValuesToForm, prepareReusablePTableLoadDataConfiguration, RxjsService, SharedTableFilteredDataCreated, tableFilteredDataSelector$ } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { Store } from '@ngrx/store';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'app-page-list-table',
  templateUrl: './page-list-table.component.html',
  styleUrls: ['./page-list-table.component.scss']
})
export class PageListTableComponent implements OnInit {
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  today = new Date();
  row: any = {};
  DATE_FORMAT_TYPES = DATE_FORMAT_TYPES;
  @Input() tabsList;
  @Input() selectedRows;
  @Input() status;
  @Input() activeList;
  @Input() totalRecords;
  @Input() loading;
  @Input() pageLimit;
  @Input() dataList: any;
  @Input() isShowNoRecord = true;
  @Input() selectedTabIndex;
  @Input() reset = false;
  @Input() first = 0;
  @Input() pageSize = 10;
  @Input() searchFirst: boolean;
  @Input() isSelectionOne: boolean;
  @Input() customScroll: boolean = true;
  @Input() clearSelectedRow: boolean = false;
  @Output() onActionSubmited = new EventEmitter<any>();
  @Output() excelPrinted = new EventEmitter<any>();
  @Output() onSelectedRows = new EventEmitter<any>();
  @Input() menuItems;
  @Input() menudownloadItems;
  @Input() enableFieldValuesPersistance = false;
  @ViewChild('dt', { static: false }) table: Table;
  @ViewChildren(Table) tables: QueryList<Table>;
  showPickedOrdersPopup = false;
  selectedriIndex: any;
  // start logic table fields search persistant
  tableFilteredFields: any = {};
  areColumnFiltersEdited = false;
  isPaginationRequested = false;
  isSortingRequested = false;
  // end logic table fields search persistant
  NO_DATA_FOUND_MESSAGE = NO_DATA_FOUND_MESSAGE;
  event;
  isSaveFilterBtnDisabled = false;
  isClearFilterBtnDisabled = false;

  constructor(private _fb: FormBuilder, private tableFilterFormService: TableFilterFormService, private store: Store<AppState>, private router: Router,
    private momentService: MomentService, private otherService: OtherService, private rxjsService: RxjsService, private httpCancelService: HttpCancelService) {
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tabsList?.length ? this.tableFilterFormService.createFormGroup(this.tabsList[this.selectedTabIndex]?.columns) : this.columnFilterForm;
    // this.tabsList.forEach((tableObject) => {
    //   tableObject.enableReset = true;
    //   tableObject.enableSaveFilterButton = true;
    // });
    this.searchKeywordRequest();
    this.columnFilterRequest();
    if (this.enableFieldValuesPersistance) {
      // start logic table fields search persistant
      combineLatest([
        // Little trick is added to validate and retrieve the field searches across the multiple tab based tables
        this.rxjsService.getAnyPropertyValue(),
        this.store.select(tableFilteredDataSelector$)
      ]).subscribe((observables) => {
        if (Object.keys(observables[0] ? observables[0] : {}).length > 0) {
          this.areColumnFiltersEdited = false;
          this.isPaginationRequested = false;
          this.isSortingRequested = false;
        }
        if (!observables[1]) return;
        this.tableFilteredFields = observables[1];
        // Give some milliseconds to render with proper history of fields search values and table results
        setTimeout(() => {
          this.otherService.currentUrl=this.otherService.currentUrl?
          this.otherService.currentUrl:this.router.url;
          // If the key value pair is about to be created for the first time
          if (Object.keys(this.tableFilteredFields?.[this.otherService.currentUrl] ?
            this.tableFilteredFields[this.otherService.currentUrl] : {}).length == 0) {
            if (this.areColumnFiltersEdited || this.isSortingRequested || this.isPaginationRequested) {
              this.httpCancelService.cancelPendingRequestsOnFormSubmission();
            }
            this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey(), null, false);
          }
          // If the key value pair is created already with existed searched filters
          else if (Object.keys(this.tableFilteredFields?.[this.otherService.currentUrl] ?
            this.tableFilteredFields[this.otherService.currentUrl] : {}).length > 2) {
            this.organizeRowVariableAsPerTabIndex();
            // Helps to persist the previously searched results and field values
            let { columnFilterForm, row }: IPatchTableFilteredFields = patchPersistedNgrxFieldValuesToForm(this.columnFilterForm, this.tableFilteredFields, this.router,
              this.otherService, this.httpCancelService, this.areColumnFiltersEdited, this.isPaginationRequested, this.isSortingRequested);
            this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = row;
            this.columnFilterForm = columnFilterForm;
            this.httpCancelService.cancelPendingRequestsOnFormSubmission();
            this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey(), null, false);
          }
          // If the key value pair is created for the pageIndex and pageSize alone
          else if (Object.keys(this.tableFilteredFields?.[this.otherService.currentUrl] ?
            this.tableFilteredFields[this.otherService.currentUrl] : {}).length == 2) {
            this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = this.tableFilteredFields[this.otherService.currentUrl];
            this.httpCancelService.cancelPendingRequestsOnFormSubmission();
            this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey(), null, false);
          }
        });
      });
      // end logic table fields search persistant
    }
  }

  onPageChange(e) {
    this.isPaginationRequested = true;
    this.isSortingRequested = false;
    this.areColumnFiltersEdited = false;
    if (this.enableFieldValuesPersistance) {
      // if first has greater than 0 then some pagination user action is done 
      this.areColumnFiltersEdited = false;
      setTimeout(() => {
        this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = prepareReusablePTableLoadDataConfiguration(this.tableFilteredFields, this.otherService, this.router,
          this.areColumnFiltersEdited, this.isPaginationRequested, this.isSortingRequested, this.event);
        this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey(), null, true, false);
      }, 100);
    }
  }

  onSortChange(e) {
    if (this.enableFieldValuesPersistance) {
      let filteredObject = this.tableFilteredFields?.[this.otherService?.currentUrl];
      if (filteredObject?.sortOrderColumn !== e.field ||
        (filteredObject?.sortOrder == 'ASC' ? 1 :
          filteredObject?.sortOrder == 'DESC' ? -1 : 0) !== e.order) {
        this.isSortingRequested = true;
      }
      this.isPaginationRequested = false;
      this.areColumnFiltersEdited = false;
      // if first has greater than 0 then some pagination user action is done 
      this.areColumnFiltersEdited = false;
      setTimeout(() => {
        this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = prepareReusablePTableLoadDataConfiguration(this.tableFilteredFields, this.otherService, this.router,
          this.areColumnFiltersEdited, this.isPaginationRequested, this.isSortingRequested, this.event);
        this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey(), null, true, false);
      }, 100);
    }
  }

  clearFilter() {
    this.columnFilterForm.reset();
    this.store.select(tableFilteredDataSelector$).pipe(take(1)).subscribe((tableFilteredData: any) => {
      if (!tableFilteredData) return;
      let tableFilteredFields: any = { ...tableFilteredData };
      tableFilteredFields = Object.assign({ saveFilter: false }, tableFilteredFields[this.otherService.currentUrl]);
      this.store.dispatch(new SharedTableFilteredDataCreated({ tableFilteredFields }));
    });
  }

  saveFilter() {
    // this.store.select(tableFilteredDataSelector$).pipe(take(1)).subscribe((tableFilteredData:any)=>{
    //   if(!tableFilteredData)return;
    //   let tableFilteredFields:any={...tableFilteredData};
    //   tableFilteredFields=Object.assign({saveFilter:true},tableFilteredFields[this.otherService.currentUrl]);
    //   this.store.dispatch(new SharedTableFilteredDataCreated({ tableFilteredFields }));
    // });
  }

  ngOnDestroy() {

  }

  // start logic table fields search persistant
  organizeRowVariableAsPerTabIndex() {
    if (!this.row?.[this.selectedTabIndex ? this.selectedTabIndex : 0]) {
      this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = {
        pageIndex: +CommonPaginationConfig.defaultPageIndex,
        pageSize: +CommonPaginationConfig.defaultPageSize
      };
    }
  }

  returnRowObjectWithIndexKey() {
    return this.row[this.selectedTabIndex ? this.selectedTabIndex : 0];
  }
  // end logic table fields search persistant

  ngOnChanges(changes: SimpleChanges) {
    if (changes?.tabsList?.currentValue != changes?.tabsList?.previousValue) {
      this.tabsList = this.tabsList;
      if (!changes?.selectedTabIndex) {
        this.columnFilterForm = this.tabsList?.length ? this.tableFilterFormService.createFormGroup(this.tabsList[this.selectedTabIndex]?.columns) : this.columnFilterForm;
        this.columnFilterRequest();
      }
    }
    // for refreshing the selections
    if ((changes?.loading?.currentValue != changes?.loading?.previousValue) ||
      (changes?.dataList?.currentValue != changes?.dataList?.previousValue)) {
      // this.selectedRows = [];
    }
    if (changes?.selectedRows?.currentValue != changes?.selectedRows?.previousValue) {
      this.selectedRows = changes?.selectedRows?.currentValue;
    }
    if (changes?.first?.currentValue == 0) {
      this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = { pageIndex: 0, pageSize: 10 };
    }
    if (changes?.reset?.currentValue == true && !this.enableFieldValuesPersistance) {
      this.resetSort();
    }
    if (changes?.clearSelectedRow?.currentValue == true) {
      this.selectedRows = [];
    }
    this.loading = this.loading;
    this.dataList = this.dataList;
    this.totalRecords = this.totalRecords;
    if (changes?.selectedTabIndex?.currentValue != changes?.selectedTabIndex?.previousValue) {
      if (!this.enableFieldValuesPersistance) {
        this.first = 0;
      }
      this.selectedRows = [];
      this.searchColumns = '';
      this.columnFilterForm = this.tabsList?.length ? this.tableFilterFormService.createFormGroup(this.tabsList[this.selectedTabIndex]?.columns) : this.columnFilterForm;
      this.columnFilterRequest();
    }
  }

  ngAfterViewChecked() {

  }

  ngAfterContentChecked() {
    if (this.enableFieldValuesPersistance) {
      if (Object.keys(this.returnRowObjectWithIndexKey() ? this.returnRowObjectWithIndexKey() : {}).length == 0 && this.table) {
        this.table.first = 0;
      }
      else if (this.table) {
        this.table.first = (this.returnRowObjectWithIndexKey()?.pageIndex * this.returnRowObjectWithIndexKey()?.pageSize);
      }
    }
  }

  transformDecimal(num?: number) {
    return this.otherService.transformDecimal(num)
  }

  getClass(rowData) {
    return rowData?.cssClass ? rowData['cssClass'] : '';
  }

  onSelectDate(rowData, index, event) {
    rowData['isChanged'] = true;
  }

  toggleMenu(menu, event, rowData) {
    this.menuItems.forEach((menuItem) => {
      menuItem.data = rowData;
    });
    menu.toggle(event);
  }

  onCRUDRequested(type: CrudType | string, data?: object, unknownVar?: number | string | any, shouldDispatchNgrxAction = true, shouldEmitEvent = true): void {
    switch (type) {
      case CrudType.CREATE:
        this.onActionSubmited.emit({ type: type, data });
        break;
      case CrudType.VIEW:
        this.onActionSubmited.emit({ type: type, data, col: unknownVar ? unknownVar : 0 });
        break;
      case CrudType.GET:
        if (this.enableFieldValuesPersistance) {
          let otherParams = {};
          if (this.searchForm.value.searchKeyword) {
            otherParams["search"] = this.searchForm.value.searchKeyword;
          }
          if (Object.keys(data ? data : {}).length > 0) {
            let dataCopy = { searchColumns: {} };
            // logic for split columns and its values to key value pair
            if (data['searchColumns']) {
              Object.keys(data['searchColumns']).forEach((key) => {
                if (data['searchColumns'][key] instanceof Date) {
                  dataCopy['searchColumns'][key] = this.momentService.localToUTCDateTime(data['searchColumns'][key]);
                } else {
                  dataCopy['searchColumns'][key] = data['searchColumns'][key];
                }
              });
              data['searchColumns'] = dataCopy['searchColumns'];
            }
          }
          else {
            data = {
              pageIndex: +CommonPaginationConfig.defaultPageIndex,
              pageSize: +CommonPaginationConfig.defaultPageSize
            };
          }
          this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = data;
          let { rowObject, otherParamsObject } = createOrUpdateFilteredFieldKeyValues(this.tableFilteredFields, data, this.tabsList, this.otherService, this.store, shouldDispatchNgrxAction,
            this.areColumnFiltersEdited, this.isPaginationRequested, this.isSortingRequested);
          Object.keys(data).forEach((key) => {
            if (key == 'searchColumns') {
              delete data[key];
            }
          });
          data = { ...data, ...rowObject };
          otherParams = { ...otherParams, ...otherParamsObject };
          setTimeout(() => {
            if (shouldEmitEvent) {
              // To fetch the default sort by if any from the ngrx store
              if (otherParams?.['sortOrderColumn']) {
                this.tabsList[this.selectedTabIndex]['sortField'] = otherParams?.['sortOrderColumn'];
                this.tabsList[this.selectedTabIndex]['sortOrder'] = otherParams?.['sortOrder'] == 'ASC' ? 1 :
                  otherParams?.['sortOrder'] == 'DESC' ? -1 : 0;
              }
              if (this.isSortingRequested || this.areColumnFiltersEdited) {
                this.first = +CommonPaginationConfig.defaultPageIndex;
                this.pageSize = +CommonPaginationConfig.defaultPageSize;
              }
              else {
                this.pageSize = data?.['pageSize'];
                this.first = (data?.['pageIndex'] * data?.['pageSize']);
              }
              this.onActionSubmited.emit({ type: type, data, search: otherParams });
            }
          });
          // end logic table fields search persistant
          break;
        }
        else {
          let otherParams = {};
          if (this.searchForm.value.searchKeyword) {
            otherParams["search"] = this.searchForm.value.searchKeyword;
          }
          if (Object.keys(this.row).length > 0) {
            // logic for split columns and its values to key value pair
            if (this.row['searchColumns']) {
              Object.keys(this.row['searchColumns']).forEach((key) => {
                if (this.row['searchColumns'][key] instanceof Date) {
                  otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
                } else {
                  otherParams[key] = this.row['searchColumns'][key];
                }
              });
            }
            if (this.row['sortOrderColumn']) {
              otherParams['sortOrder'] = this.row['sortOrder'];
              otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
            }
          }
          this.onActionSubmited.emit({ type: type, data, search: otherParams });
        }
        break;
      case CrudType.EDIT:
        this.onActionSubmited.emit({ type: type, data, col: unknownVar ? unknownVar : 0 });
        break;
      case CrudType.ACTION:

        this.onActionSubmited.emit({ type: type, data, col: unknownVar ? unknownVar : 0 });
        break;
      case CrudType.STATUS_POPUP:
        this.onActionSubmited.emit({ type: type, data, col: unknownVar ? unknownVar : 0 });
        break;
      case CrudType.DELETE:
        this.onActionSubmited.emit({ type: type, data });
        break;
      case CrudType.QUANTITY_CLICK:
        this.onActionSubmited.emit({ type: type, data, search: unknownVar ? unknownVar : 0 });
        break;
      case CrudType.FLAG:
        this.onActionSubmited.emit({ type: type, data, search: unknownVar ? unknownVar : 0 });
        break;
      case CrudType.EDIT_ACTION:
        this.onActionSubmited.emit({ type: type, data, search: unknownVar ? unknownVar : 0 });
        break;
      case CrudType.CUSTOM_VIEW:
        this.onActionSubmited.emit({ type: type, data });
        break;
      case CrudType.CUSTOM_ASSIGN:
        this.onActionSubmited.emit({ type: type, data });
        break;
      case CrudType.CUSTOM_CANCEL:
        this.onActionSubmited.emit({ type: type, data });
        break;
      default:
    }
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.areColumnFiltersEdited = true;
          this.isPaginationRequested = false;
          this.isSortingRequested = false;
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.isSaveFilterBtnDisabled = Object.keys(this.searchColumns).length > 0 ? false : true;
          this.isClearFilterBtnDisabled = Object.keys(this.searchColumns).length > 0 ? false : true;
          if (this.enableFieldValuesPersistance) {
            // start logic table fields search persistant
            this.organizeRowVariableAsPerTabIndex();
            this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = prepareReusablePTableLoadDataConfiguration(this.tableFilteredFields, this.otherService, this.router,
              this.areColumnFiltersEdited, this.isPaginationRequested, this.isSortingRequested, null, this.searchColumns);
            return of(this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey(), null, true, false));
            // end logic table fields search persistant
          }
          else {
            this.row['searchColumns'] = this.searchColumns;
            this.row['pageIndex'] = this.searchFirst ? 0 : this.row['pageIndex'];
            if (this.tabsList[this.selectedTabIndex]?.isSaveFilter && this.searchColumns) {
              localStorage.setItem(this.tabsList[this.selectedTabIndex]?.storageKey, JSON.stringify(this.row));
            } else if (this.tabsList[this.selectedTabIndex]?.isSaveFilter && !this.searchColumns) {
              localStorage.removeItem(this.tabsList[this.selectedTabIndex]?.storageKey);
            }
            return of(this.onCRUDRequested(CrudType.GET, this.row));
          }
        })
      )
      .subscribe();
  }

  // start logic table fields search persistant
  loadPaginationLazy(event) {
    this.event = event;
    let findField;
    if (event?.sortField) {
      findField = this.tabsList[this.selectedTabIndex]?.columns.find(el => el?.field == event?.sortField);
      if (!findField) {
        return;
      }
    }
    if (!this.enableFieldValuesPersistance) {
      let row = {};
      this.first = event.first;
      row['pageIndex'] = event.first / event.rows;
      row["pageSize"] = event.rows;
      row["sortOrderColumn"] = event.sortField;
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
      row["searchColumns"] = this.searchColumns;
      this.row = row;
      this.onCRUDRequested(CrudType.GET, this.row);
    }
  }
  // end logic table fields search persistant

  resetSort() {
    this.row = { pageIndex: 0, pageSize: 10 };
    this.reset = false;
    this.columnFilterForm.reset('', { emitEvent: false });
    this.table.sortOrder = 0;
    this.table.sortField = '';
    this.table._rows = this.pageSize; // for reset page size dropdown
    setTimeout(() => {
      this.table.reset();

    }, 500)
  }

  onRowSelect(e) {
    this.onSelectedRows.emit(this.selectedRows);
  }

  onRowUnselect(e) {
    this.onSelectedRows.emit(this.selectedRows);
  }

  onCheckboxChange(e) {
    this.onSelectedRows.emit(this.selectedRows);
  }

  onRowCheckboxChange(e, data) {
    if (this.isSelectionOne && this.selectedRows?.length) {
      this.selectedRows = [data];
    }
  }

  exportExcel() {
    this.excelPrinted.emit();
  }

  onChangeStatus(rowData, ri) {
    this.onCRUDRequested('status popup', rowData, ri);
  }
  onEditAction(type, rowData, index) {
    this.onCRUDRequested('edit-action', rowData, { index: index, type: type });
  }
  onQuantityClick(type, rowData, index) {
    this.onCRUDRequested('quantity-click', rowData, { index: index, type: type });
  }
  onClickIcon(rowData, ri, col) {
    this.onCRUDRequested('flag', rowData, { index: ri, column: col });
  }
  onChangeMultiStatus(rowData, ri, col) {
    this.onCRUDRequested('status popup', rowData, { index: ri, field: col });
  }
  onChangeCheckbox(rowData, ri, col) {
    if (col?.isClick) {
      this.onCRUDRequested('action', rowData, { index: ri, column: col });
    }
  }
  onChangeSwitch(rowData, ri, col) {
    if (col?.isClick) {
      this.onCRUDRequested('action', rowData, { index: ri, column: col });
    }
  }
  cancelShowPopup(index: number, type: any) {
    if (type == 'enter') {
      this.showPickedOrdersPopup = true;
      this.selectedriIndex = index;
    }
    else {
      this.showPickedOrdersPopup = false;
      this.selectedriIndex = null;
    }
  }
}
