import { Action } from '@ngrx/store';

export enum SharedTableActionTypes {
  DataCreateAction = '[Shared Table Filtered] Data Created',
  DataUpdatedAction = '[Shared Table Filtered] Data Updated',
  DataRemovedAction = '[Shared Table Filtered] Data Removed',
}
export class SharedTableFilteredDataCreated implements Action {
  readonly type = SharedTableActionTypes.DataCreateAction;
  constructor(public payload: { tableFilteredFields: any }) { }
}

export class SharedTableFilteredDataUpdated implements Action {
  readonly type = SharedTableActionTypes.DataUpdatedAction;
  constructor(public payload: { tableFilteredFields: any }) { }
}
export class SharedTableFilteredDataRemoved implements Action {
  readonly type = SharedTableActionTypes.DataRemovedAction;
}

export type SharedTableActions = SharedTableFilteredDataCreated | SharedTableFilteredDataUpdated | SharedTableFilteredDataRemoved;

