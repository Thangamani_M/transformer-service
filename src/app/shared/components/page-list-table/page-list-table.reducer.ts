import { SharedTableActions, SharedTableActionTypes } from '@app/shared';
export interface TableFilteredDataState {
  tableFilteredFields: undefined
}

export const initialState: TableFilteredDataState = {
  tableFilteredFields: undefined
};

export function SharedTableFilteredDataReducer(state = initialState,
  action: SharedTableActions): TableFilteredDataState {
  switch (action.type) {
    case SharedTableActionTypes.DataCreateAction:
      return {
        tableFilteredFields: action.payload.tableFilteredFields
      };
    case SharedTableActionTypes.DataUpdatedAction:
      return { ...state, ...action.payload.tableFilteredFields };
    case SharedTableActionTypes.DataRemovedAction:
      return {
        tableFilteredFields: undefined
      }
    default:
      return state;
  }
}
