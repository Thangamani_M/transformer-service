import { TableFilteredDataState } from '@app/shared';
import {createSelector, createFeatureSelector} from '@ngrx/store';

export const selectTableFilteredDataState = createFeatureSelector<TableFilteredDataState>("TableFilteredData");

export const tableFilteredDataSelector$ = createSelector(
  selectTableFilteredDataState,
  tableFilteredDataState => tableFilteredDataState?.tableFilteredFields
);