import { Injectable } from '@angular/core';
import { SharedTableActionTypes, SharedTableFilteredDataCreated, SharedTableFilteredDataRemoved, SharedTableFilteredDataUpdated } from '@app/shared';
import { UserService } from '@app/shared/services';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { defer, of } from 'rxjs';
import { tap } from 'rxjs/operators';
@Injectable()
export class SharedTableActionEffects {
  constructor(private actions$: Actions, private userService: UserService) {
  }

  @Effect({ dispatch: false })
  login$ = this.actions$.pipe(
    ofType<SharedTableFilteredDataCreated>(SharedTableActionTypes.DataCreateAction),
    tap(action => {
      this.userService.tableFilteredFields = action.payload.tableFilteredFields;
    })
  );

  @Effect({ dispatch: false })
  loginUpdate$ = this.actions$.pipe(
    ofType<SharedTableFilteredDataUpdated>(SharedTableActionTypes.DataUpdatedAction),
    tap(action => {
      this.userService.tableFilteredFields = action.payload.tableFilteredFields;
    })
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType<SharedTableFilteredDataRemoved>(SharedTableActionTypes.DataRemovedAction),
    tap(() => {
      this.userService.tableFilteredFields = undefined;
    })
  );

  @Effect()
  init$ = defer(() => {
    const tableFilteredFields = this.userService.tableFilteredFields;
    if (tableFilteredFields) {
      return of(new SharedTableFilteredDataCreated({ tableFilteredFields }));
    }
    else {
      return <any>of(new SharedTableFilteredDataRemoved());
    }
  });
}
