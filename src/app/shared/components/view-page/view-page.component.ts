import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DATE_FORMAT_TYPES } from '@app/shared';
@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
})
export class ViewPageComponent implements OnInit {
  DATE_FORMAT_TYPES = DATE_FORMAT_TYPES;
  @Input() viewDetail: any;
  @Input() col: any;
  @Input() marginClass: any;
  @Input() template: any = 1;
  @Input() viewPageForm: FormGroup;
  @Output() onSelectionChange = new EventEmitter<any>();
  @Output() onLinkClicked = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  onSelectChange(e, key) {
    this.onSelectionChange.emit({ val: e, col: key });
  }

  isLink(item) {
    return item?.value?.enableHyperLink && item?.value?.value;
  }

  onLinkClick(val) {
    this.onLinkClicked.emit(val);
  }

  onButtonClick(val) {
    this.onLinkClicked.emit(val);
  }
}
