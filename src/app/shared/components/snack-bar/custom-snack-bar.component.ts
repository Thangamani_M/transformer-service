import {Component, Inject, ViewEncapsulation} from '@angular/core';
import { MatSnackBarRef} from '@angular/material';
import {MAT_SNACK_BAR_DATA} from '@angular/material';

/**
 * @title Snack-bar with a custom component
 */
@Component({
  selector: 'custom-snack-bar',
  templateUrl: './custom-snack-bar.component.html',
  styleUrls:['./custom-snack-bar.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class CustomSnackBarComponent {
   constructor(public snackBarRef: MatSnackBarRef<CustomSnackBarComponent>,@Inject(MAT_SNACK_BAR_DATA) public data: any) { 
   }
}