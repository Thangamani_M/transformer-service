import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { RxjsService } from '@app/shared/services';
@Component({
  selector: 'app-page-tab',
  templateUrl: './page-tab.component.html',
})
export class PageTabComponent implements OnInit {

  @Input() selectedIndex: any;
  @Input() tabsList: any;
  @Input() template: any = 2;
  @Output() tabClicked = new EventEmitter<any>();

  constructor(private rxjsService: RxjsService) { }

  ngOnInit(): void {
  }

  tabClick(e) {
    this.tabClicked.emit(e);
    // Little trick is added to validate and retrieve the field searches across the multiple tab based tables
    this.rxjsService.setAnyPropertyValue({ tabChanged: true });
  }
}
