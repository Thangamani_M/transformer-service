import { Component } from '@angular/core';
import { DynamicConfirmByMessageConfirmationType } from '@app/shared/services/reusable-primeng-table-features.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-primeng-message-confirm-dialog',
  templateUrl: './primeng-message-confirm-dialog.component.html',
})
export class PrimengMessageConfirmDialogComponent {
  showDialogSpinner = false;
  data = [];
  dynamicConfirmByMessageConfirmationType=DynamicConfirmByMessageConfirmationType;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  confirmAction(type: string) {
    if (type == 'yes') {
      this.ref.close(true);
    }
    else {
      this.ref.close(false);
    }
  }
}
