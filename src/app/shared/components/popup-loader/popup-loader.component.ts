import { Component, Input } from '@angular/core';
import { RxjsService } from '@app/shared/services';

export const defaultPopupType={
  ANGULAR_MATERIAL_NON_DRAGGABLE:'Angular Material Non Draggable',
P_DIALOG:'P Dialog',
ANGULAR_MATERIAL_DRAGGABLE:'Angular Material Draggable',
}
@Component({
  selector: 'app-popup-loader',
  templateUrl: './popup-loader.component.html',
  styleUrls: ['./popup-loader.component.scss']
})
export class PopupLoaderComponent {
  @Input() isLoading = false;
  @Input() popupType=defaultPopupType.ANGULAR_MATERIAL_NON_DRAGGABLE;
  color = 'primary';
  mode = 'indeterminate';
  defaultPopupType=defaultPopupType;
  constructor(private rxjsService: RxjsService) {
  }

   ngOnInit() {
    this.rxjsService.getPopupLoaderProperty().subscribe((isLoading) => {
      setTimeout(() => {
        this.isLoading = isLoading;
      }, 50);
    });
  }
}