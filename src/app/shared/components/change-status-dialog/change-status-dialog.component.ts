import { Component, Inject, OnInit } from '@angular/core';
import { RxjsService, CrudService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-change-status-dialog',
  templateUrl: './change-status-dialog.component.html',
  styleUrls: ['./change-status-dialog.component.scss']
})

export class ChangeStatusDialogComponent implements OnInit {
  changeStatusForm: FormGroup;
  statusList = [{ displayName: 'Active', id: 'true' }, { displayName: 'InActive', id: 'false' }];

  constructor(@Inject(MAT_DIALOG_DATA) public enableDisableObj: any,
    private dialog: MatDialog, private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private crudService: CrudService) {
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createChangeStatusForm();
  }

  createChangeStatusForm(): void {
    this.changeStatusForm = this.formBuilder.group({
      isActive: [this.enableDisableObj.isActive],
    })
  }

  onChangeStatus(): void {
    this.crudService
      .enableDisable(this.enableDisableObj.moduleName,
        this.enableDisableObj.apiSuffixModel,
        {
          ids: this.enableDisableObj.ids,
          isActive: this.changeStatusForm.get('isActive').value,
          modifiedUserId: this.enableDisableObj.modifiedUserId
        },this.enableDisableObj.apiVersion?this.enableDisableObj.apiVersion:0)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dialog.closeAll();
        }
      });
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}