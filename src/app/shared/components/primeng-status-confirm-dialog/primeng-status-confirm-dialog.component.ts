import { Component, OnInit } from '@angular/core';
import { CrudService } from '@app/shared';
import { IApplicationResponse } from '@app/shared/utils';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-primeng-status-confirm-dialog',
  templateUrl: './primeng-status-confirm-dialog.component.html',
})
export class PrimengStatusConfirmDialogComponent implements OnInit {
  selectedRow;
  showDialogSpinner: boolean = false;
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private crudService: CrudService) { }

  ngOnInit(): void {
  }

  emitChangeStatus() {
    this.showDialogSpinner = true;
    this.crudService.enableDisable(this.config.data.moduleName,this.config.data.apiSuffixModel,
         {
          ids: this.config.data.ids,
          isActive: this.config.data.isActive,
          modifiedUserId: this.config.data.modifiedUserId,
          ...this.config.data.customData
        }).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.showDialogSpinner = true;
          this.ref.close(true);
        } else {
          this.showDialogSpinner = false;
          this.ref.close(false);
        }
      });
  }

  close() {
    this.ref.close(false);
  }
}
