import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { defaultPopupType } from '@app/shared';
import { BoundaryDetailNotificationModel } from '@app/shared/models/boundary-details-notifications.models';
import { OnDemandPreloadService } from '@app/shared/services/preload-service';
import { ReusablePrimeNGTableFeatureService } from '@app/shared/services/reusable-primeng-table-features.service';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { SalesModuleApiSuffixModels } from '../../../../modules/sales/shared/utils/sales-module.enums';
import { ResponseMessageTypes } from '../../../../shared/enums/enum';
import { PrimeNgTableVariablesModel } from '../../../../shared/models';
import { CrudService } from '../../../../shared/services/crud.service';
import { RxjsService } from '../../../../shared/services/rxjs.services';
import { SnackbarService } from '../../../../shared/services/snackbar.service';
import {
  ANGULAR_EDITOR_CONFIG, CrudType, debounceTimeForSearchkeyword, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator
} from '../../../../shared/utils';
@Component({
  selector: 'boundary-details-notification.component',
  templateUrl: './boundary-details-notification.component.html'
})
export class BoundaryDetailNotificationComponent extends PrimeNgTableVariablesModel implements OnInit {
  boundaryRequestId = "";
  boundaryRequestDetails = {
    boundaryRequestId: "", boundaryTypeId: "", description: "", boundaryTypeName: "", divisionName: "", boundaryName: "",
    comments: "", boundaryStatusId: 2, createdUserId: "", boundaryId: "", batchId: "", boundaryLayerName: "", branchName: "",
    boundaryRequestRefNo: "", areaSetup: [], boundaryLayerId: ""
  };
  emailNotificationBoundaryForm: FormGroup
  fileList: File[] = [];
  selectedEmployees = [];
  selectedEmployeesForCC = [];
  employees = [];
  isFormSubmitted = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  todayDate = new Date();
  impacted_customers_modal = false
  @ViewChild('autocompleteCCVendorsInput', { static: false }) autocompleteCCVendorsInput: ElementRef;
  primengTableConfigProperties = {
    tableCaption: "Impacted Customers",
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Impacted Customers',
          dataKey: 'customerId',
          enableAddActionBtn: true,
          enableStatusActiveAction: true,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          apiSuffixModel: UserModuleApiSuffixModels.DIVISIONS,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
          columns: [{ field: 'customerRefNo', header: 'Customer ID' },
          { field: 'customerName', header: 'Customer Name' },
          { field: 'siteAddress', header: 'Site Address' },
          { field: 'isActiveServices', header: 'Active Services', type: 'dropdown', options: [{ label: 'Yes', value: 1 }, { label: 'No', value: 2 }] }],
        },
      ]
    }
  }
  config = ANGULAR_EDITOR_CONFIG;
  selectedBtnType = '';
  defaultPopupType = defaultPopupType.P_DIALOG;

  constructor(private route: Router, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService, private crudService: CrudService, private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>, private datePipe: DatePipe,
    private preloadOnDemandService: OnDemandPreloadService) {
    super();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.boundaryRequestId = response[1].boundaryRequestId;
        this.getGoLiveNotifications();
      });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createEmailNotificationAddForm();
    this.getEmployeesBySearchKeyword();
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.GET:
        if (searchObj?.isActiveServices) {
          searchObj.isActiveServices = searchObj.isActiveServices == 1 ? true : false;
        }
        this.getImpactedCustomers(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.impacted_customers_modal = false;
        this.rxjsService.setDialogOpenProperty(false);
        this.rxjsService.setViewCustomerData({
          customerId: row['customerId'],
          addressId: row['addressId'],
          customerTab: 0,
          monitoringTab: null,
      })
      this.rxjsService.navigateToViewCustomerPage();
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getImpactedCustomers(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['boundaryLayerId'] = this.boundaryRequestDetails.boundaryLayerId;
    otherParams['boundaryRequestId'] = this.boundaryRequestDetails.boundaryRequestId;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.IMPACTED_CUSTOMERS, undefined, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.preloadOnDemandService.startPreload('customer');
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        else{
          this.totalRecords=0;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  createEmailNotificationAddForm(): void {
    let emailNotificationModel = new BoundaryDetailNotificationModel();
    this.emailNotificationBoundaryForm = this.formBuilder.group({});
    Object.keys(emailNotificationModel).forEach((key) => {
      this.emailNotificationBoundaryForm.addControl(key, new FormControl(emailNotificationModel[key]));
    });
    this.emailNotificationBoundaryForm = setRequiredValidator(this.emailNotificationBoundaryForm, ["subject", "description", "goLiveDate"]);
  }

  getGoLiveNotifications() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.BOUNDARIES_GO_LIVE_NOTIFICATION,
      this.boundaryRequestId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.boundaryRequestDetails = response.resources;
          this.selectedEmployees = response.resources.employees;
          this.boundaryRequestDetails.areaSetup.forEach((areaSetupObj) => {
            areaSetupObj.isChecked = false;
          });
          if (response.resources.goLiveDate) {
            // let exactGoLiveDateTime = new Date(response.resources.goLiveDate).getTime();
            // let goLiveDate = new Date(response.resources.goLiveDate).setHours(0, 0, 0, 0);
            // let todayDate = new Date().setHours(0, 0, 0, 0);
            // if (todayDate - goLiveDate === 0) {
            //   let actualGoLiveDate = new Date(goLiveDate);
            //   actualGoLiveDate.setTime(exactGoLiveDateTime);
            //   this.emailNotificationBoundaryForm.get('goLiveDate').setValue(new Date(actualGoLiveDate));
            // }
            // else if (todayDate - goLiveDate > 0) {
            //   this.emailNotificationBoundaryForm.get('goLiveDate').setValue(new Date());
            // }
            this.emailNotificationBoundaryForm.get('goLiveDate').setValue(new Date(response.resources.goLiveDate));
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onClickChecklistCheckbox(checkbox, areaSetup) {
    setTimeout(() => {
      areaSetup.isChecked = checkbox.checked;
      if (areaSetup.areaSetupTypeId == 1) {
        return;
      }
      if (areaSetup.isChecked == true) {
        areaSetup.employees.forEach((employee) => {
          this.selectedEmployees.push(employee);
        });
      }
      else {
        areaSetup.employees.forEach((employee, index) => {
          let removableIndex = this.selectedEmployees.indexOf(employee.employeeId);
          this.selectedEmployees.splice(removableIndex, 1);
        });
      }
    })
  }

  getEmployeesBySearchKeyword(): void {
    var searchText: string;
    this.emailNotificationBoundaryForm.get('ccIds').valueChanges.pipe(debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap(searchKeyword => {
        this.employees = [];
        if (!searchKeyword) {
          return of();
        }
        searchText = searchKeyword;
        if (!searchText) {
          return this.employees;
        } else if (typeof searchText === 'object') {
          return this.employees = [];
        } else {
          return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null,
            true, prepareRequiredHttpParams({
              search: searchKeyword
            }));
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.employees = response.resources;
          this.employees.forEach((employee) => {
            employee.fullName = employee['firstName'] + employee['lastName'];
          })
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onEmailRemoved(emailObj: MatAutocompleteSelectedEvent) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    const index = this.selectedEmployeesForCC.indexOf(emailObj);
    if (index >= 0) {
      this.selectedEmployeesForCC.splice(index, 1);
    }
  }

  onEmailSelected(isSelected: boolean, emailObj) {
    if (this.isSelectedEmailAlreadyExists(emailObj)) {
      this.snackbarService.openSnackbar("The selected CC is already exists in To section", ResponseMessageTypes.WARNING);
      return;
    }
    if (isSelected) {
      this.selectedEmployeesForCC.push(emailObj);
      this.autocompleteCCVendorsInput.nativeElement.value = '';
      this.emailNotificationBoundaryForm.get('ccIds').setValue("");
    }
  }

  isSelectedEmailAlreadyExists(emailObj): boolean {
    let concetenatedArrays=[...this.selectedEmployees,...this.selectedEmployeesForCC];
    return concetenatedArrays.find(s => s['employeeId'] == emailObj['employeeId']) ? true : false;
  }

  viewImpactedCustomers() {
  this.impacted_customers_modal = true;
  this.rxjsService.setDialogOpenProperty(true);
  this.getImpactedCustomers();
  }

  onSubmit() {
    let message = "";
    this.isFormSubmitted = true;
    if (this.selectedBtnType == 'Go Live') {
      if (this.emailNotificationBoundaryForm.invalid) {
        return;
      }
      if (this.boundaryRequestDetails.areaSetup.filter(a => a['isChecked']).length == 0) {
        this.snackbarService.openSnackbar('Atleast one checklist should be selected..!!', ResponseMessageTypes.WARNING);
        return;
      }
      let goLivedatePipeString = this.datePipe.transform(new Date(this.emailNotificationBoundaryForm.get('goLiveDate').value), 'dd-MM-yyyy HH:mm');
      message = `Are you sure the <strong>${this.boundaryRequestDetails.boundaryName}</strong> can go live on the <strong>${goLivedatePipeString}</strong> ?`;
    }
    else{
      message = `Are you sure you wish to terminate the <strong>${this.boundaryRequestDetails.boundaryName}</strong> ?`;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(message).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          this.onFormSubmit();
        }
      });
  }

  emitUploadedFiles(uploadedFiles: any[]) {
    this.fileList = uploadedFiles;
  }

  onFormSubmit() {
    let payload = {};
    let crudService: Observable<IApplicationResponse>;
    if (this.selectedBtnType == 'Terminate') {
      payload = {
        boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
        modifiedUserId: this.loggedInUserData.userId
      };
      crudService = this.crudService.update(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.GO_LIVE_BOUNDARIES_TERMINATE, payload)
    }
    else {
      payload = {
        goLiveNotificationId: null,
        goLiveDate: this.datePipe.transform(new Date(this.emailNotificationBoundaryForm.get('goLiveDate').value), 'yyyy-MM-dd HH:mm:ss'),
        boundaryRequestId: this.boundaryRequestId,
        subject: this.emailNotificationBoundaryForm.value.subject,
        description: this.emailNotificationBoundaryForm.value.description,
        checklistIds: this.boundaryRequestDetails.areaSetup.filter(a => a['isChecked']).length == 0 ? [] :
          this.boundaryRequestDetails.areaSetup.filter(a => a['isChecked']).map(a => a.areaSetupId),
        toIds: this.selectedEmployees.map(s => s.employeeId),
        ccIds: this.selectedEmployeesForCC.map(s => s.employeeId),
        createdUserId: this.loggedInUserData.userId
      };
      let formData = new FormData();
      formData.append('email', JSON.stringify(payload));
      this.fileList.forEach(file => {
        formData.append('file', file);
      });
      crudService = this.crudService.create(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.BOUNDARIES_GO_LIVE_NOTIFICATION, this.selectedBtnType == 'Terminate' ? payload : formData);
    }
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.onRedirectToBoundaryDetailPage();
      }
      this.isFormSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onRedirectToBoundaryDetailPage() {
    this.route.navigate(["/my-tasks/boundary-management/view-request/add-edit"], {
      queryParams: {
        boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
        boundaryRequestRefNo: this.boundaryRequestDetails.boundaryRequestRefNo, fromUrl: 'MyRequestDashboard'
      }
    });
  }

  onRedirectToListPage() {
    this.route.navigateByUrl("/my-tasks/boundary-management/view-request");
  }
}
