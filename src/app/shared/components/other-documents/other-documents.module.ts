import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { LeadLifecycleReusableComponentsModule } from "@modules/sales/components/lead-header";
import { OtherDocumentsComponent } from "./other-documents.component";

@NgModule({
    declarations: [OtherDocumentsComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        SharedModule,
        LayoutModule,
        LeadLifecycleReusableComponentsModule,
        // RouterModule.forChild([
        //     { path: '', component: OtherDocumentsComponent, data: { title: 'Other Documents' } },
        // ])
    ],
    exports: [OtherDocumentsComponent],
    entryComponents: [],
})
export class OtherDocumentsModule { }