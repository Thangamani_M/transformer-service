import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { OtherDocumentsListComponent } from './other-documents-list.component';
import { OtherDocumentsModule } from "./other-documents.module";

@NgModule({
    declarations: [OtherDocumentsListComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        SharedModule,
        LayoutModule,
        OtherDocumentsModule,
        RouterModule.forChild([
            { path: '', component: OtherDocumentsListComponent, data: { title: 'Other Documents' } },
        ])
    ],
    exports: [OtherDocumentsListComponent],
    entryComponents: [],
})
export class OtherDocumentsListModule { }