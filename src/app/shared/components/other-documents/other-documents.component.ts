import { Component, Input, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import {
  CUSTOMER_COMPONENT,
  LeadCreationUserDataModel,
  LeadHeaderData,
  LeadOutcomeStatusNames,
  SalesModuleApiSuffixModels,
  Sales_Leads_Components,
  selectLeadCreationUserDataState$,
  selectLeadHeaderDataState$,
} from "@app/modules";
import { AppState } from "@app/reducers";
import {
  BreadCrumbModel,
  CrudType,
  currentComponentPageBasedPermissionsSelector$,
  debounceTimeForSearchkeyword,
  IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams,
  PrimengStatusConfirmDialogComponent,
  ResponseMessageTypes,
  ReusablePrimeNGTableFeatureService,
  SnackbarService,
} from "@app/shared";
import { CrudService, RxjsService } from "@app/shared/services";
import { TableFilterFormService } from "@app/shared/services/create-form.services";
import { MomentService } from "@app/shared/services/moment.service";
import { MY_TASK_COMPONENT } from "@modules/my-tasks/shared";
import { loggedInUserData } from "@modules/others";
import { Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { combineLatest, of } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  map,
  switchMap,
  take,
} from "rxjs/operators";
import { PrimeNgTableVariablesModel } from "../../../shared/models/prime-ng-table-list-component-variables.model";
@Component({
  selector: "app-other-documents",
  templateUrl: "./other-documents.component.html",
  styleUrls: [
    "../../../modules/sales/components/task-management/lead-info/lead-info.component.scss",
  ],
})
export class OtherDocumentsComponent
  extends PrimeNgTableVariablesModel
  implements OnInit
{
  @Input() inputParams = {
    fromComponentUrl: "lead",
    customerId: "",
    addressId: "",
  };
  @Input() permission;
  customerId: string;
  tasksObservable;
  loading = false;
  selectedColumns = [];
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns: any;
  leadId = "";
  leadHeaderData: LeadHeaderData;
  row: any = {};
  primengTableConfigProperties;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  breadCrumb: BreadCrumbModel;

  constructor(
    private activatedRoute: ActivatedRoute,
    private tableFilterFormService: TableFilterFormService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private router: Router,
    private store: Store<AppState>,
    private momentService: MomentService,
    private snackbarService: SnackbarService,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private dialogService: DialogService,
  ) {
    super();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectLeadCreationUserDataState$),
      this.store.select(selectLeadHeaderDataState$),
      this.activatedRoute.queryParams,
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.leadCreationUserDataModel = new LeadCreationUserDataModel(
          response[1]
        );
        this.leadHeaderData = new LeadHeaderData(response[2]);
        this.breadCrumb = {
          pageTitle: {
            key: "Other Documents",
            value: this.leadHeaderData.leadRefNo,
          },
          items: [
            { key: this.leadHeaderData.leadRefNo, routeUrl: "landingPage" },
            { key: "Other Documents" },
          ],
        };
      });
  }

  ngOnInit(): void {
    this.primengTableConfigProperties = {
      tableCaption: "Other Documents",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [
        { displayName: "Sales", relativeRouterUrl: "" },
        { displayName: "Other Documents" },
      ],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Other Documents",
            dataKey: "documentId",
            enableBreadCrumb: true,
            ebableAddActionBtn: true,
            enableRowDelete: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: true,
            cursorLinkIndex: 2,
            cursorSecondLinkIndex: 3,
            columns:
              this.inputParams.fromComponentUrl == "lead"
                ? [
                    { field: "fileName", header: "File Name", width: "250px" },
                    {
                      field: "documentTypeName",
                      header: "Type of Document",
                      width: "180px",
                    },
                    {
                      field: "fileName",
                      header: "Uploaded File",
                      width: "250px",
                    },
                    {
                      field: "createdDate",
                      header: "Uploaded Date",
                      width: "250px",
                    },
                  ]
                : [
                    { field: "typeOfDocument", header: "Type of Document" },
                    { field: "department", header: "Department" },
                    { field: "fileName", header: "File Name" },
                    { field: "reference", header: "Reference", width: "120px" },
                    {
                      field: "description",
                      header: "Description",
                      width: "150px",
                    },
                    { field: "fileFormat", header: "File Format" },
                    { field: "createdBy", header: "Created By" },
                    {
                      field: "createdDate",
                      header: "Uploaded Date",
                      width: "150px",
                    },
                    {
                      field: "isActive",
                      header: "Action",
                      width: "150px",
                      isPDropdown: true,
                      placeholder: 'Select',
                      options: [{label: 'Archive', value: false,},{label: 'Retrieve', value: true,}]
                    },
                  ],
            apiSuffixModel: this.inputParams.fromComponentUrl == "lead" ? SalesModuleApiSuffixModels.LEAD_DOCUMENT : 
              SalesModuleApiSuffixModels.CUSTOMER_DOCS,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
        ],
      },
    };
    if (this.inputParams.fromComponentUrl == "lead") {
      this.rxjsService.getCustomerDate().subscribe((data) => {
        this.activatedRoute.queryParams.subscribe((params) => {
          this.leadId = params.leadId;
        });
      });
    } else if (this.inputParams.fromComponentUrl == 'customer') {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableRowDelete = false;
    }
    this.combineLatestNgrxStoreDataForPermissions();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].columns
    );
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    this.getOtherDocuments();
  }

  combineLatestNgrxStoreDataForPermissions() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      if (response[0]) {
        let key =
          this.leadHeaderData.fromUrl == "Leads List"
            ? Sales_Leads_Components.LEADS
            : this.leadHeaderData.fromUrl == "My Leads List" ||
              this.leadHeaderData.fromUrl ==
                CUSTOMER_COMPONENT.CUSTOMER_MANAGEMENT
            ? MY_TASK_COMPONENT.MY_SALES
            : "";
        let filteredSubComponentsPermissions;
        if (key == "My Sales") {
          let filteredFirstLevelSubComponentsPermissions = response[0]?.[
            key
          ]?.find((cP) => cP.menuName === "My Leads");
          filteredSubComponentsPermissions =
            filteredFirstLevelSubComponentsPermissions?.["subMenu"]
              ?.filter(
                (cP) => cP.menuName === Sales_Leads_Components.LEAD_LANDING_PAGE
              )
              .map((fS) => fS["subMenu"])[0];
        } else if (
          this.leadHeaderData.fromUrl == CUSTOMER_COMPONENT.CUSTOMER_MANAGEMENT
        ) {
          let customer = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(
            (el) => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD
          );
          let sales = customer?.subMenu?.find(
            (el) => el?.menuName == CUSTOMER_COMPONENT.SALES
          );
          let leads = sales?.subMenu?.find(
            (el) => el?.menuName == CUSTOMER_COMPONENT.LEADS
          );
          filteredSubComponentsPermissions = leads?.subMenu?.find(
            (el) => el?.menuName == Sales_Leads_Components.LEAD_LANDING_PAGE
          )["subMenu"];
        } else {
          filteredSubComponentsPermissions = response[0][key]
            ?.filter(
              (cP) => cP.menuName === Sales_Leads_Components.LEAD_LANDING_PAGE
            )
            .map((fS) => fS["subMenu"])[0];
        }
        let otherDocsComponentsPermissions = filteredSubComponentsPermissions
          ?.filter((cP) => cP.menuName === Sales_Leads_Components.OTHER_DOCS)
          .map((fS) => fS["subMenu"])[0];
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(
            this.primengTableConfigProperties,
            otherDocsComponentsPermissions
          );
        this.primengTableConfigProperties =
          prepareDynamicTableTabsFromPermissionsObj[
            "primengTableConfigProperties"
          ];
      }
    });
  }

  loadPaginationLazy(event): void {
    let row = {};
    row["pageIndex"] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? "ASC" : "DESC";
    row["searchColumns"] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, row);
  }

  getOtherDocuments(
    pageIndex?: string,
    pageSize?: string,
    otherParams?: object
  ) {
    let resultObservable =
      this.inputParams.fromComponentUrl == "lead"
        ? this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.LEAD_DOCUMENT,
            undefined,
            false,
            prepareGetRequestHttpParams(pageIndex, pageSize, {
              leadId: this.leadHeaderData.leadId,
              ...otherParams,
            })
          )
        : this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.CUSTOMER_DOCS,
            undefined,
            false,
            prepareGetRequestHttpParams(pageIndex, pageSize, {
              customerId: this.inputParams.customerId,
              addressId: this.inputParams.addressId,
              ...otherParams,
            })
          );
    resultObservable.pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          // val.isActive =  val.isActive ? false : true;
          return val;
        })
      }
      return res;
    }))
    .subscribe((response: IApplicationResponse) => {
      if (
        response.isSuccess &&
        response.statusCode === 200 &&
        response.resources
      ) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap((obj) => {
          Object.keys(obj).forEach((key) => {
            if (obj[key] === "") {
              delete obj[key];
            }
          });
          this.searchColumns = Object.entries(obj).reduce(
            (a, [k, v]) => (v == null ? a : ((a[k] = v), a)),
            {}
          ); //Filter all falsy values ( null, undefined )
          this.row["searchColumns"] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  onCRUDRequested(type: CrudType | string, row?, selectedColumn?): void {
    switch (type) {
      case CrudType.CREATE:
        if (
          this.inputParams.fromComponentUrl != "customer" &&
          !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].canCreate
        ) {
          this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        } else if (
          this.inputParams.fromComponentUrl == "customer" &&
          !this.permission?.canCreate
        ) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm && this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (CrudType.GET === type && Object.keys(row).length > 0) {
          Object.keys(row["searchColumns"]).forEach((key) => {
            otherParams[key] = row["searchColumns"][key]["value"];
            if (key.toLowerCase().includes("date")) {
              otherParams[key] = this.momentService.localToUTC(
                this.row["searchColumns"][key]
              );
            } else {
              otherParams[key] = this.row["searchColumns"][key];
            }
          });
          if (row["sortOrderColumn"]) {
            otherParams["sortOrder"] = row["sortOrder"];
            otherParams["sortOrderColumn"] = row["sortOrderColumn"];
          }
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.getOtherDocuments(
              row["pageIndex"],
              row["pageSize"],
              otherParams
            );
            break;
        }
        break;
      case CrudType.EDIT:
        if (
          this.inputParams.fromComponentUrl != "customer" &&
          !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].canEdit
        ) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        }
        this.openAddEditPage(CrudType.EDIT, row, selectedColumn);
        break;
      case CrudType.ICON_POPUP:
        if (
          this.inputParams.fromComponentUrl != "customer" &&
          !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].canDownload
        ) {
          this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        } else if (
          this.inputParams.fromComponentUrl == "customer" &&
          !this.permission?.canDownload
        ) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        } else {
          this.viewFile(row);
        }
        break;
      case CrudType.DELETE:
        if (
          this.inputParams.fromComponentUrl != "customer" &&
          !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].canRowDelete
        ) {
          this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        } else if (
          this.inputParams.fromComponentUrl == "customer" &&
          !this.permission?.canRowDelete
        ) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        } else {
          this.reusablePrimeNGTableFeatureService
            .openDynamicDeleteDialog(
              this.selectedTabIndex,
              this.selectedRows,
              this.primengTableConfigProperties,
              row
            )
            ?.onClose?.subscribe((result) => {
              if (result) {
                this.selectedRows = [];
                this.getOtherDocuments();
              }
            });
        }
        break;
    }
  }

  viewFile(editableObject?: object): void {
    window.open(editableObject["documentPath"], "_blank");
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        statusName: 'Document?',
        activeText: 'Retrieve',
        inActiveText: 'Archive',
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      } else {
        this.onCRUDRequested('get', this.row);
      }
    });
  }

  openAddEditPage(
    type: CrudType | string,
    editableObject?,
    selectedColumn?: string
  ): void {
    if (this.inputParams.fromComponentUrl == "lead") {
      switch (type) {
        case CrudType.CREATE:
          if (this.leadHeaderData.fromUrl == "Leads List") {
            this.router.navigateByUrl(
              "/sales/task-management/leads/other-documents/add-edit"
            );
          } else if (
            this.leadHeaderData.fromUrl == "My Leads List" ||
            this.leadHeaderData.fromUrl ==
              CUSTOMER_COMPONENT.CUSTOMER_MANAGEMENT
          ) {
            this.router.navigateByUrl(
              "/my-tasks/my-sales/my-leads/other-documents/add-edit"
            );
          }
          break;
        case CrudType.EDIT:
          if (this.leadHeaderData.fromUrl == "Leads List") {
            this.router.navigate(
              ["/sales/task-management/other-documents/add-edit"],
              { queryParams: { fileId: editableObject["documentId"] } }
            );
          } else if (
            this.leadHeaderData.fromUrl == "My Leads List" ||
            this.leadHeaderData.fromUrl ==
              CUSTOMER_COMPONENT.CUSTOMER_MANAGEMENT
          ) {
            this.router.navigate(
              ["/my-tasks/my-sales/my-leads/other-documents/add-edit"],
              { queryParams: { fileId: editableObject["documentId"] } }
            );
          }
          break;
      }
    } else {
      switch (type) {
        case CrudType.CREATE:
          this.router.navigate(
            ["/customer/manage-customers/support-document/upload"],
            {
              queryParams: {
                customerId: this.inputParams.customerId,
                addressId: this.inputParams.addressId,
              },
            }
          );
          break;
        default:
          if (
            editableObject["isManualUpload"] == true &&
            selectedColumn &&
            selectedColumn !== "fileName"
          ) {
            if (!this.permission?.canEdit) {
              return this.snackbarService.openSnackbar(
                PERMISSION_RESTRICTION_ERROR,
                ResponseMessageTypes.WARNING
              );
            }
            this.router.navigate(
              ["/customer/manage-customers/support-document/upload"],
              {
                queryParams: {
                  customerId: this.inputParams.customerId,
                  documentId: editableObject["documentId"],
                  addressId: this.inputParams.addressId,
                },
              }
            );
          } else {
            if (!this.permission?.canDownload) {
              return this.snackbarService.openSnackbar(
                PERMISSION_RESTRICTION_ERROR,
                ResponseMessageTypes.WARNING
              );
            }
            this.viewFile(editableObject);
          }
          break;
      }
    }
  }
}
