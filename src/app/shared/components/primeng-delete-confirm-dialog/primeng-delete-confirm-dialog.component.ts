import { Component, OnInit } from '@angular/core';
import { CrudService } from '@app/shared';
import { IApplicationResponse, prepareRequiredHttpParams } from '@app/shared/utils';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-primeng-delete-confirm-dialog',
  templateUrl: './primeng-delete-confirm-dialog.component.html',
})
export class PrimengDeleteConfirmDialogComponent {
  showDialogSpinner: boolean = false;
  data = [];
  
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private crudService: CrudService) {
    this.config.data.deletableIds=this.config?.data?.deletableIds?.toString();
   }

  confirmDelete() {
    this.showDialogSpinner = true;
    let httpService: Observable<IApplicationResponse>;
    if (this.config.data.method == 'delete') {
      httpService = this.crudService.deleteByParams(this.config?.data?.moduleName, this.config?.data?.apiSuffixModel,
        { body: this.config.data.dataObject });
    }
    else if (this.config.data.method == 'put') {
      httpService = this.crudService.update(this.config?.data?.moduleName, this.config?.data?.apiSuffixModel, this.config?.data?.dataObject);
    } 
    else {
      httpService = this.crudService.delete(this.config?.data?.moduleName, this.config?.data?.apiSuffixModel,
        undefined, prepareRequiredHttpParams({
          ids: this.config?.data?.deletableIds,
          modifiedUserId: this.config?.data?.modifiedUserId,
          isDeleted: this.config?.data?.isDeleted ? this.config?.data?.isDeleted : false
        }));
    }
    httpService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showDialogSpinner = true
        this.ref.close(true);
      } else {
        this.showDialogSpinner = false;
        if (this.config.data?.flag == 'doaScreen') {
          this.ref.close(response);
        } else {
          this.ref.close(false);
        }
      }
    },
      error => {
        this.showDialogSpinner = false;
        this.ref.close(false);
      });
  }

  close() {
    this.ref.close(false);
  }
}
