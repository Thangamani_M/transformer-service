import { moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { CustomDirectiveConfig } from '@app/shared';

@Component({
  selector: 'app-page-form-array',
  templateUrl: './page-form-array.component.html',
  styleUrls: ['./page-form-array.component.scss']
})
export class PageFormArrayComponent implements OnInit {

  @Input() pageForm: FormGroup;
  @Input() tabsList: any;
  @Input() isLoading: any;
  @Input() isSubmitted: any;
  @Input() selectedIndex: any;
  @Input() isShowNoData: boolean = true;
  @Input() iscdkDragDisabled: boolean;
  @Output() addItem = new EventEmitter<any>();
  @Output() editItem = new EventEmitter<any>();
  @Output() removeItem = new EventEmitter<any>();
  @Output() validateExist = new EventEmitter<any>();
  @Output() dropEvent = new EventEmitter<any>();
  @Output() keyupEvent = new EventEmitter<any>();
  @Output() changeEvent = new EventEmitter<any>();
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldCopyKeyboardEventBeRestricted: false });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 4 } });
  todayDate = '';
  exampleHeader = '';
  items = [
    { value: 'I can be dragged', disabled: false },
    { value: 'I cannot be dragged', disabled: true },
    { value: 'I can also be dragged', disabled: false }
  ];
  constructor() { }

  ngOnInit(): void {
  }

  get getFormArray(): FormArray {
    if (!this.pageForm) return;
    return this.pageForm.get(this.tabsList[this.selectedIndex].formArrayName) as FormArray;
  }

  editFormItem(i) {
    this.editItem.emit(i);
  }

  onBlurInput(i, col, val) {
    if (val) {
      this.validateExist.emit({ index: i, field: col, value: val });
    }
  }

  onKeyup(val) {
    this.keyupEvent.emit(val);
  }

  onChange(val, col) {
    if (col?.isChangeEvent) {
      this.changeEvent.emit(val);
    }
  }

  onInputClick(i, col, value) {
    if (col?.isClickEvent) {
      this.changeEvent.emit({ index: i, column: col });
    } else {
      this.keyupEvent.emit(value);
    }
  }

  onRadioClick(i, col) {
    if (col?.isClickEvent) {
      this.changeEvent.emit({ index: i, column: col });
    }
  }

  onCheckboxClick(i, col) {
    if (col?.isClickEvent) {
      this.changeEvent.emit({ index: i, column: col });
    }
  }

  getCompareField(i, col) {
    return this.tabsList[this.selectedIndex].columns.find(el => el.field == this.getFormArray.controls[i]?.get(col?.field)?.errors?.lesserGreaterThan?.comparewith)?.displayName;
  }

  addFormItem(i) {
    this.addItem.emit(i);
  }

  removeFormItem(i) {
    this.removeItem.emit(i);
  }

  getTooltip(col, i) {
    return this.getFormArray.controls[i]?.get(col.field)?.value;
  }
  // drag drop - Reorder list
  drop = (event) => {
    let _form: any = this.pageForm?.get(this.tabsList[this.selectedIndex].formArrayName);
    moveItemInArray(_form.controls, event.previousIndex, event.currentIndex);
    this.dropEvent.emit({ previousIndex: event.previousIndex, currentIndex: event.currentIndex })
  }
}
