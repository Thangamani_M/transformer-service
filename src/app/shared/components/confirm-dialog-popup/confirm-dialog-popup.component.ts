import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { IApplicationResponse, RxjsService } from '@app/shared';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog-popup.component.html',
  styleUrls: ['./confirm-dialog-popup.component.scss']
})

class ConfirmDialogPopupComponent implements OnInit {
  title: string;
  message: string;
  url: string = "https://homepages.cae.wisc.edu/~ece533/images/tulips.png";
  urlSafe: SafeResourceUrl;
  isConfirm = true;
  constructor(public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>, private rxjsService: RxjsService,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogModel | any, public sanitizer: DomSanitizer) {
    // Update view with given values
    this.title = data.title;
    this.message = data.message;
    this.isConfirm = data?.isConfirm?.toString() ? data?.isConfirm : true;
    if(data?.isAPICall) {
      this.rxjsService.setDialogOpenProperty(true);
    }else {
      setTimeout(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }, 500);

    }
  }

  ngOnInit() {
    setTimeout(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    }, 500);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

  onConfirm(): void {
    if(this.data?.isAPICall) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.data?.api?.subscribe((res: IApplicationResponse) => {
        if(res?.isSuccess && res?.statusCode == 200) {
          this.dialogRef.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      })
    } else {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.dialogRef.close(true);
    }
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }

  btnCloseClick() {
    this.dialogRef.close(false);
  }
}
class ConfirmDialogModel {
  constructor(public title: string, public message: string) {
  }
}

export { ConfirmDialogModel, ConfirmDialogPopupComponent };
