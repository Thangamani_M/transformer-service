import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { LayoutModule } from "@app/shared";
import { MaterialModule } from "@app/shared/material.module";
import { SharedModule } from "@app/shared/shared.module";
import { PrimengConfirmDialogPopupComponent } from "./primeng-confirm-dialog-popup.component";

@NgModule({
    declarations: [PrimengConfirmDialogPopupComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers: [],
    exports: [PrimengConfirmDialogPopupComponent],
    entryComponents: [PrimengConfirmDialogPopupComponent]
  })
  export class PrimengConfirmDialogPopupModule { }
