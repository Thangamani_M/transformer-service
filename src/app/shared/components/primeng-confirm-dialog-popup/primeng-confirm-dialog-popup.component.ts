import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CrudService, RxjsService } from '@app/shared';
import { IApplicationResponse } from '@app/shared/utils';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-primeng-confirm-dialog-popup',
  templateUrl: './primeng-confirm-dialog-popup.component.html',
})
export class PrimengConfirmDialogPopupComponent implements OnInit {
  title: string;
  message: string;
  isConfirm = true;
  constructor(public config: DynamicDialogConfig, private rxjsService: RxjsService, private crudService: CrudService,
    public dialogRef: DynamicDialogRef, public sanitizer: DomSanitizer) {
    // Update view with given values
    this.title = config?.data.title;
    this.message = config?.data.message;
    this.isConfirm = config?.data?.isConfirm?.toString() ? config?.data?.isConfirm : true;
    if(config?.data?.isAPICall) {
      this.rxjsService.setDialogOpenProperty(true);
    }
  }

  ngOnInit() {
  }

  onConfirm(): void {
    if(this.config?.data?.isAPICall) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.config?.data?.api?.subscribe((res: IApplicationResponse) => {
        if(res?.isSuccess && res?.statusCode == 200) {
          this.dialogRef.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      })
    } else {
      this.dialogRef.close(true);
    }
  }

  onDismiss(): void {
    if(this.isConfirm && this.config?.data?.CancelButtonName) {
      this.dialogRef.close(0)
    } else {
      this.dialogRef.close(false);
    }
    this.rxjsService.setDialogOpenProperty(false);
  }

  btnCloseClick() {
    this.dialogRef.close(false);
    this.rxjsService.setDialogOpenProperty(false);
  }

}
