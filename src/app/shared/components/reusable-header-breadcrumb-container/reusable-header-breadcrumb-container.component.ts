import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { BreadCrumbModel } from "@app/shared";
import { RxjsService } from "@app/shared/services/rxjs.services";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs/internal/observable/combineLatest";
@Component({
  selector: "app-reusable-header-breadcrumb-container",
  templateUrl: "./reusable-header-breadcrumb-container.component.html",
  styleUrls: ["./reusable-header-breadcrumb-container.component.scss"],
})

export class ReusableHeaderBreadcrumbContainerComponent implements OnInit {
  @Input() breadCrumb:BreadCrumbModel;
  @Input() shouldShowOnlyBreadCrumbs=false;
  fromUrl='';

  constructor(private router: Router, private dialog: MatDialog, private store: Store<AppState>,private rxjsService:RxjsService) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([
     this.rxjsService.getFromUrl()]
    ).subscribe((response) => {
      this.fromUrl = response[0];
    });
  }

  ngOnInit(): void {
   
   }

  onAnchorClicked(urlSegment:string){
    if(urlSegment=='landingPage'){
      if (this.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/lead-info/view']);
      }
      else if (this.fromUrl == 'My Leads List') {
        this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/view']);
      }
    }
    else{
      this.router.navigateByUrl(urlSegment);
    }
  }
}
