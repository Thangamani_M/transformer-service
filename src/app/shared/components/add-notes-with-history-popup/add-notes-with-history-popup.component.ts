import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from "@app/reducers";
import {
  formConfigs, setRequiredValidator
} from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, prepareRequiredHttpParams } from '@app/shared/utils';
import { loggedInUserData } from "@modules/others/auth.selectors";
import { Store } from "@ngrx/store";
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { take } from 'rxjs/operators';

@Component({
  selector: 'add-notes-with-history-popup.component',
  templateUrl: './add-notes-with-history-popup.component.html',
  styleUrls: ['./add-notes-with-history-popup.component.scss']
})

export class ReusableAddNotesWithHistoryPopupComponent implements OnInit {
  formConfigs = formConfigs;
  leadNotes = "";
  addNotesForm: FormGroup;
  loggedInUserData: LoggedInUserModel;

  constructor(private httpService: CrudService, private rxjsService: RxjsService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService, @Inject(MAT_DIALOG_DATA) public data) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit(): void {
    this.createAddNotesForm();
    this.rxjsService.setDialogOpenProperty(true);
    this.getNotes();
  }

  createAddNotesForm(): void {
    this.addNotesForm = this.formBuilder.group({});
    Object.keys(this.data.addNotesModel).forEach((key) => {
      this.addNotesForm.addControl(key, new FormControl(this.data.addNotesModel[key]));
    });
    this.addNotesForm = setRequiredValidator(this.addNotesForm, [this.data.commentsPropName]);
  }

  getNotes() {
    this.httpService.get(this.data.modulesBasedApiSuffix,
      this.data.moduleApiSuffixModels, undefined, false,
      prepareRequiredHttpParams({
        boundaryId: this.data.addNotesModel.boundaryId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.leadNotes = response.resources;
        }
      });
  }

  createNotes() {
    if (this.addNotesForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.httpService.create(this.data.modulesBasedApiSuffix,
      this.data.moduleApiSuffixModels,
      this.addNotesForm.value, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.getNotes();
          this.createAddNotesForm();
        }
      });
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}