import { Component, OnInit } from '@angular/core';
import { CrudService } from '@app/shared';
import { IApplicationResponse } from '@app/shared/utils';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-primeng-custom-dialog',
  templateUrl: './primeng-custom-dialog.component.html',
})
export class PrimengCustomDialogComponent implements OnInit {

  selectedRow: any;
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private crudService: CrudService) { }

  ngOnInit(): void {
  }

  close(action) {
    this.ref.close(action);
  }

}
