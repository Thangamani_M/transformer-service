import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material";
import { AppState } from "@app/reducers";
import { currentComponentPageBasedPermissionsSelector$, PermissionTypes, ResponseMessageTypes, retrieveUniqueObjectsWithProperty, SnackbarService } from "@app/shared";
import Boundary_Management_Components from "@modules/boundary-management/shared/utils/boundary-management-module.enums";
import { MY_TASK_COMPONENT } from "@modules/my-tasks/shared";
import { BoundaryStatus } from "@modules/sales/shared/utils/sales-module.enums";
import { Store } from "@ngrx/store";
import * as L from "leaflet";
import "leaflet-draw";
import { combineLatest } from "rxjs";
import { RxjsService } from '../../services/rxjs.services';
import {
	defaultLeafletLatitude,
	defaultLeafletLongitude,
	defaultLeafLetMapZoomValue,
	leaflefApiKey,
	PERMISSION_RESTRICTION_ERROR
} from "../../utils/variables.utils";
import { LeafLetFullMapViewModalComponent } from "./leaf-let-full-map-view.component";
//declare let L: any;
import { Control } from 'leaflet';
drawControl: Control.Draw;

const editedCoordinatesColor = '#ffcccb';
const editedCoordinatesColorBefore = 'red';
const editedCoordinatesColorAfter = 'yellow';
@Component({
	selector: "app-shared-leaf-let",
	templateUrl: "./shared-leaf-let.component.html",
	styleUrls: ['./shared-leaf-let.component.scss']
})
export class SharedLeafLetComponent implements OnInit {
	@Input() polygonArray;
	@Input() boundaryRequestDetails;
	@Input() isDisabled = false;
	@Input() isResetBtnDisabled = true;
	@Input() isFileUploaded = false;
	@Input() latLong;
	@Input() shouldShowFullViewIcon = true;
	@Input() isUploaded;
	@Input() id = "map_modal";
	@Input() caption = "";
	@Input() subCaption = "";
	@Input() isDisabledFullView = false;
	@Input() shouldEnableDownload = true;
	@Input() shouldShowLegend = true;
	@Output() onSelection = new EventEmitter();
	@Output() onMapSubmitAction = new EventEmitter();
	@Output() afterDataRenderedInMapAction = new EventEmitter();
	@Input() boundaries = [];
	container;
	map;
	drawControl: Control.Draw;
	editableLayers = new L.FeatureGroup();
	openedPopup: L.Popup;
	selectedLayer;
	selectedDownloadableProps = [];
	boundaryStatus = BoundaryStatus;
	createdRectangles = [];
	prevLayerKey;
	markersLayer = new L.LayerGroup();
	shouldHidePolygonDrawShape: boolean;
	isEditAccessDenied = true;

	constructor(private rxjsService: RxjsService, private dialog: MatDialog, private store: Store<AppState>,
		private snackbarService: SnackbarService) {
		this.loaderConfig(true);
	}

	loaderConfig(bool) {
		if (this.isDisabledFullView) {
			this.rxjsService.setPopupLoaderProperty(bool);
		}
		else {
			this.rxjsService.setGlobalLoaderProperty(bool);
		}
	}

	ngOnInit(): void {
		this.combineLatestNgrxStoreRolesData();
		this.setupLeafLetMapConfigs();
	}

	combineLatestNgrxStoreRolesData() {
		combineLatest([
			this.store.select(currentComponentPageBasedPermissionsSelector$)]
		).subscribe((response) => {
			let sidebarComponent = this.caption == 'sales' ? Boundary_Management_Components.SALES_BOUNDARIES :
				this.caption == 'technical' ? Boundary_Management_Components.TECHNICAL_BOUNDARIES :
					this.caption == 'operations' ? Boundary_Management_Components.OPERATIONAL_BOUNDARIES :
						this.caption == 'DOAApprovalDashboard' ? MY_TASK_COMPONENT.BOUNDARY_MGMT_APPROVAL_DASHBOARD :
							this.caption == 'MyRequestDashboard' ? MY_TASK_COMPONENT.BOUNDARY_MGMT_VIEW_REQUEST : "";
			this.isEditAccessDenied = response[0][sidebarComponent]?.find(fSC => fSC.menuName === PermissionTypes.EDIT) ? false : true;
		});
	}

	setupLeafLetMapConfigs() {
		setTimeout(() => {
			this.loaderConfig(true);
			// Check if map already exists then remove after that recreate the map
			if (this.map) {
				this.map.remove();
			}
			// Create a container using element's id
			this.container = document.getElementById(this.id);
			// create a map using created element's container using default options like zoom and lat,long values
			this.map = L.map(this.container).setView(
				[defaultLeafletLatitude, defaultLeafletLongitude],
				defaultLeafLetMapZoomValue
			);
			// create an additional view layer for better representation purpose

			// normal view layer
			// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').
			// addTo(this.map);

			// Customer's paid view layer
			let customPaidTileLayer = L.tileLayer(
				"https://api.maps.geoint.africa/v1/" +
				leaflefApiKey +
				"/tile/{x}/{y}/{z}.png?settings=global-decarta_TT"
			);
			// Satellite view layer based on google's open API
			let googleSatelliteViewTileLayer = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
				maxZoom: 20,
				subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
			});
			let baseMaps: L.Control.LayersObject = {
				"Normal View": customPaidTileLayer,
				"Satellite View": googleSatelliteViewTileLayer
			};
			L.control.layers(baseMaps, null, { position: 'bottomleft' }).addTo(this.map);
			// Select default view as custom paid layer
			customPaidTileLayer.addTo(this.map);
			if (this.shouldShowLegend) {
				this.onAddLegend();
			}
			// callback event for tile layer once all map related coordinates are successfully rendered into the map
			customPaidTileLayer.on('load', () => {
				this.loaderConfig(false);
			}, this);
			this.map.addLayer(this.editableLayers);
			this.map.addLayer(this.markersLayer);
			switch (this.caption) {
				case "Two Table Architecture Exact Address":
					this.prepareDrawableMarker();
					break;
				case "Two Table Architecture Custom Address":
					this.prepareDrawableMarker();
					break;
				default:
					this.prepareDrawableShapes();
					break;
			}
		});
	}

	// draw marker shape only
	prepareDrawableMarker() {
		this.markersLayer.clearLayers();
		this.latLong = this.latLong ? this.latLong : { lat: defaultLeafletLatitude, lng: defaultLeafletLongitude }; // second value is hardcoded
		this.map.setView([this.latLong.lat, this.latLong.lng], 8);
		L.Icon.Default.mergeOptions({
			iconRetinaUrl: require("../../../../../node_modules/leaflet/dist/images/marker-icon-2x.png"),
			iconUrl: require("../../../../../node_modules/leaflet/dist/images/marker-icon.png"),
			shadowUrl: require("../../../../../node_modules/leaflet/dist/images/marker-shadow.png")
		});
		let marker = L.marker([this.latLong.lat, this.latLong.lng], { draggable: true });
		this.markersLayer.addLayer(marker);
		marker.on('dragend', function (e) {
			this.latLong = { lat: marker.getLatLng().lat, lng: marker.getLatLng().lng };
			this.onSelection.emit({ data: { lat: this.latLong.lat, lng: this.latLong.lng } });
		}, this);
		marker.on('click', function (e) {
			this.openedPopup = L.popup()
				.setLatLng(e['latlng'])
				.setContent(`<strong>Latitude : ${e['latlng'].lat.toFixed(4)}</strong> <br> 
									<strong>Longitude : ${e['latlng'].lng.toFixed(4)}</strong>`)
				.openOn(this.map);
		}, this);
		marker.on('mouseout', function (e) {
			setTimeout(() => {
				this.map.closePopup(this.openedPopup);
			}, 1500);
		}, this);
	}

	// draw shapes like polygon, circle, rectangle etc
	prepareDrawableShapes() {
		const downloadEditableLayers = new L.FeatureGroup();
		let polyLayers = [];
		polyLayers = this.polygonArray;
		let currentEditable = null;
		this.preparePolygonShapeToBeShown();
		for (let layers of polyLayers) {
			let layer = layers,
				feature = (layer.feature = layer.feature || {});
			feature.type = feature.type || "Feature";
			feature.properties = layer.options;
			// fill color based on Batch Ids to group the layers
			layer.on('add', (e) => {
				if (layer.options.BatchId) {
					if (layer.options.BatchId == this.boundaryRequestDetails.batchId) {
						L.Icon.Default.mergeOptions({
							iconRetinaUrl: require("../../../../../node_modules/leaflet/dist/images/marker-icon-2x.png"),
							iconUrl: require("../../../../../node_modules/leaflet/dist/images/marker-icon.png"),
							shadowUrl: require("../../../../../node_modules/leaflet/dist/images/marker-shadow.png")
						});
						// marker sits at the top right of the boundary
						let marker = L.marker(e.target._bounds._northEast);
						this.markersLayer.addLayer(marker);
						marker.on('mouseover', function (e) {
							this.openedPopup = L.popup()
								.setLatLng(e['latlng'])
								.setContent(`<strong>Recently Created/Updated boundary</strong>`)
								.openOn(this.map);
						}, this);
						marker.on('mouseout', function (e) {
							setTimeout(() => {
								this.map.closePopup(this.openedPopup);
							}, 1500);
						}, this);
						var southWest = new L.LatLng(e.target._bounds._southWest.lat, e.target._bounds._southWest.lng),
							northEast = new L.LatLng(e.target._bounds._northEast.lat, e.target._bounds._northEast.lng),
							bounds = new L.LatLngBounds(southWest, northEast);
						this.map.fitBounds(bounds);
						e.target.setStyle({
							fillColor: this.prepareEditedBoundaryColor(layer.options),
							fillOpacity: 0.3,
							borderColor: this.prepareEditedBoundaryColor(layer.options),
							weight: 3
						});
					}
				}
				else if (layer?.options?.type == 'Bounding Box') {
					this.isResetBtnDisabled = true;
					e.target.setStyle({
						fillColor: 'lightgreen',
						fillOpacity: 0.5,
						borderColor: 'lightgreen',
						weight: 3
					});
					this.prepareDownloadRequestIdsLogic(layer, 'add');
				}
			});
			this.editableLayers.addLayer(layer);
			layer.on("click", (e) => {
				if (this.isDisabled || e.target.feature.properties.BatchId !== this.boundaryRequestDetails.batchId || this.shouldHidePolygonDrawShape) {
					return;
				}
				currentEditable = e.target;
				let isEditable = downloadEditableLayers.getLayer(e.target._leaflet_id) == undefined;
				if (downloadEditableLayers.getLayer(e.target._leaflet_id) != undefined) {
					currentEditable.editing.disable();
					downloadEditableLayers.removeLayer(e.target);
				}
				if (isEditable && this.caption !== 'GISDashboard' && this.caption !== 'DOAApprovalDashboard') {
					e.target.editing.enable();
					downloadEditableLayers.addLayer(currentEditable);
				}
			}, this);
			layer.on('mouseover', (e) => {
				this.selectedLayer = layer;
				this.onLayerMouseOver(e)
			}, this);
			layer.on('mouseout', this.onLayerMouseOut, this);
		}
		this.drawControl = new L.Control.Draw({
			draw: {
				circle: false,
				polygon: this.shouldHidePolygonDrawShape ? false : {
					allowIntersection: false, // Restricts shapes to simple polygons
					drawError: {
						color: "#e1e100", // Color the shape will turn when intersects
						message: "<strong>We cannot draw the intersection among the polygons<strong>", // Message that will show when intersect
					},
					shapeOptions: {
						color: "#97009c",                     //   color code newly created polygon
					},
				},
				polyline: false,
				marker: false,
				circlemarker: false,
				rectangle: this.caption == 'GISDashboard' && !this.shouldShowFullViewIcon ? {
					metric: false,                                     // to enable the rectangle is drawn correctly without any error
					shapeOptions: { color: 'lightgreen' }
				} : false,
			},
			edit: {
				edit: false,
				featureGroup: this.editableLayers,
				remove: false                                      // enable delete option if the option is commented or disable delete option if the value is null
			},
		});
		// add custom tooltip on hovering rectangle shape
		L.drawLocal.draw.toolbar.buttons.rectangle = 'Bounding box';
		// Add draw control to leaflet map if the component is enabled only
		if (!this.isDisabled) {
			this.map.addControl(this.drawControl);
		}
		this.map.on(L.Draw.Event.CREATED, (e) => {
			// total actions for newly created polygon
			if (e.layerType == 'polygon') {
				let type = e["layerType"],
					layer = e["layer"],
					feature = (layer.feature = layer.feature || {});
				feature.type = feature.type || "Feature";
				let props = (feature.properties = feature.properties || {});
				// create custom props 
				props.type = 'create'
				props.Name = "New Boundary";
				props.Id = `TEMP-NO${Math.floor((Math.random() * 10000000000) + 1)}`;   // random number for polygon identity create process
				props.BatchId = null;
				this.editableLayers.addLayer(layer);
				selectedData(this);
				let selectedFeature = null;
				layer.on("click", (e) => {
					let isEditable = downloadEditableLayers.getLayer(e.target._leaflet_id) == undefined;
					if (downloadEditableLayers.getLayer(e.target._leaflet_id) != undefined) {
						selectedFeature.editing.disable();
						downloadEditableLayers.removeLayer(e.target);
					}
					if (isEditable) {
						selectedFeature = e.target;
						e.target.editing.enable();
						downloadEditableLayers.addLayer(selectedFeature);
					}
				});
				layer.on('mouseover', (e) => {
					this.selectedLayer = layer;
					this.onLayerMouseOver(e)
				}, this);
				layer.on('mouseout', this.onLayerMouseOut, this);
				let data = this.editableLayers.toGeoJSON();
				L.geoJSON(data, {
					onEachFeature: (feature, layer) => {
						layer.bindPopup(feature.properties.description);
					},
				});
			}
			// total actions for newly created rectangle ( this shape is created for only downloading drawn area coordinates ) 
			else if (e.layerType == 'rectangle') {
				let type = e["layerType"],
					layer = e["layer"],
					feature = (layer.feature = layer.feature || {});
				feature.type = feature.type || "Feature";
				let props = (feature.properties = feature.properties || {});
				// create custom props 
				props.type = 'create rectangle';
				props.Id = `TEMP-NO-RECTANGLE${Math.floor((Math.random() * 10000000000) + 1)}`;   // random number for polygon identity create process
				this.editableLayers.addLayer(layer);
				this.prepareDownloadRequestIdsLogic(layer, 'create');
			}
		}, this);

		// emit properties on every single adjustments of edited polygon's coordinates
		this.map.on(L.Draw.Event.EDITVERTEX, (e) => {
			this.onSelection.emit({ data: e.poly.toGeoJSON() });
		}, this);

		// emit properties of all after deletion of selected polygons
		this.map.on(L.Draw.Event.DELETED, (e) => {
			this.onSelection.emit({ data: this.editableLayers.toGeoJSON() });
		}, this);

		// once all latitude and longitude is collected then emit all data to parent component ( only one drawn shape's coordinates )
		function selectedData(that) {
			that.onSelection.emit({ data: that.editableLayers.toGeoJSON() });
		}
		this.afterDataRenderedInMapAction.emit(true);
	}

	prepareEditedBoundaryColor(props): string {
		switch (true) {
			case !props?.modificationType:
				return editedCoordinatesColor;
			case props?.modificationType == 'Before':
				return editedCoordinatesColorBefore;
			case props?.modificationType == 'After':
				return editedCoordinatesColorAfter;
		}
	}

	preparePolygonShapeToBeShown() {
		if (this.shouldShowFullViewIcon) {
			this.shouldHidePolygonDrawShape = true;
		}
		else if (this.caption == 'LSS Boundary' && this.boundaryRequestDetails?.boundaryStatusId == null) {
			this.shouldHidePolygonDrawShape = false;
		}
		else if (this.caption == 'GISDashboard' || this.subCaption ||
			(this.boundaryRequestDetails?.boundaryStatusId !== BoundaryStatus.INITIATED &&
				this.caption !== 'sales' && this.caption !== 'technical' && this.caption !== 'operations' &&
				this.boundaryRequestDetails?.boundaryStatusId !== BoundaryStatus.REJECTED) ||
			(this.boundaryRequestDetails?.boundaryStatusId !== BoundaryStatus.LIVE &&
				(this.caption == 'sales' || this.caption == 'technical' || this.caption == 'operations'))) {
			this.shouldHidePolygonDrawShape = true;
		}
	}

	prepareDownloadRequestIdsLogic(layer, type: string) {
		Object.keys(this.editableLayers['_layers']).forEach((layerKey) => {
			if (type == 'create' && this.editableLayers['_layers']?.[layerKey]?.['feature']?.['properties']?.['Id'] == layer['feature']?.['properties']?.['Id']) {
				delete this.editableLayers['_layers']?.[layerKey];
			}
			if (this.editableLayers['_layers']?.[layerKey]?.['feature']?.['properties']?.['type'] == "create rectangle" && type == 'create') {
				// prepare created rectangle's lat and long
				let rectangle = { name: 'rectangle', bounds: [], coordinates: {}, Id: "" };
				let geoJson = this.editableLayers['_layers'][layerKey].toGeoJSON();
				rectangle.coordinates = geoJson['geometry']['coordinates'];
				rectangle.Id = geoJson['properties']['Id'];
				this.editableLayers['_layers'][layerKey]._latlngs[0].forEach((latLng: L.LatLng) => {
					rectangle.bounds.push({ x: latLng.lng, y: latLng.lat });
				});
				this.createdRectangles.push(rectangle);
			}
			// prepare created rectangle's lat and long
			else if (this.editableLayers['_layers']?.[layerKey]?.['feature']?.['properties']?.['type'] == "Bounding Box" && type == 'add') {
				if (!this.prevLayerKey || this.prevLayerKey !== layerKey) {
					let rectangle = { name: 'rectangle', bounds: [], coordinates: {}, Id: "" };
					let geoJson = this.editableLayers['_layers'][layerKey].toGeoJSON();
					rectangle.coordinates = geoJson['geometry']['coordinates'];
					rectangle.Id = `TEMP-NO-RECTANGLE${Math.floor((Math.random() * 10000000000) + 1)}`;
					this.editableLayers['_layers'][layerKey]._latlngs[0].forEach((latLng: L.LatLng) => {
						rectangle.bounds.push({ x: latLng.lng, y: latLng.lat });
					});
					this.createdRectangles.push(rectangle);
					this.prevLayerKey = layerKey;
				}
			}
		});
		let fallenTestCoorinates = [];
		// Loop through all layers and prepare lat long and its props to push to isFallenTestCoorinates
		Object.keys(this.editableLayers['_layers']).forEach((layerId) => {
			fallenTestCoorinates.push({
				lng: this.editableLayers['_layers'][layerId]._bounds._northEast.lng,
				lat: this.editableLayers['_layers'][layerId]._bounds._northEast.lat,
				properties: this.editableLayers['_layers'][layerId].feature.properties
			})
		});
		this.prepareSelectedDownloadableProps(fallenTestCoorinates, type);
	}

	onAddLegend() {
		let legend = L.control.attribution({ position: "topright" });
		legend.onAdd = (map) => {
			let div = L.DomUtil.create('div', 'info legend');
			let labels = [],
				categories = this.caption == 'GISDashboard' ? ['New Boundary', 'Pending - Go Live', 'Approved Boundaries', 'Bounding Box'] :
					['New Boundary', 'Pending - Go Live', 'Approved Boundaries'];
			for (let i = 0; i < categories.length; i++) {
				div.innerHTML +=
					labels.push(
						`<i class="fa fa-square" aria-hidden="true" style="color:${this.getColorForLegend(categories[i])};
						fillOpacity:1"></i>
						${categories[i]}`);
			}
			div.innerHTML = labels.join('<br>');
			return div;
		};
		legend.addTo(this.map);
	}

	getColorForLegend(d) {
		return d === 'New Boundary' ? "#97009c" :
			d === 'Pending - Go Live' ? editedCoordinatesColor :
				d === 'Approved Boundaries' ? "#3388ff" : d === 'Bounding Box' ? 'lightgreen' : '#000';
	}

	onLayerMouseOver(e) {
		if (this.map && e?.target?.options?.type !== 'Bounding Box') {
			this.openedPopup = L.popup()
				.setLatLng(L.latLng(e.latlng.lat, e.latlng.lng))
				.setContent(`<strong>${e.target.feature.properties.Name}</strong>`)
				.openOn(this.map);
			let setHighlightStyle = {};
			// let groupByBatchIdColor = e.target.options.BatchId &&
			// 	this.groupByBatchIds.find(g => g['batchId'] == e.target.options.BatchId).fillColor;

			// edited polygon
			if (e.target.hasOwnProperty('edited') && e.target.edited && e.target.options.Id) {
				setHighlightStyle = {
					fillColor: this.prepareEditedBoundaryColor(e.target.options),
					color: this.prepareEditedBoundaryColor(e.target.options),
					fillOpacity: 0.3,
					weight: 6,
					borderColor: this.prepareEditedBoundaryColor(e.target.options)
				}
			}
			// newly created polygon
			else if (e.target.feature.properties.hasOwnProperty('type')) {
				setHighlightStyle = {
					fillColor: '#97009c',
					fillOpacity: 0.3,
					weight: 6
				};
			}
			// existing polygon
			else {
				setHighlightStyle = {
					fillColor: (e.target.feature.properties.fillColor == this.prepareEditedBoundaryColor(e.target.options)) ? e.target.feature.properties.fillColor : '#3388ff',
					fillOpacity: 0.3,
					weight: 6
				}
			}
			e.target.setStyle(setHighlightStyle);
		}
		// bounding box
		if (e?.target?.options?.type == 'Bounding Box') {
			e.target.setStyle({
				fillColor: 'lightgreen',
				color: 'lightgreen',
				fillOpacity: 0.3,
				weight: 6,
				borderColor: 'lightgreen'
			});
		}
	}

	onLayerMouseOut(e) {
		if (this.openedPopup && this.map) {
			this.map.closePopup(this.openedPopup);
			let resetHighlighedStyle = {};
			// let groupByBatchIdColor = e.target.options.BatchId &&
			// 	this.groupByBatchIds.find(g => g['batchId'] == e.target.options.BatchId).fillColor;
			// edited polygon
			if (e.target.hasOwnProperty('edited') && e.target.edited && e.target.options.Id) {
				resetHighlighedStyle = {
					fillColor: this.prepareEditedBoundaryColor(e.target.options),
					color: this.prepareEditedBoundaryColor(e.target.options),
					weight: 3,
					borderColor: this.prepareEditedBoundaryColor(e.target.options)
				}
			}
			// newly created polygon
			else if (e.target.feature.properties.hasOwnProperty('type')) {
				resetHighlighedStyle = {
					fillColor: '#97009c',
					weight: 3
				};
			}
			// existing polygon
			else {
				resetHighlighedStyle = {
					fillColor: (e.target.feature.properties.fillColor == this.prepareEditedBoundaryColor(e.target.options)) ?
						e.target.feature.properties.fillColor : '#3388ff',
					weight: 3
				}
			}
			e.target.setStyle(resetHighlighedStyle);
			this.openedPopup = null;
			//this.selectedLayer = null;
		}
		// bounding box
		if (e?.target?.options?.type == 'Bounding Box') {
			e.target.setStyle({
				fillColor: 'lightgreen',
				color: 'lightgreen',
				weight: 3,
				borderColor: 'lightgreen'
			});
		}
	}

	containsLatLong(bounds, lat, lng) {
		let count = 0;
		for (let b = 0; b < bounds.length; b++) {
			let vertex1 = bounds[b];
			let vertex2 = bounds[(b + 1) % bounds.length];
			if (west(vertex1, vertex2, lng, lat))
				++count;
		}
		return count % 2;
		/**
		 * @return {boolean} true if (x,y) is west of the line segment connecting A and B
		 */
		function west(A, B, x, y) {
			if (A.y <= B.y) {
				if (y <= A.y || y > B.y ||
					x >= A.x && x >= B.x) {
					return false;
				} else if (x < A.x && x < B.x) {
					return true;
				} else {
					return (y - A.y) / (x - A.x) > (B.y - A.y) / (B.x - A.x);
				}
			} else {
				return west(B, A, x, y);
			}
		}
	}

	prepareSelectedDownloadableProps(fallenTestCoorinates, type: string) {
		let boundingBoxRectangles = [];
		for (let s = 0; s < this.createdRectangles.length; s++) {
			let rectangle = this.createdRectangles[s];
			for (let i = 0; i < fallenTestCoorinates.length; i++) {
				let coordinates = fallenTestCoorinates[i];
				if (this.containsLatLong(rectangle.bounds, coordinates.lat, coordinates.lng) == 1) {
					if (coordinates.properties?.['modificationType']) {
						if (coordinates.properties?.['modificationType'] == 'After') {
							this.selectedDownloadableProps.push(coordinates.properties.boundaryRequestId);
						}
					}
					else {
						this.selectedDownloadableProps.push(coordinates.properties.boundaryRequestId);
					}
					boundingBoxRectangles.push(rectangle);
				}
			}
		}
		boundingBoxRectangles = retrieveUniqueObjectsWithProperty(boundingBoxRectangles, 'Id');
		this.onSelection.emit({ selectedDownloadableProps: [...new Set(this.selectedDownloadableProps)], boundingBoxRectangles, type });
	}

	ngOnChanges(changes: SimpleChanges) {
		for (let prop in changes) {
			switch (prop) {
				case "polygonArray":
					this.editableLayers.clearLayers();
					this.markersLayer.clearLayers();
					this.setupLeafLetMapConfigs();
					break;
				case "isUploaded":
					if (this.isUploaded) {
						this.onSelection.emit({ data: this.editableLayers.toGeoJSON() });
					}
					break;
				case "shouldShowFullViewIcon":
					// remove later this logic
					if (this.shouldShowFullViewIcon) {
						this.isDisabled = true;
					}
					else {
						this.isDisabled = false;
					}
					break;
			}
		}

		if (!this.isDisabled && this.map && this.drawControl) {
			this.map.addControl(this.drawControl);
		} else if (!this.isDisabled && this.isFileUploaded && !this.map) {
			this.ngOnInit();
		}
	}

	openFullMapViewWithConfig() {
		// if (this.isEditAccessDenied) {
		// 	this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
		// }
		// else {
			this.rxjsService.setDialogOpenProperty(true);
			const dialogReff = this.dialog.open(LeafLetFullMapViewModalComponent, {
				width: "100vw",
				maxHeight: "100vh",
				disableClose: true,
				data: this.subCaption ? {
					polygonArray: this.polygonArray,
					subCaption: this.subCaption
				} :
					{
						fromUrl: this.caption,
						boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
						boundaryRequestRefNo: this.boundaryRequestDetails.boundaryRequestRefNo ?
							this.boundaryRequestDetails.boundaryRequestRefNo : this.boundaryRequestDetails.boundaryReqRefNo,
						isDisabled: this.isDisabled,
						boundaryRequestDetails: this.boundaryRequestDetails,
						boundaries: this.boundaries
					},
			});
			dialogReff.afterClosed().subscribe((result) => {
				if (result?.hasOwnProperty('boundaries')) {
					this.onMapSubmitAction.emit(result);
				}
				this.rxjsService.setGlobalLoaderProperty(false);
			});
		// }
	}
}
