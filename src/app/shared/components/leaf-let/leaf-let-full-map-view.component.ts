import { DatePipe } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { PERMISSION_RESTRICTION_ERROR, prepareCoordinatesForLeafLetMapComponent } from '@app/shared/utils';
import { MY_TASK_COMPONENT } from '@modules/my-tasks/shared/utils/my-tasks-components.enum';
import { Store } from '@ngrx/store';
import * as FileSaver from 'file-saver';
import "leaflet";
import 'leaflet-draw';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import * as shp from "shpjs";
import { loggedInUserData } from '../../../modules/others/auth.selectors';
import { BoundaryApprovalStatus, BoundaryStatus, SalesModuleApiSuffixModels } from '../../../modules/sales/shared/utils/sales-module.enums';
import { ReusablePrimeNGTableFeatureService } from '../../../shared/services/reusable-primeng-table-features.service';
import { ResponseMessageTypes } from '../../enums/enum';
import { currentComponentPageBasedPermissionsSelector$ } from '../../layouts/layout.selectors';
import { CrudService } from '../../services/crud.service';
import { HttpCancelService } from '../../services/http-cancel.service';
import { RxjsService } from '../../services/rxjs.services';
import { SnackbarService } from '../../services/snackbar.service';
import { ModulesBasedApiSuffix, PermissionTypes } from '../../utils/enums.utils';
import { IApplicationResponse } from '../../utils/interfaces.utils';
import { LoggedInUserModel } from '../../utils/models/logged-in-user.model';

declare let L;
declare let $;
@Component({
  selector: 'app-full-map-view',
  templateUrl: './leaf-let-full-map-view.component.html',
  styleUrls: ["./shared-leaf-let.component.scss"],
})
export class LeafLetFullMapViewModalComponent implements OnInit {
  polygonArray = [];
  boundaryRequestId = "";
  boundaryRequestDetails: any = {
    boundaryRequestId: "", boundaryTypeId: "", description: "", goLiveDate: new Date(), boundaryTypeName: "", divisionName: "", boundaryName: "",
    comments: "", boundaryStatusId: 2, createdUserId: "", boundaryId: "", batchId: "", boundaryLayerName: "", branchName: "", boundaryRequestRefNo: "",
    isBoundaryGoLive: false, approverStatusId: "", boundaryReqRefNo: "", lssId: "", boundingBoxCoordinates: "", boundaryLayerId: ""
  };
  todayDate = new Date();
  // revert to true later
  isDisabled = false;
  isDownloaded = false;
  isUploaded = false;
  isFileUploaded = false;
  boundaries = [];
  tempBoundaries = [];
  showElement = false;
  loggedInUserData: LoggedInUserModel;
  boundaryStatus = BoundaryStatus;
  boundaryApprovalStatus = BoundaryApprovalStatus;
  emittedValuesCount = 0;
  fromUrl = "";
  polyLayers = [];
  localZipFile;
  serverPathForZipFile = "";
  boundaryRequestRefNo = "";
  declineReason = "";
  @ViewChild('decline_reason', { static: false }) decline_reason: ElementRef<any>;
  @ViewChild('decline_reason_for_lss', { static: false }) decline_reason_for_lss: ElementRef<any>;
  message = "";
  isBoundaryRequestCreated = false;
  isEditBtnDisabled = false;
  newlyCreatedMultiPolygons = [];
  editedMultiPolygons = [];
  areMapCoordinatesDrawn = false;
  isMapDisabled = true;
  isBoundaryRequestDownloadedSuccessful = false;
  isResetBtnDisabled = true;
  boundariesCopy = [];
  boundingBoxes = [];
  selectedDownloadableProps = [];
  isDownloadBtnDisabled = true;
  boundingBoxCoordinates = "";
  latLong;
  shouldShowLegend = true;
  isAReadOnlyFullMapView = false;
  shouldResetBtnShow = true;
  filteredPermissions = [];

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private route: Router,
    private httpCancelService: HttpCancelService, private store: Store<AppState>,
    private snackbarService: SnackbarService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data, private dialogRef: MatDialogRef<LeafLetFullMapViewModalComponent>) {
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setPopupLoaderProperty(true);
    if (data?.subCaption) {
      this.polygonArray = data.polygonArray;
      this.isAReadOnlyFullMapView = true;
      this.showElement = true;
      this.shouldShowLegend = false;
      this.rxjsService.setPopupLoaderProperty(false);
    }
    else {
      this.isAReadOnlyFullMapView = false;
      this.fromUrl = data.fromUrl;
      this.boundaryRequestId = data.boundaryRequestId;
      this.boundaryRequestRefNo = data.boundaryRequestRefNo;
      this.boundaryRequestDetails = data.boundaryRequestDetails;
      this.boundaries = data.boundaries;
      this.boundariesCopy = data.boundaries;
      this.latLong = data.latLong;
      this.shouldShowLegend = (this.fromUrl == 'Two Table Architecture Custom Address' || this.fromUrl == 'Two Table Architecture Exact Address') ? false : true;
      //uncomment later
      //this.isDisabled = data.isDisabled;
      if (this.fromUrl == 'LSS Boundary') {
        this.shouldResetBtnShow = (this.boundaryRequestDetails?.boundaryStatusId == null || this.boundaryRequestDetails?.boundaryStatusId == this.boundaryStatus.REJECTED) ? true : false;
      }
      this.combineLatestNgrxStoreData();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData)])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        if (this.fromUrl !== 'Two Table Architecture Custom Address' && this.fromUrl !== 'Two Table Architecture Exact Address') {
          this.getGisBoundaryDetailsByBoundaryId();
        }
        else {
          this.showElement = true;
        }
      });
  }

  ngOnInit() {
    if (this.fromUrl == 'MyRequestDashboard' || this.fromUrl == 'DOAApprovalDashboard') {
      this.combineLatestNgrxStoreRolesData();
    }
    this.onBootstrapModalEventChanges();
  }

  combineLatestNgrxStoreRolesData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let key = this.fromUrl == 'DOAApprovalDashboard' ? MY_TASK_COMPONENT.BOUNDARY_MGMT_APPROVAL_DASHBOARD :
        this.fromUrl == 'MyRequestDashboard' ? MY_TASK_COMPONENT.BOUNDARY_MGMT_VIEW_REQUEST : "";
      this.filteredPermissions = response[0][key];
    });
  }

  onBootstrapModalEventChanges(): void {
    $("#decline_reason").on("shown.bs.modal", (e) => {
      this.declineReason = "";
    });
    $("#decline_reason").on("hidden.bs.modal", (e) => {
    });
    $("#decline_reason_for_lss").on("shown.bs.modal", (e) => {
      this.declineReason = "";
    });
    $("#decline_reason_for_lss").on("hidden.bs.modal", (e) => {
    });
  }

  getGisBoundaryDetailsByBoundaryId(type = 'get') {
    if (type == 'reset' || type == 'reset and close') {
      if (type == 'reset and close' && (this.isResetBtnDisabled || !this.shouldResetBtnShow)) {
        this.dialogRef.close();
        return;
      }
      let message = type == 'reset' ? `Are you sure you wish to <strong>Reset</strong> the ${this.fromUrl == 'GISDashboard' ? 'Bounding Box' : 'Map'}?` :
        `Your changes will be lost. Are you sure?`;
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(message).
        onClose?.subscribe(dialogResult => {
          if (!dialogResult) return;
          if (type == 'reset and close') {
            this.dialogRef.close({
              fromUrl: this.fromUrl,
              boundaryRequestId: this.boundaryRequestId,
              boundaryRequestRefNo: this.boundaryRequestRefNo,
              boundaryRequestDetails: this.boundaryRequestDetails,
              boundaries: this.boundaries,
              boundariesCopy: this.boundaries,
              isDisabled: this.isDisabled,
            });
          }
          else {
            this.prepareResetMapCoordinates();
          }
        });
    }
    else {
      if ((this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.DOWNLOADED || this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.UPLOADED)
        && this.fromUrl == 'GISDashboard') {
        this.isBoundaryRequestDownloadedSuccessful = true;
      }
      if (this.boundaryRequestDetails.goLiveDate) {
        let exactGoLiveDateTime = new Date(this.boundaryRequestDetails.goLiveDate).getTime();
        let goLiveDate = new Date(this.boundaryRequestDetails.goLiveDate).setHours(0, 0, 0, 0);
        let todayDate = new Date().setHours(0, 0, 0, 0);
        if (todayDate - goLiveDate === 0) {
          let actualGoLiveDate = new Date(goLiveDate);
          actualGoLiveDate.setTime(exactGoLiveDateTime);
          this.boundaryRequestDetails.goLiveDate = new Date(actualGoLiveDate);
        }
        else if (todayDate - goLiveDate > 0) {
          this.boundaryRequestDetails.goLiveDate = new Date();
        }
      }
      this.prepareButtonStatuses();
      if (this.boundaries.length > 0) {
        let data = prepareCoordinatesForLeafLetMapComponent(this.boundaries);
        this.polyLayers = [];
        this.polygonArray = [];
        this.showElement = false;
        data.forEach((feature) => {
          if (feature.coordinates.length == 1) {
            this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
          }
          else {
            feature.coordinates.forEach((coordinate) => {
              this.polyLayers.push(L.polygon(coordinate, feature.properties));
            })
          }
        });
        setTimeout(() => {
          this.showElement = true;
          this.polygonArray = this.polyLayers;
        }, 3000);
      } else {
        this.showElement = true;
        this.polygonArray = [];
      }
      if (this.boundaryRequestDetails?.boundingBoxCoordinates && this.fromUrl == 'GISDashboard') {
        this.isDownloadBtnDisabled = false;
        this.isResetBtnDisabled = true;
        let data = prepareCoordinatesForLeafLetMapComponent(this.boundaries, this.boundaryRequestDetails?.boundingBoxCoordinates, 'boundingBox');
        this.showElement = false;
        data.forEach((feature) => {
          this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
        });
      }
    }
  }

  prepareResetMapCoordinates() {
    this.rxjsService.setPopupLoaderProperty(true);
    this.polyLayers = [];
    this.polygonArray = [];
    this.areMapCoordinatesDrawn = false;
    this.selectedDownloadableProps = [];
    this.isResetBtnDisabled = true;
    this.isDownloadBtnDisabled = true;
    this.showElement = false;
    this.boundaries = this.boundariesCopy;
    let data = prepareCoordinatesForLeafLetMapComponent(this.boundariesCopy);
    data.forEach((feature) => {
      if (feature.coordinates.length == 1) {
        this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
      }
      else {
        feature.coordinates.forEach((coordinate) => {
          this.polyLayers.push(L.polygon(coordinate, feature.properties));
        })
      }
    });
    if (this.boundaryRequestDetails?.boundingBoxCoordinates) {
      let boundingBoxCoordinates = prepareCoordinatesForLeafLetMapComponent(this.boundaries, this.boundaryRequestDetails.boundingBoxCoordinates, 'boundingBox');
      this.showElement = false;
      boundingBoxCoordinates.forEach((feature) => {
        this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
      });
    }
    setTimeout(() => {
      this.showElement = true;
      this.polygonArray = this.polyLayers;
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  prepareButtonStatuses(methodCallingFrom?: string) {
    this.isEditBtnDisabled = !this.isEditBtnDisabled;
    if (methodCallingFrom) {
      switch (this.fromUrl) {
        case "GISDashboard":
          this.isDisabled = true;
          this.isMapDisabled = false;
          break;
        case "MyRequestDashboard":
          if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.UPLOADED) {
            this.isMapDisabled = true;
            this.isDisabled = true;
          }
          if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.CREATED ||
            this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.DOWNLOADED ||
            this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.APPROVED) {
            this.isMapDisabled = true;
          }
          else {
            this.isMapDisabled = false;
          }
          this.isDisabled = false;
          if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.APPROVED) {
            this.isDisabled = true;
          }
          break;
        case "DOAApprovalDashboard":
          this.isMapDisabled = true;
          this.isDisabled = false;
          break;
        default:
          this.isDisabled = false;
          this.isMapDisabled = false;
          break;
      }
    }
    else if (this.fromUrl == 'DOAApprovalDashboard') {
      this.isMapDisabled = true;
    }
  }

  prepareBoundingBox(boundingBoxArray): string {
    let coordinates = '';
    let returnableCoordinates = '';
    // swap longitude to latitude logic as per backend post request object
    boundingBoxArray.forEach((boundary, i) => {
      let deepCopyBoundary = JSON.parse(JSON.stringify(boundary));
      boundary.coordinates[0].forEach((latLong, j) => {
        let latLongDeepCopy = JSON.parse(JSON.stringify(latLong));
        if (Math.sign(latLongDeepCopy[0]) == -1) {
          latLong[0] = latLongDeepCopy[1];
          latLong[1] = latLongDeepCopy[0];
        }
        if (boundary.coordinates.length - 1 == j && boundingBoxArray.length - 1 == i && boundingBoxArray.length == 1) {
          returnableCoordinates = JSON.stringify(deepCopyBoundary.coordinates);
        }
        else if (boundary.coordinates.length - 1 == j && boundingBoxArray.length > 1) {
          deepCopyBoundary.coordinates = JSON.stringify(deepCopyBoundary.coordinates);
          deepCopyBoundary.coordinates = deepCopyBoundary.coordinates.replace(/\[[[]+/g, '([');
          deepCopyBoundary.coordinates = deepCopyBoundary.coordinates.replace(/[\]]]]/g, '])');
          coordinates = coordinates + (i > 0 ? ',' : '') + deepCopyBoundary.coordinates;
          returnableCoordinates = "[[" + coordinates + "]]";
        }
      });
    });
    return returnableCoordinates;
  }

  onSelection(data) {
    if (this.fromUrl == 'MyRequestDashboard') {
      this.onEditApprovedBoundaries(data);
    }
    else if (this.fromUrl == 'GISDashboard') {
      this.selectedDownloadableProps = data.selectedDownloadableProps;
      this.boundingBoxCoordinates = this.prepareBoundingBox(data.boundingBoxRectangles);
      if (data.type == 'create') {
        this.isResetBtnDisabled = false;
      }
      if (this.selectedDownloadableProps.length > 0) {
        this.isDownloadBtnDisabled = false;
      }
    }
    else if (this.fromUrl == 'Two Table Architecture Custom Address' || this.fromUrl == 'Two Table Architecture Exact Address') {
      this.latLong = data;
    }
    else {
      this.onViewRequestsAddEditBoundariesAction(data);
    }
  }

  onEditApprovedBoundaries(data) {
    //create polygon
    if (data.data.type == "FeatureCollection") {
      this.newlyCreatedMultiPolygons = [];
      this.newlyCreatedMultiPolygons = data.data.features.filter(fe => fe['properties']['type'] && fe['properties']['type'] == 'create')
        .map(feature => {
          return {
            boundaryId: feature.properties.Id,
            coordinates: feature.geometry.coordinates[0],
            type: feature.properties.type
          }
        });
    }
    //edit polygon
    else if (data.data.type == "Feature") {
      let polygonObj = { boundaryId: '', coordinates: [], type: 'edit', batchId: '' };
      let filteredEditedPolygon = this.editedMultiPolygons.find(eM => eM['boundaryId'] == data.data.properties.Id);
      if (filteredEditedPolygon) {
        filteredEditedPolygon.coordinates = data.data.geometry.coordinates[0];
      }
      else {
        polygonObj.boundaryId = data.data.properties.Id;
        polygonObj.batchId = data.data.properties.BatchId;
        polygonObj.coordinates = data.data.geometry.coordinates[0];
        this.editedMultiPolygons.push(polygonObj);
      }
    }
    this.tempBoundaries = [];
    let features = [];
    if (!this.isUploaded) {
      features.forEach((polygonObj) => {
        let drawnBoundaryObj = this.boundaries.find(b => b.boundaryId == polygonObj.properties.Id);
        if (drawnBoundaryObj) {
          polygonObj.geometry.coordinates[0].forEach((latLong) => {
            let latLongArr = JSON.parse(JSON.stringify(latLong));
            if (Math.sign(latLongArr[0]) == 1) {
              latLong[0] = latLongArr[1];
              latLong[1] = latLongArr[0];
            }
          });
          drawnBoundaryObj.geometry.coordinates = JSON.stringify(polygonObj.geometry.coordinates);
          this.tempBoundaries.push(drawnBoundaryObj)
        }
        else {
          polygonObj.geometry.coordinates[0].forEach((latLong, index) => {
            let latLongArr = JSON.parse(JSON.stringify(latLong));
            if (Math.sign(latLongArr[0]) == 1) {
              latLong[0] = latLongArr[1];
              latLong[1] = latLongArr[0];
            }
          });
          let newBoundary = {
            boundaryRequestId: null,
            batchId: null,
            boundaryId: null, geometry: {
              type: "MULTIPOLYGON", coordinates: JSON.stringify(polygonObj.geometry.coordinates)
            }
          };
          this.tempBoundaries.push(newBoundary);
        }
      });
    }
    else {
      features.forEach((polygonObj) => {
        let newBoundary = {
          boundaryId: polygonObj.properties.Id,
          batchId: polygonObj.properties.BatchId,
          geometry: {
            type: "MULTIPOLYGON", coordinates: JSON.stringify(polygonObj.geometry.coordinates)
          },
          modifiedUserId: this.loggedInUserData.userId
        };
        this.tempBoundaries.push(newBoundary);
      });
    }
    if (this.newlyCreatedMultiPolygons.length > 0 || this.editedMultiPolygons.length > 0) {
      this.areMapCoordinatesDrawn = true;
      this.isResetBtnDisabled = this.boundaryRequestDetails.boundingBoxCoordinates ? true : false;
    }
    else {
      this.areMapCoordinatesDrawn = false;
      this.isResetBtnDisabled = true;
    }
    this.emittedValuesCount += 1;
    if (this.emittedValuesCount == 1) {
      this.prepareButtonStatuses();
    }
  }

  onViewRequestsAddEditBoundariesAction(data) {
    this.areMapCoordinatesDrawn = true;
    //create polygon
    if (data.data.type == "FeatureCollection") {
      this.newlyCreatedMultiPolygons = [];
      this.newlyCreatedMultiPolygons = data.data.features.filter(fe => fe['properties']['type'] && fe['properties']['type'] == 'create')
        .map(feature => {
          return {
            boundaryId: feature.properties.Id,
            batchId: feature.properties.BatchId,
            coordinates: feature.geometry.coordinates[0],
            type: feature.properties.type
          }
        });
    }
    //edit polygon
    else if (data.data.type == "Feature") {
      let polygonObj = { boundaryId: '', coordinates: [], type: 'edit', batchId: "" };
      let filteredEditedPolygon = this.editedMultiPolygons.find(eM => eM['boundaryId'] == data.data.properties.Id);
      if (filteredEditedPolygon) {
        filteredEditedPolygon.coordinates = data.data.geometry.coordinates[0];
      }
      else {
        polygonObj.boundaryId = data.data.properties.Id;
        polygonObj.batchId = data.data.properties.BatchId;
        polygonObj.coordinates = data.data.geometry.coordinates[0];
        this.editedMultiPolygons.push(polygonObj);
      }
    }
    this.tempBoundaries = [];
    let features = [];
    if (!this.isUploaded) {
      features.forEach((polygonObj) => {
        let drawnBoundaryObj = this.boundaries.find(b => b.boundaryId == polygonObj.properties.Id);
        if (drawnBoundaryObj) {
          polygonObj.geometry.coordinates[0].forEach((latLong) => {
            let latLongArr = JSON.parse(JSON.stringify(latLong));
            if (Math.sign(latLongArr[0]) == 1) {
              latLong[0] = latLongArr[1];
              latLong[1] = latLongArr[0];
            }
          });
          drawnBoundaryObj.geometry.coordinates = JSON.stringify(polygonObj.geometry.coordinates);
          this.tempBoundaries.push(drawnBoundaryObj)
        }
        else {
          polygonObj.geometry.coordinates[0].forEach((latLong, index) => {
            let latLongArr = JSON.parse(JSON.stringify(latLong));
            if (Math.sign(latLongArr[0]) == 1) {
              latLong[0] = latLongArr[1];
              latLong[1] = latLongArr[0];
            }
          });
          let newBoundary = {
            boundaryRequestId: null,
            batchId: null,
            boundaryId: null, geometry: {
              type: "MULTIPOLYGON", coordinates: JSON.stringify(polygonObj.geometry.coordinates)
            }
          };
          this.tempBoundaries.push(newBoundary);
        }
      });
    }
    else {
      features.forEach((polygonObj) => {
        let newBoundary = {
          boundaryId: polygonObj.properties.Id,
          batchId: polygonObj.properties.BatchId,
          geometry: {
            type: "MULTIPOLYGON", coordinates: JSON.stringify(polygonObj.geometry.coordinates)
          },
          modifiedUserId: this.loggedInUserData.userId
        };
        this.tempBoundaries.push(newBoundary);
      });
    }
    this.emittedValuesCount += 1;
    if (this.emittedValuesCount == 1) {
      this.prepareButtonStatuses();
    }
    if (this.newlyCreatedMultiPolygons.length > 0 || this.editedMultiPolygons.length > 0) {
      this.areMapCoordinatesDrawn = true;
      this.isResetBtnDisabled = this.boundaryRequestDetails.boundingBoxCoordinates ? true : false;
    }
    else {
      this.areMapCoordinatesDrawn = false;
      this.isResetBtnDisabled = true;
    }
  }

  onDownload() {
    let payload = {
      boundingBoxCoordinates: this.boundingBoxCoordinates,
      downloadRequestIds: this.selectedDownloadableProps,
      boundaryLayerId: this.boundaryRequestDetails.boundaryLayerId,
      boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
      boundaryRequestRefNo: this.boundaryRequestDetails.boundaryRequestRefNo,
      modifiedUserId: this.loggedInUserData.userId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.FILE_PROCESS, SalesModuleApiSuffixModels.BOUNDARY_DOWNLOAD, payload)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.isResetBtnDisabled = true;
          this.boundaryRequestDetails.boundaryStatusId = this.boundaryStatus.DOWNLOADED;
          setTimeout(() => {
            this.isBoundaryRequestDownloadedSuccessful = true;
          });
          FileSaver.saveAs(response.resources.fileURL);
          this.dialogRef.close();
          this.route.navigateByUrl("/my-tasks/boundary-management/gis-dashboard");
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onUpload(event) {
    this.localZipFile = event.target.files[0];
    if (this.localZipFile.name.replace(/\s/g, '').slice(-3) != 'zip') {
      this.snackbarService.openSnackbar('Select .zip file', ResponseMessageTypes.WARNING)
      return;
    } else {
      this.isFileUploaded = true;
      this.getSavedZipFileFromServer();
    }
  }

  swapLatitudeToLongitude(coordinates) {
    coordinates.forEach((latLong, index) => {
      let latLongArr = JSON.parse(JSON.stringify(latLong));
      if (Math.sign(latLongArr[0]) == 1) {
        latLong[0] = latLongArr[1];
        latLong[1] = latLongArr[0];
      }
    });
    return coordinates;
  }

  getSavedZipFileFromServer(): void {
    let formData = new FormData();
    formData.append('file', this.localZipFile);
    this.crudService.fileUpload(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.FILE_UPLOAD, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.serverPathForZipFile = response.resources;
          this.showElement = false;
          let self = this;
          shp(this.serverPathForZipFile).then((data) => {
            console.log(data,'after read the downloaded file');
            let payload = {
              boundaries: []
            };
            self.polygonArray = [];
            if (data.features.length > 0) {
              data.features.forEach((feature: any, index: number) => {
                if (feature.geometry.coordinates.length == 1) {
                  let coordinatesCopy = JSON.parse(JSON.stringify(feature.geometry.coordinates[0]));
                  payload['boundaries'].push({
                    boundaryId: feature.properties.Id,
                    boundaryRequestId: feature.properties.RequestId,
                    geometry: { coordinates: JSON.stringify([coordinatesCopy]), type: 'Polygon' },
                    modifiedUserId: self.loggedInUserData.userId
                  });
                  feature.geometry.coordinates[0].forEach((latLong, j) => {
                    let latLongDeepCopy = JSON.parse(JSON.stringify(latLong));
                    if (Math.sign(latLongDeepCopy[0]) == 1) {
                      latLong[0] = latLongDeepCopy[1];
                      latLong[1] = latLongDeepCopy[0];
                    }
                  });
                  self.polyLayers.splice(self.polyLayers.findIndex(pl => pl.options.Id === feature.properties.Id), 1);
                  self.polyLayers.push(L.polygon(feature.geometry.coordinates[0], feature.properties));
                }
                else {
                  let coordinatesCopy = JSON.parse(JSON.stringify(feature.geometry.coordinates));
                  feature.geometry.coordinates.forEach((coordinate) => {
                    payload['boundaries'].push({
                      boundaryId: feature.properties.Id,
                      geometry: { coordinates: JSON.stringify([[coordinatesCopy]]), type: 'MULTIPOLYGON' },
                      modifiedUserId: self.loggedInUserData.userId
                    });
                    coordinate.forEach((latLong, j) => {
                      let latLongDeepCopy = JSON.parse(JSON.stringify(latLong));
                      if (Math.sign(latLongDeepCopy[0]) == 1) {
                        latLong[0] = latLongDeepCopy[1];
                        latLong[1] = latLongDeepCopy[0];
                      }
                    });
                    self.polyLayers.splice(self.polyLayers.findIndex(pl => pl.options.Id === feature.properties.Id), 1);
                    self.polyLayers.push(L.polygon([coordinate], feature.properties));
                  });
                }
              });
              self.showElement = true;
              self.polygonArray = self.polyLayers;
              payload['boundaryRequestId'] = this.boundaryRequestDetails.boundaryRequestId;
              payload['modifiedUserId'] = this.loggedInUserData.userId;
              self.onFormSubmit(SalesModuleApiSuffixModels.GIS_BOUNDARIES_UPLOAD, payload);
            }
          });
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  redirectToGoLiveDate() {
    this.dialogRef.close();
    this.route.navigate(['/my-tasks/boundary-management/view-request/go-live-date'],
      { queryParams: { boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId } });
  }
  // this method is to capture marker based lat longs
  onSaveLatLong() {
    this.dialogRef.close({
      latLong: this.latLong.data
    });
  }

  onSubmit(boundaryStatus: BoundaryStatus | BoundaryApprovalStatus) {
    switch (this.fromUrl) {
      case 'sales':
        this.prepareGeneralBoundariesRequestDetails(boundaryStatus);
        break;
      case 'technical':
        this.prepareGeneralBoundariesRequestDetails(boundaryStatus);
        break;
      case 'operations':
        this.prepareGeneralBoundariesRequestDetails(boundaryStatus);
        break;
      case 'DOAApprovalDashboard':
        if ((boundaryStatus == this.boundaryApprovalStatus.APPROVED && !this.filteredPermissions?.find(fSC => fSC.menuName === PermissionTypes.APPROVE)) ||
          (boundaryStatus == this.boundaryApprovalStatus.DECLINED && !this.filteredPermissions?.find(fSC => fSC.menuName === PermissionTypes.DECLINE))) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          this.prepareDOAApprovalDashboardRequestDetails(boundaryStatus);
        }
        break;
      case 'MyRequestDashboard':
        if ((boundaryStatus == this.boundaryApprovalStatus.APPROVED && !this.filteredPermissions?.find(fSC => fSC.menuName === PermissionTypes.APPROVE)) ||
          (boundaryStatus == this.boundaryApprovalStatus.DECLINED && !this.filteredPermissions?.find(fSC => fSC.menuName === PermissionTypes.DECLINE))) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          this.prepareMyRequestDashboardRequestDetails(boundaryStatus);
        }
        break;
      case 'GISDashboard':
        if (boundaryStatus === BoundaryStatus.DOWNLOADED || boundaryStatus === BoundaryStatus.UPLOADED) {
          this.isDownloaded = true;
        }
        this.prepareGISDashboardRequestDetails(boundaryStatus);
        break;
      case "LSS Boundary":
        this.prepareLSSBoundaryDetails(boundaryStatus);
        break;
    }
  }
  // for sales, operations and technical
  prepareGeneralBoundariesRequestDetails(boundaryStatus) {
    let payload = {
      boundaryId: this.boundaryRequestDetails.boundaryId,
      boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
      goLiveDate: this.boundaryRequestDetails.goLiveDate,
      description: this.boundaryRequestDetails.description,
      comments: this.boundaryRequestDetails.comments,
      boundaryStatusId: boundaryStatus,
      createdUserId: this.loggedInUserData.userId,
      boundaries: [{
        boundaryId: this.boundaryRequestDetails.boundaryId,
        boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
        boundaryName: this.boundaryRequestDetails.boundaryName,
        geometry: { type: 'MULTIPOLYGON', coordinates: '' }
      }]
    };
    let tempArray = [...this.newlyCreatedMultiPolygons, ...this.editedMultiPolygons];
    let coordinates = '';
    // swap longitude to latitude logic as per backend post request object
    tempArray.forEach((boundary, i) => {
      let deepCopyBoundary = JSON.parse(JSON.stringify(boundary));
      boundary.coordinates.forEach((latLong, j) => {
        let latLongDeepCopy = JSON.parse(JSON.stringify(latLong));
        if (Math.sign(latLongDeepCopy[0]) == -1) {
          latLong[0] = latLongDeepCopy[1];
          latLong[1] = latLongDeepCopy[0];
        }
        if (boundary.coordinates.length - 1 == j && tempArray.length - 1 !== i) {
          deepCopyBoundary.coordinates = JSON.stringify(deepCopyBoundary.coordinates);
          deepCopyBoundary.coordinates = deepCopyBoundary.coordinates.replace(/\[[[]+/g, '([');
          deepCopyBoundary.coordinates = deepCopyBoundary.coordinates.replace(/[\]]]/g, '])');
          coordinates = coordinates + (i > 0 ? ',' : '') + deepCopyBoundary.coordinates;
        }
        else if (boundary.coordinates.length - 1 == j && tempArray.length - 1 == i) {
          deepCopyBoundary.coordinates = JSON.stringify(deepCopyBoundary.coordinates);
          deepCopyBoundary.coordinates = deepCopyBoundary.coordinates.replace(/\[[[]+/g, '([');
          deepCopyBoundary.coordinates = deepCopyBoundary.coordinates.replace(/[\]]]/g, '])');
          coordinates = coordinates + (i > 0 ? ',' : '') + deepCopyBoundary.coordinates;
          if (this.boundaries.find(b => b['batchId'] == boundary.batchId)) {
            this.boundaries.filter(b => b['batchId'] == boundary.batchId).forEach((filteredBoundary, k) => {
              if (filteredBoundary.hasOwnProperty('coordinates')) {
                let filteredCoordinates = JSON.stringify(filteredBoundary.coordinates);
                filteredCoordinates = filteredCoordinates.replace(/\[[[]+/g, '([');
                filteredCoordinates = filteredCoordinates.replace(/[\]]]/g, '])');
                coordinates = coordinates + (k > 0 ? ',' : '') + filteredCoordinates;
              }
            });
          }
          payload.boundaries[0].geometry.coordinates = `[[${coordinates}]]`;
        }
      });
    })
    this.onFormSubmit(SalesModuleApiSuffixModels.BOUNDARIES_UPDATE_BOUNDARY, payload);
  }
  // from approval dashboard list
  prepareDOAApprovalDashboardRequestDetails(boundaryStatus) {
    let payload = {
      boundaryRequestId: this.boundaryRequestId,
      employeeId: this.loggedInUserData.employeeId,
      comments: this.declineReason,
      boundaryApprovalStatusId: boundaryStatus,
      createdUserId: this.loggedInUserData.userId
    };
    let status = boundaryStatus == BoundaryApprovalStatus.APPROVED ? 'approve' :
      boundaryStatus == BoundaryApprovalStatus.DECLINED ? 'decline' : 'create';
    this.message = `Are you sure you want to ${status} <strong>${this.boundaryRequestDetails.boundaryRequestRefNo}</strong> ?`;
    if (boundaryStatus == BoundaryApprovalStatus.APPROVED) {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(this.message).
        onClose?.subscribe(dialogResult => {
          if (dialogResult) {
            this.onFormSubmit(SalesModuleApiSuffixModels.BOUNDARY_APPROVALS, payload);
          }
        });
    }
    else if (boundaryStatus == BoundaryApprovalStatus.DECLINED) {
      $(this.decline_reason.nativeElement).modal('show');
    }
  }

  // from my task view request list
  prepareMyRequestDashboardRequestDetails(boundaryStatus) {
    let payload, moduleApiSuffixModels;
    switch (boundaryStatus) {
      case this.boundaryStatus.CREATED:
        payload = {
          boundaryRequestId: this.boundaryRequestId,
          batchId: this.boundaryRequestDetails.batchId,
          boundaryId: this.boundaryRequestDetails.boundaryId,
          goLiveDate: this.boundaryRequestDetails.goLiveDate,
          description: this.boundaryRequestDetails.description,
          comments: this.boundaryRequestDetails.comments,
          boundaryStatusId: boundaryStatus,
          createdUserId: this.loggedInUserData.userId,
          boundaries: [{
            boundaryId: this.boundaryRequestDetails.boundaryId,
            boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
            boundaryName: this.boundaryRequestDetails.boundaryName,
            geometry: { type: 'MULTIPOLYGON', coordinates: '' }
          }]
        };
        moduleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_BOUNDARIES_GIS;
        break;
      case this.boundaryStatus.APPROVED:
        payload = {
          boundaryRequestId: this.boundaryRequestId,
          batchId: this.boundaryRequestDetails.batchId,
          boundaryId: this.boundaryRequestDetails.boundaryId,
          modifiedUserId: this.loggedInUserData.userId,
        };
        moduleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_BOUNDARIES_GIS_APPROVE;
        break;
      case this.boundaryStatus.REJECTED:
        payload = {
          boundaryRequestId: this.boundaryRequestId,
          batchId: this.boundaryRequestDetails.batchId,
          boundaryId: this.boundaryRequestDetails.boundaryId,
          modifiedUserId: this.loggedInUserData.userId,
        };
        moduleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_BOUNDARIES_GIS_DECLINE;
        break;
    }
    if (this.newlyCreatedMultiPolygons.length > 0) {
      this.newlyCreatedMultiPolygons.forEach((newlyCreatedBoundaryObj, index: number) => {
        if (index == 0) {
          newlyCreatedBoundaryObj.boundaryId = this.boundaryRequestDetails.boundaryId;
          newlyCreatedBoundaryObj.boundaryRequestId = this.boundaryRequestId;
        }
        else {
          newlyCreatedBoundaryObj.boundaryId = null;
          newlyCreatedBoundaryObj.boundaryRequestId = null;
        }
        newlyCreatedBoundaryObj.geometry = {};
        newlyCreatedBoundaryObj.geometry.type = 'MULTIPOLYGON';
        newlyCreatedBoundaryObj.geometry.coordinates = JSON.stringify(JSON.parse(JSON.stringify([newlyCreatedBoundaryObj.coordinates])));
        delete newlyCreatedBoundaryObj.coordinates;
        delete newlyCreatedBoundaryObj.type;
      });
    }
    if (this.editedMultiPolygons.length > 0) {
      this.editedMultiPolygons = this.editedMultiPolygons.filter(e => !e.boundaryId.includes('TEMP-NO'));
      this.editedMultiPolygons.forEach((editedBoundaryObj) => {
        editedBoundaryObj.boundaryRequestId = this.boundaries.find(b => b['boundaryId'] == editedBoundaryObj.boundaryId).boundaryRequestId;
        editedBoundaryObj.geometry = {};
        editedBoundaryObj.geometry.type = 'MULTIPOLYGON';
        editedBoundaryObj.geometry.coordinates = JSON.stringify(JSON.parse(JSON.stringify([editedBoundaryObj.coordinates])));
        delete editedBoundaryObj.coordinates;
        delete editedBoundaryObj.type;
      })
    }
    if (this.newlyCreatedMultiPolygons.length > 0 || this.editedMultiPolygons.length > 0) {
      payload['boundaries'] = [...this.newlyCreatedMultiPolygons, ...this.editedMultiPolygons];
      // swap longitude to latitude logic
      payload['boundaries'].forEach((boundary) => {
        let parsedCoordinates = JSON.parse(boundary.geometry.coordinates);
        parsedCoordinates[0].forEach((latLong, index) => {
          let latLongDeepCopy = JSON.parse(JSON.stringify(latLong));
          if (Math.sign(latLongDeepCopy[0]) == -1) {
            latLong[0] = latLongDeepCopy[1];
            latLong[1] = latLongDeepCopy[0];
          }
        });
        boundary.geometry.coordinates = JSON.stringify(parsedCoordinates);
        boundary.geometry.type = 'MULTIPOLYGON';
      })
    }
    this.onFormSubmit(moduleApiSuffixModels, payload);
  }

  // from gis dashboard request list
  prepareGISDashboardRequestDetails(boundaryStatus) {
    let payload, moduleApiSuffixModels;
    switch (boundaryStatus) {
      case this.boundaryStatus.DOWNLOADED:
        payload = {
          boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
          modifiedUserId: this.loggedInUserData.userId,
        }
        moduleApiSuffixModels = SalesModuleApiSuffixModels.GIS_BOUNDARIES_DOWNLOAD;
        break;
    }
    this.onFormSubmit(moduleApiSuffixModels, payload);
  }

  // from lss stage 3
  prepareLSSBoundaryDetails(boundaryStatus) {
    let payload = {};
    let moduleApiSuffixModel;
    switch (boundaryStatus) {
      case BoundaryStatus.CREATED:
        this.editedMultiPolygons = this.editedMultiPolygons.filter(e => !e.boundaryId.includes('TEMP-NO'));
        payload['lssId'] = this.boundaryRequestDetails.lssId;
        payload['boundaries'] = (this.newlyCreatedMultiPolygons.length > 0) ? this.newlyCreatedMultiPolygons : this.editedMultiPolygons;
        payload['goLiveDate'] = this.boundaryRequestDetails.goLiveDate;
        payload['createdUserId'] = this.loggedInUserData.userId;
        payload['boundaries'].forEach((boundaryObj) => {
          let isTypeCreate = (boundaryObj.type == 'create') ? true : false;
          boundaryObj.boundaryId = isTypeCreate ? null : boundaryObj.boundaryId;
          boundaryObj.boundaryRequestId = isTypeCreate ? null : this.boundaries.find(b => b['boundaryId'] == boundaryObj.boundaryId).boundaryRequestId;
          boundaryObj.geometry = {
            type: "Polygon", coordinates: JSON.stringify(JSON.parse(JSON.stringify([boundaryObj.coordinates])))
          }
          delete boundaryObj.type;
          delete boundaryObj.coordinates;
        });
        this.onFormSubmit(SalesModuleApiSuffixModels.SALES_API_GIS_BOUNDARY, payload);
        break;
      // for status Approved and Declined/Rejected
      default:
        this.message = "";
        payload = {
          lssId: this.boundaryRequestDetails.lssId,
          boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
          boundaryId: this.boundaryRequestDetails.boundaryId,
          batchId: this.boundaryRequestDetails.batchId,
          modifiedUserId: this.loggedInUserData.userId,
          comments: this.declineReason,
          goLiveDate: this.boundaryRequestDetails.goLiveDate
        };
        moduleApiSuffixModel = (boundaryStatus == BoundaryStatus.APPROVED) ? SalesModuleApiSuffixModels.LSS_BOUNDARY_APPROVAL :
          SalesModuleApiSuffixModels.LSS_BOUNDARY_DECLINE;
        let status = boundaryStatus == BoundaryStatus.APPROVED ? 'approve' :
          boundaryStatus == BoundaryStatus.REJECTED ? 'decline' : 'create';
        this.message = `Are you sure you want to ${status} <strong>${this.boundaryRequestDetails.boundaryReqRefNo}</strong> ?`;
        if (boundaryStatus == BoundaryStatus.APPROVED) {
          this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(this.message).
            onClose?.subscribe(dialogResult => {
              if (dialogResult) {
                this.onFormSubmit(moduleApiSuffixModel, payload);
              }
            });
        }
        else if (boundaryStatus == BoundaryStatus.REJECTED) {
          $(this.decline_reason_for_lss.nativeElement).modal('show');
        }
        break;
    }
  }

  onRejectRequest() {
    this.onFormSubmit(SalesModuleApiSuffixModels.LSS_BOUNDARY_DECLINE, {
      boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId,
      boundaryId: this.boundaryRequestDetails.boundaryId,
      batchId: this.boundaryRequestDetails.batchId,
      modifiedUserId: this.loggedInUserData.userId,
      lssId: this.boundaryRequestDetails.lssId,
      comments: this.declineReason,
      goLiveDate: this.boundaryRequestDetails.goLiveDate
    });
  }

  onFormSubmit(moduleApiSuffixModel, payload) {
    if (payload.hasOwnProperty('goLiveDate')) {
      let goLiveDate = this.datePipe.transform(this.boundaryRequestDetails.goLiveDate, 'yyyy-MM-dd HH:mm:ss');
      payload['goLiveDate'] = goLiveDate;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, moduleApiSuffixModel, payload, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.isResetBtnDisabled = true;
          if (this.fromUrl !== 'GISDashboard') {
            this.dialogRef.close({
              fromUrl: this.fromUrl,
              boundaryRequestId: this.boundaryRequestId,
              boundaryRequestRefNo: this.boundaryRequestRefNo,
              boundaryRequestDetails: this.boundaryRequestDetails,
              boundaries: this.boundaries,
              boundariesCopy: this.boundaries,
              isDisabled: this.isDisabled,
            });
          }
          this.onResponse();
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onRedirectToListPage() {
    if (this.fromUrl == 'GISDashboard') {
      this.route.navigateByUrl("/boundary-management/gis-dashboard");
    }
    else {
      this.onResponse();
    }
  }

  onResponse() {
    switch (this.fromUrl) {
      case 'DOAApprovalDashboard':
        this.route.navigateByUrl("/my-tasks/boundary-management/approval-dashboard");
        break;
      case 'MyRequestDashboard':
        this.route.navigateByUrl("/my-tasks/boundary-management/view-request");
        break;
      case 'GISDashboard':
        this.dialogRef.close();
        this.route.navigateByUrl("/my-tasks/boundary-management/gis-dashboard");
        break;
      case "LSS Boundary":
        this.route.navigate(["/lss/lss-registration/stage-three"], {
          queryParams: { id: this.boundaryRequestDetails.lssId },
        });
        break;
      default:
        this.route.navigateByUrl(`/boundary-management/view/${this.fromUrl}-boundaries`);
        break;
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
