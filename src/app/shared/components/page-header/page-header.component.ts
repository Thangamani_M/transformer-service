import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';

const defaultConfig = {
  customEnableActionButtonStyle: { 'top': '3px', 'right': '6px' }
}
@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
})
export class PageHeaderComponent implements OnInit {

  @Input() tableCaption: string;
  @Input() isDisableCustomerPreference: boolean;
  @Input() captionFontSize: string;
  @Input() tableComponentConfigs: any;
  @Input() breadCrumbItems: any;
  @Input() selectedTabIndex: number;
  @Output() buttonClicked = new EventEmitter<any>();
  @Output() breadCrumbItemsClicked = new EventEmitter<any>();
  @Input() customEnableActionButtonStyle = defaultConfig.customEnableActionButtonStyle;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  onBreadCrumbItemsClick(val?) {
    this.breadCrumbItemsClicked.emit(val);
  }

  onButtonClick(val?) {
    this.buttonClicked.emit(val);
  }
}
