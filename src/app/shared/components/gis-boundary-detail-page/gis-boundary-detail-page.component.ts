import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared/enums/enum';
import { SnackbarService } from '@app/shared/services';
import Boundary_Management_Components from '@modules/boundary-management/shared/utils/boundary-management-module.enums';
import { MY_TASK_COMPONENT } from '@modules/my-tasks/shared';
import { Store } from '@ngrx/store';
import "leaflet";
import 'leaflet-draw';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { loggedInUserData } from '../../../modules/others/auth.selectors';
import { BoundaryApprovalStatus, BoundaryStatus, SalesModuleApiSuffixModels } from '../../../modules/sales/shared/utils/sales-module.enums';
import { currentComponentPageBasedPermissionsSelector$ } from '../../../shared/layouts/layout.selectors';
import { formConfigs, PERMISSION_RESTRICTION_ERROR } from '../../../shared/utils/variables.utils';
import { ReusableAddNotesWithHistoryPopupComponent } from '../../components/add-notes-with-history-popup/add-notes-with-history-popup.component';
import { CrudService } from '../../services/crud.service';
import { RxjsService } from '../../services/rxjs.services';
import { ModulesBasedApiSuffix, PermissionTypes } from '../../utils/enums.utils';
import { prepareCoordinatesForLeafLetMapComponent, prepareGetRequestHttpParams } from '../../utils/functions.utils';
import { LoggedInUserModel } from '../../utils/models/logged-in-user.model';
declare let L;
declare let $;
interface IBoundaryRequestDetails {
  boundaryRequestId: "", boundaryTypeId: "", description: "", tempGoLiveDate: null, goLiveDate: null, boundaryTypeName: "", divisionName: "", boundaryName: "",
  comments: "", boundaryStatusId: 2, createdUserId: "", boundaryId: "", batchId: "", boundaryLayerName: "", branchName: "", boundaryRequestRefNo: "",
  isBoundaryGoLive: false, approverStatusId: ""
}
@Component({
  selector: 'gis-boundary-detail-page',
  templateUrl: './gis-boundary-detail-page.component.html'
})
export class GISBoundaryDetailPageComponent implements OnInit {
  polygonArray = [];
  boundaryRequestId = "";
  boundaryRequestDetails: IBoundaryRequestDetails | any;
  todayDate = new Date();
  boundaryStatusLogs = [];
  isDisabled = true;
  boundaries = [];
  showElement = false;
  loggedInUserData: LoggedInUserModel;
  boundaryStatus = BoundaryStatus;
  boundaryApprovalStatus = BoundaryApprovalStatus;
  fromUrl = "";
  polyLayers = [];
  boundaryRequestRefNo = "";
  isEditBtnDisabled = false;
  isMapDisabled = true;
  title = "Boundary Status";
  boundaryStats = { boundaryCustomersCount: 0 };
  boundaryFields = [];
  formConfigs = formConfigs;
  isEditAccessDenied = true;

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private route: Router,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private snackbarService: SnackbarService, private dialog: MatDialog) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.boundaryRequestId = response[1].boundaryRequestId;
        this.boundaryRequestRefNo = response[1].boundaryRequestRefNo;
        this.fromUrl = response[1].fromUrl;
        this.getGisBoundaryDetailsByBoundaryId();
      });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreRolesData();
    // this.route.routeReuseStrategy.shouldReuseRoute = function () {
    //   return false;
    // };
  }

  combineLatestNgrxStoreRolesData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let sidebarComponent = this.fromUrl == 'sales' ? Boundary_Management_Components.SALES_BOUNDARIES :
        this.fromUrl == 'technical' ? Boundary_Management_Components.TECHNICAL_BOUNDARIES :
          this.fromUrl == 'operations' ? Boundary_Management_Components.OPERATIONAL_BOUNDARIES :
            this.fromUrl == 'DOAApprovalDashboard' ? MY_TASK_COMPONENT.BOUNDARY_MGMT_APPROVAL_DASHBOARD :
              this.fromUrl == 'MyRequestDashboard' ? MY_TASK_COMPONENT.BOUNDARY_MGMT_VIEW_REQUEST :"";
      this.isEditAccessDenied = response[0][sidebarComponent]?.find(fSC => fSC.menuName === PermissionTypes.EDIT) ? false : true;
    });
  }

  getGisBoundaryDetailsByBoundaryId() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_BOUNDARY_GIS_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, {
      boundaryRequestId: this.boundaryRequestId,
      userId: this.loggedInUserData.userId
    })).subscribe((response) => {
      if (response.isSuccess && response.resources && response.statusCode == 200) {
        this.boundaryRequestDetails = response.resources.boundaryRequestDetails;
        if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.INITIATED || this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.REJECTED) {
          this.title = `Create Boundary ${this.boundaryRequestRefNo}`;
        }
        else if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.CREATED) {
          this.title = `Approve Boundary ${this.boundaryRequestRefNo}`;
        }
        else if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.UPLOADED || this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.DOWNLOADED) {
          this.title = `Pending Boundary ${this.boundaryRequestRefNo}`;
        }
        else if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.LIVE || this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.GO_LIVE ||
          this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.APPROVED) {
          this.title = `Approved Boundary ${this.boundaryRequestRefNo}`;
        }
        else {
          this.title = `Create Boundary ${this.boundaryRequestRefNo}`;
        }
        if (this.boundaryRequestDetails.goLiveDate) {
          this.boundaryRequestDetails.goLiveDate = new Date(this.boundaryRequestDetails.goLiveDate);
        }
        this.boundaryStatusLogs = response.resources.boundaryStatusLogs;
         this.boundaries = response.resources.boundaries;
        //this.boundaries=response.resources.boundaries.filter((b)=>b.boundaryId=='de6b2d6f-f99a-4a66-9f3a-274f63cbc46e');
        this.boundaryStats = { boundaryCustomersCount: response.resources.boundaryStats.boundaryCustomersCount ? response.resources.boundaryStats.boundaryCustomersCount : 0 };
        this.boundaryFields = response?.resources?.boundaryFields;
        this.isEditBtnDisabled = !this.isEditBtnDisabled;
        this.prepareButtonStatuses();
        if (this.boundaries.length > 0) {
          let data = prepareCoordinatesForLeafLetMapComponent(this.boundaries);
          this.polyLayers = [];
          this.polygonArray = [];
          this.showElement = false;
          data.forEach((feature) => {
            if (feature.coordinates.length == 1) {
              this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
            }
            else {
              feature.coordinates.forEach((coordinate) => {
                this.polyLayers.push(L.polygon(coordinate, feature.properties));
              });
            }
          });
          setTimeout(() => {
            this.showElement = true;
            this.polygonArray = this.polyLayers;
            this.rxjsService.setGlobalLoaderProperty(false);
          }, 2000);
        } else {
          this.showElement = true;
          this.polygonArray = [];
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        if (this.boundaryRequestDetails?.boundingBoxCoordinates && this.fromUrl == 'GISDashboard') {
          let data = prepareCoordinatesForLeafLetMapComponent(this.boundaries, this.boundaryRequestDetails?.boundingBoxCoordinates, 'boundingBox');
          this.showElement = false;
          data.forEach((feature) => {
            this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
          });
        }
      }
    });
  }

  tempFunctionToFilterTheDesiredBoundaries(list, propName: string, value: string) {
    return list.filter(b => b[propName] == value);
  }

  prepareButtonStatuses(methodCallingFrom?: string) {
    if (methodCallingFrom) {
      if (this.isEditAccessDenied) {
        this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      else {
        this.isEditBtnDisabled = !this.isEditBtnDisabled;
        switch (this.fromUrl) {
          case "GISDashboard":
            this.isDisabled = true;
            this.isMapDisabled = false;
            break;
          case "MyRequestDashboard":
            if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.UPLOADED) {
              this.isMapDisabled = true;
              this.isDisabled = true;
            }
            if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.CREATED ||
              this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.DOWNLOADED ||
              this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.APPROVED) {
              this.isMapDisabled = true;
            }
            else {
              this.isMapDisabled = false;
            }
            this.isDisabled = false;
            if (this.boundaryRequestDetails.boundaryStatusId == this.boundaryStatus.APPROVED) {
              this.isDisabled = true;
            }
            break;
          case "DOAApprovalDashboard":
            this.isMapDisabled = true;
            this.isDisabled = false;
            break;
          default:
            this.isDisabled = false;
            this.isMapDisabled = false;
            break;
        }
      }
    }
    else if (this.fromUrl == 'DOAApprovalDashboard') {
      this.isMapDisabled = true;
    }
  }

  onOpenViewNotes() {
    let data = {};
    // replace later
    data['addNotesModel'] = {
      createdUserId: this.loggedInUserData.userId,
      boundaryId: this.boundaryRequestDetails.boundaryId,
      notes: ''
    };
    data['refNo'] = this.boundaryRequestRefNo;
    data['modulesBasedApiSuffix'] = ModulesBasedApiSuffix.SALES;
    data['moduleApiSuffixModels'] = SalesModuleApiSuffixModels.BOUNDARY_NOTES;
    data['commentsPropName'] = 'notes';
    this.dialog.open(ReusableAddNotesWithHistoryPopupComponent, { width: '680px', disableClose: true, data });
  }

  redirectToGoLiveDate() {
    this.route.navigate(['/my-tasks/boundary-management/view-request/go-live-date'],
      { queryParams: { boundaryRequestId: this.boundaryRequestDetails.boundaryRequestId }, skipLocationChange: true });
  }

  onRedirectToListPage() {
    switch (this.fromUrl) {
      case 'DOAApprovalDashboard':
        this.route.navigateByUrl("/my-tasks/boundary-management/approval-dashboard");
        break;
      case 'MyRequestDashboard':
        this.route.navigateByUrl("/my-tasks/boundary-management/view-request");
        break;
      case 'GISDashboard':
        this.route.navigateByUrl("/boundary-management/gis-dashboard");
        break;
      default:
        this.route.navigateByUrl(`/boundary-management/view/${this.fromUrl}-boundaries`);
        break;
    }
  }

  onLeafletFullMapClosed(data) {
  }
}
