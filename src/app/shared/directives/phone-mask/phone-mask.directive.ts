import { Directive, ElementRef, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';
import { ISubscription } from 'rxjs/Subscription';
@Directive({
  selector: '[formControl][appPhoneMask],[formControlName][appPhoneMask]',
})
export class PhoneMaskDirective {
  backspace = false;
  delete=false;
  subscription: ISubscription;
  value = "";

  constructor(public ngControl: NgControl, private elementRef: ElementRef<HTMLFormElement>,) {
    if (!this.elementRef.nativeElement.getAttribute('autocomplete')) {
      this.elementRef.nativeElement.setAttribute('autocomplete', 'off' + Math.random());
    }
  }

  ngOnInit() {
    this.onInputChange();
  }

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event) {
    this.backspace = true;
  }

  @HostListener('keydown.delete', ['$event'])
  keydownDelete(event) {
    this.delete = true;
  }

  @HostListener('blur', ['$event'])
  onBlur(event) {
    this.value = event.target.value;
    this.backspace = false;
    this.onInputChange();
  }

  @HostListener('input', ['$event'])
  onModelChange(event) {
    this.value = event.target.value;
    this.backspace = false;
  }

  onInputChange() {
    this.subscription = this.ngControl.valueChanges.subscribe(subscribedData => {
      if(!subscribedData)return;
      let data = subscribedData.toString();
      let inputValue: string = this.value;
      var newVal = data.replace(/\D/g, '');
      let start = this.elementRef.nativeElement.selectionStart;
      let end = this.elementRef.nativeElement.selectionEnd;
      //when removed value from input
      if (data.length < inputValue.length) {
        if (newVal.length == 0) {
          newVal = '';
        }
        else if (newVal.length <= 2) {
          newVal = newVal.replace(/^(\d{0,2})/, '$1');
        } else if (newVal.length <= 6) {
          newVal = newVal.replace(/^(\d{0,2})(\d{0,3})/, '$1 $2');
        } else if (newVal.length <= 10) {
          newVal = newVal.replace(/^(\d{0,2})(\d{0,3})(\d{0,3})/, '$1 $2 $3');
        } else {
          newVal = newVal.replace(/^(\d{0,2})(\d{0,3})(\d{0,4})/, '$1 $2 $3');
        }
        this.ngControl.control.setValue(newVal, { emitEvent: false });
        this.elementRef.nativeElement.setSelectionRange(start, end);
      } else {
        // var removedD = data.charAt(start);
        if (newVal.length == 0) {
          newVal = '';
        }
        else if (newVal.length <= 2) {
          newVal = newVal.replace(/^(\d{0,2})/, '$1');
        } else if (newVal.length <= 6) {
          newVal = newVal.replace(/^(\d{0,2})(\d{0,3})/, '$1 $2');
        } else if (newVal.length <= 10) {
          newVal = newVal.replace(/^(\d{0,2})(\d{0,3})(\d{0,3})/, '$1 $2 $3');
        } else {
          newVal = newVal.replace(/^(\d{0,2})(\d{0,3})(\d{0,4})/, '$1 $2 $3');
        }
        //you are typing in the middle of below logic
        if (inputValue.length >= start) {
          //change cursor according to special chars.
          // if (removedD == " ") {
          //   start = start + 1;
          //   end = end + 1;
          // }
          this.ngControl.control.setValue(newVal, { emitEvent: false });
          this.elementRef.nativeElement.setSelectionRange(start, end);
        } else {
          this.ngControl.control.setValue(newVal, { emitEvent: false });
          if (this.backspace) {
            this.elementRef.nativeElement.setSelectionRange(start, end);
          }
          else if(this.delete){
            this.elementRef.nativeElement.setSelectionRange(start, end);
          }
        }
      }
    });
    this.backspace = false;
    this.delete = false;
  }

  ngOnDestroy() {
    if(this.subscription&&!this.subscription.closed){
      this.subscription.unsubscribe();
    }
  }
}