import { Directive, HostListener, ElementRef } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[formControl][appCardMask],[formControlName][appCardMask]'
})
export class CardMaskDirective {
  constructor(public ngControl: NgControl) { }

  @HostListener('ngModelChange', ['$event'])
  onModelChange(event) {
    this.onInputChange(event);
  }

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event) {
    this.onInputChange(event.target.value);
  }

  @HostListener('blur', ['$event'])
  onBlur(event) {
    this.onInputChange(event.target.value);
  }

  @HostListener('input', ['$event'])
  onInputChange(event: KeyboardEvent | string) {
    const input = (typeof event === 'string') ? { value: event } : event?.target as HTMLInputElement;
    let trimmed = input.value.replace(/\s+/g, '');
    if (trimmed.length > 16) {
      trimmed = trimmed.substr(0, 16);
    }

    let numbers = [];
    for (let i = 0; i < trimmed.length; i += 4) {
      numbers.push(trimmed.substr(i, 4));
    }

    input.value = numbers.join(' ');
    this.ngControl.valueAccessor.writeValue(input.value);

  }
}

