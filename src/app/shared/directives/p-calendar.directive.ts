import { Directive, ElementRef, EventEmitter, Host, Input, Optional, Output, Renderer2, Self } from "@angular/core";
import { AbstractControl, NgControl, NgModel } from "@angular/forms";
import { Calendar } from "primeng/calendar";
import { adjustDateFormatAsPerPCalendar, convertCamelCasesToSpaces, CURRENT_YEAR_TO_NEXT_FIFTY_YEARS, FROM_PREVIOUS_FIFTY_YEARS_TO_NEXT_FIFTY_YEARS } from "../utils";
@Directive({ selector: 'p-calendar:not([actualDirective])' })
export class PCalendarDirective {
    @Input() monthNavigator: boolean;
    @Input() yearNavigator: boolean;
    @Input() yearRange: string;
    @Input() timeOnly: boolean;
    @Input() dateFormat: string;
    @Input() minDate: Date;
    @Input() maxDate: Date;
    @Input() readonlyInput: boolean;
    @Input() showTime: boolean;
    @Input() showButtonBar: boolean;
    @Input() appendTo: string;
    @Input() showIcon: boolean;
    @Input() hourFormat: string;
    @Input() hideOnDateTimeSelect: boolean;
    @Input() utc: boolean;
    @Input() style: string;
    @Input() styleClass: string;
    @Input() showSeconds: boolean;
    @Input() disabled: boolean;
    @Input() placeholder: string;
    @Input() baseZIndex: number;
    @Input() name: string;
    @Input() stepMinute: number;
    @Input() stepHour: number;
    @Input() minTime: Date;
    @Input() maxTime: Date;
    @Input() value: string;
    @Output() onOverlayHide = new EventEmitter<any>();

    PCalendarDefaultConfig = {
        monthNavigator: true, yearNavigator: true,
        // Dynamic year range is from current year to next 50 years
        yearRange: CURRENT_YEAR_TO_NEXT_FIFTY_YEARS,
        timeOnly: false,
        dateFormat: adjustDateFormatAsPerPCalendar(), readonlyInput: true, showTime: false,
        showButtonBar: true, appendTo: 'body', showIcon: true,
        hourFormat: "24", hideOnDateTimeSelect: true, utc: false, maxDate: null, placeholder: "",
        styleClass: "width-eighty-percentage", showSeconds: false, overlayVisible: false, disabled: false, baseZIndex: 10000,
        defaultClass: 'fdt-date-con', defaultStepMinute: 5, defaultStepHour: 1
    }
    convertCamelCasesToSpaces = convertCamelCasesToSpaces;

    constructor(private elementRef: ElementRef, @Host() @Self() private calendar: Calendar,
        @Host() @Self() @Optional() private ngControl: NgControl, private renderer: Renderer2, @Host() @Self() @Optional() private ngModelService: NgModel) {
        // Override the default p-calendar event listener. We need to bind to the current 'this' before that
        this.calendar.onClearButtonClick = this.onClearButtonClickFn.bind(this);
        this.calendar.onOverlayHide = this.onOverlayHideFn.bind(this);
        this.calendar.showOverlay = this.showOverlayFn.bind(this);
        // This below line is used to render the parent DOM nodes of the current node
        setTimeout(() => {
            this.configurationMappingLogic();
        });
    }

    // when the poup closes
    onOverlayHideFn(e) {
        this.onOverlayHide.emit(e);
    }

    // when the popup opens
    showOverlayFn(e) {
        // if the control is disabled then prevent that from clicking the date picker icon to show overlay visibility
        if (this.getFormControlOrNgModelControl().disabled) {
            e?.preventDefault();
            return;
        }
        this.calendar.overlayVisible = true;
        if (this.minTime) {
            let toTime = new Date(this.getFormControlOrNgModelControl().value).getTime();
            let fromTime = new Date(this.minTime).getTime();
            if (fromTime >= toTime) {
                this.getFormControlOrNgModelControl().setValue(new Date(this.minTime));
            }
        }
    }

    incrementMinuteFn(e) {
        this.minMaxTimeLogic(e, 'increment-minute');
    }

    incrementHourFn(e) {
        this.minMaxTimeLogic(e, 'increment-hour');
    }

    decrementHourFn(e) {
        this.minMaxTimeLogic(e, 'decrement-hour');
    }


    decrementMinuteFn(e) {
        this.minMaxTimeLogic(e, 'decrement-minute');
    }

    getFormControlOrNgModelControl(): AbstractControl {
        return this.ngControl?.control ? this.ngControl.control : this.ngModelService?.control ?
            this.ngModelService.control : null;
    }

    minMaxTimeLogic(e, type: string) {
        let ngControlValue = this.getFormControlOrNgModelControl().value;
        let fromTime = new Date(type.includes('decrement') ? this.minTime : this.maxTime).getTime();
        let hours = (new Date(ngControlValue).getHours() - ((type == 'decrement-hour') ? (this.stepHour ? this.stepHour : this.PCalendarDefaultConfig.defaultStepHour) :
            -(this.stepHour ? this.stepHour : this.PCalendarDefaultConfig.defaultStepHour)));
        let minutes = (new Date(ngControlValue).getMinutes() - ((type == 'decrement-minute') ? (this.stepMinute ? this.stepMinute : this.PCalendarDefaultConfig.defaultStepMinute) :
            -(this.stepMinute ? this.stepMinute : this.PCalendarDefaultConfig.defaultStepMinute)));
        let actualDate = new Date(ngControlValue);
        let toDateTime = new Date(ngControlValue);
        if (type == 'decrement-hour' || type == 'increment-hour') {
            toDateTime.setHours(hours);
        }
        else if (type == 'decrement-minute' || type == 'increment-minute') {
            toDateTime.setMinutes(minutes);
        }
        let toTime = new Date(toDateTime).getTime();
        let condition = type.includes('decrement') ? (fromTime <= toTime) : (toTime <= fromTime);
        if (condition) {
            if (type == 'decrement-hour' || type == 'increment-hour') {
                actualDate.setHours(hours);
                this.getFormControlOrNgModelControl().setValue(actualDate, { emitEvent: false, onlySelf: true });
            }
            else if (type == 'decrement-minute' || type == 'increment-minute') {
                actualDate.setMinutes(minutes);
                this.getFormControlOrNgModelControl().setValue(actualDate, { emitEvent: false, onlySelf: true });
            }
        }
    }

    ngOnInit() {
        if (!this.getFormControlOrNgModelControl()) return;
        this.getFormControlOrNgModelControl()?.valueChanges.subscribe((date) => {
            this.setNearByRoundedMinutes(date);
        });
    }

    ngAfterViewInit() {
        if (!this.getFormControlOrNgModelControl()) return;
        // if the control is disabled then add the class and remove if enabled accordingly by finding the input element as a host element
        if (this.getFormControlOrNgModelControl().disabled) {
            var inputElement = this.elementRef.nativeElement?.children[0]?.children[0];
            if (inputElement?.localName == 'input') {
                this.renderer.addClass(inputElement, 'disabled');
            }
        }
        // If the control is enabled programmatically then enable the control and remove the respective classes
        else if (this.getFormControlOrNgModelControl().enabled) {
            if (inputElement?.localName == 'input') {
                this.renderer.removeClass(inputElement, 'disabled');
            }
        }
    }

    setNearByRoundedMinutes(date) {
        if (!date) return;
        let currentMinutes = new Date(date).getMinutes();
        if (currentMinutes % this.calendar.stepMinute !== 0) {
            // Round off nearby minutes
            this.calendar.currentMinute = Math.round(currentMinutes / this.calendar.stepMinute) * this.calendar.stepMinute;
        }
        else {
            this.calendar.currentMinute = currentMinutes;
        }
        let alteredDate = new Date(date);
        alteredDate.setMinutes(this.calendar.currentMinute);
        this.getFormControlOrNgModelControl().setValue(alteredDate, { emitEvent: false, onlySelf: true });
    }

    // Logic is o prevent the backend request when we click the p-calendar then click clear button without even selecting any date
    onClearButtonClickFn(e: PointerEvent) {
        if (!this.ngControl?.control?.value && !this.ngModelService?.control?.value) {
            e.preventDefault();
        }
        else {
            this.getFormControlOrNgModelControl().setValue(null);
        }
        this.calendar.overlayVisible = false;
    }

    ngOnChanges() {
        this.configurationMappingLogic();
    }

    configurationMappingLogic() {
        // This line is where the developer can override the default dateformat to their own based on the rendering needs in their respective components
        this.calendar.dateFormat = this.dateFormat ? this.dateFormat : this.PCalendarDefaultConfig.dateFormat;
        this.calendar.monthNavigator = this.monthNavigator !== undefined ? this.monthNavigator : this.PCalendarDefaultConfig.monthNavigator;
        this.calendar.yearNavigator = this.yearNavigator !== undefined ? this.yearNavigator : this.PCalendarDefaultConfig.yearNavigator;
        this.applyYearsRange();
        this.calendar.readonlyInput = this.readonlyInput !== undefined ? this.readonlyInput : this.PCalendarDefaultConfig.readonlyInput;
        this.calendar.timeOnly = this.timeOnly !== undefined ? this.timeOnly : this.PCalendarDefaultConfig.timeOnly;
        if (this.timeOnly && this.minTime) {
            this.calendar.decrementMinute = this.decrementMinuteFn.bind(this);
            this.calendar.decrementHour = this.decrementHourFn.bind(this);
        }
        if (this.timeOnly && this.maxTime) {
            this.calendar.incrementMinute = this.incrementMinuteFn.bind(this);
            this.calendar.incrementHour = this.incrementHourFn.bind(this);
        }
        this.calendar.stepMinute = this.stepMinute ? this.stepMinute : this.PCalendarDefaultConfig.defaultStepMinute;
        this.calendar.stepHour = this.stepHour ? this.stepHour : this.PCalendarDefaultConfig.defaultStepHour;
        this.calendar.hourFormat = this.hourFormat ? this.hourFormat : this.PCalendarDefaultConfig.hourFormat;
        this.calendar.hideOnDateTimeSelect = this.hideOnDateTimeSelect !== undefined ? this.hideOnDateTimeSelect : this.PCalendarDefaultConfig.hideOnDateTimeSelect;
        // This line is hidden because of no support for UTC time zone in p calendar of the current using version
        // this.calendar.utc = this.utc !== undefined ? this.utc : this.PCalendarDefaultConfig.utc;
        this.calendar.showButtonBar = this.showButtonBar !== undefined ? this.showButtonBar : this.PCalendarDefaultConfig.showButtonBar;
        this.calendar.showIcon = this.showIcon !== undefined ? this.showIcon : this.PCalendarDefaultConfig.showIcon;
        this.calendar.appendTo = this.appendTo ? this.appendTo : this.PCalendarDefaultConfig.appendTo;
        if (this.style) {
            this.calendar.style = this.style;
        }
        else if (this.styleClass) {
            this.calendar.styleClass = this.styleClass ? this.styleClass : this.PCalendarDefaultConfig.styleClass;
        }
        else {
            this.calendar.styleClass = this.PCalendarDefaultConfig.styleClass;
        }
        this.calendar.baseZIndex = this.baseZIndex ? this.baseZIndex : this.PCalendarDefaultConfig.baseZIndex;
        // If the host of the p-calendar is table then by default hide the placeholder without the configuration
        let isAParentNodeRelatedToTable = this.elementRef?.nativeElement?.parentNode?.localName == 'th';
        if (isAParentNodeRelatedToTable) {
            this.calendar.placeholder = this.placeholder ? this.placeholder : this.PCalendarDefaultConfig.placeholder;
        }
        else {
            this.calendar.placeholder = (this.placeholder || this.placeholder == '') ? this.placeholder : this.name ?
                'Enter ' + this.name : 'Enter ' + this.convertCamelCasesToSpaces(this.ngControl?.name ? this.ngControl?.name : this.ngModelService?.name ?
                    this.ngModelService?.name : 'Field');
        }
        // If time only then overlayed popup comes with the current time button on current time will be fetched when selected
        if (this.timeOnly == true) {
            this.calendar.locale.today = 'Current Time';
            this.calendar.onTodayButtonClick = this.onTodayButtonClick.bind(this);
        }
        this.calendar.maxDate = this.maxDate ? this.maxDate : this.PCalendarDefaultConfig.maxDate;
        this.calendar.disabled = this.disabled !== undefined ? this.disabled : this.PCalendarDefaultConfig.disabled;
        // add default fdt class to align the date icons in the table columns
        this.renderer.setAttribute(this.elementRef.nativeElement, 'class', this.PCalendarDefaultConfig.defaultClass);
    }

    // when today button is clicked
    onTodayButtonClick(e) {
        if (this.minTime) {
            return;
        }
        this.getFormControlOrNgModelControl().setValue(new Date());
        // close the overlay popup on click of the current time button
        this.calendar.overlayVisible = this.PCalendarDefaultConfig.overlayVisible;
    }

    applyYearsRange() {
        if (this.minDate) {
            this.calendar.yearRange = this.yearRange ? this.yearRange : CURRENT_YEAR_TO_NEXT_FIFTY_YEARS;
            this.calendar.minDate = this.minDate;
        }
        else {
            this.calendar.yearRange = this.yearRange ? this.yearRange : FROM_PREVIOUS_FIFTY_YEARS_TO_NEXT_FIFTY_YEARS;
            this.calendar.minDate = null;
        }
    }
}
