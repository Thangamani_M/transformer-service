import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared/enums';
import { HttpCancelService, OtherService, RxjsService, SnackbarService } from '@app/shared/services';
import { dealerCustomerServiceAgreementDataState$ } from '@modules/dealer/components/dealer-contract/dealer-agreement-summary/dealer-customer-creation-service-agreement-ngrx-files/dealer-customer-creation-service-agreement.selectors';
import { DealerCustomerServiceAgreementParamsDataModel } from '@modules/dealer/models/dealer-service-agreement.model';
import { selectLeadCreationStepperParamsState$ } from '@modules/sales/components/task-management/lead-creation-ngrx-files/lead-creation.selectors';
import { ServiceAgreementStepsParams } from '@modules/sales/models/lead-creation-stepper.model';
import { Store } from '@ngrx/store';
import { debug } from 'console';
import { combineLatest, fromEvent } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';
@Directive({
  selector: 'form[customForm]'
})

export class FormSubmitDirective {
  isFormChangeDetected = false;
  dealerCustomerServiceAgreementParamsDataModel: DealerCustomerServiceAgreementParamsDataModel;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  submitted$ = fromEvent(this.element, 'submit')
    .pipe(tap(() => {
      if (this.otherService?.currentUrl?.includes('dealer/dealer-contract/service-agreement')) {
        this.combineLatestNgrxStoreDataForDealerServiceAgreement();
      }
      else if (this.otherService?.currentUrl?.includes('my-leads/service-agreement') ||
        this.otherService?.currentUrl?.includes('leads/service-agreement')) {
        this.combineLatestNgrxStoreDataForLeadServiceAgreement();
      }
      // find invalid form control and focus on that control 
      const nativeInvalidControl = this.host.nativeElement.querySelector('.ng-invalid') as HTMLElement;
      if (nativeInvalidControl) {
        if (nativeInvalidControl.children.length === 0 && !nativeInvalidControl.className.includes('disabled') && !nativeInvalidControl?.['disabled']) {
          nativeInvalidControl.focus();
        }
        else {
          const nativeInvalidCustomControl = nativeInvalidControl.children[0] as HTMLElement;
          if (nativeInvalidCustomControl && !nativeInvalidCustomControl.className.includes('disabled') && !nativeInvalidCustomControl?.['disabled'])
            nativeInvalidCustomControl.focus();
        }
      }
      // show popup on submitted form has an undeitable values
      const classNames = this.element.className.split(' ');
      this.rxjsService.getFormChangeDetectionProperty().subscribe((isFormChangeDetected: boolean) => {
        this.isFormChangeDetected = isFormChangeDetected;
      });
      const areFormClassNamesNotIncluded = this.isFormChangeDetected ? false :
        (!classNames.includes("ng-dirty") || classNames.includes("ng-pristine") || classNames.includes("ng-untouched"));
      const areClassNamesListOneNotIncluded = classNames.includes("ng-valid");
      if (areFormClassNamesNotIncluded && areClassNamesListOneNotIncluded) {
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setPopupLoaderProperty(false);
        this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      }
      if (this.element.classList.contains('submitted') === false) {
        this.renderer.addClass(this.host.nativeElement, 'submitted');
      }
      this.rxjsService.setFormChangeDetectionProperty(false);
      this.rxjsService.setFormChangesDetectionPropertyForPageReload(false);
    }), shareReplay(1))
  keyupValueChanged$ = fromEvent(this.element, 'keyup')
    .subscribe(() => {
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.rxjsService.setFormChangesDetectionPropertyForPageReload(true);
    });
  onSelectionChanged$ = fromEvent(this.element, 'change')
    .subscribe(() => {
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.rxjsService.setFormChangesDetectionPropertyForPageReload(true);
    });
  reset$ = fromEvent(this.element, 'reset');

  constructor(private host: ElementRef<HTMLFormElement>, private renderer: Renderer2,
    private httpCancelService: HttpCancelService, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private store: Store<AppState>, private otherService: OtherService) {


  }

  combineLatestNgrxStoreDataForDealerServiceAgreement() {
    combineLatest([
      this.store.select(dealerCustomerServiceAgreementDataState$)]
    ).subscribe((response) => {
      if (!response) return;
      this.dealerCustomerServiceAgreementParamsDataModel = new DealerCustomerServiceAgreementParamsDataModel(response[0]);
      if (this.dealerCustomerServiceAgreementParamsDataModel.isExist) {
        this.rxjsService.setFormChangeDetectionProperty(true);
      }
    });
  }

  combineLatestNgrxStoreDataForLeadServiceAgreement() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$)]
    ).subscribe((response) => {
      if (!response) return;
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      if (this.serviceAgreementStepsParams.isExist) {
        this.rxjsService.setFormChangeDetectionProperty(true);
      }
    });
  }

  get element() {
    return this.host.nativeElement;
  }
}