import { Directive, ElementRef, Host, HostListener, Input, Optional, Self } from "@angular/core";
import { NgControl, NgModel } from "@angular/forms";
import moment from "moment";
import { OwlDateTimeComponent } from "ng-pick-datetime";
import { take } from "rxjs/operators";
interface OwlDateChangeEventPayload {
    soruce: any;
    value: moment.Moment;
    input: HTMLInputElement;
}
@Directive({ selector: 'input[owlDateTime]' })
export class OwlDateTimePickerDirective {
    @Input() dateFormat;
    @Input() owlDateTimeTrigger: OwlDateTimeComponent<any>;

    constructor(private elementRef: ElementRef, @Host() @Self() @Optional() private ngControl: NgControl,
        @Host() @Self() @Optional() private ngModelService: NgModel) {
    }

    ngOnInit() {
        this.ngControl?.valueChanges.pipe(take(1)).subscribe((date) => {
            this.owlDateTimeTrigger?.confirmSelectedChange.emit(date);
        });
        this.ngModelService?.control?.valueChanges.pipe(take(1)).subscribe((date) => {
            this.owlDateTimeTrigger?.confirmSelectedChange.emit(date);
        });
    }

    getPickerType() {
        // retrieve the pickerType of the owl date time picker which is mandatory to show the desired date formats
        return this.owlDateTimeTrigger ? this.owlDateTimeTrigger.pickerType : this.elementRef?.nativeElement?.nextSibling?.attributes?.getNamedItem('ng-reflect-picker-type')?.value;
    }

    @HostListener('dateTimeChange', ['$event'])
    onDateTimeChange(payload: OwlDateChangeEventPayload) {
        let datePickerType = this.getPickerType();
        const input = payload.input;
        payload.value = moment(payload.value);
        if (datePickerType && !this.dateFormat) {
            switch (datePickerType) {
                case 'timer':
                    input.value = payload.value.format('HH:mm');
                    break;
                case 'calendar':
                    input.value = payload.value.format(localStorage.getItem('globalDateFormat'));
                    break;
                default:
                    input.value = payload.value.format(localStorage.getItem('globalDateFormat') + ' HH:mm');
                    break;
            }
        }
        else if (this.dateFormat) {
            input.value = payload.value.format(this.dateFormat);
        }
    }
}