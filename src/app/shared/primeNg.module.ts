import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DialogService } from 'primeng/api';
// Primeng Components
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { DynamicDialogModule } from 'primeng/components/dynamicdialog/dynamicdialog';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { MenuModule } from 'primeng/menu';
import { MultiSelectModule } from 'primeng/multiselect';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
    imports: [CommonModule,
        TableModule,
        InputTextModule,
        DropdownModule,
        ButtonModule,
        MenuModule,
        DynamicDialogModule,
        DialogModule,
        CheckboxModule,
        TabViewModule,
        ChipsModule,
        MultiSelectModule,
        InputSwitchModule,
        RadioButtonModule,
        CalendarModule, TooltipModule, ProgressSpinnerModule, ScrollPanelModule,
        AutoCompleteModule
    ],
    declarations: [],
    providers:[DialogService],
    exports: [TableModule,
        InputTextModule,
        DropdownModule,
        ButtonModule,
        MenuModule,
        DynamicDialogModule,
        DialogModule,
        CheckboxModule,
        TabViewModule,
        ChipsModule,
        MultiSelectModule,
        InputSwitchModule,
        RadioButtonModule,
        CalendarModule, TooltipModule, ProgressSpinnerModule, ScrollPanelModule,
        AutoCompleteModule]
})
export class PrimeNgModule { }
