import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  CardMaskDirective, ChangeStatusDialogComponent, ConfirmDialogPopupComponent, ControlErrorComponent,
  ControlErrorContainerDirective, ControlErrorsDirective, CustomSnackBarComponent, FileUploadCustomControlCustomControlComponent, FindDuplicatePipe, FormatNumberPipe, FormSubmitDirective, InputCustomControlComponent, MatSelectCustomControlComponent, OwlDateTimePickerDirective, PageFormArrayComponent, PageHeaderComponent, PageListTableComponent, PageTabComponent, PCalendarDirective, PhoneMaskDirective, PopupLoaderComponent, PrimengMessageConfirmDialogComponent, RadioGroupCustomControlComponent, ReusableAddNotesWithHistoryPopupComponent, ReusableHeaderBreadcrumbContainerComponent, SelectCustomControlComponent, SharedTableActionEffects, SharedTableFilteredDataReducer, TextAreaCustomControlComponent, ValidateInputDirective, ViewPageComponent, WholeNumberToDecimalPipe
} from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MatNativeDatetimeModule } from "@mat-datetimepicker/core";
import { KzMaskDirective } from '@modules/customer/components/customer/technician-installation/initiation-call-creation/kz-mask.directive';
import { JobSelectionScanModalComponent } from '@modules/inventory/components/instaff-job-selection/job-selection-list/job-selection-scan-modal.component';
import { callBackCustomerInfoModalComponent } from '@modules/sales/components/lead-notes/callback-customers-info-popup.component';
import { callBackCustomerListModalComponent } from '@modules/sales/components/lead-notes/callback-customers-list-popup.component';
import { ScheduleCallbackViewModalComponent } from '@modules/sales/components/lead-notes/schedule-callback-view-modal.component';
import { NgxCountdownModule } from '@modules/sales/components/task-management/ngx-countdown/ngx-countdown.module';
import { QueueTransferPopupComponent, UserTransferPopupComponent } from '@modules/sales/components/task-management/open-scape';
import { OpenScapComponent } from '@modules/sales/components/task-management/open-scape/open-scape.component';
import { RebookSnoozeModalComponent } from '@modules/technical-management/components/rebook-snooze';
import { TechnicianStockTakeAodModalComponent } from '@modules/technical-management/components/technician-stock-take-aod-modal/technician-stock-take-aod-modal/technician-stock-take-aod-modal.component';
import { TechnitianInspectionSnoozeModalComponent } from '@modules/technical-management/components/technitian-inspection-snooze/technitian-inspection-snooze-modal.component';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { SelectAutocompleteModule } from 'mat-select-autocomplete';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
// Third party modules;
import { MatIconModule } from '@angular/material';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { NgxPrintModule } from 'ngx-print';
import { SignaturePadModule } from 'ngx-signaturepad';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { LeafLetFullMapViewModalComponent } from './components/leaf-let/leaf-let-full-map-view.component';
import { SharedLeafLetComponent } from './components/leaf-let/shared-leaf-let.component';
import { PrimengCustomDialogComponent } from './components/primeng-custom-dialog';
import { PrimengDeleteConfirmDialogComponent } from './components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { MaterialModule } from './material.module';
import { CustomDatePipe, PhoneMaskPipe } from './pipes';
import { TimeFormatPipe } from './pipes/time-format.pipe';
import { PrimeNgModule } from './primeNg.module';
import { CanDeactivateGuard } from './services/authguards/can-deactivate-route.authguard';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [ConfirmDialogPopupComponent,
    CustomSnackBarComponent, PhoneMaskDirective, CardMaskDirective, PCalendarDirective,
    PopupLoaderComponent, ValidateInputDirective, ControlErrorsDirective, OwlDateTimePickerDirective,
    ControlErrorComponent, ControlErrorContainerDirective, FormSubmitDirective, PhoneMaskPipe,
    InputCustomControlComponent, SelectCustomControlComponent, FileUploadCustomControlCustomControlComponent,
    MatSelectCustomControlComponent, RadioGroupCustomControlComponent, TextAreaCustomControlComponent, ChangeStatusDialogComponent,
    ScheduleCallbackViewModalComponent, callBackCustomerListModalComponent, callBackCustomerInfoModalComponent, OpenScapComponent,
    PrimengStatusConfirmDialogComponent,
    PrimengDeleteConfirmDialogComponent, PrimengMessageConfirmDialogComponent, FindDuplicatePipe, WholeNumberToDecimalPipe,
    ViewPageComponent, PageHeaderComponent, PageListTableComponent,
    ReusableAddNotesWithHistoryPopupComponent,
    LeafLetFullMapViewModalComponent, SharedLeafLetComponent, PageTabComponent, TechnicianStockTakeAodModalComponent,
    PageFormArrayComponent, JobSelectionScanModalComponent,
    ReusableHeaderBreadcrumbContainerComponent, KzMaskDirective, RebookSnoozeModalComponent, TechnitianInspectionSnoozeModalComponent, PrimengCustomDialogComponent, FormatNumberPipe,
    QueueTransferPopupComponent, UserTransferPopupComponent, TimeFormatPipe, CustomDatePipe],
  imports: [
    CommonModule, NgxPrintModule, NgxCountdownModule, ReactiveFormsModule, FormsModule,
    MaterialModule, PrimeNgModule, SelectAutocompleteModule,
    AngularEditorModule,
    TruncateModule,
    NgxMaskModule.forRoot(maskConfig),
    RouterModule, InfiniteScrollModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatNativeDatetimeModule,
    SignaturePadModule,
    DragDropModule, TieredMenuModule, MatIconModule,
    StoreModule.forFeature('TableFilteredData', SharedTableFilteredDataReducer),
    EffectsModule.forFeature([SharedTableActionEffects])
  ],
  providers: [CanDeactivateGuard, DatePipe, DecimalPipe,
    { provide: DatePipe, useClass: CustomDatePipe },
  ],
  exports: [SelectAutocompleteModule,
    NgxMaskModule, TruncateModule, PopupLoaderComponent, ValidateInputDirective, OwlDateTimePickerDirective,
    ControlErrorsDirective, FormSubmitDirective, PhoneMaskDirective, CardMaskDirective, PhoneMaskPipe, InfiniteScrollModule, PCalendarDirective,
    AngularEditorModule, ViewPageComponent,
    InputCustomControlComponent, SelectCustomControlComponent,
    MatSelectCustomControlComponent, RadioGroupCustomControlComponent, TextAreaCustomControlComponent, FileUploadCustomControlCustomControlComponent,
    OpenScapComponent,
    NgxCountdownModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule, PageListTableComponent,
    FindDuplicatePipe,
    WholeNumberToDecimalPipe, PageHeaderComponent,
    SharedLeafLetComponent, PageTabComponent, PageFormArrayComponent,
    ReusableHeaderBreadcrumbContainerComponent, NgxPrintModule,
    KzMaskDirective, DragDropModule, FormatNumberPipe, SignaturePadModule, PrimeNgModule, MatIconModule, TimeFormatPipe, CustomDatePipe],
  entryComponents: [ControlErrorComponent, ConfirmDialogPopupComponent, CustomSnackBarComponent, ChangeStatusDialogComponent,
    ScheduleCallbackViewModalComponent, RebookSnoozeModalComponent, callBackCustomerListModalComponent, callBackCustomerInfoModalComponent,
    JobSelectionScanModalComponent,
    PrimengStatusConfirmDialogComponent, PrimengMessageConfirmDialogComponent, PrimengDeleteConfirmDialogComponent,
    ReusableAddNotesWithHistoryPopupComponent, LeafLetFullMapViewModalComponent, TechnitianInspectionSnoozeModalComponent, PrimengCustomDialogComponent,
    QueueTransferPopupComponent, UserTransferPopupComponent,
    TechnicianStockTakeAodModalComponent]
})
export class SharedModule { }
