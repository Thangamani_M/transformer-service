import { HttpParams } from '@angular/common/http';
import { AbstractControl, FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Sort } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  AddNewFormControlObj, AgentLoginDataLoaded, emailPattern, formConfigs, HttpCancelService, IAfrigisAddressComponents,
  IApplicationResponse, OtherService, RoutingStates, saidPattern, SharedTableFilteredDataCreated, trimValidator
} from '@app/shared';
import { TelephonyApiSuffixModules } from '@modules/sales/shared/utils/telephony.module.enum';
import { Store } from '@ngrx/store';
import * as CryptoJS from 'crypto-js';
import * as moment from 'moment';
import { isBoolean, isUndefined } from 'util';
import { LoginUpdate } from '../../modules/others/auth.actions';
import { CrudService, UserService } from '../services';
import { DIFFERENT_MOBILE_NUMBER_NAMES, LoggedInUserModel } from '../utils';
import { CommonPaginationConfig, MenuCategoriesForPermissions, ModulesBasedApiSuffix, PermissionTypes } from './enums.utils';

const cryptoSecretKey = 'hsbdkdshdG45897GUJHH8798sddf';
const iv = CryptoJS.lib.WordArray.random(128 / 8);
const cryptoOptions = { iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 };

const formatColumn = (column: string): string => {
  const col: any = column.charAt(0).toUpperCase() + column.substring(1);
  return col.match(/[A-Z][a-z]+|[0-9]+/g).join(' ');
};

/**
   * It prepares the Request HttpParams .
   * @param pageIndex The optional parameter of type string.
   * @param pageSize The optional parameter of type string.
   * @param otherParams The optional parameter of type ny.
   * @return The HttpParams.
   */
const prepareGetRequestHttpParams = (pageIndex?: string, pageSize?: string, ...otherParams: any): HttpParams => {
  let params = new HttpParams();
  params = params.append('pageIndex', pageIndex ? pageIndex : CommonPaginationConfig.defaultPageIndex);
  params = params.append('maximumRows', pageSize ? pageSize : CommonPaginationConfig.defaultPageSize);
  if (otherParams && otherParams[0]) {
    otherParams.forEach(param => {
      Object.keys(param).forEach((key) => {
        params = params.append(key, param[key]);
      });
    });
  }
  return params;
};

const prepareRequiredHttpParams = (...otherParams: any): HttpParams => {
  let params: any = new HttpParams();
  otherParams.forEach(param => {
    Object.keys(param).forEach((key) => {
      if (param[key] === '') {
        params[key] = undefined;
      } else {
        params = params.append(key, param[key]);
      }
    });
  });
  return params;
};

const generateQueryParams = (queryParams) => {
  const queryParamsString = new HttpParams({ fromObject: queryParams }).toString();
  return '?' + queryParamsString;
}

const removeLastPluralLetter = (caption: string): string => {
  const slicedString = (caption === 'Cities') ? 'city' : caption.substring(0, (caption.charAt(caption.length - 1) === 's') ? caption.length - 1 : caption.length);
  return formatColumn(slicedString);
};

const setRequiredValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    formGroup.controls[formControlName].setValidators(Validators.compose((formControlName.toLowerCase().includes('email') ||
      formControlName.toLowerCase().includes('notificationto')) ?
      [Validators.required, Validators.email, emailPattern] :
      // formControlName.toLowerCase().includes('cancelreasonname') ? [Validators.required, trimValidator] :
      [Validators.required]));
    formGroup.controls[formControlName].updateValueAndValidity({ emitEvent: false, onlySelf: true });
  });
  formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  return formGroup;
};

const setEmailValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    const control = formGroup.controls[formControlName];
    const existingValidators = control.validator;
    formGroup.controls[formControlName].setValidators(Validators.compose([existingValidators, Validators.email, emailPattern]));
    formGroup.controls[formControlName].updateValueAndValidity({ emitEvent: false, onlySelf: true });
  });
  formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  return formGroup;
};

const setSAIDValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    const control = formGroup.controls[formControlName];
    const existingValidators = control.validator;
    formGroup.controls[formControlName].setValidators(Validators.compose([existingValidators, saidPattern]));
    formGroup.controls[formControlName].updateValueAndValidity({ emitEvent: false, onlySelf: true });
  });
  formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  return formGroup;
};

const setFormArrayControlsRequiredValidator = (formArray: FormArray, formControlNames: Array<string>): FormArray => {
  if (formControlNames.length === 0) { return; }
  formArray.controls.forEach((formGroup: FormGroup) => {
    formControlNames.forEach((formControlName: string) => {
      if (formControlName.toLowerCase().includes('email')) {
        formGroup.controls[formControlName].setValidators(Validators.compose([Validators.required, Validators.email, emailPattern]));
      } else {
        formGroup.controls[formControlName].setValidators(Validators.compose([Validators.required]));
      }
      formGroup.controls[formControlName].updateValueAndValidity({ emitEvent: false, onlySelf: true });
    });
  });
  formArray.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  return formArray;
};

const setFormArrayControlsMinMaxValidator = (formArray: FormArray, formControlNames: Array<string>): FormArray => {
  if (formControlNames.length === 0) { return; }
  formArray.controls.forEach((formGroup: FormGroup) => {
    formControlNames.forEach((formControlName: string) => {
      let minLength: number;
      let maxLength: number;
      const name: string = formControlName.toLowerCase();
      switch (true) {
        case (name.includes('zip') || name.includes('postal')):
          minLength = maxLength = formConfigs.postalCodeMaxLength;
          break;
        case ((name.includes('number') || name.includes('contact')) &&
          !name.includes('phone') && !name.includes('home') && !name.includes('office') && !name.includes('street')):
          minLength = maxLength = formConfigs.phoneNumberMaxLength;
          break;
        case (name.includes('streetno') || name.includes('streetname')):
          minLength = formConfigs.streetNumberMinLength;
          maxLength = formConfigs.streetNumberMaxLength;
          break;
        case (name.includes('streetname')):
          minLength = formConfigs.streetNameMinLength;
          maxLength = formConfigs.streetNameMaxLength;
          break;
        case (name.includes('firstname')):
          minLength = formConfigs.firstNameMinLength;
          maxLength = formConfigs.firstNameMaxLength;
          break;
        case (name.includes('surname') || name.includes('lastname')):
          minLength = formConfigs.lastNameMinLength;
          maxLength = formConfigs.lastNameMaxLength;
          break;
        case (name.includes('buildingno') || name.includes('buildingnumber')):
          minLength = formConfigs.buildingNumberMinLength;
          maxLength = formConfigs.buildingNumberMaxLength;
          break;
        case (name.includes('buildingname')):
          minLength = formConfigs.buildingNameMinLength;
          maxLength = formConfigs.buildingNameMaxLength;
          break;
      }
      formGroup.controls[formControlName].setValidators(Validators.compose([Validators.minLength(minLength), Validators.maxLength(maxLength)]));
      formGroup.controls[formControlName].updateValueAndValidity({ emitEvent: false, onlySelf: true });
    });
  });
  formArray.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  return formArray;
};

const setFormArrayControlsMinMaxValueValidator = (formArray: FormArray, formControlNames: Array<string>): FormArray => {
  if (formControlNames.length === 0) { return; }
  formArray.controls.forEach((formGroup: FormGroup) => {
    formControlNames.forEach((formControlName: string) => {

      let minValue: number;
      let maxValue: number;
      const name: string = formControlName.toLowerCase();
      switch (true) {
        case (name.includes('stockthreshold')):
          minValue = formConfigs.stockThresholdMinValue;
          maxValue = formConfigs.stockThresholdMaxValue;
          break;
        case (name.includes('warrentperiod')):
          minValue = formConfigs.warrentPeriodMinValue;
          maxValue = formConfigs.warrentPeriodMaxValue;
          break;
      }
      formGroup.controls[formControlName].setValidators(Validators.compose([Validators.min(minValue), Validators.max(maxValue)]));
      formGroup.controls[formControlName].updateValueAndValidity({ emitEvent: false, onlySelf: true });
    });
  });
  formArray.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  return formArray;
};

const setMinMaxLengthValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  let minLength: number;
  let maxLength: number;
  formControlNames.forEach((formControlName: string) => {
    const name: string = formControlName.toLowerCase();
    const control = formGroup.controls[formControlName];
    const existingValidators = control.validator;
    switch (true) {
      case (name.includes('zip') || name.includes('postal')):
        minLength = maxLength = formConfigs.postalCodeMaxLength;
        break;
      case ((name.includes('number') || name.includes('contact')) &&
        !name.includes('phone') && !name.includes('home') && !name.includes('office') && !name.includes('street')):
        minLength = maxLength = formConfigs.phoneNumberMaxLength;
        break;
      case (name.includes('streetno') || name.includes('streetname')):
        minLength = formConfigs.streetNumberMinLength;
        maxLength = formConfigs.streetNumberMaxLength;
        break;
      case (name.includes('streetname')):
        minLength = formConfigs.streetNameMinLength;
        maxLength = formConfigs.streetNameMaxLength;
        break;
      case (name.includes('firstname')):
        minLength = formConfigs.firstNameMinLength;
        maxLength = formConfigs.firstNameMaxLength;
        break;
      case (name.includes('surname') || name.includes('lastname')):
        minLength = formConfigs.lastNameMinLength;
        maxLength = formConfigs.lastNameMaxLength;
        break;
      case (name.includes('buildingno') || name.includes('buildingnumber')):
        minLength = formConfigs.buildingNumberMinLength;
        maxLength = formConfigs.buildingNumberMaxLength;
        break;
      case (name.includes('buildingname')):
        minLength = formConfigs.buildingNameMinLength;
        maxLength = formConfigs.buildingNameMaxLength;
        break;
    }
    formGroup.controls[formControlName].setValidators(Validators.compose([existingValidators, Validators.minLength(minLength), Validators.maxLength(maxLength)]));
    formGroup.controls[formControlName].updateValueAndValidity({ emitEvent: false, onlySelf: true });
  });
  formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  return formGroup;
};

const removeAllFormValidators = (formGroup: FormGroup): FormGroup => {
  Object.keys(formGroup.controls).forEach(key => {
    if (formGroup.get(key) instanceof FormControl) {
      formGroup.get(key).clearValidators();
      formGroup.get(key).updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else if (formGroup.get(key) instanceof FormArray) {

    }
  });
  return formGroup;
};

const removeFormValidators = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  formControlNames.forEach((name: string) => {
    if (formGroup.get(name) instanceof FormControl) {
      formGroup.get(name).clearValidators();
      formGroup.get(name).updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else if (formGroup.get(name) instanceof FormArray) {

    }
  });
  return formGroup;
};

const removeAllFormControls = (formGroup: FormGroup): FormGroup => {
  Object.keys(formGroup.controls).forEach(key => {
    if (formGroup.get(key) instanceof FormControl) {
      formGroup.removeControl(key);
    } else if (formGroup.get(key) instanceof FormArray) {

    }
  });
  return formGroup;
};

const clearFormControlValidators = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  formControlNames.forEach((name: string) => {
    if (formGroup.get(name) instanceof FormControl) {
      formGroup.get(name).clearValidators();
      formGroup.get(name).updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else if (formGroup.get(name) instanceof FormArray) {

    }
  });
  return formGroup;
};

const addFormControls = (formGroup: FormGroup, formControlNames: Array<string | AddNewFormControlObj> | object): FormGroup => {
  const loopableVariable = (formControlNames instanceof Array) ? formControlNames : Object.keys(formControlNames);
  loopableVariable.forEach((name: string | AddNewFormControlObj) => {
    const controlName = name.hasOwnProperty('controlName') ? name['controlName'] : name;
    if (name.hasOwnProperty('controlName')) {
      if (formGroup.controls.hasOwnProperty(name['controlName'])) {
        formGroup.get(controlName).setValue(name['defaultValue'], { disabled: name['isDisabled'] ? name['isDisabled'] : false });
      }
      else {
        formGroup.addControl(controlName, new FormControl({ value: name['defaultValue'], disabled: name['isDisabled'] }));
      }
      formGroup.get(controlName).updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else {
      formGroup.addControl(controlName, new FormControl((null)));
    }
    formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  });
  return formGroup;
};

const removeFormControls = (formGroup: FormGroup, formControlNames: Array<string> | object): FormGroup => {
  const loopableVariable = (formControlNames instanceof Array) ? formControlNames : Object.keys(formControlNames);
  loopableVariable.forEach((name: string) => {
    if (formGroup.get(name) && formGroup.get(name) instanceof FormControl) {
      formGroup.removeControl(name);
      formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else if (formGroup.get(name) instanceof FormArray) {
      while ((formGroup.get(name) as FormArray).length !== 0) {
        (formGroup.get(name) as FormArray).removeAt(0);
      }
      if ((formGroup.get(name) as FormArray).length == 0) {
        formGroup.removeControl(name);
      }
    }
  });
  return formGroup;
};
interface IDefaultValuesForFormControls {
  formControlName: string;
  value?: string;
  isRequired?: boolean;
  shouldRemoveAllValidators?: boolean;
}

const enableFormControls = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  formControlNames.forEach((name: string) => {
    if (formGroup.get(name) instanceof FormControl) {
      formGroup.get(name).enable({ emitEvent: false, onlySelf: true });
      formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else if (formGroup.get(name) instanceof FormArray) {
    }
  });
  return formGroup;
};

const enableWithDefaultFormControlValues = (formGroup: FormGroup, formControlNamesWithValues: Array<IDefaultValuesForFormControls>): FormGroup => {
  formControlNamesWithValues.forEach((defaultValueForFormControl: IDefaultValuesForFormControls) => {
    if (formGroup.get(defaultValueForFormControl.formControlName) instanceof FormControl) {
      formGroup.get(defaultValueForFormControl.formControlName).enable({ emitEvent: false, onlySelf: true });
      formGroup.get(defaultValueForFormControl.formControlName).setValidators(defaultValueForFormControl.isRequired ? [Validators.required] : []);
      formGroup.get(defaultValueForFormControl.formControlName).setValue(defaultValueForFormControl.value);
      formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else if (formGroup.get(defaultValueForFormControl.formControlName) instanceof FormArray) {
      // let formArrayFormControls=formGroup.get(name) as FormArray;
      // formArrayFormControls.controls.forEach((formArrayControl:string)=>{
      //     formGroup.removeControl(formArrayControl);
      //     formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
      // })
    }
  });
  return formGroup;
};

const disableFormControls = (formGroup: FormGroup, formControlNames: Array<string> | object): FormGroup => {
  const loopableVariable = (formControlNames instanceof Array) ? formControlNames : Object.keys(formControlNames);
  loopableVariable.forEach((name: string) => {
    if (formGroup.get(name) instanceof FormControl) {
      formGroup.get(name).disable({ emitEvent: false, onlySelf: true });
      formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else if (formGroup.get(name) instanceof FormArray) {
      // let formArrayFormControls=formGroup.get(name) as FormArray;
      // formArrayFormControls.controls.forEach((formArrayControl:string)=>{
      //     formGroup.removeControl(formArrayControl);
      //     formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
      // })
    }
  });
  return formGroup;
};

const disableWithDefaultFormControlValues = (formGroup: FormGroup, formControlNamesWithValues: Array<IDefaultValuesForFormControls>): FormGroup => {
  formControlNamesWithValues.forEach((defaultValueForFormControl: IDefaultValuesForFormControls) => {
    if (formGroup.get(defaultValueForFormControl.formControlName) instanceof FormControl) {
      formGroup.get(defaultValueForFormControl.formControlName).disable({ emitEvent: false, onlySelf: true });
      formGroup.get(defaultValueForFormControl.formControlName).setValue(defaultValueForFormControl.value);
      if (defaultValueForFormControl.shouldRemoveAllValidators) {
        formGroup.get(defaultValueForFormControl.formControlName).clearValidators();
      }
      formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
    } else if (formGroup.get(defaultValueForFormControl.formControlName) instanceof FormArray) {
      // let formArrayFormControls=formGroup.get(name) as FormArray;
      // formArrayFormControls.controls.forEach((formArrayControl:string)=>{
      //     formGroup.removeControl(formArrayControl);
      //     formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
      // })
    }
  });
  return formGroup;
};

// for development purpose
const findInvalidFormControls = (formGroupOrArray: FormGroup | FormArray): Array<string> | Array<{}> => {
  let invalidFormControlNames = [];
  if (formGroupOrArray instanceof FormGroup) {
    Object.keys(formGroupOrArray.controls).forEach((formControlName: string) => {
      if (formGroupOrArray.controls[formControlName].status === "INVALID") {
        invalidFormControlNames.push(formControlName);
      }
    });
  }
  else {
    formGroupOrArray.controls.forEach((formArrayControl: FormGroup) => {
      Object.keys(formArrayControl.controls).forEach((formControlName: string) => {
        if (formGroupOrArray.controls[formControlName].status === "INVALID") {
          invalidFormControlNames.push({ formGroupName: formArrayControl.controls, formControlName });
        }
      });
    })
  }
  return invalidFormControlNames;
};

const findDisabledFormControls = (formGroup: FormGroup): Array<string> => {
  let invalidFormControlNames = [];
  Object.keys(formGroup.controls).forEach((formControlName: string) => {
    if (formGroup.controls[formControlName].status === "DISABLED") {
      invalidFormControlNames.push(formControlName);
    }
  });
  return invalidFormControlNames;
};

const sortCompare = (a: any, b: any, isStr: boolean) => {
  return isStr
    ? sortAlphanumeric(trim(a), trim(b))
    : (parseInt(a, 10) < parseInt(b, 10) ? -1 : 1);
};

const trim = (a: any) => {
  return (a = typeof a === 'string' ? a.trim().toLowerCase() : a);
};

const sortAlphanumeric = (a: string, b: string): number => {
  return a.localeCompare(b, 'en', { numeric: true });
};

const sortOrder = (data, sort: Sort, dateField?, BooleanField?): any[] => {
  if (sort.direction === '') {
    return data.slice();
  }
  return dateField && dateField.field === sort.active ? dateFilter(data, sort, dateField) : nonDateFilter(data, sort);
};

const dateFilter = (data, sort, dateField) => {
  let dt = [];
  const empt = [];
  data.map(k => {
    if (!k[sort.active]) {
      empt.push(k);
    } else {
      dt.push(k);
    }
  });
  dt = sortDateCustomizer(dt.slice(), sort, new RegExp(dateField.pattern, 'i'));
  return (sort.direction === 'desc' ? [...dt, ...empt] : [...empt, ...dt]).slice();
};

const getAscDesc = (chk: string) => {
  return chk === 'desc' ? 'asc' : 'desc';
};

const nonDateFilter = (data, sort) => {
  let num = [];
  let str = [];
  const empt = [];
  data.map(k => {
    sort.direction = isBoolean(k[sort.active]) ? getAscDesc(sort.direction) : sort.direction;
    if (!k[sort.active]) {
      empt.push(k);
    } else if (isNaN(k[sort.active])) {
      str.push(k);
    } else {
      num.push(k);
    }
  });
  num = sortCustomizer(num.slice(), sort, false);
  str = sortCustomizer(str.slice(), sort, true);
  return (sort.direction === 'desc' ? [...str, ...num, ...empt] : [...empt, ...num, ...str]).slice();
};

const sortDateCustomizer = (val: any[], sort: Sort, pattern) => {
  return val.sort((a, b) => sort.direction === 'asc'
    ? sortCompare(new Date(a[sort.active].match(pattern)[0]).getTime(), new Date(b[sort.active].match(pattern)[0]).getTime(), false)
    : sortCompare(new Date(b[sort.active].match(pattern)[0]).getTime(), new Date(a[sort.active].match(pattern)[0]).getTime(), false));
};

const sortCustomizer = (val: any[], sort: Sort, isStr: boolean) => {
  return val.sort((a, b) => sort.direction === 'asc'
    ? sortCompare(a[sort.active], b[sort.active], isStr)
    : sortCompare(b[sort.active], a[sort.active], isStr));
};

const phoneMask = (value, prefix?) => {
  let newVal = value.replace(/\D/g, '');
  if (newVal.length === 0) {
    newVal = '';
  } else if (newVal.length <= 3) {
    newVal = newVal.replace(/^(\d{0,3})/, '$1');
  } else if (newVal.length <= 6) {
    newVal = newVal.replace(/^(\d{0,3})(\d{0,3})/, '$1 $2');
  } else if (newVal.length === 8) {
    newVal = newVal.replace(/^(\d{0,2})(\d{0,2})(\d{0,2})(\d{0,2})/, '$1 $2 $3 $4');
  } else if (newVal.length === 9) {
    newVal = newVal.replace(/^(\d{0,2})(\d{0,3})(\d{0,4})/, '$1 $2 $3');
  } else {
    newVal = newVal.length > 10 ? newVal.substring(0, 10) : newVal;
    newVal = newVal.replace(/^(\d{0,3})(\d{0,3})(\d{0,4})/, '$1 $2 $3');
  }
  return value ? `${prefix || ''} ${newVal}` : value;
};

const isString = (val): boolean => typeof val === 'string';

const convertCamelCasesToSpaces = (ngControlName: string): string => {
  if (!ngControlName) return;
  ngControlName = ngControlName.replace(/([a-z])([A-Z])/g, '$1 $2');
  ngControlName = ngControlName.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2');
  ngControlName = ngControlName.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2 $3');
  ngControlName = ngControlName.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2 $3 $4');
  ngControlName = ngControlName.replace('Id', '');
  return capitalizeFirstLetterOfEachString(ngControlName);
}

const capitalizeFirstLetterOfEachString = (ngControlName: string): string => {
  return ngControlName = ngControlName.toLowerCase()
    .split(' ')
    .map((c) => c.charAt(0).toUpperCase() + c.substring(1))
    .join(' ');
}

const removeFormControlError = (formGroup: FormGroup, removableErrorKey?: string, errorRemovableFormControlName?: string): FormGroup => {
  // find and deletes the given formcontrol errors with the given error key
  if (removableErrorKey && errorRemovableFormControlName && formGroup.controls[errorRemovableFormControlName].errors &&
    formGroup.controls[errorRemovableFormControlName].errors[removableErrorKey]) {
    delete formGroup.controls[errorRemovableFormControlName].errors[removableErrorKey];
    formGroup.controls[errorRemovableFormControlName].updateValueAndValidity();
    formGroup.updateValueAndValidity();
  }
  // find and deletes all formcontrol errors with the given error key
  else if (removableErrorKey && !errorRemovableFormControlName) {
    Object.keys(formGroup.controls ? formGroup.controls : {}).forEach((formControlName: string) => {
      if (removableErrorKey !== 'minlength' && formGroup.controls[formControlName].errors &&
        formGroup.controls[formControlName].errors[removableErrorKey]) {
        delete formGroup.controls[formControlName].errors[removableErrorKey];
        if (formGroup.controls[formControlName].errors && !formGroup.controls[formControlName].errors[removableErrorKey]) {
          formGroup.controls[formControlName].setErrors(null);
        }
      }
      else if (removableErrorKey == 'minlength' && formGroup.controls[formControlName].value &&
        formGroup.controls[formControlName].errors && formGroup.controls[formControlName].errors[removableErrorKey]) {
        if (formGroup.controls[formControlName].errors && formGroup.controls[formControlName].value.replace(/\s/g, "").length == 9 ||
          formGroup.controls[formControlName].value.replace(/\s/g, "").length == 10) {
          delete formGroup.controls[formControlName].errors[removableErrorKey];
        }
        if (formGroup.controls[formControlName].errors && !formGroup.controls[formControlName].errors[removableErrorKey]) {
          formGroup.controls[formControlName].setErrors(null);
        }
      }
    });
  }
  // find and deletes all formcontrol and all errors with the additional logic inclusion of mobile number minlength
  else if (!removableErrorKey && !errorRemovableFormControlName) {
    Object.keys(formGroup.controls ? formGroup.controls : {}).forEach((formControlName: string) => {
      Object.keys(formGroup.controls[formControlName].errors ? formGroup.controls[formControlName].errors : {}).forEach((errorKey: string) => {
        if (errorKey !== 'minlength' && formGroup.controls[formControlName].errors && formGroup.controls[formControlName].errors[errorKey]) {
          delete formGroup.controls[formControlName].errors[errorKey];
          if (formGroup.controls[formControlName].errors && !formGroup.controls[formControlName].errors[errorKey]) {
            formGroup.controls[formControlName].setErrors(null);
          }
        }
        else if (errorKey == 'minlength' && formGroup.controls[formControlName].value) {
          if (formGroup.controls[formControlName].errors && formGroup.controls[formControlName].value.replace(/\s/g, "").length == 9 ||
            formGroup.controls[formControlName].value.replace(/\s/g, "").length == 10) {
            delete formGroup.controls[formControlName].errors[errorKey];
          }
          if (formGroup.controls[formControlName].errors && !formGroup.controls[formControlName].errors[errorKey]) {
            formGroup.controls[formControlName].setErrors(null);
          }
        }
      });
    });
  }
  // find and deletes the given formcontrol and all errors with the additional logic inclusion of mobile number minlength
  else if (!removableErrorKey && errorRemovableFormControlName) {
    Object.keys(formGroup.controls[errorRemovableFormControlName].errors ? formGroup.controls[errorRemovableFormControlName].errors : {}).forEach((errorKey: string) => {
      if (errorKey !== 'minlength' && formGroup.controls[errorRemovableFormControlName].errors) {
        delete formGroup.controls[errorRemovableFormControlName].errors[errorKey];
        if (formGroup.controls[errorRemovableFormControlName].errors && !formGroup.controls[errorRemovableFormControlName].errors[errorKey]) {
          formGroup.controls[errorRemovableFormControlName].setErrors(null);
        }
      }
      else if (errorKey == 'minlength' && formGroup.controls[errorRemovableFormControlName].value) {
        if (formGroup.controls[errorRemovableFormControlName].errors && formGroup.controls[errorRemovableFormControlName].value.replace(/\s/g, "").length == 9 ||
          formGroup.controls[errorRemovableFormControlName].value.replace(/\s/g, "").length == 10) {
          delete formGroup.controls[errorRemovableFormControlName].errors[errorKey];
        }
        if (formGroup.controls[errorRemovableFormControlName].errors && !formGroup.controls[errorRemovableFormControlName].errors[errorKey]) {
          formGroup.controls[errorRemovableFormControlName].setErrors(null);
        }
      }
    });
  }
  return formGroup;
}

// Compare min and max value validator
const setMinMaxValidator = (formGroup: FormGroup, formControlNames: Array<{}>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: object) => {
    const control = formGroup.controls[formControlName['formControlName']];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, checkMinMax(formGroup, formControlName)]));
    // formGroup.controls[formControlName['formControlName']].updateValueAndValidity();
    // formGroup.controls[formControlName['compareWith']].updateValueAndValidity();
    //{ emitEvent: false, onlySelf: true }
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};

function checkMinMax(group: FormGroup, compareFieldsData): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const skipZero = compareFieldsData['skipZero'] ? compareFieldsData['skipZero'] : false;
    const controlValue = +control.value;
    const compareControlValue = +group.get(compareFieldsData['compareWith']).value;
    let returnData = null;
    if (controlValue && compareControlValue) {
      switch (compareFieldsData.type) {
        case 'max': // Comparison for field which has to be greater
          if (controlValue <= compareControlValue) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            // control.setErrors({ lesserGreaterThan : {
            //     formControlName: 'Range To',
            //     comparewith: 'range from',
            //     type: 'greater'
            // } });
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'greater'
              }
            });
            returnData = {
              // lesserGreaterThan: {
              //     formControlName: 'Range To',
              //     comparewith: 'range from',
              //     type: 'greater'
              // }
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'greater'
              }
            }
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
        case 'min': // Comparison for field which has to be lesser
          if (controlValue >= compareControlValue) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            // control.setErrors({ lesserGreaterThan : {
            //     formControlName: 'Range From',
            //     comparewith: 'range to',
            //     type: 'greater'
            // } });
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'  // greater
              }
            });
            returnData = {
              // lesserGreaterThan: {
              //     formControlName: 'Range From',
              //     comparewith: 'range to',
              //     type: 'lesser'
              // }
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
        case 'minEqual': // Comparison for field which has to be lesser
          if (controlValue > compareControlValue) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'  // greater
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: convertCamelCasesToSpaces(compareFieldsData['compareWith']),
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
      }
      return returnData;
    }
    else if (skipZero) {
      switch (compareFieldsData.type) {
        case 'minEqual':
          if (controlValue > compareControlValue) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: convertCamelCasesToSpaces(compareFieldsData['compareWith']),
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
      }
      return returnData;
    }
    else {
      return returnData;
    }
  }
}

// Compare max value validator
const setMaxValidatorWithValue = (formGroup: FormGroup, formControlNames: Array<{}>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: object) => {
    const control = formGroup.controls[formControlName['formControlName']];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, Validators.max(formControlName['maxValue'])]));
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};

// Compare min value validator
const setMinValidatorWithValue = (formGroup: FormGroup, formControlNames: Array<{}>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: object) => {
    const control = formGroup.controls[formControlName['formControlName']];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, Validators.min(formControlName['minValue'])]));
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};

// set percentage validator
const setPercentageValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    const control = formGroup.controls[formControlName];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, checkPercentage(formGroup, formControlName)]));
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};


function checkPercentage(group: FormGroup, compareFieldsData): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const skipZero = compareFieldsData['skipZero'] ? compareFieldsData['skipZero'] : false;
    const controlValue = +control.value;
    let returnData = null;
    if (controlValue > 100 || controlValue < 0) {
      return returnData = {
        percentage: true,
      };
    } else {
      return returnData;
    }
  }
}
// set date validator
const setDateValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    const control = formGroup.controls[formControlName];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, checkDate(formGroup, formControlName)]));
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};


function checkDate(group: FormGroup, compareFieldsData): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const skipZero = compareFieldsData['skipZero'] ? compareFieldsData['skipZero'] : false;
    const controlValue = control.value ? +control.value : null;
    let returnData = null;
    // if (new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + controlValue).getDate() !== +controlValue) {
    if (!!controlValue && (controlValue < 0 || controlValue > 60)) {
      return returnData = {
        checkdate: true,
      };
    } else {
      return returnData;
    }
  }
}
// set minute validator
const setHourValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    const control = formGroup.controls[formControlName];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, checkHour(formGroup, formControlName)]));
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};
function checkHour(group: FormGroup, compareFieldsData): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const skipZero = compareFieldsData['skipZero'] ? compareFieldsData['skipZero'] : false;
    const controlValue = control.value?.toString()?.indexOf(':') != -1 ? control.value?.split(':')[0] : control.value ? +control.value : null;
    let returnData = null;
    let maxValue = 60;
    switch (compareFieldsData) {
      case 'escalationTimehoursMinutes':
        maxValue = formConfigs.thousands;
        break;
    }
    if (!!controlValue && (controlValue < 0 || controlValue >= maxValue)) {
      return returnData = {
        checkhour: true,
      };
    } else {
      return returnData;
    }
  }
}
// set minute validator
const setMinuteValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    const control = formGroup.controls[formControlName];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, checkMinute(formGroup, formControlName)]));
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};
function checkMinute(group: FormGroup, compareFieldsData): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const skipZero = compareFieldsData['skipZero'] ? compareFieldsData['skipZero'] : false;
    const controlValue = control.value?.toString()?.indexOf(':') != -1 ? control.value?.split(':')[1] : control.value ? +control.value : null;
    let returnData = null;
    // if (new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + controlValue).getDate() !== +controlValue) {
    if (!!controlValue && (controlValue < 0 || controlValue > 60)) {
      return returnData = {
        checkminute: true,
      };
    } else {
      return returnData;
    }
  }
}
// set sec validator
const setSecValidator = (formGroup: FormGroup, formControlNames: Array<string>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    const control = formGroup.controls[formControlName];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, checkSec(formGroup, formControlName)]));
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};
function checkSec(group: FormGroup, compareFieldsData): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const skipZero = compareFieldsData['skipZero'] ? compareFieldsData['skipZero'] : false;
    const controlValue = control.value ? +control.value : null;
    let returnData = null;
    // if (new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + controlValue).getDate() !== +controlValue) {
    if (!!controlValue && (controlValue < 0 || controlValue > 60)) {
      return returnData = {
        checksec: true,
      };
    } else {
      return returnData;
    }
  }
}


// Compare min and max value validator
const setMinMaxTimeValidator = (formGroup: FormGroup, formControlNames: Array<{}>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: object) => {
    const control = formGroup.controls[formControlName['formControlName']];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, checkMinMaxTime(formGroup, formControlName)]));
    // formGroup.controls[formControlName['formControlName']].updateValueAndValidity();
    // formGroup.controls[formControlName['compareWith']].updateValueAndValidity();
    //{ emitEvent: false, onlySelf: true }
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};

function checkMinMaxTime(group: FormGroup, compareFieldsData): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const skipZero = compareFieldsData['skipZero'] ? compareFieldsData['skipZero'] : false;
    const controlValue = control.value;
    const compareControlValue = group.get(compareFieldsData['compareWith']).value;
    let returnData = null;
    if (controlValue && compareControlValue) {
      switch (compareFieldsData.type) {
        case 'max': // Comparison for field which has to be greater
          if (moment(controlValue).format('HH:mm') <= moment(compareControlValue).format('HH:mm')) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'greater'
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'greater'
              }
            }
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
        case 'min': // Comparison for field which has to be lesser
          if (moment(controlValue).format('HH:mm') >= moment(compareControlValue).format('HH:mm')) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'  // greater
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
        case 'minEqual': // Comparison for field which has to be lesser
          if (moment(controlValue).format('HH:mm') > moment(compareControlValue).format('HH:mm')) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'  // greater
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: convertCamelCasesToSpaces(compareFieldsData['compareWith']),
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
      }
      return returnData;
    }
    else if (skipZero) {
      switch (compareFieldsData.type) {
        case 'minEqual':
          if (moment(controlValue).format('HH:mm') > moment(compareControlValue).format('HH:mm')) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: convertCamelCasesToSpaces(compareFieldsData['compareWith']),
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
      }
      return returnData;
    }
    else {
      return returnData;
    }
  }
}


// Compare min and max value validator
const setMinMaxDateValidator = (formGroup: FormGroup, formControlNames: Array<{}>): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: object) => {
    const control = formGroup.controls[formControlName['formControlName']];
    const existingValidators = control.validator;
    control.setValidators(Validators.compose([existingValidators, checkMinMaxDate(formGroup, formControlName)]));
    // formGroup.controls[formControlName['formControlName']].updateValueAndValidity();
    // formGroup.controls[formControlName['compareWith']].updateValueAndValidity();
    //{ emitEvent: false, onlySelf: true }
  });
  formGroup.updateValueAndValidity();
  return formGroup;
};

function checkMinMaxDate(group: FormGroup, compareFieldsData): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const skipZero = compareFieldsData['skipZero'] ? compareFieldsData['skipZero'] : false;
    const controlValue = control.value;
    const compareControlValue = group.get(compareFieldsData['compareWith']).value;
    let returnData = null;
    if (controlValue && compareControlValue) {
      switch (compareFieldsData.type) {
        case 'max': // Comparison for field which has to be greater
          if (moment(controlValue).format('yyyy-MM-d') <= moment(compareControlValue).format('yyyy-MM-d')) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'greater'
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'greater'
              }
            }
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
        case 'min': // Comparison for field which has to be lesser
          if (moment(controlValue).format('yyyy-MM-d') >= moment(compareControlValue).format('yyyy-MM-d')) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'  // greater
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
        case 'minEqual': // Comparison for field which has to be lesser
          if (moment(controlValue).format('yyyy-MM-d') > moment(compareControlValue).format('yyyy-MM-d')) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'  // greater
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: convertCamelCasesToSpaces(compareFieldsData['compareWith']),
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
      }
      return returnData;
    }
    else if (skipZero) {
      switch (compareFieldsData.type) {
        case 'minEqual':
          if (moment(controlValue).format('yyyy-MM-d') > moment(compareControlValue).format('yyyy-MM-d')) {
            group = removeFormControlError(group, 'lesserGreaterThan');
            control.setErrors({
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: compareFieldsData['compareWith'],
                type: 'lesser'
              }
            });
            returnData = {
              lesserGreaterThan: {
                formControlName: compareFieldsData['formControlName'],
                comparewith: convertCamelCasesToSpaces(compareFieldsData['compareWith']),
                type: 'lesser'
              }
            };
          } else {
            group = removeFormControlError(group, 'lesserGreaterThan');
          }
          break;
      }
      return returnData;
    }
    else {
      return returnData;
    }
  }
}

const getModelbasedKeyToFindId = (obj: object): string => {
  for (let key of Object.keys(obj)) {
    if (key.toLowerCase().includes("id")) {
      return key;
    }
  }
}

// for development only
const autofillFormFieldValues = (formGroup: FormGroup): FormGroup => {
  Object.keys(formGroup.controls).forEach((key) => {
    if (formGroup.controls[key] instanceof FormGroup) {
      let nestedFormGroup = formGroup.controls[key] as FormGroup;
      Object.keys(nestedFormGroup.controls).forEach((nestedFormControlKey) => {
        if (nestedFormGroup.controls[nestedFormControlKey] instanceof FormControl &&
          nestedFormGroup.controls[nestedFormControlKey].enabled) {
          let nestedFormControlName = nestedFormControlKey.toLowerCase();
          switch (true) {
            case nestedFormControlName.includes('name'):
              nestedFormGroup.controls[nestedFormControlKey].setValue(randomNameOrNumberGenerator('name'));
              break;
            case (nestedFormControlName.includes('email')):
              nestedFormGroup.controls[nestedFormControlKey].setValue(randomNameOrNumberGenerator('name') + "@gmail.in");
              break;
            case ((nestedFormControlName.includes('number') || nestedFormControlName.includes('mobile') ||
              nestedFormControlName.includes('office')) && !nestedFormControlName.includes('countrycode')):
              nestedFormGroup.controls[nestedFormControlKey].setValue(randomNameOrNumberGenerator('number'));
              break;
            default:
              break;
          }
        }
      })
    }
    else if (formGroup.controls[key] instanceof FormControl &&
      formGroup.controls[key].enabled) {
      let formControlName = key.toLowerCase();
      switch (true) {
        case formControlName.includes('name'):
          formGroup.controls[key].setValue(randomNameOrNumberGenerator('name'));
          break;
        case (formControlName.includes('email')):
          formGroup.controls[key].setValue(randomNameOrNumberGenerator('name') + "@gmail.in");
          break;
        case ((formControlName.includes('number') || formControlName.includes('mobile') ||
          formControlName.includes('office')) && !formControlName.includes('countrycode')):
          formGroup.controls[key].setValue(randomNameOrNumberGenerator('number'));
          break;
      }
    }
  })
  return formGroup;
}

const removeSpaceMasksFromFormControls = (formGroup: FormGroup, formControlNames: string[]): FormGroup => {
  if (formControlNames.length === 0) { return; }
  formControlNames.forEach((formControlName: string) => {
    formGroup.controls[formControlName].setValidators(Validators.compose(formControlName.toLowerCase().includes('email') ?
      [Validators.required, Validators.email, emailPattern] :
      formControlName.toLowerCase().includes('cancelreasonname') ? [Validators.required, trimValidator] : [Validators.required]));
    formGroup.controls[formControlName].updateValueAndValidity({ emitEvent: false, onlySelf: true });
  });
  formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });

  Object.keys(formGroup.controls).forEach((key) => {
    if (formGroup.controls[key] instanceof FormGroup) {
      let nestedFormGroup = formGroup.controls[key] as FormGroup;
      Object.keys(nestedFormGroup.controls).forEach((nestedFormControlKey) => {
        if (nestedFormGroup.controls[nestedFormControlKey] instanceof FormControl &&
          nestedFormGroup.controls[nestedFormControlKey].enabled) {
          let nestedFormControlName = nestedFormControlKey.toLowerCase();
          switch (true) {
            case nestedFormControlName.includes('name'):
              nestedFormGroup.controls[nestedFormControlKey].setValue(randomNameOrNumberGenerator('name'));
              break;
            case (nestedFormControlName.includes('email')):
              nestedFormGroup.controls[nestedFormControlKey].setValue(randomNameOrNumberGenerator('name') + "@gmail.in");
              break;
            case ((nestedFormControlName.includes('number') || nestedFormControlName.includes('mobile') ||
              nestedFormControlName.includes('office')) && !nestedFormControlName.includes('countrycode')):
              nestedFormGroup.controls[nestedFormControlKey].setValue(randomNameOrNumberGenerator('number'));
              break;
            default:
              break;
          }
        }
      })
    }
    else if (formGroup.controls[key] instanceof FormControl &&
      formGroup.controls[key].enabled) {
      let formControlName = key.toLowerCase();
      switch (true) {
        case formControlName.includes('name'):
          formGroup.controls[key].setValue(randomNameOrNumberGenerator('name'));
          break;
        case (formControlName.includes('email')):
          formGroup.controls[key].setValue(randomNameOrNumberGenerator('name') + "@gmail.in");
          break;
        case ((formControlName.includes('number') || formControlName.includes('mobile') ||
          formControlName.includes('office')) && !formControlName.includes('countrycode')):
          formGroup.controls[key].setValue(randomNameOrNumberGenerator('number'));
          break;
      }
    }
  })
  return formGroup;
}

const randomNameOrNumberGenerator = (type: 'name' | 'number'): string | number => {
  if (type == 'name') {
    let letters = 'abcdefghijklmn';
    let name = '';
    for (let i = 0; i < 6; i++) {
      name += letters[Math.floor(Math.random() * 8)];
    }
    return name;
  }
  else {
    let numbers = '123456789';
    let number = '';
    for (let i = 0; i < 9; i++) {
      number += numbers[Math.floor(Math.random() * 8)];
    }
    return number;
  }
}

interface AutoCompleteInputPropertiesType {
  displayProperty: string;
  valueProperty: string;
}

const prepareAutoCompleteListFormatForMultiSelection = (list: Array<object>, areIdsString = false,
  autoCompleteInputPropertiesType?: AutoCompleteInputPropertiesType): Array<object> => {
  if (!list || list.length == 0) {
    return;
  }
  let autocompleteOptionsList = [];
  for (let i = 0; i < list.length; i++) {
    let object = {};
    object['display'] = autoCompleteInputPropertiesType?.displayProperty ? list[i][autoCompleteInputPropertiesType.displayProperty] : list[i]['displayName'];
    if (areIdsString) {
      object['value'] = autoCompleteInputPropertiesType?.valueProperty ? list[i][autoCompleteInputPropertiesType.valueProperty] : list[i]['id'].toString();
    }
    else {
      object['value'] = autoCompleteInputPropertiesType?.valueProperty ? list[i][autoCompleteInputPropertiesType.valueProperty] : list[i]['id'];
    }
    autocompleteOptionsList.push(object);
  }
  return autocompleteOptionsList;
}

const validateLatLongFormat = (latlng): boolean => {
  var latlngArray = latlng.split(",");
  let isLatLongValid = true;
  for (var i = 0; i < latlngArray.length; i++) {
    if (isNaN(latlngArray[i]) || latlngArray[i] < -127 || latlngArray[i] > 75) {
      isLatLongValid = false;
      break;
    }
  }
  return isLatLongValid;
}

const retrieveUniqueObjects = (list: Array<object>): Array<object> => {
  return Array.from(new Set(list));
}

const deleteObjectProps = (object: object, deletableProperties: Array<string>): object => {
  deletableProperties.forEach((deletableProperty) => {
    delete object[deletableProperty];
  });
  return object;
}

const addIsCheckedProperty = (list: Array<object>): Array<object> => {
  list.forEach((obj) => {
    obj['isChecked'] = false;
  });
  return list;
}

const convertTwelevHoursToTwentyFourHours = (twelevHours: string): string => {
  let expectedValue = twelevHours.substring(0, twelevHours.length - 3).replace(/\s/g, "");
  let splittedExpectedValue = expectedValue.split(':');
  if (twelevHours.includes('PM')) {
    expectedValue = (Number(splittedExpectedValue[0]) + 12).toString() + ":" + splittedExpectedValue[1];
  }
  return expectedValue;
}

const convertTwentyFourHoursToTwelevHours = (twentyFourHours: Date | string): string => {
  let hours = new Date(twentyFourHours).getHours();
  let minutes = ("0" + new Date(twentyFourHours).getMinutes()).substr(-2);
  if (Number(hours) <= 12) {
    return (hours + ":" + minutes).toString() + " AM";
  }
  else {
    return (hours - 12 + ":" + minutes).toString() + " PM";
  }
}

const convertCustomHoursAndMinutesToDateFormat = (timeString: string): Date => {
  let expectedValue = timeString.substring(0, timeString.length - 2).replace(/\s/g, "");
  let splittedExpectedValue = expectedValue.split(':');
  let convertedDate = new Date();
  if (timeString.includes("PM")) {
    convertedDate.setHours(Number(splittedExpectedValue[0]) + 12);
  }
  else {
    convertedDate.setHours(Number(splittedExpectedValue[0]));
  }
  convertedDate.setMinutes(Number(splittedExpectedValue[1]));
  return convertedDate
}

const removePropsWhichHasNoValuesFromObject = (object: Object): Object => {
  Object.keys(object).forEach(key => (!object[key] && object[key] !== undefined) && delete object[key]);
  return object;
}

const filterFormControlNamesWhichHasValues = (object: Object): Object => {
  for (let key in object) {
    if (typeof object[key] == 'string' && object[key] !== '') {
      if (key.toLowerCase().includes('countrycode')) {
        let mobileFormControlName = key.slice(0, key.length - 11);
        if (!object.hasOwnProperty(mobileFormControlName) || !object[mobileFormControlName]) {
          delete object[key];
        }
      }
    }
    else if ((typeof object[key] == 'string' || object[key] === null) && !object[key]) {
      delete object[key];
    }
    else if (typeof object[key] == 'object' && Object.keys(object[key]).length > 0) {
      let nestedObject = object[key];
      for (let nestedKey in nestedObject) {
        if (typeof nestedObject[nestedKey] == 'string' && nestedObject[nestedKey] !== '') {
          if (nestedKey.toLowerCase().includes('countrycode')) {
            let nestedMobileFormControlName = nestedKey.slice(0, nestedKey.length - 11);
            if (!nestedObject.hasOwnProperty(nestedMobileFormControlName) || !nestedObject[nestedMobileFormControlName]) {
              delete nestedObject[nestedKey];
            }
          }
        }
        else if ((typeof nestedObject[nestedKey] == 'string' || nestedObject[nestedKey] === null) && !nestedObject[nestedKey]) {
          delete nestedObject[nestedKey];
        }
        else if (Array.isArray(nestedObject[nestedKey]) && nestedObject[nestedKey].length == 0) {
          delete nestedObject[nestedKey];
        }
      }
    }
    else if (typeof object[key] == 'object' && !(object[key] instanceof Date) && Object.keys(object[key]).length == 0) {
      delete object[key];
    }
    else if (Array.isArray(object[key]) && object[key].length == 0) {
      delete object[key];
    }
    else if (object[key] == undefined) {
      delete object[key];
    }
    if (typeof object[key] == 'object' && !(object[key] instanceof Date) && Object.keys(object[key]).length == 0) {
      delete object[key];
    }
  }
  return object;
}

const mobileNumberSpaceMaskingReplaceFromFormValues = (object: Object, formControlNames: Array<string>): Object => {
  formControlNames.forEach((formControlName: string) => {
    if (object.hasOwnProperty(formControlName)) {
      object[formControlName] = object[formControlName].toString().replace(/\s/g, "");
    }
  })
  return object;
}

const destructureAfrigisObjectAddressComponents = (addressComponents: any[]): IAfrigisAddressComponents => {
  let addressObj: IAfrigisAddressComponents = {
    suburbName: '', cityName: '', provinceName: '', postalCode: '', streetName: '',
    streetNo: '', buildingName: '', buildingNo: '', estateName: '', estateStreetName: '', estateStreetNo: ''
  };
  addressComponents.forEach((address) => {
    const longName = address['long_name'];
    const shortName = address['short_name'];
    const key = address['administrative_type'];
    if (key == 'suburb' || key == 'sublocality_level_2') {
      addressObj.suburbName = longName;
    }
    else if (key == 'town') {
      addressObj.cityName = longName;
    }
    else if (key == 'province') {
      addressObj.provinceName = longName;
    }
    else if (key == 'post_street' || key == 'post_box') {
      addressObj.postalCode = longName;
    }
    else if (key == 'street_number' || key == 'site_number') {
      addressObj.streetNo = shortName;
    }
    else if (key == 'route' || key == 'point_of_interest' || key == 'intersection') {
      if (isUndefined(addressObj.streetName)) {
        addressObj.streetName = longName;
      } else {
        addressObj.streetName = addressObj.streetName ? addressObj.streetName + ', ' + longName : longName;
      }
    }
    else if (key == 'unit') {
      addressObj.buildingNo = longName;
    }
    else if (key == 'building') {
      addressObj.buildingName = longName;
    }
  });
  return addressObj;
}

const getRandomLightColor = (): string => {
  return '#' + (Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');
}

const convertStringsToQueryParams = (notificationUrl: string) => {
  let splitStringUptoParam = notificationUrl.substr(notificationUrl.indexOf("?") + 1);
  let param1 = splitStringUptoParam.split("&");
  let queryParams = {};
  param1.forEach((p) => {
    let keySubstring = p.substr(0, p.indexOf("="));
    let valueSubstring = p.slice(keySubstring.length + 1);
    queryParams[keySubstring] = valueSubstring;
  });
  return queryParams;
}

const swapLongitudeToLatitute = (boundaries) => {
  boundaries.forEach((boundary) => {
    let parsedCoordinates = JSON.parse(boundary.geometry.coordinates);
    parsedCoordinates[0].forEach((latLong, index) => {
      let latLongDeepCopy = JSON.parse(JSON.stringify(latLong));
      if (Math.sign(latLongDeepCopy[0]) == -1) {
        latLong[0] = latLongDeepCopy[1];
        latLong[1] = latLongDeepCopy[0];
      }
    });
    boundary.geometry.coordinates = JSON.stringify(parsedCoordinates);
    boundary.geometry.type = 'MULTIPOLYGON';
  });
  return boundaries;
}

const convertData = (date) => {// Fri Feb 20 2015 19:29:31 GMT+0530 (India Standard Time)
  var isoDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString();
  //OUTPUT : 2015-02-20T19:29:31.238Z
  return isoDate
}

const encryptData = (appData: object | string | number, key: string) => {
  let formatedAppData = JSON.stringify(appData);
  let encJson = CryptoJS.AES.encrypt(formatedAppData, cryptoSecretKey, cryptoOptions).toString()
  let encData = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(encJson));
  localStorage.setItem(key, encData);
}

const decryptData = (key: string) => {
  let decData = CryptoJS.enc.Base64.parse(localStorage.getItem(key)).toString(CryptoJS.enc.Utf8)
  let bytes = CryptoJS.AES.decrypt(decData, cryptoSecretKey, cryptoOptions).toString(CryptoJS.enc.Utf8)
  return JSON.parse(bytes)
}

const retrieveUniqueObjectsWithProperty = (list: Array<object>, propName: string): Array<object> => {
  return [...new Set(list.map(obj => obj[propName]))].map(boundingBoxRectangle => {
    return list.find(obj => obj[propName] === boundingBoxRectangle)
  });
}

const generateCurrentYearToNext99Years = (maxYear?: number, minYear?: number) => {
  minYear = minYear ? minYear : new Date().getFullYear();
  maxYear = maxYear ? minYear + maxYear : minYear + 99;
  let years = [];
  for (var i = minYear; i <= maxYear; i++) {
    years.push(i)
  }
  return years;
}

const getFullFormatedAddress = (object) => {
  const streetName = object?.streetNo ? `${object?.streetNo}, ` : '';
  return `${streetName}${object?.streetName}, ${object?.suburbName}, ${object?.cityName}, ${object?.postalCode}, ${object?.provinceName}`
}

const getNthDate = (date, days) => {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

const getNthDateExcludeWeekends = (date = new Date(), days = 5, holiday = []) => {
  var endDate: any = "", count = 0;
  while (count < days) {
    endDate = new Date(date.setDate(date.getDate() + 1));
    if (endDate.getDay() != 0 && endDate.getDay() != 6 && checkHoliday(holiday, date)) {
      //Date.getDay() gives weekday starting from 0(Sunday) to 6(Saturday)
      count++;
    }
  }
  return endDate;
}

const checkHoliday = (list, date) => {
  list?.forEach(el => {
    if (el?.toLocaleDateString() == date?.toLocaleDateString()) {
      return false;
    }
  });
  return true;
}

const getNthMonths = (date, months) => {
  var d = date.getDate();
  date.setMonth(date.getMonth() + +months);
  if (date.getDate() != d) {
    date.setDate(0);
  }
  return date;
}

const getNthMinutes =(date, minutes) => {
  return new Date(date.getTime() + minutes*60000);
}

const generateDays = (startDayVar?: number, endDayVar?: number): Array<number> => {
  let startDay = startDayVar ? startDayVar : 0;
  let endDay = endDayVar ? endDayVar : 28;
  let totalDays = [];
  for (let i = startDay; i <= endDay; i++) {
    totalDays.push(i);
  }
  return totalDays;
}

const getPDropdownData = (array, labelName = 'displayName', valueName = 'id') => {
  let returnArray = []
  array?.forEach(el => {
    returnArray.push({
      label: el[labelName],
      value: el[valueName],
    })
  });
  return returnArray;
}

const getMatMultiData = (array, labelName = 'displayName', valueName = 'id') => {
  let returnArray = []
  array?.forEach(el => {
    returnArray.push({
      display: el[labelName],
      value: el[valueName]?.toString() ? el[valueName]?.toString() : el[valueName],
    })
  });
  return returnArray;
}

const onFormControlChangesToFindDuplicate = (formControlName: string | string[], formArray: FormArray | FormArray[], formArrayName: string | string[],
  currentCursorIndex: number, parentFormGroup: FormGroup) => {
  // these logics are to defaultly add currentCursorIndex formcontrol name to handle focus event for duplication
  let formGroupControls = [];
  if (formArray instanceof Array) {
    formArray.forEach((formArrayControl) => {
      formGroupControls = formGroupControls.concat(formArrayControl.controls);
    });
  }
  else {
    formGroupControls = formArray.controls;
  }
  formGroupControls.forEach((formGroupControlsObj, index) => {
    if (formGroupControls[currentCursorIndex].get('currentCursorIndex')) {
      formGroupControls[currentCursorIndex].get('currentCursorIndex').setValue(currentCursorIndex);
    }
    else {
      formGroupControls[index].addControl('currentCursorIndex', new FormControl(index));
    }
  });
  if (formGroupControls[0].status == "INVALID") {
    formGroupControls.forEach((formGroupControlsObj, index) => {
      if (index == 0) return;
      if (!formGroupControls[index].get('currentCursorIndex')) {
        formGroupControls[index].addControl('currentCursorIndex', new FormControl(index));
      }
      else {
        formGroupControls[index].get('currentCursorIndex').setValue(index);
      }
    });
  }
  else if (formGroupControls[0].status == "VALID") {
    formGroupControls.forEach((formGroupControlsObj, index) => {
      if (!formGroupControls[index].get('currentCursorIndex')) {
        formGroupControls[index].addControl('currentCursorIndex', new FormControl(index));
      }
      else {
        formGroupControls[index].get('currentCursorIndex').setValue(index);
      }
    });
  }
  // end of addition of currentCursorIndex to handle focus event for duplication
  setTimeout(() => {
    findDuplicate(formArray, formControlName, currentCursorIndex);
  });
}

const findDuplicate = (formArray: FormArray | FormArray[],
  duplicateFormControlNames: string | string[], currentCursorIndex: number) => {
  let formArrayVar: any = new FormArray([]);
  if (formArray instanceof Array) {
    formArray.forEach((formArrayObj) => {
      formArrayObj.controls.forEach((control) => {
        formArrayVar.push(control);
      })
    });
  }
  else {
    formArrayVar = formArray;
  }
  const keyNameAndValueCount = formArrayVar.value.reduce((prevObj, nextObj) => {
    if (prevObj[getMatchedFormControl(duplicateFormControlNames, nextObj)]) {
      prevObj[getMatchedFormControl(duplicateFormControlNames, nextObj)] = prevObj[getMatchedFormControl(duplicateFormControlNames, nextObj)] + 1;
    }
    else {
      prevObj[getMatchedFormControl(duplicateFormControlNames, nextObj)] = 1;
    }
    return prevObj;
  }, {});
  const itemArray = formArrayVar.controls;
  const duplicatedDataOne = itemArray.
    filter((duplicatedFormGroup: FormGroup) => {
      let filteredResult = keyNameAndValueCount[duplicatedFormGroup.controls[getMatchedFormControl(duplicateFormControlNames, duplicatedFormGroup.controls, 'key')].value] !== 1 &&
        keyNameAndValueCount[duplicatedFormGroup.controls[getMatchedFormControl(duplicateFormControlNames, duplicatedFormGroup.controls, 'key')].value] !== "";
      if (filteredResult) {
        return duplicatedFormGroup;
      }
    });
  if (currentCursorIndex <= duplicatedDataOne.length) {
    if (duplicatedDataOne.length > 1) {
      duplicatedDataOne.forEach((formGroupObj) => {
        Object.keys(formGroupObj.controls).forEach((formControlName) => {
          if (formControlName === getMatchedFormControl(duplicateFormControlNames, formGroupObj.controls, 'key') &&
            formGroupObj.controls[getMatchedFormControl(duplicateFormControlNames, formGroupObj.controls, 'key')].value &&
            formGroupObj.controls['currentCursorIndex'].value == currentCursorIndex) {
            formGroupObj.get(getMatchedFormControl(duplicateFormControlNames, formGroupObj.controls, 'key')).setErrors({ duplicate: true });
          }
        });
      });
    }
  }
  else {
    duplicatedDataOne.forEach((formGroupObj, ix) => {
      Object.keys(formGroupObj.controls).forEach(() => {
        if (ix === duplicatedDataOne.length - 1) {
          formGroupObj.get(getMatchedFormControl(duplicateFormControlNames, formGroupObj.controls, 'key')).setErrors({ duplicate: true });
        }
      });
    });
  }
}

const getMatchedFormControl = (duplicateFormControlNames: string | string[], specifiedObj, returnType = 'value'): string => {
  if (duplicateFormControlNames instanceof Array) {
    for (let i = 0; i < duplicateFormControlNames.length; i++) {
      if (specifiedObj.hasOwnProperty(duplicateFormControlNames[i])) {
        return returnType == 'value' ? specifiedObj[duplicateFormControlNames[i]] : duplicateFormControlNames[i];
      }
    }
  }
  else {
    return returnType == 'value' ? specifiedObj[duplicateFormControlNames] : duplicateFormControlNames;
  }
}

const onBlurActionOnDuplicatedFormControl = (duplicateFormControlNames: string | string[], formArray: FormArray | FormArray[], event, index: number) => {
  let formGroupControls = [];
  if (formArray instanceof Array) {
    formArray.forEach((formArrayControl) => {
      formGroupControls = formGroupControls.concat(formArrayControl.controls);
    });
  }
  else {
    formGroupControls = formArray.controls;
  }
  if (formGroupControls.length == 1) {
    (formGroupControls[0] as FormGroup) = removeFormControlError(formGroupControls[0] as FormGroup, 'duplicate');
    formGroupControls[0].get(getMatchedFormControl(duplicateFormControlNames, formGroupControls[0].controls, 'key')).updateValueAndValidity();
  }
  else {
    if (formGroupControls[index].get(getMatchedFormControl(duplicateFormControlNames, formGroupControls[index].controls, 'key')).errors) {
      if (formGroupControls[index].get(getMatchedFormControl(duplicateFormControlNames, formGroupControls[index].controls, 'key')).errors.hasOwnProperty('duplicate')) {
        if (formArray instanceof Array) {
          formArray.forEach((formArrayControl) => {
            if (formArrayControl.status == 'VALID') {
              formArrayControl.updateValueAndValidity();
            }
          });
        }
        let disableEvents = disableAllUserEvents();
        event.target.focus();
        disableEvents();
        return;
      }
    }
  }
  if (formGroupControls[index]?.get(getMatchedFormControl(duplicateFormControlNames, formGroupControls[index].controls, 'key'))?.errors == null ||
    !formGroupControls[index]?.get(getMatchedFormControl(duplicateFormControlNames, formGroupControls[index].controls, 'key'))?.errors?.hasOwnProperty('duplicate')) {
    if (formArray instanceof Array) {
      formArray.forEach((formArrayControl) => {
        if (formArrayControl.status == 'INVALID') {
          formArrayControl.updateValueAndValidity();
        }
      });
    }
  }
}

const fileUrlDownload = (href, download, target = false) => {
  var link = document.createElement("a");
  if (link.download !== undefined) {
    link.setAttribute("href", href);
    link.setAttribute("download", download);
    if (target) {
      link.setAttribute('target', '_blank');
    }
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}

const findInvalidControls = (controls) => {
  const invalid = [];
  for (const name in controls) {
    if (controls[name].invalid) {
      invalid.push(name);
    }
  }
  return invalid?.length;
}

const disableAllUserEvents = () => {
  const events = [
    "click",
    "contextmenu",
    "dblclick",
    "mousedown",
    "mouseenter",
    "mouseleave",
    "mousemove",
    "mouseover",
    "mouseout",
    "mouseup",
    "blur",
    "change",
    "focus",
    "focusin",
    "focusout",
    "input",
    "invalid",
    "reset",
    "search",
    "select",
    "submit",
    "drag",
    "dragend",
    "dragenter",
    "dragleave",
    "dragover",
    "dragstart",
    "drop",
    "copy",
    "cut",
    "paste",
    "mousewheel",
    "wheel",
    "touchcancel",
    "touchend",
    "touchmove",
    "touchstart"
  ];
  const handler = event => {
    event.stopPropagation();
    event.preventDefault();
    return false;
  };
  for (let i = 0, l = events.length; i < l; i++) {
    document.addEventListener(events[i], handler, true);
  }
  return () => {
    for (let i = 0, l = events.length; i < l; i++) {
      document.removeEventListener(events[i], handler, true);
    }
  };
};

const groupByArrayByKey = (list, groupByKey) => {
  return list.reduce((prevObj, currentObj) => {
    let key = currentObj[groupByKey];
    if (!prevObj[key]) {
      prevObj[key] = [];
    }
    prevObj[key].push(currentObj);
    return prevObj;
  }, {});
}

const addNullOrStringDefaultFormControlValue = (formGroup: FormGroup, formControlNames: Array<AddNewFormControlObj>): FormGroup => {
  formControlNames.forEach((formControlName: AddNewFormControlObj) => {
    if (formControlName.controlName) {
      formGroup.get(formControlName.controlName).setValue(formControlName.defaultValue ? formControlName.defaultValue : '', { disabled: formControlName.isDisabled ? formControlName.isDisabled : false });
      formGroup.get(formControlName.controlName).updateValueAndValidity({ emitEvent: false, onlySelf: true });
    }
    formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  });
  return formGroup;
};

interface FormControlWithBooleanValue {
  controlName: string;
  defaultValue?: boolean;
  isDisabled?: boolean;
}

const addTrueOrFalseDefaultFormControlValue = (formGroup: FormGroup, formControlNames: Array<FormControlWithBooleanValue>): FormGroup => {
  formControlNames.forEach((formControlName: FormControlWithBooleanValue) => {
    if (formControlName.controlName) {
      formGroup.get(formControlName.controlName).setValue(formControlName.defaultValue ? formControlName.defaultValue : false);
      if (formControlName.isDisabled && formControlName.isDisabled == true) {
        formGroup.get(formControlName.controlName).disable({ emitEvent: false, onlySelf: true });
      }
      formGroup.get(formControlName.controlName).updateValueAndValidity({ emitEvent: false, onlySelf: true });
    }
    formGroup.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  });
  return formGroup;
};

const exportToExcel = (selectedRows, data, excelFileName: string) => {
  import("xlsx").then(xlsx => {
    const worksheet = xlsx.utils.json_to_sheet((selectedRows.length > 0) ? selectedRows : data);
    const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
    const blobPropertyBag: BlobPropertyBag = { type: "'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'" };
    import("file-saver").then(FileSaver => {
      const blob: Blob = new Blob([excelBuffer], blobPropertyBag);
      FileSaver.saveAs(blob, excelFileName + '_export_' + new Date().getDate()
        + "_" + new Date().toLocaleString('default', { month: 'short' }) + "_" + new Date().getFullYear() + '.xlsx');
    });
  });
}
const exportList = (otherParams?: any, pageIndex?: any, pageSize?: any, moduleName?: any, apiName?: any, crudService?: any, rxjsService?: any, label?: any) => {
  otherParams = { ...otherParams, ...{ pageIndex: pageIndex, pageSize: pageSize } }
  let queryParams = generateQueryParams(otherParams);
  crudService.downloadFile(moduleName,
    apiName, null, null, queryParams).subscribe((response: any) => {
      rxjsService.setGlobalLoaderProperty(false);
      if (response) {
        saveAsExcelFile(response, label);
        rxjsService.setGlobalLoaderProperty(false);
      }
    });
}
const saveAsExcelFile = (buffer: any, fileName: string) => {
  import("file-saver").then(FileSaver => {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  });
}

const urlPatternForStraitaDomain = (control: AbstractControl): object | null => {
  const urlString: string = control.value;
  const regExp = new RegExp(/^(([a-zA-Z0-9\-]*[a-zA-Z0-9])\.){1,}([s][t][r][i][a][t][a].[c][o][m]{1})$/gm);
  if (urlString == '') {
    return null;
  }
  else {
    return (regExp.test(urlString)) ? null : { invalid: true };
  }
}

function removeProperyFromAnObjectWithDeepCopy<T, K extends keyof T>(object: T, removableKeys: K[]) {
  let remainingObjectPropertiesCopy;
  removableKeys.forEach((removableKey, index) => {
    const { [removableKey]: value, ...remainingObjectProperties } = index == 0 ? object : remainingObjectPropertiesCopy;
    remainingObjectPropertiesCopy = remainingObjectProperties;
    if (index == removableKeys.length - 1) {
      remainingObjectPropertiesCopy = remainingObjectProperties;
    }
  });
  return remainingObjectPropertiesCopy;
}

const prepareCoordinatesForLeafLetMapComponent = (actualBoundaries: any[], list?, type = 'polygon') => {
  if (type == 'polygon') {
    let data = [];
    let boundaries = list ? list : actualBoundaries;
    boundaries.forEach((boundary, index) => {
      if (!boundary.geometry) {
        console.log("object that doesnot has the geometry : " + boundary + "," + index);
      }
      else {
        let str = boundary.geometry.coordinates;
        str = str.replace(/^"(.*)"$/, '$1');
        str = str.replace(/[()]/g, '')
        let coordinates = JSON.parse(str);
        if (coordinates.length > 1) {
          coordinates.forEach((innerCoordinates) => {
            innerCoordinates.forEach((latLong, index) => {
              latLong.forEach((nestedLeatLngArr) => {
                let nestedLeatLngArrCopy = JSON.parse(JSON.stringify(nestedLeatLngArr));
                if (Math.sign(nestedLeatLngArr[0]) == 1) {
                  nestedLeatLngArr[0] = nestedLeatLngArrCopy[1];
                  nestedLeatLngArr[1] = nestedLeatLngArrCopy[0];
                }
              });
            });
          })
        }
        else {
          coordinates[0].forEach((latLong, index) => {
            let latLongArr = JSON.parse(JSON.stringify(latLong));
            if (Math.sign(latLongArr[0]) == 1) {
              latLong[0] = latLongArr[1];
              latLong[1] = latLongArr[0];
            }
          });
        }
        let obj;
        if (boundary?.modificationType) {
          obj = {
            coordinates, properties: {
              Id: boundary.boundaryId, Name: boundary.boundaryName, BatchId: boundary.batchId,
              boundaryRequestId: boundary.boundaryRequestId, modificationType: boundary?.modificationType
            }
          }
        }
        else {
          obj = {
            coordinates, properties: {
              Id: boundary.boundaryId, Name: boundary.boundaryName, BatchId: boundary.batchId,
              boundaryRequestId: boundary.boundaryRequestId,
            }
          }
        };
        data.push(obj);
      }
    });
    return data;
  }
  // prepare bounding box to show to leaf let component
  else {
    let data = [];
    let parsedArray = JSON.parse(list);
    parsedArray.forEach((boundingBox, i) => {
      let boundingBoxArray = parsedArray.length == 1 ? boundingBox : boundingBox[0];
      boundingBoxArray.forEach((nestedLeatLngArr) => {
        let nestedLeatLngArrCopy = JSON.parse(JSON.stringify(nestedLeatLngArr));
        if (Math.sign(nestedLeatLngArr[0]) == 1) {
          nestedLeatLngArr[0] = nestedLeatLngArrCopy[1];
          nestedLeatLngArr[1] = nestedLeatLngArrCopy[0];
        }
      });
      let obj = {
        coordinates: boundingBox,
        properties: {
          type: 'Bounding Box',
          Name: `Bounding Box ${i + 1}`
        }
      };
      data.push(obj);
    });
    return data;
  }
}

function convertTreeToList(tree) {
  return tree.reduce(function (acc, o) {       // check the docs for reducs in the link bellow
    if (o.menuName && o?.subMenu?.length) {                             // if this object has an menuName
      acc.push({ [o.menuName]: o.subMenu });                   // add the menuName to the result array
    }
    if (o?.subMenu?.length) {                            // if this object has children
      acc = acc.concat(convertTreeToList(o.subMenu)); // get their href and add (concat) them to the result
    }
    return acc;
  }, []);
}

const prepareDynamicTableTabsFromPermissions = (primengTableConfigProperties, dynamicTableTabsFromPermissions: any[]): object => {
  let selectedTabIndex;
  if (dynamicTableTabsFromPermissions?.find(dTFP => dTFP['menuCategoryId'] == MenuCategoriesForPermissions.FEATURE)) {
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEmail = filterCRUDPermissions(PermissionTypes.EMAIL, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canDownload = filterCRUDPermissions(PermissionTypes.DOWNLOAD, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canLogout = filterCRUDPermissions(PermissionTypes.LOGOUT, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canPrint = filterCRUDPermissions(PermissionTypes.PRINT, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSchedule = filterCRUDPermissions(PermissionTypes.SCHEDULE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canUpload = filterCRUDPermissions(PermissionTypes.UPLOAD, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate = filterCRUDPermissions(PermissionTypes.ADD, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit = filterCRUDPermissions(PermissionTypes.EDIT, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete = filterCRUDPermissions(PermissionTypes.DELETE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canFilter = filterCRUDPermissions(PermissionTypes.FILTER, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canChat = filterCRUDPermissions(PermissionTypes.CHAT, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSearch = filterCRUDPermissions(PermissionTypes.SEARCH, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport = filterCRUDPermissions(PermissionTypes.EXPORT, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canService = filterCRUDPermissions(PermissionTypes.SERVICE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCall = filterCRUDPermissions(PermissionTypes.CALL, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canNoContacts = filterCRUDPermissions(PermissionTypes.NO_CONTACTS, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCancel = filterCRUDPermissions(PermissionTypes.CANCEL, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canLoggedIn = filterCRUDPermissions(PermissionTypes.LOGGEDIN, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canPasswordViews = filterCRUDPermissions(PermissionTypes.PASSWORD_VIEWS, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canQuickAction = filterCRUDPermissions(PermissionTypes.QUICK_ACTION, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRefresh = filterCRUDPermissions(PermissionTypes.REFRESH, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canComplete = filterCRUDPermissions(PermissionTypes.COMPLETE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canGenerate = filterCRUDPermissions(PermissionTypes.GENERATE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canMultiDeleteActionBtn = primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete;
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canVerify = filterCRUDPermissions(PermissionTypes.VERIFY, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canViewAction = filterCRUDPermissions(PermissionTypes.VIEW, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canAssign = filterCRUDPermissions(PermissionTypes.ASSIGN, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canScan = filterCRUDPermissions(PermissionTypes.SCAN, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSuspend = filterCRUDPermissions(PermissionTypes.SUSPEND, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canTermination = filterCRUDPermissions(PermissionTypes.TERMINATION, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canAccessCard = filterCRUDPermissions(PermissionTypes.ACCESS_CREDIT_CARD, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRequestDiscount = filterCRUDPermissions(PermissionTypes.REQUEST_FOR_DISCOUNT, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canReinstate = filterCRUDPermissions(PermissionTypes.REINSTATE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canStartCampaign = filterCRUDPermissions(PermissionTypes.START_CAMPAIGN, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canPauseCampaign = filterCRUDPermissions(PermissionTypes.PAUSE_CAMPAIGN, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCancelCampaign = filterCRUDPermissions(PermissionTypes.CANCEL_CAMPAIGN, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canApprove = filterCRUDPermissions(PermissionTypes.APPROVE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].CanNotification = filterCRUDPermissions(PermissionTypes.NOTIFICATION, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSpecialEdit = filterCRUDPermissions(PermissionTypes.SPECIAL_EDIT, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSimpleEdit = filterCRUDPermissions(PermissionTypes.SIMPLE_EDIT, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRun = filterCRUDPermissions(PermissionTypes.RUN, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canPost = filterCRUDPermissions(PermissionTypes.POST, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSMS = filterCRUDPermissions(PermissionTypes.SMS, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canResolveEscalation = filterCRUDPermissions(PermissionTypes.RESOLVE_ESCALATION, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canDecline = filterCRUDPermissions(PermissionTypes.DECLINE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canApprove = filterCRUDPermissions(PermissionTypes.APPROVE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canReset = filterCRUDPermissions(PermissionTypes.RESET, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canUpgrade = filterCRUDPermissions(PermissionTypes.UPGRADE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canDowngrade = filterCRUDPermissions(PermissionTypes.DOWNGRADE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRearrange = filterCRUDPermissions(PermissionTypes.REARRAGE, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExpand = filterCRUDPermissions(PermissionTypes.EXPAND, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canTransfer = filterCRUDPermissions(PermissionTypes.INTERTECHTRANSFER, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEditCourierName = filterCRUDPermissions(PermissionTypes.COURIER_NAME, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSapNumberShow = filterCRUDPermissions(PermissionTypes.SAP_VENDOR_NUMBER, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canAccountNumber = filterCRUDPermissions(PermissionTypes.ACCOUNT_NUMBER, dynamicTableTabsFromPermissions);
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canView = filterCRUDPermissions(PermissionTypes.LIST, dynamicTableTabsFromPermissions);

    primengTableConfigProperties.tableComponentConfigs.tabsList[0].checkBox = (primengTableConfigProperties.tableComponentConfigs.tabsList[0].checkBox !== undefined) ?
      primengTableConfigProperties.tableComponentConfigs.tabsList[0].checkBox : primengTableConfigProperties.tableComponentConfigs.tabsList[0].canMultiDeleteActionBtn;

    selectedTabIndex = 0;
    // extra rare menu features
    primengTableConfigProperties.tableComponentConfigs.tabsList[0].canChangeOwner = filterCRUDPermissions(PermissionTypes.CHANGE_OWNER, dynamicTableTabsFromPermissions);
  }
  else {
    dynamicTableTabsFromPermissions?.forEach((dynamicTableTabFromPermissions: object, ix) => {
      if (!primengTableConfigProperties.tableComponentConfigs.tabsList[ix]) return;
      let index = dynamicTableTabFromPermissions['sortOrder'] - 1
      if (primengTableConfigProperties.tableComponentConfigs.tabsList.length <= index) return
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].disabled = false;
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].disabled = false;
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canView = filterCRUDPermissions(PermissionTypes.LIST, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canViewAction = filterCRUDPermissions(PermissionTypes.VIEW, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canAssign = filterCRUDPermissions(PermissionTypes.ASSIGN, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canSchedule = filterCRUDPermissions(PermissionTypes.SCHEDULE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canPrint = filterCRUDPermissions(PermissionTypes.PRINT, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canLogout = filterCRUDPermissions(PermissionTypes.LOGOUT, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canEdit = filterCRUDPermissions(PermissionTypes.EDIT, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canRowDelete = filterCRUDPermissions(PermissionTypes.DELETE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canExport = filterCRUDPermissions(PermissionTypes.EXPORT, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canMultiDeleteActionBtn = primengTableConfigProperties.tableComponentConfigs.tabsList[index].canRowDelete;
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canCreate = filterCRUDPermissions(PermissionTypes.ADD, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canFilter = filterCRUDPermissions(PermissionTypes.FILTER, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canChat = filterCRUDPermissions(PermissionTypes.CHAT, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canSearch = filterCRUDPermissions(PermissionTypes.SEARCH, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canService = filterCRUDPermissions(PermissionTypes.SERVICE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canCall = filterCRUDPermissions(PermissionTypes.CALL, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canNoContacts = filterCRUDPermissions(PermissionTypes.NO_CONTACTS, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canLoggedIn = filterCRUDPermissions(PermissionTypes.LOGGEDIN, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canDownload = filterCRUDPermissions(PermissionTypes.DOWNLOAD, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canPasswordViews = filterCRUDPermissions(PermissionTypes.PASSWORD_VIEWS, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canQuickAction = filterCRUDPermissions(PermissionTypes.QUICK_ACTION, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canRefresh = filterCRUDPermissions(PermissionTypes.REFRESH, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canCancel = filterCRUDPermissions(PermissionTypes.CANCEL, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canComplete = filterCRUDPermissions(PermissionTypes.COMPLETE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canGenerate = filterCRUDPermissions(PermissionTypes.GENERATE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canVerify = filterCRUDPermissions(PermissionTypes.VERIFY, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canScan = filterCRUDPermissions(PermissionTypes.SCAN, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canSuspend = filterCRUDPermissions(PermissionTypes.SUSPEND, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canTermination = filterCRUDPermissions(PermissionTypes.TERMINATION, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canReinstate = filterCRUDPermissions(PermissionTypes.REINSTATE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canEmail = filterCRUDPermissions(PermissionTypes.EMAIL, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canStartCampaign = filterCRUDPermissions(PermissionTypes.START_CAMPAIGN, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canPauseCampaign = filterCRUDPermissions(PermissionTypes.PAUSE_CAMPAIGN, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canCancelCampaign = filterCRUDPermissions(PermissionTypes.CANCEL_CAMPAIGN, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].CanNotification = filterCRUDPermissions(PermissionTypes.NOTIFICATION, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canRun = filterCRUDPermissions(PermissionTypes.RUN, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canPost = filterCRUDPermissions(PermissionTypes.POST, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canSMS = filterCRUDPermissions(PermissionTypes.SMS, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canSpecialEdit = filterCRUDPermissions(PermissionTypes.SPECIAL_EDIT, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canSimpleEdit = filterCRUDPermissions(PermissionTypes.SIMPLE_EDIT, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canResolveEscalation = filterCRUDPermissions(PermissionTypes.RESOLVE_ESCALATION, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canDecline = filterCRUDPermissions(PermissionTypes.DECLINE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canApprove = filterCRUDPermissions(PermissionTypes.APPROVE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canAccessCard = filterCRUDPermissions(PermissionTypes.ACCESS_CREDIT_CARD, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canRequestDiscount = filterCRUDPermissions(PermissionTypes.REQUEST_FOR_DISCOUNT, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canReset = filterCRUDPermissions(PermissionTypes.RESET, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canUpgrade = filterCRUDPermissions(PermissionTypes.UPGRADE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canDowngrade = filterCRUDPermissions(PermissionTypes.DOWNGRADE, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canUpload = filterCRUDPermissions(PermissionTypes.UPLOAD, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canTransfer = filterCRUDPermissions(PermissionTypes.INTERTECHTRANSFER, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canSapNumberShow = filterCRUDPermissions(PermissionTypes.SAP_VENDOR_NUMBER, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canAccountNumber = filterCRUDPermissions(PermissionTypes.ACCOUNT_NUMBER, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].canAccountNumber = filterCRUDPermissions(PermissionTypes.ACCOUNT_NUMBER, dynamicTableTabFromPermissions['subMenu']);
      primengTableConfigProperties.tableComponentConfigs.tabsList[index].checkBox = (primengTableConfigProperties.tableComponentConfigs.tabsList[index].checkBox !== undefined) ?
        primengTableConfigProperties.tableComponentConfigs.tabsList[index].checkBox : primengTableConfigProperties.tableComponentConfigs.tabsList[index].canMultiDeleteActionBtn;

      if (primengTableConfigProperties.tableComponentConfigs.tabsList[index].canView == false) {
        primengTableConfigProperties.tableComponentConfigs.tabsList[index].disabled = true;
      }
      else if (primengTableConfigProperties.tableComponentConfigs.tabsList[index].canView == true) {
        // primengTableConfigProperties.tableComponentConfigs.tabsList[index].disabled = false;
        if (selectedTabIndex == undefined) {
          selectedTabIndex = index;
        }
      }
    });
  }

  return { primengTableConfigProperties, selectedTabIndex };
};

const filterCRUDPermissions = (crudType: PermissionTypes, dynamicSubMebuTableTabFromPermissions: Array<object>): boolean => {
  return dynamicSubMebuTableTabFromPermissions.find(subMenu => subMenu['menuName'] == crudType) ? true : false;
}


function findTheDifferenceBetweenTwoArrays<T1, T2, K1 extends keyof T1, K2 extends keyof T2>(array1: T1[], array2: T2[], uniqueProperties: K1[] | K2[]) {
  return array1.filter((arr1) => !array2.some((arr2) => {
    let uniquePropertiesSameValueCount = 0;
    let isConditionSatisfied = false;
    uniqueProperties.forEach((uniqueProperty, index: number) => {
      if (arr2[uniqueProperty] == arr1[uniqueProperty]) {
        uniquePropertiesSameValueCount += 1;
      }
      if (uniqueProperties.length == index + 1) {
        isConditionSatisfied = (uniquePropertiesSameValueCount !== uniqueProperties.length) ? false : true;
      }
    });
    return isConditionSatisfied;
  }));
}

const findTheDifferenceBetweenTwoObjects = (obj1, obj2) => {
  let result = {};
  let obj1Keys = Object.keys(obj1);
  let obj2Keys = Object.keys(obj2);
  if (obj1Keys.length !== obj2Keys.length) {
    result['difference'] = true;
  }
  else {
    for (let key in obj2) {
      if (obj1Keys.includes(key)) {
        if (obj2[key] !== obj1[key]) {
          result[key] = obj2[key];
        }
      }
      else {
        result[key] = obj2[key];
      }
    }
  }
  return result;
}

const findTablePermission = (primengTableConfigProperties: Object | any, perObj: any[]): Object => {
  let selectedTabIndex = 0;
  if (primengTableConfigProperties?.tableComponentConfigs?.tabsList?.length > 1) {
    const permission = perObj?.filter(el => el?.menuCategoryId == MenuCategoriesForPermissions?.TAB);
    primengTableConfigProperties?.tableComponentConfigs?.tabsList?.forEach((obj: any, ix: number) => {
      const tabObj = permission?.find(el => el?.menuName == obj?.caption);
      // console.log(tabObj);
      tabObj?.subMenu?.filter(el => {
        const menuName = 'can' + el?.menuName?.replace(/\s/g, "");
        obj[menuName] = el?.isSelected;
        if (el?.menuName?.toLowerCase() == 'list') {
          obj['disabled'] = obj['canList'] ? false : true;
        }
      })
      return obj;
    })
    if (primengTableConfigProperties?.tableComponentConfigs?.tabsList[primengTableConfigProperties?.selectedTabIndex]?.canList) {
      selectedTabIndex = primengTableConfigProperties?.selectedTabIndex;
    }
  } else if (primengTableConfigProperties?.tableComponentConfigs?.tabsList?.length == 1) {
    // const permission = perObj?.filter(el => el?.menuCategoryId == MenuCategoriesForPermissions?.FEATURE);
    primengTableConfigProperties?.tableComponentConfigs?.tabsList?.forEach((obj: any, ix: number) => {
      const tabObj = perObj?.find(el => el?.menuName == obj?.caption);
      // console.log(tabObj);
      tabObj?.subMenu?.filter(el => {
        const menuName = 'can' + el?.menuName?.replace(/\s/g, "");
        obj[menuName] = el?.isSelected;
        if (el?.menuName?.toLowerCase() == 'list') {
          obj['disabled'] = obj['canList'] ? false : true;
        }
      })
      return obj;
    })
  }
  return { primengTableConfigProperties, selectedTabIndex };
}

const convertISOStringtoDateAndTodayDate = (response: IApplicationResponse, dateProps: string[]): IApplicationResponse => {
  Object.keys(response.resources).forEach((key) => {
    if (dateProps.includes(key)) {
      if (response.resources[key]) {
        if (new Date(response.resources[key]).getTime() < new Date().getTime()) {
          response.resources[key] = new Date();
        }
        else if (new Date(response.resources[key]).getTime() >= new Date().getTime()) {
          response.resources[key] = new Date(response.resources[key]);
        }
      }
      else {
        response.resources[key] = new Date();
      }
    }
  });
  return response;
}

const adjustDateFormatAsPerPCalendar = (): string => {
  switch (localStorage.getItem('globalDateFormat')) {
    case 'yyyy-MM-dd':
      return 'yy-mm-dd';
    case 'yyyy-dd-MM':
      return 'yy-dd-mm';
    case 'dd-MM-yyyy':
      return 'dd-mm-yy';
    case 'MM-dd-yyyy':
      return 'mm-dd-yy';
    default:
      return 'yy-mm-dd';
  }
}

const accountNoEncrption = (accArray, displayName = 'accountNo') => {
  accArray?.forEach(element => {
    if (element[displayName]) {
      element['displayName'] = element[displayName];
      let len = element[displayName]?.length - 4;
      let first = element[displayName]?.substring(0, len);
      let last = element[displayName]?.substring(len, element[displayName]?.length);
      let str = ''
      for (let i = 0; i < first?.length; i++) {
        str = str + '*';
      }
      element[displayName] = str + '' + last;
    }
  });
  return accArray;
}

const createOrUpdateFilteredFieldKeyValues = (tableFilteredFieldsCopy, searchedCriteriaCopy, tabsList, otherService: OtherService, store: Store<AppState>,
  shouldDispatchNgrxAction = true, areColumnFiltersEdited = false, isPaginationRequested = false, isSortingRequested = false) => {
  let currentUrlCopy = '', slicedUrl = '';
  let tableFilteredFields = { ...tableFilteredFieldsCopy };
  let searchedCriteria = { ...searchedCriteriaCopy };
  if (tabsList.length > 1 && !otherService.currentUrl?.includes('?tab')) {
    currentUrlCopy = otherService.currentUrl + '?tab=0';
  }
  if (Object.keys(tableFilteredFields ?
    tableFilteredFields : {}).length > 0) {
    for (let key in tableFilteredFields) {
      if (otherService.currentUrl == key) {
        tableFilteredFields[key] = searchedCriteria;
        // If column filters are edited then take that filter alone or else get it from ngrx store and concat along with the user currently edited fields and send to request
        if (currentUrlCopy) {
          tableFilteredFields[currentUrlCopy] = tableFilteredFields[key];
          tableFilteredFields[otherService.currentUrl] = tableFilteredFields[key];
        }
        if (otherService.currentUrl?.includes('?tab=0')) {
          slicedUrl = otherService.currentUrl.slice(0, otherService.currentUrl.length - 6);
          tableFilteredFields[slicedUrl] = tableFilteredFields[key];
        }
        break;
      }
      else {
        if (currentUrlCopy) {
          tableFilteredFields[currentUrlCopy] = searchedCriteriaCopy;
          tableFilteredFields[otherService.currentUrl] = searchedCriteriaCopy;
        }
        if (otherService.currentUrl?.includes('?tab=0')) {
          slicedUrl = otherService.currentUrl.slice(0, otherService.currentUrl.length - 6);
          tableFilteredFields[slicedUrl] = searchedCriteriaCopy;
        }
        else {
          tableFilteredFields[otherService.currentUrl] = searchedCriteriaCopy;
        }
        break;
      }
    }
  }
  else {
    // this logic hits after logout and comes back for the first time (only) field criteria such as search or filter
    if (currentUrlCopy) {
      tableFilteredFields[currentUrlCopy] = searchedCriteria;
    }
    tableFilteredFields[otherService.currentUrl] = searchedCriteria;
  }
  if (shouldDispatchNgrxAction) {
    let key = currentUrlCopy ? currentUrlCopy : slicedUrl ? slicedUrl : otherService.currentUrl;
    if (tableFilteredFields[key]?.searchColumns) {
      Object.keys(tableFilteredFields[key]?.searchColumns).forEach((searchColumnKey) => {
        tableFilteredFields[key][searchColumnKey] = tableFilteredFields[key]?.searchColumns[searchColumnKey];
      });
      delete tableFilteredFields[key].searchColumns;
    }
    //  store.dispatch(new SharedTableFilteredDataCreated({ tableFilteredFields }));
    let updateableObject, finalKey;
    if (slicedUrl) {
      updateableObject = tableFilteredFields[slicedUrl];
      finalKey = slicedUrl;
    }
    if (currentUrlCopy) {
      updateableObject = tableFilteredFields[currentUrlCopy];
      finalKey = currentUrlCopy;
    }
    if (!slicedUrl && !currentUrlCopy) {
      updateableObject = tableFilteredFields[otherService.currentUrl];
      finalKey = otherService.currentUrl;
    }
    // These two if conditions to check whether the ngrx dispatheable action objects are unique (the case id same object properties has same values ).
    if (Object.keys(tableFilteredFieldsCopy[finalKey] ? tableFilteredFieldsCopy[finalKey] : {}).length > 0) {
      let objDifference = findTheDifferenceBetweenTwoObjects(tableFilteredFieldsCopy[finalKey], tableFilteredFields[finalKey]);
      if (Object.keys(objDifference ? objDifference : {}).length > 0 || (!areColumnFiltersEdited && !isPaginationRequested && !isSortingRequested)) {
        store.dispatch(new SharedTableFilteredDataCreated({ tableFilteredFields }));
      }
    }
    else {
      store.dispatch(new SharedTableFilteredDataCreated({ tableFilteredFields }));
    }
  }
  let rowObject = {}, otherParamsObject = {};
  Object.keys(searchedCriteria).forEach((key) => {
    if (key == 'pageIndex' || key == 'pageSize') {
      rowObject[key] = searchedCriteria[key];
    }
    else {
      if (key == 'searchColumns') {
        Object.keys(searchedCriteria[key]).forEach((searchColumnsKey) => {
          if (searchColumnsKey !== 'searchColumns' && searchColumnsKey !== 'pageIndex' && searchColumnsKey !== 'pageSize') {
            otherParamsObject[searchColumnsKey] = searchedCriteria[key][searchColumnsKey];
          }
        });
      }
      else {
        otherParamsObject[key] = searchedCriteria[key];
      }
    }
  });
  return { rowObject, otherParamsObject };
}
interface IPatchTableFilteredFields {
  columnFilterForm,
  row
}

// Helps to persist the previously searched results and field values
const patchPersistedNgrxFieldValuesToForm = (columnFilterForm: FormGroup, tableFilteredFields, router: Router, otherService: OtherService, httpCancelService: HttpCancelService,
  areColumnFiltersEdited: boolean, isPaginationRequested: boolean, isSortingRequested: boolean): IPatchTableFilteredFields => {
  let row = {};
  otherService.currentUrl = otherService.currentUrl ? otherService.currentUrl : router.url;
  if (Object.keys(tableFilteredFields ?
    tableFilteredFields : {}).length > 0) {
    for (let key in tableFilteredFields) {
      if (otherService.currentUrl == key) {
        if (!areColumnFiltersEdited && !isPaginationRequested && !isSortingRequested) {
          // Cancel the default request which has the minimum of 20 records of each table to avoid replicated server requests 
          httpCancelService.cancelPendingRequestsOnFormSubmission();
        }
        // set the previous matched criterias to seacrColumns and include the existing one also using spread operator
        let columnFilterFormObj = {};
        Object.keys(tableFilteredFields[key]).forEach((objKey) => {
          if (objKey !== 'pageIndex' && objKey !== 'pageSize' && objKey !== 'sortOrder' && objKey !== 'sortOrderColumn') {
            columnFilterFormObj[objKey] = tableFilteredFields[key][objKey];
          }
          else {
            row[objKey] = tableFilteredFields[key][objKey];
          }
        });
        let preventPreviousSearchedFields = { ...columnFilterForm.value, ...columnFilterFormObj };
        Object.keys(preventPreviousSearchedFields).forEach((key) => {
          if (typeof preventPreviousSearchedFields[key] === 'string' && preventPreviousSearchedFields[key]?.split('-')?.length == 3) {
            preventPreviousSearchedFields[key] = new Date(preventPreviousSearchedFields[key]);
          }
        });
        // Patch the values to set the previously searched field values in the table
        if (preventPreviousSearchedFields) {
          row['searchColumns'] = filterFormControlNamesWhichHasValues(preventPreviousSearchedFields);
          columnFilterForm.patchValue(preventPreviousSearchedFields, { emitEvent: false, onlySelf: true });
        }
        break;
      }
    }
  }
  return { columnFilterForm, row };
}

const prepareReusablePTableLoadDataConfiguration = (tableFilteredFields, otherService: OtherService, router: Router, areColumnFiltersEdited,
  isPaginationRequested, isSortingRequested, event?, searchColumns?) => {
  otherService.currentUrl = otherService.currentUrl ? otherService.currentUrl : router.url;
  let actualTableFilteredObjectCopy = { ...tableFilteredFields[otherService.currentUrl] };
  switch (true) {
    case areColumnFiltersEdited:
      let sourceObj = SplitSearchColumnFieldsFromObject(actualTableFilteredObjectCopy);
      // Delete properties from the source object with comparing the destination object and return the source object
      let filteredSearchColumns = findPropsDifference(sourceObj, searchColumns);
      actualTableFilteredObjectCopy = { ...filteredSearchColumns, ...searchColumns };
      if (tableFilteredFields[otherService.currentUrl]?.sortOrderColumn) {
        actualTableFilteredObjectCopy['sortOrderColumn'] = tableFilteredFields[otherService.currentUrl]?.sortOrderColumn;
        actualTableFilteredObjectCopy['sortOrder'] = tableFilteredFields[otherService.currentUrl]?.sortOrder;
      }
      actualTableFilteredObjectCopy = resetToDefaultPagination(actualTableFilteredObjectCopy);
      break;
    case isPaginationRequested:
      actualTableFilteredObjectCopy['pageIndex'] = (event.first / event.rows);
      actualTableFilteredObjectCopy['pageSize'] = event.rows;
      break;
    case isSortingRequested:
      actualTableFilteredObjectCopy['sortOrderColumn'] = event.sortField;
      actualTableFilteredObjectCopy['sortOrder'] = event.sortOrder == '-1' ? 'DESC' : event.sortOrder == '1' ? 'ASC' : '';
      actualTableFilteredObjectCopy = resetToDefaultPagination(actualTableFilteredObjectCopy);
      break;
  }
  let searchColumnsObj = {};
  Object.keys(actualTableFilteredObjectCopy).forEach((key) => {
    if (key !== 'pageIndex' && key !== 'pageSize' && key !== 'sortOrderColumn' && key !== 'sortOrder' && key !== 'searchColumns') {
      searchColumnsObj[key] = actualTableFilteredObjectCopy[key];
      delete actualTableFilteredObjectCopy[key];
    }
  });
  actualTableFilteredObjectCopy['searchColumns'] = searchColumnsObj;
  return actualTableFilteredObjectCopy;
}

const resetToDefaultPagination = (actualTableFilteredObjectCopy) => {
  actualTableFilteredObjectCopy['pageIndex'] = +CommonPaginationConfig.defaultPageIndex;
  actualTableFilteredObjectCopy["pageSize"] = +CommonPaginationConfig.defaultPageSize;
  return actualTableFilteredObjectCopy;
}

const preparePTableLoadDataConfiguration = (event, tableFilteredFields, otherService: OtherService, persistedRow?) => {
  let actualTableFilteredKey = tableFilteredFields[otherService.currentUrl];
  let row = {};
  row['pageIndex'] = event.first / event.rows;
  if (row?.['pageIndex'] == 0 && event.first > 0 && actualTableFilteredKey?.['pageIndex'] > 0) {
    row['pageIndex'] = actualTableFilteredKey['pageIndex'];
  }
  row["pageSize"] = event.rows;
  row["sortOrderColumn"] = event.sortField ? event.sortField :
    actualTableFilteredKey?.sortOrderColumn ?
      actualTableFilteredKey.sortOrderColumn : '';
  row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : event.sortOrder == -1 ? 'DESC' :
    actualTableFilteredKey?.sortOrder ?
      actualTableFilteredKey.sortOrder : 'ASC';
  let rowObj = { ...persistedRow, ...row };
  let searchColumnsObj = {};
  rowObj['searchColumns'] = searchColumnsObj;
  return rowObj;
}

const SplitSearchColumnFieldsFromObject = (obj) => {
  let objWithSearchColumnFields = {};
  Object.keys(obj).forEach((key) => {
    if (key !== 'pageIndex' && key !== 'pageSize' && key !== 'sortOrderColumn' && key !== 'sortOrder' && key !== 'searchColumns') {
      objWithSearchColumnFields[key] = obj[key];
    }
  });
  return objWithSearchColumnFields;
}

// Delete properties from the source object with comparing the destination object and return the source object
const findPropsDifference = (sourceObject, destinationObject) => {
  Object.keys(sourceObject).filter(k => {
    if (sourceObject[k] !== destinationObject[k]) {
      delete sourceObject[k];
    }
  });
  return sourceObject;
}

const addDefaultTableObjectProperties = (primengTableConfigProperties, defaultPropertiesToBeOverridden?: [{ index: number, propertyName: string }]) => {
  primengTableConfigProperties.tableComponentConfigs.tabsList.forEach((tabObject, index: number) => {
    tabObject.enableBreadCrumb = true;
    tabObject.reorderableColumns = true;
    tabObject.resizableColumns = true;
    tabObject.enableScrollable = true;
    tabObject.enableFieldsSearch = true;
    tabObject.enableHyperLink = true;
    tabObject.cursorLinkIndex = 0;
    tabObject.enableAddActionBtn = true;
    tabObject.disabled = true;
    defaultPropertiesToBeOverridden?.forEach((defaultPropertyObject) => {
      if (index == defaultPropertyObject.index) {
        tabObject[defaultPropertyObject.propertyName] = defaultPropertyObject.propertyName == 'cursorLinkIndex' ? 0 : false;
      }
    });
  });
  return primengTableConfigProperties;
}

/* If the agent is already logged In the same system and the same user ID ( event with different browsers ) then call the api to fetch the
agent details rather than logging in again */
const retrieveAlreadyLoggedInAgentDetails = (crudService: CrudService, userService: UserService,
  loggedInUserData: LoggedInUserModel, store: Store<AppState>) => {
  crudService.get(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.RETRIEVE_SPECIFIC_AGENT_DETAIL, undefined,
    false, prepareRequiredHttpParams({
      userId: loggedInUserData.userId
    })).subscribe((response) => {
      if (response.resources && response.isSuccess && response.statusCode == 200) {
        let { agentKey, extension, regionId, mediaTypes } = response.resources;
        if (agentKey) {
          extension = extension.toString();
          regionId = regionId.toUpperCase();
          // After a successful media switching we want to update the media type in the store
          store.dispatch(new AgentLoginDataLoaded({
            agentKey,
            extensionNo: extension,
            agentLoginMediaTypes: mediaTypes,
            agentStatus: RoutingStates.AVAILABLE
          }));
          // take regionId as openscape pabxID and add it in ngrx store
          if (regionId) {
            loggedInUserData.openScapePabxId = regionId;
            loggedInUserData.mediaTypeIds = mediaTypes?.filter((mT) => !mT.isLoggedOff)?.map(mT => mT.mediaTypeId);
            userService.openScapePabxId = regionId;
            store.dispatch(new LoginUpdate({
              user: loggedInUserData
            }));
          }
        }
      }
    });
}

const filterListByKey = (arr, key, searchKey) => {
  return arr.filter((obj) =>
    Object.keys(obj).some((nestedKey) => {
      if (key == nestedKey) {
        if (typeof obj[key] === "number") {
          return obj[key].toString().includes(searchKey);
        } else if (typeof obj[key] === "string") {
          return obj[key].toLowerCase().includes(searchKey.toLowerCase());
        }
      }
    }));
}

const formatMobileNumbersBeforeSubmit = (body?: object | Array<object>) => {
  if (body instanceof Object) {
    Object.keys(body).forEach((bodyKey) => {
      if (body[bodyKey] instanceof Array) {
        body[bodyKey].forEach((arrayObj) => {
          arrayObj = formatMobileNumbersObject(body[bodyKey], arrayObj);
        });
      }
      else if (body[bodyKey] instanceof Object) {
        Object.keys(body[bodyKey]).forEach((nestedArrayObjKey) => {
          if (DIFFERENT_MOBILE_NUMBER_NAMES.includes(nestedArrayObjKey) && body[bodyKey][nestedArrayObjKey]) {
            body[bodyKey][nestedArrayObjKey] = body[bodyKey][nestedArrayObjKey].toString().replace(/\s/g, "");
          }
          else if (DIFFERENT_MOBILE_NUMBER_NAMES.includes(nestedArrayObjKey) && (!body[bodyKey][nestedArrayObjKey] || body[bodyKey][nestedArrayObjKey] == '')) {
            delete body[bodyKey][nestedArrayObjKey];
            delete body[bodyKey][nestedArrayObjKey + 'CountryCode'];
          }
          else if (!body[bodyKey][nestedArrayObjKey] || body[bodyKey][nestedArrayObjKey] == '') {
            delete body[bodyKey][nestedArrayObjKey];
          }
        });
      }
      else {
        if (DIFFERENT_MOBILE_NUMBER_NAMES.includes(bodyKey) && body[bodyKey]) {
          body[bodyKey] = body[bodyKey].toString().replace(/\s/g, "");
        }
        else if (DIFFERENT_MOBILE_NUMBER_NAMES.includes(bodyKey) && (!body[bodyKey] || body[bodyKey] == '')) {
          delete body[bodyKey];
          delete body[bodyKey + 'CountryCode'];
        }
        else if (!body[bodyKey] || body[bodyKey] == '') {
          delete body[bodyKey];
        }
      }
    });
  }
  else if (body as any instanceof Array) {
    (body as any).forEach((arrayObj) => {
      arrayObj = formatMobileNumbersObject(body, arrayObj);
    });
  }
  return body;
}

const formatMobileNumbersObject = (body, arrayObj) => {
  if (body instanceof Array) {
    body.forEach((innerBody) => {
      Object.keys(arrayObj).forEach((bodyKey) => {
        if (DIFFERENT_MOBILE_NUMBER_NAMES.includes(bodyKey) && innerBody[bodyKey]) {
          arrayObj[bodyKey] = arrayObj[bodyKey].toString().replace(/\s/g, "");
        }
        else if (DIFFERENT_MOBILE_NUMBER_NAMES.includes(bodyKey) && (!innerBody[bodyKey] || innerBody[bodyKey] == '')) {
          delete innerBody[bodyKey];
          delete innerBody[bodyKey + 'CountryCode'];
        }
        else if (!innerBody[bodyKey] || innerBody[bodyKey] == '') {
          delete innerBody[bodyKey];
        }
      });
    })
  }
  else if (body instanceof Object) {
    Object.keys(arrayObj).forEach((bodyKey) => {
      if (DIFFERENT_MOBILE_NUMBER_NAMES.includes(bodyKey) && body[bodyKey]) {
        arrayObj[bodyKey] = arrayObj[bodyKey].toString().replace(/\s/g, "");
      }
      else if (DIFFERENT_MOBILE_NUMBER_NAMES.includes(bodyKey) && (!body[bodyKey] || body[bodyKey] == '')) {
        delete body[bodyKey];
        delete body[bodyKey + 'CountryCode'];
      }
      else if (!body[bodyKey] || body[bodyKey] == '') {
        delete body[bodyKey];
      }
    });
  }

  return arrayObj;
}

export {
  exportList, convertTreeToList,
  formatColumn,
  getModelbasedKeyToFindId,
  prepareGetRequestHttpParams,
  removeLastPluralLetter,
  removeFormControlError,
  prepareRequiredHttpParams,
  setRequiredValidator,
  setMinMaxLengthValidator,
  setFormArrayControlsRequiredValidator,
  setFormArrayControlsMinMaxValidator,
  removeAllFormValidators,
  clearFormControlValidators,
  removeFormControls,
  addFormControls, 
  sortOrder, accountNoEncrption,
  phoneMask, fileUrlDownload, setMinMaxTimeValidator,
  getFullFormatedAddress, getNthDate, getNthMonths, getPDropdownData, getMatMultiData, getNthDateExcludeWeekends, getNthMinutes,
  setFormArrayControlsMinMaxValueValidator,
  isString, setPercentageValidator, setDateValidator, setMinuteValidator, setSecValidator, setMinMaxDateValidator, setHourValidator,
  convertCamelCasesToSpaces,
  capitalizeFirstLetterOfEachString, setEmailValidator, findInvalidControls,
  findInvalidFormControls, setSAIDValidator,
  setMinMaxValidator, enableFormControls, disableFormControls, autofillFormFieldValues,
  randomNameOrNumberGenerator, prepareAutoCompleteListFormatForMultiSelection, validateLatLongFormat,
  retrieveUniqueObjects, deleteObjectProps, addIsCheckedProperty, convertTwelevHoursToTwentyFourHours,
  convertTwentyFourHoursToTwelevHours, convertCustomHoursAndMinutesToDateFormat, removePropsWhichHasNoValuesFromObject,
  filterFormControlNamesWhichHasValues, mobileNumberSpaceMaskingReplaceFromFormValues,
  destructureAfrigisObjectAddressComponents, getRandomLightColor, setMaxValidatorWithValue, convertStringsToQueryParams,
  swapLongitudeToLatitute, setMinValidatorWithValue, convertData, encryptData, decryptData, retrieveUniqueObjectsWithProperty,
  generateCurrentYearToNext99Years, findDisabledFormControls, findDuplicate, generateQueryParams,
  onFormControlChangesToFindDuplicate, disableAllUserEvents, onBlurActionOnDuplicatedFormControl, groupByArrayByKey,
  addNullOrStringDefaultFormControlValue, addTrueOrFalseDefaultFormControlValue, exportToExcel, saveAsExcelFile,
  enableWithDefaultFormControlValues, disableWithDefaultFormControlValues, generateDays, urlPatternForStraitaDomain, findTablePermission,
  removeProperyFromAnObjectWithDeepCopy, prepareCoordinatesForLeafLetMapComponent, findTheDifferenceBetweenTwoArrays, prepareDynamicTableTabsFromPermissions,
  removeAllFormControls, removeFormValidators, convertISOStringtoDateAndTodayDate, adjustDateFormatAsPerPCalendar, createOrUpdateFilteredFieldKeyValues,
  patchPersistedNgrxFieldValuesToForm, IPatchTableFilteredFields, preparePTableLoadDataConfiguration, SplitSearchColumnFieldsFromObject,
  findPropsDifference, addDefaultTableObjectProperties, prepareReusablePTableLoadDataConfiguration, retrieveAlreadyLoggedInAgentDetails,
  filterListByKey, formatMobileNumbersBeforeSubmit
};