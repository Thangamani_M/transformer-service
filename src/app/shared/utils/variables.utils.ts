import { AngularEditorConfig } from "@kolkov/angular-editor";
import { ChatRoomModuleApiSuffixModels } from "@modules/chat-room";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { DealerModuleApiSuffixModels } from "@modules/dealer";
import { EventMgntModuleApiSuffixModels } from "@modules/event-management";
import { FAQModuleApiSuffixModels } from "@modules/faq";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { OpenScapModuleApiSuffixModels } from "@modules/my-tasks/components/email-dashboard/open-scap-email.enums";
import { ItManagementApiSuffixModels } from "@modules/others";
import { CampaignModuleApiSuffixModels } from "@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum";
import { OutOfOfficeModuleApiSuffixModels } from "@modules/out-of-office/enums/out-of-office-employee-request.enum";
import { PanicAppApiSuffixModels } from "@modules/panic-app";
import { ReportsModuleApiSuffixModels } from "@modules/reports/shared";
import { BillingModuleApiSuffixModels, CallCenterModuleApiSuffixModels, SalesModuleApiSuffixModels, TelephonyApiSuffixModules } from "@modules/sales";
import { TranslatorModuleApiSuffixModels } from "@modules/signal-translator";
import { TechnicalMgntModuleApiSuffixModels } from "@modules/technical-management/shared/enum.ts/technical.enum";
import { UserModuleApiSuffixModels } from "@modules/user";
import { AuthenticationModuleApiSuffixModels, CommonModuleApiSuffixModels, SharedModuleApiSuffixModels } from "./enums.utils";

const formConfigs = {
  masterSettingMinLength: 3,
  masterSettingMinNameLength: 3,
  masterSettingMaxNameLength: 75,
  minLengthTwo: 2,
  minLengthZero: 0,
  maxLengthTwenty: 20,
  thirteenLength: 13,
  eleven: 11,
  sixteenDigits: 16,
  minAccountNumberDigits: 9,
  minLengthEightForPassword: 8,
  maxLengthFiveHundred: 500,
  maxLengthHundred: 100,
  maxLengthFifteenForPassword: 15,
  generalLengthFive: 5,
  generalNameLengthFiftyLength: 30,
  generalNameLengthSeventyFiveLength: 75,
  generalNameLengthTwoFifty: 250,
  phoneNumberMaxLength: 16,
  postalCodeMaxLength: 4,
  firstNameMinLength: 1,
  firstNameMaxLength: 50,
  lastNameMinLength: 1,
  lastNameMaxLength: 75,
  streetNumberMinLength: 1,
  streetNumberMaxLength: 10,
  streetNameMinLength: 3,
  streetNameMaxLength: 50,
  buildingNameMinLength: 2,
  buildingNameMaxLength: 50,
  buildingNumberMinLength: 1,
  buildingNumberMaxLength: 10,
  percentageMinlength: 2,
  percentageMaxlength: 5,
  percentageMaxvalue: 100,
  contactnumberLength: 16,
  itemNameMinLength: 1,
  itemNameMaxLength: 250,
  itemCategoryNameMinLength: 0,
  itemCategoryNameMaxLength: 100,
  itemCodeMinLength: 0,
  itemCodeMaxLength: 20,
  itemBrandNameMinLength: 0,
  itemBrandNameMaxLength: 20,
  creditCardNumberWithSpaceMask: 19,
  creditCardNumberWithoutSpaceMask: 16,
  uOMNameMinLength: 0,
  uOMNameMaxLength: 20,
  itemPriceTypeNameMinLength: 1,
  itemPriceTypeNameMaxLength: 100,
  contactnumberMinLength: 11,
  contactnumberMaxLength: 12,
  divisionMinLength: 2,
  divisionMaxLength: 75,
  warrentPeriodMinValue: 1,
  warrentPeriodMaxValue: 99,
  stockThresholdMinValue: 1,
  stockThresholdMaxValue: 20000,
  contactnumberMinLengthNineDigit: 9,
  nineDigits: 9,
  fifteen: 15,
  sixteen: 16,
  one: 1,
  two: 2,
  three: 3,
  four: 4,
  five: 5,
  six: 6,
  seven: 7,
  eight: 8,
  ten: 10,
  fifty: 50,
  twoFifty: 250,
  threeHundred: 300,
  thousands: 1000,
  maxLengthThree: 3,
  maxLengthFifty: 50,
  serviceCategoryDescription: 100,
  southAfricanContactNumberMaxLength: 11,
  indianContactNumberMaxLength: 12,
  maxLengthTwentyFive: 25,
  cardNumber: 19,
  countryId: '5AF34A38-AB5B-4A45-BC19-2F4486FABCD0',
  generalName2LengthFiftyLength: 150
};

const defaultCountryCode = "+27";  // refers to south africa

const mobileNumberMaskPattern = '00 000 00000';    // default pattern refers to ngx pattern npm module

const landLineNumberMaskPattern = '00 000 00000';    // default pattern refers to ngx pattern npm module

const debounceTimeForSearchkeyword = 600       // in milliseconds

const debounceTimeForDeepNestedObjectSearchkeyword = 1000       // in milliseconds

const countryCodes = [{ displayName: '+27' }, { displayName: '+91' }, { displayName: '+45' }];   // default country codes

const defaultLeafLetMapZoomValue = 6;              // to zoom leaf let map

const defaultLeafletLatitude = -25.90589;              // to open leaf let map with initial latitude (south african region)

const defaultLeafletLongitude = 28.121942;              // to open leaf let map with initial longitude (south african region)

const leaflefApiKey = 'wly6pnqvmbuioatp8an1zigi6n3h6mntxr5c4mxw';

const regionId = "11BCD049-07B7-4E2E-84D5-12A5A5E8C608";               // Default region Id

const monthsByNumber = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

const currentOrGreaterMonths = (expectedYear) => {
  let currentYearRemainingMonths = +expectedYear == new Date().getFullYear() ?
    new Date().getMonth() + 1 : new Date(expectedYear).getMonth() + 1;
  let months = [];
  for (let i: any = 1; i <= 12; i++) {
    if (i >= currentYearRemainingMonths) {
      i = i.toString();
      if (i?.length == 1) {
        i = '0' + i;
      }
      months.push(i);
    }
  }
  return months;
}

const PERMISSION_RESTRICTION_ERROR = 'Permission is restricted..!!';

const ATLEAST_ONE_RECORD = 'Atleast one record is required..!!';

const SELECT_REQUIRED_SIGNAL = 'Select the required signal..!!';

const AGENT_LOGIN_ERROR_MESSAGE = 'Agent login is required..!!';

const ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS = ['.doc', '.docx', '.jpg', '.jpeg', '.pdf', '.png', '.xls', '.xlsx', '.DOC', '.DOCX', '.JPG', '.JPEG', '.PDF', '.PNG', '.XLS', '.XLSX'];

const WRONG_FILE_EXTENSION_SELECTION_ERROR = 'Select a valid file';

const DEFAULT_EXTENSION_NUMBER = '4028';

const MAX_FILES_UPLOAD_COUNT = 5;

const DEFAULT_COMMON_DASHBOARD_NAVIGATION = 'common-dashboard';

const CURRENT_YEAR_TO_NEXT_FIFTY_YEARS = (new Date().getFullYear() + ":" + (new Date().getFullYear() + 50));

const FROM_PREVIOUS_FIFTY_YEARS_TO_NEXT_FIFTY_YEARS = ((new Date().getFullYear() - 50) + ":" + (new Date().getFullYear() + 50));

const ANGULAR_EDITOR_CONFIG: AngularEditorConfig = {
  editable: true,
  spellcheck: true,
  height: '15rem',
  minHeight: '5rem',
  placeholder: 'Enter text here...',
  translate: 'no',
  defaultParagraphSeparator: 'p',
  defaultFontName: 'Arial',
  toolbarHiddenButtons: [
    ['bold']
  ],
  customClasses: [
    {
      name: "quote",
      class: "quote",
    },
    {
      name: 'redText',
      class: 'redText'
    },
    {
      name: "titleText",
      class: "titleText",
      tag: "h1",
    },
  ]
};

const standardHeightForPscrollContainer = '65vh';

const DELETE_CONFIRMATION_MESSAGE = `Are you sure you want to delete this?`;

const SERVER_REQUEST_DATE_TRANSFORM = 'yyyy-MM-dd';

const SERVER_REQUEST_DATE_TIME_TRANSFORM = 'yyyy-MM-dd HH:mm:ss';

const DEFAULT_OTP_REMAINING_TIME_IN_SECONDS = '300';

const NO_DATA_FOUND_MESSAGE = 'No Data Found';

const DIFFERENT_MOBILE_NUMBER_NAMES = ['mobileNo', 'mobileNumber', 'contactNo', 'contactNumber', 'premisesNo', 'premisesNumber',
  'mobile1', 'mobile1Number', 'mobile2', 'mobile2Number', 'mobile2', 'mobile2Number',
  'mobileNo1', 'mobileNo2', 'mobile1No', 'mobile2No', 'mobile2Number', 'officeNo', 'officeNumber'];

const WINDOW_CLOSE = (timeoutInMilliseconds = 1000) => {
  setTimeout(() => {
    window.close();
  }, timeoutInMilliseconds);
}

let API_SUFFIX_MODELS: UserModuleApiSuffixModels | InventoryModuleApiSuffixModels | CustomerModuleApiSuffixModels | TechnicalMgntModuleApiSuffixModels | OutOfOfficeModuleApiSuffixModels |
  SalesModuleApiSuffixModels | AuthenticationModuleApiSuffixModels | CallCenterModuleApiSuffixModels | TelephonyApiSuffixModules |
  CommonModuleApiSuffixModels | BillingModuleApiSuffixModels | TranslatorModuleApiSuffixModels | EventMgntModuleApiSuffixModels | SharedModuleApiSuffixModels | ItManagementApiSuffixModels |
  FAQModuleApiSuffixModels | DealerModuleApiSuffixModels | CollectionModuleApiSuffixModels | OpenScapModuleApiSuffixModels |
  PanicAppApiSuffixModels | CampaignModuleApiSuffixModels | ReportsModuleApiSuffixModels | ChatRoomModuleApiSuffixModels;

export {
  formConfigs, defaultCountryCode, mobileNumberMaskPattern, landLineNumberMaskPattern, debounceTimeForSearchkeyword, countryCodes,
  defaultLeafLetMapZoomValue, defaultLeafletLatitude, defaultLeafletLongitude, leaflefApiKey, regionId, monthsByNumber, PERMISSION_RESTRICTION_ERROR,
  debounceTimeForDeepNestedObjectSearchkeyword, ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS, WRONG_FILE_EXTENSION_SELECTION_ERROR,
  DEFAULT_EXTENSION_NUMBER, API_SUFFIX_MODELS, MAX_FILES_UPLOAD_COUNT, ANGULAR_EDITOR_CONFIG, standardHeightForPscrollContainer,
  DELETE_CONFIRMATION_MESSAGE, AGENT_LOGIN_ERROR_MESSAGE, SELECT_REQUIRED_SIGNAL, DEFAULT_COMMON_DASHBOARD_NAVIGATION, ATLEAST_ONE_RECORD,
  CURRENT_YEAR_TO_NEXT_FIFTY_YEARS, FROM_PREVIOUS_FIFTY_YEARS_TO_NEXT_FIFTY_YEARS, SERVER_REQUEST_DATE_TRANSFORM, WINDOW_CLOSE, SERVER_REQUEST_DATE_TIME_TRANSFORM,
  currentOrGreaterMonths, DEFAULT_OTP_REMAINING_TIME_IN_SECONDS, NO_DATA_FOUND_MESSAGE, DIFFERENT_MOBILE_NUMBER_NAMES
};