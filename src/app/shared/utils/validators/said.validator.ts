import { AbstractControl } from '@angular/forms';

export const saidPattern = (control: AbstractControl): object | null => {
    const saidValue: string = control.value;
    let validatorObj;
    if (saidValue && saidValue != "") {
        let monthsByNumber = saidValue.slice(2, 4);
        let totalDaysInAMonth = saidValue.slice(4, 6);
        // first two digits refers to years ( a user cannot have more then or equals 100 years of age )
        validatorObj = (new RegExp("^(([0][1-9])|([1][0-2])){1}$").test(monthsByNumber)) ? null : { invalid: true };
        // next two digits refers to days based on months 
        // next two digits refers to months from 01 to 12
        // the following months are having 31 as max days
        if (!validatorObj) {
            if (monthsByNumber == MonthsByNumber.JANUARY || monthsByNumber == MonthsByNumber.MARCH || monthsByNumber == MonthsByNumber.MAY ||
                monthsByNumber == MonthsByNumber.JULY || monthsByNumber == MonthsByNumber.AUGUST || monthsByNumber == MonthsByNumber.OCTOBER ||
                monthsByNumber == MonthsByNumber.DECEMBER) {
                validatorObj = (new RegExp("^(([0][1-9])|([1][0-9])|([2][0-9])|([3][0-1])){1}$").test(totalDaysInAMonth)) ? null : { invalid: true };
            }
            else if (monthsByNumber == MonthsByNumber.FEBRUARY) {
                validatorObj = (new RegExp("^(([0][1-9])|([1][0-9])|([2][0-9])){1}$").test(totalDaysInAMonth)) ? null : { invalid: true };
            }
            // the following months are having 30 as max days
            else if (monthsByNumber == MonthsByNumber.APRIL || monthsByNumber == MonthsByNumber.JUNE || monthsByNumber == MonthsByNumber.SEPTEMBER ||
                monthsByNumber == MonthsByNumber.NOVEMBER) {
                validatorObj = (new RegExp("^(([0][1-9])|([1][0-9])|([2][0-9])|([3][0])){1}$").test(totalDaysInAMonth)) ? null : { invalid: true };
            }
        }
    }
    else if (!saidValue || saidValue == "") {
        if (control?.errors?.required) {
            validatorObj = { required: true };
        }
        else {
            validatorObj = null;
        }
    }
    else if (control?.errors?.invalid) {
        validatorObj = { invalid: true };
    }
    else {
        validatorObj = null;
    }
    return validatorObj;
}

enum MonthsByNumber {
    JANUARY = '01',
    FEBRUARY = '02', MARCH = '03', APRIL = '04', MAY = '05', JUNE = '06', JULY = '07', AUGUST = '08', SEPTEMBER = '09', OCTOBER = '10', NOVEMBER = '11', DECEMBER = '12'
}