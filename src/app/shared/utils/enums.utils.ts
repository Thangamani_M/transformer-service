enum ValidatorTypes {
  REQUIRED = "required",
  MIN_LENGTH = "minLength",
  MAX_LENGTH = "maxLength",
  PATTERN = "pattern",
  EMAIL = "email",
  MIN = "min",
  MAX = "max",
}

enum CommonPaginationConfig {
  defaultPageSize = "10",
  defaultPageIndex = "0",
  defaultPageSizeAsTwenty='20'
}

enum CrudType {
  CREATE = "create",
  VIEW = 'view',
  GET = "get",
  EDIT = "edit",
  DELETE = "delete",
  CHECK = 'check',
  CLICK = 'click',
  NOCONTACTS = 'no-contacts',
  EXPORT = 'export',
  ICON_POPUP = 'icon popup',
  STATUS_POPUP = 'status popup',
  QUANTITY_CLICK = 'quantity-click',
  EDIT_ACTION = 'edit-action',
  RELOAD = 'reload',
  FILTER = 'filter', // Added to open form for Filter process
  UPLOAD = 'upload',
  APPROVED = 'approved',
  SCAN = 'scan',
  EMAIL = 'email',
  INSPECTIONCANCEL = 'inspectioncancel',
  CANCEL = 'cancel',
  INTERTECHTRANSFER = 'interTechTransfer',
  SCHEDULE = 'schedule',
  Export = 'export',
  SMS = 'sms',
  STOCK_SWAP = 'stock_swap',
  ACTION = 'action',
  FLAG = 'flag',
  QUICK_ACTION = 'quick-action',
  BULK_UPDATE = 'bulk-update',
  CUSTOM_VIEW = 'custom-view',
  CUSTOM_ASSIGN = 'custom-assign',
  CUSTOM_CANCEL = 'custom-cancel',
  REFRESH = 'refresh',
}

enum QuickActionType {
  EDIT = "edit",
  VIEW_ALTER_CONTACT = 'view alter contact',
  PROCESS_CANEL = 'process cancel',
  CANCELLATION_HISTORY = 'cancellation history',
  SAVE_OFFER = 'save offer',
  EMAIL = 'email',
  SMS = 'sms',
  UPGRADE_DOWNGRADE = 'upgrade downgrade',
  SUSPEND_SERVICE = 'suspend service',
  BALANCE_OF_CONTRACT = 'Balance Of Contract',
  PURCHASE_OF_ALARM_SYSTEM = 'Purchase Of Alarm System'
}

enum SnackbarMessageType {
  SUCCESS = "success",
  ERROR = "error",
  WARNING = "warning",
}

enum ModulesBasedApiSuffix {
  INVENTORY = "inventory",
  IT_MANAGEMENT = "users",
  CUSTOMER_MANAGEMENT = "customerManagement",
  SALES = "sales",
  AUTHENTICATION = "authentication",
  CALL_CENTER = "callcenter",
  TELEPHONY = "telephony",
  BILLING = 'billing',
  TRANSLATOR = 'translator',
  TRANSLATOR_CONFIG = 'translator-config',
  EVENT_MANAGEMENT = 'event-mangement',
  ROUTING_STATE = 'routing-state',
  COMMON_API = 'manual-repository',
  TECHNICIAN = 'technician',
  // MANUAL_REPOSITORY = 'manual-repository',
  SHARED = "shared",
  CONFIGURE_IT_MANAGEMENT = "itmgmt",
  FILE_PROCESS = 'fileprocess',
  DEALER = 'dealer',
  COLLECTIONS = 'collections',
  OPEN_SCAP_EMAIL = 'open-scap-email',
  PANIC_APP = 'panic-app',
  PDF ='pdf'
}

enum CommonRoles {
  MANAGER = "Manager",
  TECHNICIAN = "Technician",
}

enum StatusTypes {
  CREATED = "Created",
  MODIFIED = "Modified",
  REJECTED = "Rejected",
  APPROVED = "Approved",
  INITIATED = "Initiated",
  PARTIALLY_APPROVED = "Partially Approved",
}

enum AuthenticationModuleApiSuffixModels {
  LOGIN = "users/login",
  SIGNUP = "signup",
  UX_USER_CHANGE_STATUS = "users/change-status",
  GENERATE_PASSWORD = 'users/generate-password',
  SET_ROUTING_STATE = 'Agent/SetRoutingState',
  SET_ROUTING_STATE_NEW = 'agent/set-routing-state',
  SET_ROUTING_STATE_EMAIL = 'email/com/routing-state',
  REFRESH_TOKEN = 'users/refresh-token',
  REFRESH_TOKEN_V2 = 'v2/users/refresh-token',
  LOGIN_V2 = 'v2/users/login',
  AD_LOGIN = 'v2/users/ad/login',
  LOGOUT_V2 = 'v2/user/logout'
}

enum Modules {
  INVENTORY_MGMT = "Inventory Management",
  IT_MGMT = "IT Management",
  CUSTOMER_MGMT = "Customer Management",
  SALES_MGMT = "Sales",
}

enum CustomPopupMessages {
  PERM_DENIED = "Permission is denied. Contact administrator",
  ATLEAST_ONE_ITEM = "",
}

enum CommonModuleApiSuffixModels {
  SUBSCRIPTIONS = 'subscriptions',
  OUT_OF_OFFICE = 'out-of-office-configuration',
}

enum CustomFormControlTypes {
  INPUT = 'input',
  TEXT_AREA = 'text area',
  SELECT = 'select',
  MAT_SELECT = 'mat select',
  MAT_RADIO = 'mat radio',
  RADIO = 'radio'
}

enum LeadGroupTypes {
  NEW = 1,
  RECONNECTION = 2,
  RELOCATION = 3,
  UPGRADE = 4,
}

enum SharedModuleApiSuffixModels {
  NOTIFICATIONS = 'notification',
  DOA_CONFIGURATION = 'doa-config',
  TAM_INVESTIGATION_FOLLOWUP_NOTIFICATION = 'tech-stock-take-investigation-followup-notification',
  UX_OUT_OF_OFFICE_DIVISION_USER = 'ux/out-of-office/divisions-user',
  UX_OUT_OF_OFFICE_DEPARTMENTS_USER = 'ux/out-of-office/departments-user',
  UX_OUT_OF_OFFICE_ROLSE_USER = 'ux/out-of-office/roles-user',
  UX_SELF_HELP_SYSTEM_TYPES = 'ux/self-help/system-types',
  CALL_CENTER = 'call-centers',
  UX_BRANCHES = 'ux/branches',
  UX_QUEUES = 'ux/queues',
  TICKET_EMAIL_NOTIFICATIONS = 'ticket-email-notifications',
  TICKET_SMS_NOTIFICATIONS = 'ticket-sms-notifications',
  UX_REGION_BRANCHES = 'ux/region-branches',
  CALL_CENTER_NUMBER = 'call-centers- number',
  NOTIFICATION_DETAILS = 'notification-details',
  PROCESS_NAME_CONFIG = 'process-type-department-mapping',
  PROCESS_NAME_CONFIG_DETAILS = 'process-type-department-mapping/details',
  UX_PROCESS_NAME_DROPDOWN = 'ux/process-type',
  UX_PROCESS_TYPE_DEPARTMENT = 'ux/process-type/department',
  SEND_OTP = 'send-otp',
  VERIFY_OTP = 'otp',
  DATE_FORMAT_API = 'global-date-format',
  UPDATE_DATE_FORMAT_API = 'update-global-date-format',
  MICROSOFT_ENDPOINT = 'login.microsoftonline.com',
  DEBTOR_ONLINE_ACCEPTANCE_OTP_RESEND='otp/debtor'
}

enum MenuCategoriesForPermissions {
  MENU = 1,
  TAB = 2,
  FEATURE = 3
}

enum PermissionTypes {
  ADD = 'Add',
  EDIT = 'Edit',
  LIST = 'List',
  DELETE = 'Delete',
  DOWNLOAD = 'Download',
  EXPORT = 'Export',
  CHAT = 'Chat',
  SEARCH = 'Search',
  SERVICE = 'Service',
  CALL = 'Call',
  NO_CONTACTS = 'No Contacts',
  UPLOAD = 'Upload',
  FILTER = 'Filter',
  SCHEDULE = 'Schedule',
  PRINT = 'Print',
  LOGOUT = 'Logout',
  DECLINE = 'Decline',
  CHANGE_OWNER = 'Change Owner',
  LOGGEDIN = 'Logged In',
  PASSWORD_VIEWS = 'Password Views',
  REFRESH = 'Refresh',
  QUICK_ACTION = 'Quick Action',
  CANCEL = "Cancel",
  COMPLETE = "Complete",
  GENERATE = "Generate",
  VERIFY = "Verify",
  VIEW = "View",
  ASSIGN = "Assign",
  SCAN = 'Scan',
  REINSTATE = 'Reinstate',
  TERMINATION = 'Termination',
  SUSPEND = 'Suspend',
  EMAIL = 'Email',
  APPROVE = 'Approve',
  START_CAMPAIGN = 'Start Campaign',
  PAUSE_CAMPAIGN = 'Pause Campaign',
  CANCEL_CAMPAIGN = 'Cancel Campaign',
  NOTIFICATION = 'Notification',
  SPECIAL_EDIT = 'Special Edit',
  SIMPLE_EDIT = 'Simple Edit',
  RESOLVE_ESCALATION = 'Resolve Escalation',
  RUN = 'Run',
  POST = 'Post',
  SMS = "SMS",
  ACCESS_CREDIT_CARD = 'Access Credit Card',
  REQUEST_FOR_DISCOUNT = 'Request For Discount',
  RESET = "Reset",
  UPGRADE = "Upgrade",
  DOWNGRADE = "Downgrade",
  REARRAGE = 'Rearrange',
  EXPAND = 'Expand',
  INTERTECHTRANSFER = 'Inter Tech Transfer',
  COURIER_NAME = 'CourierName',
  SAP_VENDOR_NUMBER = 'SAP Vendor Number',
  ACCOUNT_NUMBER ='Account Number',
}

enum CallbackStatus {
  SCHEDULED = 1,
  COMPLETED = 2,
  CANCELLED = 3
}

enum SignalRTriggers {
  OutboundCallNotificationsTrigger = 'OutboundCallNotificationsTrigger',
  OutboundCallDisconnectedNotificationsTrigger = 'OutboundCallDisconnectedNotificationsTrigger',
  CallHoldNotificationsTrigger = 'CallHoldNotificationsTrigger',
  CallTransferredNotificationsTrigger = 'CallTransferredNotificationsTrigger',
  OutboundCallFailedNotificationsTrigger = 'OutboundCallFailedNotificationsTrigger',
  InboundCallDeliveredTrigger = 'InboundCallDeliveredTrigger',
  InboundCallNotificationsTrigger = 'InboundCallNotificationsTrigger',
  InboundCallDisconnectedNotificationsTrigger = 'InboundCallDisconnectedNotificationsTrigger',
  InboundCallFailedNotificationsTrigger = 'InboundCallFailedNotificationsTrigger',
  ScheduleCallbackTrigger = 'ScheduleCallbackTrigger',
  CustomerAgentCallbackTrigger = 'CustomerAgentCallbackTrigger',
  CallbackAcceptNotificationTrigger = 'CallbackAcceptNotificationTrigger',
  DynamicDataChangeTrigger = 'DynamicDataChangeTrigger',
  NotificationTrigger = 'NotificationTrigger',
  InspectionCallbackTrigger = 'InspectionCallbackTrigger',
  OutboundCallDeliveredTrigger = 'OutboundCallDeliveredTrigger',
  DispatcherMapViewTrigger = 'DispatcherMapViewTrigger',
  ServiceContractCheckListRefreshTrigger = 'ServiceContractCheckListRefreshTrigger',
  InstallationContractCheckListRefreshTrigger = 'InstallationContractCheckListRefreshTrigger',
  RebookSnoozeTrigger = 'RebookSnoozCallBackTrigger',
  CallInitiationOnholdFollowupTrigger = 'CallInitiationOnholdFollowupTrigger',
  RiskWatchListRefreshTrigger = 'RiskWatchListRefreshTrigger',
  CustomerLivePosTrigger = 'CustomerLivePosTrigger',
  DispatcherStackViewTrigger = 'DispatcherStackViewTrigger',
  PhonerStackViewTrigger = 'PhonerStackViewTrigger',
  CallWorkFlowDispatchTrigger = 'CallWorkFlowDispatchTrigger',
  CallWorkFlowPhoneCallFlowTrigger = 'CallWorkFlowPhoneCallFlowTrigger',
  CallWorkFlowSignalHistoryTrigger = 'CallWorkFlowSignalHistoryTrigger',
  PhonerStackViewChangesTrigger = 'PhonerStackViewChangesTrigger',
  DispatcherStackViewChangesTrigger = 'DispatcherStackViewChangesTrigger'
}

enum AgentMediaTypes {
  VOICE = '1',
  CALLBACK = '2',
  EMAIL = '3'
}

export {
  CommonPaginationConfig,
  CrudType,
  ValidatorTypes,
  SnackbarMessageType,
  ModulesBasedApiSuffix,
  CommonRoles,
  StatusTypes,
  AuthenticationModuleApiSuffixModels,
  Modules,
  CustomPopupMessages,
  CommonModuleApiSuffixModels,
  CustomFormControlTypes, MenuCategoriesForPermissions, PermissionTypes,
  LeadGroupTypes, SharedModuleApiSuffixModels, QuickActionType, CallbackStatus, SignalRTriggers,
  AgentMediaTypes
};
