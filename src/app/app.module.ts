import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthEffects, authReducer, CommonDashboardComponent, dynamicEagerLoadingReducer, EagerLoadingDataEffects, LoginComponent, PageNotFoundComponent, staticEagerLoadingReducer } from '@app/modules/others';
import { LayoutModule, SharedModule } from '@app/shared';
import { ApiPrefixInterceptor, SuccessErrorHandlerInterceptor } from '@app/shared/services';
import { MsalInterceptor, MsalModule } from '@azure/msal-angular';
import { NgIdleModule } from '@ng-idle/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MessageService } from 'primeng/api';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { WindowRefService } from '../app/modules/others/non-crm/components/remote-acceptance/components/window-service';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { metaReducers, reducers } from './reducers';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    CommonDashboardComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule, CommonModule, AppRoutingModule,
    BrowserAnimationsModule, HttpClientModule,
    FormsModule, ReactiveFormsModule,SharedModule,
    LayoutModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      }
    }), StoreModule.forFeature('auth', authReducer),
    StoreModule.forFeature('staticEagerLoadingData', staticEagerLoadingReducer),
    StoreModule.forFeature('dynamicEagerLoadingData', dynamicEagerLoadingReducer),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature([AuthEffects, EagerLoadingDataEffects]),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
    ProgressSpinnerModule,
    NgIdleModule.forRoot(),
    MsalModule.forRoot({
      auth: {
        clientId: environment.MSALClientId,
        authority: "https://login.microsoftonline.com/" + environment.MSALTenantId,
        validateAuthority: true,
        redirectUri: environment.redirectUrl,
        postLogoutRedirectUri: environment.postLogoutRedirectUri,
        navigateToLoginRequestUrl: true,
      },
      cache: {
        cacheLocation: 'localStorage'
      },
    })
  ],
  providers: [Title, MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: ApiPrefixInterceptor
    },
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: SuccessErrorHandlerInterceptor
    },
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-GB' },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    WindowRefService,
    // {
    //   provide: RouteReuseStrategy,
    //   useClass: RouteReusableStrategy
    // }
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MsalInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: []
})

export class AppModule { }
