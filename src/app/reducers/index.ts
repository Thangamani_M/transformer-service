import {
  ActionReducer,
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
export interface AppState {

}

export const reducers: ActionReducerMap<AppState> = {};
export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: [{ 
      auth: {
        encrypt: (state: string) => btoa(state),
        decrypt: (state: string) => atob(state),
      }
    },{
      layout: {
        encrypt: (state: string) => btoa(state),
        decrypt: (state: string) => atob(state),
      }
    },
    { 
      staticEagerLoadingData: {
        encrypt: (state: string) => btoa(state),
        decrypt: (state: string) => atob(state),
      }
    },
    { 
      dynamicEagerLoadingData: {
        encrypt: (state: string) => btoa(state),
        decrypt: (state: string) => atob(state),
      }
    },
    // { 
    //   leadCreationUserData: {
    //     encrypt: (state: string) => btoa(state),
    //     decrypt: (state: string) => atob(state),
    //   }
    // },
    // { 
    //   leadCreationStepperParams: {
    //     encrypt: (state: string) => btoa(state),
    //     decrypt: (state: string) => atob(state),
    //   }
    // },
    // { 
    //   leadHeaderData: {
    //     encrypt: (state: string) => btoa(state),
    //     decrypt: (state: string) => atob(state),
    //   }
  //  }
  ],
    rehydrate: true,                                                 // store app related in local storage data whenever the app refreshes
    //storageKeySerializer: (key: string) => `store-cache.${key}`,
  })(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [];

