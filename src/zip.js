/* Replaceable file PATH : "node_modules\shp-write\src\zip.js"   */

var write = require('./write'),
    geojson = require('./geojson'),
    prj = require('./prj'),
    JSZip = require('jszip');

module.exports = function(gj, options) {

    var zip = new JSZip(),
        layers = zip.folder(options && options.folder ? options.folder : 'layers');
        var myProps =[];
        var myGeoms=[];

	
    [geojson.point(gj), geojson.line(gj), geojson.polygon(gj)]
        .forEach(function(l) {
        if((l.properties.length > 1 && l.properties.length == l.geometries[0].length) || (l.properties.length > 0 && l.properties.length == l.geometries.length && l.geometries[0].length>0))
          {
            for(var i=0; i<l.properties.length; i++){
              myProps.push(l.properties[i]);
              if(l.type == "POINT"){

                myGeoms.push([[l.geometries[i]]]);
              }
              else{
                myGeoms.push([[l.geometries[0][i]]]);
              }
            }
            write(
                // field definitions
                myProps,
                // geometry type
                l.type,
                // geometries
                myGeoms,
                function(err, files) {
                    // var fileName = myProps[0].fileName;//custom filename in each feature's properties
                    var fileName = options && options.types[l.type.toLowerCase()] ? options.types[l.type.toLowerCase()] : l.type;
                    layers.file(fileName + '.shp', files.shp.buffer, { binary: true });
                    layers.file(fileName + '.shx', files.shx.buffer, { binary: true });
                    layers.file(fileName + '.dbf', files.dbf.buffer, { binary: true });
                    layers.file(fileName + '.prj', prj);
              });
          }
    });

    var generateOptions = { compression:'STORE' };
    if (!process.browser) {
      generateOptions.type = 'nodebuffer';
    }
    return zip.generate(generateOptions);
};